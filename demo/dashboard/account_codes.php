<?php
include("account_session.php"); 
if($rst['member_type']=="PLAN1"){
    $idx=2;
}elseif($rst['member_type']=="PLAN2"){
    $idx=3;
}elseif($rst['member_type']=="PLAN3"){
    $idx=4;
}
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <title>
        <?php echo $site_title; ?>
    </title>
    <!-- Extra details for Live View on GitHub Pages -->
    <!-- Canonical SEO -->

    <!--  -->
    <!--     Fonts and icons     -->
    <link href="css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/demo/demo.css" rel="stylesheet" />
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
</head>

<body class="">
    <!-- Extra details for Live View on GitHub Pages -->
    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <?php
      include('account_menu.php'); ?>
        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h5 class="card-category">&nbsp;</h5>
                        </div>
                        <div class="col-sm-12">
                            <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                                <label class="btn btn-sm btn-info btn-simple active" id="0">
                                    <input type="radio" class="d-none d-sm-none" name="options"
                                        onchange="javascript:window.location='codeslist'">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Package Pins</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-single-02"></i>
                                    </span>
                                </label>
                                <label class="btn btn-sm btn-info btn-simple" id="1">
                                    <input type="radio" class="d-none d-sm-none" name="options"
                                        onchange="javascript:window.location='downloadcodes'">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Download
                                        Pins</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-link-72"></i>
                                    </span>
                                </label></a>
                                <label class="btn btn-sm btn-info btn-simple" id="2">
                                    <input type="radio" class="d-none" name="options"
                                        onchange="javascript:window.location='codesmulti'">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Transfer
                                        Pins</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-vector"></i>
                                    </span>
                                </label>
                                <label class="btn btn-sm btn-info btn-simple" id="3">
                                    <input type="radio" class="d-none" name="options"
                                        onchange="javascript:window.location='codeslogs'">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Transfer
                                        History</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-trophy"></i>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Br>
            <div class="row">
                <div class="col-12">
                    <div class="card card-chart">
                        <div class="card-header ">
                            <div class="row">
                                <div class="col-sm-12 text-left">
                                    <h5 class="card-category">Dashboard -> Package Pins -> <a href="codeslist">My
                                            Package Pins</a></h5>
                                    <h2 class="card-title">My Package Pins</h2>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php
          if(isset($_GET['m'])){
            ?>

            <div class="alert alert-info">
                <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                </button>
                <span><?php echo $mcrypt->decrypt($_GET['m']); ?></span>
            </div>
            <?php } ?>
            <?php
          if(isset($_GET['e'])){
            ?>
            <div class="alert alert-warning">
                <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                </button>
                <span><?php echo $mcrypt->decrypt($_GET['e']); ?></span>
            </div>
            <?php } ?>

            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <div class="card ">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table tablesorter " id="">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date Created</th>
                                            <th>Activation code</th>
                                            <th>Pin</th>
                                            <th>Package</th>
                                            <th>Type</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                       
                                        $strSQL="SELECT * FROM tbl_registration_codes WHERE  purchase_code like '%". $_SESSION['shot_user_id'] ."%' AND status=0 order by date_created,registration_type DESC";                                                                       
                                        //echo $strSQL;
                                        $rsb=mysql_query($strSQL) or die(mysql_error(). $strSQL);
                                        $rsbt=mysql_fetch_assoc($rsb);
                                        $rsbt_count=mysql_num_rows($rsb); 
                            
                                            
                                            if(mysql_num_rows($rsb)>0){
                                            $i=1;
                                            do{
                                                // $name="";
                                                // if($rsbt['id_code']!=""){
                                                //     $strSQL="SELECT firstname,lastname FROM tbl_members WHERE id_number='". $rsbt['id_code'] ."'";
                                                //     $rsm=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                                //     $rsmt=mysql_fetch_assoc($rsm);
                                                //     $name=$rsmt['firstname'] . " ".$rsmt['lastname'];
                                                // }
                                         ?>
                                        <tr>
                                            <td><?=$i++?></td>
                                            <td><?php echo $rsbt['date_created']; ?></td>
                                            <td><?php echo $rsbt['registration_code']; ?></td>
                                            <td><?php echo $rsbt['password_code']; ?></td>
                                            <td><?php echo loadPosition($rsbt['registration_type']); ?></td>
                                            <td><?php if($rsbt['free']==0){ echo "PAID"; }elseif($rsbt['free']==1) { echo "FREE"; }elseif($rsbt['free']==2){ echo "CD"; }?>
                                            </td>
                                            <td class="text-center"><a
                                                    href="../members/regcode/<?php echo $mcrypt->encrypt($rsbt['registration_ctr']); ?>"
                                                    target="_new"><button
                                                        class="btn btn-sm btn-fill btn-info">Register</button></a> <a
                                                    href="codestransfer/<?php echo $mcrypt->encrypt($rsbt['registration_code']); ?>">
                                                    <button class="btn btn-sm btn-fill btn-info">Transfer</button></a>
                                                <?php
                                                for($x=$idx;$x<=$TOTAL_ENTRIES;$x++){ 
                                                    if($rsbt['registration_type']=="PLAN".$x){ ?>
                                                <a
                                                    href="upgcode/<?php echo $mcrypt->encrypt($rsbt['registration_ctr']); ?>">
                                                    <button class="btn btn-sm btn-fill btn-info">Upgrade</button></a>
                                                    <?php } ?>
                                                    <?php } ?>
                                                <a
                                                    href="addcode/<?php echo $mcrypt->encrypt($rsbt['registration_code']); ?>"><button
                                                        class="btn btn-sm btn-fill btn-info">Add Account</button></a>
                                            </td>
                                        </tr>
                                        <?php
                                            }while($rsbt=mysql_fetch_assoc($rsb));
                                        }
                                         ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <?php include('account_footer.php'); ?>

        </div>


    </div>
    </div>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Place this tag in your head or just before your close body tag. -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
    <!-- Chart JS -->
    <script src="assets/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script>
    <!-- Black Dashboard DEMO methods, don't include it in your project! -->
    <!--<script src="assets/demo/demo.js"></script>-->


    <script>
    $(document).ready(function() {
        $('body').addClass('white-content');
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
    </script>

</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->

</html>