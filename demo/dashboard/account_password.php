<?php
include('account_session.php');
function removehack($str) {
    $str=str_replace("\"","",$str);
    $str=str_replace("/","",$str);
    $str=str_replace("\\","",$str); 
    $str=str_replace("'","",$str);
    $str=str_replace("&","",$str);
    $str=str_replace("-","",$str);
    $str=str_replace("*","",$str);
    $str=str_replace(")","",$str);
    $str=str_replace("(","",$str);
    $str=str_replace("=","",$str);  
    $str=str_replace("#","",$str);      
    return $str;
} 
$message="";
if(isset($_POST['allow']) && $_POST['allow']=="YES"){
    $errorstr="";
    $password=removehack($_POST['password']);
    $new_password=removehack($_POST['new_password']);
    $confirm_password=removehack($_POST['confirm_password']);       
    if($password=="") { $errorstr.="Invalid Password<br>"; }
    if($new_password=="") { $errorstr.="Invalid New Password<br>"; }    
    if($confirm_password=="") { $errorstr.="Invalid Confirm Password<br>"; }    
    if($confirm_password!=$new_password){ $errorstr.="New and Confirm Password must be the same<br>"; }
    if($errostr==""){
        $strSQL="SELECT * FROM tbl_members WHERE username='" .$_SESSION['shot_username'] ."' AND password=ENCODE('$password','$encrypt_password')";  
        $rs=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
        $rst=mysql_fetch_assoc($rs);    
        if(mysql_num_rows($rs)>0){
            $updateSQL="UPDATE tbl_members SET password=ENCODE('$new_password','$encrypt_password') WHERE id_number='". $_SESSION['shot_user_id'] ."'";
            mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);
            $country=$_SESSION['country'];
            session_unset();
            session_destroy();
            session_start();
            $_SESSION['country']=$country;
            header("Location: ../members/logmessage/". $mcrypt->encrypt("Account Password was successfully change"));
        }else{
            $errorstr.="Invalid Account Password";
        }
    }
}
$strSQL="SELECT id_number,id_code,email,mobile,address,tin,birthdate,country,status,firstname,lastname,picture FROM tbl_members WHERE id_number='" .$_SESSION['shot_user_id'] ."'";
$rs=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rst=mysql_fetch_assoc($rs);
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    <?php echo $site_title; ?>
  </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->

  <!--  -->
  <!--     Fonts and icons     -->
  <link href="css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->

  <!-- End Google Tag Manager -->
</head>
<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
      <?php
      include('account_menu.php'); ?>
      <!-- End Navbar -->
      <div class="content">                
        <div class="row">
          <div class="col-12">
            <div class="card card-chart">
              <div class="card-header ">
                <div class="row">
                  <div class="col-sm-12 text-left">
                    <h5 class="card-category">Dashboard -> <a href="myprofile">Change Password</a></h5>
                    <h2 class="card-title">Change Account Password / Login Password</h2>
                  </div>                  
                </div>
              </div>
              
            </div>
          </div>
        </div>
                      <?php
          if(isset($_GET['m'])){
            ?>

        <div class="alert alert-success">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $mcrypt->decrypt($_GET['m']); ?></span>
                </div>
              <?php } ?>
               <?php
          if($errorstr!=""){
            ?>
                <div class="alert alert-warning">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $errorstr; ?></span>
                </div>
                <?php } ?>
        
               <form name="frm" action="" method="post" novalidate="novalidate" enctype="multipart/form-data">
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              
              <div class="card-body">
              
                  <div class="row">
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>New Password</label>
                         <input name="new_password" type="password" class="form-control" placeholder="" value="" maxlength="20">
                      </div>
                    </div>
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Confirm New Password</label>
                        <input name="confirm_password" type="password" class="form-control" placeholder="" maxlength="20">
                      </div>
                    </div>
                    
                  
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Current Password</label>
                         <input name="password" type="password" class="form-control" maxlength="20">
                      </div>
                    </div>
                    
                    
                  
                  </div>               
                                
               </div>
              </div>
              <div class="card-footer">
                
                 <div id="regbtn"><button type="button" class="btn btn-fill btn-info btn-sm" onclick="javascript: validate();">Change Password</button></div>
              </div>
            </div>
          </div>

        </div>
        <input type="hidden" name="allow" value="NO" />
        <input type="hidden" name="rep_ctr" value="0" />                      
         </form>

      <?php include('account_footer.php'); ?>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Chart JS -->
  <script src="assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script><!-- Black Dashboard DEMO methods, don't include it in your project! -->
    <script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" src="scripts/formvalidations.js"></script>
        <script language="javascript" type="text/javascript">
        function validate(){
            var errorstr="";
            if(document.frm.password.value==""){
                errorstr+="Old Password is required\n";
            }
            if(document.frm.new_password.value==""){
                errorstr+="New Password is required\n";
            }
            if(document.frm.confirm_password.value==""){
                errorstr+="Confirm Password is required\n";
            }
            if(document.frm.confirm_password.value!=document.frm.new_password.value){
                errorstr+="New and Confirm Password must be the same\n";
            }
            
            if(errorstr==""){
                if(confirm("Continue changing your Account Password?\nNote: You will automatically logout the system once successful")){
                    document.frm.allow.value="YES";
                    document.frm.submit();
                }
            }else{
                alert(errorstr);
            }
        }
    </script> 


  
  <script>
    $(document).ready(function() {
      $('body').addClass('white-content');
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
 
</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
</html>