<?php
include('account_session.php');
$message="";
if(isset($_POST['allow_update']) && $_POST['allow_update']=="YES"){
 
    $birth=$_POST['bday_year'] ."-" .$_POST['bday_month'] ."-" . $_POST['bday_day'];
    $updateSQL="UPDATE tbl_members SET
                birthdate='$birth',                 
                email='" .checkfield('email') ."',                  
                mobile='" .checkfield('mobile') ."',
                address='" .checkfield('address') ."',
                tin='" .checkfield('tin') ."',             
                bank_swift='" .checkfield('bank_swift') ."'             
                WHERE id_number='" .$_SESSION['shot_user_id'] ."'";
    mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);  

    header("Location: myprofile?m=" .$mcrypt->encrypt("Personal Account Successfully Updated"));
}
$strSQL="SELECT id_number,id_code,email,mobile,address,tin,birthdate,country,status,firstname,lastname,picture FROM tbl_members WHERE id_number='" .$_SESSION['shot_user_id'] ."'";
$rs=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rst=mysql_fetch_assoc($rs);
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    <?php echo $site_title; ?>
  </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->

  <!--  -->
  <!--     Fonts and icons     -->
  <link href="css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->

  <!-- End Google Tag Manager -->
</head>
<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
      <?php
      include('account_menu.php'); ?>
      <!-- End Navbar -->
      <div class="content">                
        <div class="row">
          <div class="col-12">
            <div class="card card-chart">
              <div class="card-header ">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">Dashboard -> <a href="myprofile">Profile</a></h5>
                    <h2 class="card-title">Profile Information</h2>
                  </div>                  
                </div>
              </div>
              
            </div>
          </div>
        </div>
                      <?php
          if(isset($_GET['m'])){
            ?>

        <div class="alert alert-success">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $mcrypt->decrypt($_GET['m']); ?></span>
                </div>
              <?php } ?>
               <?php
          if($errorstr!=""){
            ?>
                <div class="alert alert-warning">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $errorstr; ?></span>
                </div>
                <?php } ?>
        
               <form name="frm" action="" method="post" novalidate="novalidate" enctype="multipart/form-data">
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              
              <div class="card-body">
              
                  <div class="row">
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Email Address</label>
                         <input name="email" type="text" class="form-control" placeholder="" value="<?php echo $rst['email']; ?>">
                      </div>
                    </div>
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Contact Information</label>
                        <input name="mobile" type="text" class="form-control" placeholder="Mobile" value="<?php echo $rst['mobile']; ?>">
                      </div>
                    </div>
                    
                  
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>TIN Number</label>
                          <input name="tin" type="text" class="form-control" placeholder="NA" value="<?php echo $rst['tin']; ?>">
                      </div>
                    </div>
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Address</label>
                        <input name="address" type="text" class="form-control" placeholder="NA" value="<?php echo $rst['address']; ?>">
                      </div>
                    </div>
                    
                  
                  </div>               
                                   <div class="row">
                    <div class="col-md-6 pr-md-1">
                      <div class="form-group">
                        <label>Birthdate</label><br>
                          <select name="bday_month" class="form-drop" id="bday_month" >
                                                     <?php 
                                    $arr=split("-",$rst['birthdate']);
                                    $value=$_POST['bday_month'];
                                    $value=$arr[1];
                                    ?>
                                    <?php  if($value!=0) 
                                    {           ?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="bday_day" class="form-drop" id="bday_day">
                                    <?php
                                    $endmonth = 31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$arr[2]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="bday_year" class="form-drop" id="bday_year" >
                                    <?php
                                    for ($i=1900; $i<=date("Y")-18; $i++) {
                                    if($i==$arr[0]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                  </select>
                      </div>
                    </div>
                    
                    
                  
                  </div>
                </div>
               
              </div>
              <div class="card-footer">
                
                 <div id="regbtn"><button type="button" class="btn btn-fill btn-info btn-sm" onclick="javascript: validate();">Update Profile Settings</button></div>
              </div>
            </div>
          </div>

        </div>
          <input type="hidden" name="allow_update" value="NO" />                     
         </form>

      <?php include('account_footer.php'); ?>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Chart JS -->
  <script src="assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script><!-- Black Dashboard DEMO methods, don't include it in your project! -->
    <script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" type="text/javascript">
   
        function validate(){
                if(confirm("Continue updating your Profile")){
                    document.frm.allow_update.value="YES";
                    document.frm.submit();
                }
        }

    </script> 


  
  <script>
    $(document).ready(function() {
      $('body').addClass('white-content');
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
 
</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
</html>