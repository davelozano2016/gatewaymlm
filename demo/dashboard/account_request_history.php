<?php
include("account_session.php"); 
if($rst['status']=="Inactive"){
  header("Location: activate");    
}

?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    <?php echo $site_title; ?>
  </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->

  <!--  -->
  <!--     Fonts and icons     -->
  <link href="css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->

  <!-- End Google Tag Manager -->
</head>
<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
      <?php
      include('account_menu.php'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-12">
                <div class="row">
                  <div class="col-sm-12 text-left">
                    <h5 class="card-category">&nbsp;</h5>                  
                  </div>
                  <div class="col-sm-12">
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                      <label class="btn btn-sm btn-info btn-simple" id="0">
                        <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='encash'" >
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Encash Bonus</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-money-coins"></i>
                        </span>
                      </label>
                      <label class="btn btn-sm btn-info btn-simple active" id="1">
                        <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='enhistory'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Encashment Hsitory</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-align-left-2"></i>
                        </span>
                      </label></a>
                      <!--<label class="btn btn-sm btn-info btn-simple" id="2">-->
                      <!--  <input type="radio" class="d-none" name="options" onchange="javascript:window.location='gcencash'">-->
                      <!--  <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" >GC Request</span>-->
                      <!--  <span class="d-block d-sm-none">-->
                      <!--    <i class="tim-icons icon-cart"></i>-->
                      <!--  </span>-->
                      <!--</label>-->
                      <!--<label class="btn btn-sm btn-info btn-simple" id="2">-->
                      <!--  <input type="radio" class="d-none" name="options" onchange="javascript:window.location='gchistory'">-->
                      <!--  <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" >GC History</span>-->
                      <!--  <span class="d-block d-sm-none">-->
                      <!--    <i class="tim-icons icon-delivery-fast"></i>-->
                      <!--  </span>-->
                      <!--</label>                      -->
                </div>
              </div>
              
            </div>
          </div>
        </div>
        <br>
                <div class="row">
          <div class="col-12">
            <div class="card card-chart">
              <div class="card-header ">
                <div class="row">
                  <div class="col-sm-12 text-left">
                    <h5 class="card-category">Dashboard -> Encashment -> <a href="enhistory">Encashment History</a></h5>
                    <h2 class="card-title">Encashment History</h2>
                  </div>
                  
                </div>
              </div>
              
            </div>
          </div>
        </div>
               <?php
          if(isset($_GET['m'])){
            ?>

        <div class="alert alert-info">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $mcrypt->decrypt($_GET['m']); ?></span>
                </div>
              <?php } ?>
               <?php
          if(isset($_GET['e'])){
            ?>
                <div class="alert alert-warning">
                  <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                  </button>
                  <span><?php echo $mcrypt->decrypt($_GET['e']); ?></span>
                </div>
                <?php } ?>
    
        <div class="row">

          <div class="col-lg-12 col-md-12">
            <div class="card ">
            
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                   <thead>
                                    <tr>
                                              <th>Reference No:</th>
                                              <th>Date</th>
                                              <th>Date Payout</th>
                                              <th>Type</th>       
                                              <th>Status</th>
                                              <th>Email</th>         
                                              <th>Address</th>                                                                                                              
                                              <th>Amount</th> 
                                              <th>Tax</th>
                                              <th>Fee</th>
                                              <th>Payout</th>
                                    </tr>
                                </thead>
                                <tbody>
              <?php 

      
    $strSQL="SELECT * FROM tbl_encashment WHERE id_code='" . $_SESSION['shot_user_id'] ."' AND request_type!='OPT1' AND request_type!='OPT2' AND request_type!='OPT3'order by request_ctr DESC";
//  echo $strSQL;
    $rse=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
    $rset=mysql_fetch_assoc($rse);
    $rset_count=mysql_num_rows($rse);
    if($rset_count>0){
            do{
                /*$sv+=$rset['amount'];
                $tax+=$rset['tax'];
                $cd+=$rset['cd'];
                $pfee+=$rset['fee'];
                
                $payout+=$rset['payout'];*/
?>                            
                                       
      <tr>
            <td>
            <?php   if($rset['e_code']!="NA"){
                        echo $rset['e_code']; 
                    }else{
                        if($rslt['ECASH']=="ACTIVE"){                       
                            echo "WAITING..";

                        }else{
                            echo "PROCESSING..";
                        }
                    }
            ?>
                                    
                                          <td><?php echo $rset['date_requested']; ?></td>
                                          <td><?php echo $rset['date_due']; ?></td>
                                          <td><?php echo $rset['request_type']; ?></td>
                                          <td><?php echo $rset['status']; ?></td>
                                          <td><?php echo $rset['email']; ?></td>             
                                          <td><?php echo $rset['bank_acctno']; ?></td>                                                                
                                          <td><?php echo number_format($rset['amount'] * $_SESSION['shot_rate'],2);?> <?php echo $_SESSION['shot_cur'] ;?></td>
                                          <td><?php echo number_format($rset['tax'] * $_SESSION['shot_rate'],2);?> <?php echo $_SESSION['shot_cur'] ;?></td>
                                          <td><?php echo number_format($rset['fee'] * $_SESSION['shot_rate'],2);?> <?php echo $_SESSION['shot_cur'] ;?></td>
                                          <td><?php echo number_format($rset['payout'] * $_SESSION['shot_rate'],2);?> <?php echo $_SESSION['shot_cur'] ;?></td>
                                                                         
                                    </tr>
                                     <?php
                                    //$count++;
                                    }while($rset=mysql_fetch_assoc($rse));
                                    }
                                    ?>                                  
                                    
                                </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

          </div>
          <?php include('account_footer.php'); ?>

        </div>
 

    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Chart JS -->
  <script src="assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script><!-- Black Dashboard DEMO methods, don't include it in your project! -->
 
  
  <script>
    $(document).ready(function() {
      $('body').addClass('white-content');
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
 
</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
</html>