<div class="sidebar">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
    <div class="sidebar-wrapper">
        <a href="myphoto"><?php
                        $dir="images/gallery/".$_SESSION['shot_user_id'];
                          if($rst['picture']=='NA'){
              ?>
            <div class="photo" align="center">
                <br>
                <img src="images/default.png" alt="Profile Photo" class="img-circle" height="100" align="center">
            </div>
            <?php 
                  }else{
              ?>
            <div class="photo" align="center">
                <br>
                <img src="<?php echo $dir ."/". $rst['picture']; ?>" height="100" />
            </div>
            <?php
              }
              ?>
        </a>
        <div class="logo">
            <a href="summary" class="simple-text logo-mini">
                ID:
            </a>
            <a href="summary" class="simple-text logo-normal">
                <?php echo $rst['id_number'] . "<br>" .$rst['id_code']; ?>
            </a>
        </div>
        <ul class="nav">
            <li>
                <a href="summary">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <?php
          if($rst['status']=="Active"){
          ?>
            <li>
                <a href="myaccounts">
                    <i class="tim-icons icon-single-02"></i>
                    <p>My Accounts</p>
                </a>
            </li>
            <li>
                <a href="codeslist">
                    <i class="tim-icons icon-laptop"></i>
                    <p>Package Pins</p>
                </a>
            </li>
            <li>
                <a href="bonus/<?php echo $mcrypt->encrypt("DSI"); ?>">
                    <i class="tim-icons icon-money-coins"></i>
                    <p>My Bonus History </p>
                </a>
            </li>
            <li>
                <a href="encash">
                    <i class="tim-icons icon-bank"></i>
                    <p>Encashment</p>
                </a>
            </li>
            <!--<li>-->
            <!--    <a href="productcodes">-->
            <!--        <i class="tim-icons icon-cart"></i>-->
            <!--        <p>My Product</p>-->
            <!--    </a>-->
            <!--</li>-->
            <li>
                <a href="bingraph">
                    <i class="tim-icons icon-single-02"></i>
                    <p>Genealogy</p>
                </a>
            </li>
            <?php }else{ ?>
            <li>
                <a href="activate">
                    <i class="tim-icons icon-button-power"></i>
                    Activate Account

                </a>
            </li>
            <?php } ?>
            <li>
                <a href="../members/register" target="_new">
                    <i class="tim-icons icon-pencil"></i>
                    Register New Account

                </a>
            </li>
            
            
        </ul>
        <br>
    </div>
</div>
<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle d-inline">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <a style="position:relative" class="navbar-brand" href="summary"><img src="images/logo.png" class="img-responsive"
                        width="100px"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="navbar-nav ml-auto">
              
                    <li class="dropdown nav-item">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            <?php
                        $dir="images/gallery/".$_SESSION['shot_user_id'];
                          if($rst['picture']=='NA'){
              ?>
                            <div class="photo" align="center">

                                <img src="assets/img/anime3.png" alt="Profile Photo" align="center">
                            </div>
                            <?php 
                  }else{
              ?>
                            <div class="photo">
                                <img src="<?php echo $dir ."/". $rst['picture']; ?>" alt="Profile Photo" />
                            </div>
                            <?php
              }
              ?>
                            <!--<div class="photo">
                    <img src="assets/img/anime3.png" alt="Profile Photo">
                  </div>-->
                            <b class="caret d-none d-lg-block d-xl-block"></b>
                            <p class="d-lg-none">
                                Settings
                            </p>
                        </a>
                        <ul class="dropdown-menu dropdown-navbar">
                            <li class="nav-link"><a href="myprofile" class="nav-item dropdown-item">Profile</a></li>
                            <li class="dropdown-divider"></li>
                            <li class="nav-link"><a href="mybank" class="nav-item dropdown-item">Bank</a></li>
                            <li class="dropdown-divider"></li>
                            <li class="nav-link"><a href="changepass" class="nav-item dropdown-item">Change Password</a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="nav-link"><a href="signout" class="nav-item dropdown-item">Log out</a></li>
                        </ul>
                    </li>
                    <li class="separator d-lg-none"></li>
                </ul>
            </div>
        </div>
    </nav>