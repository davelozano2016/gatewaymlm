		var intRegex = /^(0|[1-9][0-9]*)$/;
		var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/i;		
		var x;	
		
		var bv=0;
		var price=0;
		var disc=0;
		var tprice=0;
		var items=0;		
		
		function compute(ctr){
		var errorstr="";
		
		bv=0;
		price=0;
		items=0;
		disc=0;
		tprice=0;

		for(x=1;x<=ctr;x++){
			if(!intRegex.test(document.forms.frm.elements['t'+x].value) && !floatRegex.test(document.forms.frm.elements['t'+x].value) && document.forms.frm.elements['t'+x].value!=""){
				alert("Invalid Data Entry");
				//document.forms.frm.elements['t'+x].value="0";
			}else if(parseFloat(document.forms.frm.elements['t'+x].value)>parseFloat(document.forms.frm.elements['i'+x].value)){
				alert("You have entered greater than available stocks");
				document.getElementById('tr'+x).bgColor='#FF0000';
			}else if(document.forms.frm.elements['t'+x].value==""){
				document.getElementById('tr'+x).bgColor='#FFFFFF';
			}else if(document.forms.frm.elements['t'+x].value==0){
				document.forms.frm.elements['t'+x].value="";
			}else{
				if(parseInt(document.forms.frm.elements['t'+x].value)>0){
					document.getElementById('tr'+x).bgColor='#FFFFCC';
					bv+=parseInt(document.forms.frm.elements['b'+x].value)* parseFloat(document.forms.frm.elements['t'+x].value);
					price+=parseFloat(document.forms.frm.elements['p'+x].value) * parseFloat(document.forms.frm.elements['t'+x].value);
					items+=parseFloat(document.forms.frm.elements['t'+x].value);
						
				}else{
					document.getElementById('tr'+x).bgColor='#FFFFFF';				
				}
				document.forms.frm.elements['t'+x].value=parseInt(document.forms.frm.elements['t'+x].value);							
			}
		}
		if(!intRegex.test(document.forms.frm.discount.value) || !floatRegex.test(document.forms.frm.discount.value)){
			errorstr="Invalid Discount Format";
			//document.forms.frm.elements['t'+x].value="0";
		}		
		if(errorstr==""){
			disc=price * (parseFloat(document.forms.frm.discount.value)/100);
			//alert(disc);
			tprice=price-disc;
			//document.getElementById("t_bv").innerHTML="Total BV:<strong> " + bv + " BV</strong>";
			document.getElementById("t_items").innerHTML="Total Items:<strong> " + items + " Items</strong>";
			document.getElementById("t_sub").innerHTML="Sub Total:<strong> " + price + " PHP</strong>";			
			document.getElementById("t_disc").innerHTML="Discount:<strong> " + disc + " PHP</strong>";			
			document.getElementById("t_price").innerHTML="Total Amount:<strong> " + tprice + " PHP</strong>";			
						
		}else{
			alert(errorstr);
		}		
	}
	
	function pview(ctr){
		window.open("products_view.php?p="+ctr);
		return false;
	}
	
	function setzero(ctr){
		for(x=1;x<=ctr;x++){
			document.forms.frm.elements['t'+x].value="";
		}	
		compute(ctr);	
	}
	
	function ask(ctr){
		if(parseInt(items)>0){
			if(confirm("Continue Ordering Products?")){
				document.frm.allow.value="YES";
				document.frm.submit();
			}
		}else{
			alert("You must order at least 1 product");
		}
	}
	function remove_cart(ctr){
		if(confirm("Continue Removing this Product to Cart?")){
			document.frm.cart_ctr.value=ctr;
			document.frm.allow.value="YES";
			document.frm.submit();
		}
	}
	function confirm_order(){
			if(confirm("Continue Submit your Order?")){
				document.frm.action="account_cart_proc.php";
				document.frm.allow.value="YES";
				document.frm.submit();
			}		
	}
	function confirm_order(){
			if(confirm("Continue Save your Order?")){
				document.frm.action="account_cart_proc.php";
				document.frm.allow.value="YES";
				document.frm.submit();
			}		
	}	
