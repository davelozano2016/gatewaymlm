		var intRegex = /^(0|[1-9][0-9]*)$/;
		var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/i;		
		var x;	
		
		var bv=0;
		var price=0;
		var items=0;
		var sfee=0;		
		
		function compute(ctr){
		var errorstr="";
		
		bv=0;
		price=0;
		items=0;
		sfee=parseFloat(document.forms.frm.sfee.value);
		
		for(x=1;x<=ctr;x++){
			if(!intRegex.test(document.forms.frm.elements['t'+x].value) && !floatRegex.test(document.forms.frm.elements['t'+x].value) && document.forms.frm.elements['t'+x].value!=""){
				alert("Invalid Data Entry");
				//document.forms.frm.elements['t'+x].value="0";
			}else if(document.forms.frm.elements['t'+x].value==""){
				document.getElementById('tr'+x).bgColor='#FFFFFF';
			}else if(document.forms.frm.elements['t'+x].value==0){
				document.forms.frm.elements['t'+x].value="";
			}else{
				if(parseInt(document.forms.frm.elements['t'+x].value)>0){
					document.getElementById('tr'+x).bgColor='#CCCCCC';
					bv+=parseInt(document.forms.frm.elements['b'+x].value)* parseFloat(document.forms.frm.elements['t'+x].value);;
					price+=parseFloat(document.forms.frm.elements['p'+x].value) * parseFloat(document.forms.frm.elements['t'+x].value);
					items+=parseFloat(document.forms.frm.elements['t'+x].value);	
				}else{
					document.getElementById('tr'+x).bgColor='#FFFFFF';				
				}
				document.forms.frm.elements['t'+x].value=parseInt(document.forms.frm.elements['t'+x].value);							
			}
		}
		if(errorstr==""){
			document.frm.subtotal.value=formatNumber(price);
			document.frm.sfee.value=formatNumber(sfee);			
			document.frm.total_amount.value=formatNumber(price+sfee);		
		}else{
			alert(errorstr);
		}		
	}
	
	function pview(ctr){
		window.open("products_view.php?p="+ctr);
		return false;
	}
	
	function setzero(ctr){
		for(x=1;x<=ctr;x++){
			document.forms.frm.elements['t'+x].value="";
		}	
		compute(ctr);	
	}
	
	function ask(ctr){
	var errorstr="";
		
	errorstr += checkitem(document.frm.fname,"Full Name");	
	errorstr += checkitem(document.frm.mobile,"Mobile");		
	errorstr += checkitem(document.frm.address,"Address");
	if(document.frm.ship_type.value==0){
		errorstr+="Shipping Method is required\n";
	}
		if(errorstr==""){	
				if(parseInt(items)>0){
					if(confirm("Continue Submit Order Form?")){
						document.getElementById("subbtn").innerHTML="Kindly wait while we process your request";				
						document.frm.action="account_cart_order_proc.php";				
						document.frm.allow.value="YES";
						document.frm.submit();
					}
				}else{
					alert("You must order at least 1 product");
				}
		}else{
			alert(errorstr);
		}
	}
	function remove_cart(ctr){
		if(confirm("Continue Removing this Product to Cart?")){
			document.frm.cart_ctr.value=ctr;
			document.frm.allow.value="YES";
			document.frm.submit();
		}
	}
	function confirm_cancel(){
			if(confirm("Continue to Cancel Order Form?")){				
				document.getElementById("subbtn").innerHTML="Kindly wait while we process your request";				
				document.frm.action="account_cart_cancel_proc.php";				
				document.frm.allow.value="YES";
				document.frm.submit();			}		
	}
	
	function confirm_upload(){
			var errorstr = "";
			var rfile="";
			var r2file="";
			var cnt=0;
			var charRegex="^[a-zA-Z0-9_.]*$";
			
			
			
				if(document.frm.approve_user.value==0){
					errorstr+="Order To is required\n";
				}
				if(document.frm.ship_type.value==0){
					errorstr+="Shipping Method is required\n";
				}
				if(parseInt(items)==0){
					errorstr+="Enter at least 1 product\n";
						
				}							
			
         	 	if(document.frm.ship_type.value!="PICK-UP"){
					for(x=1;x<=1;x++){
						if(document.forms.frm.elements['picture'+ x].value!=""){
							rfile=document.forms.frm.elements['picture'+ x].value;
							r2file=rfile.substring(rfile.length-4,rfile.length);				
							rfile=rfile.substring(rfile.length-3,rfile.length);	
							rfile=rfile.toLowerCase();
							r2file=r2file.toLowerCase();	
								if(rfile!="png" && rfile!="gif" && rfile!="jpg" && r2file!="jpeg"){
									errorstr +="Invalid Photo Selection \n";					
								}else{
									cnt++;
								}
						}
					}	
					
					if(cnt==0){
						errorstr +="Select a Payment Receipt first\n";
					}	
				}								
                if(errorstr==""){
                    if(confirm("Continue to Confirm Order Form?")){
						document.getElementById("upbtn").innerHTML="Kindly wait while we upload your Payment Receipt";
						document.getElementById("savebtn").innerHTML="";										                    	
						document.frm.action="res_order_form_proc.php";	                    	
                        document.frm.allow.value="YES";                        
                        document.frm.submit();
                    }
                }else{
                    alert(errorstr);
                }
            	
	}	
	
    function confirm_save(){
			var errorstr = "";
			var rfile="";
			var r2file="";
			var cnt=0;
			var charRegex="^[a-zA-Z0-9_.]*$";
			
			
			
				if(document.frm.approve_user.value==0){
					errorstr+="Order To is required\n";
				}
				if(document.frm.ship_type.value==0){
					errorstr+="Shipping Method is required\n";
				}
				if(parseInt(items)==0){
					errorstr+="Enter at least 1 product\n";
						
				}							
			
         	 			
                if(errorstr==""){
                    if(confirm("Continue to Save Order Form?")){
						document.getElementById("savebtn").innerHTML="Kindly wait while Saving to Order Form";		
						document.getElementById("upbtn").innerHTML="";		                    	
						document.frm.action="res_order_form_save_proc.php";	                    	
                        document.frm.allow.value="YES";                        
                        document.frm.submit();
                    }
                }else{
                    alert(errorstr);
                }
            	
	}		
	
	function formatNumber(number)
	{
    number = number.toFixed(2) + '';
    x = number.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
	}
	
	function loadShip(){
		var arr;
		var Process;
		Process=document.frm.approve_user.value;
		arr=Process.split("~");	
		if(document.frm.ship_type.value!="PICK-UP"){
			document.getElementById("bank").innerHTML=arr[3];
			document.frm.sfee.value=arr[1];
			document.frm.shipin.value=arr[1];
			document.frm.shipout.value=arr[2];
			compute(document.frm.pcount.value);
		}else{
			document.frm.sfee.value=0;
			compute(document.frm.pcount.value);
		}
		
	}	
	
	function loadDelivery(){		
		if(document.frm.ship_type.value=="PICK-UP"){
			document.getElementById("deldiv").innerHTML="";
		}else{
			document.getElementById("deldiv").innerHTML="<div class=\"form-group col-md-12\"><label><h4>Bank Information:</h4></label><div id=\"bank\"></div></div><div class=\"form-group col-md-12\"><label><h4>Upload Payment Slip:</h4></label><input name=\"picture1\" type=\"file\"  id=\"picture1\" size=\"30\" accept=\"image/jpeg\"  ></div>";
		}
		loadShip();	
		
	}	
	function loadLocation(){		
		if(document.frm.ship_type.value=="PICK-UP"){
			document.getElementById("deldiv").innerHTML="";
		}else{
			if(document.frm.ship_location.value=="IN"){
				document.frm.sfee.value=document.frm.shipin.value;
			}else if(document.frm.ship_location.value=="OUT"){
				document.frm.sfee.value=document.frm.shipout.value;
			}
		}
		
	}			
	
