function validate(){
	 var errorstr="";
	 var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
	 var intRegex = /^(0|[1-9][0-9]*)$/;	 
	 			if(document.frm.request_type.value=="0"){
					errorstr+="Request Type is required\n";
				}
				//validate Rewards
				if(document.frm.amount.value==""){
					errorstr+="Rebates is required\n";
					document.frm.amount.value=0;
				}else{
					if(parseFloat(document.frm.amount.value)>=0){
						if(!floatRegex.test(document.frm.amount.value) && !intRegex.test(document.frm.amount.value) ){
							errorstr += "Please specify valid Rebates\n";
						}else{
							if(parseFloat(document.frm.amount.value)>parseFloat(document.frm.ecash.value)){
								errorstr += "The amount you entered is greater than your current Rebates\n";
							}
							if(parseFloat(document.frm.amount.value)>0){
								if(parseFloat(document.frm.min_ecash.value)>parseFloat(document.frm.amount.value)){
									errorstr += "The amount you entered is less than the minimum required rebates\n";
								}
							}	
							if(parseFloat(document.frm.amount.value)>0){
								if(parseFloat(document.frm.max_ecash.value)<parseFloat(document.frm.amount.value)){
									errorstr += "The amount you entered is more than the maximum required rebates\n";
								}
							}													
						}		
					}else{
						errorstr += "Please specify valid Rebates\n";
					}
				}
				//check if at least 1 request
				if(parseFloat(document.frm.amount.value)==0){
					errorstr+="You must request an Rebates\n";	
				}
				if(document.frm.password.value==""){
					errorstr+="Account Password is required\n";		
				}
				if(document.frm.email.value==""){
					errorstr+="Email is required\n";		
				}								
					
				if(document.frm.mobile.value==""){
					errorstr+="Mobile is required\n";		
				}								
				if(errorstr==""){
					pass="ecash~"+document.frm.password.value+"~"+document.frm.amount.value;
					najax(pass);
				}else{
					alert(errorstr);
				}

}

function compute(){
	 var errorstr="";
	 var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
	 var intRegex = /^(0|[1-9][0-9]*)$/;	 
				//validate Rewards
		if(document.frm.amount.value==""){
			//errorstr+="Deposit is required\n";
			//document.frm.amount.value=0.02;
		}else{
				if(!floatRegex.test(document.frm.amount.value) && !intRegex.test(document.frm.amount.value) ){
					//errorstr += "Please specify valid Deposit\n";
				}
				if(parseFloat(document.frm.amount.value)>parseFloat(document.frm.max_ecash.value)){
					//errorstr += "Deposit amount is greater than Maximum Deposit\n";
				}
				if(parseFloat(document.frm.amount.value)<parseFloat(document.frm.min_ecash.value)){
					//errorstr += "Deposit amount is less than Minimum Deposit\n";
				}
					
				
		}
		if(errorstr==""){
			pass="compute~"+document.frm.amount.value+"~"+document.frm.request_type.value;
			najax(pass);
		}else{
			alert(errorstr);
		}

}

function deposit(){
	 var errorstr="";
	 var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
	 var intRegex = /^(0|[1-9][0-9]*)$/;	 
	 			if(document.frm.request_type.value=="0"){
					errorstr+="Request Type is required\n";
				}
				//validate Rewards
				if(document.frm.amount.value==""){
					errorstr+="Deposit is required\n";
					document.frm.amount.value=0.2;
				}
				//validate GC
				if(parseFloat(document.frm.amount.value)==0){
					errorstr+="You must request an Deposit Amount\n";	
				}
				if(document.frm.password.value==""){
					errorstr+="Account Password is required\n";		
				}		
				if(document.frm.allow.value=="NO"){
					errorstr+="Invalid Deposit Amount\n";		
				}				
						
				if(errorstr==""){
					if(confirm("Continue Deposit?")){ 
						document.frm.allow.value="YES";
						document.frm.action="account_deposit_proc.php";
						document.frm.submit();
					}
				}else{
					alert(errorstr);
				}

}

function validate_multi(){
	 var errorstr="";
	 var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
	 var intRegex = /^(0|[1-9][0-9]*)$/;	 


	 			if(document.frm.cnt.value==""){
					errorstr+="Number of Codes is required\n";
				}
				if(!floatRegex.test(document.frm.cnt.value) && !intRegex.test(document.frm.cnt.value) ){
					errorstr += "Please specify valid Number of Codes\n";
				}	
				//validate GC
				if(document.frm.id_code.value==""){
					errorstr+="Account Password is required\n";		
				}					
				if(document.frm.password.value==""){
					errorstr+="Account Password is required\n";		
				}	
				//alert(document.frm.cnt.value+''+document.frm.curcnt.value);
				if(parseFloat(document.frm.cnt.value)>parseFloat(document.frm.curcnt.value)){
					errorstr += "You have entered greater than your current codes\n";
				}				

	            var response = grecaptcha.getResponse();        
	            if(response.length == 0){
	                errorstr+="Google Captcha must be verified";
	            } 				
				if(errorstr==""){
					if(confirm("Continue Transfer Codes?")){ 
						document.frm.allow.value="YES";
						document.frm.action="res_transfer_multi_proc.php";
						document.frm.submit();
					}
				}else{
					alert(errorstr);
				}

}