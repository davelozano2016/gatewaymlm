<?php
include("account_session.php"); 
if($rst['status']=="Inactive"){
  header("Location: activate");    
}
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    <?php echo $site_title; ?>
  </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->

  <!--  -->
  <!--     Fonts and icons     -->
  <link href="css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->

  <!-- End Google Tag Manager -->
</head>
<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
      <?php
      include('account_menu.php'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-12">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">&nbsp;</h5>                   
                  </div>
                  <div class="col-sm-6">
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                      <label class="btn btn-sm btn-info btn-simple active" id="0">
                        <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='myaccounts'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">My Accounts</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-align-left-2"></i>
                        </span>
                      </label>
                      <!--<label class="btn btn-sm btn-info btn-simple" id="1">-->
                      <!--  <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='subaccount'">-->
                      <!--  <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Add Account</span>-->
                      <!--  <span class="d-block d-sm-none">-->
                      <!--    <i class="tim-icons icon-simple-add"></i>-->
                      <!--  </span>-->
                      <!--</label></a>-->
                      <label class="btn btn-sm btn-primary btn-simple" id="2">
                        <input type="radio" class="d-none" name="options" onchange="javascript:window.location='upgrade'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" >Upgrade Account</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-minimal-up"></i>
                        </span>
                      </label>

                </div>
              </div>
              
            </div>
          </div>
        </div>
        <br>
      <div class="content">
        <div class="row">
          <div class="col-12">
            <div class="card card-chart">
              <div class="card-header ">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">Dashboard -> My Accounts -> <a href="myaccounts">My Account List</a></h5>
                    <h2 class="card-title">My Account List</h2>
                  </div>
                  
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        

       
    
        <div class="row">

          <div class="col-lg-12 col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title"> Account List</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class="text-primary">
                      <tr>
                        <th></th>
                        <th>User ID</th>
                        <th>Username</th>
                        <th>Package</th>
                        <!-- <th>Rank</th> -->
                        <th>Last Login</th>
                        <th>Current Wallet</th>
                        <th>Entry Type</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                            $strSQL="SELECT id_number,status_type,id_code,group_code,username,last_date,ip_address,total_income,member_type,rank FROM tbl_members WHERE group_code='" .$_SESSION['shot_group'] ."'";
                            //echo $strSQL;
                            $rsg=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                            $rsgt=mysql_fetch_assoc($rsg);
                            if(mysql_num_rows($rsg)>0){
                              do{ 
                               
                       ?>                                    
                           <tr>
                           <td style="text-align:center">
                              <?php if($_SESSION['shot_user_id']!=$rsgt['id_number']){
                                echo "<a href=\"switch/". $mcrypt->encrypt($rsgt['id_number']) ."\" class=\"btn btn-sm btn-fill btn-info\">Login</a>";
                              } else {
                                echo "Currently Logged";
                              } ?>
                            </td>     
                           <td><?php echo $rsgt['id_number']; ?></td>
                           <td><?php echo $rsgt['id_code']; ?></td>
                            <td ><?php echo loadPosition($rsgt['member_type']); ?></td>
                            <!-- <td ><?php echo loadRank($rsgt['rank']); ?></td> -->
                            <td ><?php echo $rsgt['last_date']; ?></td>
                            <td ><?php echo number_format($rsgt['total_income'] * $_SESSION['shot_rate'],2); ?> <?php echo $_SESSION['shot_cur']; ?></td>
                            <td><?php echo $rsgt['status_type']?></td>

                            <!-- <td ><?php echo number_format($rsbt['amount'] * $_SESSION['shot_rate'],2); ?> <?php echo $_SESSION['shot_cur']; ?></td>   -->
                              
                       </tr>
                     <?php
                                }while($rsgt=mysql_fetch_assoc($rsg));
                              }
                          
                       ?>                                                       
                                                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

          </div>

        </div>
 

      <?php include('account_footer.php'); ?>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Chart JS -->
  <script src="assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script><!-- Black Dashboard DEMO methods, don't include it in your project! -->
 
  
  <script>
    $(document).ready(function() {
      $('body').addClass('white-content');
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
 
</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
</html>