<?php
require_once('account_session.php');
$cap="Transfer Product Pin History";
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">
<html lang="en">

<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
<link rel="icon" type="image/png" href="assets/img/favicon.png">
<title>
<?php echo $site_title; ?>
</title>
<!-- Extra details for Live View on GitHub Pages -->
<!-- Canonical SEO -->

<!--  -->
<!--     Fonts and icons     -->
<link href="css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="assets/css/nucleo-icons.css" rel="stylesheet" />
<!-- CSS Files -->
<link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="assets/demo/demo.css" rel="stylesheet" />
<!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
</head>

<body class="">
<div class="wrapper">
<?php include('account_menu.php'); ?>
<!-- END HEADER MOBILE-->

<div class="content">
<div class="row">
<?php include('account_top.php'); ?>

<div class="col-12">
<div class="row">
<div class="col-sm-6 text-left">
<h5 class="card-category">&nbsp;</h5>
</div>
<div class="col-sm-12" style="margin:auto">
<div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
<label class="btn btn-sm btn-info btn-simple " id="2">
<input type="radio" class="d-none" name="options"
onchange="javascript:window.location='productcodes'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">My Product
Pins</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-minimal-up"></i>
</span>
</label>
<label class="btn btn-sm btn-info btn-simple " id="0">
<input type="radio" class="d-none d-sm-none" name="options"
onchange="javascript:window.location='productadd'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Encode
Product</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-align-left-2"></i>
</span>
</label>
<label class="btn btn-sm btn-info btn-simple " id="1">
<input type="radio" class="d-none d-sm-none" name="options"
onchange="javascript:window.location='productencode'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Product Cart</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-simple-add"></i>
</span>
</label></a>
<label class="btn btn-sm btn-info btn-simple " id="2">
<input type="radio" class="d-none" name="options"
onchange="javascript:window.location='producthistory'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Product
Pins History</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-minimal-up"></i>
</span>
</label>

<label class="btn btn-sm btn-info btn-simple " id="2">
<input type="radio" class="d-none" name="options"
onchange="javascript:window.location='productmulti'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Transfer
Pins</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-minimal-up"></i>
</span>
</label>

<label class="btn btn-sm btn-info btn-simple active" id="2">
<input type="radio" class="d-none" name="options"
onchange="javascript:window.location='productlogs'">
<span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Transfer
Pins History</span>
<span class="d-block d-sm-none">
<i class="tim-icons icon-minimal-up"></i>
</span>
</label>
</div>
</div>
</div>

</div><BR><BR><BR><BR>
</div>
<?php
if(isset($_GET['m'])){
echo $mcrypt->decrypt($_GET['m']);
}
if(isset($_GET['e'])){
echo $mcrypt->decrypt($_GET['e']);
}

?>


<div class="row">
<div class="col-12">
<div class="card card-chart">
<div class="card-header ">
<div class="row">
<div class="col-sm-12 text-left">
<h5 class="card-category">Dashboard -> Encode Product -> <a
href="productlogs">Transfer Pins History</a></h5>
<h2 class="card-title">Transfer Pins History</h2>
</div>

</div>
</div>

</div>
</div>
</div>

<div class="content">



<div class="row">
<div class="col-md-12">
<div class="card card-body">
<div class="table-responsive">
<table class="table tablesorter" id="">
<thead>
<tr>
<th>Date Transfer</th>
<th>Transferred to</th>
<th>Activation Code</th>
<th>PIN</th>
<th>Product Name</th>
</tr>
</thead>
<tbody>
<?php                       
$strSQL="SELECT * FROM tbl_product_logs WHERE  source_id like '%". $_SESSION['shot_user_id']."%' order by date_added DESC";
$rsb=mysql_query($strSQL) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
$rsbt_count=mysql_num_rows($rsb);

if(mysql_num_rows($rsb)>0){

do{
    $name="";
    if($rsbt['recepient_id']!=""){
        $strSQL="SELECT firstname,lastname FROM tbl_members WHERE id_number=". $rsbt['recepient_id'] ."";
        $rsm=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
        $rsmt=mysql_fetch_assoc($rsm);
        $name=$rsmt['firstname'] . " ".$rsmt['lastname'];
    }
?>
<tr>
    <td><?php echo $rsbt['date_added']; ?></td>
    <td><?php echo $rsbt['recepient_id'] ." | " .$name; ?></td>
    <td><?php echo $rsbt['registration_code']; ?></td>
    <td><?php echo $rsbt['password_code']; ?></td>
    <td><?php 
                                

$strSQL="SELECT pname FROM tbl_products WHERE pctr='".$rsbt['registration_type'] ."'";
$rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsdt=mysql_fetch_assoc($rsd);

echo $rsdt['pname'];
                    
?></td>

</tr>
<?php
}while($rsbt=mysql_fetch_assoc($rsb));
}
?>

</tbody>
</table>
</div>
</div>
<?php include('account_footer.php'); ?>
</div>

</div>
</div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->


<!--   Core JS Files   -->
<!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/popper.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!-- Chart JS -->
<script src="assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->

<!-- Main JS-->
<script src="js/main.js"></script>
<script language="javascript" src="scripts/formvalidations.js"></script>
<script language="javascript" src="scripts/showid.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
$(document).ready(function() {
$('body').addClass('white-content');
// Javascript method's body can be found in assets/js/demos.js
demo.initDashboardPageCharts();

});
</script>
</body>

</html>
<!-- end document-->