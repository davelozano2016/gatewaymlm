<?php
require_once('scripts/mcrypt.php');
$mcrypt=new MCrypt();
session_start();
$country=$_SESSION['country'];
session_unset();
session_destroy();
session_start();
$_SESSION['country']=$country;
header("Location: ../members/logmessage/".$mcrypt->encrypt("Account successfully logout"));
?>