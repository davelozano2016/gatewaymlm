<?php
include("account_session.php"); 
if($rst['status']=="Inactive"){
  header("Location: activate");    
}
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    <?php echo $site_title; ?>
  </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->

  <!--  -->
  <!--     Fonts and icons     -->
  <link href="css/all.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="assets/css/black-dashboard.mine209.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <!-- Google Tag Manager -->

  <!-- End Google Tag Manager -->
</head>
<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
      <?php
      include('account_menu.php'); ?>
      <!-- End Navbar -->
      <div class="content">
                <div class="row">
          <div class="col-12">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">&nbsp;</h5>                   
                  </div>
                  <div class="col-sm-6">
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                      <label class="btn btn-sm btn-danger btn-simple" id="0">
                        <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='bingraph'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Graphical</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-single-02"></i>
                        </span>
                      </label>
                      <label class="btn btn-sm btn-danger btn-simple" id="1">
                        <input type="radio" class="d-none d-sm-none" name="options" onchange="javascript:window.location='binlist'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Listing</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-link-72"></i>
                        </span>
                      </label></a>
                      <label class="btn btn-sm btn-danger btn-simple" id="2">
                        <input type="radio" class="d-none" name="options" onchange="javascript:window.location='unilevel'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" >Unilevel</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-vector"></i>
                        </span>
                      </label>
                      <!-- <label class="btn btn-sm btn-danger btn-simple active" id="3">
                        <input type="radio" class="d-none" name="options" onchange="javascript:window.location='inactive'">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block" >Invitation List</span>
                        <span class="d-block d-sm-none">
                          <i class="tim-icons icon-trophy"></i>
                        </span>
                      </label>                      -->
                    </div>
              </div>              
            </div>
          </div>
        </div>
        <Br>
        <div class="row">
          <div class="col-12">
            <div class="card card-chart">
              <div class="card-header ">
                <div class="row">
                  <div class="col-sm-6 text-left">
                    <h5 class="card-category">Dashboard -> Genealogy -> <a href="inactive">Invitation Listing</a></h5>
                    <h2 class="card-title">Invitation Listing</h2>
                  </div>
                  
                </div>
              </div>
              
            </div>
          </div>
        </div>
       
    
        <div class="row">

          <div class="col-lg-12 col-md-12">
            <div class="card ">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                  <thead>
                                                              <tr>
                                                                  <th>Date</th>
                                                                  <th>User ID</th>
                                                                  <th>Name</th>                                                                                                                               
                                                                  <th>Email</th>
                                                                  
                                                              </tr>    
                                           </thead>
                                           <tbody>
                                                <?php
                                    $strSQL="SELECT date_registered,id_code,id_number,firstname,lastname,mobile,email FROM tbl_members  WHERE sponsor_code='" . $rst['id_number'] ."' AND status='Inactive'";
                                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
                                    $rsbt=mysql_fetch_assoc($rsb);  
                                     if(mysql_num_rows($rsb)>0){
                                    do{
                                      
                                   ?>                                 
                                                                       
                                       <tr>
                                       <td><?php echo $rsbt['date_registered']; ?></td>
                                       <td><?php echo $rsbt['id_code'] ."/".$rsbt['id_number']; ?></td>
                                       <td><?php echo $rsbt['firstname'] . " " . $rsbt['lastname']; ?></td>                   
                                       <td><?php echo $rsbt['email']; ?></td>      
                                      
                                   </tr>
                                   <?php
                                    }while($rsbt=mysql_fetch_assoc($rsb));
                                  }
                                   ?>                                                    
                                                                        
                                           </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

          </div>

        </div>
 

      <?php include('account_footer.php'); ?>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Chart JS -->
  <script src="assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/black-dashboard.mine209.js?v=1.0.0"></script><!-- Black Dashboard DEMO methods, don't include it in your project! -->
  <!--<script src="assets/demo/demo.js"></script>-->
  
  
  <script>
    $(document).ready(function() {
      $('body').addClass('white-content');
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
 
</body>


<!-- Mirrored from demos.creative-tim.com/black-dashboard/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 25 Sep 2020 08:55:14 GMT -->
</html>