<html>
<head>
<title>POP before SMTP Test</title>
</head>

<body>

<pre>
<?php
	require '../redd/phpmailer.lang-en.php';
	require '../redd/class.phpmailer.php';
	require '../redd/class.pop3.php';
	require '../redd/class.smtp.php';
	$pop = new POP3();
	$pop->Authorise('mail.profcomm.com', 110, 30, 'redd@profcomm.com', 'janered', 1);
	
	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->SMTPDebug = 2;
	$mail->IsHTML(false);

	$mail->Host     = 'mail.profcomm.com';

	$mail->From     = 'redd@profcomm.com';
	$mail->FromName = 'Example Mailer';

	$mail->Subject  =  'My subject';
	$mail->Body     =  'Hello world';
	$mail->AddAddress('angelknight_01@yahoo.com', 'Richard Davey');
	
	if (!$mail->Send())
	{
		echo $mail->ErrorInfo;
	}
?>
</pre>

</body>
</html>
