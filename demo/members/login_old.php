<?php
session_start();
require_once('conn/connBS.php');
require("js/myfunctions.php");
require_once('js/mcrypt.php');
$mcrypt=new MCrypt();
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">
<head>
<title><?php echo $site_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Quick Register Form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="favicon.png">
</head>
<body>
<div class="signupform">
<h1>Members Login</h1>
	<div class="container">
		
		<div class="agile_info">
			<div class="w3_info">
				<h2>Login Account</h2>
				<?php

                          if(isset($_GET['m'])){ 
                            echo "<h5 class=\"title\">".$mcrypt->decrypt($_GET['m']) ."<br><br></h5>"; 
                          }

                          ?>       
						<form method="post" id="frm" name="frm">
						<div class="full margin">
							<label>UserName</label>
							<div class="input-group">
								<span><i class="fa fa-user" aria-hidden="true"></i></span>
								<input type="text"  style="text-transform:lowercase !important" name="username" required="" maxlength="20"> 
							</div>
						</div>


						<div class="full margin">
							<label>Password</label>
							<div class="input-group">
								<span><i class="fa fa-lock" aria-hidden="true"></i></span>
								<input type="Password" name="password" required="" maxlength="20">
							</div>
						</div>

						<div class="clear"></div>     
							<button class="btn btn-info btn-block" type="button" onclick="javascript: validate_login();">Login <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button >                
					</form>
			</div>
			<div class="w3l_form">
				<div class="left_grid_info">
				    <p align="center"><img src="images/logo.png" alt=""></p>
					<h3>Don't Have Account?</h3>					
					<a href="register.php" class="btn">Register Now! <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					<?php
					if($_SESSION['shot_user_id']!=""){
					?>
					<a href="../dashboard/summary" class="btn"><?php echo $_SESSION['shot_username']; ?> Virtual Office <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					<?php } ?>
				</div>
			</div>
			<div class="clear"></div>
			</div>
			
		</div>
		<div class="footer">

 <p>&copy; <?php echo $site_footer; ?> <a href="../index.php" target="blank">BACK TO HOMEPAGE</a></p>
 </div>
	</div>
	</body>
	<script language="JavaScript" type="text/javascript">
    function validate_login(){
    var errorstr="";     
            if(document.frm.username.value ==""){
            errorstr+='Username is a required field!\n';
            } 
            if(document.frm.password.value ==""){
            errorstr+='Password is a required field!\n';
            }
            //var response = grecaptcha.getResponse();
        
            //if(response.length == 0){
            //    errorstr+="Google Captcha must be verified";
            //} 
            if(errorstr==""){
                //login("login~"+document.frm.username.value+"~"+document.frm.password.value+"~"+ document.frm.password.value); 
                document.frm.action="login_process.php";
                document.frm.submit();
            }else{
                alert(errorstr);
               }
            }
    </script>
</html>