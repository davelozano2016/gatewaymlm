<?php
session_start();
require_once('conn/connBS.php');
require("js/myfunctions.php");
require_once('js/mcrypt.php');
$mcrypt=new MCrypt();
?>
<!DOCTYPE html>
<base href="<?php echo $base_url;?>">

<head>
    <title><?php echo $site_title; ?></title>
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="assets/css/light.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
</head>

<body class="fixed-header menu-pin">


    <div class="login-wrapper ">
        <!-- START Login Background Pic Wrapper-->
        <div class="bg-pic">
            <!-- START Background Pic-->
            <img src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg"
                data-src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg"
                data-src-retina="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt=""
                class="lazy">


            <!-- END Background Pic-->
            <!-- START Background Caption-->
            <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                <p class="small">
                    &copy; <?php echo $site_footer; ?>

                </p>
            </div>
            <!-- END Background Caption-->
        </div>
        <!-- END Login Background Pic Wrapper-->
        <!-- START Login Right Container-->
        <div class="login-container bg-white">
            <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-10 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
                <div class="row justify-content-center">

                    <div class="col-md-10 center">
                        <center><img style="width:100%" src="images/logo.png" alt=""></center>
                    </div>
                </div>
                <?php

                    if(isset($_GET['m'])){ 
                    echo "<div class=\"mt-2 text-center alert alert-success \">".$mcrypt->decrypt($_GET['m']) ."</div>"; 
                    }

                ?>

                <p class="mt-3 text-uppercase text-center">Sign into your account</p>
                <!-- START Login Form -->
                <form method="post" id="frm" name="frm" class="p-t-15" role="form">
                    <!-- START Form Control-->


                    <div class="form-group form-group-default">
                        <label>Username</label>
                        <div class="controls">
                            <input type="text" name="username" class="form-control">
                        </div>
                    </div>
                    <!-- END Form Control-->
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>Password</label>
                        <div class="controls">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <!-- START Form Control-->
                    <!-- END Form Control-->

                    <div class="btn-group">
                        <button class="btn btn-primary  text-uppercase " style="border-radius:0px; type=" button"
                            onclick="javascript: validate_login();">Login</button>
                        <?php
					if($_SESSION['shot_user_id']!=""){
					?>
                        <a href="../dashboard/summary" style="border-radius:0px;"
                            class="text-uppercase btn btn-success ">Back To Virtual
                            Office</a>
                        <?php } ?>

                    </div>


                    <div class="row">
                        <div class="col-md-12 mt-2 mb-2">
                            Don't Have Account?
                            <a href="register" style="border-radius:0px;">Register Now</a>
                        </div>

                    </div>
                </form>


                <!--END Login Form-->

            </div>
        </div>
        <!-- END Login Right Container-->
    </div>
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
    function validate_login() {
        var errorstr = "";
        if (document.frm.username.value == "") {
            errorstr += 'Username is a required field!\n';
        }
        if (document.frm.password.value == "") {
            errorstr += 'Password is a required field!\n';
        }
        //var response = grecaptcha.getResponse();

        //if(response.length == 0){
        //    errorstr+="Google Captcha must be verified";
        //} 
        if (errorstr == "") {
            //login("login~"+document.frm.username.value+"~"+document.frm.password.value+"~"+ document.frm.password.value); 
            document.frm.action = "login_process.php";
            document.frm.submit();
        } else {
            alert(errorstr);
        }
    }
    </script>
</body>

</html>