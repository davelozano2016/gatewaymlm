<?php
include('admin_session.php');
$head="Paid Pins History";
if($rst['codes']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
$cond="";

//echo $strSQL;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <section id="container">
        <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <!--header start-->
        <?php
	  include('admin_top.php');
	  ?>
        <!--header end-->

        <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <?php
	  include('admin_menu.php');
	  ?>
        <!--sidebar end-->

        <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <h3><?php echo $head; ?></h3>

                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="content-panel">
                                    <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>
                                    <form class="form-horizontal" method="POST">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="email">Date Start:</label>
                                                <div class="col-sm-8">
                                                    <input type="date" name="date_start"
                                                        value="<?=$_POST['date_start']?>" class="form-control"
                                                        id="email" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="pwd">Date End:</label>
                                                <div class="col-sm-8">
                                                    <input type="date" name="date_end" value="<?=$_POST['date_end']?>"
                                                        class="form-control" id="email" placeholder="Enter email">
                                                </div>
                                            </div>

                                            <div class="form-group pull-right">
                                                <div class="col-sm-8">
                                                    <button type="submit" class="btn ">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                    if($_POST['date_start']!="" && $_POST['date_end']){
                        $start = date('Y-m-d',strtotime($_POST['date_start'])).' 00:00:00';
                        $end = date('Y-m-d',strtotime($_POST['date_end'])).' 23:59:59';
                        $cond .="WHERE date_process BETWEEN '$start' AND '$end'";
                    }

                    $strSQL="SELECT * FROM tbl_control_panel";
                    $rsc=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                    $rsct=mysql_fetch_assoc($rsc);

                    $strSQL="SELECT  * FROM tbl_entry_orders $cond order by reservation_ctr DESC";
                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                    $rsbt=mysql_fetch_assoc($rsb);
                    $rsbt_count=mysql_num_rows($rsb);  
                    ?>

                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>OR No</th>
                                                <th>CustNo</th>
                                                <th>User ID</th>
                                                <th>Name
                                </div>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Admin</th>
                                <th>Stockist</th>
                                <th>Order No</th>
                                <th>Action</th>

                                </tr>
                                </thead>
                                <?php
								  			
											if(mysql_num_rows($rsb)>0){
											
											do{												
                                               												
										 ?>

                                <?php
                                $total_count += $rsbt['item_count'];
                                $total_amount += $rsbt['total_amount']
                                ?>
                                <tr>
                                    <td><?php echo $rsbt['date_process']; ?></td>
                                    <td><?php echo $rsbt['orno']; ?></td>
                                    <td><?php echo $rsbt['username']; ?></td>
                                    <td><?php echo $rsbt['purchase_code']; ?></td>
                                    <td><?php echo $rsbt['fname']; ?></td>
                                    <td><?php echo number_format($rsbt['item_count'],0); ?></td>
                                    <td>₱<?php echo number_format($rsbt['total_amount'],2); ?></td>
                                    <td><?php echo $rsbt['approve_user']; ?></td>
                                    <td><?php echo $rsbt['center']; ?></td>
                                    <td><?php echo $rsbt['order_type']; ?></td>
                                    <td><?php 
										   			echo "<a href='admin_entry_orders_view.php?r=".$mcrypt->encrypt($rsbt['reservation_ctr'])."'>View</a> | <a href=\"javascript: ask('".$mcrypt->encrypt($rsbt['reservation_ctr'])."');\">Cancel</a> | <a href=\"javascript: loadreceipt('".$mcrypt->encrypt($rsbt['reservation_ctr'])."');\">Print</a>"; 
												?>
                                    </td>
                                </tr>
                                <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										 ?>
                                </tbody>
                                <tfoot>
                                    <td colspan=5 class="text-right"></td>
                                    <td><?=$total_count?></td>
                                    <td colspan=5>₱<?=number_format($total_amount,2)?></td>
                                </tfoot>

                                </table>
                                &nbsp;&nbsp;&nbsp;

                                <?php
											if($cond==""){
 												include ('admin_paging.php');
											}
										?>
                            </div>
                            <! --/content-panel -->
                        </div><!-- /col-md-12 -->
                    </div><!-- /row -->

                    <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->

                </div>
                <! --/row -->
            </section>
        </section>

        <!--main content end-->
        <!--footer start-->
        <?php  include('admin_footer.php'); ?>
        <!--footer end-->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>
    <script language="javascript" type="text/javascript">
    function ask(ctr) {
        if (confirm("Continue to Cancel Order?")) {
            window.location = "admin_entry_order_cancel.php?r=" + ctr;
        }
    }

    function NewWindow(mypage, myname, w, h, scroll) {
        var winl = (screen.width - w) / 2;
        var wint = (screen.height - h) / 2;
        winprops = 'height=' + h + ',width=' + w + ',top=' + wint + ',left=' + winl + ',scrollbars=' + scroll +
            ',resizable=no,titlebar=no,toolbar=no,status=no,menubar=no'
        win = window.open(mypage, myname, winprops)
        if (parseInt(navigator.appVersion) >= 4) {
            win.window.focus();
        }
    }

    function loadreceipt(ctr) {
        var dir = 'admin_entry_order_receipt.php?reserve=' + ctr;
        NewWindow(dir, 'view', 1070, 600, 'no');
    }
    </script>





</body>

</html>