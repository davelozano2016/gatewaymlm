<?php
require_once('admin_session.php');
$head="Edit Administrator User";
if($rst['users']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
function sclear($str){
  return mysql_escape_string($str);
}
if(isset($_POST['firstname'])){

	$updateSQL="UPDATE tbl_users SET
				firstname='" .sclear($_POST['firstname']) ."',
				lastname='" .sclear($_POST['lastname']). "',
				username='" . sclear($_POST['username']) ."',
				password=ENCODE('" . sclear($_POST['password']). "','$encrypt_password'),
				mem=" . $_POST['mem'] . ",
				codes=" . $_POST['codes'] . ",
				bonus=". $_POST['bonus'].",
				gen=" .$_POST['gen'].",
				encash=" .$_POST['encash'].",
				promain=" .$_POST['promain'].",
				propins=" .$_POST['propins'].",
				cp=" .$_POST['cp'].",
				sales=" .$_POST['sales'].",
				uni=" .$_POST['uni'].",				
				reports=" .$_POST['reports'].",				
				web=" .$_POST['web'].",								
				users=" .$_POST['users']."
				WHERE user_ctr=". $_POST['user_ctr'] ."";
	mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);
	header("Location: admin_users.php?m=" .$mcrypt->encrypt("System Message: User Successfully Updated"));
}
if(isset($_GET['r'])){
	$strSQL="SELECT *,DECODE(password,'$encrypt_password') as dpassword FROM tbl_users WHERE user_ctr=".$mcrypt->decrypt($_GET['r']);
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{
		header("Location: admin_main.php");

}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
						echo $error_message;
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="" enctype="multipart/form-data">
							                 
                          
							                      							                          
 						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">First Name</label>
                              <div class="col-sm-6">
                                    <input name="firstname" type="text" class="form-control"  id="firstname" size="80" value="<?php echo $rsbt['firstname']; ?>" /> 

                              </div>
                          </div>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Last Name</label>
                              <div class="col-sm-6">
                                    <input name="lastname" type="text" class="form-control"  id="lastname" size="80" value="<?php echo $rsbt['lastname']; ?>" /> 

                              </div>
                          </div>    
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Username</label>
                              <div class="col-sm-6">
                                    <input name="username" type="text" class="form-control"  id="username" size="80" value="<?php echo $rsbt['username']; ?>" /> 

                              </div>
                          </div>  
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Password</label>
                              <div class="col-sm-6">
                                    <input name="password" type="text" class="form-control"  id="password" size="80"  value="<?php echo $rsbt['dpassword']; ?>" /> 

                              </div>
                          </div>
							
                                                 
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Membership Accounts</label>
                              <div class="col-sm-6">
 								<select name="mem" class="form-control" id="mem">								  
                                    <option value="1" <?php if($rsbt['mem']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['mem']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>       
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Membership Pins</label>
                              <div class="col-sm-6">
 								<select name="codes" class="form-control" id="codes">								  
                                    <option value="1" <?php if($rsbt['codes']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['codes']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>       
                                
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Bonus Accounts</label>
                              <div class="col-sm-6">
 								<select name="bonus" class="form-control" id="bonus">								  
                                    <option value="1" <?php if($rsbt['bonus']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['bonus']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>   
 							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Genealogy</label>
                              <div class="col-sm-6">
 								<select name="gen" class="form-control" id="gen">								  
                                    <option value="1" <?php if($rsbt['gen']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['gen']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Encashment</label>
                              <div class="col-sm-6">
 								<select name="encash" class="form-control" id="encash">								  
                                    <option value="1" <?php if($rsbt['encash']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['encash']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>        
                         <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Maintenance</label>
                              <div class="col-sm-6">
 								<select name="promain" class="form-control" id="promain">								  
                                    <option value="1" <?php if($rsbt['promain']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['promain']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>   
                         <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Pins</label>
                              <div class="col-sm-6">
 								<select name="propins" class="form-control" id="propins">								  
                                    <option value="1" <?php if($rsbt['propins']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['propins']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>   
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Sales Summary</label>
                              <div class="col-sm-6">
 								<select name="sales" class="form-control" id="sales">								  
                                    <option value="1" <?php if($rsbt['sales']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['sales']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>       
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Repeat Sales Summary</label>
                              <div class="col-sm-6">
 								<select name="uni" class="form-control" id="uni">								  
                                    <option value="1" <?php if($rsbt['uni']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['uni']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>       
                          
 							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">System Settings</label>
                              <div class="col-sm-6">
 								<select name="cp" class="form-control" id="cp">								  
                                    <option value="1" <?php if($rsbt['cp']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['cp']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>       
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">User Settings</label>
                              <div class="col-sm-6">
 								<select name="users" class="form-control" id="users">								  
                                    <option value="1" <?php if($rsbt['users']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['users']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>   
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Reports</label>
                              <div class="col-sm-6">
 								<select name="reports" class="form-control" id="reports">								  
                                    <option value="1" <?php if($rsbt['reports']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['reports']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>                                   
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Website Management</label>
                              <div class="col-sm-6">
 								<select name="web" class="form-control" id="web">								  
                                    <option value="1" <?php if($rsbt['web']==1) { echo "selected"; }  ?>>Show Feature</option>
                                    <option value="0" <?php if($rsbt['web']==0) { echo "selected"; }  ?>>Hide Feature</option>                                   
                                  </select>
                              </div>
                          </div>                                   
                          
                                                                                    
                                                                                                                                       
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Update User</a>
                               </div>
                          </div>
                          
								 <input name="allow_update" type="hidden" id="allow_update"/>                                                                                                  
								 <!-- <input name="uni" type="hidden" id="uni" value="0"/>                                                                                                                                    -->
								 <input name="user_ctr" type="hidden" id="user_ctr" value="<?php echo $rsbt['user_ctr']; ?>"/>                                                                                                  
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
<script language="javascript" type="text/javascript">
function validate(){
	var errorstr = "";
	errorstr += checkitem(document.frm.firstname,"First Name");	
	errorstr += checkitem(document.frm.lastname,"Last Name");	
	errorstr += checkitem(document.frm.username,"User Name");	
	errorstr += checkitem(document.frm.password,"Password");	
	
	if(errorstr==""){
			document.frm.allow_update.value="YES";
			document.frm.submit();
	}else{
		alert(errorstr);
	}
}
function checkitem(item, fdesc) { 
	var errorstr = "";
	if (item.value == "" || checkblanks(item.value) == true)  {
   		if (fdesc != "") { 
   			errorstr = "'" + fdesc + "' is required.\n";
		}
   }
   return errorstr;
 }

// following looks for all blanks in a string - typical of checking//
function checkblanks(item)  {
  var isblank = true;
  for (i = 0; i < item.length; i++) {
    if (item.charAt(i) != " ") {
      isblank = false;  }
  } 
  return isblank;
}

// check for valid emails....
function checkemail(item) {
  var goodmail = true;
  var addr = item.value;
  var invchar = " /:,;";
  var errorstr = "";
  var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
  var emailFilter=/^.+@.+\..{2,3}$/;
  for (i=0; i<invchar.length; i++)  {
	badchar = invchar.charAt(i);
	if (addr.indexOf(badchar,0) > -1)  {
   		goodmail = false;
	}
  }
  atpos = addr.indexOf("@",1);
  if (atpos == -1) {
	goodmail = false;
  }
  else  {
	perpos = addr.indexOf(".",atpos);
	if (perpos == -1)  {
		goodmail = false;
	} else if (perpos + 3 > addr.length)  {
		goodmail = false;
	} else if (addr.match(illegalChars)) {
		goodmail = false;
	} else if (!(emailFilter.test(addr))) { 
		goodmail = false;
    }
  }
  if (goodmail == false) {
    errorstr = "Email address is not valid.\n";
  }
  return errorstr;
}
</script>

	
  

  </body>
</html>
