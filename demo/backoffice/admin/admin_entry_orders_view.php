<?php
include('admin_session.php');
$head="Paid Pins Purchase Summary";
if($rst['codes']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_GET['r'])){
    $id=$mcrypt->decrypt($_GET['r']);
    $strSQL="SELECT * FROM tbl_entry_orders WHERE reservation_ctr=$id";
    $rsr=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
    $rsrt=mysql_fetch_assoc($rsr);

    $strSQL="SELECT * FROM tbl_entry_carts WHERE order_type='" . $rsrt['order_type']."'";
    $rse=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
    $rset=mysql_fetch_assoc($rse);
    $rset_count=mysql_num_rows($rse);      

    $username=$rsrt['username'];
    $name=$rsrt['fname'];

}else{
    header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission")); 
}

//echo $strSQL;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	<a href="admin_entry_orders.php">Back to Paid Pins History</a>
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>                          
                                <table width="50%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
<tr>
  <td>&nbsp;&nbsp;&nbsp;Customer Number:</td>
  <td><?php echo $username; ?></td>
  </tr> 
<tr>
  <td>&nbsp;&nbsp;&nbsp;User ID:</td>
  <td><?php echo $rsrt['purchase_code']; ?></td>
  </tr> 

<tr>
  <td>&nbsp;&nbsp;&nbsp;Name :</strong> </td>
  <td width="279"   ><?php echo $name; ?></td>
  </tr> 
<tr>
  <td>&nbsp;&nbsp;&nbsp;OR No :</strong> </td>
  <td><?php echo $rsrt['orno']; ?></td>
  </tr>   
<tr>
  <td>&nbsp;&nbsp;&nbsp;Order No :</strong> </td>
  <td><?php echo $rsrt['order_type']; ?></td>
  </tr> 
<tr>
  <td>&nbsp;&nbsp;&nbsp;Date Process:</strong> </td>
  <td><?php echo $rsrt['date_process']; ?></td>
  </tr>  

 <tr>
  <td>&nbsp;&nbsp;&nbsp;Total Entry Package </strong> </td>
  <td><?php echo number_format($rsrt['item_count'],0); ?></td>
  </tr>  
   <tr>
  <td>&nbsp;&nbsp;&nbsp;Total Price </strong> </td>
  <td><?php echo number_format($rsrt['total_amount'],2) ." " . $rsct['country_currency']; ?></td>
  </tr>  
   <tr>
  <td>&nbsp;&nbsp;&nbsp;IP Address </strong> </td>
  <td><?php echo $rsrt['ip_approve']; ?></td>
  </tr>  
   <tr>
  <td>&nbsp;&nbsp;&nbsp;Representative </strong> </td>
  <td><?php echo $rsrt['approve_user']; ?></td>
  </tr>  

  </table>

                           
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                          <th>Entry</th>           
                                          <th>Items</th>                        
                                          <th>Total</th>
                                        </tr>
                                    </thead>
		                          <?php
                                    if($rset_count>0){
                                                $count=1;
                                                do{
                                                    $items+=$rset['item_count'];
                                                    $price+=$rset['total'];
                                                    $ectr=$rset['ectr'];
                                                    $strSQL="SELECT * FROM tbl_entry WHERE ectr='$ectr'";
                                                    $rsp=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                                    $rspt=mysql_fetch_assoc($rsp);                  
                                    ?>                                   
									       <tr>
                                		      <td><?php echo loadPosition($rset['registration_type']) . " ". $rspt['title']; ?></td>
                                              <td><?php echo number_format($rset['item_count'],0); ?></td>              
                                              <td><?php echo number_format($rset['total'],2); ?></td>              
									     </tr>
									 <?php
											}while($rset=mysql_fetch_assoc($rse));
										}
										 ?>   
		                          </tbody>
                                 
		                      </table>
<?php
$strSQL="SELECT * FROM tbl_registration_codes WHERE code_no='".$rsrt['order_type']."'";
$rsp=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rspt=mysql_fetch_assoc($rsp);
$rspt_count=mysql_num_rows($rsp); 
?>		                      
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                          <th>Entry</th>           
                                          <th>Activation code</th>                        
                                          <th>Pin</th>
                                        </tr>
                                    </thead>
                                 <?php
                                    if($rspt_count>0){
                                                $count=1;
                                                do{
                                                    $ectr=$rspt['ectr'];
                                                    
                                                    $strSQL="SELECT * FROM tbl_entry WHERE ectr='$ectr'";
                                                    $rse=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                                    $rset=mysql_fetch_assoc($rse);                  
                                    ?>                                
                                           <tr>
                                              <td><?php echo loadPosition($rspt['registration_type']) . " ". $rset['title']; ?></td>
                                              <td><?php echo $rspt['registration_code']; ?></td>              
                                              <td><?php echo $rspt['password_code']; ?></td>              
                                         </tr>
                                     <?php
                                            }while($rspt=mysql_fetch_assoc($rsp));
                                        }
                                         ?>   
                                  </tbody>
                                 
                              </table>		                      
                                                           
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php  include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
