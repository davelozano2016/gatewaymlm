<?php
require_once('admin_session.php');
if($rst['reports']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_POST['UNI_YEAR'])){          
    $start=$_POST['UNI_YEAR'] ."-" .$_POST['UNI_MONTH'] ."-" . $_POST['UNI_DAY'];
    $end=$_POST['END_YEAR'] ."-" .$_POST['END_MONTH'] ."-" . $_POST['END_DAY'];  
    $mcond=" AND DATEDIFF(date_registered,'$start')>=0 AND DATEDIFF(date_registered,'$end')<=0";  
    $bcond=" AND DATEDIFF(date_added,'$start')>=0 AND DATEDIFF(date_added,'$end')<=0"; 
    $pcond=" AND DATEDIFF(date_used,'$start')>=0 AND DATEDIFF(date_used,'$end')<=0"; 
    $ccond=" AND DATEDIFF(date_created,'$start')>=0 AND DATEDIFF(date_created,'$end')<=0";     
    $econd=" AND DATEDIFF(date_requested,'$start')>=0 AND DATEDIFF(date_requested,'$end')<=0"; 
    $scond="WHERE DATEDIFF(date_process,'$start')>=0 AND DATEDIFF(date_process,'$end')<=0"; 
    $scond2="AND DATEDIFF(date_process,'$start')>=0 AND DATEDIFF(date_process,'$end')<=0"; 
}
//echo $start ." " .$end;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Company Payins vs. Payout</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
						if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>
<form name="frm" method="post">
                          <table width="80%" border="0" cellpadding="0" cellspacing="0"  >
      <!--DWLayoutTable-->
	  		<?php
		  	//filter
			$strSQL="SELECT  * FROM tbl_control_panel";
			$rsc=mysql_query($strSQL,$connBS) or die(mysql_error());
			$rsct=mysql_fetch_assoc($rsc);	
			
			?>
				<tr>
				<td>&nbsp;</td>
				<td width="50%">Date Started</td>
				<td  width="50%">
				  <select name="UNI_MONTH" class="formitems_drop" id="UNI_MONTH" >
                                    <?php 
                                    if($_POST['UNI_MONTH']){
                                    	$value=$_POST['UNI_MONTH'];
                                   	}else{
                                   		$value=date("m");
                                   	}
                                    
                                    ?>
                                    <?php  if($value!=0) 
                                    {           ?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="UNI_DAY" class="formitems_drop" id="UNI_DAY">
                                    <?php
                                    $endmonth =31;
									if($_POST['UNI_DAY']){
                                    	$value=$_POST['UNI_DAY'];
                                   	}else{
                                   		$value=date("d");
                                   	}                                    
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$value){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="UNI_YEAR" class="formitems_drop" id="UNI_YEAR" >
                                    <?php
									if($_POST['UNI_YEAR']){
                                    	$value=$_POST['UNI_YEAR'];
                                   	}else{
                                   		$value=date("Y");
                                   	}                                   
                                    for ($i=2017; $i<=date("Y")+1; $i++) {
                                    if($i==$value){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select></td>			
		      <td width="185">&nbsp;</td>
			</tr>
<tr>
				<td>&nbsp;</td>
				<td width="50%">Date End</td>
				<td  width="50%">
				  <select name="END_MONTH" class="formitems_drop" id="END_MONTH" >
                                    <?php 
                                   	if($_POST['END_MONTH']){
                                    	$value=$_POST['END_MONTH'];
                                   	}else{
                                   		$value=date("m");
                                   	}  
                                    ?>
                                    <?php  if($value!=0) 
                                    {           ?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="END_DAY" class="formitems_drop" id="END_DAY">
                                    <?php
                                    $endmonth = 31;
                                   	if($_POST['END_DAY']){
                                    	$value=$_POST['END_DAY'];
                                   	}else{
                                   		$value=date("d");
                                   	}                                     
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$value){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="END_YEAR" class="formitems_drop" id="END_YEAR" >
                                    <?php
                                   	if($_POST['END_YEAR']){
                                    	$value=$_POST['END_YEAR'];
                                   	}else{
                                   		$value=date("Y");
                                   	}                                       
                                    for ($i=2017; $i<=date("Y")+1; $i++) {
                                    if($i==$value){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select></td>			
		      <td width="185">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td width="50%">Filter</td>
				<td  width="50%"><br>
				 <a href="javascript: document.frm.submit();" class="btn btn-info btn-m">Generate Report</a> <a href="javascript: window.location='admin_main_stat.php'" class="btn btn-info btn-m">Reset</a></td>			
		      <td width="185">&nbsp;</td>
			</tr>
			<tr>
				<td ></td>
		        <td colspan="3" valign="top"><h4> Members Summary</h4> </td>
		      </tr>
		  <?php
        //   for($x=1;$x<=$TOTAL_ENTRIES;$x++){		  
			$x=1;
		  ?>    
            <tr>
				<td>&nbsp;</td>
				<td width="50%"><?php echo $rsct['NAME'.$x]; ?></td>
				<td  width="50%">
				  <span class="style26">
				  <?php
					$strSQL="SELECT COUNT(*) as TOTAL FROM tbl_members WHERE member_type='PLAN$x' and status='Active' $mcond" ;						
					$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsbt=mysql_fetch_assoc($rsb);
					echo number_format($rsbt['TOTAL'],0);
			  		?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td>Total Members Account</td>
				<td>
				  <span class="style26">
				  <?php
					$strSQL="SELECT COUNT(*) as TOTAL FROM tbl_members WHERE status='Active' $mcond" ;						
					$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsbt=mysql_fetch_assoc($rsb);
					echo number_format($rsbt['TOTAL'],0);
			  		?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>		
          
<tr>
				<td ></td>
		        <td colspan="3" valign="top"><h4> Total Paid Codes Summary</h4> </td>
		      </tr>						
		  <?php
  			$strSQL="SELECT * FROM tbl_entry order by registration_type" ;						
			$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
			$rsbt=mysql_fetch_assoc($rsb);
			$x=1;
			do{	  
		  ?>    
            <tr>
				<td>&nbsp;</td>
				<td width="50%">Paid <?php echo loadPosition($rsbt['registration_type']) ."-".$rsbt['title']; ?> Codes</td>
				<td  width="50%">
				  <span class="style26">
				  <?php

					if(empty($rsbt['ectr'])) {
						$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes_used WHERE free=0 $pcond" ;
					} else {
						$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes_used WHERE ectr=".$rsbt['ectr']." AND free=0 $pcond" ;
					}
						//echo $strSQL;						
					$rsf=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsft=mysql_fetch_assoc($rsf);					

					if(empty($rsbt['ectr'])) {
			        $strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes WHERE free=0 $ccond" ;
				} else {
			        $strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes WHERE ectr=".$rsbt['ectr']." AND free=0 $ccond" ;

					}
			          //echo $strSQL;           
			        $rsf2=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
			        $rsf2t=mysql_fetch_assoc($rsf2);

			        echo number_format($rsft['TOTAL']+$rsf2t['TOTAL'],0) ." - (".$rsft['TOTAL']."/".$rsf2t['TOTAL'].")";

          			$ENTRY=0;     
					for($x=1;$x<=6;$x++){
						if($rsbt['registration_type']=="PLAN".$x){
							$ENTRY=$rsct["ENTRY".$x];
						}
					} 
					echo $rsft['TOTAL'] * $ENTRY;                  
					$DG[$x-1]=$rsft['TOTAL'] * $ENTRY;
					$x++;
			  		?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>
			<?php }while($rsbt=mysql_fetch_assoc($rsb)); ?>   
               	<?php
               	for($x=1;$x<=count($DG);$x++){
               		//echo "REDD".$DG[$x-1];
               	}
               	?>
<tr>
				<td>&nbsp;</td>
				<td>Total Paid Codes Used/Unused</td>
				<td>
				  <span class="style26">
				  <?php
					$strSQL="SELECT COUNT(*) as TOTAL FROM tbl_registration_codes_used WHERE free=0 $pcond" ;						
					$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsbt=mysql_fetch_assoc($rsb);

		          $strSQL="SELECT COUNT(*) as TOTAL FROM tbl_registration_codes WHERE free=0 $ccond" ;           
		          $rsb2=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
		          $rsb2t=mysql_fetch_assoc($rsb2);    

					echo number_format($rsbt['TOTAL']+$rsb2t['TOTAL'],0) ." - (".$rsbt['TOTAL']."/".$rsb2t['TOTAL'].")";
			  		?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>
             
			<tr>
				<td ></td>
		        <td colspan="3" valign="top"><br><h4> Members Commission Summary</h4> </td>
		      </tr>
			
			<tr>
				<td>&nbsp;</td>
				<td >Direct Sales Commission</td>
				<td >
				<?php				
				$strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like 'DSI%' $bcond";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				$debit+=$rsbt['D'];  
				echo number_format($rsbt['D'],2); 
				?></td>			
		      <td width="185">&nbsp;</td>
			</tr>
		
            
            <tr>
                <td>&nbsp;</td>
                <td >Sales Match Commission</td>
                <td >
                <?php               
                $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like 'PAIR%' $bcond";
                $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                $rsbt=mysql_fetch_assoc($rsb);  
                $debit+=$rsbt['D'];  
                echo number_format($rsbt['D'],2); 
                ?></td>         
              <td width="185">&nbsp;</td>
            </tr>
			<tr>
				<td>&nbsp;</td>
				<td >Unilevel Commission</td>
				<td >
				<?php				
				$strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like 'LEADER%' $bcond";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				$debit+=$rsbt['D'];  
				echo number_format($rsbt['D'],2); 
				?></td>			
		      <td width="185">&nbsp;</td>
			</tr>	 
			<tr>
				<td>&nbsp;</td>
				<td >Sponsor Support Commission</td>
				<td >
				<?php				
				$strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like 'DOWN%' $bcond";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				$debit+=$rsbt['D'];  
				echo number_format($rsbt['D'],2); 
				?></td>			
		      <td width="185">&nbsp;</td>
			</tr>	
			<tr>
				<td>&nbsp;</td>
				<td >Unilevel Commission</td>
				<td >
				<?php				
				$strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like 'REPEAT%' $bcond";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				$debit+=$rsbt['D'];  
				echo number_format($rsbt['D'],2); 
				?></td>			
		      <td width="185">&nbsp;</td>
			</tr>					           
			<tr>
                <td>&nbsp;</td>
                <td >Gift Certificate</td>
                <td >
                <?php               
                $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses_gc WHERE bonus_type like 'GC%' $bcond";
                $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                $rsbt=mysql_fetch_assoc($rsb);  
                $debit+=$rsbt['D'];  
                echo number_format($rsbt['D'],2); 
                ?></td>         
              <td width="185">&nbsp;</td>
            </tr>            			
           
             
            
                 
			<tr>
				<td>&nbsp;</td>
				<td >  Total Members eCash </td>
				<td >
				  <span class="style30">
				  <?php				  
				echo number_format($debit,2); 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>
		
													
			<tr>
				<td ></td>
		        <td colspan="3" valign="top"><br><h4> Company Payables and Encashment Summary </h4></td>
		      </tr>
			<tr>
				<td>&nbsp;</td>
				<td >  Total Encashment Request(s)</td>
				<td >
				  <span class="style31">
				  <?php				
				$strSQL="SELECT COUNT(*) AS D FROM tbl_encashment WHERE request_ctr>0 $econd";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				echo number_format($rsbt['D'],0) . " Request(s)"; 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>	
              
			<tr>
				<td>&nbsp;</td>
				<td >  Total Payables:</td>
				<td >
				  <span class="style31">
				  <?php				
				echo number_format($debit,2); 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>			  
              
			<tr>
				<td>&nbsp;</td>
				<td >  Less Total Encashment</td>
				<td >
				  <span class="style33">
				  <?php				
				$strSQL="SELECT SUM(amount) AS D FROM tbl_encashment WHERE request_ctr>0 $econd";
				$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
				$rsbt=mysql_fetch_assoc($rsb);  
				$pay+=$rsbt['D'];
				echo "-".number_format($rsbt['D'],2) . ""; 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>          	
											
<tr>
				<td>&nbsp;</td>
				<td >  Total Balance:</td>
				<td >
				  <span class="style31">
				  <?php				
				echo number_format($debit-$pay,2); 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>            		  
						      <tr>
        <td ></td>
            <td colspan="3" valign="top"><br><h4> Product Sales Summary </h4></td>
          </tr>
      <tr>
        <td>&nbsp;</td>
        <td >  Total Admin Product Net Sales</td>
        <td >
          <span class="style31">
          <?php       
        $strSQL="SELECT sum(total_net) as TOTAL FROM tbl_reservations WHERE payment_type!='OE' $scond2";
        $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
        $rsbt=mysql_fetch_assoc($rsb);  
        echo number_format($rsbt['TOTAL'],2); 
        $PS+=$rsbt['TOTAL'];
        ?>
          </span></td>      
          <td width="185">&nbsp;</td>
      </tr>          
      <tr>
        <td>&nbsp;</td>
        <td >  Total Product Codes Net Sales</td>
        <td >
          <span class="style31">
          <?php       
        $strSQL="SELECT sum(total_net) as TOTAL FROM tbl_pins_reservations $scond";
        $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
        $rsbt=mysql_fetch_assoc($rsb);  
        echo number_format($rsbt['TOTAL'],2); 
        $PS+=$rsbt['TOTAL'];
        ?>
          </span></td>      
          <td width="185">&nbsp;</td>
      </tr> 							
			<tr>
				<td ></td>
		        <td colspan="3" valign="top"><br><h4> Overall Statistics Summary</h4> </td>
		      </tr>
                          
                   <tr>
				<td>&nbsp;</td>
				<td >  Total Product Sales:</td>
				<td >
				  <span class="style31">
				  <?php				
				echo number_format($PS,2); 
				?>
				  </span></td>			
		      <td width="185">&nbsp;</td>
			</tr>      
		  <?php
		    $x=1;
  			$strSQL="SELECT * FROM tbl_entry order by registration_type" ;						
			$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
			$rsbt=mysql_fetch_assoc($rsb);
			do{	  
				for($y=1;$y<=6;$y++){
					if($rsbt['registration_type']=="PLAN".$y){
						$ENTRY=$rsct["ENTRY".$y];
					}
				}
		  ?> 		
			<tr>
				<td>&nbsp;</td>
				<td >  Total Paid <?php echo loadPosition($rsbt['registration_type']) ."-".$rsbt['title']; ?> Codes x <?php echo $ENTRY; ?> </td>
				<td >
				<?php	

					if(empty($rsbt['ectr'])) {
						$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes_used WHERE free=0 $pcond" ;

					} else {
						$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes_used WHERE ectr=".$rsbt['ectr']." AND free=0 $pcond" ;
					}
				
						//echo $strSQL;						
					$rsf=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsft=mysql_fetch_assoc($rsf);					 

					if(empty($rsbt['ectr'])) {
			        	$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes WHERE free=0 $ccond" ;
					} else {
						$strSQL="SELECT COUNT(registration_ctr) as TOTAL FROM tbl_registration_codes WHERE ectr=".$rsbt['ectr']." AND free=0 $ccond" ;
					}

			          //echo $strSQL;           
			        $rsf2=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
			        $rsf2t=mysql_fetch_assoc($rsf2);

          			$ENTRY=0;     
					for($x=1;$x<=$TOTAL_ENTRIES;$x++){
						if($rsbt['registration_type']=="PLAN".$x){
							$ENTRY=$rsct["ENTRY".$x];
						}
					} 
					echo number_format($rsft['TOTAL'] * $ENTRY,2);                  
					$DA+=($rsft['TOTAL'] * $ENTRY);
					$x++;
			  						
				?></td>
			</tr>  
        <?php }while($rsbt=mysql_fetch_assoc($rsb)); ?>
      <tr>
        <td>&nbsp;</td>
        <td > Total Product Sales</td>
        <td >
          <span class="style36">
          <?php       
        echo number_format($PS,2); 
        ?>
          </span></td>
      </tr>            			
			<tr>
				<td>&nbsp;</td>
				<td > Total Company Sales </td>
				<td >
				  <span class="style36">
				  <?php				
				echo number_format($DA+$PS,2); 
				?>
				  </span></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td > Less Total Commissions </td>
				<td >
				  <span class="style36">
				  <?php				
				echo "-".number_format($debit+$pdebit,2); 
				?>
				  </span></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td > Total Company Income</td>
				<td >
				  <span class="style36">
				  <?php				
				echo number_format(($DA+$PS)-($debit+$pdebit),2); 
				?>
				  </span></td>
			</tr>	
<tr>
				<td>&nbsp;</td>
				<td > Payout Percentage</td>
				<td >
				  <span class="style36">
				  <?php		
				  if(($DA+$PS)>0){		
					echo number_format((($debit+$pdebit)/($DA+$PS)) * 100,2); 
				  }else{
					 echo number_format(0,2);
				  }
				?> %
				  </span></td>
			</tr>               	
            	
			
			<!--<tr>
				<td ></td>
		        <td colspan="3" valign="top"><br><h4> Overall Membership Codes Summary</h4> </td>
		      </tr>
			<tr>
				<td>&nbsp;</td>
				<td >Available  Membership Codes</td>
				<td >
				  <?php
					$strSQL="SELECT COUNT(*) as TOTAL FROM tbl_registration_codes WHERE status=0" ;
					$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsbt=mysql_fetch_assoc($rsb);
					echo number_format($rsbt['TOTAL'],0);
			  ?>
				</td>			
		      </tr>
			<tr>
				<td>&nbsp;</td>
				<td >Registered Membership Codes </td>
				<td >
				  <?php
					$strSQL="SELECT COUNT(*) as TOTAL FROM tbl_registration_codes_used WHERE status=1" ;
					$rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
					$rsbt=mysql_fetch_assoc($rsb);
					echo number_format($rsbt['TOTAL'],0);
			  ?>
				  </td>			
		  </tr>	-->
			  			  			
																  																				
			<tr>
			  <td height="0"></td>
			  <td></td>
			  <td></td>
			  <td></td>
			  </tr>
</table>
</form>			

                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>

	
  

  </body>
</html>
