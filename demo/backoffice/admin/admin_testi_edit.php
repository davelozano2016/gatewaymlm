<?php
require_once('admin_session.php');
$head="Edit Testimonials Category";
if($rst['web']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_POST['testi_name'])){
	$name = mysql_escape_string($_POST['testi_name']); 
	$text = mysql_escape_string($_POST['testi_text']); 
	
		if($text!=""){		
							$updateSQL="UPDATE tbl_testi SET 
										testi_name='$name',
										testi_text='$text'										
										WHERE testi_ctr=" .$_POST['pctr'] ."";					
							mysql_query($updateSQL,$connBS) or die(mysql_error() . $updateSQL);																
							$error_upload="";
							$uploadpath="../products/";
							$thefile = 'thefile'; 
							if(isset($_FILES[$thefile]['name'])) {
								if (basename($_FILES[$thefile]['name']) != '' ){
									$uploadfile=basename($_FILES[$thefile]['name']);
									$uploadfileandpath = $uploadpath.$uploadfile;
										if (is_uploaded_file($_FILES[$thefile]['tmp_name']))
												{
												if (move_uploaded_file($_FILES[$thefile]['tmp_name'],$uploadfileandpath)) {
													chmod("../testimonials/$uploadfile",0777);
													$updateSQL="UPDATE tbl_testi SET img='". $uploadfile ."' WHERE testi_ctr=" .$_POST['pctr'] ."";		
													mysql_query($updateSQL,$connBS) or die(mysql_error() . $updateSQL);									
													$error_upload="";
												}
										}
								}
							}		
				header("Location: admin_testi.php?m=" .$mcrypt->encrypt("System Message: Edit Testimonials Successfully Processed"));
		}else{
				$error_message="Testimonials cannot be an empty field";
		}
}

if(isset($_GET['r'])){
	$strSQL="SELECT * FROM tbl_testi WHERE testi_ctr=".$mcrypt->decrypt($_GET['r']);
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error().$strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
						echo $error_message;
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="" enctype="multipart/form-data">
							                 
                        
                          
							                      							                          
 						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Name</label>
                              <div class="col-sm-6">
                                    <input name="testi_name" type="text" class="form-control"  id="testi_name" size="80" value="<?php echo $rsbt['testi_name']; ?>" /> 

                              </div>
                          </div>
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Testimonials</label>
                              <div class="col-sm-6">
 								<textarea id="testi_text" name="testi_text" style="height: 200px; width: 100%;" class="form-control"><?php echo $rsbt['testi_text']; ?></textarea>
                              </div>
                          </div>      
                          
                        
						<div class="form-group">
                        
						
                                 
                              <label class="col-sm-3 col-sm-3 control-label">Current Photo</label>
                              <div class="col-sm-6">
						<?php
								if($rsbt['img']=="NA.jpg"){
									$img="../testimonials/" . $rsbt['testi_img'];
								}else{
									$img="../testimonials/" . $rsbt['testi_img'];								
								}						
						?>           
							<a href="<?php echo $img; ?>" target="_blank"><img src="<?php echo $img; ?>" width="60" height="60"  class="img-thumbnail" /></a>                          
                              </div>
                              
                          </div>                                                       
						<div class="form-group">
                        
						
                                 
                              <label class="col-sm-3 col-sm-3 control-label">Change Photo</label>
                              <div class="col-sm-6">
 								<input name="thefile" type="file"  id="thefile"/>  JPEG/PNG/GIF Format
                              </div>
                              
                          </div>                                                                                                                                                            							
                          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Update Testimonials</a>
                               </div>
                          </div>
                                 <input name="bv" type="hidden" class="form-control"  id="bv" size="80" value="0" /> 
							 	 <input name="allow_update" type="hidden" id="allow_update"/>                                                                                                  
								 <input name="pctr" type="hidden" id="pctr" value="<?php echo $rsbt['testi_ctr']; ?>"/>                                                                                                  
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
<script language="javascript" type="text/javascript">
function validate(){
	var errorstr = "";
	errorstr += checkitem(document.frm.testi_name,"Name");	
	errorstr += checkitem(document.frm.testi_text,"Testimonials");		
	if(errorstr==""){
			document.frm.allow_update.value="YES";
			document.frm.submit();
	}else{
		alert(errorstr);
	}
}
function checkitem(item, fdesc) { 
	var errorstr = "";
	if (item.value == "" || checkblanks(item.value) == true)  {
   		if (fdesc != "") { 
   			errorstr = "'" + fdesc + "' is required.\n";
		}
   }
   return errorstr;
 }

// following looks for all blanks in a string - typical of checking//
function checkblanks(item)  {
  var isblank = true;
  for (i = 0; i < item.length; i++) {
    if (item.charAt(i) != " ") {
      isblank = false;  }
  } 
  return isblank;
}

// check for valid emails....
function checkemail(item) {
  var goodmail = true;
  var addr = item.value;
  var invchar = " /:,;";
  var errorstr = "";
  var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
  var emailFilter=/^.+@.+\..{2,3}$/;
  for (i=0; i<invchar.length; i++)  {
	badchar = invchar.charAt(i);
	if (addr.indexOf(badchar,0) > -1)  {
   		goodmail = false;
	}
  }
  atpos = addr.indexOf("@",1);
  if (atpos == -1) {
	goodmail = false;
  }
  else  {
	perpos = addr.indexOf(".",atpos);
	if (perpos == -1)  {
		goodmail = false;
	} else if (perpos + 3 > addr.length)  {
		goodmail = false;
	} else if (addr.match(illegalChars)) {
		goodmail = false;
	} else if (!(emailFilter.test(addr))) { 
		goodmail = false;
    }
  }
  if (goodmail == false) {
    errorstr = "Email address is not valid.\n";
  }
  return errorstr;
}
</script>

	
  

  </body>
</html>
