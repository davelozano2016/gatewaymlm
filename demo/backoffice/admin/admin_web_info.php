<?php
include('admin_session.php');
$head="Web Information";
if($rst['web']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}

if($_POST['allow_update']=="YES" && isset($_POST['allow_update'])){
	$birth=$_POST['bday_year'] ."-" .$_POST['bday_month'] ."-" . $_POST['bday_day'];
	$updateSQL="UPDATE tbl_web SET
					address='" . mysql_escape_string($_POST['address']) ."',
					contact='" . mysql_escape_string($_POST['contact']) ."',					
					email='" . mysql_escape_string($_POST['email']) ."'";					
	mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);	
	$message="System Message: Website Information Successfully Updated<br><br>";		
}

$strSQL="SELECT * FROM tbl_web";
$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);		

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="">


                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Address:</label>
                              <div class="col-sm-6">
            					<input name="address" type="text" class="form-control" id="address" size="30" value="<?php echo $rsbt['address']; ?>" />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Contact:</label>
                              <div class="col-sm-6">
                          <input name="contact" type="text" class="form-control" id="contact" size="30" value="<?php echo $rsbt['contact']; ?>" />
                              </div>
                          </div>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Email Address:</label>
                              <div class="col-sm-6">
                          <input name="email" type="text" class="form-control" id="email" size="30" value="<?php echo $rsbt['email']; ?>" />
                             </div>
                          </div> 
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Update Information</a>
                               </div>
                          </div>                                              
                          
                    <input name="allow_update" type="hidden" value="NO" />		
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

    <script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" type="text/javascript">
		function validate(){
				if(confirm("Continue Updating Web Information")){
					document.frm.allow_update.value="YES";
					document.frm.submit();
				}
		}
	</script>

        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>

	
  

  </body>
</html>
