<?php
include('admin_session.php');
$head="Generate Paid Pins Form";
if($rst['codes']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
function removehack($str) {
    $str=str_replace("\"","",$str);
    $str=str_replace("/","",$str);
    $str=str_replace("\\","",$str); 
    $str=str_replace("'","",$str);
    $str=str_replace("&","",$str);
    $str=str_replace("-","",$str);
    $str=str_replace("*","",$str);
    $str=str_replace(")","",$str);
    $str=str_replace("(","",$str);
    $str=str_replace("=","",$str);  
    $str=str_replace("#","",$str);      
    return $str;
}  
if(isset($_POST['allow']) && $_POST['allow']=="YES"){
    $delSQL="DELETE FROM tbl_entry_carts WHERE username='". $_POST['username'] ."' and order_type=null";
    mysql_query($delSQL,$connBS) or die(mysql_error(). $delSQL);        
    $delSQL="DELETE FROM tbl_entry_carts_products WHERE username='". $_POST['username'] ."' and order_type=null";
    mysql_query($delSQL,$connBS) or die(mysql_error(). $delSQL);   
                            
    for($x=1;$x<=$_POST['cnt'];$x++){
        if($_POST['t'.$x]>0){
            //check stocks if available
            $ectr=$_POST['e'.$x];
            $item_count=$_POST['t'.$x];
            $price=$_POST['t'.$x] * $_POST['p'.$x];                             
            $reg=$_POST['r'.$x];
            $insertSQL="INSERT IGNORE INTO tbl_entry_carts(username,ectr,item_count,total,date_added,registration_type)VALUES('". $_POST['username']."',$ectr,$item_count,$price,'".getDateTime()."','$reg')";
            mysql_query($insertSQL,$connBS) or die(mysql_error(). $insertSQL);                                    
            $strSQL="SELECT * FROM tbl_entry_products WHERE ectr='$ectr'";
            $rse=mysql_query($strSQL) or die(mysql_error(). $strSQL);
            $rset=mysql_fetch_assoc($rse);
            if(mysql_num_rows($rse)>0){
                do{
                    $pctr=$rset['pctr'];
                    $pitem=$rset['item_cnt'] * $_POST['t'.$x];
                    $insertSQL="INSERT IGNORE INTO tbl_entry_carts_products(username,ectr,pctr,item_count,date_added)VALUES('". $_POST['username']."',$ectr,$pctr,$pitem,'".getDateTime()."')";
                    mysql_query($insertSQL,$connBS) or die(mysql_error(). $insertSQL);                    
                }while($rset=mysql_fetch_assoc($rse));      
            }           
        }   
    }
    //stocks checking
    $errorstr="";
    $strSQL="SELECT pctr,SUM(item_count) as TOTAL FROM tbl_entry_carts_products WHERE username='". $_POST['username']."' GROUP BY pctr";
    $rse=mysql_query($strSQL) or die(mysql_error(). $strSQL);
    $rset=mysql_fetch_assoc($rse);      
    if(mysql_num_rows($rse)>0){
        do{        
            $strSQL="SELECT * FROM tbl_products WHERE pctr=".$rset['pctr'];
            $rsp=mysql_query($strSQL) or die(mysql_error(). $strSQL);
            $rspt=mysql_fetch_assoc($rsp);
            if(mysql_num_rows($rsp)>0){
               //echo $rset['TOTAL'] ." " .$rspt['item_cnt'];
               if($rset['TOTAL']>$rspt['item_cnt']){              
                   $errorstr.="System Message: [".$rspt['pname']."] Out of Stock <br>";               
               }
            }    
         }while($rset=mysql_fetch_assoc($rse));            
    }
    if($errorstr==""){
       $_SESSION['custid']=$_POST['username']; 
       header("Location: admin_entry_purchase_process.php");
    }else{
        $delSQL="DELETE FROM tbl_entry_carts WHERE username='". $_POST['username'] ."' and order_type=null";
        mysql_query($delSQL,$connBS) or die(mysql_error(). $delSQL);        
        $delSQL="DELETE FROM tbl_entry_carts_products WHERE username='". $_POST['username'] ."' and order_type=null";
        mysql_query($delSQL,$connBS) or die(mysql_error(). $delSQL);          
    }
}
if($_SESSION['custid']==""){
    $username="CUST-".mt_rand(1111111,9999999);
    $_SESSION['custid']=$username;
}else{
    $username=$_SESSION['custid'];
}
$strSQL="SELECT * FROM tbl_control_panel";
$rsc=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsct=mysql_fetch_assoc($rsc);

$strSQL="SELECT * FROM tbl_entry WHERE status='Active'";
$rsb=mysql_query($strSQL) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
$rsbt_count=mysql_num_rows($rsb);

//echo $strSQL;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
                         echo $errorstr;
					?>                          
                                <form name="frm" method="post" action="">
                                        <input type="hidden" name="username" value="<?php echo $username; ?>" />
                                        <input type="hidden" name="allow" value="" />                                    
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer ID: <strong><?php echo $username; ?></strong><br />           
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript: setzero(<?php echo $rsbt_count; ?>);">Reset Order </a>  &nbsp;&nbsp;               
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Entry Package Name</th>                                                                                          
                                            <th>Price</th>    
                                            <th>Enter Number of Codes</th>                                                                                                                                    
                                                                                                                                                                                
                                        </tr>
                                    </thead>
		                          <?php
								  			
											if(mysql_num_rows($rsb)>0){
											$count=1;
											do{
                                                    for($x=1;$x<=6;$x++){
                                                        if($rsbt['registration_type']=="PLAN".$x){
                                                            $price=$rsct['ENTRY'.$x];
                                                        }
                                                    }
																							
											?>					
																		
											<tr id="tr<?php echo $count; ?>">                                                                            
                                           <td><?php echo loadPosition($rsbt['registration_type']) . " " .$rsbt['title']; ?></td>                                                                            											    
										   <td><?php echo number_format($price,2);?></td>                                                                            
                                           <td>                
                                               <input type="text" name="t<?php echo $count;?>" class="formitems"  value="<?php echo $value; ?>" size="10" style="text-align:right;" onkeyup="javascript: compute(<?php echo $rsbt_count; ?>);" />                  
                                               <input type="hidden" name="p<?php echo $count; ?>" value="<?php echo $price; ?>" />                                                         
                                               <input type="hidden" name="e<?php echo $count; ?>" value="<?php echo $rsbt['ectr']; ?>" />
                                               <input type="hidden" name="r<?php echo $count; ?>" value="<?php echo $rsbt['registration_type']; ?>" />                                                                                             
                                            </td>                                                          
                                                                                                                                                                                                                  
									       </tr>
									 <?php
									        $count++;
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										?>  
										<input type="hidden" name="cnt" value="<?php echo $count; ?>" />
                                        	<tr>
                                        	<td colspan="2">&nbsp;</td>
                                        	<td  align="left"><div id="t_items">Total Items: <strong><?php echo number_format($amount,0); ?> Items</strong></div></td>
                                        	</tr>	
                                            <tr>
                                            <td colspan="2">&nbsp;</td>
                                            <td  align="left"><div id="t_price">Total Amount: <strong><?php echo number_format($amount,2);  ?></strong></div></td>
                                            </tr>   
                                            <tr>
                                            <td colspan="2">&nbsp;</td>
                                            <td  align="left"><input name="Submit" type="button" class="formbutton_team" value="Proceed" onclick="javascript: ask(<?php echo $rset_count; ?>);" /></td>
                                            </tr>                                             
                                        									 
		                          </tbody>
                                 
		                      </table>
		                      </form>
                
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php  include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>
    <script language="javascript" type="text/javascript" src="scripts/shop_entry.js"></script>
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
