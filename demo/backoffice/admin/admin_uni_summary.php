<?php
include('admin_session.php');
$head="Unilevel Process Sales Summary";
if($rst['uni']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}

$strSQL="SELECT  * FROM tbl_control_panel";
$rsc=mysql_query($strSQL,$connBS) or die(mysql_error());
$rsct=mysql_fetch_assoc($rsc);	

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
            
            <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>   
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
                          	 <table class="table table-hover">
                                    
                                    <tbody>
                                        <tr>
                                            <td width="50%">Cut-off Start</td>
                                            <td width="50%"><?php echo $rsct['UNI_MONTH'] ."-".$rsct['UNI_DAY'] ."-".$rsct['UNI_YEAR']; ?> &nbsp;&nbsp;( <a href="admin_cp_uni.php">Change Cut-Off Date</a> )</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Cut-off End</td>
                                            <td width="50%"><?php echo $rsct['END_MONTH'] ."-".$rsct['END_DAY'] ."-".$rsct['END_YEAR']; ?></td>
                                        </tr>
                                        <?php
											$strSQL="SELECT COUNT(reservation_ctr) as TOTAL,SUM(bv) AS tbv,SUM(total_amount) AS tamount,SUM(item_count) AS tcount FROM tbl_reservations WHERE DATEDIFF(date_process,'".$rsct['UNI_YEAR'] ."-". $rsct['UNI_MONTH'] ."-".$rsct['UNI_DAY'] ."')>=0 AND DATEDIFF(date_process,'".$rsct['END_YEAR'] ."-". $rsct['END_MONTH'] ."-".$rsct['END_DAY'] ."')<=0";
											$rso=mysql_query($strSQL, $connBS) or die(mysql_error(). $strSQL);
											$rsot=mysql_fetch_assoc($rso);												
										?>
                                       
                                        <tr>
                                            <td>Total Sales</td>
                                            <td><?php echo number_format($rsot['tamount'],2); ?></td>
                                        </tr>  
                                        <tr>
                                            <td>Total Product Sales</td>
                                            <td><?php echo number_format($rsot['TOTAL'],0); ?> Sales</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Product Items</td>
                                            <td><?php echo number_format($rsot['tcount'],0); ?> Items</td>
                                        </tr>  
										<tr>
                                            <td>Enter Administrator Password:</td>
                                            <td>
                                            <form name="frm" method="post">
 												<input type="password" name="password" class="formitems" />  
				 								<input type="hidden" name="allow" value="NO" />                                                                                          
                                            	<input name="cmdprocess" type="button" class="formbutton_team" value="Process Unilevel" onClick="javascript: validate();"  />
                                             </form></td>
                                        </tr>                                          
										                                                                                                                                                                                                                                          
                                    </tbody>
                                </table>
                                  
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
  
  	<script language="javascript" type="text/javascript">
					function validate(){
						if(document.frm.password.value!=""){
							if(confirm("Continue to Process Unilevel?")){
								document.frm.allow.value="YES";
								document.frm.action="admin_uni_process.php";
								document.frm.submit();
							}
						}else{
							alert("Kindly specify your Administrator password");
						}
					}
				</script>  

  
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
	

	
  

  </body>
</html>
