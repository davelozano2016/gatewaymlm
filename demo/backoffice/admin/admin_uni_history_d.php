<?php
include('admin_session.php');
if($rst['uni']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_GET['r'])){
	$e_code=$mcrypt->decrypt($_GET['r']);
	$_SESSION['uni_h']=$e_code;
}
if($_SESSION['uni_h']==''){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}else{
	$e_code=$_SESSION['uni_h'];
}

$cond="";

if($_POST['txtsearch']!=""){
		$cond .=" AND " .$_POST['seltype'] ." like '%" . trim($_POST['txtsearch']) . "%'";
}


$strSQL="SELECT * FROM tbl_control_panel";
$rsc=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsct=mysql_fetch_assoc($rsc);

$strSQL="SELECT * FROM tbl_cmp WHERE uni_no='$e_code'";
$rsu=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsut=mysql_fetch_assoc($rsu);


$strSQL="SELECT * FROM tbl_cmp_details_$e_code WHERE level=0 $cond order by uni_ctr";
$rsd=mysql_query($strSQL,$connBS); 	
$totalrows=mysql_num_rows($rsd);
$excessrows=$totalrows % 10;
$rowcount=$totalrows-$excessrows;
$rowcount=$totalrows / 10;
$recordstart=0;
$recordend=10;
	if(isset($_GET['pagecount'])) {
			$arr3=preg_split("/-/",$mcrypt->decrypt($_GET['range']));
			$recordstart=trim($arr3[0]);
			$recordend=trim($arr3[1]);
	}	
$rsb=mysql_query($strSQL . " Limit " .$recordstart ."," . $recordend ."",$connBS) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
$rsbt_count=mysql_num_rows($rsb); 
//echo $strSQL;
$head="$e_code Unilevel History Summary";

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	          	<a href="admin_uni_history.php">Back to Unilevel Result History</a>

          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>                          
                    
                   <table class="table table-hover">
                                    
                                    <tbody>
                                        <tr>
                                            <td width="50%">Unilevel Process No</td>
                                            <td width="50%"><?php echo $e_code; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Date Process</td>
                                            <td width="50%"><?php echo $rsut['date_added']; ?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td width="50%">Admin User</td>
                                            <td width="50%"><?php echo $rsut['user']; ?></td>
                                        </tr>
                                    
                                        <tr>
                                            <td width="50%">Cut-off Start</td>
                                            <td width="50%"><?php echo $rsut['uni_start']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Cut-off End</td>
                                            <td width="50%"><?php echo $rsut['uni_end']; ?></td>
                                        </tr>
                                        <?php
											$strSQL="SELECT COUNT(reservation_ctr) as TOTAL,SUM(bv) AS tbv,SUM(total_amount) AS tamount,SUM(item_count) AS tcount FROM tbl_reservations WHERE DATEDIFF(date_process,'".$rsut['uni_start'] ."')>=0 AND DATEDIFF(date_process,'".$rsut['uni_end']."')<=0";
											$rso=mysql_query($strSQL, $connBS) or die(mysql_error(). $strSQL);
											$rsot=mysql_fetch_assoc($rso);												
										?>
                                       
                                        <tr>
                                            <td>Total Sales</td>
                                            <td><?php echo number_format($rsot['tamount'],2); ?></td>
                                        </tr>  
                                        <tr>
                                            <td>Total Product Sales</td>
                                            <td><?php echo number_format($rsot['TOTAL'],0); ?> Sales</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Product Items</td>
                                            <td><?php echo number_format($rsot['tcount'],0); ?> Items</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Sales PV</td>
                                            <td><?php echo number_format($rsot['tbv'],0); ?> PV</td>
                                        </tr>  
 <tr>
                                            <td>Total Maintained PV</td>
                                            <td><?php echo number_format($rsut['total_mbv'],0); ?> PV</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Compressed PV</td>
                                            <td><?php echo number_format($rsut['total_cbv'],0); ?> PV</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Paout</td>
                                            <td><?php echo number_format($rsut['total_income'],2); ?></td>
                                        </tr> 										                                      
										                                                                                                                                                                                                                                          
                                    </tbody>
                                </table>
				<form name="sfrm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
                                  <div align="left">&nbsp;&nbsp;&nbsp;Search By:
                                    <script language="javascript" type="text/javascript">
                                                        function search_check(){
                                                                document.sfrm.submit();
                                                        }
                                                      </script>
									<select name="seltype" class="formitems">
                                      <option value="id_code" <?php if($_POST['seltype']=="id_code") { echo "selected"; } ?>>User ID</option>	
                                      
						      </select>                             
                                    <input name="txtsearch" type="text" class="formitems" id="txtsearch" value="<?php echo $_POST['txtsearch']; ?>">     
                                    <input name="Button" type="button" class="formbutton_team" onClick="javascript: search_check();" value="Search Record">
                                    <a href="<?php echo $_SERVER['PHP_SELF']; ?>">View All Records</a> 
                                </form>	                
                </br>
                          
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Member Name</th>
                                            <th>MBV</th>		  			  			 
                                            <th>CBV</th>			  			  			   	  			  
                                            <th>Personal</th>			  			  			   	  			  			  
                                            <th>Unilevel</th>			  			  
                                            <th>Payout</th>	
                                            <th>View</th>	                                            
                                        </tr>
                                    </thead>
		                          <?php
								  			
											if(mysql_num_rows($rsb)>0){
											
											do{
												$strSQL="SELECT firstname,lastname,cd FROM tbl_members WHERE id_number='".$rsbt['id_code'] ."'";
												$rsm=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
												$rsmt=mysql_fetch_assoc($rsm);
												
												$name=$rsmt['firstname'] . " ".$rsmt['lastname'];
												
												$income=0;
												
												
												
										 ?>                                    
										<tr>                                                                            
                                                  <td><?php echo $rsbt['id_code']; ?></td>
                                                  <td><?php echo $name; ?></td>
                                                  <td><?php echo $rsbt['bv']; ?></td> 	  			  			    			      			  								
                                                  <td><?php echo $rsbt['cbv']; ?></td> 	  	
                                                <td>
                                                  <?php
                                                    $strSQL="SELECT sum(income) as TOTAL FROM tbl_cmp_details_$e_code WHERE level=0 and id_code='" . $rsbt['id_code'] ."'"; 
                                                    $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                                    $rsdt=mysql_fetch_assoc($rsd);
                                                    $income+=$rsdt['TOTAL'];								
                                                    echo number_format($rsdt['TOTAL'],2);				
                                                   ?></td> 	  			 
                                                  	
                                                  <td>
                                                  <?php
                                                    $strSQL="SELECT sum(income) as TOTAL FROM tbl_cmp_details_$e_code WHERE level>=1 and id_code='" . $rsbt['id_code'] ."'"; 
                                                    $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                                    $rsdt=mysql_fetch_assoc($rsd);
                                                    $income+=$rsdt['TOTAL'];								
                                                    echo number_format($rsdt['TOTAL'],2);				
                                                   ?></td> 	  			 
                                                  <td><?php echo number_format($income,2); ?></td> 	  			  			    			      			                
												  <td><?php 
										   			echo "<a href='admin_uni_history_members.php?r=".$mcrypt->encrypt($rsbt['id_code'])."'>Members</a> | <a href='admin_uni_history_level.php?r=".$mcrypt->encrypt($rsbt['id_code'])."'>Per Level</a>"; 
												?> 
											</td>                                                 	  
										</tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										 ?>   
		                          </tbody>
                                 
		                      </table>
							  <?php
								if($cond==""){
									include ('admin_paging.php');
								}
							$strSQL="SELECT SUM(bv) as MBV, SUM(cbv) as CBV,SUM(income) as TAMOUNT FROM tbl_cmp_details_$e_code";
							$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
							$rsbt=mysql_fetch_assoc($rsb);
											
							?>  
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
  
  	<script language="javascript" type="text/javascript">
					function validate(){
						if(document.frm.password.value!=""){
							if(confirm("Continue to Process Unilevel?")){
								document.frm.allow.value="YES";
								document.frm.action="admin_uni_process_summary_proc.php";
								document.frm.submit();
							}
						}else{
							alert("Kindly specify your Administrator password");
						}
					}
				</script>  
      
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
