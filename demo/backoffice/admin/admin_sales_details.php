<?php
include('admin_session.php');
$head="Product Sales Summary";
if($rst['sales']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}

$order_no=$mcrypt->decrypt($_GET['r']);
$order_no=mysql_escape_string($order_no);
$order_no=substr($order_no,0,20);
$strSQL="SELECT * FROM tbl_reservations WHERE order_type='$order_no'";
$rsr=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsrt=mysql_fetch_assoc($rsr);
if(mysql_num_rows($rsr)==0){
	header("Location: admin_sales_summary.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Product Order History</h3>
          	<a href="admin_sales_summary.php">Back to Product Sales Summary</a>
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
                          	 <table class="table table-hover">
                                    
                                    <tbody>
                                        <tr>
                                            <td width="50%">Date Process</td>
                                            <td width="50%"><?php echo $rsrt['date_process']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Order No</td>
                                            <td><?php echo $rsrt['order_type']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>User ID:</td>
                                            <td><?php echo $rsrt['username']; ?></td>
                                        </tr>                                         
                                        <tr>
                                            <td>Name:</td>
                                            <td><?php echo $rsrt['fname']; ?></td>
                                        </tr>  
                                        <!--<tr>
                                            <td>Contact:</td>
                                            <td><?php echo $rsrt['mobile']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Address:</td>
                                            <td><?php echo $rsrt['address']. " ".$rsrt['province']. " ".$rsrt['city']. " ". $rsrt['district']; ?></td>

                                        </tr> 

                                        <tr>
                                            <td>Shipping Type:</td>
                                            <td><?php echo $rsrt['ship_type']; ?></td>

                                        </tr>                                                                                  
                                        <tr>
                                            <td>Status:</td>
                                            <td><?php if($rsrt['status']=="W") { echo "PENDING"; }else{ echo "PROCESSED"; }?></td>

                                        </tr> -->
                                        <tr>
                                            <td>Approved By:</td>
                                            <td><?php echo $rsrt['approve_user']?></td>

                                        </tr>                                                                                   
                                        <tr>
                                            <td>Total Items</td>
                                            <td><?php echo number_format($rsrt['item_count'],0); ?> Items</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Amount</td>
                                            <td><?php echo number_format($rsrt['total_amount'],2); ?></td>
                                        </tr>  
                                        <tr>
                                            <td>Total Discount</td>
                                            <td><?php echo number_format($rsrt['discount'],2); ?>%</td>
                                        </tr>   
                                        <tr>
                                            <td>Total Net</td>
                                            <td><?php echo number_format($rsrt['total_net'],2); ?></td>
                                        </tr>                                                                               
                                       <!-- <tr>
                                            <td>Total Shipping Fee</td>
                                            <td><?php echo number_format($rsrt['sfee'],2); ?></td>
                                        </tr>  
                                        <tr>
                                            <td>Total</td>
                                            <td><?php echo number_format($rsrt['subtotal'],2); ?></td>
                                        </tr> 
 
                                        <?php
                                        if($rsrt['ship_type']!='PICK-UP' && $rsrt['dslip']!=""){
                                        ?>
                                        <tr>
                                            <td>Receipt</td>
                                            <td><a href="../images/receipt/<?php echo $rsrt['dslip'];  ?>" target="_new"><img src="../images/receipt/<?php echo $rsrt['dslip'];  ?>" width="100" height="100"/></a><br> (Click Image to Enlarge)</td>
                                        </tr>                             
                                        <?php } ?>-->   										                                                                                                                                                                                                                                          
                                    </tbody>
                                </table>
                                  
<?php
                            $strSQL="SELECT * FROM tbl_carts WHERE tbl_carts.order_type='".$order_no. "'";
                            $rse=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                            $rset=mysql_fetch_assoc($rse);
                            $rset_count=mysql_num_rows($rse);	
                            ?>                            
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Item</th>                                                                                        
                                            <th>Amount</th> 
                                            <th>Points</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php                                    
                                    if($rset_count>0){
                                        $count=1;
                                        do{ 
										
											                       $items+=$rset['item_count'];
                                            $price+=$rset['total'];
                                            $bv+=$rset['bv'];
											$strSQL="SELECT pname FROM tbl_products WHERE pctr=".$rset['pctr'];
											$rsp=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
											$rspt=mysql_fetch_assoc($rsp);
											
										?>                                    
                                        <tr>
    
                                            <td><?php echo $rspt['pname']; ?></td> 
                                            <td><?php echo number_format($rset['item_count'],0); ?></td>                                            
                                            <td><?php echo number_format($rset['total'],2); ?></td>                                            
                                            <td><?php echo number_format($rset['bv'],2); ?></td>     
                                            
                                        </tr>
											  <?php
                                        $count++;
                                        }while($rset=mysql_fetch_assoc($rse));
                                        }
                                        ?>                                        
                                        <tr>
                                            <td>Summary</td>
                                            <td><?php echo number_format($items,0); ?></td>                                            
                                            <td><?php echo number_format($price,2); ?></td>           
                                            <td><?php echo number_format($bv,2); ?></td>                                     
                                        </tr>
                                    </tbody>
                                </table>
                              
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
  
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
	

	
  

  </body>
</html>
