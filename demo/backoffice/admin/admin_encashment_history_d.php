<?php
include('admin_session.php');

if($rst['encash']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_GET['r'])){
	$e_code=$mcrypt->decrypt($_GET['r']);
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}

$strSQL="SELECT * FROM tbl_encashment WHERE e_code='$e_code'";											
$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
$head="$e_code Members Encashment History";

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	<a href="admin_encashment_history.php">Back to Encashment History</a>

          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>                          
                    
                  
                </br>
                          
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Member Name</th>
                                            <th>Country</th>
                                            <th>Date Requested</th>			  			  
                                            <th>Request Type</th>		  			  			 
                                            <th>Request</th>			  			  			   	  			  
                                            <th>CD</th>			  			  			   	  			  			  
                                            <th>Admin Fee</th>			  			  
                                            <th>System Fee</th>			  			                
                                            <th>Payout</th>	
                                            <th>Country Payout</th>
                                        </tr>
                                    </thead>
		                          <?php
								  			
											if(mysql_num_rows($rsb)>0){
											
											do{
												$strSQL="SELECT firstname,lastname,cd FROM tbl_members WHERE id_number='".$rsbt['id_code'] ."'";
												$rsm=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
												$rsmt=mysql_fetch_assoc($rsm);

                        $strSQL="SELECT * FROM tbl_countries WHERE code='".$rsbt['bonus_type'] ."'";
                        $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                        $rsdt=mysql_fetch_assoc($rsd);                            
												
												$name=$rsmt['firstname'] . " ".$rsmt['lastname'];
												
												
												$total_cd+=$rsbt['cd'];						
												$total_tax+=$rsbt['tax'];
												$total_fee+=$rsbt['fee'];						
												$total_income+=$rsbt['income'];
												$total_payout+=$rsbt['payout'];													
												
												
										 ?>                                    
										<tr>                                                                            
                                                  <td><?php echo $rsbt['id_code']; ?></td>
                                                  <td><?php echo $name; ?></div>
                                                  <td><?php echo $rsbt['bonus_type']; ?></td>
                                                  <td><?php echo $rsbt['date_requested']; ?></td> 	  			  			    			      			  								
                                                  <td><?php echo $rsbt['request_type']; ?></td> 	  	
                                                  <td><?php echo number_format($rsbt['amount'],2); ?></td> 	  			  			    			      			  
                                                  <td><?php echo number_format($rsbt['cd'],2); ?></td> 	  			  			    			      			  
                                                  <td><?php echo number_format($rsbt['tax'],2); ?></td> 	  			  			    			      			  
                                                  <td><?php echo number_format($rsbt['fee'],2); ?></td> 	  			  			    			      			  
                                                  <td><?php echo number_format($rsbt['payout'],2); ?></td> 	  
                                                  <td><?php echo number_format($rsbt['payout'] * $rsdt['exchange_rate'],2) . " " . $rsdt['exchange_code']; ?></td>  			  			    			      			  
										</tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										 ?>   
		                          </tbody>
                                 
		                      </table>
				&nbsp;&nbsp;Total Amount: <strong><?php echo number_format($total_income,2); ?></strong><br />
				&nbsp;&nbsp;Total CD: <strong><?php echo number_format($total_cd,2); ?></strong><br />				
				&nbsp;&nbsp;Total Tax: <strong><?php echo number_format($total_tax,2); ?></strong><br />
				&nbsp;&nbsp;Total Fee: <strong><?php echo number_format($total_fee,2); ?></strong><br />                
				&nbsp;&nbsp;Total Payout: <strong><?php echo number_format($total_payout,2);  ?></strong><br />
                                        
										                 
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
  
	<script language="javascript" type="text/javascript">
					function validate(){
						if(document.payfrm.password.value!=""){
							if(confirm("Continue to Process Encashment?")){
								document.payfrm.allow.value="YES";
								document.payfrm.action="admin_encashment_proc.php";
								document.payfrm.submit();
							}
						}else{
							alert("Kindly specify your Administrator password");
						}
					}
				</script>  
	<script language="javascript" type="text/javascript">
		function confirm_delete(ctr){
			if(ctr!=""){
				if(confirm("Continue cancel request?")){
					document.selectfrm.selectedItem.value=ctr;
					document.selectfrm.action="admin_encashment_delete.php";
					document.selectfrm.submit();
				}
			}else{
				alert("Kindly select a news");
			}
		}	
    </script>
      <form name="selectfrm" method="post">
      <input type="hidden"  name="selectedItem" id="selectedItem" value="">
      </form>
      
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
