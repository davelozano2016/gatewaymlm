      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  
              	  <h5 style="margin-left:10px">User: &nbsp;<?php echo $_SESSION['bs_username']; ?></h5>                                  	  	
                  <li class="mt">
                      <a class="active" href="admin_main.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
 				<?php
				 if($rst['mem']==1){
					 ?>                  
				<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-edit"></i>
                          <span>Member Accounts</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_account.php">Member Information</a></li>
                            <li><a href="admin_bank.php">Member Bank Information</a></li>
                            <li><a href="admin_cd.php">Member CD</a></li>
                            <li><a href="admin_reports_members.php">Member Reports</a></li>
                            
                      </ul>
                  </li>
                  <?php } ?>
                                    
				 <?php
				 if($rst['codes']==1){
					 ?>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-pencil-square"></i>
                          <span>Membership Pins</span>
                      </a>
                      <ul class="sub">

                          <li><a href="admin_entry_purchase.php">Create Paid Pins</a></li>
                          <li><a href="admin_entry_select.php">Create Pins to Member</a></li>                                                                               
                          <li><a href="admin_pins_generate.php">Generate Free/CD Pins</a></li>
                          <li><a href="admin_entry_orders.php">Paid Pins History</a></li>                             
                          <li><a href="admin_pins_available.php">Available Pins</a></li>  
                          <li><a href="admin_pins_member.php">Available Member Pins</a></li>                                                                                                                                                                                                      
                          <li><a href="admin_pins_registered.php">Registered Pins</a></li>    
                          <li><a href="admin_pins_registered.php">Registered Member Pins</a></li>                      
                          <li><a href="admin_pins_download.php">Download Pins</a></li>
                          <li><a href="admin_reports_pins.php">Report Pins</a></li>                       
                                                 
                          
                      </ul>
                  </li>
                  <?php
				 }
				  ?>                    

				<?php
				 if($rst['bonus']==1){
					 ?>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-money"></i>
                          <span>Income Summary</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_rebates.php">Income Summary</a></li>
                      </ul>
                  </li>
                 <?php } ?>
                 
					<?php
				 if($rst['gen']==1){
					 ?>                 
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-sitemap"></i>
                          <span>Genealogy</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_unilevel.php">Unilevel Genealogy</a></li>
                            <li><a href="admin_binary.php">Binary Genealogy</a></li>                            
                      </ul>
                  </li>
                 <?php } ?>
					<?php
				 if($rst['encash']==1){
					 ?>                 
                 
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-folder-o"></i>
                          <span>Encash | GC</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_encashment.php">Encashment Request</a></li>
                            <li><a href="admin_encashment_history.php">Encashment History</a></li>  
                            <!--<li><a href="admin_product_pairing_summary.php">Process Product Pairing</a></li>                                                                              
                            <li><a href="admin_product_pairing_history.php">Product Pairing History</a></li>-->  
                            <!-- <li><a href="admin_encashment_gc.php">GC Request</a></li> -->
                            <!-- <li><a href="admin_encashment_gc_history.php">GC History</a></li>                                                                                     -->
                      </ul>
                  </li>
                 <?php } ?>
            <?php
         if($rst['promain']==1){ ?>                 
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-shopping-cart"></i>
                          <span>Product Maintenance</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_category.php">Category Maintenance</a></li>
                            <li><a href="admin_product.php">Product Maintenance</a></li>
                            <li><a href="admin_inventory.php">Product Inventory</a></li>
                            
                      </ul>
                  </li>
        <?php } ?>
        <?php
         if($rst['propins']==1){
           ?>                                      
        <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-barcode"></i>
                          <span>Product Pins</span>
                      </a>
                      <ul class="sub">
                          <!--<li><a href="admin_product_pins_generate.php">Generate Pins</a></li>-->
                          <!--<li><a href="admin_product_pins_generate_member.php">Generate to Member</a></li>-->
                          <!--<li><a href="admin_product_pins_available.php">Available Product Pins</a></li>--> 
                          <li><a href="admin_product_pins_members.php">Create Pins to Member</a></li> 
                          <li><a href="admin_product_pins_sales_summary.php">Pins Sales Summary</a></li>
                          <li><a href="admin_product_pins_reports_sales.php">Sales Report</a></li>   
                          <li><a href="admin_product_pins_member.php">Available Members Pins</a><li>
                          <li><a href="admin_product_pins_registered.php">Used Product Pins</a></li>                       
                          <li><a href="admin_product_pins_download.php">Download Product Pins</a></li>                       
                          
                      </ul>
                  </li>   
                 <?php } ?>  
  
        <?php
         if($rst['sales']==1){
           ?>                 
                  
                  <!-- <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-file-text-o"></i>
                          <span>Product Sales</span>
                      </a>
                      <ul class="sub">
                                                  
                            <li><a href="admin_sales_members.php">Create Sales to Member</a></li>
                            
                            <li><a href="admin_sales_summary.php">Sales Summary</a></li>
                            <li><a href="admin_sales_per_members_summary.php">Per Member Sales</a></li>
                            <li><a href="admin_reports_sales.php">Sales Report</a></li>
                      </ul>
                  </li> -->
        <?php } ?> 
                <?php
                 if($rst['uni']==1){
                     ?>                 
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-gear"></i>
                          <span>Repeat Unilevel</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_uni_summary.php">Process Unilevel</a></li>
                            <li><a href="admin_uni_process_summary.php">Current Unilevel Result</a></li>
                            <li><a href="admin_uni_history.php">Unilevel Result History</a></li>
                            
                      </ul>
                  </li>
                <?php } ?>          
				<?php
				 if($rst['cp']==1){
					 ?>                              
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-gears"></i>
                          <span>Settings and Maintenance</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_cp_encash.php">Encashment Panel</a></li>
                            <li><a href="admin_cp_comm.php">Commission Panel</a></li>
                            <li><a href="admin_cp_uni.php">Unilevel Panel</a></li>
                            <li><a href="admin_entry.php">Entry Settings</a></li>                            
                            <li><a href="admin_supplier.php">Supplier Maintenance </a></li>                            
                            <li><a href="admin_banks.php">Bank Maintenance </a></li>     
                            <li><a href="admin_country.php">Country Exchange Rate</a></li>    
                            <?php if($_SESSION['bs_username'] == 'gateway') { ?>
                                <li><a href="admin_mass_encashment.php">Mass Encashment</a></li>                            
                            <?php } ?>                        
                                                        
                      </ul>
                  </li>
                 <?php } ?>                      
				                

				<?php
				 if($rst['users']==1){
					 ?>                              
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-gears"></i>
                          <span>Users Settings</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_users.php">User Levels</a></li>
                            
                      </ul>
                  </li>
                 <?php } ?>                      
                 
				<?php
				 if($rst['reports']==1){
					 ?>                  
					
				<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-print"></i>
                          <span>Reports</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_main_stat.php">Overall Sales Summary</a></li>
                            <li><a href="admin_top_earners.php">Top Earners</a></li>
                            <li><a href="admin_top_earners_binary.php">Binary Top Per Account</a></li>
                            <li><a href="admin_top_earners_uni.php">Unilevel Top Per Account</a></li>
                            <li><a href="admin_reports_top_group.php">Generate Top Per Group</a></li>
                            <li><a href="admin_top_epoints.php">Top ECash</a></li>
                            <li><a href="admin_top_recruiters.php">Top Recruiters</a></li>
                            <li><a href="admin_top_sales.php">Top Sales</a></li>
                            
                      </ul>
                  </li>    
                 <?php } ?>
				<?php
				 if($rst['web']==1){
					 ?>                  
					<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-rss"></i>
                          <span>Website Management</span>
                      </a>
                      <ul class="sub">
                            <li><a href="admin_news.php">News and Events</a></li>
                            <li><a href="admin_terms.php">Terms and Conditions</a></li>
                            <li><a href="admin_popup.php">Pop-up News</a></li>
                            
                      </ul>
                  </li>
                 <?php } ?>                      

              </ul>
              <!-- sidebar menu end-->
          </div>
         <br /><br /><br /><br />
      </aside>
