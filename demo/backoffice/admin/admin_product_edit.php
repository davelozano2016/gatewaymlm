<?php
require_once('admin_session.php');
$head="Edit Product Category";
if($rst['promain']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_POST['pname'])){
	$Value  = mysql_escape_string($_POST['FCKeditor1']); 
	$ben = mysql_escape_string($_POST['benefits']); 
	$ing = mysql_escape_string($_POST['ingredients']); 
	
		if($Value!=""){		
							$updateSQL="UPDATE tbl_products SET 
                                        pcode='" . mysql_escape_string($_POST['pcode']) ."',														
										pname='" . mysql_escape_string($_POST['pname']) ."',
										description='" . $Value ."',
										benefits='" . $ben ."',
										ingredients='" . $ing ."',										
										category_ctr=" . $_POST['category_ctr'] .",
										price=" . $_POST['price'] .",
										cprice=" . $_POST['cprice'] .",										
										bv=" . $_POST['bv'] .",										
                                        bbv=" . $_POST['bbv'] .",       
										reorder=" . $_POST['reorder'] .",																				
										status='" . $_POST['status']. "' 
										WHERE pctr=" .$_POST['pctr'] ."";					
							mysql_query($updateSQL,$connBS) or die(mysql_error() . $updateSQL);		
							for($x=1;$x<=4;$x++){														
    							$error_upload="";
    							$uploadpath="../products/";
    							$thefile = 'thefile'. $x; 
    							if(isset($_FILES[$thefile]['name'])) {
    								if (basename($_FILES[$thefile]['name']) != '' ){
    									$uploadfile=basename($_FILES[$thefile]['name']);
    									$uploadfileandpath = $uploadpath.$uploadfile;
    										if (is_uploaded_file($_FILES[$thefile]['tmp_name']))
    												{
    												if (move_uploaded_file($_FILES[$thefile]['tmp_name'],$uploadfileandpath)) {
    													chmod("../products/$uploadfile",0777);
    													$updateSQL="UPDATE tbl_products SET img$x='". $uploadfile ."' WHERE pname='". $_POST['pname'] ."'";
    													mysql_query($updateSQL,$connBS) or die(mysql_error() . $updateSQL);									
    													$error_upload="";
    												}
    										}
    								}
    							}	
                            }	
				header("Location: admin_product.php?m=" .$mcrypt->encrypt("System Message: Edit Product Successfully Processed"));
		}else{
				$error_message="Description cannot be an empty field";
		}
}

if(isset($_GET['r'])){
	$strSQL="SELECT * FROM tbl_products WHERE pctr=".$mcrypt->decrypt($_GET['r']);
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error().$strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
						echo $error_message;
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="" enctype="multipart/form-data">
							                 
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Category</label>
                              <div class="col-sm-6">
                                 <select name="category_ctr" class="form-control" id="category_ctr">
								 <?php
									$strSQL="SELECT * FROM tbl_category order by category_name";
									$rsc=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
									$rsct=mysql_fetch_assoc($rsc);
									if(mysql_num_rows($rsc)>0){
										do{
								?>
									<option value="<?php echo $rsct['category_ctr']; ?>" <?php if($rsbt['category_ctr']==$rsct['category_ctr']) { echo "selected"; }  ?>><?php echo $rsct['category_name']; ?></option>
									<?php
										}while($rsct=mysql_fetch_assoc($rsc));
									}
								  ?>
									</select>
                              </div>
                          </div>
                          
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Code</label>
                              <div class="col-sm-6">
                                    <input name="pcode" type="text" class="form-control"  id="pcode" size="80" value="<?php echo $rsbt['pcode']; ?>" /> 

                              </div>
                          </div>
							                      							                          
 						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Product Name</label>
                              <div class="col-sm-6">
                                    <input name="pname" type="text" class="form-control"  id="pname" size="80" value="<?php echo $rsbt['pname']; ?>" /> 

                              </div>
                          </div>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Distributor Price</label>
                              <div class="col-sm-6">
                                    <input name="price" type="text" class="form-control"  id="price" size="80" value="<?php echo $rsbt['price']; ?>" /> 

                              </div>
                          </div>    
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">SRP</label>
                              <div class="col-sm-6">
                                    <input name="cprice" type="text" class="form-control"  id="cprice" size="80" value="<?php echo $rsbt['cprice']; ?>" /> 

                              </div>
                          </div>  
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Unilevel Point Value</label>
                              <div class="col-sm-6">
                                    <input name="bv" type="text" class="form-control"  id="bv" size="80" value="<?php echo $rsbt['bv']; ?>" /> 

                              </div>
                          </div>                
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Binary Point Value</label>
                              <div class="col-sm-6">
                                    <input name="bbv" type="text" class="form-control"  id="bbv" size="80" value="<?php echo $rsbt['bbv']; ?>" /> 

                              </div>
                          </div>    
                        <!-- <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">KEI Rebates</label>
                              <div class="col-sm-6">
                                    <input name="mbv" type="text" class="form-control"  id="mbv" size="80" value="<?php echo $rsbt['mbv']; ?>" /> 

                              </div>
                          </div>                                                   
                                       -->
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Re Ordering Point</label>
                              <div class="col-sm-6">
                                    <input name="reorder" type="text" class="form-control"  id="reorder" size="80" value="<?php echo $rsbt['reorder']; ?>" /> 

                              </div>
                          </div>     
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Details</label>
                              <div class="col-sm-6">
 								<textarea id="FCKeditor1" name="FCKeditor1" style="height: 200px; width: 100%;" class="form-control"><?php echo $rsbt['description']; ?></textarea>
                              </div>
                          </div>      
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Description</label>
                              <div class="col-sm-6">
 								<textarea id="benefits" name="benefits" style="height: 200px; width: 100%;" class="form-control"><?php echo $rsbt['benefits']; ?></textarea>
                              </div>
                          </div>      
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Additional Information</label>
                              <div class="col-sm-6">
 								<textarea id="ingredients" name="ingredients" style="height: 200px; width: 100%;" class="form-control"><?php echo $rsbt['ingredients']; ?></textarea>
                              </div>
                          </div>      
                          
                       <?php
                       for($x=1;$x<=4;$x++){
                       ?> 
						<div class="form-group">
                        
						
                                 
                              <label class="col-sm-3 col-sm-3 control-label">Current Photo</label>
                              <div class="col-sm-6">
						<?php
								if($rsbt["img$x"]=="NA.jpg"){
									$img="../../products/" . $rsbt["img$x"];
								}else{
									$img="../../products/" . $rsbt["img$x"];								
								}						
						?>           
							<a href="<?php echo $img; ?>" target="_blank"><img src="<?php echo $img; ?>" width="60" height="60"  class="img-thumbnail" /></a>
							<input name="thefile<?php echo $x; ?>" type="file"  id="thefile<?php echo $x; ?>"/>  JPEG/PNG/GIF Format                          
                              </div>
                              
                          </div>                                                       

                          <?php } ?>                                                                                                                                                           							
                          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Update Product</a>
                               </div>
                          </div>
 
							 <input name="allow_update" type="hidden" id="allow_update"/>                                                                                                  
								 <input name="pctr" type="hidden" id="pctr" value="<?php echo $rsbt['pctr']; ?>"/>                                                                                                  
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
<script language="javascript" type="text/javascript">
function validate(){
	var errorstr = "";
	errorstr += checkitem(document.frm.pname,"Product Name");	
	if(errorstr==""){
			document.frm.allow_update.value="YES";
			document.frm.submit();
	}else{
		alert(errorstr);
	}
}
function checkitem(item, fdesc) { 
	var errorstr = "";
	if (item.value == "" || checkblanks(item.value) == true)  {
   		if (fdesc != "") { 
   			errorstr = "'" + fdesc + "' is required.\n";
		}
   }
   return errorstr;
 }

// following looks for all blanks in a string - typical of checking//
function checkblanks(item)  {
  var isblank = true;
  for (i = 0; i < item.length; i++) {
    if (item.charAt(i) != " ") {
      isblank = false;  }
  } 
  return isblank;
}

// check for valid emails....
function checkemail(item) {
  var goodmail = true;
  var addr = item.value;
  var invchar = " /:,;";
  var errorstr = "";
  var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
  var emailFilter=/^.+@.+\..{2,3}$/;
  for (i=0; i<invchar.length; i++)  {
	badchar = invchar.charAt(i);
	if (addr.indexOf(badchar,0) > -1)  {
   		goodmail = false;
	}
  }
  atpos = addr.indexOf("@",1);
  if (atpos == -1) {
	goodmail = false;
  }
  else  {
	perpos = addr.indexOf(".",atpos);
	if (perpos == -1)  {
		goodmail = false;
	} else if (perpos + 3 > addr.length)  {
		goodmail = false;
	} else if (addr.match(illegalChars)) {
		goodmail = false;
	} else if (!(emailFilter.test(addr))) { 
		goodmail = false;
    }
  }
  if (goodmail == false) {
    errorstr = "Email address is not valid.\n";
  }
  return errorstr;
}
</script>

	
  

  </body>
</html>
