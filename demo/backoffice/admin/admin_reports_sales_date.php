<?php
include('admin_session.php');
$head="Company Sales Reports";


if($rst['reports']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
					?>
                      <form class="form-horizontal style-form" method="post" name="signupform" action="admin_reports_members_print.php">

                          <h4>Select Date Range</h4>
                          

                            <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Report Type:</label>
                              <div class="col-sm-6">
                                 <select name="report" class="formitems">
                                      <option value="ALL" >All Sales</option>
                                      <option value="EALL" >All Entry Sales</option>        
                                      <option value="ITEM">Repeat Sales Per Product Item</option>
                                      <option value="EITEM">Entry Sales Per Product Item</option>    
                                    </select>
                           
                              </div>
                          </div>  
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">User ID:</label>
                              <div class="col-sm-6">
                               <input name="id_number" type="text" id="id_number" class="form-control" value="NA"/>
                               Note: Put 'NA' if not necessary
                              </div>
                          </div>                           
                            <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Country</label>
                              <div class="col-sm-6">
                                        <select name="country" id="country" class="form-control" onchange="javascript: loadProvince();">
                                                        <?php
                                            $strSQL="SELECT * FROM tbl_countries order by name";
                                            $rs_countries=mysql_query($strSQL,$connBS);
                                            $rst_countries=mysql_fetch_assoc($rs_countries);
                                                do{
                                          ?>
                                                        <option value="<?php echo $rst_countries['code']; ?>" <?php if( $rst_countries['code']=="PH") { echo "selected='selected'"; }  ?>><?php echo strtoupper($rst_countries['name']); ?></option>
                                                        <?php 
                                                } while($rst_countries=mysql_fetch_assoc($rs_countries)); 
                                            ?>
                                                        </select>   
                              </div>
                          </div>                            
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">District/Region</label>
                              <div class="col-sm-6">
                                   <div id="province"><select name="province" id="province" onchange="javascript: loadCity();" class="form-control"> 
                                            <option value="NOPROV" >SELECT DISTRICT/REGION</option>                                                                              
                                                    <?php
                                        $strSQL="SELECT * FROM refprovince WHERE top=1";
                                        $rs_countries=mysql_query($strSQL,$connBS);
                                        $rst_countries=mysql_fetch_assoc($rs_countries);
                                            do{
                                      ?>
                                                    <option value="<?php echo $rst_countries['provCode']; ?>" ><?php echo $rst_countries['provDesc']; ?></option>
                                                    <?php 
                                            } while($rst_countries=mysql_fetch_assoc($rs_countries)); 
                                        ?>
                                    <?php
                                        $strSQL="SELECT * FROM refprovince WHERE top=0 order by provDesc";
                                        $rs_countries=mysql_query($strSQL,$connBS);
                                        $rst_countries=mysql_fetch_assoc($rs_countries);
                                            do{
                                      ?>
                                                    <option value="<?php echo $rst_countries['provCode']; ?>" ><?php echo $rst_countries['provDesc']; ?></option>
                                                    <?php 
                                            } while($rst_countries=mysql_fetch_assoc($rs_countries)); 
                                        ?>                                        
                                       </select></div>   

                              </div>
                          </div>    
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">City</label>
                              <div class="col-sm-6">
                                   <div id="city"><select name="city" class="form-control" id="city">                                
                                                    <option value="NOCITY" >SELECT REGION FIRST</option>
                                                  
                                       </select></div>     
                              </div>
                          </div> 
                           
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Baranggay</label>
                              <div class="col-sm-6">
                                    <div id="brgy"><select name="brgy" class="form-control" id="brgy">                                
                                                    <option value="NOBRGY" >SELECT CITY FIRST</option>
                                                  
                                       </select></div>                        
                              </div>
                          </div>                                               
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Date Start:</label>
                              <div class="col-sm-6">
                                  <select name="UNI_MONTH" class="formitems_drop" id="UNI_MONTH" >
                                    <?php 
                                    $value=date("m");
                                    ?>
                                    <?php  if($value!=0) 
                                    {           ?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="UNI_DAY" class="formitems_drop" id="UNI_DAY">
                                    <?php
                                    $endmonth =31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==date("d")){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="UNI_YEAR" class="formitems_drop" id="UNI_YEAR" >
                                    <?php
                                    for ($i=2017; $i<=date("Y"); $i++) {
                                    if($i==date("Y")){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select>
                           
                              </div>
                          </div>
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Date End:</label>
                              <div class="col-sm-6">
                                  <select name="END_MONTH" class="formitems_drop" id="END_MONTH" >
                                    <?php 
                                    $value=date("m");
                                    ?>
                                    <?php  if($value!=0) 
                                    {           ?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="END_DAY" class="formitems_drop" id="END_DAY">
                                    <?php
                                    $endmonth = 31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==date("d")){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="END_YEAR" class="formitems_drop" id="END_YEAR" >
                                    <?php
                                    for ($i=2017; $i<=date("Y"); $i++) {
                                    if($i==$rsbt['END_YEAR']){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select>
                           
                              </div>

                            
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
                                  <br>
 									<a href="javascript: validate();" class="btn btn-info btn-m">Generate Report</a>
                               </div>
                          </div>                                              
                          
                    <input name="allow_update" type="hidden" value="NO" />		
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

    <script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" type="text/javascript">
        function validate(){
            if(document.signupform.report.value=="ALL"){
                document.signupform.action="admin_reports_sales_print.php";
                document.signupform.submit();
            }else if(document.signupform.report.value=="EALL"){
                document.signupform.action="admin_reports_entry_sales_print.php";
                document.signupform.submit();
            }else if(document.signupform.report.value=="ITEM"){
                document.signupform.action="admin_reports_item_print.php";
                document.signupform.submit();              
            }else if(document.signupform.report.value=="EITEM"){
                document.signupform.action="admin_reports_entry_item_print.php";
                document.signupform.submit();              
            }
        }
	</script>
    <script language="javascript" src="scripts/country.js"></script>
    <script language="javascript" src="scripts/country_ajax.js"></script>   
	

        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>

	
  

  </body>
</html>
