<?php
include('admin_session.php');
if($rst['mem']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
//        rank='" . $_POST['rank'] ."',   
if($_POST['allow_update']=="YES" && isset($_POST['allow_update'])){
	$birth=$_POST['bday_year'] ."-" .$_POST['bday_month'] ."-" . $_POST['bday_day'];
	$updateSQL="UPDATE tbl_members SET
        leader='" . $_POST['leader'] ."',     
      
				firstname='" .checkfield('firstname') ."',
				middlename='" .checkfield('middlename') ."',
				lastname='" .checkfield('lastname') ."',	
				username='" .$_POST['username'] ."',	
				id_code='" .$_POST['username'] ."',	
				password=ENCODE('".$_POST['password']."','$encrypt_password'),					
				nickname='" .checkfield('nickname') ."',
				birthdate='$birth',
				sex='" .$_POST['sex']. "',
				nationality='" .checkfield('nationality') ."',
				civilstatus='" .$_POST['civilstatus'] ."',				
				tin='" . checkfield('tin') ."',	
				country='" . $_POST['country'] ."',							
				address='" . $_POST['address'] ."',
				province='" .checkfield('province') ."',				
				region='" .checkfield('region') ."',								
				district='" .checkfield('district') ."',								
				municipality='" .checkfield('municipality') ."',											
				zipcode='" .checkfield('zipcode') ."',																	
				mobile='" .$_POST['mobile'] ."',
				landline='" .checkfield('landline') ."',
				fax='" .checkfield('fax') ."',				
				beneficiary='" .checkfield('beneficiary') ."',								
				relationship='" .checkfield('relationship') ."',												
				email='" .$_POST['email'] ."',				
				block_status='" .$_POST['block_status'] ."'				
				WHERE id_number=" .$_POST['id'] ."";
	// echo $updateSQL;
	mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);	
	$message="System Message: Account Information Successfully Updated<br><br>";
	
	$id=$_POST['id'];
	$strSQL="SELECT *,DECODE(password,'$encrypt_password') as dpassword FROM tbl_members WHERE id_number=$id";
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);		
	
}elseif(isset($_GET['r'])){
	
	$id=$mcrypt->decrypt($_GET['r']);
	$strSQL="SELECT *,DECODE(password,'$encrypt_password') as dpassword FROM tbl_members WHERE id_number=$id";
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
	
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <section id="container">
        <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <!--header start-->
        <?php
	  include('admin_top.php');
	  ?>
        <!--header end-->

        <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <?php
	  include('admin_menu.php');
	  ?>
        <!--sidebar end-->

        <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <h3>Edit Account Information</h3>
                <a href="admin_account.php">Back to Members Account</a>

                <!-- BASIC FORM ELELEMNTS -->
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
					?>
                            <form class="form-horizontal style-form" method="post" name="signupform" action="">
                                <h3>Personal Information</h3>

                                <?php echo $message; ?>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Member Type:</label>
                                    <div class="col-sm-6">
                                        <select name="leader" class="formitems_drop" id="leader">
                                            <option value="0" <?php if($rsbt['leader']=="0") { echo "selected"; } ?>>
                                                Member</option>
                                            <option value="1" <?php if($rsbt['leader']=="1") { echo "selected"; } ?>>
                                                Stockist</option>
                                            <option value="2" <?php if($rsbt['leader']=="2") { echo "selected"; } ?>>
                                                Depot</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">CD Account:</label>
                                    <div class="col-sm-6">
                                        <?=$rsbt['cd'] != 0 ? 'Yes' : 'No';?>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Member Rank:</label>
                              <div class="col-sm-6">
                                    <select name="rank" class="formitems_drop" id="rank">
                                      <option value="0" <?php if($rsbt['rank']=="0") { echo "selected"; } ?>>Member</option>
                                      <option value="1" <?php if($rsbt['rank']=="1") { echo "selected"; } ?>>Bronze</option>
                                      <option value="2" <?php if($rsbt['rank']=="2") { echo "selected"; } ?>>Silver</option>
                                      <option value="0" <?php if($rsbt['rank']=="3") { echo "selected"; } ?>>Gold</option>
                                      <option value="1" <?php if($rsbt['rank']=="4") { echo "selected"; } ?>>Platinum</option>
                                      <option value="2" <?php if($rsbt['rank']=="5") { echo "selected"; } ?>>Diamond</option>
                                     </select>                                    </div>
                          </div>-->


                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">First Name:</label>
                                    <div class="col-sm-6">
                                        <input name="firstname" type="text" id="firstname" class="form-control"
                                            value="<?php echo $rsbt['firstname']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Middle Name:</label>
                                    <div class="col-sm-6">
                                        <input name="middlename" type="text" id="middlename" class="form-control"
                                            value="<?php echo $rsbt['middlename']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Last Name:</label>
                                    <div class="col-sm-6">
                                        <input name="lastname" type="text" id="lastname" class="form-control"
                                            value="<?php echo $rsbt['lastname']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Nickname:</label>
                                    <div class="col-sm-6">
                                        <input name="nickname" type="text" id="nickname" class="form-control"
                                            value="<?php echo $rsbt['nickname']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Birthdate:</label>
                                    <div class="col-sm-6">
                                        <select name="bday_month" class="formitems_drop" id="bday_month">
                                            <?php 
                                    $arr=split("-",$rsbt['birthdate']);
                                    $value=$_POST['bday_month'];
                                    $value=$arr[1];
                                    ?>
                                            <?php  if($value!=0) 
                                    { 			?>
                                            <option value="01"
                                                <?php if ($value=='01') { echo "selected='selected'"; }?>>January
                                            </option>
                                            <option value="02"
                                                <?php if ($value=='02') { echo "selected='selected'"; }?>>February
                                            </option>
                                            <option value="03"
                                                <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                            <option value="04"
                                                <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                            <option value="05"
                                                <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                            <option value="06"
                                                <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                            <option value="07"
                                                <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                            <option value="08"
                                                <?php if ($value=='08') { echo "selected='selected'"; }?>>August
                                            </option>
                                            <option value="09"
                                                <?php if ($value=='09') { echo "selected='selected'"; }?>>September
                                            </option>
                                            <option value="10"
                                                <?php if ($value=='10') { echo "selected='selected'"; }?>>October
                                            </option>
                                            <option value="11"
                                                <?php if ($value=='11') { echo "selected='selected'"; }?>>November
                                            </option>
                                            <option value="12"
                                                <?php if ($value=='12') { echo "selected='selected'"; }?>>December
                                            </option>
                                            <?php } else { ?>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                            <?php } ?>
                                        </select>
                                        Day
                                        <select name="bday_day" class="formitems_drop" id="bday_day">
                                            <?php
                                    $endmonth = 31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$arr[2]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                        </select>
                                        Year
                                        <select name="bday_year" class="formitems_drop" id="bday_year">
                                            <?php
                                    for ($i=1900; $i<=date("Y")-18; $i++) {
                                    if($i==$arr[0]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Civil Status:</label>
                                    <div class="col-sm-6">
                                        <select name="civilstatus" class="formitems_drop" id="civilstatus">
                                            <option value="S"
                                                <?php if($rsbt['civilstatus']=="S") { echo "selected"; } ?>>Single
                                            </option>
                                            <option value="M"
                                                <?php if($rsbt['civilstatus']=="M") { echo "selected"; } ?>>Married
                                            </option>
                                            <option value="W"
                                                <?php if($rsbt['civilstatus']=="W") { echo "selected"; } ?>>Widowed
                                            </option>
                                            <option value="P"
                                                <?php if($rsbt['civilstatus']=="P") { echo "selected"; } ?>>Separated
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Gender:</label>
                                    <div class="col-sm-6">
                                        <select name="sex" class="formitems_drop" id="sex">
                                            <option value="M" <?php if($rsbt['sex']=="M") { echo "selected"; } ?>>Male
                                            </option>
                                            <option value="F" <?php if($rsbt['sex']=="F") { echo "selected"; } ?>>Female
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Nationality:</label>
                                    <div class="col-sm-6">
                                        <input name="nationality" type="text" id="nationality" class="form-control"
                                            value="<?php echo $rsbt['nationality']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">TIN:</label>
                                    <div class="col-sm-6">
                                        <input name="tin" type="text" id="tin" class="form-control"
                                            value="<?php echo $rsbt['tin']; ?>" />
                                    </div>
                                </div>
                                <h3>Mailing Information</h3>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Address:</label>
                                    <div class="col-sm-6">
                                        <input name="address" type="text" id="address" class="form-control"
                                            value="<?php echo $rsbt['address']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Country:</label>
                                    <div class="col-sm-6">
                                        <select name="country" class="formitems_drop" id="country">
                                            <?php
											$strSQL="SELECT * FROM tbl_countries order by name";
											$rs_countries=mysql_query($strSQL,$connBS);
											$rst_countries=mysql_fetch_assoc($rs_countries);
												do{
										  ?>
                                            <option value="<?php echo $rst_countries['code']; ?>"
                                                <?php if( $rst_countries['code']==$rsbt['country']) { echo "selected='selected'"; }  ?>>
                                                <?php echo $rst_countries['name']; ?></option>
                                            <?php 
												} while($rst_countries=mysql_fetch_assoc($rs_countries)); 
											?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Province/State:</label>
                                    <div class="col-sm-6">
                                        <input name="province" type="text" id="province" class="form-control"
                                            value="<?php echo $rsbt['province']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Region:</label>
                                    <div class="col-sm-6">
                                        <input name="region" type="text" id="region" class="form-control"
                                            value="<?php echo $rsbt['region']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">District:</label>
                                    <div class="col-sm-6">
                                        <input name="district" type="text" id="district" class="form-control"
                                            value="<?php echo $rsbt['district']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Municipailty:</label>
                                    <div class="col-sm-6">
                                        <input name="municipality" type="text" id="municipality" class="form-control"
                                            value="<?php echo $rsbt['municipality']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Zipcode:</label>
                                    <div class="col-sm-6">
                                        <input name="zipcode" type="text" id="zipcode" class="form-control"
                                            value="<?php echo $rsbt['zipcode']; ?>" />
                                    </div>
                                </div>
                                <h3>Contact Information</h3>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Email Address:</label>
                                    <div class="col-sm-6">
                                        <input name="email" type="text" id="email" class="form-control"
                                            value="<?php echo $rsbt['email']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Cellphone No:</label>
                                    <div class="col-sm-6">
                                        <input name="mobile" type="text" id="mobile" class="form-control"
                                            value="<?php echo $rsbt['mobile']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Landline:</label>
                                    <div class="col-sm-6">
                                        <input name="landline" type="text" id="landline" class="form-control"
                                            value="<?php echo $rsbt['landline']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Fax:</label>
                                    <div class="col-sm-6">
                                        <input name="fax" type="text" id="fax" class="form-control"
                                            value="<?php echo $rsbt['fax']; ?>" />
                                    </div>
                                </div>
                                <h3>Beneficiary Information</h3>

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Beneficiary Full Name:</label>
                                    <div class="col-sm-6">
                                        <input name="beneficiary" type="text" id="beneficiary" class="form-control"
                                            value="<?php echo $rsbt['beneficiary']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Relationship:</label>
                                    <div class="col-sm-6">
                                        <input name="relationship" type="text" id="relationship" class="form-control"
                                            value="<?php echo $rsbt['relationship']; ?>" />
                                    </div>
                                </div>
                                <h3>Account Password</h3>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Username:</label>
                                    <div class="col-sm-6">
                                        <input name="username" type="text" id="username" class="form-control"
                                            value="<?php echo $rsbt['id_code']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Current Password:</label>
                                    <div class="col-sm-6">
                                        <input name="password" type="password" id="test1" class="form-control"
                                            value="<?php echo $rsbt['dpassword']; ?>" /><input id="test2"
                                            type="checkbox" />Show password
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Blocked:</label>
                                    <div class="col-sm-6">
                                        <select name="block_status" class="formitems_drop" id="block_status">
                                            <option value="0"
                                                <?php if($rsbt['block_status']=="0") { echo "selected"; } ?>>No</option>
                                            <option value="1"
                                                <?php if($rsbt['block_status']=="1") { echo "selected"; } ?>>Yes
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label"></label>
                                    <div class="col-sm-6">
                                        <a href="javascript: validate_edit();" class="btn btn-info btn-m">Update Account
                                            Information</a>
                                    </div>
                                </div>

                                <input name="pc" type="hidden" value="PLANEP" />
                                <input name="allow_update" type="hidden" value="NO" />
                                <input name="id" type="hidden" value="<?php echo $rsbt['id_number']; ?>" />

                            </form>
                        </div>
                    </div><!-- col-lg-12-->
                </div><!-- /row -->

                <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->

                </div>
                <! --/row -->
            </section>
        </section>

        <!--main content end-->
        <!--footer start-->
        <?php echo include('admin_footer.php'); ?>
        <!--footer end-->
    </section>

    <script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" type="text/javascript">
    function validate_edit() {
        var errorstr = "";
        errorstr += checkitem(document.signupform.username, "UserName");
        errorstr += checkitem(document.signupform.password, "Password");
        errorstr += checkitem(document.signupform.firstname, "First Name");
        errorstr += checkitem(document.signupform.middlename, "Middle Name");
        errorstr += checkitem(document.signupform.lastname, "Last Name");
        errorstr += checkitem(document.signupform.address, "Address");
        errorstr += checkitem(document.signupform.mobile, "Mobile Number");
        errorstr += checkitem(document.signupform.email, "E-Mail");
        errorstr += checkemail(document.signupform.email);
        if (errorstr == "") {
            if (confirm("Continue to Edit Account Information?")) {
                document.signupform.allow_update.value = "YES";
                document.signupform.submit();
            }
        } else {
            alert(errorstr);
        }
    }
    </script>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>
    <script src="assets/js/zabuto_calendar.js"></script>


    <script type="application/javascript">
    $(document).ready(function() {
        $("#date-popover").popover({
            html: true,
            trigger: "manual"
        });
        $("#date-popover").hide();
        $("#date-popover").click(function(e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function() {
                return myDateFunction(this.id, false);
            },
            action_nav: function() {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            },
            legend: [{
                    type: "text",
                    label: "Special event",
                    badge: "00"
                },
                {
                    type: "block",
                    label: "Regular event",
                }
            ]
        });


        //Place this plugin snippet into another file in your applicationb
        (function($) {
            $.toggleShowPassword = function(options) {
                var settings = $.extend({
                    field: "#password",
                    control: "#toggle_show_password",
                }, options);

                var control = $(settings.control);
                var field = $(settings.field)

                control.bind('click', function() {
                    if (control.is(':checked')) {
                        field.prop('type', 'text');
                    } else {
                        field.prop('type', 'password');
                    }
                })
            };
        }(jQuery));

        $.toggleShowPassword({
            field: '#test1',
            control: '#test2'
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
    </script>




</body>

</html>