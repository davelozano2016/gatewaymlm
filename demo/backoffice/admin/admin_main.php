<?php
require_once('admin_session.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

     <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/morris-0.4.3.min.css" rel="stylesheet">

    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Welcome to Gateway</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
						if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?><div class="col-md-3 col-sm-4 mb">
                          <div class="white-panel">
                            <div class="white-header">
                    <h5>Total Payins</h5>
                            </div>
                <h1>        ₱<?php       
                    $strSQL="SELECT SUM(price) as TOTAL FROM tbl_registration_codes_used WHERE free=0 and status=1" ;                       
                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
                    $rsbt=mysql_fetch_assoc($rsb);                                       
                    echo number_format($rsbt['TOTAL'],2); 
        ?></h1><br>
                          </div>                          
                        </div>   
<div class="col-md-3 col-sm-4 mb">
                          <div class="white-panel">
                            <div class="white-header">
                    <h5>Total Product Sales</h5>
                            </div>
                <h1>        ₱<?php       
                  $total=0;
                    $strSQL="SELECT * FROM tbl_pins_carts as tpc INNER JOIN tbl_product_codes_used as tpcu ON tpc.order_type = tpcu.code_no";                       
                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
                    while($row = mysql_fetch_assoc($rsb)) {
                      $total = $row['total'];
                    }
                                                      
                    echo number_format($total,2); 
        ?></h1><br>
                          </div>                          
                        </div>                          
                        <div class="col-md-3 col-sm-4 mb">
                          <div class="white-panel">
                            <div class="white-header">
                    <h5>Total Payouts</h5>
                            </div>
                <h1>        ₱<?php       
                    $strSQL="SELECT SUM(amount) as TOTAL FROM tbl_bonuses" ;                       
                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
                    $rsbt=mysql_fetch_assoc($rsb);                   
                    $debit=$rsbt['TOTAL'];
                    echo number_format($debit,2); 
        ?></h1><br>
                          </div>                          
                        </div>                    
                        <div class="col-md-3 col-sm-4 mb">
                          <div class="white-panel">
                            <div class="white-header">
                    <h5>Total Withdrawals</h5>
                            </div>
                <h1> ₱<?php        
        $strSQL="SELECT SUM(amount) AS D FROM tbl_encashment WHERE request_ctr>0 $pcon";
        $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
        $rsbt=mysql_fetch_assoc($rsb);  
        $pay+=$rsbt['D'];
        echo number_format($rsbt['D'],2) . ""; 
        ?></h1><br>
                          </div>
                        </div>
            <div class="col-md-12 col-sm-4 mb">
                          <div class="white-panel">
                            <div class="white-header">
                    <h5>Available Balance</h5>
                            </div>
                <h1>₱<?php       
                    $strSQL="SELECT SUM(total_income) as TOTAL FROM tbl_members" ;                       
                    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
                    $rsbt=mysql_fetch_assoc($rsb);                   
                    $debit=$rsbt['TOTAL'];
                    echo number_format($debit,2); 
        ?></h1><br>
                          </div>
                        </div>  
                      <form class="form-horizontal style-form" method="post" name="frm" action="">
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Current Date/Time:</label>
                              <div class="col-sm-6">
                                  <?php echo getDateTime(); ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Current IP Address:</label>
                              <div class="col-sm-6">
								<?php echo $_SERVER['REMOTE_ADDR']; ?>                             
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Pending for Pairing:</label>
                              <div class="col-sm-6">
                                <a href="b_cron.php" target="_blank"><?php 
                                     $strSQL="SELECT COUNT(tbpi_ctr) as TOTAL FROM tbl_members WHERE PAIR=1";
                                     $rscp=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                     $rscpt=mysql_fetch_assoc($rscp);
                                     echo "\n" .$rscpt['TOTAL'] ." members left"; 
                                  ?></a>                             
                              </div>
                          </div>  


                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Capped Platinum Account:</label>
                              <div class="col-sm-6">
                                <?php 
                                     $strSQL="SELECT * FROM tbl_members as tm INNER JOIN tbl_binary_capping as tbc ON tm.id_number = tbc.id WHERE tbc.quota >= 80000";
                                     $query = mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                     $total = mysql_num_rows($query);
                                     echo "\n" .$total ." members"; 
                                  ?>                            
                              </div>
                          </div>  
                       
    

                          <form class="form-horizontal style-form" method="post" name="frm" action="">

                           <div id="morris">
                  <div class="row mt">

                      <div class="col-lg-6">
                          <div class="content-panel">
                              <h4>Encoding in a Week</h4>
                              <div class="panel-body">
                                  <div id="hero-bar" class="graph"></div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="content-panel">
                              <h4>Encoding on a Monthly Basis in Year <?php echo date("Y"); ?></h4>
                              <div class="panel-body">
                                  <div id="hero-bar-month" class="graph"></div>
                              </div>
                          </div>
                      </div> 
                                                         
                  </div>
                  
              </div>
                          
                          
    </form>     
	</form><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
          <div class="section col-md-12">
      <?php echo include('admin_footer.php'); ?>

          </div>
      </section>

      <!--main content end-->
      <!--footer start-->
      <!--footer end-->
  </section>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/raphael-min.js"></script>
    <script src="assets/js/morris-0.4.3.min.js"></script>
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
      <script>
      //custom select box


      var Script = function () {

    //morris chart

    $(function () {
      // data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type
      

      

      Morris.Bar({
        element: 'hero-bar',
        data: [
        
        <?php
            $month=date("m");
            $day=date("d");
            $year=date("Y");                                                
            $ntoday=date("D",mktime(0,0,0,$month,$day,$year));  
            $today=$day;
            $add=0;
            $sub=0;
            if($ntoday=="Mon") { $add=6;$sub=0; }
            else if($ntoday=="Tue") { $add=5;$sub=1; }
            else if($ntoday=="Wed") { $add=4;$sub=2; }  
            else if($ntoday=="Thu") { $add=3;$sub=3; }
            else if($ntoday=="Fri") { $add=2;$sub=4; }
            else if($ntoday=="Sat") { $add=1;$sub=5; }
            else if($ntoday=="Sun") { $add=0;$sub=6; }
                                    
            $rangefrom=date("Y-m-d",mktime(0,0,0,$month,$day-$sub,$year));
            $rangeto=date("Y-m-d",mktime(0,0,0,$month,$day+$add,$year));
            $cond="DATEDIFF(date_registered,'$rangefrom')>=0 AND DATEDIFF(date_registered,'$rangeto')<=0";
            $date=preg_split("/-/",$rangefrom);
            for($x=0;$x<=6;$x++){                
                $arr[$x]=$rangefrom=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$x,$date[0]));
                $strSQL="SELECT COUNT(id_number) as TOTAL FROM tbl_members WHERE  DATEDIFF(date_registered,'". $arr[$x]. "')=0";
                $rsr=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                $rsrt=mysql_fetch_assoc($rsr); 
                if($rsrt['TOTAL']>0){            
                    $sales[$x]=$rsrt['TOTAL'];
                }else{
                    $sales[$x]=0;                    
                }
            }    
        ?>
          {device: '<?php echo $arr[0]; ?>', geekbench: <?php echo $sales[0]; ?>},
          {device: '<?php echo $arr[1]; ?>', geekbench: <?php echo $sales[1]; ?>},
          {device: '<?php echo $arr[2]; ?>', geekbench: <?php echo $sales[2]; ?>},
          {device: '<?php echo $arr[3]; ?>', geekbench: <?php echo $sales[3]; ?>},
          {device: '<?php echo $arr[4]; ?>', geekbench: <?php echo $sales[4]; ?>},
          {device: '<?php echo $arr[5]; ?>', geekbench: <?php echo $sales[5]; ?>},
          {device: '<?php echo $arr[6]; ?>', geekbench: <?php echo $sales[6]; ?>}
        ],
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['Total Entry'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        barColors: ['#5185ec','#5185ec','#5185ec','#5185ec','#5185ec','#5185ec','#5185ec'],
      });
      
      Morris.Bar({
        element: 'hero-bar-month',
        data: [
        
        <?php

                                    
            $rangefrom=date("Y-m-d",mktime(0,0,0,$month,$day-$sub,$year));
            $rangeto=date("Y-m-d",mktime(0,0,0,$month,$day+$add,$year));
            $cond="DATEDIFF(date_registered,'$rangefrom')>=0 AND DATEDIFF(date_registered,'$rangeto')<=0";
            $date=preg_split("/-/",$rangefrom);
            for($x=1;$x<=12;$x++){                
                $arr[$x]=$rangefrom=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$x,$date[0]));
                $month=$x;
                $strSQL="SELECT COUNT(id_number) as TOTAL FROM tbl_members WHERE month(date_registered)=$month AND YEAR(date_registered)=".date("Y")."";
               
                $rsr=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                $rsrt=mysql_fetch_assoc($rsr); 
                if($rsrt['TOTAL']>0){            
                    $sales[$x-1]=$rsrt['TOTAL'];
                }else{
                    $sales[$x-1]=0;                    
                }
            }    
        ?>
          {device: 'Jan', geekbench: <?php echo $sales[0]; ?>},
          {device: 'Feb', geekbench: <?php echo $sales[1]; ?>},
          {device: 'Mar', geekbench: <?php echo $sales[2]; ?>},
          {device: 'Apr', geekbench: <?php echo $sales[3]; ?>},
          {device: 'May', geekbench: <?php echo $sales[4]; ?>},
          {device: 'Jun', geekbench: <?php echo $sales[5]; ?>},
          {device: 'Jul', geekbench: <?php echo $sales[6]; ?>},
          {device: 'Aug', geekbench: <?php echo $sales[7]; ?>},
          {device: 'Sep', geekbench: <?php echo $sales[8]; ?>},
          {device: 'Oct', geekbench: <?php echo $sales[9]; ?>},
          {device: 'Nov', geekbench: <?php echo $sales[10]; ?>},
          {device: 'Dec', geekbench: <?php echo $sales[11]; ?>}
          
        ],
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['Total Entry'],
        barColors: ['#B21516'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        barColors: ['#5185ec','#5185ec','#5185ec','#5185ec','#5185ec','#5185ec','#5185ec']
      });      
      
      

      

      $('.code-example').each(function (index, el) {
        eval($(el).text());
      });
    });

}();

      $(function(){
          $('select.styled').customSelect();
      });
  </script>
  </body>
</html>
