<?php
include('admin_session.php');
if($rst['gen']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_GET['r'])){	
	$id=$mcrypt->decrypt($_GET['r']);
	
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $id ." ". $bt; ?> Binary Summary</h3>
          	
            <a href="admin_binary.php">Back to Binary Genealogy</a>
            
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>                          
                                                         
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Binary Type</th>                                          
                                            <th>Total Left</th>  
                                            <th>Total Right</th>  
                                            <th>Waiting Left</th>  
                                            <th>Waiting Right</th>  
                                        </tr>
                                    </thead>
		                         	 <?php
										$strSQL="SELECT * FROM tbl_points_b WHERE id_code=$id";
										$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
										$rsbt=mysql_fetch_assoc($rsb);
										
										if(mysql_num_rows($rsb)>0){
											
											do{
												
												
												
										 ?>                                    
										<tr>
										   <td>Builder</td>                                                                                                                    
										   <td><?php echo number_format($rsbt['LTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['LWAITING'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RWAITING'],0);?></td>                                                                            
                                         </tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}else{
											$strSQL="SELECT * FROM tbl_points_b_wait WHERE id_code=$id";
											$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
											$rsbt=mysql_fetch_assoc($rsb);
											if(mysql_num_rows($rsb)>0){
										 ?> 
                                         
										<tr>
										   <td colspan="5"><a href="admin_binary_restore.php?r=<?php  echo $_GET['r']; ?>&t=<?php echo $mcrypt->encrypt("B"); ?>">Restore Builder Waiting and Activate Account</a></td>                                                                                                                    
                                         </tr>
                                        <?php
											}
									   } ?> 
		                         	 <?php
										$strSQL="SELECT * FROM tbl_points_s WHERE id_code=$id";
										$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
										$rsbt=mysql_fetch_assoc($rsb);
										
										if(mysql_num_rows($rsb)>0){
											
											do{
												
												
												
										 ?>                                    
										<tr>
										   <td>Starter</td>                                                                                                                    
										   <td><?php echo number_format($rsbt['LTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['LWAITING'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RWAITING'],0);?></td>                                                                            
                                         </tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}else{
											$strSQL="SELECT * FROM tbl_points_s_wait WHERE id_code=$id";
											$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
											$rsbt=mysql_fetch_assoc($rsb);
											if(mysql_num_rows($rsb)>0){
										 ?> 
                                         
										<tr>
										   <td colspan="5"><a href="admin_binary_restore.php?r=<?php  echo $_GET['r']; ?>&t=<?php echo $mcrypt->encrypt("S"); ?>">Restore Starter Waiting and Activate Account</a></td>                                                                                                                    
                                         </tr>
                                        <?php
											}
									   } ?> 
									<?php
										$strSQL="SELECT * FROM tbl_points_d WHERE id_code=$id";
										$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
										$rsbt=mysql_fetch_assoc($rsb);
										
										if(mysql_num_rows($rsb)>0){
											
											do{
												
												
												
										 ?>                                    
										<tr>
										   <td>Dealer</td>                                                                                                                    
										   <td><?php echo number_format($rsbt['LTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RTOTAL'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['LWAITING'],0);?></td>                                                                            
										   <td><?php echo number_format($rsbt['RWAITING'],0);?></td>                                                                            
                                         </tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}else{
											$strSQL="SELECT * FROM tbl_points_d_wait WHERE id_code=$id";
											$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
											$rsbt=mysql_fetch_assoc($rsb);
											if(mysql_num_rows($rsb)>0){
										 ?> 
                                         
										<tr>
										   <td colspan="5"><a href="admin_binary_restore.php?r=<?php  echo $_GET['r']; ?>&t=<?php echo $mcrypt->encrypt("D"); ?>">Restore Dealer Waiting and Activate Account</a></td>                                                                                                                    
                                         </tr>
                                        <?php
											}
									   } ?> 
                                           
		                          </tbody>
                                 
		                      </table>
                                                         
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
