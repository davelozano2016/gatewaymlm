<?php
require_once('admin_session.php');
if($rst['codes']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
$strSQL="SELECT  * FROM tbl_control_panel";
$rsc=mysql_query($strSQL,$connBS) or die(mysql_error());
$rsct=mysql_fetch_assoc($rsc);	

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Generate Pins to Member</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="">
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Entry Package Type:</label>
                              <div class="col-sm-6">
                                <select name="seltype" class="formitems">
                                      <option value="PLANB" <?php if($_POST['seltype']=="PLANB") { echo "selected"; } ?>>Builder Package (PHP <?php echo number_format($rsct['BEP'],2); ?>)</option>	
                                      <option value="PLANS" <?php if($_POST['seltype']=="PLANS") { echo "selected"; } ?>>Starter Package (PHP <?php echo number_format($rsct['SEP'],2); ?>)</option>				  			  			
                                      <option value="PLAND" <?php if($_POST['seltype']=="PLAND") { echo "selected"; } ?>>Dealer Package (PHP <?php echo number_format($rsct['DEP'],2); ?>)</option>	
                                      <option value="PLANBSD" <?php if($_POST['seltype']=="PLANBSD") { echo "selected"; } ?>>BSD Package (PHP <?php echo number_format($rsct['BSDEP'],2); ?>)</option>
						      </select>                                          
                                        
                              </div>
                          </div>                      
                          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Commission Type:</label>
                              <div class="col-sm-6">
                                <select name="paytype" class="formitems">
                                      <option value="0" <?php if($_POST['paytype']==0) { echo "selected"; } ?>>Paid Account</option>	
                                      <option value="1" <?php if($_POST['paytype']==1) { echo "selected"; } ?>>Free Account</option>				  			  			
                                      <option value="2" <?php if($_POST['paytype']==2) { echo "selected"; } ?>>CD Account</option>	
						      </select>                                          
                                        
                              </div>
                          </div>     
 						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">User ID:</label>
                              <div class="col-sm-6">
                                  <input name="id_number" type="text" id="id_number" class="form-control" />
                              </div>
                          </div>                                                   
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Enter Number of Codes:</label>
                              <div class="col-sm-6">
                                  <input name="cnt" type="text" id="cnt" class="form-control" /> - Maximum of 500 per generate
                              </div>
                          </div>
                          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Password:</label>
                              <div class="col-sm-6">
                                   <input name="password" type="password" id="password" class="form-control"/>
                              </div>
                          </div>                          							                          
							
									
                             
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Generate Pins</a>
                               </div>
                          </div>                                                                        
	<input type="hidden" name="allow_update"/>                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
<script language="javascript" type="text/javascript">
function validate(){
	var intRegex = /^(0|[1-9][0-9]*)$/;
	var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/i;
	var errorstr = "";
	errorstr += checkitem(document.frm.id_number,"ID Number");		
	errorstr += checkitem(document.frm.cnt,"Number of Codes");	
	errorstr += checkitem(document.frm.password,"Password");
	
	if(!intRegex.test(document.frm.cnt.value) || !floatRegex.test(document.frm.cnt.value)){		
		errorstr+="Invalid Number of Codes\n";
	}else{
		if(document.frm.cnt.value>500){
			errorstr+="Maximum of 500 Codes per generate\n";
		}
	}
	
	if(errorstr==""){
		if(confirm("Continue Generating Membership Codes?")) {
			document.frm.allow_update.value="YES";
			document.frm.action="admin_pins_generate_member_proc.php";
			document.frm.submit();
		}
	}else{
		alert(errorstr);
	}
}
function checkitem(item, fdesc) { 
	var errorstr = "";
	if (item.value == "" || checkblanks(item.value) == true)  {
   		if (fdesc != "") { 
   			errorstr = "'" + fdesc + "' is required.\n";
		}
   }
   return errorstr;
 }

// following looks for all blanks in a string - typical of checking//
function checkblanks(item)  {
  var isblank = true;
  for (i = 0; i < item.length; i++) {
    if (item.charAt(i) != " ") {
      isblank = false;  }
  } 
  return isblank;
}

// check for valid emails....
function checkemail(item) {
  var goodmail = true;
  var addr = item.value;
  var invchar = " /:,;";
  var errorstr = "";
  var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
  var emailFilter=/^.+@.+\..{2,3}$/;
  for (i=0; i<invchar.length; i++)  {
	badchar = invchar.charAt(i);
	if (addr.indexOf(badchar,0) > -1)  {
   		goodmail = false;
	}
  }
  atpos = addr.indexOf("@",1);
  if (atpos == -1) {
	goodmail = false;
  }
  else  {
	perpos = addr.indexOf(".",atpos);
	if (perpos == -1)  {
		goodmail = false;
	} else if (perpos + 3 > addr.length)  {
		goodmail = false;
	} else if (addr.match(illegalChars)) {
		goodmail = false;
	} else if (!(emailFilter.test(addr))) { 
		goodmail = false;
    }
  }
  if (goodmail == false) {
    errorstr = "Email address is not valid.\n";
  }
  return errorstr;
}
</script>

	
  

  </body>
</html>
