<?php
require_once('admin_session.php');
$head="Create News";
if($rst['web']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_POST['allow_update']) && $_POST['allow_update']=="YES"){
	$Value  = mysql_escape_string($_POST['FCKeditor1']); 
	$birth=$_POST['bday_year'] ."-" .$_POST['bday_month'] ."-" . $_POST['bday_day'];
	
	$updateSQL="UPDATE tbl_news SET 
				title='".$_POST['title'] ."',
				content='$Value',
				dateposted='$birth',
				dateencoded='". getDateTime() ."',
				status='". $_POST['status']. "'
				WHERE news_ctr=".$_POST['id'];
	mysql_query($updateSQL,$connBS) or die(mysql_error().$updateSQL);
	
	
	header("Location: admin_news.php?m=".$mcrypt->encrypt("System Message: Updated News Successfully Created"));
}

if(isset($_GET['r'])){
	$strSQL="SELECT * FROM tbl_news WHERE news_ctr=".$mcrypt->decrypt($_GET['r']);
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error().$strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission"));	
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>
                      <form class="form-horizontal style-form" method="post" name="frm" action="">
							                 
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">News Title</label>
                              <div class="col-sm-6">
                                  <input name="title" type="text" id="title" class="form-control" value="<?php echo $rsbt['title']; ?>" /> 
                              </div>
                          </div>
						<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">News Content</label>
                              <div class="col-sm-6">
 								<textarea id="FCKeditor1" name="FCKeditor1" style="height: 200px; width: 100%;" class="form-control"><?php echo $rsbt['content']; ?></textarea>
                              </div>
                          </div>     
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Date Posted:</label>
                              <div class="col-sm-6">
                                  <select name="bday_month" class="formitems_drop" id="bday_month" >
									<?php 
                                    $arr=preg_split("/-/",$rsbt['dateposted']);
                                    $value=$arr[1];
                                    ?>
                                    
                                    <option value="01" <?php if ($value=='01') { echo "selected"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected"; }?>>December</option>
                                   
                                    </select>
                                    Day
                                    <select name="bday_day" class="formitems_drop" id="bday_day">
                                    <?php
                                    $endmonth = 31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$arr[2]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="bday_year" class="formitems_drop" id="bday_year" >
                                    <?php
                                    for ($i=2016; $i<=date("Y"); $i++) {
                                    if($i==$arr[0]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select>
                              </div>
                          </div>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Status:</label>
                              <div class="col-sm-6">
									<select name="status" class="formitems_drop" id="status">
                                      <option value="1" <?php if($rsbt['status']=="1") { echo "selected"; } ?> >Active</option>                                    
                                      <option value="0" <?php if($rsbt['status']=="0") { echo "selected"; } ?> >Inactive</option>
                                     </select>                              
								</div>
                          </div>							 
                          
                                                   
							                      							                          
							
                          
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate();" class="btn btn-info btn-m">Update News</a>
                               </div>
                          </div>
								 <input name="allow_update" type="hidden" id="allow_update"/>  
								 <input name="id" type="hidden" id="id" value="<?php echo $rsbt['news_ctr']; ?>"/>                                                                                                  
                                                                                                                                 
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/ecash_val.js"></script>
	<script language="javascript" src="scripts/ecash_ajax.js"></script>
    
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
<script language="javascript" type="text/javascript">
function validate(){
	var errorstr = "";
	errorstr += checkitem(document.frm.title,"News Title");	
	errorstr += checkitem(document.frm.FCKeditor1,"News Description");	
	
	if(errorstr==""){
			document.frm.allow_update.value="YES";
			document.frm.submit();
	}else{
		alert(errorstr);
	}
}
function checkitem(item, fdesc) { 
	var errorstr = "";
	if (item.value == "" || checkblanks(item.value) == true)  {
   		if (fdesc != "") { 
   			errorstr = "'" + fdesc + "' is required.\n";
		}
   }
   return errorstr;
 }

// following looks for all blanks in a string - typical of checking//
function checkblanks(item)  {
  var isblank = true;
  for (i = 0; i < item.length; i++) {
    if (item.charAt(i) != " ") {
      isblank = false;  }
  } 
  return isblank;
}

// check for valid emails....
function checkemail(item) {
  var goodmail = true;
  var addr = item.value;
  var invchar = " /:,;";
  var errorstr = "";
  var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
  var emailFilter=/^.+@.+\..{2,3}$/;
  for (i=0; i<invchar.length; i++)  {
	badchar = invchar.charAt(i);
	if (addr.indexOf(badchar,0) > -1)  {
   		goodmail = false;
	}
  }
  atpos = addr.indexOf("@",1);
  if (atpos == -1) {
	goodmail = false;
  }
  else  {
	perpos = addr.indexOf(".",atpos);
	if (perpos == -1)  {
		goodmail = false;
	} else if (perpos + 3 > addr.length)  {
		goodmail = false;
	} else if (addr.match(illegalChars)) {
		goodmail = false;
	} else if (!(emailFilter.test(addr))) { 
		goodmail = false;
    }
  }
  if (goodmail == false) {
    errorstr = "Email address is not valid.\n";
  }
  return errorstr;
}
</script>

	
  

  </body>
</html>
