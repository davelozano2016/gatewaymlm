<?php
include('admin_session.php');
if($rst['bonus']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
$cond="";
if($_POST['txtsearch']!=""){
  $cond .=" WHERE " .$_POST['seltype'] ." like '%" . trim($_POST['txtsearch']) . "%'";
} else {
  if($_POST['seltype'] == 'current') {
    $cond .=" WHERE total_income != 0";
  } 
}
$strSQL="SELECT id_number,id_code,id_number,firstname,lastname,total_income,status_type FROM tbl_members $cond";											
if($cond==""){
	$rsc=mysql_query($strSQL,$connBS); 	
	$totalrows=mysql_num_rows($rsc);
	$excessrows=$totalrows % 10;
	$rowcount=$totalrows-$excessrows;
	$rowcount=$totalrows / 10;
	$recordstart=0;
	$recordend=10;
		if(isset($_GET['pagecount'])) {
				$arr3=preg_split("/-/",$mcrypt->decrypt($_GET['range']));
				$recordstart=trim($arr3[0]);
				$recordend=trim($arr3[1]);
		}	
	$strSQL.=" Limit " .$recordstart ."," . $recordend ."";		
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{												
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);

  $queryCD = mysql_query("SELECT status_type FROM tbl_members $cond AND status_type = 'CD'");
	$total_cd=mysql_num_rows($queryCD);

  $queryFREE = mysql_query("SELECT status_type FROM tbl_members $cond AND status_type = 'FREE'");
	$total_free=mysql_num_rows($queryFREE);

  $queryPAID = mysql_query("SELECT status_type FROM tbl_members $cond AND status_type = 'PAID'");
	$total_paid=mysql_num_rows($queryPAID);

 
  
  $strSQL="SELECT SUM(total_income) as TOTAL FROM tbl_members $cond" ;                       
  $query =mysql_query($strSQL,$connBS) or die(mysql_error() . $strSQL);
  $q=mysql_fetch_assoc($query);                   
  $totalC=$q['TOTAL'];

}
//echo $strSQL;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <section id="container">
        <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <!--header start-->
        <?php
	  include('admin_top.php');
	  ?>
        <!--header end-->

        <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <?php
	  include('admin_menu.php');
	  ?>
        <!--sidebar end-->

        <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <h3>Income Summary</h3>

                <!-- BASIC FORM ELELEMNTS -->
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="content-panel">
                                    <?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>
                                    <form name="sfrm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                        <div align="left">&nbsp;&nbsp;&nbsp;Search By:
                                            <script language="javascript" type="text/javascript">
                                            function search_check() {
                                                document.sfrm.submit();
                                            }
                                            </script>
                                            <select name="seltype" id="triggerSearch" class="formitems">
                                                <option value="id_number"
                                                    <?php if($_POST['seltype']=="id_number") { echo "selected"; } ?>>
                                                    User ID</option>
                                                <option value="username"
                                                    <?php if($_POST['seltype']=="username") { echo "selected"; } ?>>
                                                    Username</option>
                                                <option value="firstname"
                                                    <?php if($_POST['seltype']=="firstname") { echo "selected"; } ?>>
                                                    First Name</option>
                                                <option value="lastname"
                                                    <?php if($_POST['seltype']=="lastname") { echo "selected"; } ?>>Last
                                                    Name</option>
                                                <option value="current"
                                                    <?php if($_POST['seltype']=="current") { echo "selected"; } ?>>
                                                    Current</option>

                                            </select>
                                            <?php if($_POST['seltype'] == 'current') { ?>
                                            <input name="txtsearch" type="text" hidden class="formitems" id="txtsearch"
                                                value="<?php echo $_POST['txtsearch']; ?>">
                                            <?php } else { ?>
                                            <input name="txtsearch" type="text" class="formitems" id="txtsearch"
                                                value="<?php echo $_POST['txtsearch']; ?>">
                                            <?php } ?>
                                            <input name="Button" type="button" class="formbutton_team"
                                                onClick="javascript: search_check();" value="Search Record">
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>">View All Records</a>
                                    </form>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>UserID</th>
                                                <th>Username</th>
                                                <th>Name</th>
                                                <th>Direct</th>
                                                <th>Pair</th>
                                                <th>Unilevel</th>
                                                <th>Leadership</th>
                                                <th>Entry Type</th>
                                                <th>Total</th>
                                                <th>Withdraw</th>
                                                <!--<th>Purchase</th>-->
                                                <th>Current</th>
                                            </tr>
                                        </thead>
                                        <?php
								  			
                                          if(mysql_num_rows($rsb)>0){
                                          
                                            do{
                                              $name=$rsbt['firstname'] . " ".$rsbt['lastname'];
                                              $debit=0;
                                              $total_current += $rsbt['total_income'];
                      
                                          ?>
                                        <tr>
                                            <td><?php echo $rsbt['id_number'];?></td>
                                            <td><?php echo $rsbt['id_code'];?></td>

                                            <td><?php echo $name;?></td>
                                            <td>
                                                <a
                                                    href="admin_rebates_ss.php?r=<?php echo $mcrypt->encrypt($rsbt['id_number']); ?>&b=<?php echo $mcrypt->encrypt("DSI"); ?>">
                                                    <?php
                                              $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like '%DSI%' AND recepient_code='" .$rsbt['id_number'] ."' and order_no!='HOLD'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              $debit+=$rsdt['D'];  
                                              echo number_format($rsdt['D'],2); ?></a>
                                            </td>
                                            <td>
                                                <a
                                                    href="admin_rebates_ss.php?r=<?php echo $mcrypt->encrypt($rsbt['id_number']); ?>&b=<?php echo $mcrypt->encrypt("PAIR"); ?>">
                                                    <?php
                                              $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like '%PAIR%' AND recepient_code='" .$rsbt['id_number'] ."' and order_no!='HOLD'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              $debit+=$rsdt['D'];  
                                              echo number_format($rsdt['D'],2); ?></a>
                                            </td>
                                            <td>
                                                <a
                                                    href="admin_rebates_ss.php?r=<?php echo $mcrypt->encrypt($rsbt['id_number']); ?>&b=<?php echo $mcrypt->encrypt("REPEAT"); ?>">
                                                    <?php
                                              $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like '%REPEAT%' AND recepient_code='" .$rsbt['id_number'] ."' and order_no!='HOLD'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              $debit+=$rsdt['D'];  
                                              echo number_format($rsdt['D'],2); ?></a>
                                            </td>

                                            <td>
                                                <?php
                                              $strSQL="SELECT SUM(amount) AS D FROM tbl_bonuses WHERE bonus_type like '%LEADER%' AND recepient_code='" .$rsbt['id_number'] ."' and order_no!='HOLD'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              $debit+=$rsdt['D'];  
                                              echo number_format($rsdt['D'],2); ?>
                                            </td>
                                            <td><?=$rsbt['status_type']?></td>

                                            <td><?php echo number_format($debit,2); ?></td>
                                            <td>
                                                <?php
                                              $strSQL="SELECT SUM(amount) AS U FROM tbl_encashment WHERE id_code='" .$rsbt['id_number'] ."' and e_code!='NA'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              $credit+=$rsdt['U'];  
                                              echo "-".number_format($rsdt['U'],2); ?>
                                            </td>
                                            <!--<td>
                                            <?php
                                              $strSQL="SELECT SUM(ecash) AS D FROM tbl_points_history WHERE recepient_user='" .$rsbt['id_number'] ."'";
                                              $rsd=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
                                              $rsdt=mysql_fetch_assoc($rsd);  
                                              echo "-".number_format($rsdt['D'],2); ?>
                                          </td>-->

                                            <td><?php echo number_format($rsbt['total_income'],2); ?></td>


                                        </tr>
                                        <?php
                     $grand_total  += $debit;
                        }while($rsbt=mysql_fetch_assoc($rsb));
                          
                      }
                       ?>


                                        </tbody>
                                        <?php if($cond != '') { ?>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="text-align:right" colspan=3>FREE: <?=$total_free?> | CD:
                                                    <?=$total_cd?> | PAID: <?=$total_paid?></td>
                                                <td><?=number_format($grand_total,2)?></td>
                                                <td>-<?=number_format($credit,2)?></td>
                                                <td>
                                                    <?= number_format($totalC,2) ?> </td>
                                            </tr>
                                        </tfoot>
                                        <?php }?>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;

                                    <?php
                        if($cond==""){
                           include ('admin_paging.php');
                        }
                      ?>
                                </div>
                                <! --/content-panel -->
                            </div><!-- /col-md-12 -->
                        </div><!-- /row -->

                        <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->

                    </div>
                    <! --/row -->
            </section>
        </section>

        <!--main content end-->
        <!--footer start-->
        <?php echo include('admin_footer.php'); ?>
        <!--footer end-->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>



    <script>
    $('#triggerSearch').change(e => {
        var search = $('#triggerSearch').val();
        if (search == 'current') {
            $('#txtsearch').val('').attr('hidden', true);
        } else {
            $('#txtsearch').attr('hidden', false);
        }
    })
    </script>





</body>

</html>