<?php
include('admin_session.php');
if($rst['sales']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
$order_no=$mcrypt->decrypt($_GET['r']);
$order_no=mysql_escape_string($order_no);
$order_no=substr($order_no,0,20);
$strSQL="SELECT * FROM tbl_reservations WHERE order_type='$order_no'";
$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
if(mysql_num_rows($rsb)==0){
	header("Location: admin_sales_summary.php");
}

if($_POST['allow_update']=="YES" && isset($_POST['allow_update'])){
	$birth=$_POST['bday_year'] ."-" .$_POST['bday_month'] ."-" . $_POST['bday_day'] . " ". $_POST['time'];
	$updateSQL="UPDATE tbl_reservations SET
				date_process='$birth'
				WHERE reservation_ctr=" .$_POST['id'] ."";
	mysql_query($updateSQL,$connBS) or die(mysql_error(). $updateSQL);	
	$message="System Message: Account Information Successfully Updated<br><br>";
	
	$id=$_POST['id'];
	$strSQL="SELECT * FROM tbl_reservations WHERE reservation_ctr='$id'";
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
	
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Modify Date Process</h3>
            <a href="admin_sales_summary.php">Back to Sales Summary</a>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  <?php if(isset($_GET['e'])){
				  			echo $mcrypt->decrypt($_GET['e']); 
				  		}
					?>
                      <form class="form-horizontal style-form" method="post" name="signupform" action="">

                                      <?php echo $message; ?>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Order Number:</label>
                              <div class="col-sm-6">
 									 <?php echo $rsbt['order_type']; ?>
                               </div>
                          </div>                                         
                          <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Date Process:</label>
                              <div class="col-sm-6">
                                  <select name="bday_month" class="formitems_drop" id="bday_month" >
									<?php 
                                    $arr=split("-",$rsbt['date_process']);
                                    $arr2=split(" ",$rsbt['date_process']);
									
                                    $value=$_POST['bday_month'];
                                    $value=$arr[1];
                                    ?>
                                    <?php  if($value!=0) 
                                    { 			?>
                                    <option value="01" <?php if ($value=='01') { echo "selected='selected'"; }?>>January</option>
                                    <option value="02" <?php if ($value=='02') { echo "selected='selected'"; }?>>February</option>
                                    <option value="03" <?php if ($value=='03') { echo "selected='selected'"; }?>>March</option>
                                    <option value="04" <?php if ($value=='04') { echo "selected='selected'"; }?>>April</option>
                                    <option value="05" <?php if ($value=='05') { echo "selected='selected'"; }?>>May</option>
                                    <option value="06" <?php if ($value=='06') { echo "selected='selected'"; }?>>June</option>
                                    <option value="07" <?php if ($value=='07') { echo "selected='selected'"; }?>>July</option>
                                    <option value="08" <?php if ($value=='08') { echo "selected='selected'"; }?>>August</option>
                                    <option value="09" <?php if ($value=='09') { echo "selected='selected'"; }?>>September</option>
                                    <option value="10" <?php if ($value=='10') { echo "selected='selected'"; }?>>October</option>
                                    <option value="11" <?php if ($value=='11') { echo "selected='selected'"; }?>>November</option>
                                    <option value="12" <?php if ($value=='12') { echo "selected='selected'"; }?>>December</option>
                                    <?php } else { ?>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                    <?php } ?>
                                    </select>
                                    Day
                                    <select name="bday_day" class="formitems_drop" id="bday_day">
                                    <?php
                                    $endmonth = 31;
                                    for ($i=1; $i<=$endmonth; $i++){
                                    if($i==$arr[2]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    } 
                                    ?>
                                    </select>
                                    Year
                                    <select name="bday_year" class="formitems_drop" id="bday_year" >
                                    <?php
                                    for ($i=2016; $i<=date("Y"); $i++) {
                                    if($i==$arr[0]){
                                    echo "<option value=" . strval($i) . " selected>" . strval($i) . "</option>";
                                    }else{
                                    echo "<option value=" . strval($i) . ">" . strval($i) . "</option>";
                                    }
                                    }
                                    
                                    ?>
                                                    </select>
                              </div>
                          </div>
							<div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label"></label>
                              <div class="col-sm-6">
 									<a href="javascript: validate_edit();" class="btn btn-info btn-m">Update Date Order</a>
                               </div>
                          </div>                                              
                          
                     <input name="time" type="hidden" value="<?php echo $arr2[1]; ?>" />		
                    <input name="allow_update" type="hidden" value="NO" />		
                    <input name="id" type="hidden" value="<?php echo $rsbt['reservation_ctr']; ?>" />		
                                        
	</form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>

	<script language="javascript" src="scripts/formvalidations.js"></script>
    <script language="javascript" type="text/javascript">
    function validate_edit()
    {
        var errorstr = "";
        if(errorstr=="") {	
            if(confirm("Continue to Edit Date Process?")){
                document.signupform.allow_update.value="YES";
                document.signupform.submit();
            }
        }else{
            alert(errorstr);		
        }
    }
    </script>   

        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>

	
  

  </body>
</html>
