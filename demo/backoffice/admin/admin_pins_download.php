<?php
include('admin_session.php');
if($rst['codes']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}

$strSQL="SELECT code_no FROM tbl_registration_codes WHERE status=0 and purchase_code='' GROUP BY code_no ";
$rsb=mysql_query($strSQL,$connBS); 	
$rsbt=mysql_fetch_assoc($rsb);
$totalrows=mysql_num_rows($rsb);
$excessrows=$totalrows % 25;
$rowcount=$totalrows-$excessrows;
$rowcount=$totalrows / 25;
$recordstart=0;
$recordend=25;
	if(isset($_GET['pagecount'])) {
			$arr3=split("-",$mcrypt->decrypt($_GET['range']));
			$recordstart=trim($arr3[0]);
			$recordend=trim($arr3[1]);
	}	
$rsb=mysql_query($strSQL . " Limit " .$recordstart ."," . $recordend ."",$connBS) or die(mysql_error(). $strSQL);
$rsbt=mysql_fetch_assoc($rsb);
$rst_count=mysql_num_rows($rsb);  ;
//echo " " . $rst_count ." Record(s)";
?>	
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>Registered Pins</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
					?>                          
                                                       
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Gen Code</th>                                        
                                            <th>Total Pins</th>                                                                                    
                                            <th>Download in Excel Format</th>
                                        </tr>
                                    </thead>
		                          <?php
								  			
											if(mysql_num_rows($rsb)>0){
											
											do{												
												$strSQL="SELECT COUNT(code_no) as TOTAL FROM tbl_registration_codes WHERE code_no='". $rsbt['code_no'] ."'";
												$rsm=mysql_query($strSQL,$connBS); 							
												$rsmt=mysql_fetch_assoc($rsm);
												
										 ?>                                    
																			<tr>
										   <td><?php echo $rsbt['code_no'];?></td>                                                                            
										   <td><?php echo number_format($rsmt['TOTAL'],0); ?></td>                                                                                                                       
										   <td><?php 										   			
										   			echo "<a href='admin_pins_export_csv.php?r=".$mcrypt->encrypt($rsbt['code_no'])."'>Download</a>"; 
												?> 
											</td>
                                           
										 </tr>
									 <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										 ?>   
		                          </tbody>
                                 
		                      </table>
                                        &nbsp;&nbsp;&nbsp;
                                        
										<?php
											if($cond==""){
 												include ('admin_paging.php');
											}
										?>                    
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php echo include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
