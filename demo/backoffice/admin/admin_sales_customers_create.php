<?php
include('admin_session.php');
$head="New Members Order";
if($rst['sales']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
if(isset($_POST['allow']) && $_POST['allow']=="YES"){
    $delSQL="DELETE FROM tbl_carts WHERE username='". $_POST['username'] ."' AND order_type='NA'";
    mysql_query($delSQL,$connBS) or die(mysql_error(). $delSQL);                            
    for($x=1;$x<=$_POST['cnt'];$x++){
        if($_POST['t'.$x]>0){   
            $item_count=$_POST['t'.$x];
            $bv=$_POST['t'.$x] * $_POST['b'.$x];
            $price=$_POST['t'.$x] * $_POST['p'.$x];
            $pctr=$_POST['c'.$x];       
            $insertSQL="INSERT IGNORE INTO tbl_carts(username,pctr,item_count,bv,total,date_added)VALUES('". $_POST['username']."',$pctr,$item_count,$bv,$price,'". getDateTime()."')";
            mysql_query($insertSQL,$connBS) or die(mysql_error(). $insertSQL);                  
        }   
    }   
    $_SESSION['discount']=$_POST['discount'];
    header("Location: admin_sales_customers_process.php?u=" .$mcrypt->encrypt($_POST['username']). "&order_entry=".$_POST['order_entry']);
}
if(isset($_GET['r'])){  
    $id=$mcrypt->decrypt($_GET['r']);
    $strSQL="SELECT * FROM tbl_customers WHERE cust_id='$id'";                                         
    $rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
    $rsbt=mysql_fetch_assoc($rsb);        
}else{
    header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: Invalid Data Submission")); 
}
$strSQL="SELECT tbl_products.*,tbl_category.* FROM tbl_products,tbl_category WHERE tbl_products.category_ctr=tbl_category.category_ctr AND tbl_products.item_cnt>tbl_products.reorder" . $cond . " ORDER BY tbl_products.pname";
$rse=mysql_query($strSQL) or die(mysql_error(). $strSQL);
$rset=mysql_fetch_assoc($rse);
$rset_count=mysql_num_rows($rse);

if($_SESSION['discount']>0){
    $discount=$_SESSION['discount'];    
}else{
    $discount=0;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
	  include('admin_top.php');
	  ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
	  include('admin_menu.php');
	  ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><?php echo $head; ?></h3>
          	<a href="admin_sales_customers.php">Back to Create Order as Customer</a>
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                 <div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
					<?php if(isset($_GET['m'])){
				  			echo $mcrypt->decrypt($_GET['m']); 
				  		}
                         echo $errorstr;
					?>                          
                             <form name="frm" method="post" action="">
                                    <input type="hidden" name="username" value="<?php echo $rsbt['cust_id']; ?>" />
                                    <input type="hidden" name="allow" value="" />
                                    <input type="hidden" name="credit" value="<?php echo $rsbt['total_fund']; ?>" />
                            
                                  <div align="left">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer ID: <strong><?php echo $rsbt['cust_id']; ?></strong><br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name: <strong><?php echo $rsbt['fullname']; ?></strong><br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Discount:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="discount" value="<?php echo $discount; ?>" size="5" style="text-align: right;" onkeyup="javascript: compute(<?php echo $rset_count; ?>);"> %<br>        
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment Type: <select name="order_entry" class="formitems" id="order_entry">
                                                                                                        <option value="CASH">CASH</option>
                                                                                                        <option value="CHECQUE">CHECQUE</option>
                                                                                                        <option value="BANK">BANK</option>                                                                                                        
                                                                                                       </select><br /><br />
                                    
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript: setzero(<?php echo $rset_count; ?>);">Reset Order </a>  &nbsp;&nbsp;
                                  </p>	                          
                               <table class="table table-hover">
                                    <thead>
                                        <tr>
                                          <th>Code</th>   
                                          <th>Product Name</th>
                                          <th>Category</th>                  
                                          <th>Price</td>                
                                          <th>Stocks</div></th>                                 
                                          <th>Enter Order</th>                                                                                                                                   
                                                                                                                                                                                
                                        </tr>
                                    </thead>
		                          <?php 
        
                                            if($rset_count>0){
                                                    $count=1;
                                                    do{
                                                        $strSQL="SELECT * FROM tbl_carts WHERE pctr=". $rset['pctr'] ." AND username='" . base64_decode($_GET['u']). "' and order_type='NA'";
                                                        $rsr=mysql_query($strSQL) or die(mysql_error(). $strSQL);
                                                        $rsrt=mysql_fetch_assoc($rsr);
                                                        if(mysql_num_rows($rsr)>0){
                                                            $value=$rsrt['item_count'];
                                                        }else{
                                                            $value="";
                                                        }
                                                        
                                                        
                                                        $price=$rset['cprice'];
                                                            
                                        ?>
                                              <input type="hidden" name="cnt" value="<?php echo $count; ?>" />
                                              <tr id="tr<?php echo $count; ?>">
                                                  <td><?php echo $rset['pcode']; ?></td>
                                                  <td><?php echo $rset['pname']; ?></td>
                                                  <td><?php echo $rset['category_name']; ?></td>                                                  
                                                  <td><?php echo number_format($price,2); ?></td>
                                                  <td><?php echo number_format($rset['item_cnt'],0); ?></td>                                                  
                                                  <td>
                                                   <input type="text" name="t<?php echo $count;?>" class="formitems"  value="<?php echo $value; ?>" size="10" style="text-align:right;" onkeyup="javascript: compute(<?php echo $rset_count; ?>);" />
                                                   <input type="hidden" name="c<?php echo $count; ?>" value="<?php echo $rset['pctr']; ?>" />
                                                   <input type="hidden" name="p<?php echo $count; ?>" value="<?php echo $price; ?>" />                 
                                                   <input type="hidden" name="b<?php echo $count; ?>" value="<?php echo $rset['bv']; ?>" />
                                                   <input type="hidden" name="i<?php echo $count; ?>" value="<?php echo $rset['item_cnt']; ?>" />                                                                                                     
                                                 </td>             
                                              
                                                </tr>
                                              <?php
                                        $count++;
                                        }while($rset=mysql_fetch_assoc($rse));
                                        }
                                        ?>
		                          </tbody>
		                          
                                 
		                      </table>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="t_items">Total Items: <strong><?php echo number_format($amount,0); ?></strong></div>  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="t_sub">Sub Total: <strong><?php echo number_format($amount,2);  ?></strong></div>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="t_disc">Discount: <strong><?php echo number_format($amount,2);  ?></strong></div>     
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="t_price">Total Amount: <strong><?php echo number_format($amount,2);  ?></strong></div>     
		                      <br>
		                                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="formbutton_team" value="Create Order" onclick="javascript: ask(<?php echo $rset_count; ?>);" /><br /><br />

		                      </form>
                            <script language="javascript" type="text/javascript" src="scripts/shop_admin.js"></script>

	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->      	
          	</div><!-- /row -->
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
              </div><! --/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <?php  include('admin_footer.php'); ?>
      <!--footer end-->
  </section>
        <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>

<script language="javascript" type="text/javascript">
    function setzero(ctr){
        for(x=1;x<=ctr;x++){
            document.forms.frm.elements['t'+x].value=0;
        }       
    }
    function validate(ctr){
        var errorstr="";
        var intRegex = /^(0|[1-9][0-9]*)$/;
        var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/i;       
        var x;
        var cnt=0;
        if(document.frm.password.value==""){
            errorstr="Administrator Password is required\n";
        }
        if(document.frm.inv_refno.value==""){
            errorstr="Delivery Invoice Number is required\n";
        }

        if(document.frm.inv_remarks.value==""){
            errorstr="Delivery Remarks is required\n";
        }
        
        for(x=1;x<=ctr;x++){
            if(!intRegex.test(document.forms.frm.elements['t'+x].value) || !floatRegex.test(document.forms.frm.elements['t'+x].value)){
                errorstr+="Invalid Format on Product # " + x + "\n";
            }
            if(document.forms.frm.elements['t'+x].value==""){
                errorstr+="Field should not be empty on Product # " + x + ". Please type Zero(0) by default\n";
            }
            if(parseInt(document.forms.frm.elements['t'+x].value)>0){
                cnt++;
            }

        }
        if(cnt==0){
            errorstr+="Please enter stocks to at least one product\n";      
        }
        if(errorstr==""){
            if(confirm("Continue New Delivery?")){
                document.frm.allow.value="YES";
                document.frm.submit();
            }
        }else{
            alert(errorstr);
        }
        
    }
</script>
    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

	

	
  

  </body>
</html>
