<?php
include('admin_session.php');
$head="Members Account Information";
if($rst['mem']!=1){
	header("Location: admin_main.php?m=" .$mcrypt->encrypt("System Message: This feature is not available in your User Account"));
}
$cond="";
if($_POST['txtsearch']!=""){
		$cond .=" WHERE " .$_POST['seltype'] ." like '%" . trim($_POST['txtsearch']) . "%'";
} 

if($_POST['seltype'] === 'CD')  {
  $cdAccount = $_POST['selectCD'];

  if($cdAccount === 'Yes') {
    $cond .= " WHERE cd != 0";
  } else {
    $cond .= " WHERE cd = 0";
  }
} 

$strSQL="SELECT date_registered,id_number,id_code,firstname,lastname,mobile,email,DECODE(password,'$encrypt_password') as dpassword,member_type,sponsor_code,upline_code,upline_pos,country,cd,rank FROM tbl_members $cond";	
if($cond==""){
	$rsb=mysql_query("SELECT COUNT(id_number) as TOTAL FROM tbl_members",$connBS); 	
	$rsbt=mysql_fetch_assoc($rsb);
	$totalrows=$rsbt['TOTAL'];
	$excessrows=$totalrows % 10;
	$rowcount=$totalrows-$excessrows;
	$rowcount=$totalrows / 10;
	$recordstart=0;
	$recordend=10;
		if(isset($_GET['pagecount'])) {
				$arr3=preg_split("/-/",$mcrypt->decrypt($_GET['range']));
				$recordstart=trim($arr3[0]);
				$recordend=trim($arr3[1]);
		}	
	$strSQL.=" Limit " .$recordstart ."," . $recordend ."";		
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}else{												
	$rsb=mysql_query($strSQL,$connBS) or die(mysql_error(). $strSQL);
	$rsbt=mysql_fetch_assoc($rsb);
}
//echo $strSQL;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <section id="container">
        <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <!--header start-->
        <?php
	  include('admin_top.php');
	  ?>
        <!--header end-->

        <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <?php
	  include('admin_menu.php');
	  ?>
        <!--sidebar end-->

        <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <h3><?php echo $head; ?></h3>

                <!-- BASIC FORM ELELEMNTS -->
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="content-panel">
                                    <?php 
                                      if(isset($_GET['m'])){
                                        echo $mcrypt->decrypt($_GET['m']); 
                                      }
                                    ?>
                                    <form name="sfrm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                        <div align="left">&nbsp;&nbsp;&nbsp;Search By:
                                            <script language="javascript" type="text/javascript">
                                            function search_check() {
                                                document.sfrm.submit();
                                            }
                                            </script>
                                            <select name="seltype" id="searchQuery" class="formitems">
                                                <option value="id_number"
                                                    <?php if($_POST['seltype']=="id_number") { echo "selected"; } ?>>
                                                    User ID</option>
                                                <option value="id_code"
                                                    <?php if($_POST['seltype']=="id_code") { echo "selected"; } ?>>
                                                    Username</option>
                                                <option value="firstname"
                                                    <?php if($_POST['seltype']=="firstname") { echo "selected"; } ?>>
                                                    First Name</option>
                                                <option value="lastname"
                                                    <?php if($_POST['seltype']=="lastname") { echo "selected"; } ?>>Last
                                                    Name</option>
                                                <option value="CD"
                                                    <?php if($_POST['seltype']=="CD") { echo "selected"; } ?>>CD Account
                                                </option>

                                            </select>
                                            <?php 
                                              if($_POST['seltype'] == 'CD') { ?>
                                              <select name="selectCD" id="searchCDAccount" >
                                                  <option value="Yes" <?=$_POST['selectCD'] == 'Yes' ? 'selected' : ''?>>Yes</option>
                                                  <option value="No" <?=$_POST['selectCD'] == 'No' ? 'selected' : ''?>>No</option>
                                              </select>

                                              <input name="txtsearch" type="text" class="formitems" id="txtsearch"
                                                  value="<?php echo $_POST['txtsearch']; ?>" hidden>

                                              <?php } else { ?>
                                              
                                              <input name="txtsearch" type="text" class="formitems" id="txtsearch"
                                                  value="<?php echo $_POST['txtsearch']; ?>">

                                                  <select name="selectCD" id="searchCDAccount" hidden>
                                                  <option value="Yes" <?=$_POST['selectCD'] == 'Yes' ? 'selected' : ''?>>Yes</option>
                                                  <option value="No" <?=$_POST['selectCD'] == 'No' ? 'selected' : ''?>>No</option>
                                              </select>
                                            <?php } ?>

                                            <input name="Button" type="button" class="formbutton_team"
                                                onClick="javascript: search_check();" value="Search Record">
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>">View All Records</a>
                                    </form>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Date Registration</th>
                                                <th>User ID</th>
                                                <th>Username</th>
                                                <!--th>Password</th-->
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Package</th>
                                                <!-- <th>Rank</th> -->
                                                <th>Sponsor</th>
                                                <th>Upline</th>
                                                <th>Position</th>
                                                <th>CD</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <?php
								  			
											if(mysql_num_rows($rsb)>0){
											
											do{
												$name=$rsbt['firstname'] . " ".$rsbt['lastname'];
												
												
												
										 ?>
                                        <tr>
                                            <td><?php echo $rsbt['date_registered'];?></td>
                                            <td><?php echo $rsbt['id_number'];?></td>
                                            <td><?php echo $rsbt['id_code'];?></td>
                                            <!--td><?php echo $rsbt['dpassword'];?></td-->
                                            <td><?php echo $name;?></td>
                                            <td><?php echo $rsbt['mobile'];?></td>
                                            <td><?php echo $rsbt['email'];?></td>
                                            <td><?php echo loadPosition($rsbt['member_type']);?></td>
                                            <!-- <td><?php echo loadRank($rsbt['rank']);?></td> -->
                                            <td><?php echo $rsbt['sponsor_code'];?></td>
                                            <td><?php echo $rsbt['upline_code'];?></td>
                                            <td><?php echo $rsbt['upline_pos'];?></td>
                                            <td><?php echo $rsbt['cd'] != 0 ? 'Yes' : 'No';?> </td>
                                            <td><?php 
										   			echo "<a href='admin_account_edit.php?r=".$mcrypt->encrypt($rsbt['id_number'])."'>Edit</a>"; 
												?>
                                            </td>
                                        </tr>
                                        <?php
											}while($rsbt=mysql_fetch_assoc($rsb));
										}
										 ?>
                                        </tbody>

                                    </table>
                                    &nbsp;&nbsp;&nbsp;

                                    <?php
											if($cond==""){
 												include ('admin_paging.php');
											}
										?>
                                </div>
                                <! --/content-panel -->
                            </div><!-- /col-md-12 -->
                        </div><!-- /row -->

                        <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->

                    </div>
                    <! --/row -->
            </section>
        </section>

        <!--main content end-->
        <!--footer start-->
        <?php echo include('admin_footer.php'); ?>
        <!--footer end-->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>
    <script>
    $('#searchQuery').change(e => {
        //
        var searchQuery = $('#searchQuery').val();
        if (searchQuery == 'CD') {
            $('#txtsearch').val('').attr('hidden', true);
            $('#searchCDAccount').attr('hidden', false);
        } else {
            $('#searchCDAccount').attr('hidden', true);
            $('#txtsearch').attr('hidden', false);
        }
        // 
    })
    </script>





</body>

</html>