      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div style="color:#000" class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="admin_main.php" class="logo"><img style="width:70px" src="images/logo.png" alt=""></a>
            <!--logo end-->
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="logoff.php">Logout</a></li>
            	</ul>
            </div>
        </header>
