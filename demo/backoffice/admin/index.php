<?php 
require_once('conn/connBS.php');
require_once('scripts/myfunctions.php');
require_once('scripts/mcrypt.php');
$mcrypt=new MCrypt();
mysql_select_db($database_connBS, $connBS);



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $site_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

	  <div id="login-page" >
	  	<div class="container" >
	  		<div align="center" ><img src="images/logo.png" width="300"></div>         
		      <form name="frm" class="form-login" action="index.html" method="post" >
              
		        <h2 class="form-login-heading">ADMINISTRATOR LOGIN</h2>
	
		        <div class="login-wrap" >
                    <?php
                  if(isset($_GET['m'])){
                    echo $mcrypt->decrypt($_GET['m']);
                  }
                  ?>
		            <input type="text" name="username" class="form-control" placeholder="Username" maxlength="15" autofocus>
		            <br>
		            <input type="password" name="password" class="form-control" placeholder="Password" maxlength="15" autofocus>
		            <br>
	
<input type="text" class="form-control" placeholder="Enter Human Verification Code *" name="valid_code" maxlength="10" /><br>
                                                       
                                     
	 <?php
            $real_number=rand(0,99999)+1;
            $add_number=rand(0,99999)+1;
            $sub_number=rand(0,99999)+1;
            $post_number=$real_number + $add_number;
            $post_number=$post_number-$sub_number;
        ?>
                    <div id="new_code"><img src="scripts/image_load.php?i=<?php echo $mcrypt->encrypt($real_number); ?>" /></div>
                    <a href="javascript: login('n');">Change Human Verification Code</a>
                    <input type="hidden" name="d" value="<?php echo $mcrypt->encrypt($add_number); ?> " />
                    <input type="hidden" name="b" value="<?php echo $mcrypt->encrypt($sub_number); ?> "/>
                    <input type="hidden" name="s" value="<?php echo $mcrypt->encrypt($post_number); ?> " />
                    <input type="hidden" name="pc" value="<?php echo "PLANEP"; ?>" />                                        
                    <input type="hidden" name="presentor_code" value="NA" />                                                            
                    <input type="hidden" name="allow_update" value="NO" />                                        
                                      
                    
                    <br><br>                  
                    <br><br>
                                      
		            <button class="btn btn-theme btn-block"  onClick="javascript: validate_login();" type="button"><i class="fa fa-lock"></i>  LOGIN</button>
		            <hr>
		            		           
		
		        </div>
		      </form>	  	
	  	
	  	</div>
	  </div>
	<script language="javascript" src="slogin.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
    function validate_login(){
    var errorstr="";     
    if(document.frm.username.value ==""){
    errorstr+='Username is a required field!\n';
    } 
    if(document.frm.password.value ==""){
    errorstr+='Password is a required field!\n';
    }
    if(document.frm.valid_code.value ==""){
    errorstr+='Validation Code is a required field!\n';
    }				
    if(errorstr==""){
        //login("login~"+document.frm.username.value+"~"+document.frm.password.value+"~"+ document.frm.password.value+"~"+document.frm.valid_code.value+"~"+document.frm.d.value+"~"+document.frm.b.value+"~"+document.frm.s.value);	
    
        document.frm.action="login_process.php";
        document.frm.submit(); 
    }else{
    alert(errorstr);
    }
    }
    </script>    
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
