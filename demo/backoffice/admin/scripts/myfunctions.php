<?php
//---date functions...
function getDateTime() {
$todaysdate = getdate();
if (strlen(strval($todaysdate['mon']))==1) {$themonth = '0'.$todaysdate['mon'];} else {$themonth = $todaysdate['mon'];}
if (strlen(strval($todaysdate['mday']))==1) {$theday = '0'.$todaysdate['mday'];} else {$theday = $todaysdate['mday'];}
if (strlen(strval($todaysdate['hours']))==1) {$thehours = '0'.$todaysdate['hours'];} else {$thehours = $todaysdate['hours'];}
if (strlen(strval($todaysdate['minutes']))==1) {$theminutes = '0'.$todaysdate['minutes'];} else {$theminutes = $todaysdate['minutes'];}
if (strlen(strval($todaysdate['seconds']))==1) {$theseconds = '0'.$todaysdate['seconds'];} else {$theseconds = $todaysdate['seconds'];}
$rightnow = $todaysdate['year'].'-'.$themonth.'-'.$theday.' '.$thehours.':'.$theminutes.':'.$theseconds;
return $rightnow;
}
function extractDate($thedate) {
$strdate = strval($thedate);
$strdate = substr($strdate,0,10);
$extract = "";
for($ctr=0; $ctr<=strlen($strdate)-1; $ctr++) {
if (substr($strdate, $ctr, 1) != "-") {
$extract = $extract . substr($strdate, $ctr, 1);
}
}
return $extract;
}
//---string functions...
function count_words($string) {
$word_count = 0;
$string = eregi_replace(" +", " ", $string);
$string = explode(" ", $string);
while (list(, $word) = each ($string)) {
if (eregi("[0-9A-Za-z---]", $word)) {
$word_count++;
}
}
return($word_count);
}
//---string functions...
function count_lines($string, $maxchars) {
$line_count = 1;
$chars = 0;
$counter = 1;
while ($counter <= strlen($string)) {
$chars++;
if ($chars > 65) {
$line_count++;
$chars = 1;
}
$counter++;
}
return($line_count);
}
function getDateString() {
$todaysdate = getdate();
if (strlen(strval($todaysdate['mon']))==1) {$themonth = '0'.$todaysdate['mon'];} else {$themonth = $todaysdate['mon'];}
if (strlen(strval($todaysdate['mday']))==1) {$theday = '0'.$todaysdate['mday'];} else {$theday = $todaysdate['mday'];}
$rightnow = $todaysdate['year'].'-'.$themonth.'-'.$theday;
return $rightnow;
}
function loadfooter() {
	if(file_exists("scripts/footer.txt")) {
		$content=file_get_contents("scripts/footer.txt");
	}else{
		$content="FAILED TO LOAD FOOTER";
	}
return $content;
}
function loadtitle() {
	if(file_exists("scripts/title.txt")) {
		$content=file_get_contents("scripts/title.txt");
	}else{
		$content="FAILED TO LOAD FOOTER";
	}
return $content;
}
function loadfooter_link() {
	if(file_exists("scripts/footer_link.txt")) {
		$content=file_get_contents("scripts/footer_link.txt");
	}else{
		$content="FAILED TO LOAD FOOTER";
	}
return $content;
}
function loadnews() {
	if(file_exists("scripts/news.txt")) {
		$content=file_get_contents("scripts/news.txt");
	}else{
		$content="FAILED TO LOAD FOOTER";
	}
return $content;
}
function adminloadfooter() {
	if(file_exists("../scripts/footer.txt")) {
		$content=file_get_contents("../scripts/footer.txt");
	}else{
		$content="FAILED TO LOAD FOOTER";
	}
return $content;
}
function createthumb($name,$filename,$new_w,$new_h){
	$system=explode('.',$name);
	if (preg_match('/jpg|jpeg/',$system[1])){
		$src_img=imagecreatefromjpeg($name);
	}
	if (preg_match('/png/',$system[1])){
		$src_img=imagecreatefrompng($name);
	}
	
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);
	if ($old_x > $old_y) {
		$thumb_w=$new_w;
		$thumb_h=$old_y*($new_h/$old_x);
	}
	if ($old_x < $old_y) {
		$thumb_w=$old_x*($new_w/$old_y);
		$thumb_h=$new_h;
	}
	if ($old_x == $old_y) {
		$thumb_w=$new_w;
		$thumb_h=$new_h;
	}
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
	if (preg_match("/png/",$system[1]))
	{
		imagepng($dst_img,$filename); 
	} else {
		imagejpeg($dst_img,$filename); 
	}
	imagedestroy($dst_img); 
	imagedestroy($src_img); 
}
function checkfield($post) {
	if(isset($_POST[$post])) {
		if(strlen($_POST[$post])>0) {
			$return=mysql_escape_string($_POST[$post]);
		}else{ 
			$return="NA";
		}
	}else{
		$return="NA";
	}
return $return;
}
function position($position){
	$pos="";
	if($position=="L") {
		$pos="A";
	}elseif($position=="R") {
		$pos="B";		
	}
	return $pos;
}
function gender($s){
	if($s=="M"){
	 	$gen="Male";
	}elseif($s=="F"){
	 	$gen="Female";
	}
return $gen;
}
function civilstatus($s){
	if($s=="S"){
	 	$gen="Single";
	}elseif($s=="M"){
	 	$gen="Married";
	}elseif($s=="W"){
		$gen="Widowed";
	}elseif($s=="P"){
		$gen="Separated";
	}
return $gen;
}
function load_URL($type){
	if($type==1){
		if(file_exists("scripts/url.sys")) {
			$content=file_get_contents("scripts/url.sys");
		}else{
			$content="FAILED TO LOAD FOOTER";
		}
	}elseif($type==2){
		if(file_exists("../scripts/url.sys")) {
			$content=file_get_contents("../scripts/url.sys");
		}else{
			$content="FAILED TO LOAD FOOTER";
		}	
	}
return $content;
}
function load_DIR($type){
	if($type==1){
		if(file_exists("scripts/dir.sys")) {
			$content=file_get_contents("scripts/dir.sys");
		}else{
			$content="FAILED TO LOAD FOOTER";
		}
	}elseif($type==2){
		if(file_exists("../scripts/dir.sys")) {
			$content=file_get_contents("../scripts/dir.sys");
		}else{
			$content="FAILED TO LOAD FOOTER";
		}	
	}
return $content;
}
function loadPosition($plan){
	$out="";
	if($plan=="PLAN1"){
		$out="Silver";
	}elseif($plan=="PLAN2"){
		$out="Gold";
	}elseif($plan=="PLAN3"){
		$out="Platinum";
	}elseif($plan=="PLAN4"){
		$out="KEI Package";
	}elseif($plan=="PLAN5"){
		$out="Bently";
	}			
return $out;		
}
function loadPackage($x){
    
    if($x=="PLAN1"){
        $out="Club350";
    }elseif($x=="PLAN2"){
        $out="Club5000";
    }elseif($x=="PLAN3"){
        $out="Club16888";
    }elseif($x=="PLAN4"){
        $out="Platinum";
    }elseif($x=="PLAN5"){
        $out="Diamond";
    }           
return $out;        
}
function loadRANK($x){
    
    if($x=="0"){
        $out="Member";
    }elseif($x=="1"){
        $out="Member";
    }elseif($x=="2"){
        $out="Executive";
    }elseif($x=="3"){
        $out="Jade Executive";
    }elseif($x=="4"){
        $out="Pearl Executive";
    }elseif($x=="5"){
        $out="Ruby Executive";
    } elseif($x=="6"){
        $out="Emerald Executive";
    } 
	          
return $out;        
}
function lastday($month = '', $year = '') {
   if (empty($month)) {
      $month = date('m');
   }
   if (empty($year)) {
      $year = date('Y');
   }
   $result = strtotime("{$year}-{$month}-01");
   $result = strtotime('-1 second', strtotime('+1 month', $result));
   return date('Y-m-d', $result);
}

function validate_mobile($mobile){
	//+639171234567
	$errorstr="";
	$prefix=substr($mobile,0,3);
	if($prefix!="+63"){
		$errorstr="Invalid Country Code. Must start with +63";		
	}
	if(preg_match('/^[0-9]{10}+$/', $mobile)){
		$errorstr="Invalid Mobile Number";	
	}
	$str=strlen($mobile);
	if($str!=13){
		$errorstr="Invalid Mobile Number Format";	
	}
	if($errorstr==""){
		return "OK";
	}else{
		return $errorstr;
	}
}
?>
