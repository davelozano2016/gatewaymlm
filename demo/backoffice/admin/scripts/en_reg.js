

function validate_paid()
{
	var errorstr = "";
	var strRegex=/^[a-zA-Z0-9]*$/;	
	
	errorstr += checkitem(document.signupform.firstname,"First Name");	
	errorstr += checkitem(document.signupform.middlename,"Middle Name");		
	errorstr += checkitem(document.signupform.lastname,"Last Name");	
	errorstr += checkitem(document.signupform.mobile,"Mobile Number");			
	errorstr += checkitem(document.signupform.email,"E-Mail");		
	errorstr += checkemail(document.signupform.email);		
	errorstr += checkitem(document.signupform.username,"Username");
	if(!strRegex.test(document.signupform.username.value) && document.signupform.username.value!=""){
		errorstr+="Invalid Data Format on Username. Only Alphanumeric Characters are only allowed\n";
	}
	
	errorstr += checkitem(document.signupform.password,"Password");	
	errorstr += checkitem(document.signupform.confirm_password,"Confirm Password");		
	if(document.signupform.password.value!=document.signupform.confirm_password.value) {
		errorstr += "Password and confirmed password must be the same.\n";
	}
	
	errorstr += checkitem(document.signupform.sponsor_code,"Sponsor Username");					
	errorstr += checkitem(document.signupform.upline_code,"Placement Username");	
	errorstr += checkitem(document.signupform.registration_code,"Activation code");		
	errorstr += checkitem(document.signupform.password_code,"Pin");			
	errorstr += checkitem(document.signupform.valid_code,"Valid Code");		
	
	if(document.signupform.terms.checked==false) {
		errorstr+="Terms and Conditions must be read and check";
	}
	if(errorstr=="") {	
		pass="paid~"+document.signupform.username.value+"~"+document.signupform.sponsor_code.value+"~"+document.signupform.valid_code.value+"~"+document.signupform.d.value+"~"+document.signupform.b.value+"~"+document.signupform.s.value+"~"+document.signupform.registration_code.value+"~"+document.signupform.password_code.value + "~" +document.signupform.upline_code.value + "~"+document.signupform.upline_pos.value + "~" + document.signupform.pc.value + "~" + document.signupform.presentor_code.value ;
		//alert(pass);
		najax(pass);
	}else{
		alert(errorstr);		
	}
}

function validate_sub()
{
	var errorstr = "";
	var strRegex=/^[a-zA-Z0-9]*$/;	
	
	errorstr += checkitem(document.signupform.sponsor_code,"Sponsor Username");					
	errorstr += checkitem(document.signupform.upline_code,"Placement Username");						
	errorstr += checkitem(document.signupform.registration_code,"Registration Code");		
	errorstr += checkitem(document.signupform.password_code,"Password Code");			
	errorstr += checkitem(document.signupform.valid_code,"Valid Code");		
	
	if(errorstr=="") {	
		pass="sub~NA~"+document.signupform.sponsor_code.value+"~"+document.signupform.valid_code.value+"~"+document.signupform.d.value+"~"+document.signupform.b.value+"~"+document.signupform.s.value+"~"+document.signupform.registration_code.value+"~"+document.signupform.password_code.value + "~" +document.signupform.upline_code.value + "~"+document.signupform.upline_pos.value + "~" + document.signupform.pc.value;
		najax(pass);
	}else{
		alert(errorstr);		
	}
}

function check_upline()
{
	var errorstr = "";
	var strRegex=/^[a-zA-Z0-9]*$/;	
	
	errorstr += checkitem(document.signupform.upline_code,"Placement Username");						
	
	if(errorstr=="") {	
		pass="upline~"+document.signupform.upline_code.value;
		najax(pass);
	}
}
