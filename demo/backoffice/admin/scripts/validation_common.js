var browserName = BrowserDetect.browser;
var browserVersion = BrowserDetect.version;
var browserOS = BrowserDetect.OS;
var inactive = "inactive";
var active = "active";
var focused = "focused";

this.label2value = function(){	

	$("label").each(function(){		
		obj = document.getElementById($(this).attr("for"));
		if(($(obj).attr("type") == "text") || (obj.tagName.toLowerCase() == "textarea")){			
			$(obj).addClass(inactive);			
			var text = $(this).text();
			$(this).css("display","none");			
			$(obj).val(text);
			$(obj).focus(function(){	
				$(this).addClass(focused);
				$(this).removeClass(inactive);
				$(this).removeClass(active);								  
				if($(this).val() == text) $(this).val("");
			});	
			$(obj).blur(function(){	
				$(this).removeClass(focused);													 
				if($(this).val() == "") {
					$(this).val(text);
					$(this).addClass(inactive);
				} else {
					$(this).addClass(active);		
				};				
			});				
		};	
	});		
};

$(document).ready(function() {
	$('div.corneredBanner').corner("7px");
	$('div.corneredGeneral').corner("round 5px");
	$('div.corneredBox').corner("5px");
	$('div.corneredImages').corner("7px");
	label2value();	
	$("img[@src$=png]").pngfix();
	$('#profile').popupmenu({
					target: "#pop_profile", 
					time: 300 });
	$('#services_financial').popupmenu({
					target: "#pop_services_financial", 
					time: 300 });
	$('#services_others').popupmenu({
					target: "#pop_services_others", 
					time: 300 });
	$('#pop_services_personal').hide();
	$('#pop_services_financial').hide();
	$('#pop_services_others').hide();
});

