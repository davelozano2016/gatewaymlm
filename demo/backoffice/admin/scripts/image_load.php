<?php

function CaptchaImage($width, $height, $code) {
	require_once("../scripts/mcrypt.php");
	$mcrypt = new MCrypt();
	
	$font = 'd.TTF';
	$font_size = $height * 0.8;
	$image = imagecreate($width, $height) or die('Cannot initialize new GD image stream');
 
	/* set the colours */
	$background_color = imagecolorallocate($image, 245, 245, 245);
	$text_color = imagecolorallocate($image, 0, 0, 0);
	//$noise_color = imagecolorallocate($image, 50, 57, 55);
	$noise_color = imagecolorallocate($image, 0, 0,0);
 
	/* generate random dots in background */
	for( $i=0; $i<($width*$height)/5; $i++ ) {
		imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
	}
 
	/* create textbox and add text */
	$textbox = imagettfbbox($font_size, 0, $font, $mcrypt->decrypt($code)) or die('Error in imagettfbbox function');
	$x = ($width - $textbox[4])/2;
	$y = ($height - $textbox[5])/2;
	imagettftext($image, $font_size, 2, $x, $y, $text_color, $font, $mcrypt->decrypt($code)) or die('Error in imagettftext function');
 
	/* output captcha image to browser */
	header('Content-Type: image/gif');
	imagegif($image);
	imagedestroy($image);
}
 
CaptchaImage(180, 30, $_GET['i']);
 
?>
