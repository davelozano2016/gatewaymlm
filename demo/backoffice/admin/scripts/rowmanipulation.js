// JavaScript Document for row selection...
// keep track of the old row 
var oldRow = "undefined";
var oldBackgroundColor = "undefined";  
function highlightRow(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerEdit.selectedItem.value = currentrow;
		document.messengerDelete.selectedItem.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerEdit.selectedItem.value = currentrow;
		document.messengerDelete.selectedItem.value = currentrow;
	}
}

//---function to highlight rows with details...
function highlightRowWithDetails(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFFFCC';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.selectfrm.selectedItem.value=currentrow;
		//document.messengerEdit.selectedItem.value = currentrow;
		//document.messengerDelete.selectedItem.value = currentrow;
		//document.messengerDetails.selectedItem.value = currentrow;
		//document.frmSelectClient.client_ctr.value=currentrow;
		//sendItem("details");
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFFFCC';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.selectfrm.selectedItem.value=currentrow;
		//document.messengerEdit.selectedItem.value = currentrow;
		//document.messengerDelete.selectedItem.value = currentrow;
		//document.messengerDetails.selectedItem.value = currentrow;
		//document.frmSelectClient.client_ctr.value=currentrow;
		//sendItem("details");
	}
}
//---called by TM (transcriber manager) only...
function highlightPendingRow(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFFFCC';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerPending.selectedItem.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFFFCC';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerPending.selectedItem.value = currentrow;
	}
}

//---called by TM only...
function highlightTranscribedRow(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerTranscribed.selectedItem.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...		
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;		
		document.messengerTranscribed.selectedItem.value = currentrow;
	}
}

//---called by TM (transcriber manager) only...
function highlightRowAssignment(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerConfirm.selectedUser.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerConfirm.selectedUser.value = currentrow;
	}
}

//---called by Client only...
function highlightClientRow(currentrow, origcolor,filedocument,invoiceno) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerClientRow.selectedItem.value = currentrow;
		document.messengerClientRow.selectedDocument.value=filedocument;
		//getAJAXRecords("edit_online~" + filedocument);
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerClientRow.selectedItem.value = currentrow;
		document.messengerClientRow.selectedDocument.value=filedocument;
		//getAJAXRecords("edit_online~" + filedocument);
	}
}
function highlightInvoiceRow(currentrow, origcolor,invoiceno) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.frmDownload.invoiceno.value=invoiceno;
		document.frmView.invoiceno.value=invoiceno;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.frmDownload.invoiceno.value=invoiceno;
		document.frmView.invoiceno.value=invoiceno;
	}
}

//---called by MT (medical transcriptionist) only...immediately triggers submit() on select
function highlightTranscript(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerTranscript.selectedItem.value = currentrow;
		var pass=new String();				
		pass="mtmain~" + currentrow+ "~" + origcolor;
		getAJAXRecords(pass);		
//		document.messengerTranscript.submit();
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerTranscript.selectedItem.value = currentrow;
//		document.messengerTranscript.submit();
		var pass=new String();				
		pass="mtmain~" + currentrow+ "~" + origcolor;
		getAJAXRecords(pass);
	}
}

//---called by ADMIN only...
function highlightReviewRow(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerReview.selectedItem.value = currentrow;
		document.messengerInvoice.selectedItem.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerReview.selectedItem.value = currentrow;
		document.messengerInvoice.selectedItem.value = currentrow;
	}
}

//---called by QA only...
function highlightQaReviewRow(currentrow, origcolor) {
	if (oldRow != "undefined") {
		//---set old selected row to normal state...
		obj = document.getElementById(oldRow);
		obj.bgColor = oldBackgroundColor;
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerReview.selectedItem.value = currentrow;
	}
	else if (currentrow != "undefined") {
		//---set new row to highlighted state...
		obj = document.getElementById(currentrow);
		obj.bgColor = '#FFAA55';
		//---record currentrow as old row...
		oldRow = currentrow;
		oldBackgroundColor = origcolor;
		document.messengerReview.selectedItem.value = currentrow;
	}
}
//---call this on onmouseover and onmouseout events....
function hover(id, color) {
    var obj = document.getElementById(id);
	obj.bgColor = color;
}
function sendItem(action) {
	if (action=="edit") {
		if (document.messengerEdit.selectedItem.value != "") {
			document.messengerEdit.submit();
		} else {
			alert("Kindly select a record first before performing an action.");
		}
	} else if (action == "delete") {
		if (document.messengerDelete.selectedItem.value != "") {
			document.messengerDelete.submit();
		} else {
			alert("Kindly select a record first before performing an action.");
		}
	} else if (action == "details") {
		if (document.messengerDetails.selectedItem.value != "") {
			document.messengerDetails.submit();
		} else {
			alert("Kindly select a record first before performing an action.");
		}
	} else if (action == "pending") {
		if (document.messengerPending.selectedItem.value != "") {
			document.messengerPending.submit();
		} else {
			alert("Kindly select a pending transaction before assigning to an MT.");
		}
	} else if (action == "confirm") {
		if (document.messengerConfirm.selectedItem.value != "" && document.messengerConfirm.selectedItem.value != "") {
			document.messengerConfirm.submit();
		} else {
			alert("Kindly select an MT before assigning the job order.");
		}
	} else if (action == "review") {
		if (document.messengerTranscribed.selectedItem.value != "") {
			document.messengerTranscribed.submit();
		} else {
			alert("Kindly select a transcription record before performing a review.");
		}
	} else if (action == "adminreview") {
		if (document.messengerReview.selectedItem.value != "") {
			document.messengerReview.submit();
		} else {
			alert("Kindly select a transcription record before performing a review.");
		}
	} else if (action == "invoice") {
		if (document.messengerInvoice.selectedItem.value != "") {
			document.messengerInvoice.submit();
		} else {
			alert("Kindly select a transcription record before making an invoice.");
		}
	} else if (action == "qareview") {
		if (document.messengerReview.selectedItem.value != "") {
			document.messengerReview.submit();
		} else {
			alert("Kindly select a transcription record before performing a review.");
		}
	} else if (action == "clientreview") {
		if (document.messengerClientRow.selectedItem.value != "") {
				if(document.messengerClientRow.selectedDocument.value!="NA") {
						getAJAXRecords("edit_online~"+document.messengerClientRow.selectedDocument.value);
						//document.messengerClientRow.submit();
				}else{
					alert("The dictation has no transcriptions");
				}
		} else {
			alert("Kindly select a transcription record before performing a review.");
		}
	}  else if (action == "clientinfo") {
		if (document.messengerInfoChange.selectedItem.value != "") {
			document.messengerInfoChange.submit();
		} else {
			alert("Kindly select a transcription record before performing a review.");
		}
	}  else if (action == "downloadtranscript") {
		if (document.messengerDownload.selectedItem.value != "") {
				if(document.messengerDownload.DownloadDocument.value!= "NA") {
					document.messengerDownload.action="../downDOC.php?filename=" + document.messengerDownload.DownloadDocument.value
					document.messengerDownload.submit();
				} else {
					alert("The voicefile has no transcription");
				}
				
		} else {
			alert("Kindly select a transcription record before performing a download.");
		}
	}  else if (action == "downloadaudio") {
		if (document.messengerDownload.selectedItem.value != "") {
			document.messengerDownload.submit();
		} else {
			alert("Kindly select a record from the datagrid before performing a download.");
		}		
	} else if (action=="downloadinvoice") {
		if (document.frmInvoices.selectedItem.value !="") {
			if (document.frmInvoices.InvoiceName.value=="NA") {
					alert("Invoices are not yet available");				
			} else {
				document.frmInvoices.action="../downDoc.php?filename=" + document.frmInvoices.InvoiceName.value + ".doc";
				document.frmInvoices.submit();
			}
		}
	}  else if (action=="priority") {
		if (document.messengerClientRow.selectedItem.value !="") {
				//document.messengerDownload.action="client_mainpending.php?priority=1&jobrefno=" + document.messengerDownload.selectedItem.value;
				//document.messengerDownload.submit();
				document.priorityfrm.priority.value=1;
				document.priorityfrm.jobrefno.value=document.messengerClientRow.selectedItem.value;
				document.priorityfrm.action="";
				document.priorityfrm.submit();
			}		
	} else if (action=="cancel_priority") {
		if (document.messengerClientRow.selectedItem.value !="") {
				//document.messengerDownload.action="client_mainpending.php?priority=1&jobrefno=" + document.messengerDownload.selectedItem.value;
				//document.messengerDownload.submit();
				document.priorityfrm.priority.value=0;
				document.priorityfrm.jobrefno.value=document.messengerClientRow.selectedItem.value;
				document.priorityfrm.action="";
				document.priorityfrm.submit();
			}		
	} else if (action=="approveall") {
		var ans="Do you really want to approve all documents";
			if(confirm(ans)) {
				alert("Note:The documents with no comments by Transcribed Manager and Quality Assurance will be only approve.");
				document.frmApprove.ApproveAll.value="YES";
				document.frmApprove.action="";
				document.frmApprove.submit();
				}
			else{
				alert("Approval was cancelled");
			}
					
	} else if (action=="invoicedownload") {
			if(document.frmDownload.invoiceno.value!="") {
				document.frmDownload.action="../downDOC2.php?filename=" + document.frmDownload.invoiceno.value + ".doc";
				document.frmDownload.submit();
			} else {
				alert("Kindly select first the invoice no you want to be download");
			}
	} else if (action=="invoicedetails") {
			if(document.frmView.invoiceno.value!="") { 
				document.frmView.submit();
			} else { 
				alert("Kindly select first the invoice no you to be view for details");
			}
		
	}

} 
