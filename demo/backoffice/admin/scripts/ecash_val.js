function validate(){
	 var errorstr="";
	 var floatRegex = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
	 var intRegex = /^(0|[1-9][0-9]*)$/;	 
	 			if(document.frm.request_type.value=="0"){
					errorstr+="Request Type is required\n";
				}
				//validate Rewards
				if(document.frm.amount.value==""){
					errorstr+="Rewards is required\n";
					document.frm.amount.value=0;
				}else{
					if(parseFloat(document.frm.amount.value)>=0){
						if(!floatRegex.test(document.frm.amount.value) && !intRegex.test(document.frm.amount.value) ){
							errorstr += "Please specify valid Rewards\n";
						}else{
							if(parseFloat(document.frm.amount.value)>parseFloat(document.frm.ecash.value)){
								errorstr += "The amount you entered is greater than your current Rewards\n";
							}
							if(parseFloat(document.frm.amount.value)>0){
								if(parseFloat(document.frm.min_ecash.value)>parseFloat(document.frm.amount.value)){
									errorstr += "The amount you entered is less than the minimum required amount\n";
								}
							}							
						}		
					}else{
						errorstr += "Please specify valid Rewards\n";
					}
				}
				//validate GC
				if(document.frm.gc.value==""){
					errorstr+="Number of Gift Certificate(s) is required.\n";
					document.frm.gc.value=0;
				}else{
					if(parseFloat(document.frm.gc.value)>=0){
						if(!intRegex.test(document.frm.amount.value) ){
							errorstr += "Please specify valid Number of Gift Certificate(s)\n";
						}else{
							if(parseFloat(document.frm.gc.value)>parseFloat(document.frm.cgc.value)){
								errorstr += "The Number of Gift Certificate(s) you entered is greater than your current Gift Certificate(s)\n";
							}
						}		
					}else{
						errorstr += "Please specify valid Number of Gift Certificate(s)\n";
					}
				}
				//check if at least 1 request
				if(parseFloat(document.frm.amount.value)==0 && parseInt(document.frm.gc.value)==0){
					errorstr+="You must request an Bonus Amount\n";	
				}
				if(document.frm.password.value==""){
					errorstr+="Account Password is required\n";		
				}				
				if(errorstr==""){
					pass="ecash~"+document.frm.password.value+"~"+document.frm.amount.value+"~"+document.frm.valid_code.value+"~"+document.frm.d.value+"~"+document.frm.b.value+"~"+document.frm.s.value;
					najax(pass);
				}else{
					alert(errorstr);
				}

}
