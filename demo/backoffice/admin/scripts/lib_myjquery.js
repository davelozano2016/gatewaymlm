$(document).ready(function() {
/*	$("#searchform").validate({
		//As soon as a key within a form field in "myform" is release then start
		event: "keyup",
		//Here the rules for the individual inputs are defined.
		rules: {
			//for id "captcha"
			captcha: {
				required: true
			}
		},
		
		//Here the error messages for all rules are defined.
		messages: {
			captcha: {
				required: '&nbsp;Required.'
			}
		}
		
	});
*/	
  	function filterPath(string) {
    	return string
      		.replace(/^\//,'')  
      		.replace(/(index|default).[a-zA-Z]{3,4}$/,'')  
      	.replace(/\/$/,'');
  	}
  
  	$('a[href*=#]').each(function() {
    	if ( filterPath(location.pathname) == filterPath(this.pathname)
    		&& location.hostname == this.hostname
    		&& this.hash.replace(/#/,'') ) {
      			var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
      			var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
       			if ($target) {
         			var targetOffset = $target.offset().top;
         			$(this).click(function() {
           				$('html, body').animate({scrollTop: targetOffset}, 400);
           			return false;
         		});
      		}
    	}
  	});
	/*
	$('div.cornered').corner("10px");
	$('div.gradient').gradient({ topcolor: '#ffffff', bottomcolor: '#cccccc', horizontal: false, opacity: 40});
	$('div.border').wrap("<div class='xtra'><\/div>");
	$('div.border').corner("round 7px").parent().css('padding', '0px').corner("7px");
	$('div.dropshadow').shadow({ color: "#444", offset: 5, opacity: 0.2 });
	$('div.combo').corner("10px").parent().css('padding', '0px').corner("10px");
	$('div.combo').gradient({ topcolor: '#ffffff', bottomcolor: '#cccccc', horizontal: false, opacity: 10});
	$('div.combo').wrap("<div class='xtra2'><\/div>");
	$('div.xtra2').shadow({ color: "#444", offset: 5, opacity: 0.2 });
	$('div.liquid').wrap("<div class='xtra3'><\/div>");
	$('div.liquid').corner("10px").parent().css('padding', '0px').corner("10px");
	*/	
});

