// @d 		String. Required.
// @zeros 	Mixed. Optional.
// @trunc 	Boolean. Optional.
function parseDecimal(d, zeros, trunc) 
{
	//d=d.replace(/[a-zA-Z\!\@\#\$\%\^\&\*\(\)\_\+\-\=\{\}\|\[\]\\\:\"\;'\<\>\?\,\/\~\`]/g,"");
	d=d.replace(/[^\d\.]/g,"");
	while (d.indexOf(".") != d.lastIndexOf("."))
		d=d.replace(/\./,"");
	if (typeof zeros == 'undefined' || zeros == "") {
		return parseFloat(d);
	}
	else 
	{
		var mult = Math.pow(10,zeros);
		if (typeof trunc == 'undefined' || (trunc) == false)
			return parseFloat(Math.round(d*mult)/mult);
		else
			return parseFloat(Math.floor(d*mult)/mult);
	}
}

function parseCurrency(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function parseComma(amount)
{
	var delimiter = ","; // replace comma if desired
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}
jQuery.preloadImages = function()
{
  for(var i = 0; i<arguments.length; i++)
  {
    jQuery("<img>").attr("src", arguments[i]);
  }
}
