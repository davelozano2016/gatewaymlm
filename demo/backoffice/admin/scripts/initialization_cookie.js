var toggle_status = "hidden";

function getCookie(Name){ var re=new RegExp(Name+"=[^;]+", "i"); if (document.cookie.match(re)) return document.cookie.match(re)[0].split("=")[1]; else return "home"; }
function setCookie(name, value, days){ var expireDate = new Date(); var expstring=expireDate.setDate(expireDate.getDate()+parseInt(days)); document.cookie = name+"="+value+"; expires="+expireDate.toGMTString()+"; path=/";}
function setMenu()
{
	var activeMenu = getCookie("activePage");
	switch (activeMenu)
	{
		case "services_personal": { 
			$('#services_personal').removeClass('menu_item_inactive').addClass('menu_item_active');; 
			break;
		}
		case "services_financial": { 
			$('#services_financial').removeClass('menu_item_inactive').addClass('menu_item_active');; 
			break;
		}
		case "services_others": { 
			$('#services_others').removeClass('menu_item_inactive').addClass('menu_item_active');; 
			break;
		}
		case "news": {
			$('#news').removeClass('menu_item_inactive').addClass('menu_item_active'); 
			break;
		}
		case "about": {
			$('#about').removeClass('menu_item_inactive').addClass('menu_item_active'); 
			break;
		}
		case "location": { 
			$('#location').removeClass('menu_item_inactive').addClass('menu_item_active'); 
			break;
		}
		case "contact": { 
			$('#contact').removeClass('menu_item_inactive').addClass('menu_item_active'); 
			break;
		}
		case "default": { 
			break;
		}
	}
}