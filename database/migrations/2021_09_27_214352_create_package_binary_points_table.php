<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePackageBinaryPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_binary_points', function (Blueprint $table) {
            $table->id();
            $table->string('id_number')->nullable();
            $table->string('upline_id')->nullable();
            $table->integer('position')->nullable();
            $table->integer('position_on_top')->nullable();
            $table->decimal('points',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_binary_points');
    }
}
