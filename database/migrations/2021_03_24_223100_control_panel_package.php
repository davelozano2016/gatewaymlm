<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ControlPanelPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('control_panel_packages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('entries_id')->references('id')->on('control_panel_entries');
            $table->integer('package_unique_code')->nullable();
            $table->string('package_name',255);
            $table->string('description',10000)->nullable();
            $table->integer('package_status');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
