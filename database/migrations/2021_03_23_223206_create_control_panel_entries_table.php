<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateControlPanelEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_panel_entries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('countries_id')->references('id')->on('countries');
            $table->string('entry');
            $table->decimal('price',10,2);
            $table->string('discount_type');
            $table->decimal('stockist_discount',10,2);
            $table->decimal('depot_discount',10,2);
            $table->decimal('country_manager_discount',10,2);
            $table->decimal('direct_referal',10,2);
            $table->decimal('binary_point_value',10,2);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_panel_entries');
    }
}
