<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserNetworkStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_network_structure', function (Blueprint $table) {
            $table->id();
            $table->foreignId('users_id')->references('id')->on('users');
            $table->string('country')->nullable();
            $table->string('sponsor_id');
            $table->string('upline_id')->nullable();
            $table->integer('position')->comment('0 = Left | 1 = Right | 2 = TOP HEAD');
            $table->string('activation_code')->nullable();
            $table->integer('status_type')->comment('0 = Paid | 1 = CD | 2 = FREE | 3 = Floating');
            $table->string('group_code');
            $table->integer('account_type')->comment("0 = Main Account | 1 = Sub Account");
            $table->datetime('date_upgraded')->default('0000-00-00 00:00:00');
            $table->timestamps();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
