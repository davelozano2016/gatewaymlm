<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateControlPanelEncashmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_panel_encashments', function (Blueprint $table) {
            $table->id();
            $table->integer('status')->nullable()->comment('0 = Active | 1 = Active');
            $table->integer('pairing_status')->nullable()->comment('0 = Active | 1 = Active');
            $table->integer('commission_deduction')->nullable()->comment('1 - 100 in percentage');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_panel_encashments');
    }
}
