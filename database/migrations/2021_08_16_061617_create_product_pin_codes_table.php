<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProductPinCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_pin_codes', function (Blueprint $table) {
            $table->id();
            $table->integer('distributor_id')->nullable();
            $table->integer('used_by')->nullable();
            $table->integer('product_description_id')->nullable();
            $table->string('customer_id');
            $table->string('reference');
            $table->string('full_name');
            $table->string('activation_code');
            $table->string('order_number');
            $table->decimal('price',10,2);
            $table->string('order_status');
            $table->string('method_of_payment')->nullable();
            $table->integer('status')->comment('0 = Unused | 1 = Used');
            $table->string('local_pickup')->nullable();
            $table->decimal('unilevel_point_value',10,2);
            $table->decimal('binary_point_value',10,2);
            $table->integer('lock_status')->comment('0 = Unlocked | 1 = Locked');
            $table->string('country_code');
            $table->integer('is_order')->comment('0 = Internal Ecommerce | 1 = External Ecommerce');
            $table->string('created_by');
            $table->string('registered_ip')->nullable();
            $table->datetime('date_processed');
            $table->string('date_used');
            $table->datetime('date_created');
            $table->datetime('date_expiration');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_pin_codes');
    }
}
