<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRegistrationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_codes', function (Blueprint $table) {
            $table->id();
            $table->integer('distributor_id')->nullable();
            $table->integer('used_by')->nullable();
            $table->foreignId('packages_id')->references('id')->on('control_panel_packages');
            $table->string('customer_id');
            $table->string('full_name');
            $table->string('activation_code');
            $table->decimal('bpv',10,2);
            $table->string('order_number');
            $table->string('reference')->nullable();
            $table->string('order_status')->nullable();
            $table->string('local_pickup')->nullable();
            $table->string('method_of_payment')->nullable();
            $table->decimal('price',10,2);
            $table->integer('status')->comment('0 = Unused | 1 = Used');
            $table->integer('type')->comment('0 = PAID | 1 = FREE | 2 = CD');
            $table->integer('lock_status')->comment('0 = Unlocked | 1 = Locked');
            $table->string('country_code');
            $table->string('created_by');
            $table->string('registered_ip')->nullable();
            $table->datetime('date_processed');
            $table->string('date_used');
            $table->datetime('date_created');
            $table->datetime('date_expiration');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_codes');
    }
}
