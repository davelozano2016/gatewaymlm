<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateControlPanelBinaryParingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_panel_binary_paring_settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('countries_id')->references('id')->on('countries');
            $table->decimal('pair_condition',10,2)->nullable();
            $table->decimal('pair_amount_condition',10,2)->nullable();
            $table->integer('pair_gc')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_panel_binary_paring_settings');
    }
}
