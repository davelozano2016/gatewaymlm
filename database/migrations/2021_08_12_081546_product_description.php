<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ProductDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_description', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_packages_id')->references('id')->on('product_packages');
            $table->string('country');
            $table->decimal('distributor_price',10,2);
            $table->decimal('srp',10,2);
            $table->decimal('retailer',10,2);
            $table->string('discount_type');
            $table->decimal('stockist_discount',10,2);
            $table->decimal('depot_discount',10,2);
            $table->decimal('country_manager_discount',10,2);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
