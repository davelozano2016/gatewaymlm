<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('users', function (Blueprint $table) {
    //         $table->id();
    //         $table->string('user_details',10000);
    //         $table->foreignId('user_level_id')->references('id')->on('user_levels');
    //         $table->timestamp('created_at');
    //         $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
    //     });
    // }


    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $random = rand(111111,999999);
            $table->id();
            $table->string('id_number')->unique()->nullable();
            $table->string('surname')->nullable();
            $table->string('firstname')->nullable();
            $table->string('middlename')->nullable();
            $table->string('email')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->integer('status')->comment('0 = Active | 1 = Inactive');
            $table->datetime('date_registered');
            $table->string('month')->nullable();
            $table->integer('roles_id')->nullable();
            $table->integer('ranks_id')->nullable();
            $table->integer('security_code')->default($random);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

