<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateEncashmentControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encashment_controls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('countries_id')->references('id')->on('countries');
            $table->decimal('tax',10,2)->nullable();
            $table->decimal('maintenance',10,2)->nullable();
            $table->decimal('internal_processing_fee',10,2)->nullable();
            $table->decimal('minimum_encash_request',10,2)->nullable();
            $table->decimal('minimum_gc_request',10,2)->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encashment_controls');
    }
}
