<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBinaryPairingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_pairings', function (Blueprint $table) {
            $table->id();
            $table->integer('id_number')->nullable();
            $table->integer('position')->nullable();
            $table->decimal('points',10,2)->nullable();
            $table->integer('cycle')->nullable()->comment('0 = AM | 1 = PM | 2 = No Cycle');
            $table->date('date_added')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binary_pairings');
    }
}
