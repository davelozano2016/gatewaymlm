<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUnilevelSponsorTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unilevel_sponsor_trees', function (Blueprint $table) {
            $table->id();
            $table->integer('id_number')->nullable();
            $table->integer('waiting_id_number')->nullable();
            $table->integer('upline_id')->nullable();
            $table->integer('level')->nullable();
            $table->decimal('points',10,2)->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unilevel_sponsor_trees');
    }
}
