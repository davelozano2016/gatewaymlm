<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateControlPanelCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_panel_commissions', function (Blueprint $table) {
            $table->id();
            $table->string('entry_settings_details',2000);
            $table->string('entry_binary_pv_settings_details',2000);
            $table->string('direct_referral_settings_details',2000);
            $table->string('binary_paring_settings_details',2000);
            $table->string('maximum_daily_pairing_settings_details',2000);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_panel_commissions');
    }
}
