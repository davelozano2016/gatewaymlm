<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProductPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_packages', function (Blueprint $table) {
            $table->id();
            $table->string('product_title');
            $table->string('product_code')->unique();
            $table->foreignId('product_category_id')->references('id')->on('product_categories');
            $table->foreignId('product_supplier_id')->references('id')->on('product_supplier');
            $table->integer('binary_points');
            $table->integer('unilevel_points');
            $table->integer('net_weight_per_gram');
            $table->string('facebook_thumbnail');
            $table->string('product_image');
            $table->string('product_description');
            $table->string('product_miscellaneous');
            $table->integer('product_status');
            $table->string('product_stocks');
            $table->integer('is_featured')->default(0)->comment('0 = Normal Product | 1 = Featured Product');
            $table->integer('locked_status')->comment('0 = Unlocked | 1 = Locked');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_packages');
    }
}
