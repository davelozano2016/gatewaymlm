<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('product_logs', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('transfer_no');
        //     $table->foreignId('receiptient_id')->references('id')->on('users');
        //     $table->foreignId('source_id')->references('id')->on('users');
        //     $table->date('date_added');
        //     $table->string('remarks');
        //     $table->foreignId('product_id')->references('id')->on('product_packages');
        //     $table->timestamp('created_at');
        //     $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_logs');
    }
}
