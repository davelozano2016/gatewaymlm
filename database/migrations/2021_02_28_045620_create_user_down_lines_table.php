<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUserDownLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_down_lines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("upline_id");
            $table->unsignedBigInteger("downline_id")->unique();
            $table->boolean("position")->default(false)->comment("0/false => left, 1/true = right");
            $table->timestamps();

            $table->foreign('upline_id')->references('id')->on('users');
            $table->foreign('downline_id')->references('id')->on('users');
            $table->unique(["upline_id", "position"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_down_lines');
    }
}
