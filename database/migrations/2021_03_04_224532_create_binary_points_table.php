<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBinaryPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_points', function (Blueprint $table) {
            $table->id();
            $table->integer('id_number');
            $table->integer('waiting_id_number');
            $table->integer('upline_id');
            $table->integer('level');
            $table->integer('position')->comment('0 = Left Leg | 1 = Right Leg');
            $table->integer('position_on_top')->comment('0 = Left Panel | 1 = Right Panel');
            $table->decimal('points',10,2);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binary_points');
    }
}
