<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->id();
            $table->integer('recipient');
            $table->integer('source');
            $table->string('bonus_type')->comment('Direct Referral | Indirect Referral | Pairing Bonus | Unilevel Bonus | Markup Bonus');
            $table->decimal('amount',10,2);
            $table->integer('cycle')->comment('0 = AM | 1 = PM | 2 = No Cycle');
            $table->string('starting_date')->nullable();
            $table->string('ending_date')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuses');
    }
}
