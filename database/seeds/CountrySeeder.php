<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $countries = [
            [
                'name' => 'Philippines',
                'code' => 'PH',
                'currency_code' => 'PHP',
                'currency_name' => 'Philippine peso',
                'currency_symbol' => '₱',
                'timezones' => 'UTC+08:00',
                'is_active' => '0'
            ],[
                'name' => 'United States of America',
                'code' => 'US',
                'currency_code' => 'USD',
                'currency_name' => 'United States dollar',
                'currency_symbol' => '$',
                'timezones' => 'UTC-12:00',
                'is_active' => '0'
            ],[
                'name' => 'United Arab Emirates',
                'code' => 'AE',
                'currency_code' => 'AED',
                'currency_name' => 'United Arab Emirates dirham',
                'currency_symbol' => 'د.إ',
                'timezones' => 'UTC+04',
                'is_active' => '0'
            ],
        ];
        foreach($countries as $data) {
            App\Country::create([
                'name'              => $data['name'],
                'code'              => $data['code'],
                'currency_code'     => $data['currency_code'],
                'currency_name'     => $data['currency_name'],
                'currency_symbol'   => $data['currency_symbol'],
                'timezones'         => $data['timezones'],
                'is_active'         => 0
            ]);
        }
    }
}
