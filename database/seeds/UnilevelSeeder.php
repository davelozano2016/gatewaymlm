<?php

use Illuminate\Database\Seeder;
use App\ControlPanelUnilevelSettings;
use App\ControlPanelUnilevel;

class UnilevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ControlPanelUnilevelSettings::create([
            'maintenance' => 1200,
            'unilevel_count' => 10
        ]);

        for($i=0;$i<=10;$i++) {
            ControlPanelUnilevel::create([
                'level' => $i,
                'percentage' => 10
            ]);
        }

    }
}
