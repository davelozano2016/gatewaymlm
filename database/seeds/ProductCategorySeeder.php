<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_categories = ['Beauty Product','Health Product','Beverages'];
        foreach($product_categories as $data) {
            App\ProductCategory::create(['category'  => $data]);
        }
    }
}
