<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Http\Request;
use App\UserNetworkStructure;
use App\UserEwallet;
use App\RegistrationCodes;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserAccount;
use App\UserSocialMediaAccounts;
use App\SocialMedia;
use App\UsersUnilevel;
use App\BinaryPoints;
use Carbon\Carbon; 
class MigrateUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function ru2n() {
        //
        // $url = 'https://keivirtual.com/keiwwcpanel888/migration.php?module=user-networks';
        // $curl = curl_init();
        // curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($curl, CURLOPT_HEADER, false);
        // $array = curl_exec($curl);
        // curl_close($curl);
        // $query = json_decode($array);
        $generated_id = rand(1111111,9999999);

        $prefix = substr('Platinum', 0, 1);
        $base   = 'FREE';
        $price  = '104999.00';
        $activation_code = $prefix.rand(11111,99999).$base.rand(11111,99999).'PH';
        RegistrationCodes::create([
            'distributor_id'    => $generated_id,
            'used_by'           => $generated_id,
            'packages_id'       => 7,
            'customer_id'       => 'FIRSTACCOUNT'.$base,
            'full_name'         => 'TOP HEAD',
            'activation_code'   => $activation_code,
            'bpv'               => 0,
            'order_number'      => 'FIRSTACCOUNT'.$base,
            'price'             => $price,
            'status'            => 1,
            'type'              => 2, // Paid
            'country_code'      => 'PH', 
            'created_by'        => 'Administrator',
            'registered_ip'     => \Request::ip(),
            'date_processed'    => date('Y-m-d h:i:s'),
            'date_expiration'   => date('Y-m-d h:i:s',strtotime('+1 month')),
            'date_used'         => date('Y-m-d h:i:s'),
            'date_created'      => date('Y-m-d h:i:s'),
        ]);

        $query = [
            'id_number'       => $generated_id,
            'surname'         => 'HEAD',
            'firstname'       => 'TOP',
            'middlename'      => '',
            'email'           => 'support@gatewaybackoffice.com',
            'username'        => 'gateway',
            'country'         => 'PH',
            'sponsor_code'    => '0000000',
            'upline_code'     => '0000000',
            'upline_pos'      => 2,
            'group_code'      => $generated_id,
            'date_registered' => Carbon::now(),
        ];


        $status_type          = 2;
        $id_number            = $query['id_number'];
        $surname              = $query['surname'];
        $firstname            = $query['firstname'];
        $middlename           = $query['middlename'];
        $email                = $query['email'];
        $username             = $query['username'];
        $password             = Crypt::encryptString(12345678);
        $country              = $query['country'];
        $sponsor_id           = $query['sponsor_code'];
        $upline_id            = $query['upline_code'];
        $position             = 2;
        $group_code           = Crypt::encryptString($query['group_code']);
        $users_id             = User::create([
            'id_number'       => $id_number,
            'surname'         => $surname,
            'firstname'       => $firstname,
            'middlename'      => $middlename,
            'email'           => $email,
            'username'        => $username,
            'password'        => $password,
            'date_registered' => $query['date_registered'],
            'month'           => date('m',strtotime($query['date_registered'])),
            'ranks_id'        => 1,
        ])->id;

        
        $network              = UserNetworkStructure::create([
            'users_id'        => $users_id,
            'country'         => $country,
            'sponsor_id'      => $sponsor_id,
            'upline_id'       => $upline_id,
            'position'        => $position,
            'activation_code' => $activation_code,
            'status_type'     => $status_type,
            'group_code'      => $group_code,
            'account_type'    => 0,
        ]);

        UserEwallet::create(['users_id' => $users_id]);
        UserDetails::create(['users_id' => $users_id]);
        UserContacts::create(['users_id' => $users_id]);
        UserBeneficiary::create(['users_id' => $users_id]);
        UserBankAccounts::create(['users_id' => $users_id]);
        UserAccount::create(['users_id' => $users_id]);
        UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
        BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 0,'position_on_top' => $position,'points' => 0]);
        BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 1,'position_on_top' => $position,'points' => 0]);
        $social = SocialMedia::all();
        foreach($social as $s) {
            UserSocialMediaAccounts::create(['users_id' => $users_id,'social_media_id' => $s->id]);
        }
    }

    public function run() {
        //
        $url = 'http://localhost/migration/migration.php?module=user-networks';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);
        foreach($query as $query) {
            $stats = $query->status_type;
            if($stats == 'PAID') {
                $status_type = 0;
            } elseif($stats == 'CD') {
                $status_type = 1;
            } elseif($stats == 'FREE') {
                $status_type = 2;
            } else {
                $status_type = 3;
            }

            DB::beginTransaction();
            try {

                
            $id_number            = $query->id_number;
            $surname              = $query->lastname;
            $firstname            = $query->firstname;
            $middlename           = $query->middlename;
            $email                = $query->email;
            $username             = $query->username;
            $password             = Crypt::encryptString(12345678);
            $country              = $query->country;
            $sponsor_id           = $query->sponsor_code;
            $upline_id            = $query->upline_code;
            $position             = $query->upline_pos == 'R' ? 1 : 0;
            $group_code           = Crypt::encryptString($query->group_code);

            $users_id = User::create([
                'id_number'       => $id_number,
                'surname'         => $surname,
                'firstname'       => $firstname,
                'middlename'      => $middlename,
                'email'           => $email,
                'username'        => $username,
                'password'        => $password,
                'date_registered' => $query->date_registered,
                'month'           => date('m',strtotime($query->date_registered)),
                'ranks_id'        => 1,
            ])->id;

         

            DB::table('user_network_structure')->insert([
                'users_id'        => $users_id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => 'P85209P87052PH',
                'status_type'     => $status_type,
                'group_code'      => $group_code,
                'account_type'    => 0,
            ]);

            DB::table('user_ewallets')->insert(['users_id' => $users_id]);
            DB::table('users_details')->insert(['users_id' => $users_id]);
            DB::table('users_contacts')->insert(['users_id' => $users_id]);
            DB::table('users_beneficiaries')->insert(['users_id' => $users_id]);
            DB::table('users_bank_accounts')->insert(['users_id' => $users_id]);
            DB::table('user_accounts')->insert(['users_id' => $users_id,'total_current_income' => $query->total_income]);
            DB::table('users_unilevel')->insert(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
            
            $social = SocialMedia::all();
            $this->genealogy($id_number,$upline_id, $position, 100);
            foreach($social as $s) {
                UserSocialMediaAccounts::create(['users_id' => $users_id,'social_media_id' => $s->id]);
            }

           
            DB::commit();
            } catch(Throwable $e) {
                DB::rollback();
                throw $e;
            }


        }
    }

    private function genealogy($source,$uplineId, $position, $points) {
        $table           = 'binary_points';
        $getTreeIds      = function($uplineId, $position) use($table) {
            $level           = 0;
            $idNumber        = $uplineId;
        $currentPosition = $position;
        $ids             = [];
        
        $get = function($idNumber, $level, $position) use($table) {
            return DB::table($table)->where([
                ['id_number', '=', $idNumber],
                ['level', '=', $level],
                ['position', '=', $position]
            ])->first();
        };
        
        while ($row = $get($idNumber, $level, $currentPosition)) {
            $nextUplineId = $row->upline_id;
            $ids[] = $row->id;
            if ($idNumber === $nextUplineId) {
                break;
            }
            $idNumber = $nextUplineId;
            $currentPosition = $row->position_on_top;
        }
            return $ids;
        };

        
        
        DB::beginTransaction();
        try {
            $partialValues = [
            'id_number'         => $source,
            'waiting_id_number' => $source,
            'upline_id'         => $uplineId,
            'level'             => 0,
            'position_on_top'   => $position,
            'points'            => 0
        ];
        DB::table($table)->insert([
            Arr::add($partialValues, 'position', 0),
            Arr::add($partialValues, 'position', 1)
        ]);
        // DB::table($table)->whereIn('id', $getTreeIds($uplineId, $position))->update(['points'=>DB::raw("points+$points")]);
        DB::commit();
        } catch(Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}
