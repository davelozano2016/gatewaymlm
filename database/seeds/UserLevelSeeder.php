<?php

use Illuminate\Database\Seeder;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_levels = array(
            ['user_level' => 'Provider'],
            ['user_level' => 'Super Administrator'],
            ['user_level' => 'Administrator'],
            ['user_level' => 'Sub Administrator'],
            ['user_level' => 'Cashier'],
            ['user_level' => 'Distributor'],
            ['user_level' => 'Member'],
            ['user_level' => 'Stockist'],
            ['user_level' => 'Mobile Stockist'],
            ['user_level' => 'Country Manager'],
            ['user_level' => 'Store Manager'],
            ['user_level' => 'Store Administrator'],
            ['user_level' => 'Support Supervisor'],
            ['user_level' => 'Support Agent'],
            ['user_level' => 'Finance Manager'],
            ['user_level' => 'Finance Administrator'],
            ['user_level' => 'Booking Manager'],
            ['user_level' => 'Booking Agent'],
            
        );
        foreach($user_levels as $key => $data) {
            App\UserLevel::create([
                'user_level'        => $data['user_level'],
                'user_level_status' => 0,
            ]);
        }

    }
}
