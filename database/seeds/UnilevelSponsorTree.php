<?php

use Illuminate\Database\Seeder;

class UnilevelSponsorTree extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = 'http://localhost/migration/migration.php?module=sponsor-tree';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);

        foreach($query as $key => $value) {
            $query = DB::table('unilevel_sponsor_trees')->insert([
                'id_number'         => $value->id_number,
                'waiting_id_number' => $value->waiting_id_number,
                'upline_id'         => $value->upline_id,
                'level'             => $value->level
            ]);
        }
    }
}
