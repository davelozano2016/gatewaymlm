<?php

use Illuminate\Database\Seeder;

class BanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = array(
            ['banks' => 'BDO','banks_logo' => 'default.png'],
            ['banks' => 'PNB','banks_logo' => 'default.png'],
            ['banks' => 'BPI','banks_logo' => 'default.png'],
            ['banks' => 'AUB','banks_logo' => 'default.png']
        );
        foreach($banks as $key => $data) {
            App\Banks::create([
                'banks'        => $data['banks'],
                'banks_logo'   => $data['banks_logo'],
                'banks_status' => 0,
            ]);
        }

    }
}
