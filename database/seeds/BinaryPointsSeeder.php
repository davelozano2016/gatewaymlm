<?php

use Illuminate\Database\Seeder;

class BinaryPointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return 
     */
    public function run()
    {
        //

        $url = 'http://localhost/migration/migration.php?module=binary-points&position=L';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);

        // dd($query);

        foreach($query as $key => $value) {
            $id = $key;
            $points =  $value[0];
            $query = DB::table('binary_points')->where(['id_number' => $id,'position' => 0])->update(['points' => $points]);
        }
        

        $url = 'http://localhost/migration/migration.php?module=binary-points&position=R';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);

        foreach($query as $key => $value) {
            $id = $key;
            $points =  $value[0];
            $query = DB::table('binary_points')->where(['id_number' => $id,'position' => 1])->update(['points' => $points]);
        }
    }
}
