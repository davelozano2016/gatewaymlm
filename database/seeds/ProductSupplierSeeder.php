<?php

use Illuminate\Database\Seeder;

class ProductSupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product_categories = [
            [
                'supplier_code' => 'SC0001',
                'supplier_name' => 'Computology'
            ],
            [
                'supplier_code' => 'SC0002',
                'supplier_name' => 'Yumina'
            ]
        ];
        
        foreach($product_categories as $key => $data) {
            App\ProductSupplier::create([
                'supplier_code'  => $data['supplier_code'],
                'supplier_name'  => $data['supplier_name']
            ]);
        }
    }
}
