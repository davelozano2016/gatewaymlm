<?php

use Illuminate\Database\Seeder;

class SocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_levels = array(
            ['social_media_name' => 'Facebook','social_media_logo' => 'default.png'],
            ['social_media_name' => 'Twitter','social_media_logo'  => 'default.png'],
            ['social_media_name' => 'Yahoo','social_media_logo'    => 'default.png'],
        );
        foreach($user_levels as $key => $data) {
            App\SocialMedia::create([
                'social_media_name'   => $data['social_media_name'],
                'social_media_logo'   => $data['social_media_logo'],
                'social_media_status' => 0,
            ]);
        }

    }
}
