<?php

use Illuminate\Database\Seeder;
use App\EcpayTopup;

class LoadingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $soapClient = new SoapClient('https://s2s.oneecpay.com/wstopupv2/WSTopUp.asmx?wsdl');
	
		$response = $soapClient->GetTelcoList(array(
			'LoginInfo' => [
				'AccountID'  => '2411',
				'Username'   => 'mike.miranda',
				'Password'   => 'Emperor1@#',
				'BranchID'   => '38989'
			]
		));
	   
		foreach($response->GetTelcoListResult as $result) {
			array_pop($result);
			foreach($result as $data) {
				$array[] = $data;
			}
		}
	
        foreach($array as $result) {
            $TelcoTag 		= $result->TelcoTag;
            $TelcoName 		= $result->TelcoName;
            $Denomination 	= $result->Denomination;
            $ExtTag 		= $result->ExtTag;
            EcpayTopup::create([
                'TelcoTag' => $TelcoTag, 'TelcoName' => $TelcoName,'Denomination' => $Denomination, 'ExtTag' => $ExtTag
            ]);
        }
    }
}
