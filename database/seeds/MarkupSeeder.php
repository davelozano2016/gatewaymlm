<?php

use Illuminate\Database\Seeder;
use App\Markup;
use App\User;

class MarkupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $query = User::all();

        foreach($query as $row) {
            $markups = array(
                ['users_id' => $row->id, 'booking_type' => 'Flight','category' => 'Domestic Markup','type' => 'Fixed','amount' => 500],
                ['users_id' => $row->id, 'booking_type' => 'Flight','category' => 'International Markup','type' => 'Fixed','amount' => 900],
    
                ['users_id' => $row->id, 'booking_type' => 'Hotel','category' => 'Domestic Markup','type' => 'Fixed','amount' => 500],
                ['users_id' => $row->id, 'booking_type' => 'Hotel','category' => 'International Markup','type' => 'Fixed','amount' => 900],
    
                ['users_id' => $row->id, 'booking_type' => 'Tours','category' => 'Domestic Markup','type' => 'Fixed','amount' => 500],
                ['users_id' => $row->id, 'booking_type' => 'Tours','category' => 'International Markup','type' => 'Fixed','amount' => 900],
    
                ['users_id' => $row->id, 'booking_type' => 'Cars','category' => 'Domestic Markup','type' => 'Fixed','amount' => 500],
                ['users_id' => $row->id, 'booking_type' => 'Cars','category' => 'International Markup','type' => 'Fixed','amount' => 900],
    
                ['users_id' => $row->id, 'booking_type' => 'Cruises','category' => 'Domestic Markup','type' => 'Fixed','amount' => 500],
                ['users_id' => $row->id, 'booking_type' => 'Cruises','category' => 'International Markup','type' => 'Fixed','amount' => 900],
            );
            foreach($markups as $markup) {
                Markup::create($markup);
            }
        }

        

    }
}
