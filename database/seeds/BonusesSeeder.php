<?php

use Illuminate\Database\Seeder;
use App\User;
class BonusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        

        $url = 'https://overview.gatewaybackoffice.com/backoffice/admin/migration.php?module=bonuses';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);

        foreach($query as $key => $k) {

            DB::beginTransaction();
            try {
                foreach($k as $data) {
                    $query = User::where(['id_number' => $data->id_number])->exists();
                    if($query) {
                        $user = DB::table('users')->where(['id_number' => $data->id_number])->first();
                        DB::table('user_accounts')->where(['users_id' => $user->id])->update(['total_current_income' => $data->total_current_income == null ? 0 : $data->total_current_income,'total_overall_income' => $data->total_overall_income == null ? 0 : $data->total_overall_income]);
                    }
                }
                DB::commit();
            } catch(Throwable $e) {
                DB::rollback();
                throw $e;
            }
            
        }
    }
}
