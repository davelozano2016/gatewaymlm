<?php

use Illuminate\Database\Seeder;
use App\UserNetworkStructure;
use App\RegistrationCodes;

class RegistrationCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $url = 'http://localhost/migration/migration.php?module=user-networks';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);
        foreach($query as $structure) {
            $created_by     = 'Admin';
            $registered_ip  = \Request::ip();

            

            if($structure->status_type == 'PAID') {
                $base = 'P';
                $type = 0;
            } elseif($structure->status_type == 'CD') {
                $base = 'CD';
                $type = 1;
            } elseif($structure->status_type == 'FREE') {
                $base = 'FREE';
                $type = 2;
            } else {
                $type = 3;
            }
            $name = $structure->member_type;
            if($name == 'PLAN1') {
                $package_type = 'Silver';
            } elseif($name == 'PLAN2') {
                $package_type = '';
            } elseif($name == 'PLAN3') {
                $package_type = 'Executive';
            } elseif($name == 'PLAN4') {
                $package_type = 'KEI Package';
            }

            if($structure->country == 'PH') {
                if($name == 'PLAN1') {
                    $price = '3499';
                    $packages_id = 1;
                } elseif($name == 'PLAN2') {
                    $price = '7899';
                    $packages_id = 4;
                } elseif($name == 'PLAN3') {
                    $price = '8000';
                    $packages_id = 7;
                } elseif($name == 'PLAN4') {
                    $price = '15799';
                    $packages_id = 10;
                }
            } elseif($structure->country == 'AE') {
                if($name == 'PLAN1') {
                    $price = '253.01';
                    $packages_id = 3;
                } elseif($name == 'PLAN2') {
                    $price = '571.17';
                    $packages_id = 6;
                } elseif($name == 'PLAN3') {
                    $price = '578.47';
                    $packages_id = 9;
                } elseif($name == 'PLAN4') {
                    $price = '11420';
                    $packages_id = 12;
                }
            } elseif($structure->country == 'US') {
                if($name == 'PLAN1') {
                    $price = '68.88';
                    $packages_id = 2;
                } elseif($name == 'PLAN2') {
                    $price = '155.5';
                    $packages_id = 5;
                } elseif($name == 'PLAN3') {
                    $price = '157.48';
                    $packages_id = 8;
                } elseif($name == 'PLAN4') {
                    $price = '311.01';
                    $packages_id = 11;
                }
            }

          
            if($structure->status_type != 3) {
                $prefix = substr($package_type, 0, 1);
                $activation_code = $prefix.rand(11111,99999).$base.rand(11111,99999).$structure->country;
                $users_id = RegistrationCodes::create([
                    'distributor_id'    => 1234567,
                    'used_by'           => $structure->id_number,
                    'packages_id'       => $packages_id,
                    'customer_id'       => 'MIGRATION2021'.$base,
                    'full_name'         => 'Arleen Ramos Miyakawa',
                    'activation_code'   => $activation_code,
                    'bpv'               => 0,
                    'order_number'      => 'MIGRATION2021'.$base,
                    'price'             => $price,
                    'status'            => 1,
                    'type'              => $type, // Paid
                    'country_code'      => $structure->country, 
                    'created_by'        => $created_by,
                    'registered_ip'     => $registered_ip,
                    'date_processed'    => date('Y-m-d h:i:s'),
                    'date_expiration'   => date('Y-m-d h:i:s',strtotime('+1 month')),
                    'date_used'         => date('Y-m-d h:i:s'),
                    'date_created'      => date('Y-m-d h:i:s'),
                ])->id;
                UserNetworkStructure::where(['users_id' => $users_id])->update(['activation_code' => $activation_code]);
            }
        }
    }
}
