<?php

use Illuminate\Database\Seeder;
use App\EncashmentControls;
use App\ControlPanelEncashment;
use App\ControlPanelBinaryParingSettings;

class EncashmentControlsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'countries_id'            => 1,
                'tax'                     => 5,
                'maintenance'             => 50,
                'internal_processing_fee' => 280,
                'minimum_encash_request'  => 2500,
                'minimum_gc_request'      => 5,
            ],[
                'countries_id'            => 2,
                'tax'                     => 5,
                'maintenance'             => 0.99,
                'internal_processing_fee' => 3.97,
                'minimum_encash_request'  => 49.61,
                'minimum_gc_request'      => 5
            ],[
                'countries_id'            => 3,
                'tax'                     => 5,
                'maintenance'             => 3.65,
                'internal_processing_fee' => 14.59,
                'minimum_encash_request'  => 182.22,
                'minimum_gc_request'      => 5
            ],
        ];

        
        foreach($countries as $data) {
            EncashmentControls::create([
                'countries_id'            => $data['countries_id'],
                'tax'                     => $data['tax'],
                'maintenance'             => $data['maintenance'],
                'internal_processing_fee' => $data['internal_processing_fee'],
                'minimum_encash_request'  => $data['minimum_encash_request'],
                'minimum_gc_request'      => $data['minimum_gc_request'],
            ]);
        }

        $controls = [
            [
                'status'                => 0,
                'pairing_status'        => 0,
                'commission_deduction'   => 50
            ]
        ];

      
        foreach($controls as $data) {
            ControlPanelEncashment::create([
                'status'                => $data['status'],
                'pairing_status'        => $data['pairing_status'],
                'commission_deduction'  => $data['commission_deduction']
            ]);
        }

        $binary_settings = [
            [
                'countries_id'          => 1,
                'pair_condition'        => 100,
                'pair_amount_condition' => 2500,
                'pair_gc'               => 5
            ],
            [
                'countries_id'          => 2,
                'pair_condition'        => 50,
                'pair_amount_condition' => 11.91,
                'pair_gc'               => 5
            ],
            [
                'countries_id'          => 2,
                'pair_condition'        => 50,
                'pair_amount_condition' => 43.74,
                'pair_gc'               => 5
            ]
        ];

        

        foreach($binary_settings as $data) {
            ControlPanelBinaryParingSettings::create([
                'countries_id'          => $data['countries_id'],
                'pair_condition'        => $data['pair_condition'],
                'pair_amount_condition' => $data['pair_amount_condition'],
                'pair_gc'               => $data['pair_gc']
            ]);
        }


    }
}
