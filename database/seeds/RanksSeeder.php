<?php

use Illuminate\Database\Seeder;
use App\Ranks;

class RanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $countries = [
            [
                'ranks'      => 'Bronze',
                'commission' => '0.1',
            ],[
                'ranks'      => 'Silver',
                'commission' => '0.15',
            ],[
                'ranks'      => 'Gold',
                'commission' => '0.2',
            ],
        ];
        foreach($countries as $data) {
            Ranks::create([
                'ranks'      => $data['ranks'],
                'commission' => $data['commission']
            ]);
        }
    }
}
