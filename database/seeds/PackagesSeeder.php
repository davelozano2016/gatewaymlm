<?php

use Illuminate\Database\Seeder;
use App\ControlPanelEntries;
use App\ControlPanelPackages;
use App\Country;
use App\ControlPanelMaximumDailyPairingSettings;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $url = 'http://localhost/migration/migration.php?module=packages';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array      = curl_exec($curl);
        curl_close($curl);
        $query      = json_decode($array);
        $countries  = Country::all();
        foreach($query as $data) {
            $name = $data->registration_type;
            if($name == 'PLAN4'){
                
            } else {
                if($name == 'PLAN1') {
                    $entry = 'Silver';
                } elseif($name == 'PLAN2') {
                    $entry = 'Gold';
                } elseif($name == 'PLAN3') {
                    $entry = 'Platinum';
                } 
                foreach($countries as $country) {
                    $random     = rand(1111111,9999999);
                    if($country->code == 'PH') {
                        if($name == 'PLAN1') {
                            $price = '14999';
                            $maximum_pairing = 14;
                            $direct_referal = 1000;
                            $bpv = 100;
                        } elseif($name == 'PLAN2') {
                            $price = '44999';
                            $maximum_pairing = 42;
                            $direct_referal = 3000;
                            $bpv = 300;
                        } elseif($name == 'PLAN3') {
                            $price = '104999';
                            $maximum_pairing = 70;
                            $direct_referal = 5000;
                            $bpv = 500;
                        } 
                    } elseif($country->code == 'AE') {
                        if($name == 'PLAN1') {
                            $price = '1092.96';
                            $maximum_pairing = 3;
                        } elseif($name == 'PLAN2') {
                            $price = '3279.03';
                            $maximum_pairing = 16;
                        } elseif($name == 'PLAN3') {
                            $price = '7651.18';
                            $maximum_pairing = 25;
                        } 
                        $direct_referal = 51.01;
                    } elseif($country->code == 'US') {
                        if($name == 'PLAN1') {
                            $price = '297.57';
                            $maximum_pairing = 3;
                        } elseif($name == 'PLAN2') {
                            $price = '892.74';
                            $maximum_pairing = 16;
                        } elseif($name == 'PLAN3') {
                            $price = '2083.09';
                            $maximum_pairing = 25;
                        } 
                        $direct_referal = 13.89;
                    }
                    $entries_id = ControlPanelEntries::create([
                        'countries_id'       => $country->id,
                        'entry'              => $entry,
                        'price'              => $price,
                        'discount_type'      => 'Fixed',
                        'direct_referal'     => $direct_referal,
                        'binary_point_value' => $bpv
                    ])->id;
                    
                    ControlPanelPackages::create([
                        'entries_id'          => $entries_id,
                        'package_unique_code' => $random,
                        'package_name'        => $data->title,
                        'description'         => 'This is sample description for '.$data->title,
                    ]);

                    ControlPanelMaximumDailyPairingSettings::create([
                        'entries_id'      => $entries_id,
                        'maximum_pairing' => $maximum_pairing
                    ]);
                }
            }
        }
    }
}
