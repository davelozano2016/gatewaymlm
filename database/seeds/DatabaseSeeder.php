<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        Model::unguard();

        $this->call([
            // UserSeeder::class,
            // LoadingSeeder::class,
            UnilevelSponsorTree::class,
            SocialMediaSeeder::class,
            BanksSeeder::class,
            UserLevelSeeder::class,
            CountrySeeder::class,
            ProductCategorySeeder::class,
            ProductSupplierSeeder::class,
            UnilevelSeeder::class,
            PackagesSeeder::class,
            RanksSeeder::class,
            EncashmentControlsSeeder::class,
            MigrateUser::class,
            RegistrationCodesSeeder::class,
            BinaryPointsSeeder::class,
        ]);
        
    }
}
