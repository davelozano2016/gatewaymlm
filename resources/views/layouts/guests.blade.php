<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

    <!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('VirtualOffice/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/images/mini_logo.png') }}" rel="icon" type="image/png">
    <link href="{{ asset('assets/parsley/parsley.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/custom.css') }}"  rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	
</head>
<body>
	<div class="page-content">
		@yield('container')
	</div>
    <!-- Core JS files -->
	<script src="{{ asset('assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script src="{{ asset('VirtualOffice/js/app.js') }}"></script>
	<script src="{{ asset('assets/js/demo_pages/login.js') }}"></script>
	<script src="{{ asset('assets/parsley/parsley.min.js') }}"></script>
</body>
</html>