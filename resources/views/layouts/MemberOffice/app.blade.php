<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="turbolinks-cache-control" content="no-preview">
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta name="turbolinks-visit-control" content="reload">
    <title>{{config('app.name')}}</title>

    <!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.min.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('memberoffice/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('memberoffice/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('memberoffice/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('memberoffice/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('memberoffice/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/images/mini_logo.png') }}"  rel="icon" type="image/png">
    <link href="{{ asset('assets/css/custom.css') }}"  rel="stylesheet" type="text/css">

    <script src="{{ asset('js/app.js') }}"  ></script>


</head>
<body>
    @include('layouts.MemberOffice.top-navigation')
    
<!-- Page content -->
    <div class="page-content pt-0">

        @include('layouts.MemberOffice.navigation')
            @yield('container')
    </div>
    @include('layouts.MemberOffice.footer')
       
    <script src="{{ asset('assets/js/main/jquery.min.js') }}" ></script>
    <script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}" ></script>
    <script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"  ></script>
    <script src="{{ asset('memberoffice/js/app.js') }}"  ></script>
    @yield('custom')
</body>
</html>