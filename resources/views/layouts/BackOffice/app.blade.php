@if(session()->get('admin') === true)
@else
    @php
        header("Location: " . URL::to('backoffice/login'), true, 302);
        exit();
    @endphp
@endif
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="turbolinks-cache-control" content="no-preview">
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta name="turbolinks-visit-control" content="reload">
    <title>{{config('app.name')}}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('BackOffice/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('BackOffice/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('BackOffice/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('BackOffice/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('BackOffice/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/parsley/parsley.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/images/mini_logo.png') }}" rel="icon" type="image/png">


    <style>
    * {
        /* border:none !important; */
        border-radius: 0px !important;
    }

    hr {
        border: none;
        border-top: 1px dotted #000;
        color: #fff;
        background-color: #fff;
        height: 1px;
        width: 50%;
    }
    </style>
</head>

<body>


    @include('layouts.BackOffice.top-navigation')
    <!-- Page content -->
    <div class="page-content">
        @include('layouts.BackOffice.navigation')
        @yield('container')
        @include('layouts.BackOffice.footer')
    </div>
    <!-- /theme JS files -->

    <script src="{{ asset('assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('assets/parsley/parsley.min.js') }}"></script>
    <script src="{{ asset('BackOffice/js/app.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script>
        function notification(message,type) {
            var SweetAlert = function () {
                var _componentSweetAlert = function() {
                    if (typeof swal == 'undefined') {
                        console.warn('Warning - sweet_alert.min.js is not loaded.');
                        return;
                    }

                    // Defaults
                    var swalInit = swal.mixin({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary',
                            cancelButtonClass: 'btn btn-light'
                        });

                    // Bottom
                    
                    
                        swalInit.fire({
                            text: message,
                            type: type,
                            showConfirmButton: true,
                        });
                };

                return {
                    initComponents: function() {
                        _componentSweetAlert();
                    }
                }
            }();


            // Initialize module
            // ------------------------------

            document.addEventListener('DOMContentLoaded', function() {
                SweetAlert.initComponents();

            });
        }
    </script>
	<!-- /theme JS files -->
    @yield('custom')
    @if(Session::has('message'))
    <script>
        notification('{{Session::get("message")}}','info') 
    </script>
    @endif
</body>

</html>