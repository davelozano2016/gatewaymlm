 <!-- Main sidebar -->
 <div class="sidebar sidebar-dark sidebar-main  sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ asset('assets/images/mikewooting.jpg') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">Michael Miranda</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;Quezon City, PH
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Dashboard</div> <i class="icon-menu" title="ECOMMERCE"></i></li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/dashboard') }}" class="nav-link dashboard" title="">
                        <i class="icon-home4"></i> <span> Dashboard </span>
                    </a>
                </li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/register') }}" class="nav-link register" title="">
                        <i class="icon-user"></i> <span> Register </span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu profile-management-must-open">
                    <a href="#" class="nav-link"><i class="icon-profile"></i> <span>Profile Management</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Profile Management">
                        <li class="nav-item"><a href="{{url('backoffice/profile-management/member-list')}}" class="nav-link pm-member-list"> Member List</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/profile-management/kyc-details')}}" class="nav-link pm-kyc-details"> KYC Details</a></li>
                    </ul>
                </li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/payout') }}" class="nav-link payout" title="">
                        <i class="icon-cash3"></i> <span> Payout  </span>
                    </a>
                </li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/e-wallet') }}" class="nav-link e-wallet" title="">
                        <i class="icon-wallet"></i> <span> E-Wallet </span>
                    </a>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Code Management</div> <i class="icon-menu" title="Code Management"></i></li>
                
                <li class="nav-item nav-item-submenu e-pin-must-open">
                    <a href="#" class="nav-link"><i class="icon-pen2"></i> <span>E-Pin</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/paid-codes')}}" class="nav-link e-paid-codes"> Paid Codes </a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/cd-codes')}}" class="nav-link e-cd-codes"> CD Codes</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/free-slot-codes')}}" class="nav-link e-free-slot-codes"> Free Slot Codes</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/download-pins')}}" class="nav-link e-download-pins-codes"> Download Pins</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/reports')}}" class="nav-link e-report-pins"> Report Pins</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu p-pin-must-open">
                    <a href="#" class="nav-link"><i class="icon-cart4"></i> <span>Product Pins</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/product-pins/generate')}}" class="nav-link pp-generate-distributor-pins"> Generate Distributor Pins</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/product-pins/available-pins')}}" class="nav-link pp-available-pins">Available Pins</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/product-pins/used-pins')}}" class="nav-link pp-used-pins">Used Pins</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/product-pins/download-pins')}}" class="nav-link pp-download-pins"> Download Pins</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/e-pin/product-pins/reports')}}" class="nav-link pp-report-pins"> Report Pins</a></li>
                    </ul>
                </li>


                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">ECOMMERCE</div> <i class="icon-menu" title="ECOMMERCE"></i></li>

                <li class="nav-item nav-item-submenu e-commerce-store-must-open">
                    <a href="#" class="nav-link"><i class="icon-store"></i> <span>E-Commerce Store</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="E-Commerce Store">
                        <li class="nav-item nav-item-submenu e-commerce-store-must-open"><a href="#" class="nav-link ps-product-setup"> Product Setup</a>
                            <ul class="nav nav-group-sub" data-submenu-title="Product Setup">
                                <li class="nav-item"><a href="{{url('backoffice/e-commerce-store/product-setup/products')}}" class="nav-link ps-products"> Products</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/e-commerce-store/product-setup/promos')}}" class="nav-link ps-promos"> Promos</a></li>
                            </ul>
                        </li>
                        
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu crm-must-open">
                    <a href="#" class="nav-link"><i class="icon-users2"></i> <span>CRM</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="CRM">
                        <li class="nav-item"><a href="{{url('backoffice/crm/dashboard')}}" class="nav-link crm-dashboard"> Dashboard </a></li>
                        <!-- <li class="nav-item"><a href="{{url('backoffice/crm/add-lead')}}" class="nav-link crm-add-lead"> Add Lead</a></li> -->
                        <li class="nav-item"><a href="{{url('backoffice/crm/view-lead')}}" class="nav-link crm-view-lead"> View Lead</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/crm/graph')}}" class="nav-link crm-graph"> Graph</a></li>
                    </ul>
                </li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/store-administration') }}" class="nav-link store-administration" title="">
                        <i class="icon-store"></i> <span> Store Administration </span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu order-details-must-open">
                    <a href="#" class="nav-link"><i class="icon-list3"></i> <span>Order Details</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{url('backoffice/order-details/order-history')}}" class="nav-link od-order-history"> Order History</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/order-details/order-approval')}}" class="nav-link od-order-approval"> Order Approval</a></li>
                    </ul>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Genealogy</div> <i class="icon-menu" title="Genealogy"></i></li>

                <li class="nav-item nav-item-submenu network-must-open">
                    <a href="#" class="nav-link "><i class="icon-tree7"></i> <span>Network </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Network">
                        <li class="nav-item"><a href="{{url('backoffice/network/binary')}}" class="nav-link n-binary"> Binary</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/network/genealogy-tree')}}" class="nav-link n-genealogy-tree"> Genealogy Tree</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/network/sponsor-tree')}}" class="nav-link n-sponsor-tree"> Sponsor Tree</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/network/tree-view')}}" class="nav-link n-tree-view"> Tree view</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/network/downline-members')}}" class="nav-link n-downline-members"> Downline Members</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/network/referral-members')}}" class="nav-link n-referral-members"> Referral Members</a></li>
                    </ul>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Reports</div> <i class="icon-menu" title="Reports"></i></li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/business') }}" class="nav-link business" title="">
                        <i class="icon-office"></i> <span> Business </span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu reports-must-open">
                    <a href="#" class="nav-link"><i class="icon-stats-dots"></i> <span>Reports</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Reports">
                        <li class="nav-item"><a href="{{url('backoffice/reports/profile')}}" class="nav-link r-profile"> Profile</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/activate-deactivate')}}" class="nav-link r-activate-deactivate"> Activate / Deactivate</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/joining')}}" class="nav-link r-joining"> Joining</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/commission')}}" class="nav-link r-commission"> Commission </a> </li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/total-bonus')}}" class="nav-link r-total-bonus"> Total Bonus</a> </li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/top-earners')}}" class="nav-link r-top-earners"> Top Earners</a> </li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/payout')}}" class="nav-link r-payout "> Payout</a> </li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/rank-performance')}}" class="nav-link r-rank-performance"> Rank Performance</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/reports/e-pin-transfer')}}" class="nav-link r-e-pin-transfer"> E-Pin Transfer</a></li>
                        
                    </ul>
                </li>
                

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Settings</div> <i class="icon-menu" title="Integrations"></i></li>

                <li class="nav-item nav-item-submenu tools-must-open">
                    <a href="#" class="nav-link"><i class="icon-gear"></i> <span>Tools</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Tools">
                        <li class="nav-item"><a href="{{url('backoffice/tools/auto-responder')}}" class="nav-link t-auto-responder"> Auto Responder</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/tools/upload-materials')}}" class="nav-link t-upload-materials"> Upload Materials</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/tools/news')}}" class="nav-link t-news"> News</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/tools/faqs')}}" class="nav-link t-faqs"> FAQs</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu settings-must-open">
                    <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Settings</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Settings">
                        <li class="nav-item"><a href="{{url('backoffice/control-panel/create-entry-package')}}" class="nav-link s-create-entry-panel"> Create Entry Package</a></li>
                        <li class="nav-item nav-item-submenu commission-settings-must-open"><a href="{{url('backoffice/settings/commission-settings')}}" class="nav-link s-commission-settings"> Commission Settings</a>
                            <ul class="nav nav-group-sub " data-submenu-title="Commission Settings">
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/commission')}}" class="nav-link cs-commission"> Commission </a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/compensation')}}" class="nav-link cs-compensation"> Compensation </a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/rank')}}" class="nav-link cs-rank"> Rank</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/payout')}}" class="nav-link cs-payout"> Payout</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/payment')}}" class="nav-link cs-payment"> Payment</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/signup')}}" class="nav-link cs-signup"> Signup</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/mail')}}" class="nav-link cs-mail"> Mail</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/api-key')}}" class="nav-link cs-api-key"> API Key</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/commission-settings/currency')}}" class="nav-link cs-currency"> Currency </a></li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item-submenu advanced-settings-must-open"><a href="{{url('backoffice/settings/advanced-settings')}}" class="nav-link s-advanced-settings"> Advanced Settings</a> 
                            <ul class="nav nav-group-sub " data-submenu-title="Commission Settings">
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/profile')}}" class="nav-link as-profile"> Profile </a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/conversion')}}" class="nav-link as-conversion"> Conversion </a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/language')}}" class="nav-link as-language"> Language</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/e-pin')}}" class="nav-link as-epin"> E-Pin</a></li>
                                <!-- <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/custom-field')}}" class="nav-link as-custom-field"> Custom Field</a></li> -->
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/user-dashboard')}}" class="nav-link as-user-dashboard"> User Dashboard</a></li>
                                <li class="nav-item"><a href="{{url('backoffice/settings/advanced-settings/tree')}}" class="nav-link as-tree"> Tree</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a href="{{url('backoffice/settings/company-profile')}}" class="nav-link s-company-profile"> Company Profile</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/settings/content-management')}}" class="nav-link s-content-management"> Content Management</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/settings/mail-content')}}" class="nav-link s-mail-content"> Mail Content</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/settings/sms-content')}}" class="nav-link s-sms-content"> SMS Content</a></li> 
                    </ul>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Miscellaneous</div> <i class="icon-menu" title="Miscellaneous"></i></li>

                

                <li class="nav-item nav-item-submenu support-center-must-open">
                    <a href="#" class="nav-link"><i class="icon-lifebuoy"></i> <span>Support Center</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Support Center">
                        <li class="nav-item"><a href="{{url('backoffice/support-center/ticket-dashboard')}}" class="nav-link sc-ticket-dashboard"> Ticket Dashboard</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/support-center/view-tickets')}}" class="nav-link sc-view-tickets"> View Tickets </a></li>
                        <li class="nav-item"><a href="{{url('backoffice/support-center/open-tickets')}}" class="nav-link sc-open-tickets"> Open Tickets</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/support-center/resolved-tickets')}}" class="nav-link sc-resolved-tickets"> Resolved Tickets </a></li>
                        <li class="nav-item"><a href="{{url('backoffice/support-center/category')}}" class="nav-link sc-category"> Category</a></li>
                        <li class="nav-item"><a href="{{url('backoffice/support-center/configuration')}}" class="nav-link sc-configuration"> Configuration</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu privileged-user-must-open">
                    <a href="#" class="nav-link"><i class="icon-user-tie"></i> <span>Privileged User</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Privileged Users">
                        <!-- <li class="nav-item"><a href="{{url('backoffice/privileged-users/register')}}" class="nav-link pu-register"> Register</a></li> -->
                        <li class="nav-item"><a href="{{url('backoffice/privileged-users/users-list')}}" class="nav-link pu-users-list"> Users List</a></li>
                        <!-- <li class="nav-item"><a href="{{url('backoffice/privileged-users/change-password')}}" class="nav-link pu-change-password"> Change Password</a></li> -->
                        <li class="nav-item"><a href="{{url('backoffice/privileged-users/user-activity')}}" class="nav-link pu-user-activity"> User Activity</a></li>
                    </ul>
                </li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/mailbox/inbox') }}" class="nav-link inbox" title="">
                        <i class="icon-envelope"></i> <span> Mailbox  </span>
                    </a>
                </li>

                <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Integrations</div> <i class="icon-menu" title="Integrations"></i></li>

                <li class="nav-item nav-item-submenu ecpay-must-open">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>ECPay</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="ECPay">
                        <li class="nav-item"><a href="{{url('backoffice/ecpay/dashboard')}}" class="nav-link e-dashboard"> Dashboard</a></li>
                    </ul>
                </li>


                <li class="nav-item nav-item-submenu booking-travel-must-open">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Booking Travel</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Booking Travel">
                        <li class="nav-item"><a href="{{url('backoffice/booking-travel/dashboard')}}" class="nav-link bt-dashboard"> Dashboard</a></li>
                    </ul>
                </li> -->

                

                <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Database</div> <i class="icon-menu" title="Database"></i></li>

                <li class="nav-item nav-item-submenu database-must-open">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Database</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Database">
                        <li class="nav-item"><a href="{{url('backoffice/database/import')}}" class="nav-link d-import"> Import </a></li>
                        <li class="nav-item"><a href="{{url('backoffice/database/export')}}" class="nav-link d-export"> Export</a></li>
                    </ul>
                </li> -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Others</div> <i class="icon-menu" title="Integrations"></i></li>

                <li class="nav-item " >
                    <a href="{{ url('backoffice/database') }}" class="nav-link database" title="">
                        <i class="icon-database"></i> <span> Database </span>
                    </a>
                </li>

                <!-- <li class="nav-item " >
                    <a href="{{ url('backoffice/dashboard') }}" class="nav-link DashboardDashboard" title="">
                        <i class="icon-home4"></i> <span> Logout  </span>
                    </a>
                </li> -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>
<!-- /main sidebar -->

