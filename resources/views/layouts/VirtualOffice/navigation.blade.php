
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body" style="background:url({{asset('assets/images/backgrounds/user_bg2.jpg') }}) center center no-repeat;background-size:cover">
                <div class="card-body text-center">
                    <a href="#">
                        <img src="{{ asset('assets/images/mikewooting.jpg') }}" class="img-fluid rounded-circle " width="80" height="80" alt="">
                    </a>
                    <h6 class="mb-0 text-white text-shadow-dark">{{$query[0]->firstname}} {{$query[0]->surname}}</h6>
                    <span class="font-size-sm text-white text-shadow-dark"><a class="badge badge-primary">{{$query[0]->id_number}}</a> <br> {{$query[0]->country}}</span>
                    
                </div>
                                            
                <div class="sidebar-user-material-footer">
                    <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>Profile</span></a>
                </div>
            </div>

            <div class="collapse" id="user-nav">
                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="{{url('virtualoffice/my-account/profile')}}" class="nav-link">
                            <i class="icon-user-plus"></i>
                            <span>My profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-coins"></i>
                            <span>My balance</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-comment-discussion"></i>
                            <span>Messages</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-cog5"></i>
                            <span>Account settings</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('logout')}}" class="nav-link">
                            <i class="icon-switch2"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Dashboard</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="nav-link dashboard">
                        <i class="icon-home4"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- <li class="nav-item">
                    <a href="{{ url('virtualoffice/register') }}" class="nav-link register">
                        <i class="icon-home4"></i>
                        <span>Register</span>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a href="{{url('virtualoffice/my-account/convert-current-income')}}" class="nav-link "><i class="icon-users"></i> <span>Transfer Commission</span></a>
                    <!-- <ul class="nav nav-group-sub" data-submenu-title="Payout">
                        <li class="nav-item"><a href="{{url('virtualoffice/my-account/sub-account')}}" class="nav-link sub-account"> Sub Account</a></li>
                    </ul> -->
                </li>

                <!-- <li class="nav-item nav-item-submenu payout-must-open">
                    <a href="#" class="nav-link "><i class="icon-copy"></i> <span>Payout </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Payout">
                        <li class="nav-item"><a href="{{url('virtualoffice/payout/encash')}}" class="nav-link p-encash"> Encash</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/payout/payout-history')}}" class="nav-link p-payout-history"> Payout History</a></li>
                    </ul>
                </li> -->


                <li class="nav-item">
                    <a href="{{ url('virtualoffice/e-wallet') }}" class="nav-link e-wallet">
                        <i class="icon-wallet"></i>
                        <span>E-Wallet</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu encashment-must-open">
                    <a href="#" class="nav-link "><i class="icon-file-media"></i> <span>Encashment </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="My Pins">
                        <li class="nav-item"><a href="{{url('virtualoffice/encashment/encash')}}" class="nav-link encash"> Encash</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/encashment/encashment-history')}}" class="nav-link encashment-history">Encashment History</a></li>
                        <!-- <li class="nav-item"><a href="{{url('virtualoffice/encashment/redeem-gc')}}" class="nav-link gc-request">Redeem GC</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/encashment/rewards-history')}}" class="nav-link gc-history">Rewards History</a></li> -->
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu top-up-must-open">
                    <a href="#" class="nav-link "><i class="icon-bell-plus"></i> <span>Mobile Networks</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="My Pins">
                        <li class="nav-item"><a href="{{url('virtualoffice/top-up/smart')}}" class="nav-link smart"> Smart / TNT / Sun</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/top-up/globe')}}" class="nav-link globe"> Globe / TM</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/top-up/dito')}}" class="nav-link dito"> DITO</a></li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ url('virtualoffice/bills-payments') }}" class="nav-link bills-payments">
                        <i class="icon-grid3"></i>
                        <span>Bills Payments</span>
                    </a>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Genealogy</div> <i class="icon-menu" title="Genealogy"></i></li>

             
                <li class="nav-item nav-item-submenu network-must-open">
                    <a href="#" class="nav-link "><i class="icon-tree7"></i> <span>Network </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Network">
                        <li class="nav-item"><a href="{{url('virtualoffice/network/genealogy-tree')}}" class="nav-link n-genealogy-tree"> Genealogy Tree</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/network/sponsor-tree')}}" class="nav-link n-sponsor-tree"> Sponsor Tree</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/network/member-list')}}" class="nav-link n-member-list"> Member List</a></li>
                    </ul>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Booking</div> <i class="icon-menu" title="E-Commerce"></i></li>

                <li class="nav-item">
                    <a href="{{url('virtualoffice/booking/markup')}}" class="nav-link b-markup">
                        <i class="icon-collaboration"></i>
                        <span>Markup</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('virtualoffice/booking/booking-history')}}" class="nav-link b-b-history">
                        <i class="icon-history"></i>
                        <span>Booking History</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('virtualoffice/booking/booking-reports')}}" class="nav-link b-b-reports">
                        <i class="icon-graph"></i>
                        <span>Booking Reports</span>
                    </a>
                </li>

                <!-- <li class="nav-item">
                    <a href="{{ url('virtualoffice/unilevel') }}" class="nav-link n-unilevel-product">
                        <i class="icon-collaboration"></i>
                        <span>Unilevel</span>
                    </a>
                </li> -->

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">ECommerce</div> <i class="icon-menu" title="E-Commerce"></i></li>
                
                <li class="nav-item">
                    <a href="{{ url('virtualoffice/e-commerce-store') }}" class="nav-link e-commerce-store">
                        <i class="icon-store"></i>
                        <span>Buy Package Codes</span>
                    </a>
                </li>

                <!--<li class="nav-item nav-item-submenu order-details-must-open">-->
                <!--    <a href="#" class="nav-link "><i class="icon-clipboard6"></i> <span>Order Details </span></a>-->
                <!--    <ul class="nav nav-group-sub" data-submenu-title="Order Details">-->
                <!--        <li class="nav-item"><a href="{{url('virtualoffice/order-details/orders')}}" class="nav-link od-orders"> Orders</a></li>-->
                <!--        <li class="nav-item"><a href="{{url('virtualoffice/order-details/order-history')}}" class="nav-link od-order-history"> Order History</a></li>-->
                <!--        <li class="nav-item"><a href="{{url('virtualoffice/order-details/order-reports')}}" class="nav-link od-order-reports"> Order Reports</a></li>-->
                <!--    </ul>-->
                <!--</li>-->
                
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Code Management</div> <i class="icon-menu" title="E-Commerce"></i></li>
               
               

                <li class="nav-item nav-item-submenu my-pins-must-open">
                    <a href="#" class="nav-link "><i class="icon-pen2"></i> <span>My Package Pins </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="My Pins">
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-pins/paid-pins')}}" class="nav-link mp-paid-pins"> Paid Pins</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-pins/download-pins')}}" class="nav-link mp-download-pins">Download Pins</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-pins/transfer-pins')}}" class="nav-link mp-transfer-pins">Transfer Pins</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-pins/transfer-history')}}" class="nav-link mp-transfer-history">Transfer History</a></li>

                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-pins/report-pins')}}" class="nav-link mp-transfer-history">Report Pins</a></li>
                        <!-- <li class="nav-item"><a href="{{url('virtualoffice/code-management/binary-codes/free-slot-codes')}}" class="nav-link bc-free-slot-codes"> Free Slots Codes</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/binary-codes/cd-codes')}}" class="nav-link bc-cd-codes"> CD Codes</a></li> -->
                    </ul>
                </li>

                <!-- <li class="nav-item nav-item-submenu my-product-must-open">
                    <a href="#" class="nav-link "><i class="icon-pen2"></i> <span>My Product </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="My Pins">
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/product-pins')}}" class="nav-link prod-product-pins"> Product Pins</a></li>
                        {{-- <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/encode-product')}}" class="nav-link prod-encode-product">Encode Product</a></li> --}}
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/product-cart')}}" class="nav-link prod-product-cart">Product Cart</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/purchase-product-history')}}" class="nav-link prod-product-pins-history">Purchase Product History</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/transfer-pins')}}" class="nav-link prod-transfer-pins">Transfer Pins</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/transfer-pins-history')}}" class="nav-link prod-transfer-pins-history">Transfer Pins History</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/report-pins')}}" class="nav-link prod-transfer-pins-history">Report Pins</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-product/unilevel-maintenance')}}" class="nav-link prod-transfer-pins-history">Unilevel Maintenance</a></li>
                    </ul>
                </li>
 -->
                

                 
                <!-- <li class="nav-item">
                    <a href="{{ url('virtualoffice/code-management/product-codes') }}" class="nav-link cm-product-codes">
                        <i class="icon-pen2"></i>
                        <span>Product Codes</span>
                    </a>
                </li> -->

                <!-- <li class="nav-item">
                    <a href="{{ url('virtualoffice/code-management/promos') }}" class="nav-link cm-promos">
                        <i class="icon-pen2"></i>
                        <span>Promos</span>
                    </a>
                </li> -->


                <li class="nav-item">
                    <a href="{{ url('virtualoffice/code-management/activation-code') }}" class="nav-link cm-e-pin">
                        <i class="icon-pen2"></i>
                        <span>Activation Code</span>
                    </a>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Reports</div> <i class="icon-menu" title="Reports"></i></li>

                <li class="nav-item nav-item-submenu my-bonus-must-open">
                    <a href="#" class="nav-link "><i class="icon-pen2"></i> <span>My Bonus </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="My Pins">
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-bonus/direct-referral')}}" class="nav-link direct-referral"> Direct Referral</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-bonus/royalty-bonus')}}" class="nav-link indirect-referral">Royalty Bonus</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-bonus/pairing-bonus')}}" class="nav-link pairing-bonus">Pairing Bonus</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/code-management/my-bonus/markup-bonus')}}" class="nav-link unilevel-bonus">Markup Bonus</a></li>
                    </ul>
                </li>
                
                <li class="nav-item"><a class="nav-link r-top-earners" href="{{url('virtualoffice/reports/top-earners')}}"><i class="icon-statistics"></i> Top Earners</a></li>
                <li class="nav-item"><a class="nav-link r-top-recruiters" href="{{url('virtualoffice/reports/top-recruiters')}}"><i class="icon-chart"></i> Top Recruiters</a></li>
                <li class="nav-item"><a class="nav-link r-package-overview" href="{{url('virtualoffice/reports/package-overview')}}"><i class="icon-stats-growth"></i> Package Overview</a></li>
                <li class="nav-item"><a class="nav-link r-rank-overview" href="{{url('virtualoffice/reports/rank-overview')}}"><i class="icon-stats-dots"></i> Rank Overview</a></li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Settings</div> <i class="icon-menu" title="Settings"></i></li>

                <li class="nav-item nav-item-submenu tools-must-open">
                    <a href="#" class="nav-link "><i class="icon-cog4"></i> <span>Tools </span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Tools">
                        <li class="nav-item"><a href="{{url('virtualoffice/tools/download-materials')}}" class="nav-link t-download-materials"> Download Materials</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/tools/news')}}" class="nav-link t-news"> News</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/tools/faqs')}}" class="nav-link t-faqs"> FAQs</a></li>
                    </ul>
                </li>

                

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Miscellaneous</div> <i class="icon-menu" title="Miscellaneous"></i></li>

                <li class="nav-item nav-item-submenu support-center-must-open">
                    <a href="#" class="nav-link"><i class="icon-lifebuoy"></i> <span>Support Center</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Support Center">
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/tickets')}}" class="nav-link sc-ticket-dashboard"> Create Ticket</a></li>
                        <!-- <li class="nav-item"><a href="{{url('virtualoffice/support-center/ticket-dashboard')}}" class="nav-link sc-ticket-dashboard"> Ticket Dashboard</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/view-tickets')}}" class="nav-link sc-view-tickets"> View Tickets </a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/open-tickets')}}" class="nav-link sc-open-tickets"> Open Tickets</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/resolved-tickets')}}" class="nav-link sc-resolved-tickets"> Resolved Tickets </a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/category')}}" class="nav-link sc-category"> Category</a></li>
                        <li class="nav-item"><a href="{{url('virtualoffice/support-center/configuration')}}" class="nav-link sc-configuration"> Configuration</a></li> -->
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ url('virtualoffice/mailbox/inbox') }}" class="nav-link mailbox">
                        <i class="icon-envelope"></i>
                        <span>Mailbox</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>
