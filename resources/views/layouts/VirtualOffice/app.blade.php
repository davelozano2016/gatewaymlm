<?php
$session = session()->get('id');
?>
@if(!isset($session))
   <script>location.href='/'</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="turbolinks-cache-control" content="no-preview">
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta name="turbolinks-visit-control" content="reload">
    <title>{{config('app.name')}}</title>


    <!-- Global stylesheets -->
	<link href="{{ asset('VirtualOffice/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.min.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/material/styles.min.css') }}"  rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/bootstrap_limitless.min.css') }}"  rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/layout.min.css') }}"  rel="stylesheet" type="text/css">
	<link href="{{ asset('VirtualOffice/css/components.min.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('VirtualOffice/css/colors.min.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/images/mini_logo.png') }}"  rel="icon" type="image/png">
    <link href="{{ asset('assets/css/custom.css') }}"  rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/parsley/parsley.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    
    <script src="{{ asset('js/app.js') }}"  ></script>


</head>
<body onload="prettyPrint();">
    @include('layouts.VirtualOffice.top-navigation')
    
<!-- Page content -->
    <div class="page-content">
        @include('layouts.VirtualOffice.navigation')
            @yield('container')
        @include('layouts.VirtualOffice.footer')
    </div>
   
    <!-- /theme JS files -->
       
    <script src="{{ asset('assets/js/main/jquery.min.js') }}" ></script>
    <script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}" ></script>
    <script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"  ></script>
    <script src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
    <script src="{{ asset('assets/parsley/parsley.min.js') }}"></script>
    <script src="{{ asset('VirtualOffice/js/app.js') }}"  ></script>
    <script src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $('.page-header .page-title').append('<img src="{{ asset('assets/images/logo.png') }}" class="float-right gateway-logo">');
    </script>
    <script>
        function notification(message,type) {
            var SweetAlert = function () {
                var _componentSweetAlert = function() {
                    if (typeof swal == 'undefined') {
                        console.warn('Warning - sweet_alert.min.js is not loaded.');
                        return;
                    }

                    // Defaults
                    var swalInit = swal.mixin({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary',
                            cancelButtonClass: 'btn btn-light'
                        });

                    // Bottom
                    
                    
                        swalInit.fire({
                            text: message,
                            type: type,
                            showConfirmButton: true,
                        });
                };

                return {
                    initComponents: function() {
                        _componentSweetAlert();
                    }
                }
            }();


            // Initialize module
            // ------------------------------

            document.addEventListener('DOMContentLoaded', function() {
                SweetAlert.initComponents();

            });
        }
    </script>
    @yield('custom')
    @if(Session::has('message'))
    <script>
        notification('{{Session::get("message")}}','info') 
    </script>
    @endif
</body>
</html>