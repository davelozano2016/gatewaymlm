<div class="navbar navbar-expand-md navbar-dark navbar-static">
    <div class="navbar-brand">
        <a href="index.html" class="d-inline-block">
            <img src="{{ asset('assets/images/logo_light.png') }}" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                    <i class="icon-bell3"></i>
                    <span class="d-md-none ml-2">Notification</span>
                    <span class="badge badge-pill bg-danger-400 ml-auto ml-md-0">1</span>
                </a>

                <div class="dropdown-menu dropdown-content wmin-md-350">
                    <div class="dropdown-content-header">
                        <span class="font-weight-semibold">Notification</span>
                        <a href="#" class="text-default"><i class="icon-sync"></i></a>
                    </div>

                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent "><i class="icon-bell3"></i></a>
                                </div>

                                <div class="media-body">
                                    Purchased Platinum package
                                    <div class="text-muted font-size-sm">4 minutes ago</div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div class="dropdown-content-footer bg-light">
                        <a href="#" class="text-grey mr-auto">All updates</a>
                        <div>
                            <a href="#" class="text-grey" data-popup="tooltip" title="Mark all as read"><i class="icon-radio-unchecked"></i></a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

        <span class="ml-md-3 mr-md-auto"></span>

        <ul class="navbar-nav">
            

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                    <i class="icon-cart4"></i>
                    <span class="d-md-none ml-2">My Cart</span>
                    <span class="badge badge-pill bg-danger-400 ml-auto ml-md-0">2</span>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                    <div class="dropdown-content-header">
                        <span class="font-weight-semibold">My Cart</span>
                    </div>

                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            @for($i=1;$i<=1;$i++)
                            <li class="media">
                                <div class="mr-3 position-relative">
                                    <img src="{{ asset('assets/images/mikewooting.jpg') }}" width="36" height="36" class="rounded-circle" alt="">
                                </div>

                                <div class="media-body">
                                    <div class="media-title">
                                        <a href="#">
                                            <span class="font-weight-semibold">Platinum Package</span>
                                        </a>
                                        <a href="#"><span class="text-muted float-right font-size-sm"><i class="icon icon-cross"></i></span></a>
                                    </div>

                                    <span class="text-muted">Quantity: x1</span><br>
                                    <span class="text-muted">Total: {{@$query[0]->currency_symbol}}{{number_format(104999,2)}}</span>
                                </div>
                            </li>
                            @endfor
                        </ul>
                    </div>
                    <div class="dropdown-content-footer bg-light">
                        <a href="#" class="text-grey mr-auto">Grand Total</a>
                        <a href="#" class="text-grey">{{@$query[0]->currency_symbol}}{{number_format(104999,2)}}</a>
                    </div>
                </div>
            </li>

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('assets/images/mikewooting.jpg') }}" class="rounded-circle mr-2" height="34" alt="">
                    <span>{{$query[0]->firstname}} {{$query[0]->surname}}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
                    <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                    <a href="{{url('logout')}}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
