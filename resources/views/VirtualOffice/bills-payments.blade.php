@extends('layouts.VirtualOffice.app')
@section('container')


<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Top Up</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="row">
        <div class="col-xl-12">

            <div class="row">
                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_cableinternet" class="btn btn-light  btn-block btn-float m-0">
                            <img src="{{url('assets/img/bills-payments/cable-internet2.png')}}" style="width:130px;height:80px" alt="">
                            <span>Cable/Internet</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_education" class="btn btn-light  btn-block btn-float m-0">
                            <img src="{{url('assets/img/bills-payments/schools2.png')}}" style="width:130px;height:80px" alt="">
                            <span>Education</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_electricutilities" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/electricutility-black3.png')}}" style="width:130px;height:80px" alt="">
                            <span>Electric utilities</span>
                        </button>
                    </div>
                </div>    

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_loans" class="btn btn-light  btn-block btn-float m-0">
                            <img src="{{url('assets/img/bills-payments/loans1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Loans</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_creditcards" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/creditcard1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Credit cards</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_insurance" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/insurance1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Insurance</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_telecoms" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/telecoms1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Telecoms</span>               
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_airlines" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/transportation1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Airlines</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_government" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/government1.png')}}" style="width:130px;height:80px" alt="">
                            <span>Government</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_waterutilities" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/water-utilities1.png')}}" style="width:130px;height:80px" alt=""> 
                            <span>Water Utilities</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_realstate" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/real-state1.png')}}" style="width:130px;height:80px" alt=""> 
                            <span>Real State</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-3 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_collectionservice" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/healthcare1.png')}}" style="width:130px;height:80px" alt=""> 
                            <span>Collection Service</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-4 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_ecommerce" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/solution1.png')}}" style="width:130px;height:80px" alt=""> 
                            <span>Ecommerce</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-4 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_more" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/more1.png')}}" style="width:130px;height:80px" alt=""> 
                            <span>More</span>
                        </button>
                    </div>
                </div>

                <div class="col-md-4 col-6">
                    <div class="card card-body">
                        <button type="button" data-toggle="modal" data-target="#modal_donation" class="btn btn-light  btn-block btn-float m-0">                            
                            <img src="{{url('assets/img/bills-payments/favorites1.png ')}}" style="width:130px;height:80px" alt=""> 
                            <span>Donation</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('VirtualOffice.bills-payments.manila')
@include('VirtualOffice.bills-payments.sss')
@include('VirtualOffice.bills-payments.casureco')
@include('VirtualOffice.bills-payments.pag-ibig')
@include('VirtualOffice.bills-payments.pldt')
@include('VirtualOffice.bills-payments.smart')
@include('VirtualOffice.bills-payments.maynilad')
@include('VirtualOffice.bills-payments.globelines')
@include('VirtualOffice.bills-payments.ghphone')
@include('VirtualOffice.bills-payments.cablelink')
@include('VirtualOffice.bills-payments.primewater')
@include('VirtualOffice.bills-payments.suncellular')
@include('VirtualOffice.bills-payments.bayantel')
@include('VirtualOffice.bills-payments.planet-cable')
@include('VirtualOffice.bills-payments.golden-heaven')
@include('VirtualOffice.bills-payments.veco')
@include('VirtualOffice.bills-payments.davaolight')
@include('VirtualOffice.bills-payments.sta-lucia-reality')
@include('VirtualOffice.bills-payments.sumisho')
@include('VirtualOffice.bills-payments.subic-enerzone')
@include('VirtualOffice.bills-payments.norkis')
@include('VirtualOffice.bills-payments.cignal')
@include('VirtualOffice.bills-payments.unistar')
@include('VirtualOffice.bills-payments.skycable')
@include('VirtualOffice.bills-payments.beneco')
@include('VirtualOffice.bills-payments.ileco1')
@include('VirtualOffice.bills-payments.legazpi-water')
@include('VirtualOffice.bills-payments.pal')
@include('VirtualOffice.bills-payments.tabaco-water')
@include('VirtualOffice.bills-payments.dctv-cable')
@include('VirtualOffice.bills-payments.mcwd')
@include('VirtualOffice.bills-payments.pelco1')
@include('VirtualOffice.bills-payments.sta-maria-water')
@include('VirtualOffice.bills-payments.mfi-lending')
@include('VirtualOffice.bills-payments.cotabato-light')
@include('VirtualOffice.bills-payments.aeon-credit')
@include('VirtualOffice.bills-payments.pesopay')
@include('VirtualOffice.bills-payments.presco')
@include('VirtualOffice.bills-payments.maybridge')
@include('VirtualOffice.bills-payments.iconnex')
@include('VirtualOffice.bills-payments.metro-fastbet')
@include('VirtualOffice.bills-payments.first-peak')
@include('VirtualOffice.bills-payments.batelec1')
@include('VirtualOffice.bills-payments.plaridel-water')
@include('VirtualOffice.bills-payments.dragonpay')
@include('VirtualOffice.bills-payments.home-credit')
@include('VirtualOffice.bills-payments.chinatrust')
@include('VirtualOffice.bills-payments.gate')
@include('VirtualOffice.bills-payments.batelec2')
@include('VirtualOffice.bills-payments.lagunawater')
@include('VirtualOffice.bills-payments.airasia')
@include('VirtualOffice.bills-payments.baguio-water')
@include('VirtualOffice.bills-payments.bpi-creditcard')
@include('VirtualOffice.bills-payments.angeles-electric')
@include('VirtualOffice.bills-payments.bp-waterworks')
@include('VirtualOffice.bills-payments.goodhands-water')
@include('VirtualOffice.bills-payments.happywell')
@include('VirtualOffice.bills-payments.tarelco2')
@include('VirtualOffice.bills-payments.axa-phil')
@include('VirtualOffice.bills-payments.cebupacific')
@include('VirtualOffice.bills-payments.neeco2-area1')
@include('VirtualOffice.bills-payments.converge-ict')
@include('VirtualOffice.bills-payments.angeles-cable')
@include('VirtualOffice.bills-payments.central-luzon-cable')
@include('VirtualOffice.bills-payments.hitech-cable')
@include('VirtualOffice.bills-payments.primecast-cable')
@include('VirtualOffice.bills-payments.daraga-water')
@include('VirtualOffice.bills-payments.pelco2')
@include('VirtualOffice.bills-payments.aub-creditcards')
@include('VirtualOffice.bills-payments.rs-property')
@include('VirtualOffice.bills-payments.canoreco')
@include('VirtualOffice.bills-payments.inec')
@include('VirtualOffice.bills-payments.royalcable')
@include('VirtualOffice.bills-payments.honeycomb')
@include('VirtualOffice.bills-payments.pelco3')
@include('VirtualOffice.bills-payments.fleco')
@include('VirtualOffice.bills-payments.sanclemente-cable')
@include('VirtualOffice.bills-payments.golden-eagle-cable')
@include('VirtualOffice.bills-payments.calumpit-water')
@include('VirtualOffice.bills-payments.cocolife')
@include('VirtualOffice.bills-payments.tagum-water')
@include('VirtualOffice.bills-payments.mabalacat-water')
@include('VirtualOffice.bills-payments.neeco2-area2')
@include('VirtualOffice.bills-payments.philsmile')
@include('VirtualOffice.bills-payments.penelco')
@include('VirtualOffice.bills-payments.boracaywater')
@include('VirtualOffice.bills-payments.clarkwater')
@include('VirtualOffice.bills-payments.viacom')
@include('VirtualOffice.bills-payments.paramount')
@include('VirtualOffice.bills-payments.global-dominion')
@include('VirtualOffice.bills-payments.pili-water')
@include('VirtualOffice.bills-payments.insular-savers')
@include('VirtualOffice.bills-payments.sitelco')
@include('VirtualOffice.bills-payments.eprime')
@include('VirtualOffice.bills-payments.asialink')
@include('VirtualOffice.bills-payments.finaswide')
@include('VirtualOffice.bills-payments.south-asialink')
@include('VirtualOffice.bills-payments.cable-television')
@include('VirtualOffice.bills-payments.dragonloans')
@include('VirtualOffice.bills-payments.dragongames')
@include('VirtualOffice.bills-payments.cebeco2')
@include('VirtualOffice.bills-payments.soreco2')
@include('VirtualOffice.bills-payments.tagum-coop')
@include('VirtualOffice.bills-payments.payexpress')
@include('VirtualOffice.bills-payments.parasat')
@include('VirtualOffice.bills-payments.cdo-water')
@include('VirtualOffice.bills-payments.nsohelpline-com')
@include('VirtualOffice.bills-payments.cepalco')
@include('VirtualOffice.bills-payments.surseco1')
@include('VirtualOffice.bills-payments.laguna-water-district')
@include('VirtualOffice.bills-payments.easycash')
@include('VirtualOffice.bills-payments.zameco2')
@include('VirtualOffice.bills-payments.oedc')
@include('VirtualOffice.bills-payments.pera-agad')
@include('VirtualOffice.bills-payments.rdak-land')
@include('VirtualOffice.bills-payments.dasureco')
@include('VirtualOffice.bills-payments.zameco1')
@include('VirtualOffice.bills-payments.south-luzon-catv')
@include('VirtualOffice.bills-payments.jmd-cable-network')
@include('VirtualOffice.bills-payments.enterprisebank-loan')
@include('VirtualOffice.bills-payments.iseco')
@include('VirtualOffice.bills-payments.nbi')
@include('VirtualOffice.bills-payments.manilawater-philventures')
@include('VirtualOffice.bills-payments.dibble')
@include('VirtualOffice.bills-payments.celestial-meadows')
@include('VirtualOffice.bills-payments.quickpera')
@include('VirtualOffice.bills-payments.medicard')
@include('VirtualOffice.bills-payments.lima-enerzone')
@include('VirtualOffice.bills-payments.waters-ph')
@include('VirtualOffice.bills-payments.panelco1')
@include('VirtualOffice.bills-payments.loan-star')
@include('VirtualOffice.bills-payments.bicore')
@include('VirtualOffice.bills-payments.olm-financing-corp')
@include('VirtualOffice.bills-payments.connext-marketing')
@include('VirtualOffice.bills-payments.icash-express')
@include('VirtualOffice.bills-payments.rfc-loan')
@include('VirtualOffice.bills-payments.grab')
@include('VirtualOffice.bills-payments.fil-products')
@include('VirtualOffice.bills-payments.airyougo-travels')
@include('VirtualOffice.bills-payments.broadband-everywhere')
@include('VirtualOffice.bills-payments.sajelco')
@include('VirtualOffice.bills-payments.dfa')
@include('VirtualOffice.bills-payments.lipa-bank')
@include('VirtualOffice.bills-payments.noceco')
@include('VirtualOffice.bills-payments.panelco3')
@include('VirtualOffice.bills-payments.quezelco2')
@include('VirtualOffice.bills-payments.metrobank')
@include('VirtualOffice.bills-payments.angeles-water')
@include('VirtualOffice.bills-payments.bulacan-water')
@include('VirtualOffice.bills-payments.simbayanan-coop')
@include('VirtualOffice.bills-payments.baciwa')
@include('VirtualOffice.bills-payments.fuse-lending')
@include('VirtualOffice.bills-payments.casureco2')
@include('VirtualOffice.bills-payments.obando-water')
@include('VirtualOffice.bills-payments.peza')
@include('VirtualOffice.bills-payments.tieza')
@include('VirtualOffice.bills-payments.pahiram')
@include('VirtualOffice.bills-payments.ilagan-water')
@include('VirtualOffice.bills-payments.iselco1')
@include('VirtualOffice.bills-payments.quezelco1')
@include('VirtualOffice.bills-payments.dagupan-city-water')
@include('VirtualOffice.bills-payments.metro-iloilo-water')
@include('VirtualOffice.bills-payments.pami-insurance')
@include('VirtualOffice.bills-payments.eagle-vision-inc')
@include('VirtualOffice.bills-payments.excel-cable')
@include('VirtualOffice.bills-payments.shama-broadband-and-catv')
@include('VirtualOffice.bills-payments.trend-cable')
@include('VirtualOffice.bills-payments.megasaver')
@include('VirtualOffice.bills-payments.robinsonbank-creditcard')
@include('VirtualOffice.bills-payments.sanagustinrealty-sancdepaz')
@include('VirtualOffice.bills-payments.acom')
@include('VirtualOffice.bills-payments.sfelapco')
@include('VirtualOffice.bills-payments.galaxy-cable')
@include('VirtualOffice.bills-payments.lazada')
@include('VirtualOffice.bills-payments.marina')
@include('VirtualOffice.bills-payments.pacific-ace-loan')
@include('VirtualOffice.bills-payments.subic-water')
@include('VirtualOffice.bills-payments.nuvelco')
@include('VirtualOffice.bills-payments.gentrias-cable')
@include('VirtualOffice.bills-payments.skypay')
@include('VirtualOffice.bills-payments.pisopay')
@include('VirtualOffice.bills-payments.cashalo')
@include('VirtualOffice.bills-payments.johndorf-ventures')
@include('VirtualOffice.bills-payments.moresco1')
@include('VirtualOffice.bills-payments.cebeco3')
@include('VirtualOffice.bills-payments.u-peso-lending')
@include('VirtualOffice.bills-payments.peco')
@include('VirtualOffice.bills-payments.decorp')
@include('VirtualOffice.bills-payments.integranet-network-services')
@include('VirtualOffice.bills-payments.luelco')
@include('VirtualOffice.bills-payments.loadxtreme')
@include('VirtualOffice.bills-payments.payexpress-loans')
@include('VirtualOffice.bills-payments.payexpress-tickets')
@include('VirtualOffice.bills-payments.dragonschools')
@include('VirtualOffice.bills-payments.nwow-marketing')
@include('VirtualOffice.bills-payments.pal-contractcenter')
@include('VirtualOffice.bills-payments.shopee')
@include('VirtualOffice.bills-payments.tagaytay-broadband-and-catv')
@include('VirtualOffice.bills-payments.cvm-finance')
@include('VirtualOffice.bills-payments.filinvest-water')
@include('VirtualOffice.bills-payments.umbrella')
@include('VirtualOffice.bills-payments.idol-mo')
@include('VirtualOffice.bills-payments.tagaytay-water')
@include('VirtualOffice.bills-payments.motorstar')
@include('VirtualOffice.bills-payments.parajet')
@include('VirtualOffice.bills-payments.securitybank-mastercard')
@include('VirtualOffice.bills-payments.pitakamo')
@include('VirtualOffice.bills-payments.fami')
@include('VirtualOffice.bills-payments.afterwest-microloans')
@include('VirtualOffice.bills-payments.easytv')
@include('VirtualOffice.bills-payments.balanga-water')
@include('VirtualOffice.bills-payments.cagelco1')
@include('VirtualOffice.bills-payments.libmanan-water')
@include('VirtualOffice.bills-payments.global-sme-loans')
@include('VirtualOffice.bills-payments.neeco1')
@include('VirtualOffice.bills-payments.lendpinoy')
@include('VirtualOffice.bills-payments.afc-sme-finance')
@include('VirtualOffice.bills-payments.emcor')
@include('VirtualOffice.bills-payments.ctfsi')
@include('VirtualOffice.bills-payments.kservico')
@include('VirtualOffice.bills-payments.ksprime')
@include('VirtualOffice.bills-payments.cenpelco')
@include('VirtualOffice.bills-payments.iligan-light')
@include('VirtualOffice.bills-payments.andali')
@include('VirtualOffice.bills-payments.now-corp')
@include('VirtualOffice.bills-payments.cbn-asia')
@include('VirtualOffice.bills-payments.operation-blessing')
@include('VirtualOffice.bills-payments.asian-center-for-mission')
@include('VirtualOffice.bills-payments.dinalupihan-water')
@include('VirtualOffice.bills-payments.country-funders')
@include('VirtualOffice.bills-payments.ideal')
@include('VirtualOffice.bills-payments.motorcentral')
@include('VirtualOffice.bills-payments.prime-loan-plus')
@include('VirtualOffice.bills-payments.benlify')
@include('VirtualOffice.bills-payments.bulakan-water')
@include('VirtualOffice.bills-payments.home-outlet')
@include('VirtualOffice.bills-payments.give-a-love')
@include('VirtualOffice.bills-payments.gtsi')
@include('VirtualOffice.bills-payments.abra')
@include('VirtualOffice.bills-payments.mitsukoshi')
@include('VirtualOffice.bills-payments.calasiao-water')
@include('VirtualOffice.bills-payments.zurich')
@include('VirtualOffice.bills-payments.jaccs')
@include('VirtualOffice.bills-payments.active-realty')
@include('VirtualOffice.bills-payments.yy-kredit')
@include('VirtualOffice.bills-payments.makati-finance')
@include('VirtualOffice.bills-payments.chailease-berjaya')
@include('VirtualOffice.bills-payments.pay-2c2p-plus')
@include('VirtualOffice.bills-payments.resellee')
@include('VirtualOffice.bills-payments.agoda')
@include('VirtualOffice.bills-payments.golden-future-lifeplans')
@include('VirtualOffice.bills-payments.cropital')
@include('VirtualOffice.bills-payments.san-miguel-foods')
@include('VirtualOffice.bills-payments.san-miguel-mills')
@include('VirtualOffice.bills-payments.purefoods-hormel')
@include('VirtualOffice.bills-payments.food-crave-marketing')
@include('VirtualOffice.bills-payments.magnolia')
@include('VirtualOffice.bills-payments.binangonan-cable')
@include('VirtualOffice.bills-payments.kabayan-cable')
@include('VirtualOffice.bills-payments.skyline-catv')
@include('VirtualOffice.bills-payments.citi-appliance')
@include('VirtualOffice.bills-payments.cashbee')
@include('VirtualOffice.bills-payments.orico')
@include('VirtualOffice.bills-payments.cepat-kredit')
@include('VirtualOffice.bills-payments.pesomat')
@include('VirtualOffice.bills-payments.encore')
@include('VirtualOffice.bills-payments.aqua-centro')
@include('VirtualOffice.bills-payments.carousell')
@include('VirtualOffice.bills-payments.unionbank-quickloans')
@include('VirtualOffice.bills-payments.firstmetrosec')
@include('VirtualOffice.bills-payments.mysuperpay')
@include('VirtualOffice.bills-payments.flexi-finance')
@include('VirtualOffice.bills-payments.mentors')
@include('VirtualOffice.bills-payments.phinma-education')
@include('VirtualOffice.bills-payments.agribank')
@include('VirtualOffice.bills-payments.truemoney')
@include('VirtualOffice.bills-payments.insular-life')
@include('VirtualOffice.bills-payments.par-pahiram')
@include('VirtualOffice.bills-payments.asian-vision-cable')
@include('VirtualOffice.bills-payments.card-inc')
@include('VirtualOffice.bills-payments.card-mri-rizal-bank')
@include('VirtualOffice.bills-payments.card-sme-bank')
@include('VirtualOffice.bills-payments.card-bank-inc')
@include('VirtualOffice.bills-payments.first-bay-power')
@include('VirtualOffice.bills-payments.teresa-waterworks')
@include('VirtualOffice.bills-payments.personal-collection')
@include('VirtualOffice.bills-payments.hermosa-water-district')
@include('VirtualOffice.bills-payments.forest-lake')
@include('VirtualOffice.bills-payments.zamcelco')
@include('VirtualOffice.bills-payments.cagelco2')
@include('VirtualOffice.bills-payments.calamba-institute')
@include('VirtualOffice.bills-payments.icct-colleges')
@include('VirtualOffice.bills-payments.cebu-cfi-coop')
@include('VirtualOffice.bills-payments.first-standard')
@include('VirtualOffice.bills-payments.metro-roxas-water-district')
@include('VirtualOffice.bills-payments.dungganon')
@include('VirtualOffice.bills-payments.dasca-cable')
@include('VirtualOffice.bills-payments.tarelco1')
@include('VirtualOffice.bills-payments.tala')
@include('VirtualOffice.bills-payments.national-teachers-college')
@include('VirtualOffice.bills-payments.generali-ph')
@include('VirtualOffice.bills-payments.pa-properties')
@include('VirtualOffice.bills-payments.novadeci')
@include('VirtualOffice.bills-payments.cable-internet')
@include('VirtualOffice.bills-payments.education')
@include('VirtualOffice.bills-payments.electric')
@include('VirtualOffice.bills-payments.loans')
@include('VirtualOffice.bills-payments.credit-cards')
@include('VirtualOffice.bills-payments.insurance')
@include('VirtualOffice.bills-payments.telecoms')
@include('VirtualOffice.bills-payments.transportation')
@include('VirtualOffice.bills-payments.government')
@include('VirtualOffice.bills-payments.water-utilities')
@include('VirtualOffice.bills-payments.real-state')
@include('VirtualOffice.bills-payments.favorites')
@include('VirtualOffice.bills-payments.memorial-plan')
@include('VirtualOffice.bills-payments.airlines')
@include('VirtualOffice.bills-payments.payment-gateway')
@include('VirtualOffice.bills-payments.more')
@include('VirtualOffice.bills-payments.fund-transfer')
@include('VirtualOffice.bills-payments.donation')
@include('VirtualOffice.bills-payments.electronic-cash')
@include('VirtualOffice.bills-payments.ecommerce')
@include('VirtualOffice.bills-payments.collection-service')



@section('custom')
<script>
    $('a.bills-payments').addClass('active');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection