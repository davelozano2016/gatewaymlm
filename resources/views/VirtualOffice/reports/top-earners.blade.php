@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Account</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <button onclick="search()" class="btn btn-primary float-right">Search</button>
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="text-uppercase table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>Full Name</th>
                                        <th>Username</th>
                                        <th>Left Team</th>
                                        <th>Right Team</th>
                                        <th> Income</th>
                                        <th>Date Joined</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="#">
        @csrf
        <div class="modal" id="modal_top_earners" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Search Top Earners</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">From:</label>
                            <div class="col-lg-10">
                                <input type="text" name="" class="form-control daterange startdate">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">To:</label>
                            <div class="col-lg-10">
                                <input type="text" name="" class="form-control daterange enddate">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.r-top-earners').addClass('active');
    $('li.reports-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
        });

        if($('.startdate, .enddate').length){
            // check if element is available to bind ITS ONLY ON HOMEPAGE
            var currentDate = moment().format("DD-MM-YYYY");

            $('.startdate, .enddate').daterangepicker({
                locale: {
                      format: 'DD-MM-YYYY'
                },
                "alwaysShowCalendars": true,
                autoApply: true,
                autoUpdateInput: false
            }, function(start, end, label) {
              // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
              // Lets update the fields manually this event fires on selection of range
              var selectedStartDate = start.format('DD-MM-YYYY'); // selected start
              var selectedEndDate = end.format('DD-MM-YYYY'); // selected end

              $checkinInput = $('.startdate');
              $checkoutInput = $('.enddate');

              // Updating Fields with selected dates
              $checkinInput.val(selectedStartDate);
              $checkoutInput.val(selectedEndDate);

              // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
              var checkOutPicker = $checkoutInput.data('daterangepicker');
              checkOutPicker.setStartDate(selectedStartDate);
              checkOutPicker.setEndDate(selectedEndDate);

              // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
              var checkInPicker = $checkinInput.data('daterangepicker');
              checkInPicker.setStartDate(selectedStartDate);
              checkInPicker.setEndDate(selectedEndDate);

            });

        }
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    <script>
    function transferEwallet(users_id, user_wallet_fund, firstname, surname, username) {
        $('#fund_transfer_modal').modal()
        $('#users_id').val(users_id)
        $('#user_wallet_fund').val(user_wallet_fund)
        $('#transfer_to').val(firstname + ' ' + surname + ' (' + username + ')')
    }

    function search() {
        $('#modal_top_earners').modal()
    }
    </script>
    @endsection
    @endsection