@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Account</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->

	<!-- Cover area -->
    <div class="profile-cover">
        <div class="profile-cover-img" style="background-image: url(https://root-nation.com/wp-content/uploads/2019/10/How-to-Start-Forex-Trading-01.jpeg)"></div>
        <div class="media align-items-center text-center text-lg-left flex-column flex-lg-row m-0">
            <div class="mr-lg-3 mb-2 mb-lg-0">
                <a href="#" class="profile-thumb">
                    <img src="{{asset('assets/images/mikewooting.jpg')}}" class="border-white rounded-circle" width="48" height="48" alt="">
                </a>
            </div>

            <div class="media-body text-white">
                <h1 class="mb-0">{{$query[0]->firstname.' '.$query[0]->surname}}</h1>
                <span class="d-block">{{date('F j, Y',strtotime($query[0]->date_registered))}}</span>
            </div>

        </div>
    </div>
    <!-- /cover area -->

    <!-- Profile navigation -->
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-second">
                <i class="icon-menu7 mr-2"></i>
                Profile navigation
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-second">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a href="#profile" class="navbar-nav-link active" data-toggle="tab">
                        <i class="icon-user mr-2"></i>
                        Profile
                    </a>
                </li>
             
            </ul>

            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-stack-text mr-2"></i>
                        Notes
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-collaboration mr-2"></i>
                        Friends
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-images3 mr-2"></i>
                        Photos
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear"></i>
                        <span class="d-lg-none ml-2">Settings</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-image2"></i> Update cover</a>
                        <a href="#" class="dropdown-item"><i class="icon-clippy"></i> Update info</a>
                        <a href="#" class="dropdown-item"><i class="icon-make-group"></i> Manage sections</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-three-bars"></i> Activity log</a>
                        <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Profile settings</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /profile navigation -->


    <!-- Content area -->
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-stretch align-items-lg-start flex-column flex-lg-row">

            <!-- Left content -->
            <div class="tab-content w-100 order-2 order-lg-1">
                <div class="tab-pane active" id="profile">

                    <!-- Profile info -->
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" data-parsley-validate action="{{url('virtualoffice/my-account/update_distributor/'.Crypt::encryptString($query[0]->users_id))}}">
                                @csrf
                                <h5 class="title ">Personal Information</h5>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="firstname" readonly value="{{@$query[0]->firstname}}" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Middle Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="middlename" readonly value="{{@$query[0]->middlename}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="surname" readonly value="{{@$query[0]->surname}}" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Nickname</label>
                                    <div class="col-md-9">
                                        <input type="text" value="{{@$query[0]->nickname}}" name="nickname" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Birthdate</label>
                                    <div class="col-md-9">
                                        <input type="date" value="{{@$query[0]->birthdate}}" name="birthdate" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Civil Status</label>
                                    <div class="col-md-9">
                                        <select name="civil_status" class="form-control form-control-select2">
                                            <option value="Single" {{@$query[0]->civil_status == 'Single' ? 'selected' : ''}}>Single</option>
                                            <option value="Married" {{@$query[0]->civil_status == 'Married' ? 'selected' : ''}}>Married</option>
                                            <option value="Widowed" {{@$query[0]->civil_status == 'Widowed' ? 'selected' : ''}}>Widowed</option>
                                            <option value="Divorced" {{@$query[0]->civil_status == 'Divorced' ? 'selected' : ''}}>Divorced</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Gender</label>
                                    <div class="col-md-9">
                                        <select name="gender" class="form-control form-control-select2">
                                            <option value="Male" {{@$query[0]->gender == 'Male' ? 'selected' : ''}}>Male</option>
                                            <option value="Female" {{@$query[0]->gender == 'Female' ? 'selected' : ''}}>Female</option>
                                            <option value="Others" {{@$query[0]->gender == 'Others' ? 'selected' : ''}}>Others</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Nationality</label>
                                    <div class="col-md-9">
                                        <input type="text" name="nationality" value="{{@$query[0]->nationality}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Tax Identification Number</label>
                                    <div class="col-md-9">
                                        <input type="text" name="tin" value="{{@$query[0]->tin}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Address Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Country</label>
                                    <div class="col-md-9">
                                        <input type="text" readonly value="{{@$query[0]->country_name}}" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">State / Province</label>
                                    <div class="col-md-9">
                                        <input type="text" name="province" value="{{@$query[0]->province}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">City</label>
                                    <div class="col-md-9">
                                        <input type="text" name="city" value="{{@$query[0]->city}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" name="address" value="{{@$query[0]->address}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Zip Code</label>
                                    <div class="col-md-9">
                                        <input type="text" name="zipcode" value="{{@$query[0]->zipcode}}" class="form-control">
                                    </div>
                                </div>
                                <h5 class="title ">Contact Information</h5>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Email Address</label>
                                    <div class="col-md-9">
                                        <input type="email" value="{{@$query[0]->email}}" name="email"
                                            class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Contact Number</label>
                                    <div class="col-md-9">
                                        <input type="text" name="contact_number" value="{{@$query[0]->contact_number}}" readonly class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Landline</label>
                                    <div class="col-md-9">
                                        <input type="text" name="landline" value="{{@$query[0]->landline}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Fax</label>
                                    <div class="col-md-9">
                                        <input type="text" name="fax" value="{{@$query[0]->fax}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Beneficiary Information</h5>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Full Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" value="{{@$query[0]->name}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Relationship</label>
                                    <div class="col-md-9">
                                        <input type="text" name="relationship" value="{{@$query[0]->relationship}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Bank Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Bank Name</label>
                                    <div class="col-md-9">
                                        <select name="banks_id" class="form-control form-control-select2" id="">
                                            @foreach($banks as $bank)
                                                <option value="{{Crypt::encryptString($bank->id)}}"
                                                @if($bank->id == $query[0]->banks_id)
                                                    selected
                                                @endif    
                                                >{{@$bank->banks}}</option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Branch</label>
                                    <div class="col-md-9">
                                        <input type="text" name="branch" value="{{@$query[0]->branch}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Account Name</label>
                                    <div class="col-md-9">
                                        <input type="text" value="{{@$query[0]->account_name}}" name="account_name" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Account Number</label>
                                    <div class="col-md-9">
                                        <input type="text" value="{{@$query[0]->account_number}}" name="account_number" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Social Media Information</h5>
                                <?php $i=0;?>
                                @foreach($social_media as $sm)
                                    <input type="hidden" name="social_media_id[]" value="{{Crypt::encryptString($sm->id)}}">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{@$sm->social_media_name}}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="url[]" value="{{@$social_media_accounts[$i]->url}}" class="form-control">
                                        </div>
                                    </div>
                                    <?php $i++?>
                                @endforeach

                                <h5 class="title ">Account Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Username</label>
                                    <div class="col-md-9">
                                        <input type="text" required name="username" readonly value="{{@$query[0]->username}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Password</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <input type="password" required  value="{{Crypt::decryptString($query[0]->password)}}"
                                                id="password" name="password" class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text"><a style="cursor:pointer"><i
                                                            onclick="unmask()" id="unmask"
                                                            class="icon-eye"></i></a></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Status </label>
                                    <div class="col-md-9">
                                        <input type="text" required name readonly value="{{@$query[0]->status == 0 ? 'Inactive' : 'Active'}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Security Code</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <input type="text" id="clipboard" value="{{@$query[0]->security_code}}"
                                                readonly class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text"><a style="cursor:pointer"><i
                                                            onclick="clipboard()" class="icon-copy4"></i></a></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm float-right">Update Account Information</button>

                            </form>
                        </div>
                    </div>
                    <!-- /profile info -->

                </div>
            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-none order-1 order-lg-2 sidebar-expand-lg">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Navigation -->
                    <div class="card">
                        <div class="card-header bg-transparent">
                            <span class="card-title font-weight-semibold">My Details</span>
                        </div>

                        <ul class="nav nav-sidebar">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-user"></i>
                                     My profile
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-cash3"></i>
                                    Balance
                                    <span class="text-muted font-size-sm font-weight-normal ml-auto">$1,430</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-tree7"></i>
                                    Connections
                                    <span class="badge badge-danger badge-pill ml-auto">29</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-users"></i>
                                    Friends
                                </a>
                            </li>

                            <li class="nav-item-divider"></li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-calendar3"></i>
                                    Events
                                    <span class="badge badge-teal badge-pill ml-auto">48</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-cog3"></i>
                                    Account settings
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /navigation -->



                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
    <!-- /content area -->


    
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.sub-account').addClass('active');
    $('li.my-account-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script>
        
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration
        $('.datatable-responsive').DataTable();

    function unmask() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmask').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmask').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function unmasksub() {
        var x = document.getElementById("sub_account_password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmasksub').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmasksub').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function clipboard() {
        /* Get the text field */
        var copyText = document.getElementById("clipboard");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }

    </script>
    
    @endsection
    @endsection