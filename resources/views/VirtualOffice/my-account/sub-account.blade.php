@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Account</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>User ID</th>
                                        <th>Username</th>
                                        <th>Current Income</th>
                                        <th>Current Wallet</th>
                                        <th>Package</th>
                                        <th>Rank</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1?>
                                    @foreach($sub_accounts as $account)
                                    @if(empty($commission_deduction[$account->id_number]))
                                        @php
                                            $cd = 0;
                                        @endphp
                                    @else
                                        @php
                                            $cd = $commission_deduction[$account->id_number];
                                        @endphp
                                    @endif
                                    <tr>
                                        <td>{{$i++}}</td>
                                       
                                        <td>{{$account->id_number}}</td>
                                        <td>{{$account->username}}</td>
                                        <td style="width:10%">{{$account->currency_symbol}}{{number_format($account->total_current_income,2)}}</td>
                                        <td style="width:10%">{{$account->currency_symbol}}{{number_format($account->fund,2)}}</td>
                                        <td>
                                            @if($account->entry == 'Silver')
                                            <label class="text-uppercase badge badge-primary">{{$account->entry}}</label>
                                            @elseif($account->entry == 'Gold')
                                            <label class="text-uppercase badge badge-info">{{$account->entry}}</label>
                                            @else 
                                            <label class="text-uppercase badge badge-success">{{$account->entry}}</label>
                                            @endif
                                        </td>
                                        <td><label class="text-uppercase badge badge-success">Affiliate</label></td>
                                        <td>
                                            @if($account->status_type == 0)
                                            PAID
                                            @elseif($account->status_type == 1)
                                            CD
                                            @elseif($account->status_type == 2)
                                            FREE
                                            @endif
                                        </td>
                                        <td style="width:1px" class="text-center">
                                            <a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i
                                                    class="icon-gear"></i></a>
                                            <div class="dropdown-menu">
                                                @if($account->total_current_income < $controls->minimum_encash_request)
                                                <a class="text-muted dropdown-item"><i class="icon-reset"></i> Convert to E-Wallet</a>
                                                @else
                                                <a href="javascript:(void)" onclick="currentToEwallet('{{Crypt::encryptString($account->users_id)}}','{{$account->total_current_income}}','{{$controls->tax}}','{{$controls->maintenance}}','{{$controls->internal_processing_fee}}','{{$account->fund}}','{{$cd}}')" class="dropdown-item"><i class="icon-reset"></i> Convert to E-Wallet</a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <form method="post" data-parsley-validate action="{{ url('virtualoffice/my-account/sub-account/transfer-wallet')}}">
        @csrf
        <div class="modal" id="fund_transfer_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer E-Wallet Fund</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="{{$wallet_balance[0]->fund}}" name="wallet_balance">
                        <input type="hidden" id="users_id" name="users_id">
                        <input type="hidden" id="user_wallet_fund" name="user_wallet_fund">
                        <div class="form-group row">
                            <label class="col-lg-12 col-form-label">E-Wallet Balance:</label>
                            <div class="col-lg-12">
                                <input type="text" readonly value="{{$account->currency_symbol}}{{number_format($wallet_balance[0]->fund,2)}}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 col-form-label">Transfer To:</label>
                            <div class="col-lg-12">
                                <input type="text" readonly id="transfer_to" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 col-form-label">Amount:</label>
                            <div class="col-lg-12">
                                <input type="text" name="amount" class="form-control" data-parsley-pattern="^\d+(\.\d{2})?$" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Transfer</button>
                    </div>

                </div>
            </div>
        </div>
    </form>

    <form method="post" data-parsley-validate action="{{ url('virtualoffice/my-account/sub-account/convert-income-to-wallet')}}">
        @csrf
        <input type="hidden" name="users_id" id="c_users_id">
        <input type="hidden" name="tax" id="c_tax">
        <input type="hidden" name="maintenance" id="c_maintenance">
        <input type="hidden" name="processing_fee" id="c_internal_processing_fee">
        <input type="hidden" name="income_to_convert" id="c_income_to_convert">
        <input type="hidden" name="income_original" id="c_income_original">
        <input type="hidden" name="user_wallet_fund" id="c_user_wallet_fund">
        <div class="modal" id="convert_income_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Convert Income to E-Wallet</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <label class="col-form-label float-left">Current Balance:</label>
                                    <label class="col-form-label float-right">{{$query[0]->currency_symbol}}<span id="convert_total_income"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12 showCD">
                                <div>
                                    <label class="col-form-label float-left">Commission Deduction: (<span id="deduction"></span>)</label>
                                    <label class="col-form-label float-right text-danger"> - {{$query[0]->currency_symbol}}<span id="convert_cd"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12 showSubTotal">
                                <div>
                                    <label class="col-form-label float-left">Sub Total:</label>
                                    <label class="col-form-label float-right text-success">{{$query[0]->currency_symbol}}<span id="convert_subtotal"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div>
                                    <label class="col-form-label float-left">Tax: (<span id="tax_convert"></span>)</label>
                                    <label class="col-form-label float-right text-danger"> - {{$query[0]->currency_symbol}}<span id="convert_tax"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div>
                                    <label class="col-form-label float-left">Maintenance:</label>
                                    <label class="col-form-label float-right text-danger"> - {{$query[0]->currency_symbol}}<span id="convert_maintenance"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div>
                                    <label class="col-form-label float-left">Processing Fee:</label>
                                    <label class="col-form-label float-right text-danger"> - {{$query[0]->currency_symbol}}<span id="convert_processing_fee"></span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div>
                                    <label class="col-form-label float-left">Conversion Total:</label>
                                    <label class="col-form-label float-right text-success">{{$query[0]->currency_symbol}}<span id="convert_total"></span></label>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Convert</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.sub-account').addClass('active');
    $('li.my-account-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script>
        
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration
        $('.datatable-responsive').DataTable();

        function transferEwallet(users_id,user_wallet_fund,firstname,surname,username) {
            $('#fund_transfer_modal').modal()
            $('#users_id').val(users_id)
            $('#user_wallet_fund').val(user_wallet_fund)
            $('#transfer_to').val(firstname+' '+surname+' ('+username+')')
        }

        function currentToEwallet(users_id,current_income,tax,maintenance,internal_processing_fee,ewallet_fund,cd) {
            $('#convert_income_modal').modal()
            $('#users_id').val(users_id)
            if(cd > 0) {
                $('.showCD,.showSubTotal').attr('hidden',false)
            } else {
                $('.showCD,.showSubTotal').attr('hidden',true)
            }

            var subtotal,a,commission_deduction
            a = current_income * (cd / 100);
                commission_deduction = current_income - a;

            if(cd > 0) {
                
                subtotal = commission_deduction.toFixed(2);
                
            } else {
                subtotal =  current_income;
            }



            var tax = Number(tax)/100;
            var maintenance = Number(maintenance).toFixed(2);
            var internal_processing_fee = Number(internal_processing_fee).toFixed(2);
            var deduct =  Number(subtotal * tax).toFixed(2) ;
            var total  = subtotal - deduct - maintenance - internal_processing_fee;
            

            $('#convert_total_income').html(numberWithCommas(current_income))
            $('#convert_tax').html(numberWithCommas(deduct))
            $('#convert_maintenance').html(numberWithCommas(maintenance))
            $('#convert_processing_fee').html(numberWithCommas(internal_processing_fee))
            $('#convert_total').html(numberWithCommas(total.toFixed(2)))
            $('#deduction').html(cd+'%')
            $('#tax_convert').html(tax*100+'%')

            $('#convert_cd').html(cd == 0 ? 0 : numberWithCommas(a.toFixed(2)))
            $('#convert_subtotal').html(numberWithCommas(subtotal))

            $('#c_users_id').val(users_id);
            $('#c_tax').val(deduct);
            $('#c_maintenance').val(maintenance);
            $('#c_internal_processing_fee').val(internal_processing_fee);
            $('#c_user_wallet_fund').val(ewallet_fund);
            $('#c_income_to_convert').val(total);
            $('#c_income_original').val(current_income);
            

            $('#total_amount').val(total);
        }

        function numberWithCommas(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }


    </script>
    
    @endsection
    @endsection