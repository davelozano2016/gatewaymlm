@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Product</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-md-9">

                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table text-nowrap">
                                        <thead>
                                            <tr>
                                                <th style="width:1px;">#</th>
                                                <th>Activation Code</th>
                                                <th>Product</th>
                                                <th class="text-right" style="width:1px">UPV</th>
                                                <th class="text-right" style="width:1px">BPV</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $upv = 0;
                                            $bpv = 0;      
                                            ?>                                                                                                                                                                                                    
                                            @foreach($cart as $key => $value) 
                                                <?php 
                                                $upv += $value[0]->upv;
                                                $bpv += $value[0]->bpv;
                                                ?>
                                                <tr>
                                                    <td><a href="{{url('virtualoffice/code-management/my-product/product-cart/remove-item-from-cart')}}/{{Crypt::encryptString($value[0]->id)}}"><i class="icon icon-trash"></i></a></td>
                                                    <td>{{$value[0]->code}}</td>
                                                    <td>{{$value[0]->product_title}}</td>
                                                    <td class="text-right">{{$value[0]->upv}}</td>
                                                    <td class="text-right">{{$value[0]->bpv}}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="3" class=""></td>
                                                <td class="text-right">{{number_format($upv,2)}}</td>
                                                <td class="text-right">{{number_format($bpv,2)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-body ">
                        <form method="POST" action="{{url('virtualoffice/code-management/my-product/product-cart/unilevel')}}" id="demo-form" data-parsley-validate>
                            @csrf
                            <div class="form-group">
                                <label for="">Position</label>
                                <select name="position" class="form-control">
                                    <option value="0">Left</option>
                                    <option value="1">Right</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="g-recaptcha btn btn-primary btn-block" data-sitekey="6Ld-p_AaAAAAADBsBt4agSjazxyqABh3ElQpSu4h" data-callback='onSubmit' data-action='submit' >Encode Product</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.prod-product-cart').addClass('active');
    $('li.my-product-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        });    
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
       function onSubmit(token) {
         document.getElementById("demo-form").submit();
       }
     </script>
    @endsection
    @endsection