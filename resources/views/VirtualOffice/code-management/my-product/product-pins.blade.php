@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Product</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-3 col-12">
                <div class="card card-body">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-info icon-list3 icon-2x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">{{$used}}</h3>
                            <span class="text-uppercase font-size-sm">Used Codes</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card card-body">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-warning icon-list3 icon-2x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">{{$unused}}</h3>
                            <span class="text-uppercase font-size-sm">Unused Codes</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card card-body">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-success icon-list3 icon-2x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">{{$available}}</h3>
                            <span class="text-uppercase font-size-sm">Available</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card card-body">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-warning icon-list3 icon-2x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">{{$pending}}</h3>
                            <span class="text-uppercase font-size-sm">Pending</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif
            <form method="POST" action="{{url('virtualoffice/code-management/my-product/product-pins/add-to-cart')}}">
                @csrf
                <div class="card">
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table  class="table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px;">#</th>
                                        <th style="width:1px;"><input type="checkbox" id="checkAll"></th>
                                        <th>Activation Code</th>
                                        <th>Product</th>
                                        <th>Date Created</th>
                                        <th style="width:1px">UPV</th>
                                        <th style="width:1px">BPV</th>
                                        <th style="width:1px">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1?>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>
                                            @if($product->status == 0)
                                            <input type="checkbox" name="product_pin_id[]" value="{{Crypt::encryptString($product->id)}}">
                                            @else
                                            @endif
                                        </td>
                                        <td>{{$product->activation_code}}</td>
                                        <td>{{$product_name[$product->product_description_id]->product_title}}</td>
                                        <td>{{date('Y-m-d',strtotime($product->date_created))}}</td>
                                        <td>{{number_format($product->unilevel_point_value,2)}}</td>
                                        <td>{{number_format($product->binary_point_value,2)}}</td>
                                        <td>
                                            @if($product->status == 0)
                                                <label class="badge badge-success text-uppercase">Unused</label>
                                            @else
                                                <label class="badge badge-info text-uppercase">Used</label>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary flat">Add To Cart</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <form method="post">
        <div class="modal" id="transfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-slate">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer Single Pin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Activation Code:</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" value="Test" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Send Code To:</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Password:</label>
                            <div class="col-lg-8">
                                <input type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Send Code</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.prod-product-pins').addClass('active');
    $('li.my-product-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    @endsection
@endsection