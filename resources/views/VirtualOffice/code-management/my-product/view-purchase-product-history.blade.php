@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item ">Paid Codes</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <!-- Traffic sources -->
            <div class="col-12 col-md-12">
                <div class="card">
                    <div class=" ">
                        <div class="table-responsive">
                            <table class="table ">
                                <tr>
                                    <th style="width:40%">Customer Number</th>
                                    <td>{{$details[0]->customer_id}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Full Name</th>
                                    <td>{{$details[0]->full_name}} <a href="">{{$details[0]->distributor_id}}</a>
                                    </td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Order Number</th>
                                    <td>{{$details[0]->order_number}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Date Processed</th>
                                    <td>{{$details[0]->date_processed}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Date Expiration</th>
                                    <td>{{$details[0]->date_expiration}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Total Entry Packages</th>
                                    <td>{{$details[0]->items}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Total Price</th>
                                    <td>{{$details[0]->currency_symbol}}{{number_format($details[0]->total,2)}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Status</th>
                                    <td>
                                        @if($details[0]->order_status == 'To Pay')
                                            <span class="badge badge-warning">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'To Ship')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'To Pickup')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'To Receive')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'Completed')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'Cancelled')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @elseif($details[0]->order_status == 'Return Refund')
                                            <span class="badge badge-primary">{{$details[0]->order_status}}</span>
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Mode of  Payment</th>
                                    <td>{{$details[0]->method_of_payment}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">IP Address</th>
                                    <td>{{$details[0]->registered_ip}}</td>
                                </tr>

                                <tr>
                                    <th style="width:40%">Representative</th>
                                    <td>{{$details[0]->created_by}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="col-12 col-md-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-columned datatable-responsive">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Product</th>
                                    <th>Activation Code</th>
                                    <th style="width:1px">UPV</th>
                                    <th style="width:1px">BPV</th>
                                    <th class="text-center" style="width:1px">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($details[0]->order_status == 'Cancelled')
                                @else
                                <?php $i=1;?>
                                @foreach($product_history as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->product_title}}</td>
                                    <td>{{$data->activation_code}}</td>
                                    <td>{{$data->unilevel_point_value}}</td>
                                    <td>{{$data->binary_point_value}}</td>
                                    <td>
                                        @if($data->status == 0)
                                        <a class="badge badge-success text-white text-uppercase">Unused</a>
                                        @else
                                        <a class="badge badge-info text-white text-uppercase">Used</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>






    <!-- /content area -->
    @section('custom')
    <script>
        $('a.prod-product-pins-history').addClass('active');
        $('li.my-product-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    @endsection
    @endsection