@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Product</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="card">
            <div class="card-body ">
                <div class="table-responsive text-uppercase">
                    <table class="table table-columned datatable-responsive">
                        <thead>
                            <tr>
                                <th style="width:1px">#</th>
                                <th>Product Title</th>
                                <th>Product Code</th>
                                <th>Category</th>
                                <th style="width:1px">UPV</th>
                                <th class="text-center">AUto Order</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1?>
                            @foreach($entries as $entry)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$entry->product_title}}</td>
                                <td>{{$entry->product_code}}</td>
                                <td>{{$entry->category}}</td>
                                <td>{{number_format($entry->unilevel_points,2)}}</td>
                                <td>
                                    <div class="form-check form-check-switch form-check-switch-left">
                                        <label class="form-check-label d-flex align-items-center">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger"
                                                data-on-text="ON" data-off-text="OFF"
                                                class="form-check-input-switch">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- /content area -->

    @section('custom')
    <script>
    $('a.prod-transfer-pins').addClass('active');
    $('li.my-product-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    @endsection
    @endsection