@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Product</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px;">#</th>
                                                <th>Order Number</th>
                                                <th>Order Date</th>
                                                <th>Expiration Date</th>
                                                <th style="width:1px">UPV</th>
                                                <th style="width:1px">BPV</th>
                                                <th style="width:1px">Items</th>
                                                <th style="width:1px">Amount</th>
                                                
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i=1;
                                            @endphp
                                            @foreach($product_history as $data)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$data->order_number}}</td>
                                                <td>{{date('Y-m-d',strtotime($data->created))}}</td>
                                                <td>{{date('Y-m-d',strtotime($data->expiration))}}</td>
                                                <td>{{number_format($data->total_upv,2)}}</td>
                                                <td>{{number_format($data->total_bpv,2)}}</td>
                                                <td>{{$data->items}}</td>
                                                <td>{{$data->currency_symbol}}{{number_format($data->total,2)}}</td>
                                               
                                                <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i
                                                            class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a href="{{url('virtualoffice/code-management/my-product/purchase-product-history/view')}}/{{Crypt::encryptString($data->reference)}}"  class="dropdown-item"><i class="icon-eye"></i> View Product Pins</a>
                                                    </div>
                                                </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.prod-product-pins-history').addClass('active');
    $('li.my-product-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    @endsection
    @endsection