@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row" id="printable">
            <div class="col-12 col-md-12">
                <!-- Invoice template -->
                <div class="btn-group">
                    <button type="button" data-toggle="modal" data-target="#search_paid_codes"
                        class="btn mb-2 btn-success  avoid-this">Search</button>
                    <button type="button" class="btn print mb-2 avoid-this btn-light  ">Print</button>
                </div>
                <div style="border:none !important" class="card">
                    <div style="border:none !important" class="card-header bg-transparent header-elements-inline">
                        <h6 class="card-title"></h6>

                    </div>
                    <h5 class="text-center">
                        COMPUTOLOGY PINS REPORT
                    </h5>

                    @isset($date_start)
                    <div style="border:none !important" class="card-body">
                        <div class="row justify-content-center text-center">
                            <div class="col-md-3">
                                <span style="display:block">{{date('F j, Y')}}</span>
                                Date Printed
                            </div>
                            <div class="col-md-3">
                                <span style="display:block">{{date('F j, Y',strtotime($date_start))}} -
                                    {{date('F j, Y',strtotime($date_end))}}</span>
                                Date Range
                            </div>
                          
                        </div>
                    </div>
                    @endisset
                    <div class="mb-2"></div>


                    <div class="table-responsive">
                        <table style="width:100%" class="text-center table-striped">
                            <thead>
                                <tr>
                                    <th>Order Number</th>
                                    <th>Activation Code</th>
                                    <th>Product Title</th>
                                    <th>Product Code</th>
                                    <th>Date Created</th>
                                    <th>Date Used</th>
                                    <th>Bought By</th>
                                    <th>Used By</th>
                                    <th>Status / Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td>{{$product->order_number}}</td>
                                    <td>{{$product->activation_code}}</td>
                                    <td>{{$product->product_title}}</td>
                                    <td>{{$product->product_code}}</td>
                                    <td>{{$product->date_created}}</td>
                                    <td>{{$product->date_used}}</td>
                                    <td>{{$product->created_by}}</td>
                                    <td>-</td>
                                    <td>{{$product->status == 0 ? 'Unused' : $product->full_name}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer text-center text-uppercase">
                        <div class="row justify-content-center">
                            <div class="col-md-2">
                                <span style="display:block">{{$used}}</span>
                                Total Used Pins
                            </div>
                            <div class="col-md-2">
                                <span style="display:block">{{$unused}}</span>
                                Total Unused Pins
                            </div>
                            <div class="col-md-2">
                                <span style="display:block">{{$used + $unused}}</span>
                                Total Pins
                            </div>
                        </div>
                    </div>
                </div>


                <!-- /invoice template -->
            </div>
        </div>
    </div>


    <div class="modal" id="search_paid_codes" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">

                <div class="modal-header ">
                    <h5 class="modal-title" id="exampleModalLabel">Search Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('virtualoffice/code-management/my-product/report-pins/result')}}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Report Type</label>
                            <div class="col-md-8">
                                <select name="report_type" class="form-control">
                                    <option value="Date Generated">Date Generated</option>
                                    <option value="Date Used">Date Used</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Date Start</label>
                            <div class="col-md-8">
                                <input type="date" name="date_start" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Date End</label>
                            <div class="col-md-8">
                                <input type="date" name="date_end" class="form-control" required>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">FILTER</button>
                </div>
                </form>
            </div>
        </div>
    </div>



    @section('custom')
    <script>
    $('a.e-report-pins').addClass('active');
    $('li.e-pin-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js"></script>
    <style type="text/css" media="print">
    @page {
        size: landscape;
    }
    </style>
    <script>
    $("select").select2({
        theme: "classic"
    });

    $(function() {
        $("#printable").find('.print').on('click', function() {
            $("#printable").print({

                // Use Global styles
                globalStyles: true,

                // Add link with attrbute media=print
                mediaPrint: true,

                //Print in a hidden iframe
                iframe: false,

                // Don't print this
                noPrintSelector: ".avoid-this",

                // Manually add form values
                manuallyCopyFormValues: true,

                // resolves after print and restructure the code for better maintainability
                deferred: $.Deferred(),

                // timeout
                timeout: 1,

                noPrintSelector: ".avoid-this",

                // Custom title
                title: null,

                // Custom document type
                doctype: '<!doctype html>'

            });
        });
    });
    </script>
    @endsection
    @endsection