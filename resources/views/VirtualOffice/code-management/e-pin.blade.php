@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<div class="content">
    <div class="row">
        <div class="col-xl-12">
                <div class="row justify-content-center">
                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-credit-card icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{ number_format(0) }}</h3>
                                    <span class="text-uppercase font-size-sm">Unused Activation Code</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-cash2 icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Used Activation Code Balance</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="col-xl-12">
            <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                <li class="nav-item">
                    <a href="#statement" class="nav-link active" data-toggle="tab">Activation Code Lists</a>
                </li>
                <li class="nav-item">
                    <a href="#purchase-wallet" class="nav-link" data-toggle="tab">Activation Code Transfer History</a>
                </li>
            </ul>

            <div class="col-md-12">
                <div class="d-flex align-items-start flex-column flex-md-row">
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Statement -->
                        <div class="tab-pane  active show" id="statement">
                            <div class="row">
                                <div class="card card-body">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Description</th>
                                                        <th>Category</th>
                                                        <th>Amount</th>
                                                        <th>Transaction Date</th>
                                                        <th>Balance</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++)
                                                        <tr>
                                                            <td>{{$i}}.</td>
                                                            <td>Level Commission From <a href="">{{'12345'.date('Y')}}{{$i}}</a></td>
                                                            <td>Level Commission</td>
                                                            <td>₱{{number_format(12345,2)}}</td>
                                                            <td>{{date('F j, Y')}}</td>
                                                            <td>₱{{number_format(12345,2)}}</td>
                                                        </tr>
                                                    @endfor 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <!-- Purchase Wallet -->
                        <div class="tab-pane " id="purchase-wallet">
                            <div class="row">
                                <div class="card card-body">
                                    <div class="col-md-12">
                                        <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                    <th style="width:1px">#</th>
                                                    <th>Description</th>
                                                    <th>Category</th>
                                                    <th style="width:1px">Amount</th>
                                                    <th>Transaction Date</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for($i=1;$i<=10;$i++)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>Level Commission From <a href="">{{'12345'.date('Y')}}{{$i}}</a></td>
                                                        <td>Category</td>
                                                        <td>₱{{number_format(12345,2)}}</td>
                                                        <td>{{date('F j, Y')}}</td>
                                                        <td>₱{{number_format(1000,2)}}</td>
                                                    </tr>
                                                @endfor 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <!-- My Earnings -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="transaction_history_modal" class="modal" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Search Transaction History</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Search:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" placeholder="1234520211">
                        </div>
                    </div>
            
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">From:</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" placeholder="Eugene Kopyov">
                        </div>
                    </div>
            
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">To:</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" placeholder="Eugene Kopyov">
                        </div>
                    </div>
            </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>


@section('custom')
<script>
    $('.cm-e-pin').addClass('active');
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        });    
    });
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>

<script src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_multiselect.js') }}"></script>

<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
@endsection
@endsection