@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Pins</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="form-group">
            <div class="col-md-12">
                <div class="btn-group" style="float:right;">
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#search_paid_codes"><span>Search</span></a>
                </div>
            </div>
            </div>
            <!-- Traffic sources -->
            <div class="col-xl-12">

                <div class="card">

                    <div class="card-body ">
                        <div class="row">

                            <div class="col-md-12">

                                <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 ">
                                    <li class="nav-item"><a href="#tab1" class="nav-link active" data-toggle="tab">Paid
                                            Pins History</a></li>
                                    <li class="nav-item"><a href="#tab2" class="nav-link" data-toggle="tab">Activation
                                            Code Transaction History</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab1">
                                        <div class="table-responsive" >
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <!-- <th style="width:1px"><input type="checkbox" id="referral-count"
                                                                class="referral-count form-check-input-styled"
                                                                data-fouc></th> -->
                                                        <th>Date Created</th>
                                                        <th>OR No.</th>
                                                        <th>Name</th>
                                                        <th>Items</th>
                                                        <th>Price</th>
                                                        <th>Generated By</th>
                                                        <th>Stocklist</th>
                                                        <th>Order No.</th>
                                                        <th class="text-center">Action</th>
                                                        {{-- <th colspan=2  style="width:1px;text-align:center">Action</th> --}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <!-- <td><input type="checkbox" id="referral-count"
                                                                class="referral-count form-check-input-styled"
                                                                data-fouc></td> -->
                                                        <td>{{NOW()}}</td>
                                                        <td>M2130GL0{{$i}}</td>
                                                        <td>John Wick {{$i}}</td>
                                                        <td style="width:1px">90</td>
                                                        <td style="width:1px">₱93,000</td>
                                                        <td>computology</td>
                                                        <td style="width:1px">NA</td>
                                                        <td>123456</td>
                                                        <td style="width:1px">
                                                            <div class="btn-group">
                                                                <button class="btn btn-success btn-sm"
                                                                    data-toggle="modal" data-target="#view_history"><i
                                                                        class="icon-eye"></i></button>
                                                                <button class="btn btn-danger btn-sm"><i
                                                                        class="icon-cross2"></i></button>
                                                                <button class="btn btn-info btn-sm"><i class="icon-printer"></i></button>
                                                            </div>
                                                        </td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="table-responsive">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <!-- <th style="width:1px"><input type="checkbox" id="referral-count"
                                                                class="referral-count form-check-input-styled"
                                                                data-fouc></th> -->
                                                        <th>Account Type</th>
                                                        <th>Activation Code</th>
                                                        <th>Ordered By</th>
                                                        <th>Order ID</th>
                                                        <th>Date Created</th>
                                                        <th>Status</th>
                                                        <th>Used By</th>
                                                        <th>Encoded To</th>
                                                        <th>Date Used</th>
                                                        {{-- <th colspan=2  style="width:1px;text-align:center">Action</th> --}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <!-- <td><input type="checkbox" id="referral-count"
                                                                class="referral-count form-check-input-styled"
                                                                data-fouc></td> -->
                                                        <td>Sumpreme Pack {{$i}}</td>
                                                        <td>M2130GL0{{$i}}</td>
                                                        <td>computology</td>
                                                        <td>200PPPL8XX25</td>
                                                        <td>{{NOW()}}</td>
                                                        <td>Used</td>
                                                        <td>johnwick{{$i}}</td>
                                                        <td>Category {{$i}}</td>
                                                        <td>{{NOW()}}</td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <div class="modal fade" id="search_paid_codes" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Search Activation Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                    <div class="form-group">
                        <button type="button" class="btn btn-light btn-block daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                        </button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Filter</button>
                </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal fade" id="generate_pin_code_modal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Generate Pin Codes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <table class="table table-columned">
                                <thead>
                                    <tr>
                                        <th>Entry Package Name</th>
                                        <th style="width:1px">Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <?php $i = 1 ; ?>
                                @foreach($packages as $package => $value)
                                <tbody>
                                    <tr>
                                        <th>{{$value->entry}} {{$value->package_type}}</th>
                                        <td>₱{{number_format($value->price,2)}}</td>
                                        <td><input type="text" wire:model="quantity_{{$i}}"
                                                class="text-center form-control"></td>
                                    </tr>
                                </tbody>
                                @endforeach
                                <tfoot>
                                    <tr>
                                        <th class="text-right" colspan=2>Total Pins:</th>
                                        <td><span wire:model="pins">0</span> Pins</td>
                                    </tr>
                                    <tr>
                                        <th class="text-right" colspan=2>Total Amount:</th>
                                        <td>₱<span wire:model="amount">0.00</span></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">PROCEED</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="view_history" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="icon-list mr-2"></i>Paid Pins Purchase
                        Summary</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <table class="table table-columned">
                                <tbody>
                                    <tr>
                                        <td><b>Customer Number:</b></td>
                                        <td colspan="3">CUST-3805214</td>
                                    </tr>
                                    <tr>
                                        <td><b>User ID:</b></td>
                                        <td colspan="3">9375978</td>
                                    </tr>
                                    <tr>
                                        <td><b>Name:</b></td>
                                        <td colspan="3">Susan Vergara</td>
                                    </tr>
                                    <tr>
                                        <td><b>OR No:</b></td>
                                        <td colspan="3">00922</td>
                                    </tr>
                                    <tr>
                                        <td><b>Order No:</b></td>
                                        <td colspan="3">OR03252115346</td>
                                    </tr>
                                    <tr>
                                        <td><b>Date Process:</b></td>
                                        <td colspan="3">2021-03-25 15:20:46</td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Entry Package:</b></td>
                                        <td colspan="3">1</td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Price:</b></td>
                                        <td colspan="3">9,188.00</td>
                                    </tr>
                                    <tr>
                                        <td><b>IP Address:</b></td>
                                        <td colspan="3">202.175.233.227</td>
                                    </tr>
                                    <tr>
                                        <td><b>Representative:</b></td>
                                        <td colspan="3">crisadmin</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td><b>Entry</b></td>
                                        <td></td>
                                        <td><b>Items</b></td>
                                        <td><b>Total</b></td>
                                    </tr>
                                    <tr>
                                        <td>Gold Package A</td>
                                        <td></td>
                                        <td>1</td>
                                        <td>9,188.00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td><b>Entry</b></td>
                                        <td><b>Pin 1</b></td>
                                        <td><b></b></td>
                                        <td><b>Pin 2</b></td>
                                    </tr>
                                    <tr>
                                        <td>Gold Package A</td>
                                        <td>PHgppny5mkp2</td>
                                        <td></td>
                                        <td>u7232384gg</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
    $('a.bc-paid-codes').addClass('active');
    $('li.my-pins-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        });    
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    @endsection
    @endsection