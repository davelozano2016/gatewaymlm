@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Pins</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row justify-content-center e-wallet-top-counts">
                    <div class="col-md-3 col-12 mt-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-success icon-checkmark icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱0.00</h3>
                                    <span class=" font-size-sm">Unused Pins</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-12 mt-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-danger icon-cross2   icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱0.00</h3>
                                    <span class=" font-size-sm">Unused Pins</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-12 mt-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-info icon-blocked icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱0.00</h3>
                                    <span class=" font-size-sm">Locked Pins</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body ">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">#</th>
                                                <th>Date</th>
                                                <th>Activation Code</th>
                                                <th>Package</th>
                                                <th>Order ID</th>
                                                <th class="text-center">DR</th>
                                                <th class="text-center">BPV</th>
                                                <th style="width:1px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1?>
                                            @foreach($registration_codes as $codes)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{date('Y-m-d',strtotime($codes->created_at))}}</td>
                                                <td>{{$codes->activation_code}}</td>
                                                <td class="text-uppercase">{{$codes->entry}}</td>
                                                <td class="text-uppercase">{{$codes->customer_id}}</td>
                                                <td class="text-center">{{$codes->currency_symbol}}{{number_format($codes->direct_referal,2)}}</td>
                                                <td class="text-center">{{number_format($codes->bpv,2)}}</td>
                                                <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i
                                                            class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class=" dropdown-item"><i class="icon-forward"></i> Transfer Pin</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
<form method="post">
    <div class="modal" id="add-account" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Sub Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">System Generated User Login:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Sponsor ID:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Binary Placement ID:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Binary Placement:</label>
                        <div class="col-lg-8">
                        <select class="form-control form-control-select2" required>
                            <option>Left</option>
                            <option>Right</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Select Package:</label>
                        <div class="col-lg-8">
                        <select class="form-control form-control-select2" required>
                            <option>Starter</option>
                            <option>Business</option>
                            <option>Platinum</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Activation Code::</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Enter Account Password:</label>
                        <div class="col-lg-8">
                        <input type="password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Add Account</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</form>

<form method="post">
    <div class="modal" id="transfer" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Transfer Single Pin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Select Package:</label>
                        <div class="col-lg-8">
                        <select class="form-control form-control-select2" required>
                            <option>Starter</option>
                            <option>Business</option>
                            <option>Platinum</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Activation Code:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Transfer Pin to:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Enter Account Password:</label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control">
                        </div>
                    </div>
                       
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Transfer Code</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</form>
    @section('custom')
    <script>
    $('a.mp-paid-pins').addClass('active');
    $('li.my-pins-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable();
        });    
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    @endsection
    @endsection