@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">
                    <div class="btn-group">
                        <button data-toggle="modal" data-target="#fund_transfer_modal"
                            class="btn btn-sm btn-primary ">Top Up E-Wallet</button>

                        <button data-toggle="modal" data-target="#transfer_ewallet_modal"
                            class="btn btn-sm btn-info ">Transfer E-Wallet</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row justify-content-center e-wallet-top-counts">
                    <div class="col-md-3 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-success icon-credit-card icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{ number_format($credit, 2) }}</h3>
                                    <span class=" font-size-sm">Credit</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-danger icon-cash2 icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{ number_format($debit, 2) }}</h3>
                                    <span class=" font-size-sm">Debit</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-1 align-self-center" id="c-count-header">
                                    <i class="text-info icon-wallet icon-4x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{ number_format($total, 2) }}</h3>
                                    <span class=" font-size-sm">E-Wallet Balance</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid  mt-2">
                    <li class="nav-item">
                        <a href="#statement" class=" nav-link active" data-toggle="tab"> Statement of
                            Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="#purchase-wallet" class=" nav-link " data-toggle="tab">Purchase Wallet</a>
                    </li>

                    <li class="nav-item">
                        <a href="#income_to_ewallet" class=" nav-link nav-last" data-toggle="tab">Income to E-Wallet</a>
                    </li>
                </ul>
                <div class="col-md-12 content-tab">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="tab-content w-100 order-2 order-md-1">
                            <!-- Statement -->
                            <div class="tab-pane  active show" id="statement">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-columned datatable-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:1px">#</th>
                                                            <th>Category</th>
                                                            <th>Description</th>
                                                            <th>Transaction Date</th>
                                                            <th>Reference</th>
                                                            <th class="text-right">Value ({{$query[0]->currency_symbol}})</th>
                                                            <th class="text-right">Balance ({{$query[0]->currency_symbol}})</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i=1;?>
                                                        @foreach($wallet_logs as $wallet)
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{{$wallet->category}}</td>
                                                            <td>{!!$wallet->description!!}</td>
                                                            <td>{{$wallet->created_at}}</td>
                                                            <td>{{$wallet->reference}}</td>
                                                            <td class="text-right">
                                                                @if($wallet->status == 0)
                                                                <span
                                                                    class="text-success">+{{number_format($wallet->value,2)}}</span>
                                                                @else
                                                                <span
                                                                    class="text-danger">-{{number_format($wallet->value,2)}}</span>
                                                                @endif
                                                            </td>
                                                            <td class="text-right"><strong>{{number_format($wallet->current_balance,2)}}</strong>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Purchase Wallet -->
                            <div class="tab-pane" id="purchase-wallet">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                    <th style="width:1px">#</th>
                                                    <th>Transaction Date</th>
                                                    <th>Amount</th>
                                                    <th>Type</th>
                                                    <th>Note</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i=1;
                                                @endphp
                                                @foreach($wallets as $wallet)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$wallet->created_at}}</td>
                                                    <td>{{$query[0]->currency_symbol}}{{number_format($wallet->amount,2)}}
                                                    </td>
                                                    <td>{{$wallet->deposit_via}}</td>
                                                    <td>{{$wallet->note == null ? 'Not Available' : $wallet->note}}</td>
                                                    <td>
                                                        @if($wallet->status == 'Completed')
                                                        <label class="badge badge-success">Completed</label>
                                                        @elseif($wallet->status == 'Processing')
                                                        <label class="badge badge-warning">Processing</label>
                                                        @else
                                                        <label class="badge badge-danger">Cancelled</label>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="income_to_ewallet">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                    <th style="width:1px">#</th>
                                                    <th>Conversion Date</th>
                                                    <th class="text-right">Requested Bonus</th>
                                                    <th class="text-right">CD</th>
                                                    <th class="text-right">Tax</th>
                                                    <th class="text-right">Maintenance</th>
                                                    <th class="text-right">Processing Fee</th>
                                                    <th class="text-right">Payout</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i=1;
                                                @endphp
                                                @foreach($conversions as $conversion)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$conversion->created_at}}</td>
                                                    <td class="text-right">{{$query[0]->currency_symbol}}{{number_format($conversion->request_bonus,2)}}</td>
                                                    <td class="text-right">
                                                        @if($commission_deduction > 0)
                                                        {{$query[0]->currency_symbol}}{{number_format($conversion->request_bonus * ($commission_deduction[$query[0]->id_number] / 100),2)}}</td>
                                                        @else
                                                            0
                                                        @endif 
                                                    <td class="text-right">{{$query[0]->currency_symbol}}{{number_format($conversion->tax,2)}}</td>
                                                    <td class="text-right">{{$query[0]->currency_symbol}}{{number_format($conversion->maintenance,2)}}</td>
                                                    <td class="text-right">{{$query[0]->currency_symbol}}{{number_format($conversion->processing_fee,2)}}</td>
                                                    <td class="text-right">{{$query[0]->currency_symbol}}{{number_format($conversion->payout,2)}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="fund_transfer_modal" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Top up to E-wallet</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="{{url('virtualoffice/e-wallet/store')}}" data-parsley-validate
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_number" value="{{$query[0]->id_number}}">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Deposit Via:</label>
                            <div class="col-lg-9">
                                <select name="deposit_via" class="form-control form-control-select2" required>
                                    <option value="Bank">Bank</option>
                                    <option value="Over The Counter">Over The Counter</option>
                                    <option value="Gcash">Gcash</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Upload Receipt:</label>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" required name="upload_receipt">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Amount:</label>
                            <div class="col-lg-9">
                                <input type="number" name="amount" class="form-control"
                                    data-parsley-pattern="^\d+(\.\d{2})?$" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Notes:</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="" name="notes"
                                    style="resize:none;height:100px"></textarea>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary btn-sm">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="transfer_ewallet_modal" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Transfer E-wallet</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <form action="#">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">E-Wallet Balance:</label>
                            <div class="col-lg-9">
                                <input type="text" readonly class="form-control"
                                    value="{{$query[0]->currency_symbol}}{{number_format(0,2)}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Transfer To:</label>
                            <div class="col-lg-9">
                                <select class="form-control form-control-select2" required>
                                    <option>Affiliate 1</option>
                                    <option>Affiliate 2</option>
                                    <option>Affiliate 3</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Amount:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" value="0.00" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Notes:</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="" style="resize:none;height:100px"></textarea>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary btn-sm">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
        $('a.e-wallet').addClass('active');
        $(document).ready(function () {
            $('#example').DataTable({
                responsive: true
            });
        });

    </script>


    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    @endsection
    @endsection
