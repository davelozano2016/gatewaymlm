@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}

                </h4>


                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-blue icon-wallet icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{number_format($wallet[0]->fund,2)}}</h3>
                                    <span class="text-uppercase font-size-sm text-muted">E-wallet Balance</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-success icon-cash3 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{number_format($total_overall_income,2)}}</h3>
                                    <span class="text-uppercase font-size-sm text-muted">Overall Income</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-warning icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{number_format($payout_released,2)}}</h3>
                                    <span class="text-uppercase font-size-sm text-muted">Payout Released</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-danger icon-piggy-bank icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">
                                        {{$query[0]->currency_symbol}}{{number_format($income->total_current_income,2)}}</h3>
                                    <span class="text-uppercase font-size-sm text-muted">Current Income</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body mb-1">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 text-center">
                                <a href=""><img class="mt-2 rounded-circle" style="width:75px"
                                        src="{{asset('assets/images/mikewooting.jpg')}}" alt=""></a>
                                <p><b class="text-blue">{{$query[0]->firstname}} {{$query[0]->surname}}</b> <br> <b
                                        class="badge badge-success">{{$query[0]->id_number}}</b> <br> <b><small>AFFILIATE</b></small></p>
                            </div>
                            <div class="col-lg-9 col-sm-6">
                                <div class="row mr-2">
                                    <div class="col-6 col-md-6 text-uppercase"><small class="text-muted">Membership Package:</small>
                                        <br><b class="text-blue">Platinum</b></div>
                                        
                                    <div class="col-6 col-md-6">
                                        <b>
                                            <div class="row text-right">
                                                <div class="col-sm-6 ">
                                                    <button type="button" class="btn btn-primary  btn-sm btn-flat btn-clipboard btn-clipboard-dashboard"
                                                onclick="copyClipboard('{{$site}}')">My Booking Portal</button>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <button type="button" class="btn btn-primary btnPinCode btn-sm btn-flat btn-clipboard btn-clipboard-dashboard">My Pincode</button>
                                                </div>
                                            </div>
                                        </b>
                                    <br>
                                        
                                    </div>


                                    <div class="col-6 col-md-6 text-uppercase"><small class="text-muted">Phone number:</small>
                                    <br><b class="text-blue">+639999999999</b></div>

                                    <div class="col-6 col-md-6 text-uppercase text-right">
                                        
                                        <a href="#"><i class="icon icon-copy3 icon-1x"></i></a>  
                                        <a href="#"><i class="icon icon-facebook icon-1x"></i></a> 
                                        <a href="#"><i class="icon icon-twitter icon-1x"></i></a>  
                                        <a href="#"><i class="icon icon-linkedin icon-1x"></i></a>

                                    </div>
                                    

                                    <div class="col-6 col-md-12 "><small class="text-muted">Email Address:</small>
                                        <br><b class="text-blue">admin@gatewaybackoffice.com</b></div>

                                </div>
                             
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Joinings</h5>
                        <ul class="nav ">
                            <li class="nav-item dropdown">
                                <a href="#" class=" " data-toggle="dropdown">
                                    <div class="icon-filter3"></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item">Year</a>
                                    <a href="#" class="dropdown-item">Month</a>
                                    <a href="#" class="dropdown-item">Day</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="chart-container">
                            <div class="chart" id="google-column"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12">
                <!-- Assigned users -->
                <div class="card">
                    <div class="card-body mb-2">
                        <div class="row">
                            <div class="col-md-12 ">
                                <table class="w100 sponsor-table">
                                    <thead>
                                        <th class="text-blue">Sponsor Name</th>
                                        <th class="text-blue text-right">
                                            {{empty($sponsor_row->username) ? '--' : $sponsor_row->username.' '.$sponsor_row->id_number}}
                                        </th>

                                    </thead>
                                    <tr>
                                        <td>Personal PV</td>
                                        <td class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <td>Group PV</td>
                                        <td class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <td>Team Left</td>
                                        <td class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <td>Team Right</td>
                                        <td class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <td>Total Points Left</td>
                                        <td class="text-right">{{number_format($total_left + $total_waiting_points_left->points + $total_flush_left,2)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Points Right</td>
                                        <td class="text-right">{{number_format($total_right + $total_waiting_points_right->points + $total_flush_right,2)}} </td>
                                    </tr>
                                    <tr>
                                        <td>Total Waiting Points Left</td>
                                        <td class="text-right">{{number_format($total_waiting_points_left->points,2)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total Waiting Points Right</td>
                                        <td class="text-right">{{number_format($total_waiting_points_right->points,2)}}
                                        </td>
                                    </tr>

                                </table>

                            </div>
                            <!-- <div class="col-md-12 text-capitalize ">
                                <p>Personal PV <label class="float-right">2000</label> </p>
                                <p>Group PV <label class="float-right">10200</label> </p>
                                <p>Team Left <label class="float-right">78</label> </p>
                                <p>Team Right <label class="float-right">20</label> </p>
                                <p>Total Points Left <label class="float-right">6200</label> </p>
                                <p>Total Points Right <label class="float-right">4000</label> </p>
                                <p>Total Waiting Points Left <label class="float-right">750</label> </p>
                                <p>Total Waiting Points Right <label class="float-right">0</label> </p>

                            </div> -->
                        </div>
                        <!-- <div class="row text-capitalize ">
                            <div class="col-md-6 col-6">
                                <p>Current Rank: <strong>Silver</strong></p>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%">
                                        <span>100%</span>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn bg-danger btn-block btn-sm" id="c-button-ar">4
                                                Required</button></div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn btn-success btn-block btn-sm" id="c-button-ar">5
                                                Achieved</button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Next Rank: <strong>Gold</strong></p>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 83.3%">
                                        <span> 83.3%</span>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn bg-danger btn-block btn-sm" id="c-button-ar">6
                                                Required</button></div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn btn-success btn-block btn-sm" id="c-button-ar">5
                                                Achieved</button></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">

                <!-- Sales stats -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Sales statistics</h6>
                        <div class="header-elements">
                            <select class="form-control custom-select" id="select_date">
                                <option value="monthly">Monthly</option>
                                <option value="weekly">Weekly</option>
                                <option value="daily" selected>Daily</option>

                            </select>
                        </div>
                    </div>

                    <!-- <div class="card-body py-0">
                                <div class="row text-center">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <h5 class="font-weight-semibold mb-0">5,689</h5>
                                            <span class="text-muted font-size-sm">new orders</span>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="mb-3">
                                            <h5 class="font-weight-semibold mb-0">32,568</h5>
                                            <span class="text-muted font-size-sm">this month</span>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="mb-3">
                                            <h5 class="font-weight-semibold mb-0">$23,464</h5>
                                            <span class="text-muted font-size-sm">expected profit</span>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                    <div class="chart mb-2" id="app_sales"></div>
                    <div class="chart" id="monthly-sales-stats"></div>
                </div>
                <!-- /sales stats -->
            </div>

            <!--
            <div class="col-lg-6 col-md-12 col-sm-12">
                
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">My Account</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">
                            <div class="table-responsive">
                                <table class="table-dash-account text-uppercase table-columned" width="100%" cellpadding="2" cellspacing="2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="width:40px"></th>
                                            <th>ID Number</th>
                                            <th>Username</th>
                                            <th>Wallet</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1?>
                                        @foreach($sub_accounts as $account) 
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td class="text-center">
                                                    @if($account->id_number == $query[0]->id_number)
                                                    <button type="button"
                                                        class="disabled btn btn-link btn-flat btn-sm">Currently Login</button>
                                                    @else
                                                    <a href="{{ url('virtualoffice/my-account/sub-account/secure/'.Crypt::encryptString($account->id_number))}}" class="btn btn-primary btn-flat btn-sm">Login</a>
                                                    @endif
                                                </td>
                                                <td>{{$account->id_number}}</td>
                                                <td>{{$account->username}}</td>
                                                <td>{{$account->currency_symbol}}{{number_format($account->fund,2)}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{-- <ul class="media-list" style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">

                        @for($i=1;$i<=10;$i++)
                        
                        @endfor
                    </ul> --}}
                    </div>
                </div>
            </div>
    -->
            <div class="col-lg-6 col-md-12 col-sm-12">
                <!-- Assigned users -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">New Members</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="media-list" style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">
                            @for($i=1;$i<=10;$i++) <li class="media">
                                <a href="#" class="mr-3">
                                    <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}" width="36"
                                        height="36" class="rounded-circle" alt="">
                                </a>
                                <div class="media-body">
                                    <a href="#" class="media-title font-weight-semibold">James Alexander {{$i}}</a>
                                    <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                </div>
                                <div class="align-self-center mr-4">
                                    <a href="#" class="media-title font-weight-semibold">{{$query[0]->currency_symbol}}123,456.00</a>
                                    <div class="font-size-sm text-muted ">{{date('M j, Y',strtotime(date('Y-m-d')))}}
                                    </div>
                                </div>
                                </li>
                                @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Team Performance</h6>
                    </div>

                    <div class="card-body team-performance">
                        <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                            <li class="nav-item"><a href="#top-earners" class="nav-link active" data-toggle="tab">Top
                                    Earners</a></li>
                            <li class="nav-item"><a href="#top-recruiters" class="nav-link" data-toggle="tab">Top
                                    Recruiters</a></li>
                            <li class="nav-item"><a href="#package-overview" class="nav-link" data-toggle="tab">Package
                                    Overview</a></li>
                            <!-- <li class="nav-item"><a href="#rank-overview" class="nav-link" data-toggle="tab">Rank
                                Overview</a></li> -->
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="top-earners">
                                <ul class="media-list" style=" overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <a href="#" class="mr-3">
                                            <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}"
                                                width="36" height="36" class="rounded-circle" alt="">
                                        </a>
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">James Alexander
                                                {{$i}}</a>
                                            <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                        </div>

                                        <div class="align-self-center mr-4">
                                            <a href="#" class="media-title font-weight-semibold">{{$query[0]->currency_symbol}}123,456.00</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="top-recruiters">
                                <ul class="media-list" style=" overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <a href="#" class="mr-3">
                                            <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}"
                                                width="36" height="36" class="rounded-circle" alt="">
                                        </a>
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">James Alexander
                                                {{$i}}</a>
                                            <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="package-overview">
                                <ul class="media-list" style=" overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">Membership {{$i}}</a>
                                            <div class="font-size-sm text-muted">You have {{$i}} Membership package
                                                purchases in your team </div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="rank-overview">
                                <ul class="media-list" style=" overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">Bronze</a>
                                            <div class="font-size-sm text-muted">You have 15 Bronze rank in your team
                                            </div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">15</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Income & Comission</h6>
                    </div>

                    <div class="card-body ">
                        <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                            <li class="nav-item"><a href="#earnings" class="nav-link active"
                                    data-toggle="tab">Commissions</a></li>
                            <li class="nav-item"><a href="#expenses" class="nav-link" data-toggle="tab">Expenses</a>
                            </li>
                            <li class="nav-item"><a href="#payout" class="nav-link" data-toggle="tab">Payout Status</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="earnings" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                <div class="table-responsive">
                                    <table class="table table-columned">
                                        <tr>
                                            <th>Binary Commission</th>
                                            <td>{{$query[0]->currency_symbol}}{{number_format(0,2)}}</td>
                                            <td><span class="badge bg-blue-400">FC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Royalty Commission</th>
                                            <td>{{$query[0]->currency_symbol}}{{number_format(0,2)}}</td>
                                            <td><span class="badge bg-blue-400">RC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Referral Commission</th>
                                            <td>{{$query[0]->currency_symbol}}{{number_format(0,2)}}</td>
                                            <td><span class="badge bg-blue-400">RC</span></td>
                                        </tr>

                                        {{-- <tr>
                                            <th>Unilevel Commission</th>
                                            <td>{{$query[0]->currency_symbol}}0</td>
                                            <td><span class="badge bg-blue-400">HF</span></td>
                                        </tr> --}}
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="expenses" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                <div class="table-responsive table-columned">
                                    <table class="table">
                                        {{-- <tr>
                                            <th>Product Purchased</th>
                                            <td>{{$query[0]->currency_symbol}}123,456.00</td>
                                            <td><span class="badge bg-blue-400">PRM</span></td>
                                        </tr> --}}

                                        <tr>
                                            <th>E-Wallet Topup</th>
                                            <td>{{$query[0]->currency_symbol}}{{number_format(0,2)}}</td>
                                            <td><span class="badge bg-blue-400">PR</span></td>
                                        </tr>

                                        <tr>
                                            <th>Package Purchased</th>
                                            <td>{{$query[0]->currency_symbol}}{{number_format(0,2)}}</td>
                                            <td><span class="badge bg-blue-400">PP</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="payout" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                                <div class="table-responsive table-columned">
                                    <table class="table">
                                        <tr>
                                            <th>Requested</th>
                                            <td><span class="text-blue">{{$query[0]->currency_symbol}}{{number_format(0,2)}}</span></td>
                                        </tr>

                                        <tr>
                                            <th>Approved</th>
                                            <td><span class="text-blue">{{$query[0]->currency_symbol}}{{number_format(0,2)}}</span></td>
                                        </tr>

                                        <tr>
                                            <th>Paid</th>
                                            
                                            <td><span class="text-green">{{$query[0]->currency_symbol}}{{number_format('0',2)}}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <form action="{{url('virtualoffice/my-account/update-pincode')}}" data-parsley-validate method="POST">
        @csrf
    <div class="modal" id="modal_security_code" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Pin Code Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Pin Code {{$query[0]->security_code}}</label>
                    <input type="text" readonly class="form-control" value="{{substr_replace($query[0]->security_code, str_repeat('*', strlen($query[0]->security_code)-2), 0, -2)
                }}">
                </div>

                <div class="form-group">
                    <label for="">Old Pin Code</label>
                    <input type="text" class="form-control" minlength=4 maxlength="4" name="old_pincode" required>
                </div>

                <div class="form-group">
                    <label for="">New Pin Code</label>
                    <input type="text" class="form-control" minlength=4 maxlength="4" name="new_pincode" required>
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary btn-sm">Save Changes</button>
            </div>
          </div>
        </div>
      </div>
    </form>
      

    @section('custom')
    <script>
        $('a.dashboard').addClass('active');
        $('.btnPinCode').click(e => {
            $('#modal_security_code').modal()
        })
        function copyClipboard(text) {
            const elem = document.createElement('textarea');
            elem.value = text;
            document.body.appendChild(elem);
            elem.select();
            document.execCommand('copy');
            document.body.removeChild(elem);
            alert("Copied Link: " + text);
        }

    </script>

    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('assets/js/demo_charts/google/light/lines/lines.js')}}"></script>
    <script src="{{asset('assets/js/demo_charts/google/light/pies/donut.js')}}"></script>
    <script src="{{asset('assets/js/demo_charts/google/light/bars/column.js')}}"></script>

    @endsection
    @endsection
