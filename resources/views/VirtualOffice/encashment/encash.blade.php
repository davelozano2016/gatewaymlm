@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">Encashment</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid mt-2">
                    <li class="nav-item">
                        <a href="#individual" class=" nav-link active" data-toggle="tab"><i class="mi-account-circle mr-3 mi-2x"></i> INDIVIDUAL ENCASHMENT</a>
                    </li>
                </ul>
                <div class="col-md-12 content-tab content-tab-encash">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="tab-content w-100 order-2 order-md-1">
                            <div class="tab-pane  active show " id="individual">
                                <div class="row  mb-0">
                                    <div class="col-lg-12 col-sm-12 ">
                                        <form action="{{url('virtualoffice/encashment/encash-bonus')}}" method="POST" data-parsley-validate>
                                            @csrf
                                            <input type="hidden" name="country_code" value="{{$query[0]->code}}">
                                            <input type="hidden" name="id_number" value="{{$query[0]->code}}">
                                            <div class="col-md-12" style="margin-top:-30px">
                                              
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label"></label>
                                                    <div class="col-lg-10">
                                                        <div class="row">
                                                            <div class="col-12 col-md-6 text-center" style="margin-top:-30px">
                                                                <div class=" card-body">
                                                                    <div class="media">
                                                                        <div class="media-body ">
                                                                            <h3 class="font-weight-semibold mb-0">
                                                                                {{$query[0]->currency_symbol}}{{number_format($query[0]->fund,2)}}
                                                                            </h3>
                                                                            <span
                                                                                class="text-uppercase font-size-sm">Current E-Wallet</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="col-12 col-md-6 text-center" style="margin-top:-30px">
                                                                <div class="card-body">
                                                                    <div class="media">
                                                                        <div class="media-body ">
                                                                            <h3 class="font-weight-semibold mb-0">₱2,500.00
                                                                            </h3>
                                                                            <span
                                                                                class="text-uppercase font-size-sm">Minimum Request</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
    
    
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Request Type:</label>
                                                    <div class="col-lg-10">
                                                        <select name="request_type"
                                                            class="form-control select form-control-select2">
                                                            <option value="Cash">Cash</option>
                                                            <option value="Cheque">Cheque</option>
                                                            {{-- <option value="Bitcoin">Bitcoin</option> --}}
                                                            <option value="Bank Transfer">Bank Transfer</option>
                                                            {{-- <option value="Remittance">Remittance</option> --}}
                                                        </select>
                                                    </div>
                                                </div>
    
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Request Bonus:</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" data-parsley-pattern="^[0-9]{1,}\.?[0-9]{0,2}$" name="request_bonus" required>
                                                    </div>
                                                </div>
    
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Password:</label>
                                                    <div class="col-lg-10">
                                                        <input type="password" class="form-control" name="password" required>
                                                    </div>
                                                </div>
    
                                                <div class="float-right mb-0 row">
                                                    <button class="btn btn-info btn-sm">Encash Bonus</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /content area -->
    @section('custom')
    <script>
        $('a.encash').addClass('active');
        $('li.encashment-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    @endsection
    @endsection
