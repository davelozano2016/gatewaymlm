@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">Encashment</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px;">#</th>
                                                <th>Reference No.</th>
                                                <th>Type</th>
                                                <th>Amount</th>
                                                <th>Country</th>
                                                <th>IP</th>
                                                <th>Payout</th>
                                                <th>Status</th>
                                                <th>Date Requested</th>
                                                <th>Date Processed</th>
                                                {{-- <th></th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php 
                                            $i=1;
                                            @endphp
                                            @foreach($histories as $history)
                                            <tr>
                                                <td style="width:1px">{{$i}}.</td>
                                                <td>{{empty($history->reference_code) ? $history->reference : $history->reference_code}}</td>
                                                <td>{{$history->request_type}}</td>
                                                <td>{{$query[0]->currency_symbol}}{{number_format($history->request_bonus,2)}}</td>
                                                <td>{{$history->country_code}}</td>
                                                <td>{{$history->ip_address}}</td>
                                                <td>{{$query[0]->currency_symbol}}{{number_format($history->payout,2)}}</td>
                                                <td>
                                                    @if($history->status == 0)
                                                        <label class="badge badge-warning">Pending</label>
                                                    @elseif($history->status ==1)
                                                        <label class="badge badge-success">Completed</label>
                                                    @elseif($history->status ==3)
                                                    <label class="badge badge-danger">Cancelled</label>
                                                    @endif
                                                </td>
                                                <td>{{$history->date_requested}}</td>
                                                <td>{{empty($history->date_processed) ? '00:00:00 00:00:00' : $history->date_processed}}</td>
                                                {{-- <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i
                                                            class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a href="javascript:(void)" class="dropdown-item"><i class="icon-eye"></i>Details</a>

                                                        <a href="javascript:(void)" class="dropdown-item"><i class="icon-file-text2"></i>Voucher</a>

                                                        <a href="javascript:(void)" class="dropdown-item"><i class="icon-printer"></i> Print Encashment</a>
                                                    </div>
                                                </td> --}}

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.encashment-history').addClass('active');
    $('li.encashment-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection