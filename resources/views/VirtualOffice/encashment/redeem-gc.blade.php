@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">Encashment</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row justify-content-center  product-list-counts">
            <div class="col-md-3 col-sm-12">
                <div class="card card-body redeem-top-counts">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-success icon-cash4 icon-4x"></i>
                        </div>

                        <div class="media-body text-right ">
                            <h3 class="font-weight-semibold mb-0">0 GC</h3>
                            <span class="text-uppercase font-size-sm">Current GC</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <div class="card card-body redeem-top-counts">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-success icon-cash4 icon-4x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">0 GC</h3>
                            <span class="text-uppercase font-size-sm">Total GC</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-12">
                <div class="card card-body redeem-top-counts">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-warning icon-cart-add2  icon-4x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">0</h3>
                            <span class="text-uppercase font-size-sm">Redeem Request</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-12">
                <div class="card card-body redeem-top-counts">
                    <div class="media">
                        <div class="mr-1 align-self-center" id="c-count-header">
                            <i class="text-warning icon-cart-add2  icon-4x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">0</h3>
                            <span class="text-uppercase font-size-sm">Total Redeemed Rewards</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-uppercase product-list">
            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/autoorder.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="mb-2">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Auto Order</a>
                            </h6>
                            <a href="#" class="text-muted">Unilevel Maintenance</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">8 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/EmpressWhite.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Empress White</a>
                            </h6>
                            <a href="#" class="text-muted">2 Pcs. Empress White</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">8 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/BagLV.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Louis Vuitton Bag</a>
                            </h6>
                            <a href="#" class="text-muted">Sling Bag</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">15 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/Acer.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Acer Laptop</a>
                            </h6>
                            <a href="#" class="text-muted">acer i5 2021 model</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">25 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/Boracay.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Boracay Staycation</a>
                            </h6>
                            <a href="#" class="text-muted">Boracay for 2 (3 Days & 2 Nights)</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">150 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/maldives.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Maldives Staycation</a>
                            </h6>
                            <a href="#" class="text-muted">Maldives for 2 (3 Days & 2 Nights)</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">160 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/Vios.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Toyota Car Vios 2021</a>
                            </h6>
                            <a href="#" class="text-muted">Downpayment</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">150 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-img-actions">
                            <a class="row justify-content-center">
                                <img src="{{asset('assets/img/gc/Fortuner.png')}}" class="card-img" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="card-body bg-light text-center">
                        <div class="">
                            <h6 class="font-weight-semibold mb-0">
                                <a href="#" class="text-default">Toyota Fortuner 2021</a>
                            </h6>
                            <a href="#" class="text-muted">Downpayment</a>
                        </div>

                        <h3 class="mb-0 font-weight-semibold">210 GC REQUIRED</h3>


                        <div class="text-muted mb-2">8 GC Remaining</div>

                        <button type="button" class="btn btn-primary btn-sm">Redeem Now</button>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.gc-request').addClass('active');
    $('li.encashment-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    @endsection
    @endsection