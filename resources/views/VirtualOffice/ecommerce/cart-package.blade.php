@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="input-group">
                <input type="text" placeholder="Search Product" class="form-control border-right-0">
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">E-commerce Store</span>
                <span class="breadcrumb-item">Package</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>
<!-- /page header -->

<!-- Inner container -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Orders history (static table) -->
            <form method="POST" action="{{url('virtualoffice/store/cart/package/update')}}">
            @csrf
        
				<div class="card card-body">
					<div class="table-responsive">
						<table class="table text-nowrap">
							<thead>
								<tr>
									<th style="width:1px;">#</th>
									<th>Package</th>
									<th>DR</th>
									<th>BPV</th>
                                    <th class="text-center">Price</th>
									<th class="text-center">Quantity</th>
									<th class="text-center">Sub Total</th>
								</tr>
							</thead>
							<tbody>
                            <?php 
                                $i                  = 1;
                                $direct_referal     = 0;
                                $bpv                = 0;
                                $distributor_price  = 0;
                                $total              = 0;
                            ?>
                               @foreach($_SESSION['package_cart'] as $key => $value)
                               <?php $direct_referal += $value['direct_referal'] * $value['quantity']; ?>
                               <?php $bpv += $value['binary_point_value'] * $value['quantity']; ?>
                               <?php $distributor_price = $value['price'] * $value['quantity']; ?>
                               <?php $total += $distributor_price; ?>
                                <input type="hidden" name="package_id[]" value="{{$value['package_id']}}">
								<tr>
									<td>{{$i++}}.</td>
									<td>
										<a href="javascript:void(0)" class="font-weight-semibold">{{$value['package_name']}}</a>
										<div class="text-muted font-size-sm">
											<span class="badge badge-mark bg-grey border-grey mr-1"></span> Processing
										</div>
									</td>
									<td>{{$value['currency_symbol']}}{{$value['direct_referal']}}</td>
                                    <td>{{$value['binary_point_value']}}</td>
                                    <td class="text-center">{{$value['currency_symbol']}}{{number_format($value['price'],2)}}</td>
									<td class="pr-0 text-center" style="width:100px;"><input type="text" onkeypress="return isNumberKey(event)"  value="{{$value['quantity']}}" id="validation_{{$value['package_id']}}" x="validate('{{$value['package_id']}}')" name="quantity[]"  class="text-center form-control"></td>
									<td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($distributor_price, 2) }}</h6></td>
								</tr>
                               @endforeach
                               <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Price:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($total, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Direct Referral:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($direct_referal, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Binary Point Value:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{ number_format($bpv, 2) }}</h6></td>
                                </tr>
							</tbody>
						</table>
					</div>
                    <hr>
                    <div class="row float-right">
                        <div style="text-align:right" class="col-md-12">
                            <a href="{{url('virtualoffice/e-commerce-store')}}" class="btn btn-warning ">Continue Shopping</a>
                            <button type="submit" class="btn btn-primary">Update Cart</button>
                            <a href="{{url('virtualoffice/store/package/checkout')}}" class="btn btn-info ">Check Out</a>
                        </div>
                    </div>
				</div>
				<!-- /orders history (static table) -->
            </form>
        </div>
    </div>
    <!-- /main charts -->
</div>
<!-- /inner container -->

@section('custom')
<script>
    $('a.e-commerce-store').addClass('active');
   
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function validate(id) {
        var validation = $('#validation_'+id).val();
        if(validation == 0) {
            $('#validation_'+id).val(1)
        }

    }
</script>
<!-- /theme JS files -->

@endsection
@endsection