<?php session_start() ?>
@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="input-group">
                    <input type="text" placeholder="Search Product" class="form-control border-right-0">
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">E-commerce Store</span>
                    <span class="breadcrumb-item">Product</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->

    <!-- Inner container -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-9 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th style="width:1px;">#</th>
                                        <th colspan="2">Product Name</th>
                                        <th>Product Code</th>
                                        <th>UPV</th>
                                        <th>BPV</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                $i                  = 1;
                                $upv                = 0;
                                $bpv                = 0;
                                $distributor_price  = 0;
                                $total              = 0;
                            ?>
                                    @foreach($_SESSION['cart'] as $key => $value)
                                    <?php $upv += $value['unilevel_points'] * $value['quantity']; ?>
                                    <?php $bpv += $value['binary_points'] * $value['quantity']; ?>
                                    <?php $distributor_price = $value['distributor_price'] * $value['quantity']; ?>
                                    <?php $total += $distributor_price; ?>
                                    <?php
                                    if($value['product_image'] == 'default.png') {
                                            $product_image = 'default.png';
                                    } else {
                                        $product_image = $value['product_image'];
                                    }
                                    ?>
                                    <tr>
                                        <td>{{$i++}}.</td>
                                        <td class="pr-0" style="width: 45px;">
                                            <a href="#"><img src="{{url('assets/products/'.$product_image)}}" height="60" alt=""></a>
                                        </td>
                                        <td>
                                            <a href="#" class="font-weight-semibold">{{$value['product_title']}}</a>
                                            <div class="text-muted font-size-sm">
                                                <span class="badge badge-mark bg-grey border-grey mr-1"></span>
                                                Processing
                                            </div>
                                        </td>
                                        <td>{{$value['product_code']}}</td>
                                        <td>{{$value['unilevel_points']}}</td>
                                        <td>{{$value['binary_points']}}</td>
                                        <td class="text-center">
                                            {{$value['currency_symbol']}}{{number_format($value['distributor_price'],2)}}
                                        </td>
                                        <td class="text-center" style="width:100px;">{{$value['quantity']}}</td>
                                        <td class="text-center">
                                            <h6 class="mb-0 font-weight-semibold">
                                                {{$value['currency_symbol']}}{{ number_format($distributor_price, 2) }}
                                            </h6>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="8" class="text-right">
                                            <h6 class="mb-0 font-weight-semibold">Total Price:</h6>
                                        </td>
                                        <td class="text-center">
                                            <h6 class="mb-0 font-weight-semibold">
                                                {{$value['currency_symbol']}}{{ number_format($total, 2) }}</h6>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="8" class="text-right">
                                            <h6 class="mb-0 font-weight-semibold">Total Unilevel Point Value:</h6>
                                        </td>
                                        <td class="text-center">
                                            <h6 class="mb-0 font-weight-semibold">{{ number_format($upv, 2) }}</h6>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="8" class="text-right">
                                            <h6 class="mb-0 font-weight-semibold">Total Binary Point Value:</h6>
                                        </td>
                                        <td class="text-center">
                                            <h6 class="mb-0 font-weight-semibold">{{ number_format($bpv, 2) }}</h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-12">
                <form action="{{url('virtualoffice/store/processing')}}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-uppercase" for="">Local Pickup</label>
                                        <select name="local_pickup" class="form-control" id="">
                                            <option value="UAE Office">UAE Office</option>
                                            <option value="KEI Factory Bulacan">KEI Factory Bulacan</option>
                                            <option value="Davao Center">Davao Center</option>
                                        </select>
                                    </div>
                                </div> --}}

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-uppercase" for="">Method of Payment</label>
                                        <select name="method_of_payment" class="form-control" id="">
                                            <option value="E-Wallet">E-Wallet (Balance: {{$query[0]->currency_symbol}}{{number_format($wallet[0]->fund,2)}} PHP)</option>
                                            <option value="Credit Card">Credit Card</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div style="" class="col-md-12">
                                    <a href="{{url('virtualoffice/store/cart')}}"
                                        class="btn btn-warning btn-sm btn-block ">Go Back To
                                        Cart</a>
                                    <button type="submit"
                                        class="btn btn-info btn-sm btn-block ">Place Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /inner container -->

    @section('custom')
    <script>
    $('a.e-commerce-store').addClass('active');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
    <!-- /theme JS files -->
    
    @endsection
    @endsection