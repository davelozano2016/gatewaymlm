<?php session_start() ?>
@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="input-group">
                    <input type="text" placeholder="Search Product" class="form-control border-right-0">
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">E-commerce Store</span>
                    <span class="breadcrumb-item">Package</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->

    <!-- Inner container -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-9 col-12">
                <div class="card">
                    <div class="card-body">
                    <table class="table text-nowrap">
							<thead>
								<tr>
									<th style="width:1px;">#</th>
									<th>Package</th>
									<th>DR</th>
									<th>BPV</th>
                                    <th class="text-center">Price</th>
									<th class="text-center">Quantity</th>
									<th class="text-center">Sub Total</th>
								</tr>
							</thead>
							<tbody>
                            <?php 
                                $i                  = 1;
                                $direct_referal     = 0;
                                $bpv                = 0;
                                $distributor_price  = 0;
                                $total              = 0;
                            ?>
                               @foreach($_SESSION['package_cart'] as $key => $value)
                               <?php $direct_referal += $value['direct_referal'] * $value['quantity']; ?>
                               <?php $bpv += $value['binary_point_value'] * $value['quantity']; ?>
                               <?php $distributor_price = $value['price'] * $value['quantity']; ?>
                               <?php $total += $distributor_price; ?>
								<tr>
									<td>{{$i++}}.</td>
									<td>
										<a href="javascript:void(0)" class="font-weight-semibold">{{$value['package_name']}}</a>
										<div class="text-muted font-size-sm">
											<span class="badge badge-mark bg-grey border-grey mr-1"></span> Processing
										</div>
									</td>
									<td>{{$value['currency_symbol']}}{{$value['direct_referal']}}</td>
                                    <td>{{$value['binary_point_value']}}</td>
                                    <td class="text-center">{{$value['currency_symbol']}}{{number_format($value['price'],2)}}</td>
                                    <td class="text-center">{{$value['quantity']}}</td>
									<td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($distributor_price, 2) }}</h6></td>
								</tr>
                               @endforeach
                               <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Price:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($total, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Direct Referral:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($direct_referal, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Binary Point Value:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{ number_format($bpv, 2) }}</h6></td>
                                </tr>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-12">
                <form action="{{url('virtualoffice/store/package/processing')}}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-uppercase" for="">Local Pickup</label>
                                        <select name="local_pickup" class="form-control" id="">
                                            <option value="UAE Office">UAE Office</option>
                                            <option value="KEI Factory Bulacan">KEI Factory Bulacan</option>
                                            <option value="Davao Center">Davao Center</option>
                                        </select>
                                    </div>
                                </div> --}}

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-uppercase" for="">Method of Payment</label>
                                        <select name="method_of_payment" class="form-control" id="">
                                            <option value="E-Wallet">E-Wallet (Balance: {{$query[0]->currency_symbol}}{{number_format($wallet[0]->fund,2)}} PHP)</option>
                                            <option value="Credit Card">Credit Card</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div style="" class="col-md-12">
                                    <a href="{{url('virtualoffice/e-commerce-store/package/cart')}}"
                                        class="btn btn-warning btn-sm btn-block ">Go Back To
                                        Cart</a>
                                    <button type="submit"
                                        class="btn btn-info btn-sm btn-block ">Place Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /inner container -->

    @section('custom')
    <script>
    $('a.e-commerce-store').addClass('active');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
    <!-- /theme JS files -->
    
    @endsection
    @endsection