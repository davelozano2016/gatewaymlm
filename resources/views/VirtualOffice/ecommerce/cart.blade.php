@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="input-group">
                <input type="text" placeholder="Search Product" class="form-control border-right-0">
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">E-commerce Store</span>
                <span class="breadcrumb-item">Product</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>
</div>
<!-- /page header -->

<!-- Inner container -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Orders history (static table) -->
            <form method="POST" action="{{url('virtualoffice/store/cart/update')}}">
            @csrf
        
				<div class="card card-body">
					<div class="table-responsive">
						<table class="table text-nowrap">
							<thead>
								<tr>
									<th style="width:1px;">#</th>
									<th colspan="2">Product Name</th>
									<th>Product Code</th>
									<th>UPV</th>
									<th>BPV</th>
                                    <th class="text-center">Price</th>
									<th class="text-center">Quantity</th>
									<th class="text-center">Sub Total</th>
								</tr>
							</thead>
							<tbody>
                            <?php 
                                $i                  = 1;
                                $upv                = 0;
                                $bpv                = 0;
                                $distributor_price  = 0;
                                $total              = 0;
                            ?>
                               @foreach($_SESSION['cart'] as $key => $value)
                               <?php $upv += $value['unilevel_points'] * $value['quantity']; ?>
                               <?php $bpv += $value['binary_points'] * $value['quantity']; ?>
                               <?php $distributor_price = $value['distributor_price'] * $value['quantity']; ?>
                               <?php $total += $distributor_price; ?>
                               <?php
                                if($value['product_image'] == 'default.png') {
                                        $product_image = 'default.png';
                                } else {
                                    $product_image = $value['product_image'];
                                }
                                ?>
                                <input type="hidden" name="product_description_id[]" value="{{$value['product_description_id']}}">
								<tr>
									<td>{{$i++}}.</td>
									<td class="pr-0" style="width: 45px;">
										<a href="#"><img src="{{url('assets/products/'.$product_image)}}" height="60" alt=""></a>
									</td>
									<td>
										<a href="#" class="font-weight-semibold">{{$value['product_title']}}</a>
										<div class="text-muted font-size-sm">
											<span class="badge badge-mark bg-grey border-grey mr-1"></span> Processing
										</div>
									</td>
									<td>{{$value['product_code']}}</td>
									<td>{{$value['unilevel_points']}}</td>
                                    <td>{{$value['binary_points']}}</td>
                                    <td class="text-center">{{$value['currency_symbol']}}{{number_format($value['distributor_price'],2)}}</td>
									<td class="pr-0 text-center" style="width:100px;"><input type="text" onkeypress="return isNumberKey(event)"  value="{{$value['quantity']}}" id="validation_{{$value['product_description_id']}}" x="validate('{{$value['product_description_id']}}')" name="quantity[]"  class="text-center form-control"></td>
									<td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($distributor_price, 2) }}</h6></td>
								</tr>
                               @endforeach
                               <tr>
                                    <td colspan="8" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Price:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{$value['currency_symbol']}}{{ number_format($total, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="8" class="text-right"><h6 class="mb-0 font-weight-semibold">Total Unilevel Point Value:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{ number_format($upv, 2) }}</h6></td>
                                </tr>

                                <tr>
                                    <td colspan="8" class="text-right"><h6 class="mb-0 font-weight-semibold">Binary Point Value:</h6></td>
                                    <td class="text-center"><h6 class="mb-0 font-weight-semibold">{{ number_format($bpv, 2) }}</h6></td>
                                </tr>
							</tbody>
							<!-- <tfooter>
                                <tr>
                                    <td colspan="8"><b>Select Payment Method:</b></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><input type="radio" class="form-input-styled" name="payment_method" id="payment1" data-fouc="" ></td>
                                    <td colspan="7"><label for="payment1"><img src="{{asset('assets/img/payments/gcash-logo.png')}}" class="im-float img-responsive img-payments"></label></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><input type="radio" class="form-input-styled" name="payment_method" id="payment2" data-fouc="" ></td>
                                    <td colspan="7"><label for="payment2"><img src="{{asset('assets/img/payments/paypal-logo.png')}}" class="im-float img-responsive img-payments"></label></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><input type="radio" class="form-input-styled" name="payment_method" id="payment3" data-fouc="" ></td>
                                    <td colspan="7"><label for="payment3"><img src="{{asset('assets/img/payments/ecpay-logo.png')}}" class="im-float img-responsive img-payments"></label></td>
                                </tr>
							</tfooter> -->
						</table>
					</div>
                    <hr>
                    <div class="row float-right">
                        <div style="text-align:right" class="col-md-12">
                            <a href="{{url('virtualoffice/e-commerce-store')}}" class="btn btn-warning ">Continue Shopping</a>
                            <button type="submit" class="btn btn-primary">Update Cart</button>
                            <a href="{{url('virtualoffice/store/checkout')}}" class="btn btn-info ">Check Out</a>
                        </div>
                    </div>
				</div>
				<!-- /orders history (static table) -->
            </form>
        </div>
    </div>
    <!-- /main charts -->
</div>
<!-- /inner container -->

@section('custom')
<script>
    $('a.e-commerce-store').addClass('active');
   
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function validate(id) {
        var validation = $('#validation_'+id).val();
        if(validation == 0) {
            $('#validation_'+id).val(1)
        }

    }
</script>
<!-- /theme JS files -->

@endsection
@endsection