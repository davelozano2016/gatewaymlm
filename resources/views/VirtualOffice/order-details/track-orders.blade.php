@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Order Details</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body ">
                        <div class="card-title"><h5 class="">Tracking Number {{$reference}}</h5></div>
                        <div class="row">
                            <div class="col-md-12">
                                @if($timelines[0]->pickup != null)
                                <h6 class="">Pickup Location: {{$timelines[0]->pickup}}</h6>
                                @endif
                                <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                    @foreach($timelines as $timeline)
                                    <div class="vertical-timeline-item vertical-timeline-element">
                                        <div> <span class="vertical-timeline-element-icon bounce-in"> <i
                                                    class="badge badge-dot badge-dot-xl badge-success"></i> </span>
                                            <div class="vertical-timeline-element-content bounce-in">
                                                <h4 class="timeline-title">
                                                    @if($timeline->method == 'Cash On Delivery')
                                                    {{$timeline->timeline_order_status}} ({{$timeline->method}} - {{date('F j Y',strtotime($timeline->created_at))}})
                                                    @elseif($timeline->method == 'Bank Deposit')
                                                    {{$timeline->status}} - {{$timeline->timeline_order_status}} ({{$timeline->method}} - {{date('F j Y',strtotime($timeline->created_at))}})
                                                    @elseif($timeline->method == 'GCash')
                                                    {{$timeline->status}} - {{$timeline->timeline_order_status}} ({{$timeline->method}} - {{date('F j Y',strtotime($timeline->created_at))}})
    
                                                    @elseif($timeline->method == 'E-Wallet')
                                                    {{$timeline->status}} - {{$timeline->timeline_order_status}} ({{$timeline->method}} - {{date('F j Y',strtotime($timeline->created_at))}})
    
                                                    @elseif($timeline->method == 'Credit Card')
                                                    {{$timeline->status}} - {{$timeline->timeline_order_status}} ({{$timeline->method}} - {{date('F j Y',strtotime($timeline->created_at))}})
                                                    @endif
                                                </h4>
                                                <p>{{$timeline->notes}}</p> 
                                                <span class="vertical-timeline-element-date">{{date('g:i A',strtotime($timeline->created_at))}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>




    @section('custom')
    <script>
        $('a.od-order-history').addClass('active');
        $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection
