@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Order Details</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="text-blue"><b>Order Details<hr></b></h4>
                                    <div class="form-group">
                                        <label for="" class="text-blue">Order Date</label>
                                        <input type="text" class="form-control" disabled value="2021-01-01 01:00:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="text-blue">Order Status</label>
                                        <select class="form-control form-control-select2" required>
                                            <option>Pending</option>
                                            <option>Cancelled</option>
                                            <option>On-hold</option>
                                            <option>Delivered</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4 class="text-blue"><b>Payment Address<hr></b></h4>
                                    <p>John Doe</p>
                                    <p>Address</p>
                                    <p>City</p>
                                    <p>Philippines</p>
                                </div>
                                <div class="col-md-3">
                                    <h4 class="text-blue"><b>Shipping Address<hr></b></h4>
                                    <p>John Doe</p>
                                    <p>Address</p>
                                    <p>City</p>
                                    <p>Philippines</p>
                                </div>
                                <div class="col-md-3">
                                    <h4 class="text-blue"><b>Distributor Details<hr></b></h4>
                                    <p>123IDCode123</p>
                                    <p>Username</p>
                                    <p>John Doe</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <h4><b>Orders List<hr></b></h4>
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Pair Value</th>
                                            <th>Price</th>
                                            <th>Total Pair Value</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Product 1</td>
                                            <td>1</td>
                                            <td>₱ {{ number_format('100', 2) }}</td>
                                            <td>₱ {{ number_format('1000', 2) }}</td>
                                            <td>₱ {{ number_format('100', 2) }}</td>
                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                        </tr>
                                        <tr><td colspan="6"><hr></td></tr>
                                        <tr>
                                            <td colspan="5" class="text-right">Sub Total</td>
                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-right">Free Shipping</td>
                                            <td>₱ {{ number_format('0', 2) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /main charts -->
</div>

@section('custom')
<script>
    $('a.od-orders').addClass('active');
    $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection
