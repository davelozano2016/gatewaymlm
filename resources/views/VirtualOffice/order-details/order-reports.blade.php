@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Order Details</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="text-info icon-chart icon-2x"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Gross Sales</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="text-info icon-chart icon-2x"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Average</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="text-info icon-chart icon-2x"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Net Sales</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="text-info icon-chart icon-2x"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Average Sales</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-sm-12">
                                    <button class="btn bg-primary btn-flat">Export CSV</button>
                                </div>

                                <div class="col-lg-8 col-sm-12 text-right">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-light daterange-predefined">
                                            <span></span>
                                        </button>
                                        <button class="btn bg-primary btn-flat"><i class="icon-search4 text-right"></i></button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="chart-container">
                                <div class="chart has-fixed-height" id="line_basic"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /main charts -->
</div>
@section('custom')
<script>
    $('a.od-order-reports').addClass('active');
    $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>

<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{asset('assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
<script src="{{asset('assets/js/demo_charts/echarts/light/lines/lines_basic.js') }}"></script>
@endsection
@endsection
