@extends('layouts.VirtualOffice.app')
@section('container')
<style>
    input[type=radio]:checked + label>img {
        background-image: linear-gradient(16deg, #3f51b5, #3d696a);
    }
    .input-hidden {
        position: absolute;
        left: -9999px;
    }

    .card-img-actions label#product-error{
        position: absolute;
    }
</style>
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-xl-12">
                            <form class="wizard-form steps-validation" action="#" data-fouc>
                                <h6>Package & Sponsor Information</h6>
                                <fieldset>
                                    <div class="row">
                                        <!-- <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="card-img-actions mx-1 mt-1">
                                                            <input  type="radio" name="product" id="product" class="input-hidden required" checked/>
                                                            <label for="product"><img src="{{asset('assets/images/mini_logo.png')}}" class="img-responsive img-fluid" alt="Product Image" /></label>
                                                        </div>

                                                        <div class="card-body text-center">
                                                            <div class="align-items-start flex-nowrap">
                                                                <div>
                                                                    <div class="font-weight-semibold mr-2">Product 1</div>
                                                                    <span class="font-size-sm text-muted">&#8369; {{number_format('1200', 2)}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="card-img-actions mx-1 mt-1">
                                                            <input  type="radio" name="product" id="product2" class="input-hidden required" />
                                                            <label for="product2"><img src="{{asset('assets/images/mini_logo.png')}}" class="img-responsive img-fluid" alt="Product Image" /></label>
                                                        </div>

                                                        <div class="card-body text-center">
                                                            <div class="align-items-start flex-nowrap">
                                                                <div>
                                                                    <div class="font-weight-semibold mr-2">Product 1</div>
                                                                    <span class="font-size-sm text-muted">&#8369; {{number_format('1200', 2)}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="card-img-actions mx-1 mt-1">
                                                            <input  type="radio" name="product" id="product3" class="input-hidden required" />
                                                            <label for="product3"><img src="{{asset('assets/images/mini_logo.png')}}" class="img-responsive img-fluid" alt="Product Image" /></label>
                                                        </div>

                                                        <div class="card-body text-center">
                                                            <div class="align-items-start flex-nowrap">
                                                                <div>
                                                                    <div class="font-weight-semibold mr-2">Product 1</div>
                                                                    <span class="font-size-sm text-muted">&#8369; {{number_format('1200', 2)}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="card-img-actions mx-1 mt-1">
                                                            <input  type="radio" name="product" id="product4" class="input-hidden required" />
                                                            <label for="product4"><img src="{{asset('assets/images/mini_logo.png')}}" class="img-responsive img-fluid" alt="Product Image" /></label>
                                                        </div>

                                                        <div class="card-body text-center">
                                                            <div class="align-items-start flex-nowrap">
                                                                <div>
                                                                    <div class="font-weight-semibold mr-2">Product 1</div>
                                                                    <span class="font-size-sm text-muted">&#8369; {{number_format('1200', 2)}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div> -->

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Sponsor ID: <span class="text-danger">*</span></label>
                                                <input type="text" name="name" class="form-control required">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Placement ID: <span class="text-danger">*</span></label>
                                                <input type="text" name="name" class="form-control required">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Position: <span class="text-danger">*</span></label>
                                                <select name="birth-month" data-placeholder="Select Position" class="form-control form-control-select2 required" data-fouc>
                                                    <option></option>
                                                    <option value="1">Left</option>
                                                    <option value="2">Right</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Activation Code: <span class="text-danger">*</span></label>
                                                <input type="text" name="activation" class="form-control required">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <h6>Contact Information</h6>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12"><h3>Your Login Details</h3><hr></div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Username: <span class="text-danger">*</span></label>
                                                        <input type="text" name="username" id="username" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Password: <span class="text-danger">*</span></label>
                                                        <input type="password" name="password" id="password" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Confirm Password: <span class="text-danger">*</span></label>
                                                        <input type="password" name="cpassword" id="cpassword" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12"><h3>Your Personal Details</h3><hr></div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>First Name: <span class="text-danger">*</span></label>
                                                        <input type="text" name="firstname" id="firstname" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Last Name: <span class="text-danger">*</span></label>
                                                        <input type="text" name="lastname" id="lastname" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Email: <span class="text-danger">*</span></label>
                                                        <input type="text" name="email" id="email" class="form-control">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Mobile Number: <span class="text-danger">*</span></label>
                                                        <input type="text" name="mobile_number" id="mobile_number" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12"><h3>Your Address</h3><hr></div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Address 1: <span class="text-danger">*</span></label>
                                                        <input type="text" name="address1" id="address1" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Address 2: <span class="text-danger">*</span></label>
                                                        <input type="text" name="address2" id="address2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City: <span class="text-danger">*</span></label>
                                                        <input type="text" name="city" id="city" class="form-control">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Post Code: <span class="text-danger">*</span></label>
                                                        <input type="text" name="post_code" id="post_code" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Country:</label>
                                                        <select name="country" data-placeholder="Select Country" class="form-control form-control-select2" data-fouc>
                                                            <option></option> 
                                                            <option value="1">United States</option> 
                                                            <option value="2">France</option> 
                                                            <option value="3">Germany</option> 
                                                            <option value="4">Spain</option> 
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Region/State:</label>
                                                        <select name="region" data-placeholder="Select Region/State" class="form-control form-control-select2" data-fouc>
                                                            <option></option> 
                                                            <option value="1">United States</option> 
                                                            <option value="2">France</option> 
                                                            <option value="3">Germany</option> 
                                                            <option value="4">Spain</option> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group d-flex align-items-center">
                                                <div class="form-check mb-0">
                                                    <label class="form-check-label">
                                                        <div class="uniform-checker"><input type="checkbox" name="remember" class="form-input-styled" checked="" data-fouc=""></div>
                                                        I have read and agree to the Privacy Policy
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>

                                <h6>Payment Information</h6>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <label for="">Please select the preferred payment method to use on this order.</label>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" checked>
                                                    Bank Transfer
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input">
                                                    Bitgo
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input">
                                                    BlockChain
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input">
                                                    E-PIN
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input">
                                                    E-Wallet
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input">
                                                    PayPal Express Checkout
                                                </label>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Add Comments About Your Order</label>
                                                <textarea name="experience-description" rows="4" cols="4" placeholder="Tasks and responsibilities" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group d-flex align-items-center">
                                                <div class="form-check mb-0">
                                                    <label class="form-check-label">
                                                        <div class="uniform-checker"><input type="checkbox" name="remember" class="form-input-styled" data-fouc=""></div>
                                                        I have read and agree to the Terms & Conditions
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <h6>Confirm Order</h6>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Product Name</th>
                                                            <th>Model</th>
                                                            <th>Quantity</th>
                                                            <th>Unit Price</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Testing</td>
                                                            <td>Testing</td>
                                                            <td>1</td>
                                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan=4 class="text-right"><b>Sub Total:</b></td>
                                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan=4 class="text-right"><b>Total:</b></td>
                                                            <td>₱ {{ number_format('1200', 2) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h3>Bank Transfer Instructions</h3>
                                            <p>Please transfer the total amount to the following bank account.</p>

                                            <p>Your order will not ship until we receive payment.</p>

                                            <div class="form-group">
                                                <label for="">Select Receipt</label>
                                                <input type="file">
                                                <small class="text-danger">Allowed types JPG|JPEG|PNG only</small>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



@section('custom')
<script>
    $('a.register').addClass('active');
</script>
<script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/inputs/inputmask.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_wizard.js') }}"></script>
@endsection
@endsection