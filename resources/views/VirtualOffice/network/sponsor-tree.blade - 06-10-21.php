@extends('layouts.VirtualOffice.app')
@section('container')
<style>
    /* Custom styles for popover */
    .pop-image {
        width:50px;
    }
    .pop-container {
        width:200px !important;
    }
    
    .custom {
        width:75% !important;
        margin:auto;
    }
    
    #pop {
        padding:0px 0px;
    }
    #example {
        position:relative;
    }
    
    
    * {padding: 0}
    .tree div.extended {
    }
    
    .tree div.extendedv2{
        width:50% !important;
        border-top:1px solid #ccc !important;
        border-right:1px solid #ccc !important;
        margin-top:-4px;
    }
    
    
    .tree div.co{
        width:0% !important;
        margin:auto;
        border-left:1px solid #ccc !important;
    }
    
    
    .tree ul {
        padding-top: 20px; position: relative;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    .tree ul li  ul li {
        width:50%;
    }
    
    .tree li {
        float: left; text-align: center;
        list-style-type: none;
        position: relative;
        padding: 20px 5px 0 3px;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        border:none !important;
    }
    
    .tree li a i {
        font-size:24px;
    }
    
    /*We will use ::before and ::after to draw the connectors*/
    
    .tree li::before, .tree li::after{
        content: '';
        position: absolute; top: 0; right: 50%;
        border-top: 1px solid #ccc;
        width: 50%; height: 20px;
    }
    .tree li::after{
        right: auto; left: 50%;
        border-left: 1px solid #ccc;
    }
    
    /*We need to remove left-right connectors from elements without 
    any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
        display: none;
    }
    
    /*Remove space from the top of single children*/
    .tree li:only-child{ padding-top: 0;}
    
    /*Remove left connector from first child and 
    right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after{
        border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before{
        border-right: 1px solid #ccc;
        border-radius: 0 0px 0 0;
        -webkit-border-radius: 0 0px 0 0;
        -moz-border-radius: 0 0px 0 0;
    }
    .tree li:first-child::after{
        -webkit-border-radius: 0px 0 0 0;
        -moz-border-radius: 0px 0 0 0;
    }
    
    /*Time to add downward connectors from parents*/
    .tree ul ul::before{
        content: '';
        position: absolute; top: 0; left: 50%;
        border-left: 1px solid #ccc;
        width: 0; height: 20px;
    }
    
    .tree li a{
        border: 1px solid #fff;
        /* padding: 5px 10px; */
        text-decoration: none;
        color: #666;
        font-family: arial, verdana, tahoma;
        font-size: 11px;
        display: inline-block;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    
    
    .sample{
        border: 1px solid #fff;
        /* padding: 5px 10px; */
        text-decoration: none;
        color: #666;
        font-family: arial, verdana, tahoma;
        font-size: 11px;
        display: inline-block;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    
    
    /*Thats all. I hope you enjoyed it.
    Thanks :)*/
    
    
    </style>

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <section id="focal">
                <h1>Use the mousewheel to zoom on a focal point</h1>
                <div class="parent" style="height:1000px;background:#fff">
                  <div class="panzoom" >
                    <div class="tree" >
                        <ul>
                            <li style="width:100%">
                                {{-- <a class="pop" data-container="body" data-toggle="popover" data-placement="right">Parent</a> --}}
                                <ul>
                                    <li style="width:50%">
                                        <a class="pop" data-container="body" data-toggle="popover" data-placement="right" >
                                            <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px">
                                                <img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt="">
                                            </div>
                                            <div class="py-1">
                                                <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <span style="display:block;">
                                                        <a href="" style="font-size:10px;"><b>COMPUTOLOGY</b></a> <br>
                                                        <b><a href="">123456</a></b> <br> 
                                                        <b class="badge badge-success">PREMIUM</b>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                        <ul>
                                            <li style="width:50%">
                                                <a class="pop" data-container="body" data-toggle="popover" data-placement="right">
                                                    <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>
                                                    <div class="py-1">
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 col-12">
                                                            <span style="display:block">
                                                                <a href="" style="font-size:10px"><b>COMPUTOLOGY</b></a> <br>
                                                                <b><a href="">123456</a></b> <br> 
                                                                <b class="badge badge-success">PREMIUM</b>
                                                                <div class="co">&nbsp;</div>
                                                                {{-- <a id="continues" style="display:block" class="mt-1 arrow"><i style="font-size:14px !important" class="icon icon-arrow-down16"></i> <span class="line-separator"></span></a> --}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>

                                            </li>
                                            

                                            <li style="width:50%">
                                                <a class="pop" data-container="body" data-toggle="popover" data-placement="right">
                                                    <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>
                                                    <div class="py-1">
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 col-12">
                                                            <span style="display:block">
                                                                <a href="" style="font-size:10px"><b>COMPUTOLOGY</b></a> <br>
                                                                <b><a href="">123456</a></b> <br> 
                                                                <b class="badge badge-success">PREMIUM</b>
                                                                <a id="continues" style="display:block" class="mt-1 arrow"><i style="font-size:14px !important" class="icon icon-arrow-down16"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li style="width:50%">
                                        <a class="pop" data-container="body" data-toggle="popover" data-placement="right">
                                            <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>
                                            <div class="py-1">
                                                <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <span style="display:block">
                                                        <a href="" style="font-size:10px"><b>COMPUTOLOGY</b></a> <br>
                                                        <b><a href="">123456</a></b> <br> 
                                                        <b class="badge badge-success">PREMIUM</b>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                        <ul>
                                            <li style="width:50%">
                                                <a class="pop" data-container="body" data-toggle="popover" data-placement="right">
                                                    <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>
                                                    <div class="py-1">
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 col-12">
                                                            <span style="display:block">
                                                                <a href="" style="font-size:10px"><b>COMPUTOLOGY</b></a> <br>
                                                                <b><a href="">123456</a></b> <br> 
                                                                <b class="badge badge-success">PREMIUM</b>
                                                                <a id="continues" style="display:block" class="mt-1 arrow"><i style="font-size:14px !important" class="icon icon-arrow-down16"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>

                                            <li style="width:50%">
                                                <a class="pop" data-container="body" data-toggle="popover" data-placement="right">
                                                    <div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>
                                                    <div class="py-1">
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span> &nbsp;
                                                        <span style="background:#9ed98c" class="text-white badge"><b>0 PV</b></span>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 col-12">
                                                            <span style="display:block">
                                                                <a href="" style="font-size:10px"><b>COMPUTOLOGY</b></a> <br>
                                                                <b><a href="">123456</a></b> <br> 
                                                                <b class="badge badge-success">PREMIUM</b>
                                                                <a id="continues" style="display:block" class="mt-1 arrow"><i style="font-size:14px !important" class="icon icon-arrow-down16"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                  </div>
                </div>
              </section>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script src="{{asset("assets/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{asset("assets/js/jquery.panzoom.js')}}"></script>
<script>
    $('a.n-sponsor-tree').addClass('active');
    $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
    $(".pop").popover({ trigger: "manual" ,
        content: '<div class="pop-container"><div class="row"><div class="text-center py-2 col-12"><img class="pop-image" src="{{asset("assets/images/mini_logo.png")}}" alt=""><br>computology<br>123456</div></div> <div class="row"><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{$date->format("Y/m/d")}}</div></div><div class="row"><div class="col-md-5 col-5">Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">12100</div></div><div class="row"><div class="col-md-5 col-5">Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">4200</div></div><div class="row"><div class="col-md-5 col-5">Left Carry</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row"><div class="col-md-5 col-5">Right Carry</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">1000</div></div><div class="row"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">16340</div></div><div class="row mt-2"><div class="col-md-12"><span class="btn-block badge badge-success">PREMIUM</span></div></div></div>',
        html: true, animation:false})
        .on("mouseenter", function () {
            var _this = this;
            $(this).popover("show");
            $(".popover").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }).on("mouseleave", function () {
            var _this = this;
            setTimeout(function () {
                if (!$(".popover:hover").length) {
                    $(_this).popover("hide");
                }
            }, 100);
    });

    $('#continues').click( e => {
        $('.continues').append(
        '<div class="custom"><div class="tree"> '+
            '<ul>'+
                '<li style="width:100%">'+
                    '<div class="extendedv2">&nbsp;</div><a class="pop" data-container="body" data-toggle="popover" data-placement="right">'+
                        '<div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px"><img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt=""></div>'+
                    '</a>'+
                    '<ul>'+
                        '<li style="width:50%">'+
                            '<a>'+
                                '<div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px">'+
                                    '<img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt="">'+
                                '</div>'+
                            '</a>'+
                        '</li>'+

                        '<li style="width:50%">'+
                            '<a>'+
                                '<div style="margin:auto;border:1px solid #ccc;border-radius:100% !important;width:60px;padding:10px">'+
                                    '<img style="width:30px" class="img-responsive" src="{{asset("assets/images/mini_logo.png")}}" alt="">'+
                                '</div>'+
                            '</a>'+
                        '</li>'+
                    '</ul>'+
                '</li>'+
            '</ul>'+
        '</div></div>')
    })
    
</script>
<script>
    (function() {
          var $section = $('#focal');
          var $panzoom = $section.find('.panzoom').panzoom();
          $panzoom.parent().on('mousewheel.focal', function( e ) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            $panzoom.panzoom('zoom', zoomOut, {
              animate: false,
              focal: e
            });
          });
        })();
</script>

@endsection
@endsection