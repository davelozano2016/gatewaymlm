@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Support Center</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <div class="row">

                    <div class="col-md-3 ">
                        <div class="card card-body unilevel-box">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <img class="img-fluid rounded-circle computology-img"
                                        src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                </div>
                                <div class="media-body text-right">
                                    
                                    <h6 class="font-weight-semibold mb-0 text-uppercase">{{$query[0]->username}} </h6>
                                    <span class="text-uppercase font-size-sm text-muted">
                                        <div class="btn-group">
                                            <a style="border-radius:0 !important;" href="#" class="badge badge">Add Points</a> 
                                            <b style="border-radius:0 !important;" class="badge badge-success">Auto Order On</b> 
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-body unilevel-box">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-info icon-wallet icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱0.00</h3>
                                    <span class="text-uppercase font-size-sm text-muted">My E-Wallet Balance</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="card card-body unilevel-box">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-primary icon-cart2 icon-3x top-margin-15"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">0.00 PV (₱0.00)</h3>
                                    <span class="text-uppercase font-size-sm text-muted">My Total Orders</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="card card-body unilevel-box">
                            <div class="media">
                                <div class="mr-3 align-self-center" id="c-count-header">
                                    <i class="text-success icon-cart2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱0.00</h3>
                                    <span class="text-uppercase font-size-sm text-muted">Total Group Sales</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="card bg-success card-body unilevel-second-box">
                            <h2 class="mb-0"><strong>0.00 PV (₱0.00)</strong></h2>
                            <p class="">My Current Orders</p>
                            <div class="progress">
                                <div class="progress-bar" style="width: 39%">
                                    <span>39%</span>
                                </div>
                            </div>
                            <div class="mt-1">
                                <b class="">{{date('M Y')}} Current Month Target PV Progress Bar
                                    Monitoring</b>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card bg-blue card-body unilevel-second-box">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="mb-0"><strong>₱0.00</strong></h2>
                                    <p style="color:#fff !important" class="">Pending Commission</p>
                                    <b style="color:#fff !important" class="">As of {{date('M Y')}} Group
                                        Sales Performance</b>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 mb-0">

                        <div class="card">
                            <div class="card-body ">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mt-1 mb-2 form-check form-check-switchery form-check-switchery-double">
                                            <label class=" form-check-label">
    
                                                <input type="checkbox" class="form-check-input-switchery" checked data-fouc>
                                                Switch to Dynamic Compression
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="col-sm-3 form-control selectpicker float-right" data-live-search="true" id="select-level">
                                            @for($i=1;$i<=$settings->unilevel_count;$i++) 
                                                <option value="vertical-left-tab{{$i}}">Level {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                    

                                <div class="tab-content">
                                    <!-- Level 1 -->
                                    <?php $total_unilevel_income = 0;?>
                                    @for($x=1;$x<=$settings->unilevel_count;$x++) 
                                    <div class="tab-pane show <?=$x % $settings->unilevel_count == 1 ? 'active' : '' ?>"
                                        id="vertical-left-tab{{$x}}">
                                        <!-- Rounded thumbs -->
                                        <div class="row">
                                            <div class="col-md-12 mb-1">
                                                <h4 class="mt-2 text-right text-primary">
                                                    <strong class=" badge badge-warning">{{$total_unilevel_income}} {{$users[0]->currency_code}}</strong> 
                                                </h4>
                                            </div>
                                        @foreach($unilevel as $level)
                                            @if($level->level == $x)
                                                <div class="col-md-3 col-6 mb-4">
                                                    <div
                                                        class="media level-box align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row bg-light-gold">
                                                        <div class="mr-lg-3 mb-3 mb-lg-0">
                                                            <img src="{{asset('assets/images/mikewooting.jpg')}}" width="80"
                                                                height="80" class="rounded-circle" alt="">
                                                        </div>
                                                        <h6 class="media-title font-weight-semibold">
                                                            <label class="btn btn-primary btn-sm">{{$level->waiting_id_number}}</label><br>
                                                            @if($level->waiting_id_number == $UsersUnilevel[$level->waiting_id_number]->id_number)
                                                                <?php $total_unilevel_income = $UsersUnilevel[$level->waiting_id_number]->unilevel_income;?>
                                                                <label>{{number_format($UsersUnilevel[$level->waiting_id_number]->unilevel_point_value,2)}} UPV</label><br>
                                                                <label>{{number_format($UsersUnilevel[$level->waiting_id_number]->unilevel_income,2)}} {{$users[0]->currency_code}}</label>
                                                            @else
                                                            @endif
                                                        </h6>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h3 class="card-title text-primary">Group Sales <small class="text-muted">History</small></h3>

            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>Date Purchased</th>
                                        <th>Product Name</th>
                                        <th style="width:1px">Price</th>
                                        <th style="width:1px">PV</th>
                                        <th>Order No.</th>
                                        <th>Ordered By</th>
                                        <th>Encoded To</th>
                                        <th>Date Encoded</th>
                                        <th style="width:1px">Level</th>
                                        <th style="width:1px">Commission</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=10;$i++) <tr>
                                        <td>{{$i}}</td>
                                        <td>{{date('Y-m-d')}}</td>
                                        <td>sample</td>
                                        <td>{{$query[0]->currency_symbol}}{{number_format($i,2)}}</td>
                                        <td>{{number_format($i,2)}}</td>
                                        <td>sample</td>
                                        <td>sample</td>
                                        <td>johnwick</td>
                                        <td>{{date('Y-m-d')}}</td>
                                        <td>1</td>
                                        <td>{{$query[0]->currency_symbol}}0.00</td>
                                        </tr>
                                        @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- /traffic sources -->

</div>
<!-- /main charts -->


@section('custom')
<script>
$('a.n-unilevel-product').addClass('active');
$('#select-level').selectpicker();
$( "#select-level" ).on( 'changed.bs.select', function(e) {
    var selectLevelVal = $("#select-level").val();
    $('.tab-pane').each(function(){
        $(this).removeClass('show');
        $(this).removeClass('active');
    });
    $('#' + selectLevelVal).tab('show');

});
</script>
<script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
@endsection
@endsection