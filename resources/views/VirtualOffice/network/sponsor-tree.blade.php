@extends('layouts.VirtualOffice.app')
@section('container')



<link type="text/css" rel="stylesheet" href="{{asset('assets/sponsor/css/jquery.jOrgChart.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/sponsor/css/custom.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/sponsor/css/prettify.css')}}"  />
<style>
    .toScroll {
        margin: 4px, 4px;
        padding: 4px;
        width: 100%;
        overflow-x: auto;
        overflow-y: hidden;
        white-space: nowrap;
    }
</style>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Network</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row ">
            <div class="col-xl-12">
                <div class="card ">
                    <div class="card-body ">

                        <div class="toScroll">
                            <div class="row ">
                                <div class="col-md-12">
                                    {!!$test!!}
                                    <div id="chart" class="orgChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <div class="modal" id="sponsorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-slate">
                        <h5 class="modal-title" id="exampleModalLabel">Sponsor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Information here
                    </div>
                </div>
            </div>
        </div>

    @section('custom')

    <script>
        $('a.n-sponsor-tree').addClass('active');
        $('li.network-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/sponsor/prettify.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/sponsor/jquery.jOrgChart.js')}}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

    
    <script>


        jQuery(document).ready(function () {
            $("#org").jOrgChart({
                chartElement: '#chart',
            });

            $('[data-toggle="popover"]').popover({
                trigger: 'hover'
            });
            
        });
        jQuery(document).ready(function () {

            /* Custom jQuery for the example */
            $("#show-list").click(function (e) {
                e.preventDefault();

                $('#list-html').toggle('fast', function () {
                    if ($(this).is(':visible')) {
                        $('#show-list').text('Hide underlying list.');
                        $(".topbar").fadeTo('fast', 0.9);
                    } else {
                        $('#show-list').text('Show underlying list.');
                        $(".topbar").fadeTo('fast', 1);
                    }
                });
            });

            $('#list-html').text($('#org').html());

            $("#org").bind("DOMSubtreeModified", function () {
                $('#list-html').text('');

                $('#list-html').text($('#org').html());

                prettyPrint();
            });
        });


    </script>


    @endsection
    @endsection
