<div id="modal_form_Pagibig" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pag-Ibig</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Payor Type</label>
                            <select class="form-control">
                                <option value="SE">Self Employed - Contributions</option>
                                <option value="VO">Voluntary - Contributions</option>
                                <option value="OFW">OFW - Contributions</option>
                                <option value="NWS">Non-Working Spouse - Contributions</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Account No(12-20 digits)</label>
                            <input type="number" class="form-control" placeholder="Enter 12-20 digits MID/HLID No."
                                required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Amount</label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">PHP</span>
                                </span>
                                <input type="number" class="form-control" placeholder="Enter Amount in PHP(1-8 digits)"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Period Covered From</label>
                            <div class="input-group">
                                <input type="date" placeholder="Select Period Covered From" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Period Covered To</label>
                            <div class="input-group">
                                <input type="date" placeholder="Select Period Covered To" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 mt-2">
                        <div class="alert alert-primary border-0 alert-dismissible">
                            <span> PAYMENTS made beyond 08:30PM will be posted the next day.<br></span>
                            <span> Past due and disconnected amount can still be accepted</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn text-uppercase bg-primary">Pay Now</button>
            </div>
        </div>
    </div>
</div>