
<div id="modal_creditcards" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Credit Cards</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                     <div class="modal-body" id="modal-dialog-scrollable">                      
                       
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_bpicreditcard" data-dismiss="modal">BPI Credit Card</button> 
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_aubcreditcards" data-dismiss="modal">Aub Credit Cards</button>  
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_metrobank" data-dismiss="modal">Metrobank</button> 
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_robinsonbankcreditcard" data-dismiss="modal">Robinson Bank Credit Card</button> 
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_securitybankmastercard" data-dismiss="modal">Securitybank MasterCard</button> 
                    


                    </div>


            </div>
        </div>
    </div>

</form>
