<div id="modal_waterutilities" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Water Utilities</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                        <div class="modal-body" id="modal-dialog-scrollable">    
                        <!-- <i class="icon-search4 text-dark" id="showSearch"></i>    -->
                        <div id="icn">
                        <a class="list-icons-item" data-action="collapse" id="showSearchWater"></a> 
                        </div>            
                        <div id="mySearchWater" style="display:none;">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-dark border-dark text-white">
                                    <i class="icon-search4"></i>
                                    </span>
                                </span>
                                <input class="form-control" id="myInputWater" type="text" placeholder="Biller Name">
                            </div>
                        </div>
                            <div id="myDIVWater">
                                <br> 
                      
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_manilawater" data-dismiss="modal">Manila Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_maynilad" data-dismiss="modal">Maynilad</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_primewater" data-dismiss="modal">Prime Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_staluciarealty" data-dismiss="modal">Sta Lucia Realty</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_legazpiwater" data-dismiss="modal">Legazpi Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tabacowater" data-dismiss="modal">Tabaco Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mcwd" data-dismiss="modal">Mcwd</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_stamariawater" data-dismiss="modal">Sta Maria Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_firstpeak" data-dismiss="modal">First Peak</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_plaridelwater" data-dismiss="modal">Plaridel Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_lagunawater" data-dismiss="modal">Laguna Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_baguiowater" data-dismiss="modal">Baguio Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_bpwaterworks" data-dismiss="modal">Bp Waterworks</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_goodhandswater" data-dismiss="modal">Goodhands Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_happywell" data-dismiss="modal">Happy Well</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_daragawater" data-dismiss="modal">Daraga Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_calumpitwater" data-dismiss="modal">Calumpit Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tagumwater" data-dismiss="modal">Tagum Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mabalacatwater" data-dismiss="modal">Mabalacat Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_boracaywater" data-dismiss="modal">Boracay Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_clarkwater" data-dismiss="modal">Clark Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_piliwater" data-dismiss="modal">Pili Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cdowater" data-dismiss="modal">Cdo Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_lagunawaterdistrict" data-dismiss="modal">Laguna Water District</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_manilawaterphilventures" data-dismiss="modal">Manilawater Philventures</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_angeleswater" data-dismiss="modal">Angeles Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_bulacanwater" data-dismiss="modal">Bulacan Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_baciwa" data-dismiss="modal">Baciwa</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_obando" data-dismiss="modal">Obando</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_ilaganwater" data-dismiss="modal">Ilagan Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dagupancitywater" data-dismiss="modal">Dagupan City Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_metroiloilowater" data-dismiss="modal">Metro Iloilo Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_subicwater" data-dismiss="modal">Subic Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_filinvestwater" data-dismiss="modal">Filinvest Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tagaytaywater" data-dismiss="modal">Tagaytay Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_balangawater" data-dismiss="modal">Balanga Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_libmananwater" data-dismiss="modal">Libmanan Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dinalupihanwater" data-dismiss="modal">Dinalupihan Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_bulakanwater" data-dismiss="modal">Bulakan Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_calasiaowater" data-dismiss="modal">Calasiao Water</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_aquacentro" data-dismiss="modal">Aqua Centro</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_teresawaterworks" data-dismiss="modal">Teresa Waterworks</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_hermosawaterdistrict" data-dismiss="modal">Hermosa Water District</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_metroroxaswaterdistrict" data-dismiss="modal">Metro Roxas Water District</button> 
                            
                            </div>
                    </div>
            </div>
        </div>
    </div>



    <script>
let btns = document.querySelector("#showSearchWater");
let divs = document.querySelector("#mySearchWater");

btns.addEventListener('click', () =>{ 
    if(divs.style.display === 'none'){
        divs.style.display = 'block';
    }else{
        divs.style.display = 'none';
    }

});


    $(document).ready(function(){
        $("#myInputWater").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIVWater *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
     });
</script>
