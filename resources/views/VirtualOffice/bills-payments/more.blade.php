<div id="modal_more" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">More</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                    <div class="modal-body" id="modal-dialog-scrollable">                   
                        <h5 class="card-header font-weight-bold bg-secondary">Electronic Cash</h5>                  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_grab" data-dismiss="modal">Grab</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_abra" data-dismiss="modal">Abra</button>                          
                        <br>
                        <h5 class="card-header font-weight-bold bg-secondary">Memorial Plan</h5>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_goldenheaven" data-dismiss="modal">Golden Heaven</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_celestialmeadows" data-dismiss="modal">Celestial Meadows</button>
                        <br>
                        <h5 class="card-header font-weight-bold bg-secondary">Fund Transfer</h5>                  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_gate" data-dismiss="modal">Gate</button>                       
                    </div>


            
        </div>
    </div>
</div>