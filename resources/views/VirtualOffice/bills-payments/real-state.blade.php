
<div id="modal_realstate" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Real State</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                    <div class="modal-body" id="modal-dialog-scrollable">

                       <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_rsproperty" data-dismiss="modal">Rs Property</button> 
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_honeycomb" data-dismiss="modal">Honeycomb</button>
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_rdakland" data-dismiss="modal">Rdak Land</button>  
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sanagustinrealtysancdepaz" data-dismiss="modal">Sanagustin Realty Sancdepaz</button>     
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_johndorfventures" data-dismiss="modal">Johndorf Ventures</button>  
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_activerealty" data-dismiss="modal">Active Realty</button>  
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_paproperties" data-dismiss="modal">Pa Properties</button> 
                    </div>
            </div>
        </div>
    </div>
