<div id="modal_form_maynilad" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                   <div class="d-flex justify-content-center">
                                <a href="#"><img src="../assets/images/billerlogo/maynilad.png"
                                        width="89" height="89" class="rounded-circle" alt=""></a>
                            </div><br>             
                            <h5 class="modal-title text-center">Maynilad</h5>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Contract Account Number </label>
                                <input type="number" name="minimum_number" class="form-control"
                                    placeholder="Enter Contract Account Number" required
                                    placeholder="Please enter a value less than or equal to 10" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Name</label>
                                <input type="text" class="form-control" placeholder="Enter Account Name" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Amount Paid</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PHP</span>
                                    </span>
                                    <input type="text" class="form-control"
                                        placeholder="Enter Amount Paid in PHP(1-8 digits)" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Amount Due</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PHP</span>
                                    </span>
                                    <input type="text" class="form-control"
                                        placeholder="Enter Amount Due in PHP(1-8 digits)" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Convenience Fee</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" disabled value="0.00">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="alert alert-primary border-0 alert-dismissible">
                                <span> PAYMENTS made beyond 02:00PM will be posted the next day.<br></span>
                                <span> Past due and disconnected amount can still be accepted</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn text-uppercase bg-primary">Pay Now</button>
                </div>
            </div>
        </div>
    </div>