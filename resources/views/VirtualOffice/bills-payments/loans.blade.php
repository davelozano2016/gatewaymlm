
<div id="modal_loans" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Loans</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                     
                     <div class="modal-body" id="modal-dialog-scrollable">    
                        <!-- <i class="icon-search4 text-dark" id="showSearch"></i>    -->
                        <div id="icn">
                        <a class="list-icons-item" data-action="collapse" id="showSearchLoan"></a> 
                        </div>            
                        <div id="mySearchLoan" style="display:none;">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-dark border-dark text-white">
                                    <i class="icon-search4"></i>
                                    </span>
                                </span>
                                <input class="form-control" id="myInputLoan" type="text" placeholder="Biller Name">
                            </div>
                        </div>
                            <div id="myDIVLoan">
                                <br> 

                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sumisho" data-dismiss="modal">Sumisho</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_norkis" data-dismiss="modal">Norkis</button>
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_unistar" data-dismiss="modal">Unistar</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mfilending" data-dismiss="modal">MFI Lending</button>     
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_aeoncredit" data-dismiss="modal">Aeon Credit</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_maybridge" data-dismiss="modal">Maybridge</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_homecredit" data-dismiss="modal">Home Credit</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_chinatrust" data-dismiss="modal">China Trust</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_globaldominion" data-dismiss="modal">Global Dominion</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_insularsavers" data-dismiss="modal">Insular Savers</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_asialink" data-dismiss="modal">Asialink</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_finaswide" data-dismiss="modal">Finaswide</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_southasialink" data-dismiss="modal">South Asialink</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dragonloans" data-dismiss="modal">Dragonloans</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tagumcoop" data-dismiss="modal">Tagum Coop</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_easycash" data-dismiss="modal">Easycash</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_peraagad" data-dismiss="modal">Pera Agad</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_enterprisebankloan" data-dismiss="modal">Enterprise Bank Loan</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_quickpera" data-dismiss="modal">Quick Pera</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_loanstar" data-dismiss="modal">Loan Star</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_olmfinancingcorp" data-dismiss="modal">OLM Financing Corp</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_icashexpress" data-dismiss="modal">Icash Express</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_rfcloan" data-dismiss="modal">RFC Loan</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_lipabank" data-dismiss="modal">Lipa Bank</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_simbayanancoop" data-dismiss="modal">Simbayanan Coop</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_fuselending" data-dismiss="modal">Fuse Lending</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pahiram" data-dismiss="modal">Pahiram</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_megasaver" data-dismiss="modal">Megasaver</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_acom" data-dismiss="modal">Acom</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pacificaceloan" data-dismiss="modal">Pacific Ace Loan</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_skypay" data-dismiss="modal">Skypay</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cashalo" data-dismiss="modal">Cashalo</button>     
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_upesolending" data-dismiss="modal">U Peso Lending</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_payexpressloans" data-dismiss="modal">Payexpress Loans</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_nwowmarketing" data-dismiss="modal">Nwow Marketing</button>                            
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cvmfinance" data-dismiss="modal">CVM Finance</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_umbrella" data-dismiss="modal">Umbrella</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_motorstar" data-dismiss="modal">Motorstar</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_parajet" data-dismiss="modal">Parajet</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_afterwestmicroloans" data-dismiss="modal">Afterwest Microloans</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_globalsmeloans" data-dismiss="modal">Global SME Loans</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_lendpinoy" data-dismiss="modal">Lendpinoy</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_afcsmefinance" data-dismiss="modal">AFC SME Finance</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_emcor" data-dismiss="modal">Emcor</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_ctfsi" data-dismiss="modal">Ctfsi</button>
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_kservico" data-dismiss="modal">Kservico</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_ksprime" data-dismiss="modal">Ksprime</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_andali" data-dismiss="modal">Andali</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_countryfunders" data-dismiss="modal">Country Funders</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_motorcentral" data-dismiss="modal">Motorcentral</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_primeloanplus" data-dismiss="modal">Prime Loan Plus</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_benlify" data-dismiss="modal">Benlify</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_homeoutlet" data-dismiss="modal">Home Outlet</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mitsukoshi" data-dismiss="modal">Mitsukoshi</button>     

                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_zurich" data-dismiss="modal">Zurich</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_jaccs" data-dismiss="modal">Jaccs</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_yykredit" data-dismiss="modal">YY Kredit</button>                            
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_makatifinance" data-dismiss="modal">Makati Finance</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_chaileaseberjaya" data-dismiss="modal">Chailease Berjaya</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cropital" data-dismiss="modal">Cropital</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_citiappliance" data-dismiss="modal">Citi Appliance</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cashbee" data-dismiss="modal">Cashbee</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_orico" data-dismiss="modal">Orico</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cepatkredit" data-dismiss="modal">Cepat Kredit</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pesomat" data-dismiss="modal">Pesomat</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_encore" data-dismiss="modal">Encore</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_unionbankquickloans" data-dismiss="modal">Unionbank Quickloans</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mysuperpay" data-dismiss="modal">Mysuperpay</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_flexifinance" data-dismiss="modal">Flexi Finance</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_mentors" data-dismiss="modal">Mentors</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_agribank" data-dismiss="modal">Agribank</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_truemoney" data-dismiss="modal">Truemoney</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_parpahiram" data-dismiss="modal">Par Pahiram</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cardinc" data-dismiss="modal">Card INC</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_" data-dismiss="modal">Card MRI Rizal Bank</button>                               
                                
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cardsmebank" data-dismiss="modal">Card SME Bank</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cardbankinc" data-dismiss="modal">Card Bank INC</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cebucficoop" data-dismiss="modal">Cebu CFI Coop</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_firststandard" data-dismiss="modal">First Standard</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dungganon" data-dismiss="modal">Dungganon</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tala" data-dismiss="modal">Tala</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_novadeci" data-dismiss="modal">Novadeci</button>   
                            </div>

                    </div>


            </div>
        </div>
    </div>


    <script>
let btnzs = document.querySelector("#showSearchLoan");
let divzs = document.querySelector("#mySearchLoan");

btnzs.addEventListener('click', () =>{ 
    if(divzs.style.display === 'none'){
        divzs.style.display = 'block';
    }else{
        divzs.style.display = 'none';
    }

});


    $(document).ready(function(){
        $("#myInputLoan").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIVLoan *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
     });
</script>

