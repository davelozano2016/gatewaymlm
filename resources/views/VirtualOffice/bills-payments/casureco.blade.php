
<div id="modal_form_casureco" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
            
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Number </label>
                                <input type="number" name="AccountNo" class="form-control"
                                    placeholder="Enter 8 Digits Account Number">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Name</label>
                                <input type="text" class="form-control" name="Identifier" placeholder="Enter Account Name" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Amount</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PHP</span>
                                    </span>
                                    <input type="text" name="Amount" class="form-control"
                                        placeholder="" required>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Convenience Fee</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly name="ConvenienceFee" value="15">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="alert alert-primary border-0 alert-dismissible">
                                <span> Past due can be accepted (plus interest and arrears).<br></span>
                                <span> Only the exact amount should be accepted</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn text-uppercase bg-primary">Pay Now</button>
                </div>
            </div>
        </div>
    </div>

