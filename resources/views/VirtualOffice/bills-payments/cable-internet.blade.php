<div id="modal_cableinternet" class="modal " tabindex="-1">
<!-- <div class="modal-dialog modal-dialog-scrollable"> -->
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-dark">
            <h5 class="modal-title" style="font-weight:600;">Cable & Internet</h5>
            <div class="list-icons">          
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        </div>	

            <div class="modal-body" id="modal-dialog-scrollable">    
                <!-- <i class="icon-search4 text-dark" id="showSearch"></i>    -->
                <a class="list-icons-item" data-action="collapse" id="showSearch"></a>             
                <div id="mySearch" style="display:none;">
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text bg-dark border-dark text-white">
                            <i class="icon-search4"></i>
                            </span>
                        </span>
                        <input class="form-control" id="myInput" type="text" placeholder="Biller Name">
                    </div>
                </div>
                    <div id="myDIV">
                         <br>          
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cignal" data-dismiss="modal">Cignal</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_skycable" data-dismiss="modal">Skycable</button>     
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dctvcable" data-dismiss="modal">Dctv Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_convergeict" data-dismiss="modal">Converge Ict</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_angelescable" data-dismiss="modal">Angeles Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_centralluzoncable" data-dismiss="modal">Central Luzon Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_hitechcable" data-dismiss="modal">Hitech Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_primecastcable" data-dismiss="modal">Primecast Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_royalcable" data-dismiss="modal">Royal Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sanclementecable" data-dismiss="modal">Sanclemente Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_goldeneaglecable" data-dismiss="modal">Golden Eagle Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cabletelevision" data-dismiss="modal">Cable Television</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_parasat" data-dismiss="modal">Parasat</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_southluzoncatv" data-dismiss="modal">South Luzon Catv</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_jmdcablenetwork" data-dismiss="modal">Jmd Cable Network</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_bicore" data-dismiss="modal">Bicore</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_filproducts" data-dismiss="modal">Fil Products</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_broadbandeverywhere" data-dismiss="modal">Broadband Everywhere</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_eaglevisioninc" data-dismiss="modal">Eagle Vision Inc</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_excelcable" data-dismiss="modal">Excel Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_shamabroadbandandcatv" data-dismiss="modal">Shama Broadband and Catv</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_galaxycable" data-dismiss="modal">Galaxy Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_trendcable" data-dismiss="modal">Trend Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_gentriascable" data-dismiss="modal">Gentrias Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_integranetnetworkservices" data-dismiss="modal">Integranet Network Services</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tagaytaybroadbandandcatv" data-dismiss="modal">Tagaytay Broadband and Catv</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_easytv" data-dismiss="modal">Easytv</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_nowcorp" data-dismiss="modal">Now Corp</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_binangonancable" data-dismiss="modal">Binangonan Cable</button>  
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_kabayancable" data-dismiss="modal">Kabayan Cable</button>     
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_skylinecatv" data-dismiss="modal">Skyline Catv</button> 
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_asianvisioncable" data-dismiss="modal">Asian Vision Cable</button>   
                            <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dascacable" data-dismiss="modal">Dasca Cable</button>  
                        </div>

                   


            </div>
    </div>
</div>
</div>



<script>
let btn = document.querySelector("#showSearch");
let div = document.querySelector("#mySearch");

btn.addEventListener('click', () =>{ 
    if(div.style.display === 'none'){
        div.style.display = 'block';
    }else{
        div.style.display = 'none';
    }

});


    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIV *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
     });
</script>


