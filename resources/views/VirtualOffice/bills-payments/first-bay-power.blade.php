
<div id="modal_form_firstbaypower" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="d-flex justify-content-center">
                             <div id="dot" class="text-center text-white ">F</div>                          
                            </div><br>
                            <h5 class="modal-title text-center">Fist Bay Power</h5>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Number</label>
                                <input type="number" name="AccountNo" class="form-control"
                                    placeholder="Enter Account Number">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Bill ID</label>
                                <input type="text" class="form-control" name="Identifier" placeholder="Enter Bill ID" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Amount Paid</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PHP</span>
                                    </span>
                                    <input type="number" name="Amount" class="form-control"
                                        placeholder="Enter Amount in PHP(1-8 digits)" required>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Convenience Fee</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly name="ConvenienceFee" value="0.00">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="alert alert-primary border-0 alert-dismissible">
                                <span> Payments made beyond 3:00PM will be posted the next day.<br></span> 
                                <span> Accept On or Before Due Date Bill Payments</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn text-uppercase bg-primary">Pay Now</button>
                </div>
            </div>
        </div>
    </div>

</form>
