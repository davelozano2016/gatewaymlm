<div id="modal_collectionservice" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title">Collection Service</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="modal-dialog-scrollable">    
                <div id="icn">
                    <a class="list-icons-item" data-action="collapse" id="showSearchCol"></a> 
                </div>            
                <div id="mySearchCol" style="display:none;">
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text bg-dark border-dark text-white">
                            <i class="icon-search4"></i>
                            </span>
                        </span>
                        <input class="form-control" id="myInputCol" type="text" placeholder="Biller Name">
                    </div>
                </div>
                <div id="myDIVCol">
                    <br> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_iconnex" data-dismiss="modal">Iconnex</button> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_metrofastbet" data-dismiss="modal">Metro Fastbet</button>
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_eprime" data-dismiss="modal">Eprime</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dragongames" data-dismiss="modal">Dragon Games</button>     
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_payexpress" data-dismiss="modal">Payexpress</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dibble" data-dismiss="modal">Dibble</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_medicard" data-dismiss="modal">Medicard</button> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_watersph" data-dismiss="modal">Waters ph</button> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_airyougotravels" data-dismiss="modal">Airyou Travels</button>
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pisopay" data-dismiss="modal">Pisopay</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_loadxtreme" data-dismiss="modal">Loadxtreme</button>     
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_idolmo" data-dismiss="modal">Idol Mo</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pitakamo" data-dismiss="modal">Pitakamo</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_fami" data-dismiss="modal">Fami</button> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_ideal" data-dismiss="modal">Ideal</button>
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pay2c2pplus" data-dismiss="modal">Pay 2c2p Plus</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_resellee" data-dismiss="modal">Resellee</button>     
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sanmiguelfoods" data-dismiss="modal">San Miguel Foods</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sanmiguelmills" data-dismiss="modal">San Miguel Mills</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_purefoodshormel" data-dismiss="modal">Purefoods Hormel</button> 
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_foodcravemarketing" data-dismiss="modal">Food Crave Marketing</button>
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_magnolia" data-dismiss="modal">Magnolia</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_firstmetrosec" data-dismiss="modal">First Metrosec</button>     
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_personalcollection" data-dismiss="modal">Personal Collection</button>  
                    <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_forestlake" data-dismiss="modal">Forest Lake</button>  
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
let btnzss = document.querySelector("#showSearchCol");
let divzss = document.querySelector("#mySearchCol");

btnzss.addEventListener('click', () =>{ 
    if(divzss.style.display === 'none'){
        divzss.style.display = 'block';
    }else{
        divzss.style.display = 'none';
    }

});


    $(document).ready(function(){
        $("#myInputCol").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIVCol *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
     });
</script>
