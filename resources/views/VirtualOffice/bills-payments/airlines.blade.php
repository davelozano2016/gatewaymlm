
<div id="modal_airlines" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Airlines</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                    <div class="modal-body" id="modal-dialog-scrollable">

                       <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pal" data-dismiss="modal">PAL</button> 
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cebupacific" data-dismiss="modal">Cebu Pacific</button>
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_viacom" data-dismiss="modal">Viacom</button>  
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_payexpresstickets" data-dismiss="modal">Payexpress tickets</button>     
                        <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_palcontractcenter" data-dismiss="modal">Pal Contractcenter</button>  

                    </div>
            </div>
        </div>
    </div>
