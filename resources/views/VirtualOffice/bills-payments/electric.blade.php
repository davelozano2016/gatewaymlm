
<div id="modal_electricutilities" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-dark">
                    <h5 class="modal-title">Electric Utilities</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body" id="modal-dialog-scrollable">    
                        <!-- <i class="icon-search4 text-dark" id="showSearch"></i>    -->
                        <div id="icn">
                        <a class="list-icons-item" data-action="collapse" id="showSearchElectric"></a> 
                        </div>            
                        <div id="mySearchElectric" style="display:none;">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-dark border-dark text-white">
                                    <i class="icon-search4"></i>
                                    </span>
                                </span>
                                <input class="form-control" id="myInputElectric" type="text" placeholder="Biller Name">
                            </div>
                        </div>
                            <div id="myDIVElectric">
                                <br> 
                      
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_veco" data-dismiss="modal">Veco<i id="icn" class="icon-spinner4 spinner mr-2"></i></button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_davaolight" data-dismiss="modal">Davao Light</button>
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_subicenerzone" data-dismiss="modal">Subic Enerzone</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_beneco" data-dismiss="modal">Beneco</button>     
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_ileco1" data-dismiss="modal">Ileco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pelco1" data-dismiss="modal">Pelco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cotabatolight" data-dismiss="modal">Cotabato Light</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_presco" data-dismiss="modal">Presco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_batelec1" data-dismiss="modal">Batelec 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_batelec2" data-dismiss="modal">Batelec 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_angeleselectric" data-dismiss="modal">Angeles Electric</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tarelco2" data-dismiss="modal">Tarelco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_neeco2area1" data-dismiss="modal">Neeco 2 Area 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pelco2" data-dismiss="modal">Pelco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_canoreco" data-dismiss="modal">Canoreco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_inec" data-dismiss="modal">Inec</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_pelco3" data-dismiss="modal">Pelco 3</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_fleco" data-dismiss="modal">Fleco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_neeco2area2" data-dismiss="modal">Neeco 2 Area 2</button>  
                                <!-- <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_panelco" data-dismiss="modal">Panelco</button>   -->
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cebeco2" data-dismiss="modal">Cebeco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_soreco2" data-dismiss="modal">Soreco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cepalco" data-dismiss="modal">Cepalco </button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_surseco1" data-dismiss="modal">Surseco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_zameco2" data-dismiss="modal">Zameco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_oedc" data-dismiss="modal">Oedc</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_dasureco" data-dismiss="modal">Dasureco</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_zameco1" data-dismiss="modal">Zameco 1</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_iseco" data-dismiss="modal">Iseco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_limaenerzone" data-dismiss="modal">Lima Enerzone</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_panelco1" data-dismiss="modal">Panelco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sajelco" data-dismiss="modal">Sajelco</button>     
                                <!-- <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_nececo" data-dismiss="modal">Noceco</button>  -->
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_panelco3" data-dismiss="modal">Panelco 3</button>   
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_quezelco2" data-dismiss="modal">Quezelco 2</button>                            
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_casureco2" data-dismiss="modal">Casureco 2</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_iselco1" data-dismiss="modal">Iselco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_quezelco1" data-dismiss="modal">Quezelco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_sfelapco" data-dismiss="modal">Sfelapco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_nuvelco" data-dismiss="modal">Nuvelco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_moresco1" data-dismiss="modal">Moresco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cebeco3" data-dismiss="modal">Cebeco 3</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_peco" data-dismiss="modal">Peco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_decorp" data-dismiss="modal">Decorp</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_luelco" data-dismiss="modal">Luelco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cagelco1" data-dismiss="modal">Cagelco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_neeco1" data-dismiss="modal">Neeco 1</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cenpelco" data-dismiss="modal">Cenpelco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_iliganlight" data-dismiss="modal">Iligan Light</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_firstbaypower" data-dismiss="modal">First Bay Power</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_zamcelco" data-dismiss="modal">Zamcelco</button>  
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_cagelco2" data-dismiss="modal">Cagelco 2</button> 
                                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_tarelco1" data-dismiss="modal">Tarelco 1</button>                               
                            </div>
                </div>
            </div>
        </div>
    </div>


    <script>
let btnz = document.querySelector("#showSearchElectric");
let divz = document.querySelector("#mySearchElectric");

btnz.addEventListener('click', () =>{ 
    if(divz.style.display === 'none'){
        divz.style.display = 'block';
    }else{
        divz.style.display = 'none';
    }

});


    $(document).ready(function(){
        $("#myInputElectric").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIVElectric *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
     });
</script>
