<div id="modal_ecommerce" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title">Ecommerce</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="modal-dialog-scrollable">

                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_airasia" data-dismiss="modal">Airasia</button> 
                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_lazada" data-dismiss="modal">Lazada</button>
                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_shopee" data-dismiss="modal">Shopee</button>  
                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_agoda" data-dismiss="modal">Agoda</button>     
                <button type="button" id="btn-btn" class="btn btn-outline btn-block" data-toggle="modal" data-target="#modal_form_carousell" data-dismiss="modal">Carousell</button>  
            </div>
        </div>
    </div>
</div>


