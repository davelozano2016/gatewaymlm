
<div id="modal_form_batelec2" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="d-flex justify-content-center">
                                <a href="#"><img src="../assets/images/billerlogo/batelec2.png"
                                        width="89" height="89" class="rounded-circle" alt=""></a>
                            </div><br> 
                            <h5 class="modal-title text-center">Batelec 2</h5>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Number</label>
                                <input type="number" name="AccountNo" class="form-control"
                                    placeholder="Enter 7 digits (Account Number)">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Account Name</label>
                                <input type="text" class="form-control" name="Identifier" placeholder="Enter Account Name" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Amount Paid</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">PHP</span>
                                    </span>
                                    <input type="text" name="Amount" class="form-control"
                                        placeholder="Enter Amount in PHP(1-8 digits)" required>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Convenience Fee</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly name="ConvenienceFee" value="5.00">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="alert alert-primary border-0 alert-dismissible">
                                <span> PAYMENTS made beyond 12:00 AM will be posted the next day.<br></span>
                                <span> Total Current Bill Without Arrears must be paid On or Before the Payment Center Due Date</span>                       
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn text-uppercase bg-primary">Pay Now</button>
                </div>
            </div>
        </div>
    </div>

</form>
