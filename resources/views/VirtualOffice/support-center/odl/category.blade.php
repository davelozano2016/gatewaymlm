@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Manage {{$title}}</h6>
                    <div class="header-elements">
                        <button class="btn btn-info">Add New Category</button>
                    </div>
                </div>

                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                         <tr>
                                             <th style="width:1px">#</th>
                                             <th>Category</th>
                                             <th style="width:1px;">Tickets</th>
                                             <th>Graph</th>
                                             <th colspan=2  style="width:1px;text-align:center">Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                         @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>Category {{$i}}</td>
                                                <td class="text-center">{{$i}}</td>
                                                <td><div class="progress">
                                                   <div class="progress-bar bg-teal" style="width: {{$i}}%">
                                                       <span>{{$i}}% Complete</span>
                                                   </div>
                                               </div></td>
                                                <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                                <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                            </tr>
                                         @endfor
                                    </tbody>
                                 </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.sc-category').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
@endsection
@endsection
