@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Filter Ticket</h6>
                    {{-- <div class="header-elements">
                    </div> --}}
                </div>

                <div class="card-body py-0">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Ticket ID</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Created By</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Assignee</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Category</label>
                                    <select class="form-control payout-fee form-control-select2">
                                        @for($i=1;$i<=10;$i++)
                                            <option>Category {{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Tags</label>
                                    <select class="form-control payout-fee form-control-select2">
                                        @for($i=1;$i<=10;$i++)
                                            <option>Tags {{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Date Range</label>
                                    <select class="form-control payout-fee form-control-select2">
                                        <option value="Today">Today</option>
                                        <option value="This Month">This Month</option>
                                        <option value="This Year">This Year</option>
                                        <option value="Custom">Custom</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row" hidden>
                            <div class="col-md-12">
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Assigned To Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Assigned To Others
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Unassigned Tickets
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Only Tagged Tickets
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-info">Show</button>
                                    <button class="btn btn-info">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /traffic sources -->

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">On Going Tickets</h6>
                    {{-- <div class="header-elements">
                    </div> --}}
                </div>

                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Ticket ID</th>
                                            <th>Subject</th>
                                            <th>Created By</th>
                                            <th>Assignee</th>
                                            <th>Status</th>
                                            <th>Category</th>
                                            <th>Priority</th>
                                            <th>Created On</th>
                                            <th>Last Updated</th>
                                            <th style="width:1px">Timeline</th>
                                        </tr>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>IMS965715{{$i}}</td>
                                                <td>This is subject</td>
                                                <td>John Doe (1002010{{$i}})</td>
                                                <td>computology</td>
                                                <td>New</td>
                                                <td>Error</td>
                                                <td>Critical</td>
                                                <td>{{date('F j, Y',strtotime($date))}}</td>
                                                <td>{{date('F j, Y',strtotime($date))}}</td>
                                                <td style="text-align:center"><a class="" href="#"><i class="icon icon-screen-full"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.sc-view-tickets').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection