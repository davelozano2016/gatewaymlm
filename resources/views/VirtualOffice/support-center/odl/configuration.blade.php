@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <!-- Traffic sources -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">{{$title}} Settings</h6>
                </div>

                <div class="card-body ">
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0  ">
                        <li class="nav-item"><a href="#status" class="nav-link active" data-toggle="tab">Status</a></li>
                        <li class="nav-item"><a href="#tags" class="nav-link" data-toggle="tab">Tags</a></li>
                        <li class="nav-item"><a href="#priority" class="nav-link" data-toggle="tab">Priority</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="status">
                            <div class="form-group">
                                <button class="btn btn-info">Add New Status</button>
                             </div>
                            <div class="table-responsive">
                                <table class="table table-columned">
                                   <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Status</th>
                                            <th colspan=2 style="width:1px;text-align:center">Action</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>New</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>

                                        <tr>
                                            <td>2</td>
                                            <td>In Progress</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>

                                        <tr>
                                            <td>3</td>
                                            <td>Resolved</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>
                                        
                                        <tr>
                                            <td>4</td>
                                            <td>On Hold</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>

                                        <tr>
                                            <td>5</td>
                                            <td>Re-Open</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>
                                   </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade show" id="tags">
                            <div class="form-group">
                                <button class="btn btn-info">Add New Tags</button>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                         <tr>
                                             <th style="width:1px">#</th>
                                             <th>Tags</th>
                                             <th colspan=2 style="width:1px;text-align:center">Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                         @for($i=1;$i<=10;$i++)
                                         <tr>
                                            <td>{{$i}}</td>
                                            <td>Sample Tag {{$i}}</td>
                                            <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                 </table>
                            </div>
                        </div>

                        <div class="tab-pane fade show" id="priority">
                            <div class="form-group">
                                <button class="btn btn-info">Add New Priority</button>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                         <tr>
                                             <th style="width:1px">#</th>
                                             <th>Priority</th>
                                             <th colspan=2  style="width:1px;text-align:center">Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                         <tr>
                                             <td>1</td>
                                             <td>High</td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                         </tr>
 
                                         <tr>
                                             <td>2</td>
                                             <td>Low</td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                         </tr>
 
                                         <tr>
                                             <td>3</td>
                                             <td>Critical</td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-check"></i></a></td>
                                             <td style="width:1px"><a class="" href=""><i class="icon icon-eye"></i></td>
                                         </tr>
                                    </tbody>
                                 </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.sc-configuration').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
@endsection
@endsection
