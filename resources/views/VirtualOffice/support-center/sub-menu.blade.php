<div class="form-group">
    <a href="{{ url('virtualoffice/support-center/compose') }}" class="btn bg-indigo-400 btn-block">Compose</a>
</div>
 <!-- Sub navigation -->
 <div class="card">
    <div class="card-header bg-transparent header-elements-inline">
        <span class="text-uppercase font-size-sm font-weight-semibold">Navigation</span>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>

    <div class="card-body p-0">
        <ul class="nav nav-sidebar mb-2" data-nav-type="accordion">
            <li class="nav-item-header">Folders</li>
            <li class="nav-item">
                <a href="{{ url('virtualoffice/support-center/tickets') }}" class="nav-link m-inbox">
                    <i class="icon-drawer-in"></i>
                    Tickets
                    <span class="badge bg-success badge-pill ml-auto">32</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a href="{{ url('virtualoffice/support-center/sent-tickets') }}" class="nav-link m-sent-items"><i class="icon-drawer-out"></i> Sent Tickets</a>
            </li>
        </ul>
    </div>
</div>
<!-- /sub navigation -->