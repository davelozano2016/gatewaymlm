@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h2>{{$title}}</h2>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">Booking</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">


        <div class="card" id="myElementId">
            <div class="card-header bg-transparent header-elements-inline py-0">
                <h6 class="card-title ">Itinerary Receipt</h6>
                <div class="header-elements">
                    <img src="{{asset('assets/images/logo.png')}}" class="mb-2 mt-2" alt="" style="width: 120px;">
                    <button type="button" onclick="print()" class="btn no-print btn-light btn-sm ml-3"><i class="icon-printer mr-2"></i> Print</button>
                </div>
            </div>

            <div class="card-body">
                <div class="row mb-lg-2">

                    <table style="width:100%" class="table table-columned">
                        <tr>
                            <th>
                                Booking Details
                            </th>

                            <td>Status: Confirmed <br> Booking Date: {{date('F j, Y g:i A',strtotime($data->Itinerary->CreatedOn))}}</td>
                        </tr>
                        <tr>

                            <th>
                                Booking Reference Number
                            </th>
                            <td><h3>{{$data->Itinerary->PNR}}</h3></td>
                        </tr>

                        <tr>

                            <th>
                                Guest Details
                            </th>
                            <td>
                                @foreach($data->Itinerary->Passenger as $key => $value)
                                <span style="display:block">{{$value->FirstName}} {{$value->LastName}}</span>
                                @endforeach
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-columned datatable-responsive">
                    <thead>
                        <tr>
                            <th>Route</th>
                            <th>Airline</th>
                            <th>Flight #</th>
                            <th>Departure</th>
                            <th>Arrival</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data->Itinerary->Segments as $key => $value)
                        <tr>
                            <td>{{$value->Origin->CityName}} - {{$value->Destination->CityName}}</td>
                            <td>{{$value->AirlineName}}</td>
                            <td>{{$value->Airline}}{{$value->FlightNumber}}</td>
                            <td>
                                {{date('F j, Y g:i A',strtotime($value->DepartureTime))}}
                                <br>
                                {{$value->Origin->AirportName}} 
                                @if($value->Origin->Terminal == '')
                                @else
                                Terminal {{$value->Origin->Terminal}}
                                @endif
                            </td>

                            <td>
                                {{date('F j, Y g:i A',strtotime($value->ArrivalTime))}}
                                <br>
                                {{$value->Destination->AirportName}} 
                                @if($value->Destination->Terminal == '')
                                @else
                                Terminal {{$value->Destination->Terminal}}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <!-- /content area -->
    @section('custom')
    <script>
    $('a.b-b-history').addClass('active');
    $(document).ready(function() {
       
    });
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script>
          // Setting datatable defaults
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span></span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration
        $('.datatable-responsive').DataTable([
            "searching": false
        ]);

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js"></script>
    <script>
        function print() {
            $("#myElementId").print({
                addGlobalStyles : true,
                stylesheet : null,
                rejectWindow : true,
                noPrintSelector : ".no-print",
                iframe : true,
                append : null,
                prepend : null
            });
        }
    </script>
    
    @endsection
    @endsection