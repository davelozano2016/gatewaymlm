@extends('layouts.VirtualOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h2>{{$title}}</h2>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item">My Account</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid  mt-2">
                    <li class="nav-item">
                        <a href="#flight" class=" nav-link active" data-toggle="tab">Flights</a>
                    </li>
                    <li class="nav-item">
                        <a href="#hotel" class=" nav-link " data-toggle="tab"> Hotel</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tours" class=" nav-link " data-toggle="tab"> Tours</a>
                    </li>

                    <li class="nav-item">
                        <a href="#cars" class=" nav-link" data-toggle="tab">Cars</a>
                    </li>

                    <li class="nav-item">
                        <a href="#cruises" class=" nav-link  nav-last" data-toggle="tab">Cruises</a>
                    </li>


                </ul>
                <div class="col-md-12 content-tab">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="tab-content w-100 order-2 order-md-1">
                            <!-- Statement -->
                            <div class="tab-pane  active show" id="flight">

                                <form method="POST" action="{{url('virtualoffice/booking/markup/create')}}">
                                    @csrf
                                    @foreach($markups as $markup)
                                        @if($markup['booking_type'] == 'Flight')
                                        <input type="hidden" name="id[]" value="{{Crypt::encryptString($markup->id)}}">
                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label">Category:</label>
                                            <div class="col-lg-10">
                                                <input type="text" name="category[]" value="{{$markup->category}}" readonly
                                                    class="form-control" required>
                                            </div>  
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label">Markup Type:</label>
                                            <div class="col-lg-10">
                                                <select name="type[]" class="form-control" id="">
                                                    <option value="Fixed" {{$markup->type == 'Fixed' ? 'selected' : ''}}>Fixed (USD)</option>
                                                    <!--<option value="Percentage" {{$markup->type == 'Percentage' ? 'selected' : ''}}>Percentage (%)</option>-->
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label">Amount:</label>
                                            <div class="col-lg-10">
                                                <input type="text" name="amount[]" value="{{$markup->amount}}" class="form-control" required>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach

                                    <button class="btn btn-primary btn-sm float-right">Save Changes</button>

                                </form>
                            </div>

                            <!-- Purchase Wallet -->
                            <div class="tab-pane" id="hotel">
                                <div class="col-md-12">

                                </div>
                            </div>

                            <div class="tab-pane" id="tours">
                                <div class="col-md-12">

                                </div>
                            </div>

                            <div class="tab-pane" id="cars">
                                <div class="col-md-12">

                                </div>
                            </div>

                            <div class="tab-pane" id="cruises">
                                <div class="col-md-12">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /content area -->
    @section('custom')
    <script>
        $('a.b-markup').addClass('active');

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>

    @endsection
    @endsection