@extends('layouts.VirtualOffice.app')
@section('container')


<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Top Up</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Promo</th>
                                            <th>Denomination</th>
                                            <th>Network</th>
                                            <th>Code</th>
                                            <!-- <th>Description</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i= 1; ?>
                                        @foreach($topup as $top)
                                        <tr>
                                            <td>{{$i++}}.</td>
                                            <td>{{$top->TelcoTag}}</td>
                                            <td>{{$top->Denomination}}</td>
                                            <td>{{$top->TelcoName}}</td>
                                            <td>{{$top->ExtTag}}</td>
                                            <!-- <td>{{($top->description == '') ? 'Not Available' : $top->description}}</td> -->
                                            <td><button type="button" class="btn btn-sm btn-primary"
                                                    onclick="topup('{{$top->TelcoName}}', '{{$top->ExtTag}}', '{{$top->Denomination}}','{{$top->TelcoTag}}')">TOPUP
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post">
        <div class="modal" id="top-up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Top Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Promo</label>
                            <div class="col-lg-8">
                                <input type="text" readonly class="form-control" id="TelcoTag">
                                <input type="hidden" readonly id="TelcoName" class="form-control">

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Code:</label>
                            <div class="col-lg-8">
                                <input type="text" readonly class="form-control" id="ExtTag">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Denomination:</label>
                            <div class="col-lg-8">
                                <input type="text" readonly class="form-control" id="Denomination">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Mobile Number:</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="mobile_number">
                            </div>
                        </div>
                        
                            
                        <div class="alert alert-primary border-0 text-center">

                            <strong> Please note: 3 pesos service fee will apply in every transaction</strong>
                          
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="topup" class="btn btn-primary btn-sm">Process</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @section('custom')
    <script>
    $('a.smart').addClass('active');
    $('li.top-up-must-open').addClass('nav-item-expanded nav-item-open');

    function topup(TelcoName, ExtTag, Denomination, TelcoTag) {
        $('#top-up').modal()
        $('#TelcoName').val(TelcoName);
        $('#ExtTag').val(ExtTag);
        $('#Denomination').val(Denomination);
        $('#TelcoTag').val(TelcoTag);
    }
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/notifications/sweet_alert.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/extra_sweetalert.js') }}"></script>

    <script>
    $('#topup').click(e => {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "{{url('virtualoffice/top-up/store')}}",
            data: {
                _token: "{{ csrf_token() }}",
                TelcoName: $('#TelcoName').val(),
                ExtTag: $('#ExtTag').val(),
                Denomination: $('#Denomination').val(),
                mobile_number: $('#mobile_number').val()
            },
            dataType: 'json',
            beforeSend: function() {
                $('#topup').html('PLEASE WAIT...').attr('disabled', true);
            },
            success: function(data) {
                swal.fire({
                    title: data.success == true ? 'Congratulations!' : 'Oops!',
                    text: data.message,
                    type: data.success == true ? 'success' : 'error'
                });
                $('#topup').html('TOP UP').attr('disabled', false);
            },
            completed: function() {
                $('#topup').html('TOP UP').attr('disabled', false);
            },
            error: function() {
                // swal({
                //     title: 'Oops!',
                //     text: 'There\'s an error. Please contact administrator.',
                //     type: 'error'
                // });
                // $('#topup').html('TOP UP').attr('disabled', false);
                alert('error');

            }
        })
    })
    </script>
    @endsection
    @endsection