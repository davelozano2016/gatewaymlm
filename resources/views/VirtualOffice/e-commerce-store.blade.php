@extends('layouts.VirtualOffice.app')
@section('container')


<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

          
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>



                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="d-flex align-items-start flex-md-row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid  mt-2">
                    <li class="nav-item">
                        <a href="#statement" class=" nav-link active" data-toggle="tab">Purchase Package Pins</a>
                    </li>
                </ul>
                <div class="col-md-12 ecommerce-product-list">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="tab-content w-100 order-2 order-md-1">
                            <!-- Statement -->
                            <div class="tab-pane  active show" id="statement">
                                    @foreach($packages as $package)
                                            <div class="row">
                                                <div class="card card-body">
                                                    <div
                                                        class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                                        <div class="mr-lg-3 mb-3 mb-lg-0">
                                                            <img src="{{url('assets/images/mini_logo.png')}}" width="50" alt="">
                                                        </div>
            
                                                        <div class="media-body">
                                                            <h6 class="media-title text-bold text-blue">
                                                                <span>{{$package->entry}}</span>
                                                            </h6>
                                                            <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                                                <li>{{$package->package_name}} Inclusions: </li>
                                                                @if(isset($package_inclusions[$package->package_unique_code]))
                                                                    <li>No Inclusions Available</li>
                                                                @else
                                                                    @foreach($package_inclusions[$package->package_unique_code] as $unique_code)
                                                                    <li><a href="#"
                                                                            class="text-muted"><b>{{$unique_code->quantity}} pc/s</b> {{$unique_code->product_title}} </a></li>
                                                                    @endforeach
                                                                @endif
                                                                
                                                                <li>{{$package->description}}</li>
                                                            </ul>
                                                        </div>
            
                                                        <div class="mt-lg-0 ml-lg-3 text-center">
                                                            <h3 class="mb-0 font-weight-semibold">
                                                                {{$package->currency_symbol}}{{number_format($package->price,2)}}
                                                            </h3>
                                                            <h3 class="mb-0 font-weight-semibold">
                                                                <a href="{{url('virtualoffice/e-commerce-store/package/add-to-cart')}}/{{Crypt::encryptString($package->packages_id)}}"
                                                                    class="btn mt-2 bg-blue"><i class="icon-cart-add mr-2"></i>
                                                                    Add to cart</a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                            </div>

                            <!-- Purchase Wallet -->
                            <div class="tab-pane" id="purchase-wallet">
                                <div class="row">
                                    <div class="w-100 overflow-auto order-2 order-md-1"
                                        style="overflow-x: hidden !important;">
                                        <div class="row">
                                            @foreach($entries as $entry)
                                            <div class="col-md-12 col-12">
                                                <div class="card card-body">
                                                    <div
                                                        class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                                        <div class="mr-lg-3 mb-3 mb-lg-0">
                                                            <?php 
                                                                if($entry->product_image == 'default.png') {
                                                                    $product_image = 'default.png';
                                                                } else {
                                                                    $product_image = $entry->product_image;
                                                                }
                                                                ?>
                                                            <a href="{{url('assets/products/'.$product_image)}}"
                                                                data-popup="lightbox">
                                                                <img src="{{url('assets/products/'.$product_image)}}"
                                                                    width="96" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="media-body">
                                                            <h6 class="media-title font-weight-semibold">
                                                                <a href="#">{{$entry->product_title}}</a>
                                                            </h6>

                                                            <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                                                <li class="list-inline-item"><a href="#"
                                                                        class="text-muted">{{$entry->category}}</a></li>
                                                            </ul>

                                                            <p class="mb-3 text-justify">{{$entry->product_description}}
                                                            </p>

                                                            <ul class="list-inline list-inline-dotted mb-0">
                                                                <li class="list-inline-item">Supplier <a
                                                                        href="#">{{$entry->supplier_name}} </a></li>
                                                                <!-- <li class="list-inline-item">Add to <a href="#">wishlist</a></li> -->
                                                            </ul>
                                                        </div>

                                                        <div class="mt-3 mt-lg-0 ml-lg-3 text-center">
                                                            <h3 class="mb-0 font-weight-semibold">
                                                                {{$entry->currency_symbol}}{{number_format($entry->distributor_price,2)}}
                                                            </h3>

                                                            <div>
                                                                <i
                                                                    class="icon-star-full2 font-size-base text-warning-300"></i>
                                                                <i
                                                                    class="icon-star-full2 font-size-base text-warning-300"></i>
                                                                <i
                                                                    class="icon-star-full2 font-size-base text-warning-300"></i>
                                                                <i
                                                                    class="icon-star-full2 font-size-base text-warning-300"></i>
                                                                <i
                                                                    class="icon-star-full2 font-size-base text-warning-300"></i>
                                                            </div>

                                                            <div class="text-muted">85 reviews</div>

                                                            <a href="{{url('virtualoffice/e-commerce-store/add_to_cart/'.Crypt::encryptString($entry->product_packages_id).'/'.Crypt::encryptString($entry->country))}}"
                                                                class="btn mt-2 bg-blue"><i
                                                                    class="icon-cart-add mr-2"></i> Add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->

    @section('custom')
    <script>
        $('a.e-commerce-store').addClass('active');

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/ecommerce_product_list.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <!-- /theme JS files -->

    @endsection
    @endsection
