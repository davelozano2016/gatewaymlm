@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Transaction History</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Direct Referral Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Sales Match Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Royalty Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Unilevel Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Online Selling Override</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Product Bundle Override</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-wallet icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Product Sales Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Unilevel Product Bundle - Transaction History</span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                   <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Reference ID</th>
                                            <th>Date & Time</th>
                                            <th>Transaction Type</th>
                                            <th>Wallet Transaction</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Description</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{$i}}.</td>
                                            <td>Order ID</td>
                                            <td>{{date('F d, Y - h:m:s')}}</td>
                                            <td>Transaction {{$i}}</td>
                                            <td>Wallet {{$i}}</td>
                                            <td>Debit {{$i}}</td>
                                            <td>Credit {{$i}}</td>
                                            <td>Description {{$i}}</td>
                                        </tr>
                                        @endfor
                                   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.th-unilevel-product-bundle').addClass('active');
    $('li.transaction-history-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection