@extends('layouts.VirtualOffice.app')
@section('container')
<style>
input[type=radio]:checked+label>img {
    background-image: linear-gradient(16deg, #3f51b5, #3d696a);
}

.input-hidden {
    position: absolute;
    left: -9999px;
}

.card-img-actions label#product-error {
    position: absolute;
}
</style>
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>Distributor Sub Account</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">{{$title}}</span>
                    <span class="breadcrumb-item active">Sub Account</span>

                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->
    <div class="content">
        <form method="POST" action="{{url('virtualoffice/register/store_sub_account')}}">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title">Distributor Sub Account Network Details</div>
                            <input type="hidden" name="sub_account_users_id" value="{{$users[0]->id}}">
                            <input type="hidden" name="sub_account_group_code" value="{{$users[0]->group_code}}">

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Activation Code:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="sub_account_activation_code" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Sponsor ID:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="sub_account_sponsor_id" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Placement ID:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="sub_account_placement_id"
                                        value="{{Crypt::decryptString($id_number)}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Position:</label>
                                <div class="col-lg-10 ">
                                    <select name="sub_account_position" class="form-control-select2 form-control">
                                        <option value="0" @if(Crypt::decryptString($position)==0) selected @else @endif>
                                            Left</option>
                                        <option value="1" @if(Crypt::decryptString($position)==1) selected @else @endif>
                                            Right</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Username:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="sub_account_username"
                                        value="{{$users[0]->username}}{{$increment}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Password</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <input type="password" name="sub_account_password"
                                            value="{{Crypt::decryptString($users[0]->password)}}"
                                            id="sub_account_password" class="form-control">
                                        <span class="input-group-append">
                                            <span class="input-group-text"><a style="cursor:pointer"><i
                                                        onclick="unmasksub()" id="unmasksub"
                                                        class="icon-eye"></i></a></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Create Distributor</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title">Distributor's Available Codes</div>
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Date</th>
                                            <th>Activation Code</th>
                                            <th>Package</th>
                                            <th>Order ID</th>
                                            <th class="text-center">DR</th>
                                            <th class="text-center">BPV</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1?>
                                        @foreach($registration_codes as $codes)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{date('Y-m-d',strtotime($codes->created_at))}}</td>
                                            <td>{{$codes->activation_code}}</td>
                                            <td class="text-uppercase">{{$codes->entry}}</td>
                                            <td class="text-uppercase">{{$codes->customer_id}}</td>
                                            <td class="text-center">{{$codes->currency_symbol}}{{number_format($codes->direct_referal,2)}}</td>
                                            <td class="text-center">{{number_format($codes->bpv,2)}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>

    @section('custom')
    <script>
    $('a.register').addClass('active');
    </script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script>
    function unmask() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmask').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmask').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function unmasksub() {
        var x = document.getElementById("sub_account_password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmasksub').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmasksub').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function clipboard() {
        /* Get the text field */
        var copyText = document.getElementById("clipboard");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }
    </script>
    @endsection
    @endsection