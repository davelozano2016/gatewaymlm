@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center" id="c-count-header">
                                <i class="icon-wallet icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                                <span class="text-uppercase font-size-sm text-muted">E-wallet Balance</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center" id="c-count-header">
                                <i class="icon-wallet icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                                <span class="text-uppercase font-size-sm text-muted">Commission Earned</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center" id="c-count-header">
                                <i class="icon-wallet icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                                <span class="text-uppercase font-size-sm text-muted">Payout Released</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center" id="c-count-header">
                                <i class="icon-wallet icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                                <span class="text-uppercase font-size-sm text-muted">Payout Pending</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 text-center">
                            <a href=""><img class="mt-2" style="width:75px" src="{{asset('assets/images/mikewooting.jpg')}}" alt=""></a>
                            <p><b>John Wick</b> <br> <b class="badge badge-info">10002021</b> <br> Rank: <b>GOLD</b></p>
                        </div>
                        <div class="col-8">
                            <div class="row ">
                                <div class="col-7 col-md-6">Membership Package: <br><b>Executive</b></div>
                                <div class="col-5 col-md-6">Replica Link: <br>  
                                    <b> 
                                        <a href="#"><i class="icon icon-copy3"></i></a>  
                                        <a href="#"><i class="icon icon-facebook"></i></a> 
                                        <a href="#"><i class="icon icon-twitter"></i></a>  
                                        <a href="#"><i class="icon icon-linkedin"></i></a>
                                    </b>
                                </div>
                            
                            </div>

                            <div class="row ">
                                <div class="col-12 mt-2">
                                    <h6 class="mb-0 font-weight-semibold">0.00 PV (₱ 0.00)</h6>
                                    <p class="text-muted">My Current Orders</p>
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 39%">
                                            <span>39%</span>
                                        </div>
                                    </div>
                                    <p class="text-muted">{{date('M Y')}} Current Month Target PV Progress Bar Monitoring</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Joinings</h5>
                    <ul class="nav ">
                        <li class="nav-item dropdown">
                            <a href="#" class=" " data-toggle="dropdown"><div class="icon-filter3"></div> </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item" data-toggle="tab">Year</a>
                                <a href="#" class="dropdown-item" data-toggle="tab">Month</a>
                                <a href="#" class="dropdown-item" data-toggle="tab">Day</a>
                            </div>
                        </li>
                    </ul>
                </div>
               

                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart" id="google-line"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12">
            <!-- Assigned users -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Sponsor Name</label>
                            <p><strong>John Doe</strong></p>
                        </div>
                        <div class="col-md-6 col-6">
                            <label for="">Personal PV</label>
                            <p><strong>790</strong></p>
                        </div>
                        <div class="col-md-6 col-6">
                            <label for="">Group PV</label>
                            <p><strong>7200</strong></p>
                        </div>

                        <div class="col-md-6 col-6">
                            <label for="">Left Carry</label>
                            <p><strong>2550</strong></p>
                        </div>
                        <div class="col-md-6 col-6">
                            <label for="">Right Carry</label>
                            <p><strong>0</strong></p>
                        </div>

                        <div class="col-md-6 col-6">
                            <label for="">Downline Left</label>
                            <p><strong>100</strong></p>
                        </div>
                        <div class="col-md-6 col-6">
                            <label for="">Downline Right</label>
                            <p><strong>0</strong></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row" style="margin-top:25px !important">
                        <div class="col-md-6 col-6">
                            <p>Current Rank: <strong>Silver</strong></p>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%">
                                    <span>100%</span>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn bg-danger btn-block btn-sm" id="c-button-ar">4 Required</button></div>
                                </div>

                                <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn btn-success btn-block btn-sm" id="c-button-ar">5 Achieved</button></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <p>Next Rank: <strong>Gold</strong></p>
                            <div class="progress">
                                <div class="progress-bar" style="width: 83.3%">
                                    <span> 83.3%</span>
                                </div>
                            </div>
                            <div class="row mt-2" >
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn bg-danger btn-block btn-sm" id="c-button-ar">6 Required</button></div>
                                </div>

                                <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div><button class="btn btn-success btn-block btn-sm" id="c-button-ar">5 Achieved</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <!-- Assigned users -->
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="card-title font-weight-semibold">My Accounts</span>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">
                    <div class="table-responsive">
                        <table class="table table-columned">
                           <thead>
                               <tr>
                                    <th style="width:1px">#</th>
                                    <th style="width:1px">ID</th>
                                    <th >Type</th>
                                    <th >Balance</th>
                                    <th style="width:1px">Select</th>
                                </tr>
                           </thead>
                           <tbody>
                               @for($i=1;$i<=10;$i++)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{'12345'.date('Y').$i}}</td>
                                    <td>SUPREME</td>
                                    <td>₱{{number_format(12345,2)}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-warning btn-sm">Convert</button>
                                            <button class="btn btn-info btn-sm">Login</button>
                                        </div>
                                    </td>
                                </tr>  
                               @endfor
                           </tbody>
                        </table>
                    </div>
                </div>
                    {{-- <ul class="media-list" style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">

                        @for($i=1;$i<=10;$i++)
                        
                        @endfor
                    </ul> --}}
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12">
            <!-- Assigned users -->
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="card-title font-weight-semibold">New Members</span>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <ul class="media-list" style=" overflow-y: scroll;
                    overflow-x: hidden;
                    max-height: 320px;">
                        @for($i=1;$i<=10;$i++)
                        <li class="media">
                            <a href="#" class="mr-3">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}" width="36" height="36" class="rounded-circle" alt="">
                            </a>
                            <div class="media-body">
                                <a href="#" class="media-title font-weight-semibold">James Alexander {{$i}}</a>
                                <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                            </div>
                            <div class="align-self-center mr-4">
                                <a href="#" class="media-title font-weight-semibold">₱123,456.00</a>
                                <div class="font-size-sm text-muted ">{{date('M j, Y',strtotime(date('Y-m-d')))}}</div>
                            </div>
                        </li>
                        @endfor
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Income & Comission</h6>
                </div>

                <div class="card-body ">
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                        <li class="nav-item"><a href="#earnings" class="nav-link active" data-toggle="tab">Earnings</a></li>
                        <li class="nav-item"><a href="#expenses" class="nav-link" data-toggle="tab">Expenses</a></li>
                        <li class="nav-item"><a href="#payout" class="nav-link" data-toggle="tab">Payout Status</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="earnings" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <tr>
                                        <th>Fund Credit</th> <td>₱123,456.00</td> <td><span class="badge bg-blue-400">FC</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Rank Commission</th> <td>₱123.00</td> <td><span class="badge bg-blue-400">RC</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Referral Commission</th> <td>₱0</td> <td><span class="badge bg-blue-400">RC</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>House Fund</th> <td>₱0</td> <td><span class="badge bg-blue-400">HF</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="expenses" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                            <div class="table-responsive table-columned">
                                <table class="table">
                                    <tr>
                                        <th>Payout Released Manually</th> <td>₱123,456.00</td> <td><span class="badge bg-blue-400">PRM</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Payout Requested</th> <td>₱123.00</td> <td><span class="badge bg-blue-400">PR</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Payout Release</th> <td>₱0</td> <td><span class="badge bg-blue-400">PR</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Pin Purchase</th> <td>₱0</td> <td><span class="badge bg-blue-400">PP</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="payout" style="overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                            <div class="table-responsive table-columned">
                                <table class="table">
                                    <tr>
                                        <th>Requested</th> <td><span class="text-blue">₱0.00</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Approved</th> <td><span class="text-blue">₱0.00</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Paid</th><td><span class="text-green">₱{{number_format('237417.12',2)}}</span></td>
                                    </tr>
    
                                    <tr>
                                        <th>Rejected</th>  <td><span class="text-red">₱0.00</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Team Performance</h6>
                </div>

                <div class="card-body ">
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                        <li class="nav-item"><a href="#top-earners" class="nav-link active" data-toggle="tab">Top Earners</a></li>
                        <li class="nav-item"><a href="#top-recruiters" class="nav-link" data-toggle="tab">Top Recruiters</a></li>
                        <li class="nav-item"><a href="#package-overview" class="nav-link" data-toggle="tab">Package Overview</a></li>
                        <li class="nav-item"><a href="#rank-overview" class="nav-link" data-toggle="tab">Rank Overview</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="top-earners">
                            <ul class="media-list" style=" overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                @for($i=1;$i<=10;$i++)
                                <li class="media">
                                    <a href="#" class="mr-3">
                                        <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}" width="36" height="36" class="rounded-circle" alt="">
                                    </a>
                                    <div class="media-body">
                                        <a href="#" class="media-title font-weight-semibold">James Alexander {{$i}}</a>
                                        <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                    </div>
                                    
                                    <div class="align-self-center mr-4">
                                        <a href="#" class="media-title font-weight-semibold">₱123,456.00</a>
                                    </div>
                                </li>
                                @endfor
                            </ul>
                        </div>

                        <div class="tab-pane fade" id="top-recruiters">
                            <ul class="media-list" style=" overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                @for($i=1;$i<=10;$i++)
                                <li class="media">
                                    <a href="#" class="mr-3">
                                        <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}" width="36" height="36" class="rounded-circle" alt="">
                                    </a>
                                    <div class="media-body">
                                        <a href="#" class="media-title font-weight-semibold">James Alexander {{$i}}</a>
                                        <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                    </div>
                                    <div class="align-self-center mr-4">
                                        <a href="#" class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                    </div>
                                </li>
                                @endfor
                            </ul>
                        </div>

                        <div class="tab-pane fade" id="package-overview">
                            <ul class="media-list" style=" overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                @for($i=1;$i<=10;$i++)
                                <li class="media">
                                    <div class="media-body">
                                        <a href="#" class="media-title font-weight-semibold">Membership {{$i}}</a>
                                        <div class="font-size-sm text-muted">You have {{$i}} Membership package purchases in your team </div>
                                    </div>
                                    <div class="align-self-center mr-4">
                                        <a href="#" class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                    </div>
                                </li>
                                @endfor
                            </ul>
                        </div>

                        <div class="tab-pane fade" id="rank-overview">
                            <ul class="media-list" style=" overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                @for($i=1;$i<=10;$i++)
                                <li class="media">
                                    <div class="media-body">
                                        <a href="#" class="media-title font-weight-semibold">Bronze</a>
                                        <div class="font-size-sm text-muted">You have 15 Bronze rank in your team</div>
                                    </div>
                                    <div class="align-self-center mr-4">
                                        <a href="#" class="media-title font-weight-semibold badge bg-blue-400">15</a>
                                    </div>
                                </li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.dashboard').addClass('active');
</script>
<script src="https://www.gstatic.com/charts/loader.js"></script>
<script src="{{asset('assets/js/demo_charts/google/light/lines/lines.js')}}"></script>
<script src="{{asset('assets/js/demo_charts/google/light/pies/donut.js')}}"></script>
<script src="{{asset('assets/js/demo_charts/google/light/bars/column.js')}}"></script>
@endsection
@endsection