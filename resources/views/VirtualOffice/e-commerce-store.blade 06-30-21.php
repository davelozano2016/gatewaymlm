@extends('layouts.VirtualOffice.app')
@section('container')


<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="input-group">
                <input type="text" placeholder="Search Product" class="form-control border-right-0">
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 order-2 order-md-1">
            <div class="card">
                <div class="card-body ">
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 ">
                        <li class="nav-item"><a href="#c1" class="nav-link active" data-toggle="tab">Category 1</a></li>
                        <li class="nav-item"><a href="#c2" class="nav-link" data-toggle="tab">Category 2</a></li>
                        <li class="nav-item"><a href="#c3" class="nav-link" data-toggle="tab">Category 3</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="c1">
                            <div class="row">
                                @for($i=1;$i<=10;$i++)
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="card-img-actions">
                                        <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    </div>
                
                                    <div class="card-body bg-light text-center">
                                        <div class="mb-2">
                                            <h6 class="font-weight-semibold mb-0">
                                                <a data-toggle="modal" data-target="#exampleModal" class="text-default">COCONUT SCRUB SOAP</a>
                                            </h6>
            
                                            <a data-toggle="modal" data-target="#exampleModal" class="text-muted">COMPSKIN</a>
                                        </div> 
            
                                        <h3 class="mb-0 font-weight-semibold">$49.99</h3>
            
                                        <a href="{{url('virtualoffice/store/cart')}}" class="btn bg-teal-400 btn-sm mt-2"><i class="icon-cart-add mr-2"></i> Add to cart</a>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>

                        <div class="tab-pane fade show" id="c2">
                            <div class="row">
                                @for($i=1;$i<=10;$i++)
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="card-img-actions">
                                        <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    </div>
                
                                    <div class="card-body bg-light text-center">
                                        <div class="mb-2">
                                            <h6 class="font-weight-semibold mb-0">
                                                <a data-toggle="modal" data-target="#exampleModal" class="text-default">COCONUT SCRUB SOAP</a>
                                            </h6>
            
                                            <a data-toggle="modal" data-target="#exampleModal" class="text-muted">COMPSKIN</a>
                                        </div> 
            
                                        <h3 class="mb-0 font-weight-semibold">$49.99</h3>
            
                                        <a href="{{url('virtualoffice/store/cart')}}" class="btn bg-teal-400 btn-sm mt-2"><i class="icon-cart-add mr-2"></i> Add to cart</a>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>

                        <div class="tab-pane fade show" id="c3">
                            <div class="row">
                                @for($i=1;$i<=10;$i++)
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="card-img-actions">
                                        <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    </div>
                
                                    <div class="card-body bg-light text-center">
                                        <div class="mb-2">
                                            <h6 class="font-weight-semibold mb-0">
                                                <a data-toggle="modal" data-target="#exampleModal" class="text-default">COCONUT SCRUB SOAP</a>
                                            </h6>
            
                                            <a data-toggle="modal" data-target="#exampleModal" class="text-muted">COMPSKIN</a>
                                        </div> 
            
                                        <h3 class="mb-0 font-weight-semibold">$49.99</h3>
            
                                        <a href="{{url('virtualoffice/store/cart')}}" class="btn bg-teal-400 btn-sm mt-2"><i class="icon-cart-add mr-2"></i> Add to cart</a>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!-- Grid -->
            
        </div>
        <!-- /left content -->
    </div>
    <!-- /inner container -->

</div>
<!-- /content area -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">COCONUT SCRUP SOAP</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 justify-content-center">
                        <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                    </div>
                    <div class="col-md-9">
                        <p>Availability: <strong>in Stocks</strong></p>
                        <p>SKU: <strong>COMPSOAPCSS</strong></p>
                        <h5><strong class="text-success">₱187.50</strong></h5>
                        
                        <div class="form-group mt-2">
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>

                        <div class="form-group">
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <div class="input-group">
                    <input type="text" class="form-control border-right-0" placeholder="Quantity">
                    <span class="input-group-append">
                        <button class="btn btn-info btn-sm btn-block" type="button">Add To Cart</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.e-commerce-store').addClass('active');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_input_groups.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/ecommerce_product_list.js')}}"></script>

<!-- /theme JS files -->

@endsection
@endsection