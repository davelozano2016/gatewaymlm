@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
       <!-- <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <button data-toggle="modal" data-target="#fund_transfer_modal" class="btn btn-info ">E-Wallet Fund Transfer</button>
            </div>
        </div> -->
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body bg-info">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="icon-credit-card icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Current Bonus</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body bg-info">
                        <div class="media">
                            <div class="mr-1 align-self-center" id="c-count-header">
                                <i class="icon-credit-card icon-2x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                <span class="text-uppercase font-size-sm">Minimum Bonus Request</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-body">
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Request Type:</label>
                    <div class="col-lg-10">
                        <select class="form-control select form-control-select2">
                            <option value="1">Cash</option>
                            <option value="2">Cheque</option>
                            <option value="3">Bitcoin</option>
                            <option value="4">Bank Transfer</option>
                            <option value="5">Remittance</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Enter Bonus Request:</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Enter Account Password:</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-info">Encash Bonus</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.p-encash').addClass('active');
    $('li.payout-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
@endsection
@endsection