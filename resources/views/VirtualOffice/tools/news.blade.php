@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="text-center mb-3 py-2">
                        <h4 class="font-weight-semibold mb-1">Featured articles and tutorials</h4>
                        <span class="text-muted d-block">And porcupine the wallaby far the due thus rash did near dear far pangolin parrot less</span>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-img-actions">
                                    <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    <div class="card-img-actions-overlay card-img-top">
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 rounded-round">
                                            <i class="icon-download4"></i>
                                        </a>
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 ml-2 rounded-round">
                                            <i class="icon-link2"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">For ostrich much</h5>
                                    <p class="card-text">Some various less crept gecko the jeepers dear forewent far the ouch far a incompetent saucy wherever towards</p>
                                </div>

                                <div class="card-footer bg-transparent d-flex justify-content-between">
                                    <span class="text-muted">April 12, 2018</span>
                                    <span>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-half font-size-base text-warning-300"></i>
                                        <span class="text-muted ml-2">(12)</span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-img-actions">
                                    <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    <div class="card-img-actions-overlay card-img-top">
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 rounded-round">
                                            <i class="icon-download4"></i>
                                        </a>
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 ml-2 rounded-round">
                                            <i class="icon-link2"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Helpfully stolidly</h5>
                                    <p class="card-text">Hippopotamus aside while a shrewdly this after kookaburra wow in haphazardly much salmon buoyantly sullen gosh</p>
                                </div>

                                <div class="card-footer bg-transparent d-flex justify-content-between">
                                    <span class="text-muted">April 11, 2018</span>
                                    <span>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <span class="text-muted ml-2">(35)</span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-img-actions">
                                    <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    <div class="card-img-actions-overlay card-img-top">
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 rounded-round">
                                            <i class="icon-download4"></i>
                                        </a>
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 ml-2 rounded-round">
                                            <i class="icon-link2"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Considering far</h5>
                                    <p class="card-text">Kookaburra so hey a less tritely far congratulated this winked some under had unblushing beyond sympathetic</p>
                                </div>

                                <div class="card-footer bg-transparent d-flex justify-content-between">
                                    <span class="text-muted">April 10, 2018</span>
                                    <span>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-empty3 font-size-base text-warning-300"></i>
                                        <span class="text-muted ml-2">(42)</span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-img-actions">
                                    <img src="{{asset('assets/images/mini_logo.png')}}" class="card-img" width="96" alt="">
                                    <div class="card-img-actions-overlay card-img-top">
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 rounded-round">
                                            <i class="icon-download4"></i>
                                        </a>
                                        <a href="#" class="btn btn-outline btn-icon bg-white text-white border-white border-2 ml-2 rounded-round">
                                            <i class="icon-link2"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Despite perversely</h5>
                                    <p class="card-text">Coming merits and was talent enough far. Sir joy northward sportsmen education. Put still any about manor heard</p>
                                </div>

                                <div class="card-footer bg-transparent d-flex justify-content-between">
                                    <span class="text-muted">April 09, 2018</span>
                                    <span>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                        <span class="text-muted ml-2">(59)</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.t-news').addClass('active');
    $('li.tools-must-open').addClass('nav-item-expanded nav-item-open');
</script>
@endsection
@endsection