@extends('layouts.VirtualOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('virtualoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Mailbox</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Inner container -->
    <div class="d-md-flex align-items-md-start">

        <!-- Left sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left border-0 shadow-0 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                @include('VirtualOffice.mailbox.sub-menu')
            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /left sidebar component -->


        <!-- Right content -->
        <div class="flex-fill overflow-auto">
        <div class="card">

<!-- Action toolbar -->
<div class="navbar navbar-light bg-light navbar-expand-lg border-bottom-0 py-lg-2 rounded-top">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler w-100 h-100" data-toggle="collapse" data-target="#inbox-toolbar-toggle-read">
            <i class="icon-circle-down2"></i>
        </button>
    </div>

    <div class="navbar-collapse text-center text-lg-left flex-wrap collapse" id="inbox-toolbar-toggle-read">
        <div class="mt-3 mt-lg-0 mr-lg-3">
            <div class="btn-group">
                <a href="{{ url('virtualoffice/mailbox/reply-mail') }}" class="btn btn-light">
                    <i class="icon-reply"></i>
                    <span class="d-none d-lg-inline-block ml-2">Reply</span>
                </a>
                <a href="{{ url('virtualoffice/mailbox/reply-mail') }}" class="btn btn-light">
                    <i class="icon-reply-all"></i>
                    <span class="d-none d-lg-inline-block ml-2">Reply to all</span>
                </a>
                <button type="button" class="btn btn-light">
                    <i class="icon-forward"></i>
                    <span class="d-none d-lg-inline-block ml-2">Forward</span>
                </button>
                <button type="button" class="btn btn-light">
                    <i class="icon-bin"></i>
                    <span class="d-none d-lg-inline-block ml-2">Delete</span>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item">Select all</a>
                        <a href="#" class="dropdown-item">Select read</a>
                        <a href="#" class="dropdown-item">Select unread</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">Clear selection</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-text ml-lg-auto">12:49 pm</div>

        <div class="ml-lg-3 mb-3 mb-lg-0">
            <div class="btn-group">
                <button type="button" class="btn btn-light">
                    <i class="icon-printer"></i>
                    <span class="d-none d-lg-inline-block ml-2">Print</span>
                </button>
                <button type="button" class="btn btn-light">
                    <i class="icon-new-tab2"></i>
                    <span class="d-none d-lg-inline-block ml-2">Share</span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- /action toolbar -->


<!-- Mail details -->
<div class="card-body">
    <div class="media flex-column flex-md-row">
        <a href="#" class="d-none d-md-block mr-md-3 mb-3 mb-md-0">
            <span class="btn bg-teal-400 btn-icon btn-lg rounded-round">
                <span class="letter-icon"></span>
            </span>
        </a>
        <div class="media-body">
            <h6 class="mb-0">New Imperatives for Enterprise Mobility</h6>
            <div class="letter-icon-title font-weight-semibold">Amanda Smith <a href="#">&lt;jira@diakultd.atlassian.net&gt;</a></div>
        </div>
       
    </div>
</div>
<!-- /mail details -->


<!-- Mail container -->
<div class="card-body">
    <div class="overflow-auto mw-100">
        <p>Sample Email</p>
    </div>
</div>
<!-- /mail container -->


<!-- Attachments -->
<div class="card-body border-top">
    <h6 class="mb-0">2 Attachments</h6>

    <ul class="list-inline mb-0">
        <li class="list-inline-item">
            <div class="card bg-light py-2 px-3 mt-3 mb-0">
                <div class="media my-1">
                    <div class="mr-3 align-self-center"><i class="icon-file-pdf icon-2x text-danger-400 top-0"></i></div>
                    <div class="media-body">
                        <div class="font-weight-semibold">new_december_offers.pdf</div>

                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item text-muted">174 KB</li>
                            <li class="list-inline-item"><a href="#">View</a></li>
                            <li class="list-inline-item"><a href="#">Download</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="list-inline-item">
            <div class="card bg-light py-2 px-3 mt-3 mb-0">
                <div class="media my-1">
                    <div class="mr-3 align-self-center"><i class="icon-file-pdf icon-2x text-danger-400 top-0"></i></div>
                    <div class="media-body">
                        <div class="font-weight-semibold">assignment_letter.pdf</div>

                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item text-muted">736 KB</li>
                            <li class="list-inline-item"><a href="#">View</a></li>
                            <li class="list-inline-item"><a href="#">Download</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<!-- /attachments -->

</div>
        </div>
        <!-- /right content -->
    </div>
    <!-- /inner container -->
</div>


@section('custom')
<script>
    $('a.inbox').addClass('active');
    $('li.mailbox-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/plugins/extensions/rowlink.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/mail_list.js') }}"  ></script>
@endsection
@endsection