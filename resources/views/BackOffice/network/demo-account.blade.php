@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Network</span>
                <span class="breadcrumb-item">Genealogy Tree</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">New Demo Member's Registration Portal</span>
                </div>
                <div class="card-body ">
                    <form>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Sponsor ID:</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-select2 input-form-c" name="sponsor_id" id="sponsor_id">
                                    <option value="1">Sponsor 1</option>
                                    <option value="2">Sponsor 2</option>
                                    <option value="3">Sponsor 3</option>
                                    <option value="4">Sponsor 4</option>
                                    <option value="5">Sponsor 5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Activation Code:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="activation_code" id="activation_code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">First Name:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="first_name" id="first_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Middle Name:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="middle_name" id="middle_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Last Name:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="last_name" id="last_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Email Address:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Mobile Number:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mobile_number" id="mobile_number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Username:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="username" id="username">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Password:</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Confirm Password:</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="cpassword" id="cpassword">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Sponsor:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sponsor" id="sponsor">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Upline:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="upline" id="upline">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Position:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="position" id="position">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-8">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Available Codes</span>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table datatable-basic">
                                   <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Activation Code</th>
                                            <th>Order ID</th>
                                            <th>Product</th>
                                            <th>Points</th>
                                            <th>Order Date</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{$i}}.</td>
                                            <td>Activation{{$i}}</td>
                                            <td>1</td>
                                            <td>test</td>
                                            <td>1</td>
                                            <td>{{ date('F j, Y') }}</td>
                                        </tr>
                                        @endfor
                                   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>

@section('custom')
<script>
    $('a.n-genealogy-tree').addClass('active');
    $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
@endsection
@endsection