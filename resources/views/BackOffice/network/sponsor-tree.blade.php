@extends('layouts.BackOffice.app')
@section('container')
@section('container')
<!-- Main content -->
<link rel="stylesheet" href="{{asset('assets/tree/listree.min.css')}}" />
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Network</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">

                @if(count($users) > 0)
                <div class="card">
                    <div class="card-body" style="background:#046ab9;color:#fff">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>1,500.00 PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6"><b>3</b> <span style="display:block">Total Left
                                            Downlines</span></div>
                                    <div class="col-md-6"><b>1,500</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                        <h6 class="text-uppercase mt-2">{{$users[0]->firstname}} {{$users[0]->surname}}
                                            <span style="display:block"><b>{{$users[0]->id_number}}</b></span>
                                            <span style="display:block"><small
                                                    class="badge badge-success">{{$users[0]->entry}}</small></span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>500.00 PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6">3 <span style="display:block">Total Right Downlines</span>
                                    </div>
                                    <div class="col-md-6">1,500 <span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="card">
                    <div class="card-body" style="background:#046ab9;color:#fff">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Left
                                            Downlines</span></div>
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12 mt-4 mb-2">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6">0 <span style="display:block">Total Right Downlines</span>
                                    </div>
                                    <div class="col-md-6">0<span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="card " style="margin-top:-20px">
                    <div class="card-body ">

                        <div class="row ">
                            <div class="col-md-12">
                                <ul class="listree">
                                    @if(count($unilevel_level_1) > 0)
                                        @foreach($unilevel_level_1 as $level_1)
                                        <li>
                                            <div class="listree-submenu-heading">{{$level_1->waiting_id_number}}</div>
                                            <ul class="listree-submenu-items">
                                                @if(count($unilevel_level_2[$level_1->waiting_id_number]) > 0) 
                                                    @foreach($unilevel_level_2[$level_1->waiting_id_number] as $level_2)
                                                    <li>
                                                        <div class="listree-submenu-heading">{{$level_2->waiting_id_number}}</div>
                                                        <ul class="listree-submenu-items">
                                                        </ul>
                                                    </li>
                                                    @endforeach
                                                @else
                                                @endif
                                            </ul>
                                        </li>
                                        @endforeach
                                    @else
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- /main charts -->
    </div>


    @section('custom')

    <script>
        $('a.n-sponsor-tree').addClass('active');
        $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/extra_trees.js') }}"></script>
    <script src="{{ asset('assets/tree/listree.umd.min.js') }}"></script>
    <script>
        listree();

    </script>
    @endsection
    @endsection
