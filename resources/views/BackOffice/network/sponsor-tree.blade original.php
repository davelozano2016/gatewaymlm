@extends('layouts.BackOffice.app')
@section('container')
@section('container')
<style>
    .fancytree-has-children.fancytree-ico-e .fancytree-icon:after,
    .fancytree-ico-c .fancytree-icon:after,
    .fancytree-ico-e .fancytree-icon:after,
    .fancytree-has-children.fancytree-ico-cf .fancytree-icon:after {
        content: none !important;
    }

    .fancytree-title {
        margin-left: 0 !important;
    }

    .fancytree-custom-icon,
    .fancytree-expander,
    .fancytree-icon {
        width: 0 !important;
    }

</style>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Network</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">

                @if(count($users) > 0)
                <div class="card">
                    <div class="card-body" style="background:#046ab9;color:#fff">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>1,500.00 PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6"><b>3</b> <span style="display:block">Total Left
                                            Downlines</span></div>
                                    <div class="col-md-6"><b>1,500</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                        <h6 class="text-uppercase mt-2">{{$users[0]->firstname}} {{$users[0]->surname}}
                                            <span style="display:block"><b>{{$users[0]->id_number}}</b></span>
                                            <span style="display:block"><small
                                                    class="badge badge-success">{{$users[0]->entry}}</small></span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>500.00 PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6">3 <span style="display:block">Total Right Downlines</span>
                                    </div>
                                    <div class="col-md-6">1,500 <span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="card">
                    <div class="card-body" style="background:#046ab9;color:#fff">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Left
                                            Downlines</span></div>
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12 mt-4 mb-2">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6">0 <span style="display:block">Total Right Downlines</span>
                                    </div>
                                    <div class="col-md-6">0<span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="card" style="margin-top:-20px">
                    <div class="card-body">
                    </div>

                </div>

            </div>
        </div>
        <!-- /main charts -->
    </div>


    @section('custom')

    <script>
        $('a.n-sponsor-tree').addClass('active');
        $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
        $(".pop").popover({
                trigger: "manual",
                content: '<div class="pop-container"><div class="row"><div class="text-center py-2 col-12"><img class="pop-image" src="{{asset("assets/images/mini_logo.png")}}" alt=""><br>computology<br>123456</div></div> <div class="row"><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{$date->format("Y/m/d")}}</div></div><div class="row"><div class="col-md-5 col-5">Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">12100</div></div><div class="row"><div class="col-md-5 col-5">Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">4200</div></div><div class="row"><div class="col-md-5 col-5">Left Carry</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row"><div class="col-md-5 col-5">Right Carry</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">1000</div></div><div class="row"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">16340</div></div><div class="row mt-2"><div class="col-md-12"><span class="btn-block badge badge-success">PREMIUM</span></div></div></div>',
                html: true,
                animation: false
            })
            .on("mouseenter", function () {
                var _this = this;
                $(this).popover("show");
                $(".popover").on("mouseleave", function () {
                    $(_this).popover('hide');
                });
            }).on("mouseleave", function () {
                var _this = this;
                setTimeout(function () {
                    if (!$(".popover:hover").length) {
                        $(_this).popover("hide");
                    }
                }, 100);
            });

    </script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/extra_trees.js') }}"></script>

    @endsection
    @endsection
