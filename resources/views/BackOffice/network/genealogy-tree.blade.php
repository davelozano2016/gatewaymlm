@extends('layouts.VirtualOffice.app')
@section('container')
<style>
/* Custom styles for popover */
.pop-image {
    width: 50px;
}

.pop-container {
    width: 250px !important;
    padding:5px;
}

.custom {
    width: 75% !important;
    margin: auto;
}

#pop {
    padding: 0px 0px;
}

#example {
    position: relative;
}


* {
    padding: 0
}

.tree div.extended {}

.tree div.extendedv2 {
    width: 50% !important;
    border-top: 1px solid #ccc !important;
    border-right: 1px solid #ccc !important;
    margin-top: -4px;
}


.tree div.co {
    width: 0% !important;
    margin: auto;
    /* border-left:1px solid #ccc !important; */
}


.tree ul {
    padding-top: 20px;
    position: relative;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}

.tree ul li ul li {
    width: 50%;
}

.tree li {
    float: left;
    text-align: center;
    list-style-type: none;
    position: relative;
    padding: 20px 5px 0 3px;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    border: none !important;
}

.tree li a i {
    font-size: 24px;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before,
.tree li::after {
    content: '';
    position: absolute;
    top: 0;
    right: 50%;
    /* border-top: 1px solid #ccc; */
    width: 50%;
    height: 20px;
}

.tree li::after {
    right: auto;
    left: 50%;
    /* border-left: 1px solid #ccc; */
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after,
.tree li:only-child::before {
    display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child {
    padding-top: 0;
}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before,
.tree li:last-child::after {
    border: 0 none;
}

/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before {
    /* border-right: 1px solid #ccc; */
    border-radius: 0 0px 0 0;
    -webkit-border-radius: 0 0px 0 0;
    -moz-border-radius: 0 0px 0 0;
}

.tree li:first-child::after {
    -webkit-border-radius: 0px 0 0 0;
    -moz-border-radius: 0px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before {
    content: '';
    position: absolute;
    top: 0;
    left: 50%;
    /* border-left: 1px solid #ccc; */
    width: 0;
    height: 20px;
}

.tree li a {
    border: 1px solid #fff;
    /* padding: 5px 10px; */
    text-decoration: none;
    color: #666;
    font-family: arial, verdana, tahoma;
    font-size: 11px;
    display: inline-block;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}


.sample {
    border: 1px solid #fff;
    /* padding: 5px 10px; */
    text-decoration: none;
    color: #666;
    font-family: arial, verdana, tahoma;
    font-size: 11px;
    display: inline-block;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
}

.popover-body {
    padding: 0 !important;
}

.row.pop_description {
    padding: 0px 10px;
}

.row.mt-2.pop_description.pop_desc_bot {
    padding-bottom: 10px;
}

.text-center.col-12.pop_header {
    background: #109ad8;
    padding: 10px 0px !important;
}

/*Thats all. I hope you enjoyed it.
Thanks :)*/
</style>
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Network</span>
                    <span class="breadcrumb-item active">Genealogy Tree</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">

            <div class="col-xl-12">
                @if(count($users) > 0)
                <div class="card">
                    <div class="card-body genealogy-tree-background">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>{{number_format($total_waiting_points_left->points,2)}} PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    {{-- <div class="col-md-6"><b>3</b> <span style="display:block">Total Left
                                            Downlines</span></div> --}}
                                    <div class="col-md-12 text-center"><b>{{number_format($total_left + $total_waiting_points_left->points + $total_flush_left,2)}}</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                        <h6 class="text-uppercase mt-2">{{$users[0]->firstname}} {{$users[0]->surname}}
                                            <span style="display:block"><b>{{$users[0]->id_number}}</b></span>
                                            <span style="display:block"><small
                                                    class="badge badge-success">{{$users[0]->entry}}</small></span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>{{number_format($total_waiting_points_right->points,2)}} PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    {{-- <div class="col-md-6">3 <span style="display:block">Total Right Downlines</span>
                                    </div> --}}
                                    <div class="col-md-12 text-center">{{number_format($total_right + $total_waiting_points_right->points + $total_flush_right,2)}} <span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="card">
                    <div class="card-body" style="background:#046ab9;color:#fff">
                        <div class="row">
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Left</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Left
                                            Downlines</span></div>
                                    <div class="col-md-6"><b>0</b> <span style="display:block">Total Group
                                            Points</span></div>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="row ">
                                    <div class="col-md-12 mt-4 mb-2">
                                        <a data-toggle="modal" data-target="#modal_default" style="cursor:pointer">
                                            <img style="width:100px" class="img-responsive"
                                                src="{{asset('assets/images/mini_logo.png')}}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center" style="padding:20px">
                                <div class="row text-uppercase">
                                    <div class="col-md-12">
                                        <h6 for=""><b>0.00 PV</b> <br> Waiting Points Right</h6>
                                    </div>
                                </div>
                                <div class="row text-uppercase" style="padding:20px;background:#109ad8;color:#fff">
                                    <div class="col-md-6">0 <span style="display:block">Total Right Downlines</span>
                                    </div>
                                    <div class="col-md-6">0<span style="display:block">Total Group Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="card" style="margin-top:-20px">
                    
                    <div class="card-body" style="margin-top:-40px">
                        <div class="row">
                            <div class="col-md-12" id="c-tree">
                                <div class="tree">
                                    <ul>
                                    @if(isset($distributor_id))
                                    <div class="row justify-content-center">
                                        <a href="#" class="btn btn-info btn-sm" style="z-index:1000" onclick="goBack()"><i class="icon icon-chevron-up"></i></a>
                                    </div>
                                    @else
                                    @endif
                                        <li style="width:100%">
                                            <ul>
                                                <!-- Left Panel -->
                                                <li style="width:50%">
                                                    @if(count($upline_left_level_1) > 0)
                                                    
                                                    <a class="pop" data-container="body" 
                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_1[0]->firstname}} {{$upline_left_level_1[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_1[0]->id_number)}}>{{@$upline_left_level_1[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_1[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_1[0]->entry}}</span></div></div></div>'
                                                        data-toggle="popover"
                                                        data-placement="right">
                                                        <div
                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                        </div>
                                                    </a>
                                                    <div>
                                                        <span style="background:#9ed98c"
                                                            class="text-white badge"><b>0PV</b></span>
                                                        &nbsp;
                                                        <span style="background:#9ed98c"
                                                            class="text-white badge"><b>0PV</b></span>
                                                    </div>

                                                    <div class="row text-uppercase" id="c-first-desc">
                                                        <div class="col-md-12 col-sm-12">
                                                            <span style="display:block;">
                                                                <a href="javascript:void(0)"
                                                                    style="font-size:10px;"><b>{{$upline_left_level_1[0]->username}}</b></a>
                                                                <br>
                                                                <b><a href="javascript:void(0)">{{$upline_left_level_1[0]->id_number}}</a></b>
                                                                <br>
                                                                    @if($upline_left_level_1[0]->entry == 'Silver')
                                                                        <label class="text-uppercase badge badge-primary">{{$upline_left_level_1[0]->entry}}</label>
                                                                    @elseif($upline_left_level_1[0]->entry == 'Gold')
                                                                        <label class="text-uppercase badge badge-info">{{$upline_left_level_1[0]->entry}}</label>
                                                                    @else 
                                                                        <label class="text-uppercase badge badge-success">{{$upline_left_level_1[0]->entry}}</label>
                                                                    @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    @else
                                                    <a class="pop" data-container="body" data-toggle="popover"
                                                        data-placement="right">
                                                        <div
                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                            @if(count($users) > 0)
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                onclick="popoverModal('{{$users[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                            @else
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                            @endif
                                                        </div>
                                                    </a>
                                                    @endif
                                                    <ul>
                                                        @if(count($upline_left_level_2_left) > 0)
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" 
                                                                data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_2_left[0]->firstname}} {{$upline_left_level_2_left[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_2_left[0]->id_number)}}>{{@$upline_left_level_2_left[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_2_left[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_2_left[0]->entry}}</span></div></div></div>'
                                                                data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                </div>
                                                            </a>
                                                            
                                                            <div>
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                                &nbsp;
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                            </div>

                                                            <div class="row text-uppercase" id="c-first-desc">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <span style="display:block;">
                                                                        <a href="javascript:void(0)"
                                                                            style="font-size:10px;"><b>{{$upline_left_level_2_left[0]->username}}</b></a>
                                                                        <br>
                                                                        <b><a
                                                                                href="javascript:void(0)">{{$upline_left_level_2_left[0]->id_number}}</a></b>
                                                                        <br>
                                                                        @if($upline_left_level_2_left[0]->entry == 'Silver')
                                                                            <label class="text-uppercase badge badge-primary">{{$upline_left_level_2_left[0]->entry}}</label>
                                                                        @elseif($upline_left_level_2_left[0]->entry == 'Gold')
                                                                            <label class="text-uppercase badge badge-info">{{$upline_left_level_2_left[0]->entry}}</label>
                                                                        @else 
                                                                            <label class="text-uppercase badge badge-success">{{$upline_left_level_2_left[0]->entry}}</label>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <ul>
                                                                @if(count($upline_left_level_3_left_A) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_3_left_A[0]->firstname}} {{$upline_left_level_3_left_A[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_3_left_A[0]->id_number)}}>{{@$upline_left_level_3_left_A[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_3_left_A[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_3_left_A[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>

                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_left_level_3_left_A[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_left_level_3_left_A[0]->id_number}}</a></b>
                                                                                <br>
                                                                                    @if($upline_left_level_3_left_A[0]->entry == 'Silver')
                                                                                        <label class="text-uppercase badge badge-primary">{{$upline_left_level_3_left_A[0]->entry}}</label>
                                                                                    @elseif($upline_left_level_3_left_A[0]->entry == 'Gold')
                                                                                        <label class="text-uppercase badge badge-info">{{$upline_left_level_3_left_A[0]->entry}}</label>
                                                                                    @else 
                                                                                        <label class="text-uppercase badge badge-success">{{$upline_left_level_3_left_A[0]->entry}}</label>
                                                                                    @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_2_left) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_2_left[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif

                                                                @if(count($upline_left_level_3_left_B) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_3_left_B[0]->firstname}} {{$upline_left_level_3_left_B[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_3_left_B[0]->id_number)}}>{{@$upline_left_level_3_left_B[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_3_left_B[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_3_left_B[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>

                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_left_level_3_left_B[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_left_level_3_left_B[0]->id_number}}</a></b>
                                                                                <br>
                                                                                    @if($upline_left_level_3_left_B[0]->entry == 'Silver')
                                                                                        <label class="text-uppercase badge badge-primary">{{$upline_left_level_3_left_B[0]->entry}}</label>
                                                                                    @elseif($upline_left_level_3_left_B[0]->entry == 'Gold')
                                                                                        <label class="text-uppercase badge badge-info">{{$upline_left_level_3_left_B[0]->entry}}</label>
                                                                                    @else 
                                                                                        <label class="text-uppercase badge badge-success">{{$upline_left_level_3_left_B[0]->entry}}</label>
                                                                                    @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_2_left) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_2_left[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    @if(count($upline_left_level_1) > 0)
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}"
                                                                        onclick="popoverModal('{{$upline_left_level_1[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                    @else
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                    @endif
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_3_left_A) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_3_left_A[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>

                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_3_left_B) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_3_left_B[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif

                                                        @if(count($upline_left_level_2_right) > 0)
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" 
                                                                data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_2_right[0]->firstname}} {{$upline_left_level_2_right[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_2_right[0]->id_number)}}>{{@$upline_left_level_2_right[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_2_right[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_2_right[0]->entry}}</span></div></div></div>'
                                                                data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                </div>
                                                            </a>

                                                            <div>
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                                &nbsp;
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                            </div>

                                                            <div class="row text-uppercase" id="c-first-desc">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <span style="display:block;">
                                                                        <a href="javascript:void(0)"
                                                                            style="font-size:10px;"><b>{{$upline_left_level_2_right[0]->username}}</b></a>
                                                                        <br>
                                                                        <b><a
                                                                                href="javascript:void(0)">{{$upline_left_level_2_right[0]->id_number}}</a></b>
                                                                        <br>
                                                                        @if($upline_left_level_2_right[0]->entry == 'Silver')
                                                                            <label class="text-uppercase badge badge-primary">{{$upline_left_level_2_right[0]->entry}}</label>
                                                                        @elseif($upline_left_level_2_right[0]->entry == 'Gold')
                                                                            <label class="text-uppercase badge badge-info">{{$upline_left_level_2_right[0]->entry}}</label>
                                                                        @else 
                                                                            <label class="text-uppercase badge badge-success">{{$upline_left_level_2_right[0]->entry}}</label>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <ul>
                                                                @if(count($upline_left_level_3_right_A) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_3_right_A[0]->firstname}} {{$upline_left_level_3_right_A[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_3_right_A[0]->id_number)}}>{{@$upline_left_level_3_right_A[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_3_right_A[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_3_right_A[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>

                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_left_level_3_right_A[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_left_level_3_right_A[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_left_level_3_right_A[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_left_level_3_right_A[0]->entry}}</label>
                                                                                @elseif($upline_left_level_3_right_A[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_left_level_3_right_A[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_left_level_3_right_A[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_2_right) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_2_right[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif

                                                                @if(count($upline_left_level_3_right_B) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_left_level_3_right_B[0]->firstname}} {{$upline_left_level_3_right_B[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_left_level_3_right_B[0]->id_number)}}>{{@$upline_left_level_3_right_B[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_left_level_3_right_B[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_left_level_3_right_B[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>

                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_left_level_3_right_B[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_left_level_3_right_B[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_left_level_3_right_B[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_left_level_3_right_B[0]->entry}}</label>
                                                                                @elseif($upline_left_level_3_right_B[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_left_level_3_right_B[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_left_level_3_right_B[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_left_level_2_right) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_left_level_2_right[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    @if(count($upline_left_level_1) > 0)
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}"
                                                                        onclick="popoverModal('{{$upline_left_level_1[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                    @else
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                    @endif
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                </li>

                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                    </ul>
                                                </li>

                                                <!-- Right Panel -->

                                                <li style="width:50%">
                                                    @if(count($upline_right_level_1) > 0)
                                                    
                                                    <a class="pop" data-container="body" 
                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_1[0]->firstname}} {{$upline_right_level_1[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_1[0]->id_number)}}>{{@$upline_right_level_1[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_1[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_1[0]->entry}}</span></div></div></div>'
                                                        data-toggle="popover"
                                                        data-placement="right">
                                                        <div
                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                        </div>
                                                    </a>
                                                    <div>
                                                        <span style="background:#9ed98c"
                                                            class="text-white badge"><b>0PV</b></span>
                                                        &nbsp;
                                                        <span style="background:#9ed98c"
                                                            class="text-white badge"><b>0PV</b></span>
                                                    </div>
                                                
                                                    <div class="row text-uppercase" id="c-first-desc">
                                                        <div class="col-md-12 col-sm-12">
                                                            <span style="display:block;">
                                                                <a href="javascript:void(0)"
                                                                    style="font-size:10px;"><b>{{$upline_right_level_1[0]->username}}</b></a>
                                                                <br>
                                                                <b><a
                                                                        href="javascript:void(0)">{{$upline_right_level_1[0]->id_number}}</a></b>
                                                                <br>
                                                                    @if($upline_right_level_1[0]->entry == 'Silver')
                                                                        <label class="text-uppercase badge badge-primary">{{$upline_right_level_1[0]->entry}}</label>
                                                                    @elseif($upline_right_level_1[0]->entry == 'Gold')
                                                                        <label class="text-uppercase badge badge-info">{{$upline_right_level_1[0]->entry}}</label>
                                                                    @else 
                                                                        <label class="text-uppercase badge badge-success">{{$upline_right_level_1[0]->entry}}</label>
                                                                    @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    @else
                                                    <a class="pop" data-container="body" data-toggle="popover"
                                                        data-placement="right">
                                                        <div
                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                            @if(count($users) > 0)
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                onclick="popoverModal('{{$users[0]->id_number}}','{{$users[0]->id_number}}',1,'{{$name}}')">
                                                            @else
                                                            <img style="width:30px;cursor:pointer;"
                                                                class="img-responsive"
                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                            @endif
                                                        </div>
                                                    </a>
                                                    @endif
                                                    <ul>
                                                        @if(count($upline_right_level_2_left) > 0)
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" 
                                                                data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_2_left[0]->firstname}} {{$upline_right_level_2_left[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_2_left[0]->id_number)}}>{{@$upline_right_level_2_left[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_2_left[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_2_left[0]->entry}}</span></div></div></div>'
                                                                data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                </div>
                                                            </a>
                                                            
                                                            <div>
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                                &nbsp;
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                            </div>
                                                
                                                            <div class="row text-uppercase" id="c-first-desc">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <span style="display:block;">
                                                                        <a href="javascript:void(0)"
                                                                            style="font-size:10px;"><b>{{$upline_right_level_2_left[0]->username}}</b></a>
                                                                        <br>
                                                                        <b><a
                                                                                href="javascript:void(0)">{{$upline_right_level_2_left[0]->id_number}}</a></b>
                                                                        <br>
                                                                        @if($upline_right_level_2_left[0]->entry == 'Silver')
                                                                            <label class="text-uppercase badge badge-primary">{{$upline_right_level_2_left[0]->entry}}</label>
                                                                        @elseif($upline_right_level_2_left[0]->entry == 'Gold')
                                                                            <label class="text-uppercase badge badge-info">{{$upline_right_level_2_left[0]->entry}}</label>
                                                                        @else 
                                                                            <label class="text-uppercase badge badge-success">{{$upline_right_level_2_left[0]->entry}}</label>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <ul>
                                                                @if(count($upline_right_level_3_left_A) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_3_left_A[0]->firstname}} {{$upline_right_level_3_left_A[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_3_left_A[0]->id_number)}}>{{@$upline_right_level_3_left_A[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_3_left_A[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_3_left_A[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>
                                                
                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_right_level_3_left_A[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_right_level_3_left_A[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_right_level_3_left_A[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_right_level_3_left_A[0]->entry}}</label>
                                                                                @elseif($upline_right_level_3_left_A[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_right_level_3_left_A[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_right_level_3_left_A[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_2_left) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_2_left[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                
                                                                @if(count($upline_right_level_3_left_B) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_3_left_B[0]->firstname}} {{$upline_right_level_3_left_B[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_3_left_B[0]->id_number)}}>{{@$upline_right_level_3_left_B[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_3_left_B[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_3_left_B[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>
                                                
                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_right_level_3_left_B[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_right_level_3_left_B[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_right_level_3_left_B[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_right_level_3_left_B[0]->entry}}</label>
                                                                                @elseif($upline_right_level_3_left_B[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_right_level_3_left_B[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_right_level_3_left_B[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_2_left) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_2_left[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    @if(count($upline_right_level_1) > 0)
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}"
                                                                        onclick="popoverModal('{{$upline_right_level_1[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                    @else
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                    @endif
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_3_left_A) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_3_left_A[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_3_left_B) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_3_left_B[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                
                                                        @if(count($upline_right_level_2_right) > 0)
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" 
                                                                data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_2_right[0]->firstname}} {{$upline_right_level_2_right[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_2_right[0]->id_number)}}>{{@$upline_right_level_2_right[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_2_right[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_2_right[0]->entry}}</span></div></div></div>'
                                                                data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                </div>
                                                            </a>
                                                
                                                            <div>
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                                &nbsp;
                                                                <span style="background:#9ed98c"
                                                                    class="text-white badge"><b>0PV</b></span>
                                                            </div>
                                                
                                                            <div class="row text-uppercase" id="c-first-desc">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <span style="display:block;">
                                                                        <a href="javascript:void(0)"
                                                                            style="font-size:10px;"><b>{{$upline_right_level_2_right[0]->username}}</b></a>
                                                                        <br>
                                                                        <b><a
                                                                                href="javascript:void(0)">{{$upline_right_level_2_right[0]->id_number}}</a></b>
                                                                        <br>
                                                                        @if($upline_right_level_2_right[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_right_level_2_right[0]->entry}}</label>
                                                                        @elseif($upline_right_level_2_right[0]->entry == 'Gold')
                                                                            <label class="text-uppercase badge badge-info">{{$upline_right_level_2_right[0]->entry}}</label>
                                                                        @else 
                                                                            <label class="text-uppercase badge badge-success">{{$upline_right_level_2_right[0]->entry}}</label>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                
                                                            <ul>
                                                                @if(count($upline_right_level_3_right_A) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_3_right_A[0]->firstname}} {{$upline_right_level_3_right_A[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_3_right_A[0]->id_number)}}>{{@$upline_right_level_3_right_A[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_3_right_A[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_3_right_A[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>
                                                
                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_right_level_3_right_A[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_right_level_3_right_A[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_right_level_3_right_A[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_right_level_3_right_A[0]->entry}}</label>
                                                                                @elseif($upline_right_level_3_right_A[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_right_level_3_right_A[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_right_level_3_right_A[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_2_right) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_2_right[0]->id_number}}','{{$id_number}}',0,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                
                                                                @if(count($upline_right_level_3_right_B) > 0)
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body" 
                                                                        data-content='<div class="pop-container"><div><div class="text-center col-12 "><img class="pop-image" src="{{asset('assets/images/mini_logo.png')}}" alt=""><br>{{$upline_right_level_3_right_B[0]->firstname}} {{$upline_right_level_3_right_B[0]->surname}}<br><a href={{url("virtualoffice/network/genealogy-tree/visit")}}/{{Crypt::encryptString(@$upline_right_level_3_right_B[0]->id_number)}}>{{@$upline_right_level_3_right_B[0]->id_number}}</a></div></div> <div class="row pop_description" ><div class="col-md-5 col-5">Joined Date</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">{{date("Y-m-d",strtotime($upline_right_level_3_right_B[0]->date_registered))}}</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Team Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Left</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">7900</div></div><div class="row pop_description"><div class="col-md-5 col-5">Waiting Right</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Personal PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row pop_description"><div class="col-md-5 col-5">Group PV</div><div class="text-center col-md-2 col-2">:</div><div class="col-md-5 col-5">0</div></div><div class="row mt-2 pop_description pop_desc_bot"><div class="col-md-12"><span class="btn-block badge badge-success d-none text-uppercase">{{$upline_right_level_3_right_B[0]->entry}}</span></div></div></div>'
                                                                        data-toggle="popover"
                                                                        data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                    <div>
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                        &nbsp;
                                                                        <span style="background:#9ed98c"
                                                                            class="text-white badge"><b>0PV</b></span>
                                                                    </div>
                                                
                                                                    <div class="row text-uppercase" id="c-first-desc">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <span style="display:block;">
                                                                                <a href="javascript:void(0)"
                                                                                    style="font-size:10px;"><b>{{$upline_right_level_3_right_B[0]->username}}</b></a>
                                                                                <br>
                                                                                <b><a
                                                                                        href="javascript:void(0)">{{$upline_right_level_3_right_B[0]->id_number}}</a></b>
                                                                                <br>
                                                                                @if($upline_right_level_3_right_B[0]->entry == 'Silver')
                                                                                    <label class="text-uppercase badge badge-primary">{{$upline_right_level_3_right_B[0]->entry}}</label>
                                                                                @elseif($upline_right_level_3_right_B[0]->entry == 'Gold')
                                                                                    <label class="text-uppercase badge badge-info">{{$upline_right_level_3_right_B[0]->entry}}</label>
                                                                                @else 
                                                                                    <label class="text-uppercase badge badge-success">{{$upline_right_level_3_right_B[0]->entry}}</label>
                                                                                @endif
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @else
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            @if(count($upline_right_level_2_right) > 0)
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}"
                                                                                onclick="popoverModal('{{$upline_right_level_2_right[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                            @else
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li style="width:50%">
                                                            <a class="pop" data-container="body" data-toggle="popover"
                                                                data-placement="right">
                                                                <div
                                                                    style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                    @if(count($upline_right_level_1) > 0)
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}"
                                                                        onclick="popoverModal('{{$upline_right_level_1[0]->id_number}}','{{$id_number}}',1,'{{$name}}')">
                                                                    @else
                                                                    <img style="width:30px;cursor:pointer;"
                                                                        class="img-responsive"
                                                                        src="{{asset('assets/images/mini_logo.png')}}">
                                                                    @endif
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                
                                                                <li style="width:50%">
                                                                    <a class="pop" data-container="body"
                                                                        data-toggle="popover" data-placement="right">
                                                                        <div
                                                                            style="margin:auto;border:1px solid #e0ac64;border-radius:100% !important;width:60px;padding:10px">
                                                                            <img style="width:30px;cursor:pointer;"
                                                                                class="img-responsive"
                                                                                src="{{asset('assets/images/mini_logo.png')}}">
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                    </ul>
                                                </li>
                                               
                                              
                                            </ul>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Registered
                            Downline List</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="text-uppercase table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th>Joined Date</th>
                                        <th>Name</th>
                                        <th>ID</th>
                                        <th>Package</th>
                                        <th>Sponsor</th>
                                        <th>Upline</th>
                                        <th>Position</th>
                                        <th>Total Left Points</th>
                                        <th>Total Right Points</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($registered_downlines) > 0)
                                        @foreach($registered_downlines as $downlines)
                                            <tr>
                                                <td>{{$downlines->date_registered}}</td>
                                                <td>{{$downlines->firstname}} {{$downlines->surname}}</td>
                                                <td>{{$downlines->id_number}}</td>
                                                <td>{{$downlines->entry}}</td>
                                                <td>{{$downlines->sponsor_id}}</td>
                                                <td>{{$downlines->upline_id}}</td>
                                                <td>
                                                    @if($downlines->position == 0)
                                                        LEFT
                                                    @else
                                                        RIGHT
                                                    @endif
                                                </td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                        @endforeach
                                    @else
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /main charts -->
        </div>
    </div>

    <form action="{{url('virtualoffice/network/genealogy-tree/processing')}}" method="POST">
        @csrf
        <div class="modal" id="accountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Placement <b id="title_position"></b> of <b
                                id="title_id_number"></b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="distributor_id_number" id="account_id_number">
                        <input type="hidden" name="distributor_position" id="account_position">
                        <input type="hidden" name="distributor_sponsor_id" id="account_sponsor_id">
                        <button type="submit" name="btn_new_account" class="btn btn-primary text-uppercase btn-block">
                            <h3>Register NEW Account</h3><b>Note: This portal is for the registration of NEW
                                ACCOUNT</b>
                        </button>
                        @if($query[0]->account_type == 0)
                            <button type="submit" name="btn_sub_account" class="btn btn-primary text-uppercase btn-block">
                                <h3>Register Sub Account</h3><b>Note: This portal is for the
                                    additional account of <b id="account_name_desc"></b></b>
                            </button>
                        @else
                        <a href="{{url('virtualoffice/my-account/sub-account')}}" class="btn btn-primary text-uppercase btn-block">
                            <h3>Register Sub Account</h3><b>Note: You need to switch to your main account to add sub account</b>
                        </a>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="{{url('virtualoffice/network/genealogy-tree/result')}}" data-parsley-validate method="POST">
        @csrf
        <div id="modal_default" class="modal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Search Distrubitor</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Distributor ID</label>
                            <div class="col-lg-10">
                                <input type="text" id="sponsor_id" onkeyup="search_sponsor()" name="distributor_id" class="form-control" required>
                                <span id="sponsor"></span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="btn_search" class="btn btn-sm bg-primary">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @section('custom')
    <script>
    $('a.n-genealogy-tree').addClass('active');
    $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    
    <script>
    function popoverModal(id, sponsor_id, position, name) {
        $('#accountModal').modal();
        $('#account_id_number').val(id)
        $('#title_id_number').html(id)
        $('#title_position').html(position == 0 ? 'LEFT' : 'RIGHT')
        $('#account_position').val(position)
        $('#account_sponsor_id').val(sponsor_id)
        $('#account_name, #account_name_desc').html(name)
    }


        $(".pop").popover({
            trigger: "manual",
            html: true,
            animation: false
        })
        .on("mouseenter", function() {
            var _this = this;
            $(this).popover("show");
            $(".popover").on("mouseleave", function() {
            $(_this).popover('hide');
            });
        }).on("mouseleave", function() {
            var _this = this;
            setTimeout(function() {
                if (!$(".popover:hover").length) {
                    $(_this).popover("hide");
                }
            }, 1);
        });

    function goBack() {
        window.history.back();
    }

    function search_sponsor() {
        var sponsor_id = $('#sponsor_id').val();
        $.ajax({
            type : 'POST',
            url : "{{url('backoffice/search')}}",
            dataType: 'json',
            data: {
                id_number : sponsor_id,
                "_token": "{!! csrf_token() !!}"
            },
            beforeSend() {
                $('#sponsor').html('Searching...')
                $('#btn_search').attr('disabled',true);
            },
            success:function(data){
                if(data.success == true) {
                    $('#sponsor').html(data.name)
                } else {
                    $('#sponsor').html(data.name)
                }
                $('#btn_search').attr('disabled',false);
            },
            error:function() {
                $('#sponsor').html('No record found.')
                $('#btn_search').attr('disabled',true);
            }
        })
    }


    </script>
    @endsection
    @endsection