@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Network</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
    
        <div class="col-xl-12">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-teal icon-tree6 icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">123</h3>
                                <span class="text-uppercase font-size-sm">Total Referral Downlines</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-success icon-tree7 icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">123</h3>
                                <span class="text-uppercase font-size-sm">Total Levels</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <label class="">Username</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <label class="">Level</label>
                                            <select name="order_type" id="order_type" class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-4">
                                        <div class="form-group"><br>
                                            <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                            <button class="btn btn-warning"><i class="icon-reset"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                   <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Member</th>
                                            <th>Sponsor</th>
                                            <th>Level</th>
                                            <th>Joining Date</th>
                                            <th>Action</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{$i}}.</td>
                                            <td>John Doe (username{{$i}}) <br> <a href="{{ url('backoffice/profile-management/profile-view') }}">C0000{{$i}}</a></td>
                                            <td>Member</td>
                                            <td>1</td>
                                            <td>{{$date->format('M j, Y g:i:s A')}}</td>
                                            <td>
                                                <a href="{{ url('backoffice/network/sponsor-tree') }}" class="btn btn-info" data-popup="tooltip" title="View Sponsor"><i class="icon-tree7"></i></a>
                                            </td>
                                        </tr>
                                        @endfor
                                   </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-warning" type="button">Block</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>

@section('custom')
<script>
    $('a.n-referral-members').addClass('active');
    $('li.network-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection