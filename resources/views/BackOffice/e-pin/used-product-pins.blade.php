@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Code Management</span>
                    <span class="breadcrumb-item">Product Pins</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <!-- Traffic sources -->
            <div class="col-12 col-md-12">
                <div class="card">

                    <div class="">
                        <div class="table-responsive">
                            <form method="POST" action="{{url('backoffice/e-pin/paid-codes/mass-status-update')}}">
                                @csrf
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th >Product</th>
                                            <th>Activation Code</th>
                                            <th>UPV</th>
                                            <th>BPV</th>
                                            <th>Used By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($used as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->product_title}}</td>
                                            <td>{{$data->activation_code}}</td>
                                            <td style="width:1px">{{number_format($data->unilevel_point_value,2)}}</td>
                                            <td style="width:1px">{{number_format($data->binary_point_value,2)}}</td>
                                            <td>{{$data->used_by}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>






    <!-- /content area -->
    @section('custom')
    <script>
    $('a.pp-used-pins').addClass('active');
    $('li.p-pin-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/dat$atables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script>
    var selected = new Array();


    $("#selectAll").click(function() {
        $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
    });
    </script>
    @endsection
    @endsection