@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Code Management</span>
                    <span class="breadcrumb-item">Product Pins</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <!-- Traffic sources -->
            <div class="col-12 col-md-12">
                <div class="card">

                    <div class="">
                        <div class="table-responsive">
                            <form method="POST" action="{{url('backoffice/e-pin/paid-codes/mass-status-update')}}">
                                @csrf
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th style="width:1px"><input id="selectAll"
                                                    class="form-check-input-styled-primary" type="checkbox"></th>
                                            <th>User ID</th>
                                            <th>Product</th>
                                            <th>Activation Code</th>
                                            <th>UPV</th>
                                            <th>BPV</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($unused as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><input type="checkbox" name="selected[]" value="{{$data->pin_id}}"></td>
                                            <td><a href="{{url('backoffice/profile-management/members')}}/{{Crypt::encryptString($data->distributor_id)}}">{{$data->distributor_id}}</a></td>
                                            <td>{{$data->product_title}}</td>
                                            <td>{{$data->activation_code}}</td>
                                            <td style="width:1px">{{number_format($data->unilevel_point_value,2)}}</td>
                                            <td style="width:1px">{{number_format($data->binary_point_value,2)}}</td>
                                            <td style="width:1%">
                                                @if($data->status == 0)
                                                @if($data->lock_status == 0)
                                                <div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox"
                                                            onchange="lock_status('{{Crypt::encryptString($data->lock_status)}}','{{Crypt::encryptString($data->pin_id)}}')"
                                                            id="lock_status_{{$data->pin_id}}" data-on-color="primary"
                                                            data-off-color="danger" data-on-text="UNLOCKED"
                                                            data-off-text="LOCKED" class="form-check-input-switch"
                                                            checked>
                                                    </label>
                                                </div>
                                                @else
                                                <div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox"
                                                            onchange="lock_status('{{Crypt::encryptString($data->lock_status)}}','{{Crypt::encryptString($data->pin_id)}}')"
                                                            id="lock_status_{{$data->pin_id}}" data-on-color="primary"
                                                            data-off-color="danger" data-on-text="UNLOCKED"
                                                            data-off-text="LOCKED" class="form-check-input-switch">
                                                    </label>
                                                </div>
                                                @endif
                                                @else
                                                @endif
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- /main charts -->
        <div class="col-md-12 row">
            <!-- <div class="form-group">
                <button type="submit" name="btn_unlocked" class="btn btn-primary btn-sm">UNLOCKED SELECTED</button>
                <button type="submit" name="btn_locked" class="btn btn-danger btn-sm">LOCKED SELECTED</button>
            </div> -->
        </div>
        </form>
    </div>






    <!-- /content area -->
    @section('custom')
    <script>
    $('a.pp-available-pins').addClass('active');
    $('li.p-pin-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/dat$atables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script>
    var selected = new Array();

    function lock_status(lock_status, pin_id) {
        $.ajax({
            type: "GET",
            url: "{{url('backoffice/e-pin/product-pins/status')}}/{{Crypt::encryptString(@$data->customer_id)}}/" +
                pin_id + '/' + lock_status,
            success: function(data) {

            }
        })
    }

    $("#selectAll").click(function() {
        $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
    });
    </script>
    @endsection
    @endsection