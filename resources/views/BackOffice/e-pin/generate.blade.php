@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item ">{{$title}}</span>
                    <span class="breadcrumb-item active">Generate</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <form method="POST" action="{{url('backoffice/e-commerce-store/product-setup/products/distributor-pins')}}">
            @csrf
            <div class="row">
                <!-- Traffic sources -->

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="title ">Regular / Discounted Price Chart</h5>
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table ">
                                        <tbody>
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Distributor Price</th>
                                                    <th>SRP</th>
                                                    <th>Retailer</th>
                                                    <th>Stockist</th>
                                                    <th>Depot</th>
                                                    <th>Country Manager</th>
                                                </tr>
                                            </thead>
                                            @foreach($entries as $chart)
                                            <tr>
                                                <td>{{$chart->product_title}}</td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->distributor_price)}}</td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->srp)}}</td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->retailer)}}</td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->stockist_discount)}}
                                                </td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->depot_discount)}}
                                                </td>
                                                <td>{{$chart->currency_symbol}}{{number_format($chart->country_manager_discount)}}
                                                </td>
                                            </tr>

                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="card">
                        <div class="card-body ">
                            <div class="row">
                                <table class="table ">
                                    <tr>
                                        <th colspan=2 class="text-center">Distributor Details</th>
                                    </tr>
                                    <tr>
                                        <th>User ID</th>
                                        <td>{{$users[0]->id_number}}</td>
                                    </tr>

                                    <tr>
                                        <th>Username</th>
                                        <td>{{$users[0]->username}}</td>
                                    </tr>

                                    <tr>
                                        <th>Distributor Name</th>
                                        <td>{{$users[0]->firstname}} {{$users[0]->surname}}</td>
                                    </tr>

                                    <tr>
                                        <th>Member Type</th>
                                        <td>
                                            @if($users[0]->account_type == 0)
                                                Paid
                                            @elseif($users[0]->account_type == 1)
                                                CD
                                            @elseif($users[0]->account_type == 2)
                                                FREE
                                            @else 
                                                Floating
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="row mt-2">
                                <table class="table ">
                                    <tbody>
                                        <tr>
                                            <th colspan=2 class="text-center">Product Pins Details</th>
                                        </tr>
                                        <tr>
                                            <th>Total Items</th>
                                            <td class="text-right"><b class="total_items">0</b></td>
                                        </tr>

                                        <tr>
                                            <th>Total Amount</th>
                                            <td class="text-right">{{$entries[0]->currency_symbol}}<b class="total_amount">0.00</b></td>
                                        </tr>

                                        <tr>
                                            <th>Total Unilevel Point Value</th>
                                            <td class="text-right"><b class="total_upv">0.00</b></td>
                                        </tr>

                                        <tr>
                                            <th>Total Binary Point Value</th>
                                            <td class="text-right"><b class="total_bpv">0.00</b></td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8">
                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title ">Generate Product Pins</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-columned  ">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Product Title</th>
                                            <th>Product Code</th>
                                            <th style="width:1px">UPV</th>
                                            <th style="width:1px">BPV</th>
                                            <th style="width:1px">Price</th>
                                            <th style="width:1px">Stocks</th>
                                            <th style="width:150px" class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <?php $i = 1;?>
                                    <tbody>
                                        @foreach($entries as $value)
                                        <?php $count = $i++ ?>
                                        <tr>
                                            <td>{{$count}}</td>
                                            <td>{{$value->product_title}}</td>
                                            <td>{{$value->product_code}}</td>
                                            <td>{{number_format($value->unilevel_points,2)}}</td>
                                            <td>{{number_format($value->binary_points,2)}}</td>
                                            <td>{{$value->currency_symbol}}{{number_format($value->distributor_price,2)}}
                                            </td>
                                            <td>{{number_format($value->product_stocks)}}</td>
                                            <td>    
                                                <input style="width:100px" onkeypress="return isNumberKey(event)" onkeyup="TotalPins()" type="text" class="form-control text-center" name="quantity[]" id="quantity_{{$count}}">
                                                
                                                <input type="hidden" name="distributor_id" value="{{Crypt::encryptString($users[0]->id_number)}}">
                                                <input type="hidden" name="product_description_id[]" value="{{$value->pdid}}">
                                                <input type="hidden" name="country_code[]" value="{{$value->country}}">
                                                <input type="hidden" name="product_title[]" value="{{$value->product_title}}">
                                                <input type="hidden" name="product_code[]" value="{{$value->product_code}}">
                                                <input type="hidden" name="price[]" id="price_<?=$count?>" value="{{$value->distributor_price}}">
                                                <input type="hidden" name="upv[]" id="upv_<?=$count?>" value="{{$value->unilevel_points}}">
                                                <input type="hidden" name="bpv[]" id="bpv_<?=$count?>" value="{{$value->binary_points}}">
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row float-right">
                        <div class="col-lg-12 ">
                            <button type="submit" <?=isset($count) ?  : 'disabled'?> name="btn_generate_pins"
                                class="btn btn-primary ">Generate
                                Pins</button>
                        </div>
                    </div>
                </div>




            </div>
        </form>
        <!-- /main charts -->
    </div>

    <!-- /content area -->
    @section('custom')
    <script>
    $('a.pp-generate-distributor-pins').addClass('active');
    $('li.p-pin-must-open').addClass('nav-item-expanded nav-item-open');

    function TotalPins() {
        var count   = '<?=isset($count) ? $count : 0 ?>';
        var counter = 0
        var total   = 0;
        var upv     = 0;
        var bpv     = 0;
        var price   = 0;
        for (i = 1; i <= count; i++) {
            if ($('#quantity_' + i).val() != '') {
                price       = Number($('#price_' + i).val());
                quantity    = Number($('#quantity_' + i).val());
                upv         += Number($('#upv_' + i).val() * quantity);
                bpv         += Number($('#bpv_' + i).val() * quantity);
                counter     += Number($('#quantity_' + i).val());
                total       += Number(price) * Number(quantity);
            }
        }
        $('.total_items').html(numberWithCommas(counter))
        $('.total_amount').html(numberWithCommas(total.toFixed(2)))
        $('.total_upv').html(numberWithCommas(upv.toFixed(2)))
        $('.total_bpv').html(numberWithCommas(bpv.toFixed(2)))
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    </script>
    @endsection
    @endsection