@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <div class="row  justify-content-center">

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-plus2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{$used}}</h3>
                                    <span class="text-uppercase font-size-sm">Total Used</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-warning icon-minus2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{$unused}}</h3>
                                    <span class="text-uppercase font-size-sm">Total Unused</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <div class="btn-group" style="float:right;">
                        <div class="btn-group justify-content-center">
                            <a href="#" class="btn  btn-primary dropdown-toggle" data-toggle="dropdown">Generate Pin
                                Code</a>
                            <div class="dropdown-menu">
                                @foreach($countries as $country)
                                <a href="{{url('backoffice/e-pin/generate/free-slot-codes')}}/{{Crypt::encryptString($country->id)}}"
                                    class="dropdown-item">{{$country['name']}}</a>
                                @endforeach
                            </div>
                        </div>

                        <a href="#" class="btn btn-success" data-toggle="modal"
                            data-target="#search_paid_codes"><span>Search</span></a>
                        <a href="#" class="btn btn-warning"><span>Export To Excel</span></a>
                    </div>
                </div>
            </div>
            <!-- Traffic sources -->
            <div class="col-xl-12">

                <div class="card">

                    <div class="card-body ">
                        <div class="row">

                            <div class="col-md-12">

                                <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                    <li class="nav-item"><a href="#paid-pin" class="nav-link active"
                                            data-toggle="tab">Free Slot Pins History</a></li>
                                    <li class="nav-item"><a href="#activation-code" class="nav-link"
                                            data-toggle="tab">Activation Code Transaction History</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane show active" id="paid-pin">
                                        <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                    <th style="width:8%">Date Created</th>
                                                    <th>Customer ID</th>
                                                    <th>Order Number</th>
                                                    <th>Name</th>
                                                    <th style="width:1px">Items</th>
                                                    <th style="width:1px">Price</th>
                                                    <th>Generated By</th>
                                                    <th>Country</th>
                                                    <th class="text-center"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($pins_history as $free_pins)
                                                <tr>
                                                    <td>{{date('Y-m-d',strtotime($free_pins->created_at))}}</td>
                                                    <td>{{$free_pins->customer_id}}</td>
                                                    <td>{{$free_pins->order_number}}</td>
                                                    <td>{{$free_pins->full_name}} <a
                                                            href="">{{$free_pins->distributor_id}}</a></td>
                                                    <td>{{$free_pins->items}}</td>
                                                    <td>{{$free_pins->currency_symbol}}{{number_format($free_pins->total,2)}}
                                                    </td>
                                                    <td>{{$free_pins->created_by}}</td>
                                                    <td>{{$free_pins->name}}</td>
                                                    <!-- <td>{{$free_pins->created_at}}</td> -->
                                                    <td style="width:1px" class="text-center">
                                                        <a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i
                                                                class="icon-gear"></i></a>
                                                        <div class="dropdown-menu">
                                                            <a href="{{url('backoffice/e-pin/free-slot-codes/visit')}}/{{Crypt::encryptString($free_pins->customer_id)}}" class="dropdown-item"><i
                                                                    class="icon-eye"></i> View Pins</a>
                                                            <a href="{{url('backoffice/e-pin/free-slot-codes/destroy')}}/{{Crypt::encryptString($free_pins->customer_id)}}" class="dropdown-item"><i
                                                                    class="icon-trash"></i> Delete Pins</a>
                                                            <a href="{{url('backoffice/e-pin/free-slot-codes/print')}}/{{Crypt::encryptString($free_pins->customer_id)}}" class="dropdown-item"><i
                                                                    class="icon-printer"></i> Print Pins</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="tab-pane" id="activation-code">
                                        <table class="table table-columned datatable-responsive">
                                            <thead>
                                                <tr>
                                                    <th style="width:1px">#</th>
                                                    <th>Type</th>
                                                    <th style="width:1px" >Activation Code</th>
                                                    <th>Ordered By</th>
                                                    <th>Order ID</th>
                                                    <th>Used By</th>
                                                    <th>Date Created</th>
                                                    <th>Date Used</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1 ?>
                                                @foreach($used_fs_pins as $pins) 
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$pins->entry}}</td>
                                                        <td>{{$pins->activation_code}}</td>
                                                        <td>{{$pins->full_name}} {{$pins->distributor_id}}</td>
                                                        <td>{{$pins->customer_id}}</td>
                                                        <td>{{$pins->used_by}}</td>
                                                        <td>{{$pins->created_at}}</td>
                                                        <td>{{$pins->date_used}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <div class="modal" id="search_paid_codes" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Search Activation Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-light btn-block daterange-predefined">
                                    <i class="icon-calendar22 mr-2"></i>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Filter</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
    $('a.e-free-slot-codes').addClass('active');
    $('li.e-pin-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });
    </script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    
    @endsection
    @endsection