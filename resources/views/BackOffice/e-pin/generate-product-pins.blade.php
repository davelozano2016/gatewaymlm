@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Code Management</span>
                    <span class="breadcrumb-item">Product Pins</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>Name</th>
                                        <th class="text-center">Used</th>
                                        <th class="text-center">Unused</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1?>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td style="width:1px"><a href="javascript:void(0)">{{$user->id_number}}</a></td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->firstname}} {{$user->surname}}</td>
                                        <td style="width:1px" class="text-center">{{$used[$user->id_number]}}</td>
                                        <td style="width:1px" class="text-center">{{$unused[$user->id_number]}}</td>
                                        <td style="width:1px" class="text-center">
                                            <a class="list-icons-item" href="javascript:void(0)"
                                                data-toggle="dropdown"><i class="icon-gear"></i></a>
                                            <div class="dropdown-menu">
                                                <a href="{{url('backoffice/e-pin/product-pins/products/generate/'.Crypt::encryptString($user->id_number).'/'.Crypt::encryptString($user->country))}}"
                                                    class="dropdown-item"><i class="icon icon-plus2"></i> Generate Product Pins</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->
    @section('custom')
    <script src="{{asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script>
    $('a.pp-generate-distributor-pins').addClass('active');
    $('li.p-pin-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
     @if(Session::has('message'))
     <script>
         notification('{{Session::get("message")}}','info') 
     </script>
     @endif
    @endsection
    @endsection