@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">
    <form method="POST" action="{{url('backoffice/e-pin/free-slot-codes/mass-status-update')}}">
        @csrf
        <input type="hidden" name="customer_id" value="{{Crypt::encryptString($details[0]->customer_id)}}">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4>{{$title}}</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i
                                class="icon-home2 mr-2"></i>
                            Dashboard</a>
                        <span class="breadcrumb-item">E-Pin</span>
                        <span class="breadcrumb-item ">Free Slot Codes</span>
                        <span class="breadcrumb-item active">{{$title}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">

            <!-- Main charts -->
            <div class="row">
                <!-- Traffic sources -->
                <div class="col-12 col-md-12">
                    <div class="card">
                        <div class=" ">
                            <div class="table-responsive">
                                <table class="table ">
                                    <tr>
                                        <th style="width:40%">Customer Number</th>
                                        <td>{{$details[0]->customer_id}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Full Name</th>
                                        <td>{{$details[0]->full_name}} <a href="">{{$details[0]->distributor_id}}</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Order Number</th>
                                        <td>{{$details[0]->order_number}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Date Processed</th>
                                        <td>{{$details[0]->date_processed}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Date Expiration</th>
                                        <td>{{$details[0]->date_expiration}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Total Entry Packages</th>
                                        <td>{{$details[0]->items}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Total Price</th>
                                        <td>{{$details[0]->currency_symbol}}{{number_format($details[0]->total,2)}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">IP Address</th>
                                        <td>{{$details[0]->registered_ip}}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:40%">Representative</th>
                                        <td>{{$details[0]->created_by}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12">
                    <div class="card">
                        <div class=" ">
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th style="width:1px">Entry</th>
                                            <th>Package</th>
                                            <th style="width:1px">Items</th>
                                            <th class="text-center" style="width:1px">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($packages as $package)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$package->entry}}</td>
                                            <td>{{$package->package_name}}</td>
                                            <td class="text-center">{{$package->items}}</td>
                                            <td class="text-right">
                                                {{$package->currency_symbol}}{{number_format($package->total,2)}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12">
                    <div class="card">
                        <div class=" ">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th style="width:1px"><input id="selectAll"
                                                    class="form-check-input-styled-primary" type="checkbox"></th>
                                            <th style="width:1px">Entry</th>
                                            <th>Package</th>
                                            <th>Activation Code</th>
                                            <th class="text-center" style="width:1px">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($pins_history as $free_pins)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><input type="checkbox" name="selected[]"
                                                    value="{{$free_pins->registration_codes_id}}"></td>
                                            <td>{{$free_pins->entry}}</td>
                                            <td>{{$free_pins->package_name}}</td>
                                            <td>{{$free_pins->activation_code}}</td>
                                            <td>
                                                @if($free_pins->status == 0)
                                                <a class="badge badge-success text-white text-uppercase">Unused</a>
                                                @else
                                                <a class="badge badge-info text-white text-uppercase">Used</a>
                                                @endif
                                            </td>
                                            <td style="width:1%">
                                                @if($free_pins->status == 0)
                                                @if($free_pins->lock_status == 0)
                                                <div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox"
                                                            onchange="lock_status('{{Crypt::encryptString($free_pins->lock_status)}}','{{Crypt::encryptString($free_pins->registration_codes_id)}}')"
                                                            id="lock_status_{{$free_pins->registration_codes_id}}"
                                                            data-on-color="primary" data-off-color="danger"
                                                            data-on-text="UNLOCKED" data-off-text="LOCKED"
                                                            class="form-check-input-switch" checked>
                                                    </label>
                                                </div>
                                                @else
                                                <div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox"
                                                            onchange="lock_status('{{Crypt::encryptString($free_pins->lock_status)}}','{{Crypt::encryptString($free_pins->registration_codes_id)}}')"
                                                            id="lock_status_{{$free_pins->registration_codes_id}}"
                                                            data-on-color="primary" data-off-color="danger"
                                                            data-on-text="UNLOCKED" data-off-text="LOCKED"
                                                            class="form-check-input-switch">
                                                    </label>
                                                </div>
                                                @endif
                                                @else
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /main charts -->
            <div class="col-md-12 row">
                <div class="form-group">
                    <button type="submit" name="btn_unlocked" class="btn btn-primary btn-sm">UNLOCKED SELECTED</button>
                    <button type="submit" name="btn_locked" class="btn btn-danger btn-sm">LOCKED SELECTED</button>
                </div>
            </div>
    </form>
</div>

<!-- /content area -->
@section('custom')
<script>
$('a.e-free-slot-codes').addClass('active');
$('li.e-pin-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script>
function lock_status(lock_status, registration_codes_id) {
    $.ajax({
        type: "GET",
        url: "{{url('backoffice/e-pin/paid-codes/status')}}/{{Crypt::encryptString($free_pins->customer_id)}}/" +
            registration_codes_id + '/' + lock_status,
        success: function(data) {

        }
    })
}

$("#selectAll").click(function() {
    $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
});
</script>
@endsection
@endsection