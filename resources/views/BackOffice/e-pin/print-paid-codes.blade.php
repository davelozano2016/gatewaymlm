@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item ">Paid Codes</span>
                    <span class="breadcrumb-item ">Print</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <div class="row" id="printable">
            <div class="col-12 col-md-12">
                <!-- Invoice template -->
                <div style="border:none !important" class="card">
                    <div style="border:none !important" class="card-header bg-transparent header-elements-inline">
                        <h6 class="card-title">INVOICE {{$title}}</h6>
                        <div class="header-elements">
                            <button type="button" class="btn print avoid-this btn-light btn-sm ml-3"><i
                                    class="icon-printer mr-2"></i>
                                Print</button>
                        </div>
                    </div>

                    <div style="border:none !important" class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mb-4">
                                    <img src="{{url('assets/images/logo.png')}}" class="mb-3 mt-2" alt=""
                                        style="width: 120px;">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="mb-4">
                                    <div class="text-sm-right">
                                        <h4 class="text-primary mb-2 mt-md-2">{{$details[0]->order_number}}</h4>
                                        <ul class="list list-unstyled mb-0">
                                            <li>Date / Time Printed: <span class="">{{date('F d, Y g:i A')}}</span></li>
                                            <li>Date / Time Processed: <span
                                                    class="">{{date('F j, Y g:i A',strtotime($details[0]->date_processed))}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-md-flex flex-md-wrap">
                            <div class="mb-4 mb-md-2">
                                <h5 class="text-muted">Distributor Details:</h5>
                                <div class="d-flex flex-wrap wmin-md-400">
                                    <ul class="list list-unstyled mb-0">
                                        <li>Distributor:</li>
                                        <li>Email Address:</li>
                                        <li>Contact Number:</li>
                                        <li>Address:</li>
                                    </ul>

                                    <ul class="list list-unstyled text-right mb-0 ml-auto">
                                        <li>{{$details[0]->full_name}}</li>
                                        <li>NA</li>
                                        <li>NA</li>
                                        <li>NA</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="mb-2 ml-auto">
                                <h5 class="text-muted">Order Details:</h5>
                                <div class="d-flex flex-wrap wmin-md-400">
                                    <ul class="list list-unstyled mb-0">
                                        <li>Country:</li>
                                        <li>IP Address:</li>
                                        <li>Representative:</li>
                                        <li>Total Entry Packages:</li>
                                        <li>Total Price:</li>
                                    </ul>

                                    <ul class="list list-unstyled text-right mb-0 ml-auto">
                                        <li>{{$details[0]->name}}</li>
                                        <li>{{$details[0]->registered_ip}}</li>
                                        <li>{{$details[0]->created_by}}</li>
                                        <li><span class="">{{$details[0]->items}}</span></li>
                                        <li><span
                                                class="">{{$details[0]->currency_symbol}}{{number_format($details[0]->total, 2)}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="text-center mb-3">ACTIVATION CODE SUMMARY</h5>
                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Entry</th>
                                    <th>Package</th>
                                    <th>Activation Code</th>
                                    <th>Date of Expiration</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($pins_history as $paid_pins)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$paid_pins->entry}}</td>
                                    <td>{{$paid_pins->package_name}}</td>
                                    <td>{{$paid_pins->activation_code}}</td>
                                    <td>{{$paid_pins->date_expiration}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class=" mt-4 mb-4 text-center">
                        <div class="row">
                            <div class="col-md-4">
                                <hr>
                                Released By
                            </div>

                            <div class="col-md-4">
                                <hr>
                                Checked By
                            </div>

                            <div class="col-md-4">
                                <hr>
                                Signature Over Printed Name
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /invoice template -->
            </div>
        </div>
    </div>

    <!-- /content area -->
    @section('custom')
    <script>
    $('a.e-paid-codes').addClass('active');
    $('li.e-pin-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js"></script>
    <style type="text/css" media="print">
    @page {
        size: portrait;
    }
    </style>
    <script>
    $(function() {
        $("#printable").find('.print').on('click', function() {
            $("#printable").print({

                // Use Global styles
                globalStyles: true,

                // Add link with attrbute media=print
                mediaPrint: false,

                //Print in a hidden iframe
                iframe: false,

                // Manually add form values
                manuallyCopyFormValues: false,

                // resolves after print and restructure the code for better maintainability
                deferred: $.Deferred(),

                // timeout
                timeout: 1,

                noPrintSelector: ".avoid-this",

                doctype: '<!doctype html>'

            });
        });
    });
    </script>
    @endsection
    @endsection