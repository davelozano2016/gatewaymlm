@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>Generate {{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">E-Pin</span>
                    <span class="breadcrumb-item ">{{$title}}</span>
                    <span class="breadcrumb-item active">Generate</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <form method="POST" data-parsley-validate action="{{url('backoffice/e-pin/free-slot-codes/create')}}">
            @csrf
            <div class="row">
                <!-- Traffic sources -->

                <div class="col-12 col-md-12">

                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title">Distributor Details</div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Customer
                                    ID:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="customer_id" value="C{{$random}}" readonly
                                        class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Distributor
                                    ID:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="distributor_id" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Order
                                    Number:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="order_number" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Full
                                    Name:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="full_name" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Password:</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12">
                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title">Generate Free Slot Codes</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-columned">
                                    <thead>
                                        <tr>
                                            <th>Entry Package Name</th>
                                            <th>Price</th>
                                            <th style="width:10%">Quantity</th>
                                        </tr>
                                    </thead>
                                    <?php $i = 1;?>
                                    @foreach($entries as $entry => $value)
                                    <tbody>
                                        @foreach($packages as $key => $package)
                                        @if($value->id == $package->entries_id)
                                        <?php $count = $i++?>

                                        <tr>
                                            <th>{{$value->entry}} {{$package->package_name}} </th>
                                            <td>{{number_format($value->price,2)}} {{$value->currency_code}}</td>
                                            <td>
                                                <input type="text" name="quantity[]" id="quantity_<?=$count?>"
                                                    onkeyup="TotalPins()" onkeypress="return isNumberKey(event)"
                                                    class="text-center form-control">
                                                <input type="hidden" name="price[]" id="price_<?=$count?>"
                                                    value="{{$value->price}}">
                                                <input type="hidden" name="package_type[]" value="{{$value->entry}}">
                                                <input type="hidden" name="packages_id[]" value="{{$package->id}}">
                                                <input type="hidden" name="bpv[]" value="0">
                                                <input type="hidden" name="country_code" value="{{$value->code}}">
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                    @endforeach
                                    <tfoot>
                                        <tr>
                                            <th class="text-right" colspan=2>Total Pins:</th>
                                            <td><span id="show_pins">0</span> Pins</td>
                                        </tr>
                                        <tr>
                                            <th class="text-right" colspan=2>Total Amount:</th>
                                            <td> <span id="show_amount">0</span> {{$value->currency_code}}</td>
                                        </tr>
                                    </tfoot>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row float-right">
                        <div class="col-lg-12 ">
                            <button type="submit" <?=isset($count) ?  : 'disabled'?> name="btn_generate_pins"
                                class="btn btn-primary">Generate
                                Pins</button>
                        </div>
                    </div>
                </div>


            </div>
        </form>
        <!-- /main charts -->
    </div>

    <!-- /content area -->
    @section('custom')
    <script>
    $('a.e-free-slot-codes').addClass('active');
    $('li.e-pin-must-open').addClass('nav-item-expanded nav-item-open');

    function TotalPins() {
        var count = '<?=isset($count) ? $count : 0?>';
        var counter = 0
        var total = 0;
        var price = 0;
        for (i = 1; i <= count; i++) {
            if ($('#quantity_' + i).val() != '') {
                price = Number($('#price_' + i).val());
                quantity = Number($('#quantity_' + i).val());
                counter += Number($('#quantity_' + i).val());
                total += Number(price) * Number(quantity);
            }
        }
        $('#show_pins').html(counter)
        $('#show_amount').html(total)
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    </script>
    @endsection
    @endsection