@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Tools</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">

        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Add News</span>
                </div>
                <div class="card-body">
                    <form>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">News Title:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Select a File:</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Description:</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" cols="30" rows="10" style="resize:none;"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>    
        </div>

        <div class="col-xl-8">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">News Lists</span>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>News Title</th>
                                            <th>Description</th>
                                            <th>User Created</th>
                                            <th>Date Created</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{ $i }}.</td>
                                            <td>Title {{$i}} </td>
                                            <td><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#view-description">View Description</button></td>
                                            <td>John Doe (username{{$i}}) </td>
                                            <td>{{ date('M j, Y g:i:s A')}}</td>
                                            <td>
                                                <div class="btn-group">
                                                <a href="{{ url('backoffice/tools/news?c=edit&nid='.$i) }}" class="btn btn-info"><i class="icon-pencil7"></i></a>
                                                <button type="button" class="btn btn-danger"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>

<div class="modal fade" id="view-description" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header bg-slate">
            <h5 class="modal-title" id="exampleModalLabel">View Description (Title)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Description here....
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn bg-slate" data-dismiss="modal" aria-label="Close">Close</button>
        </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.t-news').addClass('active');
    $('li.tools-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection