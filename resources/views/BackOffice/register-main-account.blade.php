@extends('layouts.BackOffice.app')
@section('container')
<style>
input[type=radio]:checked+label>img {
    background-image: linear-gradient(16deg, #3f51b5, #3d696a);
}

.input-hidden {
    position: absolute;
    left: -9999px;
}

.card-img-actions label#product-error {
    position: absolute;
}
</style>
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->
    <div class="content">
        <form method="POST" action="{{ url('backoffice/register/store') }}">
            @csrf
            <div class="row">
                <div class="col-12 col-md-12">

                    <div class="card">
                        <div class="card-body ">
                            <div class="card-title">Distributor Initial Details</div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Surname</label>
                                <div class="col-lg-10">
                                    <input type="text" name="surname" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">First Name</label>
                                <div class="col-lg-10">
                                    <input type="text" name="firstname" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Middle Name</label>
                                <div class="col-lg-10">
                                    <input type="text" name="middlename" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Email Address</label>
                                <div class="col-lg-10">
                                    <input type="text" name="email" class="form-control">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header header-elements-inline ">
                            <h6 class="card-title">Distributor Network Details</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Sponsor ID</label>
                                <div class="col-lg-10">
                                    <input type="text" name="sponsor_id" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Placement ID:</label>
                                <div class="col-lg-10">
                                    <input type="text" name="upline_id" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Position:</label>
                                <div class="col-lg-10">
                                    <select name="position" class="form-control-select2 form-control">
                                        @if($tophead == 0)
                                            <option value="2">Top</option>
                                        @else if
                                            <option value="0">Left</option>
                                            <option value="1">Right</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Country:</label>
                                <div class="col-lg-10">
                                    <select name="country" class="form-control-select2 form-control">
                                        @foreach($countries as $country)
                                        <option value="{{$country->code}}"> {{$country->name}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> -->

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Activation Code</label>
                                <div class="col-lg-10">
                                    <input type="text" name="activation_code" class="form-control">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header header-elements-inline ">
                            <h6 class="card-title">Distributor Account Details</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Username</label>
                                <div class="col-lg-10">
                                    <input type="text" name="username" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Password:</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary float-right">Create Distributor</button>
            </div>
        </form>
    </div>

    @section('custom')
    <script>
    $('a.register').addClass('active');
    </script>
    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/inputs/inputmask.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_wizard.js') }}"></script>

    @endsection
    @endsection