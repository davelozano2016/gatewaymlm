@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h5>Distributor Details</h5>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Profile Management</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                    <span class="breadcrumb-item active">{{$users[0]->firstname}} {{$users[0]->middlename}} {{$users[0]->surname}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Cover area -->
    <div class="profile-cover">
        <div class="profile-cover-img"
            style="background-image: url(https://root-nation.com/wp-content/uploads/2019/10/How-to-Start-Forex-Trading-01.jpeg)">
        </div>
        <div class="media align-items-center text-center text-md-left flex-column flex-md-row m-0">
            <div class="mr-md-3 mb-2 mb-md-0">
                <a href="#" class="profile-thumb">
                    <img src="{{asset('assets/images/mikewooting.jpg')}}" class="border-white rounded-circle" width="48"
                        height="48" alt="">
                </a>
            </div>

            <div class="media-body text-white ">
                <h1 class="mb-0">{{@$users[0]->firstname}} {{@$users[0]->surname}}</h1>
                <span class="d-block">{!!$users[0]->account_type == 0 ? '<label class="badge badge-success">Main Account</label>' : '<label class="badge badge-info">Sub Account</label>'!!}</span>
            </div>
        </div>
    </div>
    <!-- /cover area -->


    <!-- Profile navigation -->
    <div class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-second">
                Distributor Information
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-second">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a href="#distributor_information" class="navbar-nav-link active" data-toggle="tab">
                        Distributor Information
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#distributor_sub_accounts" class="navbar-nav-link" data-toggle="tab">
                        Distributor Sub Accounts
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#package_pins" class="navbar-nav-link" data-toggle="tab">
                        Package Pins
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#product_pins" class="navbar-nav-link" data-toggle="tab">
                        Product Pins
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#ewallet_soa" class="navbar-nav-link" data-toggle="tab">
                        E-Wallet SOA
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#purchase_wallet" class="navbar-nav-link" data-toggle="tab">
                        Purchase Wallet
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#income_to_ewallet" class="navbar-nav-link" data-toggle="tab">
                        Income to E-Wallet
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#encashment_history" class="navbar-nav-link" data-toggle="tab">
                        Encashment History
                    </a>
                </li>

            </ul>

            
        </div>
    </div>
    <!-- /profile navigation -->


    <!-- Content area -->
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row">

            <!-- Left content -->
            <div class="tab-content w-100 order-2">
                <div class="tab-pane active show" id="distributor_information">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" data-parsley-validate action="{{url('backoffice/profile-management/members/update_distributor/'.Crypt::encryptString($users[0]->users_id))}}">
                                @csrf
                                <h5 class="title ">Personal Information</h5>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">First Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="firstname" value="{{@$users[0]->firstname}}" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Middle Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="middlename" value="{{@$users[0]->middlename}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Last Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="surname" value="{{@$users[0]->surname}}" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Nickname</label>
                                    <div class="col-md-10">
                                        <input type="text" value="{{@$users[0]->nickname}}" name="nickname" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Birthdate</label>
                                    <div class="col-md-10">
                                        <input type="date" value="{{@$users[0]->birthdate}}" name="birthdate" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Civil Status</label>
                                    <div class="col-md-10">
                                        <select name="civil_status" class="form-control form-control-select2">
                                            <option value="Single" {{@$users[0]->civil_status == 'Single' ? 'selected' : ''}}>Single</option>
                                            <option value="Married" {{@$users[0]->civil_status == 'Married' ? 'selected' : ''}}>Married</option>
                                            <option value="Widowed" {{@$users[0]->civil_status == 'Widowed' ? 'selected' : ''}}>Widowed</option>
                                            <option value="Divorced" {{@$users[0]->civil_status == 'Divorced' ? 'selected' : ''}}>Divorced</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Gender</label>
                                    <div class="col-md-10">
                                        <select name="gender" class="form-control form-control-select2">
                                            <option value="Male" {{@$users[0]->gender == 'Male' ? 'selected' : ''}}>Male</option>
                                            <option value="Female" {{@$users[0]->gender == 'Female' ? 'selected' : ''}}>Female</option>
                                            <option value="Others" {{@$users[0]->gender == 'Others' ? 'selected' : ''}}>Others</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Nationality</label>
                                    <div class="col-md-10">
                                        <input type="text" name="nationality" value="{{@$users[0]->nationality}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Tax Identification Number</label>
                                    <div class="col-md-10">
                                        <input type="text" name="tin" value="{{@$users[0]->tin}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Address Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Country</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly value="{{@$users[0]->country_name}}" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">State / Province</label>
                                    <div class="col-md-10">
                                        <input type="text" name="province" value="{{@$users[0]->province}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">City</label>
                                    <div class="col-md-10">
                                        <input type="text" name="city" value="{{@$users[0]->city}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Address</label>
                                    <div class="col-md-10">
                                        <input type="text" name="address" value="{{@$users[0]->address}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Zip Code</label>
                                    <div class="col-md-10">
                                        <input type="text" name="zipcode" value="{{@$users[0]->zipcode}}" class="form-control">
                                    </div>
                                </div>
                                <h5 class="title ">Contact Information</h5>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Email Address</label>
                                    <div class="col-md-10">
                                        <input type="email" value="{{@$users[0]->email}}" name="email"
                                            class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Contact Number</label>
                                    <div class="col-md-10">
                                        <input type="text" name="contact_number" value="{{@$users[0]->contact_number}}" readonly class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Landline</label>
                                    <div class="col-md-10">
                                        <input type="text" name="landline" value="{{@$users[0]->landline}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Fax</label>
                                    <div class="col-md-10">
                                        <input type="text" name="fax" value="{{@$users[0]->fax}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Beneficiary Information</h5>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Full Name</label>
                                    <div class="col-md-10">
                                        <input type="text" name="name" value="{{@$users[0]->name}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Relationship</label>
                                    <div class="col-md-10">
                                        <input type="text" name="relationship" value="{{@$users[0]->relationship}}" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Bank Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Bank Name</label>
                                    <div class="col-md-10">
                                        <select name="banks_id" class="form-control form-control-select2" id="">
                                            @foreach($banks as $bank)
                                                <option value="{{Crypt::encryptString($bank->id)}}"
                                                @if($bank->id == $users[0]->banks_id)
                                                    selected
                                                @endif    
                                                >{{@$bank->banks}}</option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Branch</label>
                                    <div class="col-md-10">
                                        <input type="text" name="branch" value="{{@$users[0]->branch}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Account Name</label>
                                    <div class="col-md-10">
                                        <input type="text" value="{{@$users[0]->account_name}}" name="account_name" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Account Number</label>
                                    <div class="col-md-10">
                                        <input type="text" value="{{@$users[0]->account_number}}" name="account_number" class="form-control">
                                    </div>
                                </div>

                                <h5 class="title ">Social Media Information</h5>
                                <?php $i=0;?>
                                @foreach($social_media as $sm)
                                    <input type="hidden" name="social_media_id[]" value="{{Crypt::encryptString($sm->id)}}">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">{{@$sm->social_media_name}}</label>
                                        <div class="col-md-10">
                                            <input type="text" name="url[]" value="{{@$social_media_accounts[$i]->url}}" class="form-control">
                                        </div>
                                    </div>
                                    <?php $i++?>
                                @endforeach

                                <h5 class="title ">Account Information</h5>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Username</label>
                                    <div class="col-md-10">
                                        <input type="text" required name="username" value="{{@$users[0]->username}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Password</label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <input type="password" required  value="{{Crypt::decryptString($users[0]->password)}}"
                                                id="password" name="password" class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text"><a style="cursor:pointer"><i
                                                            onclick="unmask()" id="unmask"
                                                            class="icon-eye"></i></a></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Status</label>
                                    <div class="col-md-10">
                                        <select name="status" class="form-control-select2 form-control" id="">
                                            <option value="0" <?= $users[0]->status == 0 ? 'selected' : ''?>>Active
                                            </option>
                                            <option value="1" <?= $users[0]->status == 1 ? 'selected' : ''?>>Inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Security Code</label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <input type="text" id="clipboard" value="{{@$users[0]->security_code}}"
                                                readonly class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text"><a style="cursor:pointer"><i
                                                            onclick="clipboard()" class="icon-copy4"></i></a></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm float-right">Update Account Information</button>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="distributor_sub_accounts">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th style="width:1px">ID</th>
                                            <th>Username</th>
                                            <th>Upline</th>
                                            <th style="width:1px">Position</th>
                                            <th style="width:1px">Activation Code</th>
                                            <th style="width:1px">Type</th>
                                            <th>Package</th>
                                            <th style="width:1px">Country</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1?>
                                        @foreach($sub_accounts as $sub_account)
                                        <tr>
                                            <td>{{@$i++}}</td>
                                            <td>{{@$sub_account->id_number}}</td>
                                            <td>{{@$sub_account->username}}  </td>
                                            <td>{{@$sub_account->firstname}} <a href="{{url('backoffice/profile-management/members')}}/{{Crypt::encryptString($sub_account->id_number)}}"">{{@$sub_account->upline_id}}</a></td>
                                            <td>
                                                @if($sub_account->position == 0)
                                                LEFT
                                                @elseif($sub_account->position == 1)
                                                RIGHT
                                                @elseif($sub_account->position == 2)
                                                TOP
                                                @endif
                                            </td>
                                            <td>{{@$sub_account->activation_code}}</td>
                                            <td>
                                                @if($sub_account->status_type == 0)
                                                PAID
                                                @elseif($sub_account->status_type == 1)
                                                CD
                                                @elseif($sub_account->status_type == 2)
                                                FS
                                                @endif
                                            </td>
                                            <td>{{@$sub_account->entry}}</td>
                                            <td>{{@$sub_account->country}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="package_pins">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Order Number</th>
                                            <th style="width:1px">Items</th>
                                            <th style="width:1px">Price</th>
                                            <th>Country</th>
                                            <th style="width:1px">Type</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pins_history as $history)
                                        <tr>
                                            <td>{{date('Y-m-d',strtotime($history->created_at))}}</td>
                                            <td>{{@$history->order_number}}</td>
                                            <td>{{@$history->items}}</td>
                                            <td>{{@$history->currency_symbol}}{{number_format($history->total,2)}}
                                            </td>
                                            <td>{{@$history->name}}</td>
                                            <td style="width:1px">
                                                @if($history->type == 0)
                                                PAID
                                                @elseif($history->type == 1)
                                                CD
                                                @elseif($history->type == 2)
                                                FS
                                                @endif
                                            </td>
                                            <!-- <td>{{@$history->created_at}}</td> -->
                                            <td style="width:1px" class="text-center">
                                                <a class="list-icons-item" href="javascript:void(0)"
                                                    data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                <div class="dropdown-menu">
                                                    <a href="{{url('backoffice/e-pin/paid-codes/visit')}}/{{Crypt::encryptString($history->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-eye"></i> View Package Pins</a>

                                                    <a href="{{url('backoffice/e-pin/paid-codes/destroy')}}/{{Crypt::encryptString($history->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-trash"></i> Delete Package Pins</a>

                                                    <a href="{{url('backoffice/e-pin/paid-codes/print')}}/{{Crypt::encryptString($history->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-printer"></i> Print Package Pins
                                                        Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="product_pins">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Order Number</th>
                                            <th>Status</th>
                                            <th>Total</th>
                                            <th>Items</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($product_pins as $pins)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$pins->order_number}}</td>
                                            <td>{{$pins->order_status}}</td>
                                            <td style="width:1%">{{$total[$pins->pin_id]->total}}</td>
                                            <td style="width:1%">{{$items[$pins->pin_id]->items}}</td>
                                            <td style="width:1px" class="text-center">
                                                <a class="list-icons-item" href="javascript:void(0)"
                                                    data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                <div class="dropdown-menu">
                                                    <a href="{{url('backoffice/e-pin/product-pins/visit')}}/{{Crypt::encryptString($pins->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-eye"></i> View Product Pins</a>

                                                    <a href="{{url('backoffice/e-pin/product-pins/destroy')}}/{{Crypt::encryptString($pins->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-trash"></i> Delete Product Pins</a>

                                                    <a href="{{url('backoffice/e-pin/product-pins/print')}}/{{Crypt::encryptString($pins->customer_id)}}"
                                                        class="dropdown-item"><i class="icon-printer"></i> Print Product Pins
                                                        Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ewallet_soa">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Category</th>
                                            <th>Description</th>
                                            <th>Transaction Date</th>
                                            <th>Reference</th>
                                            <th>Value ({{$users[0]->currency_symbol}})</th>
                                            <th>Balance ({{$users[0]->currency_symbol}})</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;?>
                                        @foreach($wallet_logs as $wallet)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$wallet->category}}</td>
                                            <td>{{$wallet->description}}</td>
                                            <td>{{$wallet->created_at}}</td>
                                            <td>{{$wallet->reference}}</td>
                                            <td class="text-right">
                                                @if($wallet->status == 0) 
                                                    <label class="text-success">+{{number_format($wallet->value,2)}}</label>
                                                @else
                                                    <label class="text-danger">-{{number_format($wallet->value,2)}}</label>
                                                @endif
                                            </td>
                                            <td><strong>{{number_format($wallet->current_balance,2)}}</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="purchase_wallet">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Transaction Date</th>
                                            <th>Amount</th>
                                            <th>Type</th>
                                            <th>Note</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php 
                                        $i=1;
                                        @endphp
                                        @foreach($wallets as $wallet)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$wallet->created_at}}</td>
                                            <td>{{$users[0]->currency_symbol}}{{number_format($wallet->amount,2)}}</td>
                                            <td>{{$wallet->deposit_via}}</td>
                                            <td>{{$wallet->note == null ? 'Not Available' : $wallet->note}}</td>
                                            <td>
                                                @if($wallet->status == 'Completed')
                                                    <label class="badge badge-success">Completed</label>
                                                @elseif($wallet->status == 'Processing')
                                                    <label class="badge badge-warning">Processing</label>
                                                @else
                                                    <label class="badge badge-danger">Cancelled</label>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-400 border-0 shadow-0 order-1 order-lg-2 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Navigation -->
                    <div class="card">
                        <div class="card-body ">
                            <h5 class="title ">Distributor Details</h5>
                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    User ID:
                                </div>
                                <div class="col-md-6 text-right">
                                    {{@$users[0]->id_number}}
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Rank:
                                </div>
                                <div class="col-md-6 text-right">
                                    {{!empty($ranks->ranks) ? $ranks->ranks : 'Bronze'}}
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Sponsor Name:
                                </div>
                                <div class="col-md-6 text-right ">
                                    {{@$data[0]->firstname}} {{@$data[0]->surname}}
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Position:
                                </div>
                                <div class="col-md-6 text-right">
                                    @if($users[0]->position == 0)
                                    LEFT
                                    @elseif($users[0]->position == 1)
                                    RIGHT
                                    @elseif($users[0]->position == 2)
                                    TOP
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Package Type:
                                </div>
                                <div class="col-md-6  text-right">
                                    {{@$details[0]->entry}}
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Mermbership Type:
                                </div>
                                <div class="col-md-6 text-right">
                                    @if($details[0]->type == 0)
                                    PAID
                                    @elseif($details[0]->type == 1)
                                    CD
                                    @elseif($details[0]->type == 2)
                                    FS
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 ">
                                    Activation Code:
                                </div>
                                <div class="col-md-6 text-right">
                                    {{@$users[0]->activation_code}}
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="card card-body">
                    <div class="media">
                        <div class="align-self-center">
                            <i class="icon-wallet icon-3x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">{{@$users[0]->currency_symbol}}{{number_format($users[0]->fund,2)}}</h3>
                            <span class=" font-size-sm text-muted">E-wallet Balance</span>
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <div class="row row-tile no-gutters">
                        <div class="col-6">
                            <button type="button" class="btn btn-primary btn-block btn-float m-0">
                                <i class="icon-coin-dollar icon-2x"></i>
                                <span>Direct Referral</span>
                            </button>

                            <button type="button" class="btn btn-warning btn-block btn-float m-0">
                                <i class="icon-coin-dollar  icon-2x"></i>
                                <span>Royale Bonus</span>
                            </button>
                        </div>
                        
                        <div class="col-6">
                            <button type="button" class="btn btn-info btn-block btn-float m-0">
                                <i class="icon-coin-dollar text-pink icon-2x"></i>
                                <span>Pairing Bonus</span>
                            </button>

                            <button type="button" class="btn btn-success btn-block btn-float m-0">
                                <i class="icon-coin-dollar text-success icon-2x"></i>
                                <span>Unilevel Bonus</span>
                            </button>
                        </div>
                    </div>
                </div>


                    <!-- /navigation -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
    <!-- /content area -->
    @section('custom')

    <script src="{{ asset('assets/js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_wizard.js') }}"></script>
    
    <script>
    $('a.pm-member-list').addClass('active');
    $('li.profile-management-must-open').addClass('nav-item-expanded nav-item-open');
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });

    function unmask() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmask').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmask').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function unmasksub() {
        var x = document.getElementById("sub_account_password");
        if (x.type === "password") {
            x.type = "text";
            $('#unmasksub').removeClass('icon-eye').addClass('icon-eye-blocked')
        } else {
            x.type = "password";
            $('#unmasksub').removeClass('icon-eye-blocked').addClass('icon-eye')
        }
    }

    function clipboard() {
        /* Get the text field */
        var copyText = document.getElementById("clipboard");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }
    </script>
    @endsection
    @endsection