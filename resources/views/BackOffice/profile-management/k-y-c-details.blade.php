@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Profile Management</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="">Select Category</label>
                                            <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                <option value="Any">Any</option>
                                                <option value="Passport Test">Passport Test</option>
                                                <option value="Driving Liscense">Driving Liscense</option>
                                                <option value="ID Proof">ID Proof</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="">Status</label>
                                            <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                <option value="Pending">Pending</option>
                                                <option value="Rejected">Rejected</option>
                                                <option value="Approved">Approved</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="">Username</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">&nbsp;</label>
                                        <div class="form-group">
                                            <button class="btn bg-slate"><i class="icon-search4"></i></button>
                                            <button class="btn btn-warning"><i class="icon-reset"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-basic">
                                   <thead>
                                        <tr>
                                            <th>Member Name</th>
                                            <th>Category</th>
                                            <th>Status</th>
                                            <th>View</th>
                                            <th>Action</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        <tr>
                                            <td>John David Lozano </td>
                                            <td>Password Test</td>
                                            <td><span class="badge badge-success"> Approved</span></td>
                                            <td><button type="submit" class="btn bg-indigo-700 btn-sm" data-toggle="modal" data-target="#view-details"><i class="icon icon-eye4"></i></button></td>
                                            <td>NA</td>
                                        </tr>
                                        <tr>
                                            <td>John David Lozano </td>
                                            <td>Password Test</td>
                                            <td><span class="badge badge-success"> Approved</span></td>
                                            <td><button type="submit" class="btn bg-indigo-700 btn-sm" data-toggle="modal" data-target="#view-details"><i class="icon icon-eye4"></i></button></td>
                                            <td>NA</td>
                                        </tr>
                                        <div id="view-details" class="modal" tabindex="-1">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-slate">
                                                        <h6 class="modal-title"><i class="icon-list mr-2"></i> View Details</h6>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <p><b>Member Name :</b> John David Lozano<hr></p>
                                                        <p><b>Category :</b> Password Test<hr></p>
                                                        <p><b>Status :</b> <span class="badge badge-success"> Approved</span><hr></p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn bg-slate">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                </div>
              
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.pm-kyc-details').addClass('active');
    $('li.profile-management-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
@endsection
@endsection