@extends('layouts.BackOffice.app')
@section('container')
<style type="text/css">
   .dataTables_processing {
z-index: 1000;
}
</style>
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <a class="breadcrumb-item">Profile Management</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">

            <div class="col-xl-12">
                <div class="row justify-content-center">
                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-teal icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($total)}}</h3>
                                    <span class="text-uppercase font-size-sm">Total Joinings</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-slate icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($today)}}</h3>
                                    <span class="text-uppercase font-size-sm">Today Joinings</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-slate icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($paid)}}</h3>
                                    <span class="text-uppercase font-size-sm">PAID Account</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-slate icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($cd)}}</h3>
                                    <span class="text-uppercase font-size-sm">CD Account</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-slate icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($free)}}</h3>
                                    <span class="text-uppercase font-size-sm">FS Account</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-slate icon-users icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{number_format($floating)}}</h3>
                                    <span class="text-uppercase font-size-sm">FLOATING</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body ">

                        <div class="table-responsive">
                            <table class="table table-striped datatable-responsive" >
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>Joined Date</th>
                                        <th>Username</th>
                                        <th>Name</th>
                                        <th style="width:1px">Sponsor</th>
                                        <th style="width:1px">Upline</th>
                                        <th style="width:1px">Position</th>
                                        <th style="width:1px">Type</th>
                                        <th>Package</th>
                                        <th style="width:1px">Country</th>
                                        <th style="width:1px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan=10 class="text-center"><i class="icon-spinner2 spinner"></i> </td>
                                    </tr>
                                </tbody>
                            
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    @section('custom')
    <script>
        $('a.pm-member-list').addClass('active');
        $('li.profile-management-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    {{-- <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script> --}}
    <script>
        $(document).ready(function () {

            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                responsive: true,
                columnDefs: [{
                    orderable: false,
                }],
                dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {
                        'first': 'First',
                        'last': 'Last',
                        'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                        'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                    }
                }
            });

            var table = $('.datatable-responsive').DataTable({
                "processing": true,  
                "serverSide": true, 
                "ajax": {
                    "url": "{{url('backoffice/profile-management/get-users')}}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}"
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "joined_date"},
                    {"data": "username"},
                    {"data": "name"},
                    {"data": "sponsor"},
                    {"data": "upline"},
                    {"data": "position"},
                    {"data": "type"},
                    {"data": "package"},
                    {"data": "country"},
                    {"data": "action"},
                ]
            });

        });
    </script>
    @endsection
    @endsection
