@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">
        
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a href="#basic-tab1" class="nav-link active" data-toggle="tab">Classic Import</a></li>
                        <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">Advance Import</a></li>
                        <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">Export</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="basic-tab1">
                            <form action="post">
                                <h3>Classic</h3>
                                <div class="form-group">
                                    <label for="">Select a file</label>
                                    <input type="file">
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                            <form action="post">
                                <h3>Advanced</h3>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="basic-tab3">
                            <form action="post">
                                <button type="button" class="btn btn-primary">Export Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.database').addClass('active');
    $('li.database-must-open').addClass('nav-item-expanded nav-item-open');

</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/form_multiselect.js') }}"></script>

<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
@endsection
@endsection