@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Advanced Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">E-Pin Configuration</span>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row">
                    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">E-Pin Character Set</label>
                                    <select class="form-control form-control-select2 input-form-c">
                                        <option value="1">Alphabets</option>
                                        <option value="2">Numerals</option>
                                        <option value="3">Alphanumerals</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-primary btn-form-c">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>

        <div class="col-xl-9">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm" onclick="newRank()"><i class="icon icon-add"></i> Add New E-Pin Amount</button>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search-toogle" style="display:none;padding-top:20px;">
                                <div class="form-group">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="E-Pin Amount">
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <button class="btn bg-primary">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-danger">Delete</button>
                        </div>
                        <div class="col-md-12"><br>
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></th>
                                            <th>E-Pin Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>&#8369;{{number_format('100',2)}}</td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.as-epin').addClass('active');
    $('li.settings-must-open,li.advanced-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>

@endsection
@endsection