@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}} Settings</h4>
                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <a class="breadcrumb-item">Settings</a>
                    <a class="breadcrumb-item">Advanced Settings</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-4">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-header bg-dark text-white d-flex justify-content-between">
                        <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Tooltip
                            Details</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox"
                                                            class="personal-pv form-check-input-styled" data-fouc
                                                            checked>
                                                        First Name
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Date of Joining
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Left
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Right
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Left Carry
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox"
                                                            class="personal-pv form-check-input-styled" data-fouc
                                                            checked>
                                                        Right Carry
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Personal PV
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Group PV
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="group-pv form-check-input-styled"
                                                            data-fouc checked>
                                                        Rank Status
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <button class="btn btn-sm btn-primary" name="update" type="submit"
                                            value="update">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-8">
                    <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                        <li class="nav-item">
                            <a href="#member-status" class="nav-link active" data-toggle="tab">Member
                                Status</a>
                        </li>
                        <li class="nav-item">
                            <a href="#membership-packs" class="nav-link" data-toggle="tab">Membership
                                Pack</a>
                        </li>
                        <li class="nav-item">
                            <a href="#rank-tab" class="nav-link" data-toggle="tab">Rank</a>
                        </li>
                    </ul>
                <!-- /profile navigation -->
                <!-- Inner container -->
                <div class="d-flex align-items-start flex-column flex-md-row">
                    <!-- Left content -->
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Member Status -->
                        <div class="tab-pane fade active show" id="member-status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Active</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Inactive</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Membership Packs -->
                        <div class="tab-pane fade" id="membership-packs">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Membership 1</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Membership 2</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Membership 3</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Rank -->
                        <div class="tab-pane fade" id="rank-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Bronze</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Silver</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="card border-primary">
                                                        <div class="card-header header-elements-inline">
                                                            <h6 class="card-title">Gold</h6>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <input type="file" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn bg-primary">Save</button>
                                                                <button class="btn btn-danger">Remove</button>
                                                            </div>

                                                            <div class="form-group text-center">
                                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}"
                                                                    class="img-fluid rounded-circle" width="200">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



        </div>
        <!-- /main charts -->
    </div>

    @section('custom')
    <script>
    $('a.as-tree').addClass('active');
    $('li.settings-must-open,li.advanced-settings-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    @endsection
    @endsection