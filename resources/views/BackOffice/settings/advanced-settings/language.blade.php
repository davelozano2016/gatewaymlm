@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span>Currency Management</span>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-basic">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Language</th>
                                            <th>Action</th>
                                            <th>Default Language</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/en.png"> 
                                                English
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" disabled><i class="icon-check"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/es.png"> 
                                                Español
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/ch.png"> 
                                                中文
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/de.png"> 
                                                Deutsch
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/pt.png"> 
                                                Português
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/fr.png"> 
                                                français
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>7.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/it.png"> 
                                                italiano
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>8.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/tr.png"> 
                                                Türk
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>9.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/po.png"> 
                                                polski
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>10.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/ar.png"> 
                                                العربية
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>11.</td> 
                                            <td class="flag_width">
                                                <img src="https://BackOffice.infinitemlmsoftware.com/BackOffice/public_html/images/flags/ru.png"> 
                                                русский
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm"><i class="icon-cross"></i></button>
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_currency" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Currency</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Title</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Code</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Value</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Symbol</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Symbol Position</label>
                        <select class="form-control form-control-select2 input-form-c">
                            <option>Left</option>
                            <option>Right</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select class="form-control form-control-select2 input-form-c">
                            <option>Enable</option>
                            <option>Disable</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
            </form>
      </div>
    </div>
</div>
@section('custom')
<script>
    $('a.as-language').addClass('active');
    $('li.settings-must-open,li.advanced-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
@endsection
@endsection