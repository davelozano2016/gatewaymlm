@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Advanced Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Profile Settings</span>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST">
                                <div class="form-group">
                                    <label for="">Automatic Logout After</label>
                                    <select class="form-control form-control-select2">
                                        <option value="300">5 minutes of inactivity</option>
                                        <option value="600">10 minutes of inactivity</option>
                                        <option value="900">15 minutes of inactivity</option>
                                        <option value="1800">30 minutes of inactivity</option>
                                        <option value="3600">1 hour of inactivity</option>
                                        <option value="7200">2 hours of inactivity</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Two Factor Authentication(2FA)
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                            Enable Age Restriction
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="required">Minimum Age Required</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                            Enable Unapproved User Login
                                        </label>
                                    </div>
                                </div>

                                <h4><hr>Password</h4>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" id="enable_prefix" onchange="passwordPolicy()" data-fouc>
                                            Enable Password Policy
                                        </label>
                                    </div>
                                </div>

                                <div class="password_policy_fields" style="display:none;">

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Should Contain a Lowercase Letter
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Should Contain an Uppercase Letter
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Should Contain a Number
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Should Contain a Special Character
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="required">Minimum Password Length</label>
                                    <input type="number" class="form-control">
                                </div>

                                </div>
 
                                <div class="form-group">
                                    <button class="btn btn-sm btn-primary" name="update" type="submit" value="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.as-profile').addClass('active');
    $('li.settings-must-open,li.advanced-settings-must-open').addClass('nav-item-expanded nav-item-open');

    function usernameType() {
        if ($('#username_type').val() == '1') {
            $('#enable_prefix').hide()
        } else {
            $('#field_enable_prefix').show()
        }
    }

    function usernamePrefix() {
        if($('#enable_prefix').prop("checked") == true){
            $('#field_username_prefix').show();
        } else {
            $('#field_username_prefix').hide();
        }
    }

    function passwordPolicy() {
        if($('#enable_prefix').prop("checked") == true){
            $('.password_policy_fields').show();
        } else {
            $('.password_policy_fields').hide();
        }
    }
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection