@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="text-uppercase font-weight-semibold" id="header-c-title">Payment Methods</span>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Payment Method</th>
                                            <th>Action</th>
                                            <th>Status</th>
                                            <th>Registration</th>
                                            <th>Admin Only</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>Free Joining</td>
                                            <td></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>2.</td>
                                            <td>Payeer <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_payeer"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>3.</td>
                                            <td>Bitgo <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_bitgo"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>4.</td>
                                            <td>Paypal <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_paypal"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>5.</td>
                                            <td>Authorized.Net <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_au"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>6.</td>
                                            <td>Sofort <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_sofort"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>7.</td>
                                            <td>Blockchain <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_blockchain"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>8.</td>
                                            <td>SquareUp <b>[Test]</b></td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_squareup"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>
                                        
                                        <tr>
                                            <td>9.</td>
                                            <td>E-Pin</td>
                                            <td></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>10.</td>
                                            <td>E-Wallet</td>
                                            <td></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>

                                        <tr>
                                            <td>11.</td>
                                            <td>Bank Transfer</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_banktransfer"><i class="icon-gear"></i></button>
                                            </td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                            <td><div class="form-check form-check-switch form-check-switch-left">
                                                <label class="form-check-label d-flex align-items-center">
                                                    <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                </label>
                                            </div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form>
<div class="modal fade" id="edit_payeer" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Payeer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Merchant ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Merchant Key</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Encryption Key</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Account</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_bitgo" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Bitgo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Wallet ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Token</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Mode</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Test</option>
                            <option value="2">Live</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Wallet Passphrase</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_paypal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Paypal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">API Username</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">API Password</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">API Signature</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Mode</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Test</option>
                            <option value="2">Live</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Return URL</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Cancel URL</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_au" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Authorize.Net</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Merchant Login ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Transaction Key</label>
                        <input type="password" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_sofort" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Sofort</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Customer ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Project ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Project Password</label>
                        <input type="password" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_blockchain" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Blockchain</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">xPub</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">API Key</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Secret</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Main Password</label>
                        <input type="password" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Second Password</label>
                        <input type="password" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Fee</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_squareup" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Square Up</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Access Token</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Application ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Location ID</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="edit_banktransfer" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Bank Details</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Bank Details</label>
                        <textarea class="form-control" rows="10" style="resize:none;"></textarea>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>
@section('custom')
<script>
    $('a.cs-payment').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

@endsection
@endsection