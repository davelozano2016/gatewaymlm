@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Rank Settings</span>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row">
                    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Rank Calculation Period</label>
                                    <select class="form-control form-control-select2 input-form-c">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                        <option value="4">Yearly</option>
                                        <option value="5">Instant</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                            <label for="">Rank Criteria</label>
                                <div class="form-group" id="rc-referral">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Referral Count
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="rc-personal">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Personal PV
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="rc-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                            Group PV
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" id="joinee_package" onclick="rcIncluded()" data-fouc>
                                            Joinee Package (Rank same as member package)
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="rc-downline">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Downline Member Count
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="rc-package">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                            Downline Member Package Count
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="rc-rank">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                            Downline Rank
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-primary btn-form-c">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>

        <div class="col-xl-9">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm" onclick="newRank()"><i class="icon icon-add"></i> Add New Rank</button>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search-toogle" style="display:none;padding-top:20px;">
                                <div class="form-group">
                                    <form id="search-form-advance">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Rank Name</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Referral Count</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Rank Achievers Bonus</label>
                                                    <div class="input-group">
                                                        <span class="input-group-prepend">
                                                            <span class="input-group-text input-c-icon-left">₱</span>
                                                        </span>
                                                        <input type="number" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="">Rank Color</label>
                                                    <select class="form-control form-control-select2 input-form-c">
                                                        <option>Red</option>
                                                        <option>Blue</option>
                                                        <option>Yellow</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group text-right"><br>
                                                    <button class="btn bg-primary">Add</button>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Rank Name</th>
                                            <th>Referral Count</th>
                                            <th>Rank Commission</th>
                                            <th>Rank Color</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{ $i }}.</td>
                                            <td>Bronze</td>
                                            <td>{{ $i }}</td>
                                            <td>&#8369;{{number_format('100',2)}}</td>
                                            <td>Red</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_rank"><i class="icon-pencil7"></i></button>
                                                    <button class="btn btn-warning btn-sm"><i class="icon-blocked"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_rank" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Rank</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Rank Name</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Referral Count</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Rank Achievers Bonus</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text input-c-icon-left">₱</span>
                            </span>
                            <input type="number" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Rank Color</label>
                        <select class="form-control form-control-select2 input-form-c">
                            <option>Red</option>
                            <option>Blue</option>
                            <option>Yellow</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
            </form>
      </div>
    </div>
</div>
@section('custom')
<script>
    $('a.cs-rank').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');

    function rcIncluded() {
        if($('#joinee_package').prop("checked") == true){
            $('#rc-personal').hide();
            $('#rc-referral').hide();
            $('#rc-group').hide();
            $('#rc-downline').hide();
            $('#rc-package').hide();
            $('#rc-rank').hide();
        } else {
            $('#rc-personal').show();
            $('#rc-referral').show();
            $('#rc-group').show();
            $('#rc-downline').show();
            $('#rc-package').show();
            $('#rc-rank').show();
        }
    }

    function newRank() {
        if($('.search-toogle:visible').length == 0) {
            $('.search-toogle').show()
        } else {
            $('.search-toogle').hide()
        }
    }
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

@endsection
@endsection