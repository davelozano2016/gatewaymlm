@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <a class="breadcrumb-item">Compensation</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">

        <div class="col-xl-6">
            <div class="card">
                <div class="card-header  d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Single Item Unilevel</span>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('backoffice/settings/commission-settings/compensation/unilevel-commission/store')}}" data-parsley-validate>
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Maintenance:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="maintenance" value="{{$settings->maintenance}}" id="maintenance">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Unilevel Count:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="unilevel_count" value="{{$settings->unilevel_count}}" id="unilevel_count">
                            </div>
                        </div>

                        @for($i=0;$i <= $settings->unilevel_count; $i++)
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">
                                    Percentage on Level {{$i}}
                                </label>
                                <div class="col-lg-9">
                                    <input type="hidden" class="form-control" name="unilevel_id[]" value="{{isset($unilevel[$i]->id) ? $unilevel[$i]->id : 0}}" required>
                                    <input type="hidden" class="form-control" name="level[]" value="{{$i}}" required>
                                    <input type="text" class="form-control" data-parsley-pattern="^[0-9]\d*(\.\d+)?$" name="percentage[]" value="{{isset($unilevel[$i]->percentage) ? $unilevel[$i]->percentage : 0}}" required>
                                </div>
                            </div>
                        @endfor
                     
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card">
                <div class="card-header  d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Product Bundle Unilevel</span>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Maintenance:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_maintenance" id="p_maintenance">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">You:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_you" id="p_you">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_you_status" id="p_you_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 1:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_1" id="p_level_1">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_1_status" id="p_level_1_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 2:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_2" id="p_level_2">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_2_status" id="p_level_2_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 3:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_3" id="p_level_3">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_3_status" id="p_level_3_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 4:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_4" id="p_level_4">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_4_status" id="p_level_4_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 5:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_5" id="p_level_5">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_5_status" id="p_level_5_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 6:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_6" id="p_level_6">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_6_status" id="p_level_6_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 7:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_7" id="p_level_7">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_7_status" id="p_level_7_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 8:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_8" id="p_level_8">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_8_status" id="p_level_8_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 9:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_9" id="p_level_9">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_9_status" id="p_level_9_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 10:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_10" id="p_level_10">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_10_status" id="p_level_10_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 11:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_11" id="p_level_11">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_11_status" id="p_level_11_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 12:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_12" id="p_level_12">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_12_status" id="p_level_12_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 13:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_13" id="p_level_13">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_13_status" id="p_level_13_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 14:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_14" id="p_level_14">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_14_status" id="p_level_14_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 15:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_15" id="p_level_15">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_15_status" id="p_level_15_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 16:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_16" id="p_level_16">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_16_status" id="p_level_16_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 17:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_17" id="p_level_17">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_17_status" id="p_level_17_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 18:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_18" id="p_level_18">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_18_status" id="p_level_18_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 19:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_19" id="p_level_19">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_19_status" id="p_level_19_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Level 20:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="p_level_20" id="p_level_20">
                            </div>
                            <div class="col-lg-3">
                                    <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="p_level_20_status" id="p_level_20_status" class="form-check-input-switch" checked>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Dynamic Compression:</label>
                            <div class="col-lg-5">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <span class="checked"><input type="radio" class="form-input-styled" name="p_compression" checked="" data-fouc=""></span>
                                        Enabled
                                    </label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <span><input type="radio" class="form-input-styled" name="p_compression" data-fouc=""></span>
                                        Disabled
                                    </label>
                                </div>
                            </div>
                        </div>
                     
                     
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>



@section('custom')
<script>
    $('a.cs-compensation').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection