@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <a class="breadcrumb-item">Compensation</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                        <li class="nav-item"><a href="#premium-account" class="nav-link active" data-toggle="tab">Premium Account</a></li>
                        <li class="nav-item"><a href="#supreme-account" class="nav-link" data-toggle="tab">Supreme Account</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="premium-account">
                            <h1 class="text-danger"><b>GOLD ACCOUNT (LOW)</b><hr></h1>
                            <form>
                                <h4>Matching & Points System</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Pairing Bonus:</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="pairing_bonus" id="pairing_bonus">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Required Points:</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="required_points" id="required_points">
                                    </div>
                                </div>
                                <hr>
                                <h4>Pairing Cut-Off & Schedules</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">(Default) Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_1_count" id="p_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_1_time" id="p_tier_1_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="p_tier_2_status" id="p_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_2_count" id="p_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_2_time" id="p_tier_2_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_2_status" id="p_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_3_count" id="p_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_3_time" id="p_tier_3_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="p_tier_3_status" id="p_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_4_count" id="p_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_4_time" id="p_tier_4_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_4_status" id="p_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_5_count" id="p_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_5_time" id="p_tier_5_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_5_status" id="p_tier_5_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Flushout Percentage:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="flushout_count" id="flushout_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="flushout_status" id="flushout_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Choose Leg:</label>
                                    <div class="col-lg-5">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span class="checked"><input type="radio" class="form-input-styled" name="leg" checked="" data-fouc=""></span>
                                                Strong Leg Flush
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span><input type="radio" class="form-input-styled" name="leg" data-fouc=""></span>
                                                Weak Leg Flush
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span><input type="radio" class="form-input-styled" name="leg" data-fouc=""></span>
                                                Both Leg Flush
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4>Maxout Limit & Schedules</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_1_count" id="m_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_1_time" id="m_tier_1_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="m_tier_2_status" id="m_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_2_count" id="m_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_2_time" id="m_tier_2_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_2_status" id="m_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_3_count" id="m_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_3_time" id="m_tier_3_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="m_tier_3_status" id="m_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_4_count" id="m_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_4_time" id="m_tier_4_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_4_status" id="m_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_5_count" id="m_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_5_time" id="m_tier_5_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_5_status" id="m_tier_5_status" checked>
                                    </div>
                                </div>
                                
                                <hr>
                                <h4>Leadership Bonus</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Required Maintenance Points:</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="required_points" id="required_points">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_1_count" id="s_level_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_level_2_status" id="s_level_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_2_count" id="s_level_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_2_status" id="s_level_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_3_count" id="s_level_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_level_3_status" id="s_level_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_4_count" id="s_level_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_4_status" id="s_level_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_5_count" id="s_level_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_5_status" id="s_level_5_status" checked>
                                    </div>
                                </div>
                                <hr>
                                <h4>Strong Leg Limit Settings</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_1_count" id="s_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_1_limit" id="s_tier_1_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_tier_2_status" id="s_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_2_count" id="s_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_2_limit" id="s_tier_2_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_2_status" id="s_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_3_count" id="s_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_3_limit" id="s_tier_3_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_tier_3_status" id="s_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_4_count" id="s_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_4_limit" id="s_tier_4_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_4_status" id="s_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_5_count" id="s_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_5_limit" id="s_tier_5_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_5_status" id="s_tier_5_status" checked>
                                    </div>
                                </div>
                                <hr>
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="supreme-account">
                            <h1 class="text-danger"><b>GOLD ACCOUNT (HIGH)</b><hr></h1>
                            <form>
                                <h4>Matching & Points System</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Pairing Bonus:</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="pairing_bonus" id="pairing_bonus">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Required Points:</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="required_points" id="required_points">
                                    </div>
                                </div>
                                <hr>
                                <h4>Pairing Cut-Off & Schedules</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">(Default) Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_1_count" id="p_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_1_time" id="p_tier_1_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="p_tier_2_status" id="p_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_2_count" id="p_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_2_time" id="p_tier_2_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_2_status" id="p_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_3_count" id="p_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_3_time" id="p_tier_3_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="p_tier_3_status" id="p_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_4_count" id="p_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_4_time" id="p_tier_4_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_4_status" id="p_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="p_tier_5_count" id="p_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="p_tier_5_time" id="p_tier_5_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="p_tier_5_status" id="p_tier_5_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Flushout Percentage:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="flushout_count" id="flushout_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="flushout_status" id="flushout_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Choose Leg:</label>
                                    <div class="col-lg-5">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span class="checked"><input type="radio" class="form-input-styled" name="leg" checked="" data-fouc=""></span>
                                                Strong Leg Flush
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span><input type="radio" class="form-input-styled" name="leg" data-fouc=""></span>
                                                Weak Leg Flush
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <span><input type="radio" class="form-input-styled" name="leg" data-fouc=""></span>
                                                Both Leg Flush
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4>Maxout Limit & Schedules</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_1_count" id="m_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_1_time" id="m_tier_1_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="m_tier_2_status" id="m_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_2_count" id="m_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_2_time" id="m_tier_2_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_2_status" id="m_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_3_count" id="m_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_3_time" id="m_tier_3_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" name="m_tier_3_status" id="m_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_4_count" id="m_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_4_time" id="m_tier_4_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_4_status" id="m_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="m_tier_5_count" id="m_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control form-control-select2 input-form-c"  name="m_tier_5_time" id="m_tier_5_time">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                            <option value="4">Yearly</option>
                                            <option value="5">Instant</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="form-check-input-switch" name="m_tier_5_status" id="m_tier_5_status" checked>
                                    </div>
                                </div>
                                
                                <hr>
                                <h4>Leadership Bonus</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Required Maintenance Points:</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="required_points" id="required_points">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_1_count" id="s_level_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_level_2_status" id="s_level_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_2_count" id="s_level_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_2_status" id="s_level_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_3_count" id="s_level_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_level_3_status" id="s_level_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_4_count" id="s_level_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_4_status" id="s_level_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Level 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_level_5_count" id="s_level_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_level_5_status" id="s_level_5_status" checked>
                                    </div>
                                </div>
                                <hr>
                                <h4>Strong Leg Limit Settings</h4>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 1:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_1_count" id="s_tier_1_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_1_limit" id="s_tier_1_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_tier_2_status" id="s_tier_2_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 2:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_2_count" id="s_tier_2_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_2_limit" id="s_tier_2_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_2_status" id="s_tier_2_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 3:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_3_count" id="s_tier_3_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_3_limit" id="s_tier_3_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" name="s_tier_3_status" id="s_tier_3_status" class="form-check-input-switch" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 4:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_4_count" id="s_tier_4_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_4_limit" id="s_tier_4_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_4_status" id="s_tier_4_status" checked>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Tier 5:</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="s_tier_5_count" id="s_tier_5_count">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="s_tier_5_limit" id="s_tier_5_limit">
                                    </div>
                                    <div class="col-lg-3">
                                            <input type="checkbox" data-on-color="primary" data-off-color="danger" data-on-text="Enabled" data-off-text="Disabled" class="form-check-input-switch" name="s_tier_5_status" id="s_tier_5_status" checked>
                                    </div>
                                </div>
                                <hr>
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->
        </div>
    </div>
    <!-- /main charts -->
</div>



@section('custom')
<script>
    $('a.cs-compensation').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection