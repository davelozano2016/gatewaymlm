@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Signup Settings</span>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST">
                                <div class="smtp-configuration" >
                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Block User Registration
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="group-pv form-check-input-styled" data-fouc>
                                                Enable Mail Notification
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Enable Admin verification For Free Joining
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="group-pv form-check-input-styled" id="enable_binary" data-fouc onchange="positionLeg()">
                                                Enable Binary Position(Leg) Locking
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="position-select" style="display:none;">
                                        <label for="">Position to Lock</label>
                                        <select class="form-control form-control-select2">
                                            <option value="1">Left Leg</option>
                                            <option value="2">Right Leg</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Personal PV
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <button class="btn btn-sm btn-primary" name="update" type="submit" value="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.cs-signup').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');

    function positionLeg() {
        if($('#enable_binary').prop("checked") == true){
            $('#position-select').show();
        } else {
            $('#position-select').hide();
        }
    }
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection