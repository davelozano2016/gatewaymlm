@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Compensation Settings</span>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Type of Compensation</th>
                                            <th style="width:1px;text-align:center">Status</th>
                                            <th style="width:1px">Configurations</th>
                                        </tr>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Binary Commission</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><a href="{{url('backoffice/settings/commission-settings/compensation/binary-commission')}}" class="btn btn-info text-white"><i class="icon icon-gear"></i></a></td>
                                            </tr>

                                            <tr>
                                                <td>2</td>
                                                <td>Unilevel  Commission</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><a href="{{url('backoffice/settings/commission-settings/compensation/unilevel-commission')}}" class="btn btn-info text-white"><i class="icon icon-gear"></i></a></td>
                                            </tr>

                                            <tr>
                                                <td>3</td>
                                                <td>Rank Commission</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><a href="{{ url('backoffice/settings/commission-settings/rank') }}" class="btn btn-info text-white"><i class="icon icon-gear"></i></a></td>
                                            </tr>

                                            <tr>
                                                <td>4</td>
                                                <td>Referral Commission</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#referral_commission"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            <tr>
                                                <td>5</td>
                                                <td>Repurchase Sales Commission</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#repurchase_sales_commission"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            <tr>
                                                <td>6</td>
                                                <td>Matching Bonus</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#matching_bonus"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            <tr>
                                                <td>7</td>
                                                <td>Pool Bonus</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#pool_bonus"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            <tr>
                                                <td>8</td>
                                                <td>Fast Start Bonus</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#fast_start_bonus"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            <tr>
                                                <td>9</td>
                                                <td>Performance Bonus</td>
                                                <td><div class="form-check form-check-switch form-check-switch-left">
                                                    <label class="form-check-label d-flex align-items-center">
                                                        <input type="checkbox" data-on-color="danger" data-off-color="primary" data-on-text="Enable" data-off-text="Disable" class="form-check-input-switch" checked>
                                                    </label>
                                                </div></td>
                                                <td style="text-align:center"><button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#performance_bonus"><i class="icon icon-gear"></i></button></td>
                                            </tr>

                                            
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->
        </div>
    </div>
    <!-- /main charts -->
</div>

<form>
<div class="modal fade" id="unilevel_commission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Level Commission</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Type of Commission</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Flat</option>
                            <option value="1">Percentage</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level Commission Criteria</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Commissions based on genealogy</option>
                            <option value="2">Commissions based on registration pack</option>
                            <option value="3">Commissions based on member pack</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Maximum commission upto level</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <h4>Commissions Based on Genealogy<hr></h4>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 1 Commission %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 2 Commission %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 3 Commission %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="referral_commission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Referral Commission</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Type of Commission</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Flat</option>
                            <option value="1">Percentage</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Referral Commission Criteria</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Based on Sponsor Package </option>
                            <option value="2">Based on Sponsor Rank </option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Membership 2</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Membership 3</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Membership 4</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Product 1</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Product 2</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="">Referral Commission - Terst</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="matching_bonus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Matching Bonus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Type of Commission</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Matching bonus based on genealogy</option>
                            <option value="2">Matching bonus based on member package</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Matching Bonus Up To Level</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <h4>Matching Bonus Based On Genealogy<hr></h4>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 1 Bonus %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 2 Bonus %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Level 3 Bonus %</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="repurchase_sales_commission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Repurchase Sales Commission</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Sales commission criteria</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1" selected="true">Sales commission based on sales volume</option>
                            <option value="2">Sales commission based on  Sales price</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Sales commission distribution</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Distribution based on genealogy level</option>
                            <option value="2">Distribution based on rank</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Distribution up to level</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <h4>Distribution Based On Upline Package (%)<hr></h4>
                    <h5><b>Level 1</b><hr></h5>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Membership 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 2</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 3</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 4</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 5</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 6</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 7</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <h5><b>Level 2</b><hr></h5>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Membership 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 2</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 3</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 4</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 5</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 6</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 7</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <h5><b>Level 3</b><hr></h5>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Membership 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 1</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 2</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 3</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 4</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 5</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 6</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Product 7</label>
                        <input type="text" class="form-control">
                    </div>
                </div>


            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="pool_bonus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Pool Bonus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Bonus (%)</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="sales_volume">Binary Commission based on Sales Volume</option>
                            <option value="sales_price">Binary Commission based on Sales Price</option>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <label for="">Eligible Ranks For Pool Bonus</label>
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" id="ranks_bronze" class="form-check-input-styled" data-fouc onclick="bronzeCheckbox()">
                                Bronze
                            </label>
                        </div>
                    </div>
                </div>
                    

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" id="ranks_silver" class="form-check-input-styled" data-fouc onclick="silverCheckbox()">
                                Silver
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" id="ranks_gold" class="form-check-input-styled" data-fouc onclick="goldCheckbox()">
                                Gold
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" id="bronze-row" style="display:none;">
                    <div class="form-group">
                        <label for="">Pool Percentage For Bronze (%)</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12" id="silver-row" style="display:none;">
                    <div class="form-group">
                        <label for="">Pool Percentage For Silver (%)</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12" id="gold-row" style="display:none;">
                    <div class="form-group">
                        <label for="">Pool Percentage For Gold (%)</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Calculation Period</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Monthly</option>
                            <option value="2">Quarterly</option>
                            <option value="3">Half Yearly</option>
                            <option value="4">Yearly</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Pool Bonus Calculation Criteria</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Sales</option>
                            <option value="2">Sales Volume</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Pool Bonus Distribution Criteria</label>
                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                            <option value="1">Equally</option>
                        </select>
                    </div>
                </div>


            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="fast_start_bonus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Fast Start Bonus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Referral Counts</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">No.of Days (from date of join)</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="">Bonus Amount</label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text input-c-icon-left">₱</span>
                                </span>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>

<form>
<div class="modal fade" id="performance_bonus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Performance Bonus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Bonus Name</th>
                            <th>Personal PV</th>
                            <th>Group PV</th>
                            <th>Bonus (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Vacation Fund</td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                        </tr>

                        <tr>
                            <td>2.</td>
                            <td>Education Fund</td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                        </tr>

                        <tr>
                            <td>3.</td>
                            <td>Car Fund</td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                        </tr>

                        <tr>
                            <td>4.</td>
                            <td>House Fund</td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
</div>
</form>


@section('custom')
<script>
    $('a.cs-compensation').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');

    function bronzeCheckbox() {
        var bronze = $('#bronze-row');
        if($('#ranks_bronze').prop("checked") == true){
            bronze.show();
        } else {
            bronze.hide();
        }
    }

    function silverCheckbox() {
        var silver = $('#silver-row');
        if($('#ranks_silver').prop("checked") == true){
            silver.show();
        } else {
            silver.hide();
        }
    }

    function goldCheckbox() {
        var gold = $('#gold-row');
        if($('#ranks_gold').prop("checked") == true){
            gold.show();
        } else {
            gold.hide();
        }
    }
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection