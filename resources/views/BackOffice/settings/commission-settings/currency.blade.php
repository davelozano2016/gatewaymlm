@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <a class="breadcrumb-item">Commission Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_currency"><span>Add Currency</span></button>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span>Currency Management</span>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px"><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></th>
                                            <th>Currency Title</th>
                                            <th>Currency Code</th>
                                            <th>Currency Value</th>
                                            <th>Symbol</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Bitcoin </td>
                                            <td>BTC</td>
                                            <td>0.00027712102822425</td>
                                            <td>฿</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Chinese Yuan </td>
                                            <td>CNY</td>
                                            <td>6.7705025712622</td>
                                            <td>¥</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Euro </td>
                                            <td>EUR</td>
                                            <td>0.87696527919067</td>
                                            <td> €</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>British Pound Sterling </td>
                                            <td>GBP</td>
                                            <td>0.77530483313105</td>
                                            <td>£</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Indian Rupee </td>
                                            <td>INR</td>
                                            <td>71.038523330784</td>
                                            <td>₹</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Philippine Peso </td>
                                            <td>PHP</td>
                                            <td>52.412029858914</td>
                                            <td>₱</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Russian Ruble </td>
                                            <td>RUB</td>
                                            <td>66.485999249318</td>
                                            <td>₽</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>Saudi Riyal </td>
                                            <td>SAR</td>
                                            <td>3.7510514813697</td>
                                            <td>﷼</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>United States Dollar  <span class="label label-info">Default</span> </td>
                                            <td>USD</td>
                                            <td>1</td>
                                            <td>$</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" id="referral-count" class="referral-count form-check-input-styled" data-fouc></td>
                                            <td>South African Rand </td>
                                            <td>ZAR</td>
                                            <td>13.716154402015</td>
                                            <td>R</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-warning btn-sm"><i class="icon-check"></i></button>
                                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_currency"><i class="icon-pencil7"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-warning">Block</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_currency" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Currency</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Title</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Code</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Value</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Currency Symbol</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Symbol Position</label>
                        <select class="form-control form-control-select2 input-form-c">
                            <option>Left</option>
                            <option>Right</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select class="form-control form-control-select2 input-form-c">
                            <option>Enable</option>
                            <option>Disable</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
            </form>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal " id="add_currency" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title" id="exampleModalLabel">Add Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label>Country Title:</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label>Currency Code:</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label>Currency Value:</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label>Symbol:</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.cs-currency').addClass('active');
    $('li.settings-must-open,li.commission-settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection