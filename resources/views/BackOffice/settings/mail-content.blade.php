@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}} Settings</h4>
                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <a class="breadcrumb-item">Settings</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                    <li class="nav-item">
                        <a href="#registration-verification-email" class="nav-link active" data-toggle="tab">Registration Verification Email</a>
                    </li>
                    <li class="nav-item">
                        <a href="#registration-email" class="nav-link" data-toggle="tab">Registration Email</a>
                    </li>
                    <li class="nav-item">
                        <a href="#payout-release-mail" class="nav-link" data-toggle="tab">Payout Release Mail</a>
                    </li>
                    <li class="nav-item">
                        <a href="#change-password" class="nav-link" data-toggle="tab">Change Password</a>
                    </li>
                    <li class="nav-item">
                        <a href="#change-transaction-password" class="nav-link" data-toggle="tab">Change Transaction Password</a>
                    </li>
                    <li class="nav-item">
                        <a href="#payout-request" class="nav-link" data-toggle="tab">Payout Request</a>
                    </li>
                    <li class="nav-item">
                        <a href="#forgot-password" class="nav-link" data-toggle="tab">Forgot Password</a>
                    </li>
                    <li class="nav-item">
                        <a href="#reset-google-auth" class="nav-link" data-toggle="tab">Reset Google Auth</a>
                    </li>
                    <li class="nav-item">
                        <a href="#forgot-transaction-password" class="nav-link" data-toggle="tab">Forgot Transaction Passsword</a>
                    </li>
                    <li class="nav-item">
                        <a href="#external-email" class="nav-link" data-toggle="tab">External Email</a>
                    </li>
                </ul>

                <div class="d-flex align-items-start flex-column flex-md-row">
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Registration Verification Email -->
                        <div class="tab-pane fade active show" id="registration-verification-email">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Registration Email -->
                        <div class="tab-pane fade" id="registration-email">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary" data-toggle="modal"
                                                                    data-target="#edit_reg_email"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Payout Release Mail -->
                        <div class="tab-pane fade" id="payout-release-mail">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Change Password -->
                        <div class="tab-pane fade" id="change-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Change Transaction Password -->
                        <div class="tab-pane fade" id="change-transaction-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Payout Request -->
                        <div class="tab-pane fade" id="payout-request">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Forgot Password -->
                        <div class="tab-pane fade" id="forgot-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('6')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Reset Google Auth -->
                        <div class="tab-pane fade" id="reset-google-auth">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('7')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Forgot Transaction Password -->
                        <div class="tab-pane fade" id="forgot-transaction-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Forgot Transaction Password -->
                        <div class="tab-pane fade" id="forgot-transaction-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('8')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- External Email -->
                        <div class="tab-pane fade" id="external-email">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('9')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal except Registration Email -->
    <form>
        <div class="modal fade" id="edit_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content ">
                    <div class="modal-header bg-slate">
                        <h5 class="modal-title modal-c-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="summernote"></div>
                        </div>
                        <p>Don't Edit Contents inside { }, these fields...It will be replaced by actual values!!</p>
                        <p><b>Other Variables That You Can Use</b></p>
                        <p>A. {fullname}</p>
                        <p>B. {company_name}</p>
                        <p>C. {company_address}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Registration Email Only -->
    <form>
        <div class="modal fade" id="edit_reg_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content ">
                    <div class="modal-header bg-slate">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i>Update
                            Registration Email</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="summernote"></div>
                        </div>
                        <p>Don't Edit Contents inside { }, these fields...It will be replaced by actual values!!</p>
                        <p><b>Other Variables That You Can Use</b></p>
                        <p>A. {fullname}</p>
                        <p>B. {username}</p>
                        <p>C. {company_name}</p>
                        <p>D. {company_address}</p>
                        <p>E. {sponsor_username}</p>
                        <p>F. {payment_type}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>


    @section('custom')
    <script>
    $('a.s-mail-content').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');

    function editMail(tab) {
        $('.modal-c-title').empty();
        if (tab == 1) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Registration Verification Email')
        } else if (tab == 2) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Payout Release Mail')
        } else if (tab == 3) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Change Password')
        } else if (tab == 4) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Change Transaction Password')
        } else if (tab == 5) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Payout Request')
        } else if (tab == 6) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Forgot Password')
        } else if (tab == 7) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Reset Google Auth')
        } else if (tab == 8) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Forgot Transaction Passsword')
        } else if (tab == 9) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update External Email')
        }
    }
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/editor_summernote.js') }}"></script>
    @endsection
    @endsection