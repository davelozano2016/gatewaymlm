@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Welcome Letter</span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                                <tr>
                                    <th width="1px">#x</th>
                                    <th>Language</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>English</td>
                                    <td><p>Dear Distributor, Congratulations on your decision...! A journey of thousand miles must begin wit</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Spanish</td>
                                    <td><p> Estimado Distribuidor, ¡Felicitaciones por tu decisión ...! Un viaje de mil millas debe comenz</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Chinese</td>
                                    <td><p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td>German</td>
                                    <td><p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer Entscheidung ...!Eine Reise von tausend Me</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>5.</td>
                                    <td>Portuguese</td>
                                    <td><p>Prezado Distribuidor, Parabéns pela sua decisão ...!Uma jornada de mil milhas deve começar co</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>6.</td>
                                    <td>French</td>
                                    <td><p> Cher distributeur, Félicitations pour votre décision ...! Un voyage de mille kilomètres doit </p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>7.</td>
                                    <td>Italian</td>
                                    <td><p> Gentile distributore, Congratulazioni per la tua decisione ...!Un viaggio di mille miglia deve </p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>8.</td>
                                    <td>Turkish</td>
                                    <td><p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil yolculuk tek bir adımla başlamalıdır.</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>9.</td>
                                    <td>Polish</td>
                                    <td><p> Drogi dystrybutorze, Gratulujemy twojej decyzji ...!Podróż tysiąca mil musi rozpocząć się</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>10.</td>
                                    <td>Arabic</td>
                                    <td><p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة ال�</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                                <tr>
                                    <td>11.</td>
                                    <td>Russian</td>
                                    <td><p> Уважаемый дистрибьютор! Поздравляю с решением ...! Пу�</p></td>
                                    <td><button class="btn btn-sm btn-primary" data-popup="tooltip" title="Edit" data-toggle="modal" data-target="#edit_details"><i class="icon-pencil7"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form>
<div class="modal fade" id="edit_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header bg-slate">
            <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i>Update Welcome Letter</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="summernote"></div>
            </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>
</form>

@section('custom')
<script>
    $('a.s-content-management').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>

<script src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/editor_summernote.js') }}"></script>
@endsection
@endsection