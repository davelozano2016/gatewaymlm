@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}} Settings</h4>
            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">Settings</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-4">

            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Site Information</span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST">
                                <div class="form-group">
                                    <label class="">Company Name</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="">Company Address</label>
                                    <textarea class="form-control" rows="5" style="resize:none;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label class="">Email Address</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="">Phone Number</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-sm btn-primary" name="update" type="submit" value="update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card border-primary">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Logo For Light Background</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="file" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn bg-primary">Save</button>
                                                <button class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group text-center">
                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}" class="img-fluid rounded-circle" width="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card border-primary">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Logo For Dark Background</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="file" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn bg-primary">Save</button>
                                                <button class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group text-center">
                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}" class="img-fluid rounded-circle" width="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card border-primary">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Logo For Collapse Sidebar</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="file" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn bg-primary">Save</button>
                                                <button class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group text-center">
                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}" class="img-fluid rounded-circle" width="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card border-primary">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Favicon</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="file" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn bg-primary">Save</button>
                                                <button class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group text-center">
                                                <img src="{{ url('assets/images/placeholders/placeholder.jpg') }}" class="img-fluid rounded-circle" width="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.s-company-profile').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection