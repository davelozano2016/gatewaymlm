@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}} Settings</h4>
                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <a class="breadcrumb-item">Settings</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                    <li class="nav-item">
                        <a href="#registration" class="nav-link active" data-toggle="tab">Registration</a>
                    </li>
                    <li class="nav-item">
                        <a href="#payout-release" class="nav-link" data-toggle="tab">Payout Release</a>
                    </li>
                    <li class="nav-item">
                        <a href="#change-password" class="nav-link" data-toggle="tab">Change Password</a>
                    </li>
                    <li class="nav-item">
                        <a href="#change-transaction-password" class="nav-link" data-toggle="tab">Change Transaction Password</a>
                    </li>
                    <li class="nav-item">
                        <a href="#payout-request" class="nav-link" data-toggle="tab">Payout Request</a>
                    </li>
                </ul>

                <div class="d-flex align-items-start flex-column flex-md-row">
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Registration Verification Email -->
                        <div class="tab-pane fade active show" id="registration">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('1')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Registration Email -->
                        <div class="tab-pane fade" id="payout-release">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('2')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Change Password -->
                        <div class="tab-pane fade" id="change-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('3')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Change Transaction Password -->
                        <div class="tab-pane fade" id="change-transaction-password">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('4')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Payout Request -->
                        <div class="tab-pane fade" id="payout-request">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned">
                                                    <thead>
                                                        <tr>
                                                            <th width="1px">#</th>
                                                            <th>Language</th>
                                                            <th>Content</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1.</td>
                                                            <td>English</td>
                                                            <td>
                                                                <p>Dear Distributor, Congratulations on your
                                                                    decision...! A journey of thousand miles must begin
                                                                    wit</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.</td>
                                                            <td>Spanish</td>
                                                            <td>
                                                                <p> Estimado Distribuidor, ¡Felicitaciones por tu
                                                                    decisión ...! Un viaje de mil millas debe comenz</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3.</td>
                                                            <td>Chinese</td>
                                                            <td>
                                                                <p>尊敬的经销商，祝贺您的决定...！一千英里的旅程必须从第一步开始。�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4.</td>
                                                            <td>German</td>
                                                            <td>
                                                                <p> Lieber Distributor, Herzlichen Glückwunsch zu Ihrer
                                                                    Entscheidung ...!Eine Reise von tausend Me</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5.</td>
                                                            <td>Portuguese</td>
                                                            <td>
                                                                <p>Prezado Distribuidor, Parabéns pela sua decisão
                                                                    ...!Uma jornada de mil milhas deve começar co</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6.</td>
                                                            <td>French</td>
                                                            <td>
                                                                <p> Cher distributeur, Félicitations pour votre décision
                                                                    ...! Un voyage de mille kilomètres doit </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7.</td>
                                                            <td>Italian</td>
                                                            <td>
                                                                <p> Gentile distributore, Congratulazioni per la tua
                                                                    decisione ...!Un viaggio di mille miglia deve </p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8.</td>
                                                            <td>Turkish</td>
                                                            <td>
                                                                <p> Sayın Bayimiz, Kararın için tebrikler ...! Bin mil
                                                                    yolculuk tek bir adımla başlamalıdır.</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9.</td>
                                                            <td>Polish</td>
                                                            <td>
                                                                <p> Drogi dystrybutorze, Gratulujemy twojej decyzji
                                                                    ...!Podróż tysiąca mil musi rozpocząć się</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10.</td>
                                                            <td>Arabic</td>
                                                            <td>
                                                                <p> عزيزي الموزع ،مبروك على قرارك ...!يجب أن تبدأ رحلة
                                                                    ال�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11.</td>
                                                            <td>Russian</td>
                                                            <td>
                                                                <p> Уважаемый дистрибьютор! Поздравляю с решением ...!
                                                                    Пу�</p>
                                                            </td>
                                                            <td><button class="btn btn-sm btn-primary"
                                                                    onclick="editMail('5')" data-toggle="modal" data-target="#edit_mail"><i
                                                                        class="icon-pencil7"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal-->
    <form>
        <div class="modal fade" id="edit_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content ">
                    <div class="modal-header bg-slate">
                        <h5 class="modal-title modal-c-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="summernote"></div>
                        </div>
                        <p><b>The variables those you can use are</b></p>
                        <div class="list-code"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>


    @section('custom')
    <script>
    $('a.s-sms-content').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');

    function editMail(tab) {
        $('.modal-c-title').empty();
        $('.list-code').empty();
        if (tab == 1) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Registration')
            $('.list-code').append('<p>1. {fullname}</p><p>2. {company_name}</p><p>3. {link}</p>')
        } else if (tab == 2) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Payout Release')
            $('.list-code').append('<p>1. {fullname}</p><p>2. {company_name}</p><p>3. {amount}</p>')
        } else if (tab == 3) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Change Password')
            $('.list-code').append('<p>1. {fullname}</p><p>2. {company_name}</p><p>3. {new_password}</p>')
        } else if (tab == 4) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Change Transaction Password')
            $('.list-code').append('<p>1. {fullname}</p><p>2. {company_name}</p><p>3. {new_password}</p>')
        } else if (tab == 5) {
            $('.modal-c-title').append('<i class="icon-pencil7 mr-2"></i> Update Payout Request')
            $('.list-code').append(
                '<p>1. {fullname}</p><p>2. {company_name}</p><p>3. {admin_user_name}</p><p>4. {username}</p><p>5. {payout_amount}</p>'
                )
        }
    }
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/editor_summernote.js') }}"></script>
    @endsection
    @endsection