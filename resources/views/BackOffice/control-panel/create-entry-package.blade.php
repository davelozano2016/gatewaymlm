@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Settings</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#add_entry_product_modal"><span>Add Entry</span></button>
                </div>
            </div>
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card" >
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Entry Name</th>
                                                <th>Price</th>
                                                <th>Currency</th>
                                                <th class="text-center">DR</th>
                                                <th class="text-center">BPV</th>
                                                <th>Date Created</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1?>
                                            @foreach($entries as $entry)
                                            <tr>
                                                <td style="width:1px">{{$i++}}</td>
                                                <td>{{$entry->entry}}</td>
                                                <td>
                                                    {{$entry->currency_symbol}}{{number_format($entry->price)}}</td>
                                                <td>{{$entry->currency_code}}</td>
                                                <td style="width:1px">
                                                    {{$entry->currency_symbol}}{{number_format($entry->direct_referal,2)}}
                                                </td>
                                                <td style="width:1px" class="text-center">
                                                    {{number_format($entry->binary_point_value,2)}}</td>
                                                <td>{{$date::parse($entry->created_at)->format("F j, Y")}}</td>
                                                <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)"
                                                        data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a href="{{url('backoffice/control-panel/create-entry-package/add-entry-package/'.Crypt::encryptString($entry->entry_id))}}"
                                                            class="dropdown-item"><i class="icon-eye"></i> Add Package</a>
                                                        <a href="#"
                                                            class="dropdown-item"><i class="icon-pencil"></i>Edit Package</a>
                                                        <a href="#"
                                                            class="dropdown-item"><i class="icon-trash"></i>Delete Package</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal " id="add_entry_product_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Add Entry</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" data-parsley-validate action="{{url('backoffice/control-panel/create-entry-package/add-entry')}}">
                        @csrf
                        <h6>Entry Details</h6>
                        <input type="hidden" name="entry_id" id="entry_id" class="form-control">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Entry</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="entry" id="entry" required>
                                @error('entry') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Discount Type</label>
                            <div class="col-md-8">
                                <select name="discount_type" class="form-control payout-fee form-control-select2"
                                    id="discount_type">
                                    <option value="Percentage">%</option>
                                    <option value="Fixed" selected>Fixed</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Binary Point Value</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-parsley-pattern="^\d+(\.\d{2})?$" name="binary_point_value" id="entry" required>
                                @error('entry') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        @foreach($countries as $country)

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Direct Referal in {{$country->currency_code}} (
                                {{$country->name}}
                                )</label>
                            <div class="col-md-8">
                                <input type="text" name="direct_referal[]" data-parsley-pattern="^\d+(\.\d{2})?$" class="form-control" placeholder="{{$country->currency_name}}" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Price in {{$country->currency_code}} (
                                {{$country->name}}
                                )</label>
                            <div class="col-md-8">
                                <input type="hidden" name="countries_id[]" class="form-control"
                                    value="{{$country->id}}">
                                <input type="text" name="price[]" data-parsley-pattern="^\d+(\.\d{2})?$" class="form-control"
                                    placeholder="{{$country->currency_name}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Stockist Discount</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-parsley-pattern="^\d+(\.\d{2})?$" name="stockist_discount[]" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Depot Discount</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-parsley-pattern="^\d+(\.\d{2})?$" name="depot_discount[]" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Country Manager Discount</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" data-parsley-pattern="^\d+(\.\d{2})?$" name="country_manager_discount[]" required>
                            </div>
                        </div>
                        <div style="border:1px solid #f5f5f5" class="mb-3"></div>

                        @endforeach



                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <!-- After Submit -->
                    <!-- <a href="{{url('backoffice/control-panel/entry-package')}}" class="btn btn-primary ">Submit</a> -->
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
    $('a.s-create-entry-panel').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    
    <script>
    function edit_entry_package(id) {
        $.ajax({
            type: 'GET',
            url: '{{url("backoffice/control-panel/create-entry-package/show-entry-package")}}/' + id,
            dataType: 'json',
            success: function(data) {
                $('#add_entry_product_modal').modal()
                $('#entry_id').val(data.entry_id);
                $('#entry').val(data.entry);
                $('#price').val(data.price);
            }
        })
    }
    </script>
    @endsection
    @endsection