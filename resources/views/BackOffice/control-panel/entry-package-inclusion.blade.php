@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Settings</span>
                    <span class="breadcrumb-item">Create Entry Package</span>
                    <span class="breadcrumb-item">Create Entry Package</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#add_inclusions_modal"><span>Add Product Inclusion</span></button>
                </div>
            </div>
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-columned datatable-responsive">
                                <thead>
                                    <tr>
                                        <th style="width:1px">#</th>
                                        <th>Package Entry</th>
                                        <th>Product Title</th>
                                        <th>Product Code</th>
                                        <th style="width:1px">Quantity</th>
                                        <th style="width:1px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @foreach($inclusions as $inclusion)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$inclusion->package_name}}</td>
                                        <td>{{$inclusion->product_title}}</td>
                                        <td>{{$inclusion->product_code}}</td>
                                        <td>{{$inclusion->quantity}}</td>
                                        <td style="width:1px" class="text-center">
                                            <a class="list-icons-item" href="javascript:void(0)"
                                                data-toggle="dropdown"><i class="icon-gear"></i></a>
                                            <div class="dropdown-menu">
                                                <a href="javascript:(0)" class="dropdown-item"><i
                                                        class="icon-pencil"></i>Edit Inclusion</a>
                                                <a href="{{url('backoffice/control-panel/create-entry-package/delete-inclusion/'.Crypt::encryptString($inclusion->inclusions_id))}}" class="dropdown-item"><i
                                                        class="icon-trash"></i>Delete Inclusion</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->

    <!-- Modal -->

    <div class="modal " id="add_inclusions_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Add Package Inclusion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @if(count($products) == 0) 
                    @else
                    
                    <form method="POST"
                        action="{{url('backoffice/control-panel/create-entry-package/add-inclusion/store')}}"
                        data-parsley-validate>
                        @csrf
                        <div class="row">
                            @foreach($products as $data)
                            <input type="hidden" name="product_package_id[]" value="{{Crypt::encryptString($data->package_id)}}">
                            <input type="hidden" name="unique_code" value="{{Crypt::encryptString($package_unique_code)}}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Product Title:</label>
                                    <label for="" class="form-control">{{$data->product_title}}</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Quantity:</label>
                                    <input type="text" class="form-control" name="quantity[]" id="quantity">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    @endif

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" {{count($products) == 0 ? 'disabled' : ''}}>Add Inclusion</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    
    <script>
        $('a.s-create-entry-panel').addClass('active');
        $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection
