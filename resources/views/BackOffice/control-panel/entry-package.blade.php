@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Settings</span>
                    <span class="breadcrumb-item">Create Entry Package</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" wire:click="resetInput()" data-toggle="modal"
                        data-target="#add_entry_package_modal"><span>Add Entry Package</span></button>
                </div>
            </div>
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date Added</th>
                                                <th>Package Type</th>
                                                <th>Status</th>
                                                <th>Products</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1 ?>
                                            @foreach ($packages as $package)
                                            <tr>
                                                <td style="width:1px"><?=$i++?></td>
                                                <td>{{$package->created_at}}</td>
                                                <td>{{$package->package_name}}</td>
                                                <td style="width:1px">{{$package->package_status == 0 ? 'Active' : 'Inactive'}}</td>
                                                <td style="width:1px;text-align:center"><a
                                                        href="{{url('backoffice/control-panel/create-entry-package/entry-package-inclusion/'.Crypt::encryptString($package->package_unique_code))}}">
                                                        <?php $quantity = 0; ?>
                                                        @foreach($inclusions[$package->package_unique_code] as $key => $values)
                                                           <?php $quantity += $values->quantity ?>
                                                        @endforeach
                                                        {{$quantity}}
                                                    
                                                    </a>
                                                </td>
                                                <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)"
                                                        data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a href="javascript:(0)" onclick="edit_entry_package('{{Crypt::encryptString($package->package_unique_code)}}')" class="dropdown-item"><i
                                                                class="icon-pencil"></i>Edit Entry Package</a>
                                                        <a href="{{url('backoffice/control-panel/create-entry-package/delete-entry-package/'.Crypt::encryptString($package->id))}}" class="dropdown-item"><i
                                                                class="icon-trash"></i>Delete Entry Package</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal " id="add_entry_package_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Entry Package Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST"
                        action="{{url('backoffice/control-panel/create-entry-package/add-entry-package/store')}}" data-parsley-validate>
                        @csrf
                        <input type="hidden" name="packages_id" id="packages_id" class="form-control">
                        <input type="hidden" name="entries_id" id="entries_id" value="{{Crypt::encryptString($id)}}" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Package Name:</label>
                                    <input type="text" class="form-control" name="package_name" id="package_name" required>
                                    @error('package_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status:</label>
                                    <select id="package_status" class="form-control " name="package_status">
                                        <option value="0">Active</option>
                                        <option value="1">Inactive</option>
                                    </select>
                                    @error('status') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description:</label>
                                    <textarea name="description" class="form-control" style="resize:none;height:200px" required></textarea>
                                    @error('description') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                    <!-- After Submit -->

                    <button type="submit" class="btn btn-primary ">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('custom')
    
    <script>
    $('a.s-create-entry-panel').addClass('active');
    $('li.settings-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script>
    function edit_entry_package(id) {
        $.ajax({
            type: 'GET',
            url: '{{url("backoffice/control-panel/create-entry-package/show-entry-package")}}/' + id,
            dataType: 'json',
            success: function(data) {
                $('#add_entry_package_modal').modal()
                $('#packages_id').val(data.packages_id);
                $('#package_name').val(data.package_name);
                $('#package_status').val(data.package_status);
            }
        })
    }
    </script>
    @endsection
    @endsection