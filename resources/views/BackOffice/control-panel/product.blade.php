@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">Settings</span>
                    <span class="breadcrumb-item">Entry Package</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" wire:click="resetInput()" data-toggle="modal"
                        data-target="#add_product_modal"><span>Add Product</span></button>
                </div>
            </div>
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card" >
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-basic">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Code</th>
                                            <th>Product Name</th>
                                            <th>Item</th>
                                            <th colspan=2 class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>A</td>
                                            <td>Product A</td>
                                            <td>1</td>
                                            <div class="btn-group">
                                            <td class="text-center">
                                                    <a href="#" class="btn btn-info pull-right" onclick="edit_product_package('1')" data-placement="top"><i class="icon-pencil"></i></a>
                                            </td>
                                            <td class="text-center">
                                                    <a class="btn btn-danger" href="#" data-placement="top"><i class="icon-trash"></i></a>
                                            </td>
                                            </div>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>
<!-- /content area -->

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal " id="add_product_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title" id="exampleModalLabel">Product Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{url('backoffice/control-panel/create-entry-package/add-entry-package')}}">
                    @csrf
                    <input type="hidden" name="entry_id" id="entry_id" class="form-control">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Entry:</label>
                                    <input type="text" class="form-control" name="entry" id="entry" value="Entry Name(Price)">
                                    @error('entry') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Package Name:</label>
                                    <input type="text" class="form-control" name="package_name" id="package_name">
                                    @error('package_name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Status:</label>
                                    <select type="text" class="form-control form-control-select2" name="status" id="status">
                                        <option>Active</option>    
                                    </select>
                                    @error('status') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                <!-- After Submit -->
                <a href="{{url('backoffice/control-panel/entry-package')}}" class="btn btn-primary ">Submit</a>
            </div>
            </form>
        </div>
    </div>
</div>

@section('custom')
<script>
$('a.s-create-entry-panel').addClass('active');
$('li.settings-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>

<script>

function edit_product_package(id) {
    $.ajax({
        type : 'GET',
        url : '{{url("backoffice/control-panel/create-entry-package/show-entry-package")}}/'+id,
        dataType : 'json',
        success:function(data) {
            $('#add_product_modal').modal()
            $('#entry_id').val(data.entry_id);
            $('#entry').val(data.entry);
            $('#price').val(data.price);
        }
    })
}

</script>
@endsection
@endsection