@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-success icon-ticket icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">1</h3>
                                <span class="text-uppercase font-size-sm">Total Tickets</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-info icon-ticket icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">1</h3>
                                <span class="text-uppercase font-size-sm">In Progress</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-danger icon-ticket icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">1</h3>
                                <span class="text-uppercase font-size-sm">Critical</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3 align-self-center">
                                <i class="text-primary icon-ticket icon-3x"></i>
                            </div>
            
                            <div class="media-body text-right">
                                <h3 class="font-weight-semibold mb-0">1</h3>
                                <span class="text-uppercase font-size-sm">New Tickets</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title text-uppercase">All Tickets</h6>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Ticket ID</th>
                                            <th>Subject</th>
                                            <th>Created By</th>
                                            <th>Assignee</th>
                                            <th>Status</th>
                                            <th>Category</th>
                                            <th>Created On</th>
                                            <th>Timeline</th>
                                        </tr>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>IMS965715{{$i}}</td>
                                                <td>This is subject</td>
                                                <td>John Doe (1002010{{$i}})</td>
                                                <td>computology</td>
                                                <td>New</td>
                                                <td>Error</td>
                                                <td>{{date('F j, Y',strtotime($date))}}</td>
                                                <td><a class="btn btn-primary btn-sm" href="{{ url('backoffice/support-center/timeline') }}"><i class="icon icon-screen-full"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.sc-ticket-dashboard').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection