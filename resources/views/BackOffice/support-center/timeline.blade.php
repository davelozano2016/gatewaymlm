@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="card-title font-weight-semibold">Ticket Details</span>
                    <button class="btn bg-slate" onclick="editDetails()"><i class="icon-pencil7"></i></button>  

                </div>

                <div class="card-body p-0">
                    <ul class="nav nav-sidebar my-2">
                        <li class="nav-item">
                            <a class="nav-link">Ticket ID
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">82211523</span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Username
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">John David Lozano</span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Subject
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">123456789</span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Created on
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">{{date('F j, Y',strtotime($date))}}</span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Updated
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">{{date('F j, Y',strtotime($date))}}</span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Ticket Status
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                    <select class="form-control c-input" id="" disabled>
                                        <option value="">Select Status</option>
                                        <option value="Mital" selected>Mital</option>
                                        <option value="Approved">Approved</option>
                                        <option value="Rejected">Rejected</option>
                                    </select>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Category
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                    <select class="form-control c-input" id="" disabled>
                                        <option value="">Select Category</option>
                                        <option value="1" selected>Commissions</option>
                                        <option value="2">tds related</option>
                                        <option value="3">Buisness plan related</option>
                                        <option value="4">Account verification related</option>
                                
                                    </select>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Priority
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                    <select class="form-control c-input" id="" disabled>
                                        <option value="">Select Category</option>
                                        <option value="1" selected>Low</option>
                                    </select>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Assignee
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                    <select class="form-control c-input" id="" disabled>
                                        <option value="">Select Status</option>
                                        <option value="Mital" selected>Admin</option>
                                    </select>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">Tags
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                     <select multiple="multiple" class="form-control select c-input" data-fouc disabled>
                                        <option value="">Select Status</option>
                                        <option value="Demo" selected>Demo</option>
                                        <option value="Demo" >Demo2</option>
                                        <option value="Demo" >Demo3</option>
                                        <option value="Demo" >Demo4</option>
                                        <option value="Demo" >Demo5</option>
                                        <option value="Demo" >Demo6</option>
                                        <option value="Demo" >Demo7</option>
                                    </select>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-divider"></li>
                        <li class="nav-item">
                            <a class="nav-link">
                                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                                    <button class="btn btn-info btn-sm" id="c-btn-update" style="display:none;">Update</button>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>

       

        <div class="col-xl-9">
                <div class="text-right">
                    <button class="btn bg-info" data-toggle="modal" data-target="#view_reply">Reply</button>  
                    <button class="btn bg-success" data-toggle="modal" data-target="#view_comments">Comments</button>  
                </div>
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="text-uppercase font-weight-semibold">Ticket - IMS159813</span>
                </div>
                <div class="card-body border-top-teal">
                    <div class="list-feed">
                        <div class="list-feed-item">
                            <div class="text-muted">Jan 12, 12:47</div>
                            <a href="#">David Linner</a> requested refund for a double bank card charge
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 11, 10:25</div>
                            User <a href="#">Christopher Wallace</a> from Google is awaiting for staff reply
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 10, 09:37</div>
                            Ticket <strong>#43683</strong> has been resolved by <a href="#">Victoria Wilson</a>
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 9, 08:28</div>
                            <a href="#">Eugene Kopyov</a> merged <strong>Master</strong>, <strong>Demo</strong> and <strong>Dev</strong> branches
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 8, 07:58</div>
                            All sellers have received payouts for December, 2016!
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 7, 06:32</div>
                            <a href="#">Chris Arney</a> created a new ticket <strong>#43136</strong> and assigned to <a href="#">John Nod</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div id="view_comments" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Comments</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label for="">Comment</label>
                    <textarea class="form-control" name="" id="" rows="5" style="resize: none;"></textarea>
                </div>
                <div class="form-group text-right">
                    <button type="button" class="btn btn-info btn-sm">Add</button>
                </div>
                </form>
                <hr>
                <div class="table-responsive table-scrollable">
                    <table class="table table-columned" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Comment</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=1;$i<=10;$i++)
                            <tr>
                                <td>1.</td>
                                <td>Comment</td>
                                <td><button class="btn bg-danger"><i class="icon-trash"></i></button></td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="modal-footer bg-slate">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="view_reply" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Reply</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <ul class="media-list media-chat media-chat-scrollable mb-3">
                    <li class="media content-divider justify-content-center text-muted mx-0">Monday, Feb 10</li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Below mounted advantageous spread yikes bat stubbornly crud a and a excepting</div>
                            <div class="font-size-sm text-muted mt-2">Mon, 9:54 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media media-chat-item-reverse">
                        <div class="media-body">
                            <div class="media-chat-item">Far squid and that hello fidgeted and when. As this oh darn but slapped casually husky sheared that cardinal hugely one and some unnecessary factiously hedgehog a feeling one rudely much but one owing sympathetic regardless more astonishing evasive tasteful much.</div>
                            <div class="font-size-sm text-muted mt-2">Mon, 10:24 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>

                        <div class="ml-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>
                    </li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Darn over sour then cynically less roadrunner up some cast buoyant. Macaw krill when and upon less contrary warthog jeez some koala less since therefore minimal.</div>
                            <div class="font-size-sm text-muted mt-2">Mon, 10:56 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media media-chat-item-reverse">
                        <div class="media-body">
                            <div class="media-chat-item">Some upset impious a and submissive when far crane the belched coquettishly. More the puerile dove wherever</div>
                            <div class="font-size-sm text-muted mt-2">Mon, 11:29 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>

                        <div class="ml-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>
                    </li>

                    <li class="media content-divider justify-content-center text-muted mx-0">Yesterday</li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Regardless equitably hello heron glum cassowary jocosely before reliably a jeepers wholehearted shuddered more that some where far by koala.</div>
                            <div class="font-size-sm text-muted mt-2">Tue, 6:40 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Crud reran and while much withdrew ardent much crab hugely met dizzily that more jeez gent equivalent unsafely far one hesitant so therefore.</div>
                            <div class="font-size-sm text-muted mt-2">Tue, 10:28 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media media-chat-item-reverse">
                        <div class="media-body">
                            <div class="media-chat-item">Thus superb the tapir the wallaby blank frog execrably much since dalmatian by in hot. Uninspiringly arose mounted stared one curt safe</div>
                            <div class="font-size-sm text-muted mt-2">Tue, 8:12 am <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>

                        <div class="ml-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>
                    </li>

                    <li class="media content-divider justify-content-center text-muted mx-0">Today</li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Tolerantly some understood this stubbornly after snarlingly frog far added insect into snorted more auspiciously heedless drunkenly jeez foolhardy oh.</div>
                            <div class="font-size-sm text-muted mt-2">Wed, 4:20 pm <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media media-chat-item-reverse">
                        <div class="media-body">
                            <div class="media-chat-item">Satisfactorily strenuously while sleazily dear frustratingly insect menially some shook far sardonic badger telepathic much jeepers immature much hey.</div>
                            <div class="font-size-sm text-muted mt-2">2 hours ago <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>

                        <div class="ml-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>
                    </li>

                    <li class="media">
                        <div class="mr-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>

                        <div class="media-body">
                            <div class="media-chat-item">Grunted smirked and grew less but rewound much despite and impressive via alongside out and gosh easy manatee dear ineffective yikes.</div>
                            <div class="font-size-sm text-muted mt-2">13 minutes ago <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                        </div>
                    </li>

                    <li class="media media-chat-item-reverse">
                        <div class="media-body">
                            <div class="media-chat-item"><i class="icon-menu"></i></div>
                        </div>

                        <div class="ml-3">
                            <a href="{{asset('assets/images/placeholders/placeholder.jpg') }}">
                                <img src="{{asset('assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle" width="40" height="40" alt="">
                            </a>
                        </div>
                    </li>
                </ul>

                <textarea name="enter-message" class="form-control mb-3" rows="3" cols="1" placeholder="Enter your message..."></textarea>

                <div class="d-flex align-items-center">
                    <div class="list-icons list-icons-extended">
                        <a href="#" class="list-icons-item"><i class="icon-file-picture"></i></a>
                        <a href="#" class="list-icons-item"><i class="icon-file-video"></i></a>
                        <a href="#" class="list-icons-item"><i class="icon-file-plus"></i></a>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b><i class="icon-paperplane"></i></b> Send</button>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
    $('a.sc-ticket-dashboard').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');

    function editDetails() {
        $('#c-btn-update').show();
        $(".c-input").prop( "disabled", false );
    }
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/chat_layouts.js') }}"></script>

@endsection
@endsection