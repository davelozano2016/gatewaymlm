@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<div class="content">
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Manage Status</h6>
                    <button class="btn btn-info" data-toggle="modal" data-target="#modal_status">Add New Status</button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>New</td>
                                    <td>
                                    <button class="btn btn-success btn-sm"><i class="icon-checkmark-circle2"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_status"><i class="icon-pencil7"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Manage Tags</h6>
                    <button class="btn btn-info" data-toggle="modal" data-target="#modal_tags">Add New Tags</button>
                </div>
                <div class="card-body ">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tags</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Tags 1</td>
                                    <td>
                                    <button class="btn btn-success btn-sm"><i class="icon-checkmark-circle2"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_status"><i class="icon-pencil7"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Manage Priority</h6>
                    <button class="btn btn-info" data-toggle="modal" data-target="#modal_status">Add New Priority</button>
                </div>
                <div class="card-body ">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Priority</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Priority 1</td>
                                    <td>
                                    <button class="btn btn-success btn-sm"><i class="icon-checkmark-circle2"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_status"><i class="icon-pencil7"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="">
<div id="modal_status" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Add Status</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Status:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b><i class="icon-paperplane"></i></b> Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

<form action="">
<div id="modal_tags" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Add Tags</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Tags:</label>
                    <div class="col-md-8">
                    <input type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b><i class="icon-paperplane"></i></b> Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

<form action="">
<div id="modal_priority" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Add Priority</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Priority:</label>
                    <div class="col-md-8">
                    <input type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b><i class="icon-paperplane"></i></b> Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

@section('custom')
<script>
    $('a.sc-configuration').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
@endsection
@endsection
