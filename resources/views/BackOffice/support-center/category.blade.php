@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Add {{$title}}</h6>
                    <div class="header-elements">
                        <!-- Edit Mode -->
                        <!-- <button class="btn btn-info">Add New Category</button> -->
                    </div>
                </div>
                <div class="card-body">
                    <form action="">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Category Name:</label>
                        <div class="col-md-8">
                        <input type="text" class="form-control" name="" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Assign To:</label>
                        <div class="col-md-8">
                        <select class="form-control form-control-select2">
                            <option value="">Select User</option>
                            <option value="1">Admin 1</option>
                            <option value="2">Admin 2</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-info btn-sm">Add</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Manage {{$title}}</h6>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                         <tr>
                                             <th style="width:1px">#</th>
                                             <th>Category</th>
                                             <th style="width:1px;">Tickets</th>
                                             <th>Graph</th>
                                             <th colspan=2  style="width:1px;text-align:center">Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                         @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>Category {{$i}}</td>
                                                <td class="text-center">{{$i}}</td>
                                                <td><div class="progress">
                                                   <div class="progress-bar bg-teal" style="width: {{$i}}%">
                                                       <span>{{$i}}% Complete</span>
                                                   </div>
                                               </div></td>
                                                <td style="width:1px"><button class="btn btn-danger"><i class="icon icon-blocked"></i></button></td>
                                                <td style="width:1px"><a class="btn btn-info" href="{{ url('backoffice/support-center/category') }}"><i class="icon icon-pencil7"></i></td>
                                            </tr>
                                         @endfor
                                    </tbody>
                                 </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('custom')
<script>
    $('a.sc-category').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
@endsection
@endsection
