@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Support Center</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" onclick="xtoogleSearchx()">Advance Search</button>
                    <div class="search-toogle" style="display:none;padding-top:20px;">
                        <form action="" method="POST" id="search-form-advance">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="form-group">
                                        <label for="">Ticket ID</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div class="form-group">
                                        <label for="">Created By</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div class="form-group">
                                        <label for="">Assignee</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6" id="category-c">
                                    <div class="form-group">
                                        <label for="">Category</label>
                                        <select class="form-control payout-fee form-control-select2">
                                            @for($i=1;$i<=10;$i++)
                                                <option>Category {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-5" id="tags-c">
                                    <div class="form-group">
                                        <label for="">Tags</label>
                                        <select class="form-control payout-fee form-control-select2">
                                            @for($i=1;$i<=10;$i++)
                                                <option>Tags {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-7 row">
                                    <div class="col-md-12">
                                    <label for="">Date Range</label>
                                        <button type="button" style="width:100%;" class="btn btn-light daterange-predefined">
                                            <span></span>
                                        </button>
                                    </div>
                                </div>
                                
                            
                                <div class="col-lg-12 col-md-12" id="search-c">
                                    <div class="form-group">
                                        <label class="d-block">Search Within:</label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Assigned To Me
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Assigned To Others
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Unassigned Tickets
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                                Only Tagged Tickets
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="btn bg-slate"><i class="icon-search4"></i></button>
                                        <button type="button" class="btn btn-warning" onclick="resetFormSearch()"><i class="icon-reset"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">On Going Tickets</h6>
                    {{-- <div class="header-elements">
                    </div> --}}
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Ticket ID</th>
                                            <th>Subject</th>
                                            <th>Created By</th>
                                            <th>Assignee</th>
                                            <th>Status</th>
                                            <th>Category</th>
                                            <th>Priority</th>
                                            <th>Created On</th>
                                            <th>Last Updated</th>
                                            <th>Timeline</th>
                                        </tr>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>IMS965715{{$i}}</td>
                                                <td>This is subject</td>
                                                <td>John Doe (1002010{{$i}})</td>
                                                <td>computology</td>
                                                <td>New</td>
                                                <td>Error</td>
                                                <td>Critical</td>
                                                <td>{{date('F j, Y',strtotime($date))}}</td>
                                                <td>{{date('F j, Y',strtotime($date))}}</td>
                                                <td><a class="btn btn-primary btn-sm" href="{{ url('backoffice/support-center/timeline') }}"><i class="icon icon-screen-full"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>


@section('custom')
<script>
    $('a.sc-view-tickets').addClass('active');
    $('li.support-center-must-open').addClass('nav-item-expanded nav-item-open');

    function xtoogleSearchx() {
        if($('.search-toogle:visible').length == 0) {
            $('.search-toogle').show()
        } else {
            $('.search-toogle').hide()
        }
    }

    function resetFormSearch() {
        $('#search-form-advance').find('input,  textarea, select').val('');
        $('#search-form-advance select').trigger("change");
    }
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
@endsection
@endsection