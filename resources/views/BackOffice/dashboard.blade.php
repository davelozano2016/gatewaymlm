@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card card-body">
                    <div class="media">
                        <div class="align-self-center">
                            <i class="text-brown icon-wallet icon-3x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                            <span class="text-uppercase font-size-sm text-muted">E-wallet Balance</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="card card-body">
                    <div class="media">
                        <div class="align-self-center">
                            <i class="text-success icon-cash3 icon-3x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                            <span class="text-uppercase font-size-sm text-muted">Income</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="card card-body">
                    <div class="media">
                        <div class="align-self-center">
                            <i class="text-info icon-credit-card icon-3x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                            <span class="text-uppercase font-size-sm text-muted">Paid</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="card card-body">
                    <div class="media">
                        <div class="align-self-center">
                            <i class="text-pink icon-piggy-bank icon-3x"></i>
                        </div>

                        <div class="media-body text-right">
                            <h3 class="font-weight-semibold mb-0">₱123,456.00</h3>
                            <span class="text-uppercase font-size-sm text-muted">Pending Amount</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Income Vs Commission</h5>
                        <ul class="nav ">
                            <li class="nav-item dropdown">
                                <a href="#" class=" " data-toggle="dropdown">
                                    <div class="icon-filter3"></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item" data-toggle="tab">Year</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Month</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Day</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="chart-container">
                            <div class="chart" id="google-column"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Payout Overview</h5>
                        <ul class="nav ">
                            <li class="nav-item dropdown">
                                <a href="#" class=" " data-toggle="dropdown">
                                    <div class="icon-filter3"></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item" data-toggle="tab">Year</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Month</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Day</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="chart-container has-scroll text-center">
                            <div class="d-inline-block" id="google-donut"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Joinings</h5>
                        <ul class="nav ">
                            <li class="nav-item dropdown">
                                <a href="#" class=" " data-toggle="dropdown">
                                    <div class="icon-filter3"></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item" data-toggle="tab">Year</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Month</a>
                                    <a href="#" class="dropdown-item" data-toggle="tab">Day</a>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <div class="card-body">
                        <div class="chart-container">
                            <div class="chart" id="google-line"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <!-- Assigned users -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">New Members</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="media-list" style=" overflow-y: scroll;
                        overflow-x: hidden;
                        max-height: 320px;">
                            @for($i=1;$i<=10;$i++) <li class="media">
                                <a href="#" class="mr-3">
                                    <img src="{{asset('assets/images/mikewooting.jpg')}}" width="36" height="36"
                                        class="rounded-circle" alt="">
                                </a>
                                <div class="media-body">
                                    <a href="#" class="media-title font-weight-semibold">Michael Miranda {{$i}}</a>
                                    <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                </div>
                                <div class="align-self-center mr-4">
                                    <a href="#" class="media-title font-weight-semibold">₱123,456.00</a>
                                    <div class="font-size-sm text-muted ">{{date('M j, Y',strtotime(date('Y-m-d')))}}
                                    </div>
                                </div>
                                </li>
                                @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Promotion Tools</h5>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Replica Link</label>
                                    <input type="text" value="{{url('promotion/replica-tools/')}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Lead Capture</label>
                                    <input type="text" value="{{url('promotion/lead-tools/')}}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Team Performance</h6>
                    </div>

                    <div class="card-body ">
                        <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                            <li class="nav-item"><a href="#top-earners" class="nav-link active" data-toggle="tab">Top
                                    Earners</a></li>
                            <li class="nav-item"><a href="#top-recruiters" class="nav-link" data-toggle="tab">Top
                                    Recruiters</a></li>
                            <li class="nav-item"><a href="#package-overview" class="nav-link" data-toggle="tab">Package
                                    Overview</a></li>
                            <li class="nav-item"><a href="#rank-overview" class="nav-link" data-toggle="tab">Rank
                                    Overview</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="top-earners">
                                <ul class="media-list" style=" overflow-y: scroll;
                                overflow-x: hidden;
                                max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <a href="#" class="mr-3">
                                            <img src="{{asset('assets/images/mikewooting.jpg')}}" width="36" height="36"
                                                class="rounded-circle" alt="">
                                        </a>
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">James Alexander
                                                {{$i}}</a>
                                            <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                        </div>

                                        <div class="align-self-center mr-4">
                                            <a href="#" class="media-title font-weight-semibold">₱123,456.00</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="top-recruiters">
                                <ul class="media-list" style=" overflow-y: scroll;
                                overflow-x: hidden;
                                max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <a href="#" class="mr-3">
                                            <img src="{{asset('assets/images/mikewooting.jpg')}}" width="36" height="36"
                                                class="rounded-circle" alt="">
                                        </a>
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">James Alexander
                                                {{$i}}</a>
                                            <div class="font-size-sm text-muted">MLMCOMPUTOLOGY{{$i}}</div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="package-overview">
                                <ul class="media-list" style=" overflow-y: scroll;
                                overflow-x: hidden;
                                max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">Membership {{$i}}</a>
                                            <div class="font-size-sm text-muted">You have {{$i}} Membership package
                                                purchases in your team </div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">{{$i}}</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="rank-overview">
                                <ul class="media-list" style=" overflow-y: scroll;
                                overflow-x: hidden;
                                max-height: 320px;">
                                    @for($i=1;$i<=10;$i++) <li class="media">
                                        <div class="media-body">
                                            <a href="#" class="media-title font-weight-semibold">Bronze</a>
                                            <div class="font-size-sm text-muted">You have 15 Bronze rank in your team
                                            </div>
                                        </div>
                                        <div class="align-self-center mr-4">
                                            <a href="#"
                                                class="media-title font-weight-semibold badge bg-blue-400">15</a>
                                        </div>
                                        </li>
                                        @endfor
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Income & Comission</h6>
                    </div>

                    <div class="card-body ">
                        <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified ">
                            <li class="nav-item"><a href="#income" class="nav-link active" data-toggle="tab">Income</a>
                            </li>
                            <li class="nav-item"><a href="#commission" class="nav-link" data-toggle="tab">Commission</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="income" style="overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                <div class="table-responsive">
                                    <table class="table table-columned">
                                        <tr>
                                            <th>Package Amount</th>
                                            <td>₱123,456.00</td>
                                            <td><span class="badge bg-blue-400">PA</span></td>
                                        </tr>

                                        <tr>
                                            <th>Commission Charges</th>
                                            <td>₱123.00</td>
                                            <td><span class="badge bg-blue-400">CC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Fund Transfer Fee</th>
                                            <td>₱0</td>
                                            <td><span class="badge bg-blue-400">FT</span></td>
                                        </tr>

                                        <tr>
                                            <th>Payout Fee</th>
                                            <td>₱0</td>
                                            <td><span class="badge bg-blue-400">PF</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="commission" style="overflow-y: scroll;
                            overflow-x: hidden;
                            max-height: 320px;">
                                <div class="table-responsive table-columned">
                                    <table class="table">
                                        <tr>
                                            <th>Referral Commission</th>
                                            <td>₱123,456.00</td>
                                            <td><span class="badge bg-blue-400">RC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Rank Commission</th>
                                            <td>₱123.00</td>
                                            <td><span class="badge bg-blue-400">RC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Level Commission</th>
                                            <td>₱0</td>
                                            <td><span class="badge bg-blue-400">LC</span></td>
                                        </tr>

                                        <tr>
                                            <th>Education Fund</th>
                                            <td>₱0</td>
                                            <td><span class="badge bg-blue-400">EF</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <div class="mb-3">
                    <div class="row row-tile no-gutters">
                        <div class="col-3">
                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-display icon-2x"></i>
                                <span>Commission</span>
                            </a>

                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-calculator icon-2x"></i>
                                <span>Compensation</span>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-trophy2 icon-2x"></i>
                                <span>Rank</span>
                            </a>

                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-history icon-2x"></i>
                                <span>Payout</span>
                            </a>
                        </div>

                        <div class="col-3">
                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-credit-card icon-2x"></i>
                                <span>Payment</span>
                            </a>

                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-user-plus icon-2x"></i>
                                <span>Signup</span>
                            </a>
                        </div>

                        <div class="col-3">
                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-envelope icon-2x"></i>
                                <span>Mail</span>
                            </a>

                            <a href="#" type="button" class="btn btn-light btn-block btn-float m-0">
                                <i class="icon-key icon-2x"></i>
                                <span>API Key</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    @section('custom')
    <script>
    $('a.dashboard').addClass('active');
    </script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('assets/js/demo_charts/google/light/lines/lines.js')}}"></script>
    <script src="{{asset('assets/js/demo_charts/google/light/pies/donut.js')}}"></script>
    <script>
    var GoogleColumnBasic = function() {

        var _googleColumnBasic = function() {
            if (typeof google == 'undefined') {
                console.warn('Warning - Google Charts library is not loaded.');
                return;
            }
            google.charts.load('current', {
                callback: function() {

                    // Draw chart
                    drawColumn();

                    // Resize on sidebar width change
                    var sidebarToggle = document.querySelector('.sidebar-control');
                    sidebarToggle && sidebarToggle.addEventListener('click', drawColumn);

                    // Resize on window resize
                    var resizeColumn;
                    window.addEventListener('resize', function() {
                        clearTimeout(resizeColumn);
                        resizeColumn = setTimeout(function() {
                            drawColumn();
                        }, 200);
                    });
                },
                packages: ['corechart']
            });

            // Chart settings
            function drawColumn() {

                // Define charts element
                var line_chart_element = document.getElementById('google-column');

                // Data
                var data = google.visualization.arrayToDataTable([
                    ['Year', 'Income', 'Commission'],
                    ['Feb 2020', 1500, 400],
                    ['Mar 2020', 800, 400],
                    ['Apr 2020', 1200, 400],
                    ['May 2020', 400, 400],
                    ['Jun 2020', 1300, 400],
                    ['Jul 2020', 1400, 400],
                    ['Aug 2020', 1000, 400],
                    ['Sep 2020', 200, 400],
                    ['Oct 2020', 4500, 400],
                    ['Nov 2020', 1500, 400],
                    ['Dec 2020', 2500, 400],
                    ['Jan 2021', 3000, 400],
                ]);


                // Options
                var options_column = {
                    fontName: 'Roboto',
                    height: 325,
                    fontSize: 12,
                    backgroundColor: 'transparent',
                    chartArea: {
                        left: '5%',
                        width: '95%',
                        height: 270
                    },
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: '',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false,
                            color: '#333'
                        },
                        textStyle: {
                            color: '#333'
                        },
                        baselineColor: '#ccc',
                        gridlines: {
                            color: '#eee',
                            count: 10
                        },
                        minValue: 0
                    },
                    hAxis: {
                        textStyle: {
                            color: '#333'
                        }
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            color: '#333'
                        }
                    },
                    series: {
                        0: {
                            color: '#2ec7c9'
                        },
                        1: {
                            color: '#b6a2de'
                        }
                    }
                };

                // Draw chart
                var column = new google.visualization.ColumnChart(line_chart_element);
                column.draw(data, options_column);
            }
        };


        //
        // Return objects assigned to module
        //

        return {
            init: function() {
                _googleColumnBasic();
            }
        }
    }();


    // Initialize module
    // ------------------------------

    GoogleColumnBasic.init();
    </script>

{{-- @php
foreach($query as $d) {
    echo "['".$d->month_name."', ".$d->count."],";
}
@endphp --}}
    {{-- <script src="{{asset('assets/js/demo_charts/c3/c3_bars_pies.js')}}"></script> --}}
    @endsection
    @endsection