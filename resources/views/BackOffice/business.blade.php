@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="row justify-content-center">
                    <div class="col-xl-2">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Business</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-violet icon-cash2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Bonus</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-checkmark-circle2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Paid</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-warning icon-hour-glass3 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Pending</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-stats-growth2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Profit</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified mt-2">
                    <li class="nav-item">
                        <a href="#business-summary" class="nav-link active" data-toggle="tab">Business Summary</a>
                    </li>
                    <li class="nav-item">
                        <a href="#business-transactions" class="nav-link" data-toggle="tab">Business Transaction</a>
                    </li>
                </ul>
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex align-items-start flex-column flex-md-row">
                            <div class="tab-content w-100 order-2 order-md-1">
                                <!-- Business Summary -->
                                <div class="tab-pane fade active show" id="business-summary">
                                    <div class="card card-body row">
                                        <div class="col-md-12">
                                            <div class="row text-right">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-light daterange-predefined">
                                                        <span></span>
                                                    </button>
                                                    <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                                </div>
                                                <div class="col-md-12"><br><br></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h2 class="text-uppercase">BONUS</h2>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Referral
                                                                            Commission</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Rank
                                                                            Commission</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Level
                                                                            Commission</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Binary
                                                                            Commission</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Matching
                                                                            Bonus</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Pool
                                                                            Bonus</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Fast Start
                                                                            Bonus</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Vacation
                                                                            Fund</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Education
                                                                            Fund</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Car
                                                                            Fund</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-info rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">House
                                                                            Fund</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h2 class="text-uppercase">Income</h2>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Package
                                                                            Amount</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Purchase
                                                                            Amount</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Fund
                                                                            Transfer
                                                                            Fee</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Payout
                                                                            Fee</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span class="text-uppercase font-size-sm">Commission
                                                                            Charges</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h2 class="text-uppercase">&nbsp;</h2>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-success rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span
                                                                            class="text-uppercase font-size-sm">Paid</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card card-body border-left-warning rounded-left-0">
                                                                <div class="media">

                                                                    <div class="media-body">
                                                                        <h3 class="font-weight-semibold mb-0">₱
                                                                            {{ number_format('100', 2) }}</h3>
                                                                        <span
                                                                            class="text-uppercase font-size-sm">Pending</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Business Transactions -->
                                <div class="tab-pane fade" id="business-transactions">
                                    <div class="card card-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Username</label>
                                                            <input type="text" class="form-control"
                                                                placeholder="Search by first name, last name, skype ID, email ID,mobile no.">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <div class="form-group">
                                                            <label class="">Type</label>
                                                            <select
                                                                class="form-control form-control-select2 form-control-sm select"
                                                                data-container-css-class="text-primary-700"
                                                                multiple="multiple">
                                                                <option value="Income">Income</option>
                                                                <option value="Bonus">Bonus</option>
                                                                <option value="Paid">Paid</option>
                                                                <option value="Pending">Pending</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <div class="form-group">
                                                            <label class="">Category</label>
                                                            <select class="form-control form-control-select2 form-control-sm select" data-container-css-class="" multiple="multiple">
                                                                <option value="Package Amount">Package Amount</option>
                                                                <option value="Purchase Amount">Purchase Amount</option>
                                                                <option value="Fund Transfer Fee">Fund Transfer Fee
                                                                </option>
                                                                <option value="Commission Charges">Commission Charges
                                                                </option>
                                                                <option value="Payout Fee">Payout Fee</option>
                                                                <option value="Referral Commission">Referral Commission
                                                                </option>
                                                                <option value="Rank Commission">Rank Commission</option>
                                                                <option value="Level Commission">Level Commission
                                                                </option>
                                                                <option value="Binary Commission">Binary Commission
                                                                </option>
                                                                <option value="Matching Bonus">Matching Bonus</option>
                                                                <option value="Pool Bonus">Pool Bonus</option>
                                                                <option value="Fast Start Bonus">Fast Start Bonus
                                                                </option>
                                                                <option value="Vacation Fund">Vacation Fund</option>
                                                                <option value="Education Fund">Education Fund</option>
                                                                <option value="Car Fund">Car Fund</option>
                                                                <option value="House Fund">House Fund</option>
                                                                <option value="Paid">Paid</option>
                                                                <option value="Pending">Pending</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <label for="">&nbsp;</label>
                                                        <div class="form-group">
                                                            <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12">
                                                <table class="table table-columned datatable-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:1px;">#</th>
                                                            <th>Name</th>
                                                            <th>Category</th>
                                                            <th>Type</th>
                                                            <th>Amount</th>
                                                            <th>Transaction Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @for($i=1;$i<=10;$i++)
                                                        <tr>
                                                            <td>{{ $i }}.</td>
                                                            <td>John Doe (username{{$i}}) </td>
                                                            <td>Commission Charges</td>
                                                            <td>Income</td>
                                                            <td>₱ {{ number_format('100', 2) }}</td>
                                                            <td>{{ date('F j, Y') }}</td>
                                                        </tr>
                                                        @endfor
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
    $('a.business').addClass('active');
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        });    
    });
    </script>

    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_multiselect.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>

    @endsection
    @endsection