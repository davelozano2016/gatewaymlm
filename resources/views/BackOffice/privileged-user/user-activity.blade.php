@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
            <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Privileged User</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="">Username</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                                    <label for="">&nbsp;</label>
                                        <div class="form-group">
                                            <button class="btn bg-slate"><i class="icon-search4"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table datatable-responsive table-columned">
                                   <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Activity</th>
                                            <th>Order Date</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>John Doe</td>
                                            <td>12345</td>
                                            <td>email</td>
                                            <td>NA</td>
                                            <td>{{ date('M j, Y g:i:s A')}}</td>
                                        </tr>
                                        @endfor
                                   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>
<div class="modal fade" id="view_invoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-list2 mr-2"></i> View Invoice</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div style="border: 1px solid #bbb; padding:10px;">
                <h3><b>Order Number: 0001</b><hr></h3>
                <p><b>Date Added:</b> {{date('M j, Y g:i:s A')}}</p>
                <p><b>Shipping Method:</b> Free Shipping</p>
                <p><b>Payment Method:</b> Bank Transfer</p>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <h4><b>Payment Address</b></h4>
                        <p>John Doe</p>
                        <p>Address</p>
                        <p>City</p>
                        <p>Philippines</p>
                    </div>
                    <div class="col-md-6">
                        <h4><b>Shipping Address</b></h4>
                        <p>John Doe</p>
                        <p>Address</p>
                        <p>City</p>
                        <p>Philippines</p>
                    </div>
                </div>
                <hr>
                <h4><b>Order Products</b></h4>
                <table class="table table-columned">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Pair Value</th>
                            <th>Price</th>
                            <th>Total Pair Value</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Product 1</td>
                            <td>1</td>
                            <td>₱ {{ number_format('100', 2) }}</td>
                            <td>₱ {{ number_format('1000', 2) }}</td>
                            <td>₱ {{ number_format('100', 2) }}</td>
                            <td>₱ {{ number_format('1200', 2) }}</td>
                        </tr>
                        <tr><td colspan="6"><hr></td></tr>
                        <tr>
                            <td colspan="5" class="text-right">Sub Total</td>
                            <td>₱ {{ number_format('1200', 2) }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-right">Free Shipping</td>
                            <td>₱ {{ number_format('0', 2) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn bg-slate" >Print</button>
        </div>
      </div>
    </div>
</div>

@section('custom')
<script>
    $('a.pu-user-activity').addClass('active');
    $('li.privileged-user-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection