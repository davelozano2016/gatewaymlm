@extends('layouts.backoffice')
@section('container')
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
            
            <div class="content d-flex justify-content-center align-items-center">
            <!-- Login card -->
            <form class="login-form" id="demo-form" method="POST" action="{{ url('backoffice/login/secureLogin') }}">
            @csrf
                <div class="content d-flex justify-content-center align-items-center">
                    <img src="{{ asset('assets/images/logo.png') }}" style="width:250px" alt="">
                </div>
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">Login to your account</h5>
                            <span class="d-block text-muted">Your credentials</span>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="g-recaptcha btn btn-primary btn-block" data-sitekey="6Ld-p_AaAAAAADBsBt4agSjazxyqABh3ElQpSu4h" data-callback='onSubmit' data-action='submit' >Sign in</button>
                        </div>

                        <span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a></span>
                    </div>
                </div>
            </form>
            <!-- /login card -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
@endsection

@section('custom')

@endsection