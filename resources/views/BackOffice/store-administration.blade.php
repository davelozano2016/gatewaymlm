@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="row  justify-content-center">

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-primary icon-cash2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">Pending Payments</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-wallet icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">Processing</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-warning icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">On-hold</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">Completed</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-violet icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">Cancelled</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-danger icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">22</h3>
                                    <span class="text-uppercase font-size-sm">Failed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
                <div class="card card-body">
                    <div class="row">

                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="">Status</label>
                                <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                    <option value="All">All</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Processing">Processing</option>
                                    <option value="On-hold">On-hold</option>
                                    <option value="Completed">Completed</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Failed">Failed</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <label for="">&nbsp;</label>
                            <div class="form-group">
                                <button class="btn bg-slate"><i class="icon-search4"></i></button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-columned datatable-responsive">
                        <thead>
                            <tr>
                                <th style="width:1px">#</th>
                                <th>Order</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=1;$i<=10;$i++) <tr>
                                <td>{{$i}}.</td>
                                <td>#12343</td>
                                <td>{{ date('M j, Y g:i:s A')}}</td>
                                <td>Processing</td>
                                <td>₱ {{ number_format('100', 2) }}</td>
                                <td><button class="btn btn-success btn-sm" data-toggle="modal" data-target="#view_history" ><i class="icon-eye"></i></button></td>
                                </tr>
                                @endfor
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
<div class="modal" id="view_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Order Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <table class="table table-columned">
                            <tbody>
                                <tr>
                                    <td><b>Status:</b></td>
                                    <td colspan="2">
                                        <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                            <option value="Pending" selected>Pending</option>
                                            <option value="Processing">Processing</option>
                                            <option value="On-hold">On-hold</option>
                                            <option value="Completed">Completed</option>
                                            <option value="Cancelled">Cancelled</option>
                                            <option value="Failed">Failed</option>
                                        </select>
                                    </td>
                                    <td>
                                        <button class="btn btn-primary btn-sm">Update</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Order Number:</b></td>
                                    <td colspan="3">CUST-3805214</td>
                                </tr>
                                <tr>
                                    <td><b>Customer Name:</b></td>
                                    <td colspan="3">John David</td>
                                </tr>
                                <tr>
                                    <td><b>Date:</b></td>
                                    <td colspan="3">{{ date('F d, Y - h:i')}}</td>
                                </tr>
                                <tr>
                                    <td><b>Email Address:</b></td>
                                    <td colspan="3">test@email.com</td>
                                </tr>
                                <tr>
                                    <td><b>Phone:</b></td>
                                    <td colspan="3">11111111111</td>
                                </tr>
                                <tr>
                                    <td><b>Billing:</b></td>
                                    <td colspan="3">Addreess</td>
                                </tr>
                                <tr>
                                    <td><b>Shipping:</b></td>
                                    <td colspan="3">Addreess</td>
                                </tr>
                                
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td><b>Product</b></td>
                                    <td><b>Cost</b></td>
                                    <td><b>Qty</b></td>
                                    <td><b>Total</b></td>
                                </tr>
                                <tr>
                                    <td>Gold Package A</td>
                                    <td></td>
                                    <td>x1</td>
                                    <td>₱ {{ number_format('100', 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><b>Free Shipping</b></td>
                                    <td>₱ {{ number_format('0', 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><b>Items Subtotal:</b></td>
                                    <td>₱ {{ number_format('100', 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><b>Shipping:</b></td>
                                    <td>₱ {{ number_format('0', 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><b>Order Total:</b></td>
                                    <td>₱ {{ number_format('100', 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><b>Paid By Customer:</b></td>
                                    <td>₱ {{ number_format('100', 2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@section('custom')
<script>
    $('a.store-administration').addClass('active');
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>ript>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
@endsection
@endsection