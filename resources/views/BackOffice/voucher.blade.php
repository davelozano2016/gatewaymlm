@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

       
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
         
            <div class="col-xl-12">
                
                <div class="card card-body">
                    <table class="table table-columned datatable-responsive">
                        <thead>
                            <tr>
                                <th style="width:1px;">#</th>
                                <th>Reference No.</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Amount</th>
                                <th>Country</th>
                                <th>IP</th>
                                <th>Payout</th>
                                <th>Date Requested</th>
                                <th></th>          
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1;@endphp
                            @foreach($encashments as $encashment)
                                <tr>
                                    <td style="width:1px">{{$i++}}.</td>
                                    <td>{{$encashment->reference}}</td>
                                    <td>{{$encashment->firstname.' '.$encashment->surname.' '.$encashment->id_number}}</td>
                                    <td>{{$encashment->request_type}}</td>
                                    <td>{{$encashment->currency_symbol}}{{number_format($encashment->request_bonus,2)}}</td>
                                    <td>{{$encashment->country_code}}</td>
                                    <td>{{$encashment->ip_address}}</td>
                                    <td>{{$encashment->currency_symbol}}{{number_format($encashment->payout,2)}}</td>
                                   
                                    <td>{{$encashment->created_at}}</td>
                                    <td>
                                        <div><a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i class="icon-gear"></i></a><div class="dropdown-menu">
                                            <a href="{{url('backoffice/payout/print')}}/{{Crypt::encryptString($encashment->encash_id)}}" class="dropdown-item"><i class="icon-printer"></i> Print Encashment</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                     
                </div>

            </div>
        </div>
    </div>



    <!-- /basic modal -->


    @section('custom')
    <script>
        $('a.payout').addClass('active');
        $(document).ready(function () {
            $('#example').DataTable({
                responsive: true
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust()
                    .responsive.recalc();
            });
        });

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_multiselect.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>

    @endsection
    @endsection
