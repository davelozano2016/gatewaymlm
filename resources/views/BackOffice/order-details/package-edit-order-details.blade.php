@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                        <span class="breadcrumb-item">Order History</span>
                        <span class="breadcrumb-item">Details</span>
                        <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <div class="row">

                   
                        <div class="col-md-12">
                            <form action="{{url('backoffice/order-details/package-edit-order-details/update')}}" method="POST">
                            @csrf
                            <input type="hidden" name="reference" value="{{$title}}">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="title">Transaction Details</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Transaction Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="To Pay">To Pay</option>
                                                    <option value="To Ship">To Ship</option>
                                                    <option value="To Pickup">To Pickup</option>
                                                    <option value="To Recieve">To Recieve</option>
                                                    <option value="Completed">Completed</option>
                                                    <option value="Cancelled">Cancelled</option>
                                                    <option value="Return Refund">Return Refund</option>
                                                </select>
                                            </div>
                                        </div>
    
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Order Status</label>
                                                <select name="order_status" class="form-control">
                                                    <optgroup label="To Pay">
                                                        <option value="Payment Pending">Payment Pending</option>
                                                        <option value="Processing">Processing</option>
                                                        <option value="Verifying">Verifying</option>
                                                        <option value="Verified">Verified</option>
                                                    </optgroup>
    
                                                    <optgroup label="To Ship">
                                                       
                                                    </optgroup>
    
                                                    <optgroup label="To Receive">
                                                        <option value="Attempt to Deliver">Attempt to Deliver</option>
                                                        <option value="Delivered">Delivered</option>
                                                    </optgroup>
    
                                                    <optgroup label="To Pickup">
                                                        <option value="To Claim">To Claim</option>
                                                        <option value="Completed">Completed</option>
                                                    </optgroup>

                                                    <optgroup label="Cancelled">
                                                        <option value="Cancelled">Cancelled</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
    
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Order Note</label>
                                                <textarea class="form-control" name="notes" style="resize:none" cols="30" rows="5"></textarea>
                                            </div>
                                        </div>
    
                                        <div class="col-md-12">
                                            <button class="btn float-right btn-primary">Save Changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="title">Billing Details <i class="icon-pencil float-right"></i></h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing To</label>
                                            <input type="text" value="{{$packages[0]->billing_firstname}} {{$packages[0]->billing_surname}}" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Email Address</label>
                                            <input type="text" value="{{$packages[0]->billing_email}}" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Contact Number</label>
                                            <input type="text" value="{{$packages[0]->billing_contact}}"  class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Address 1</label>
                                            <input type="text" value="{{$packages[0]->billing_address1}}" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Address 2</label>
                                            <input type="text" value="{{$packages[0]->billing_address2}}" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing City</label>
                                            <input type="text" value="{{$packages[0]->billing_city}}"  class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing State</label>
                                            <input type="text" value="{{$packages[0]->billing_state}}"  class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Zipcode</label>
                                            <input type="text" value="{{$packages[0]->billing_zipcode}}"  class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Billing Notes</label>
                                            <textarea class="form-control" readonly style="resize:none" cols="30" rows="5">{{$packages[0]->billing_notes}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="title">Shipping Details <i class="icon-pencil float-right"></i></h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping To</label>
                                            <input type="text" value="{{$packages[0]->shipping_firstname}} {{$packages[0]->shipping_surname}}" class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping Email Address</label>
                                            <input type="text" value="{{$packages[0]->shipping_email}}" class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping Contact Number</label>
                                            <input type="text" value="{{$packages[0]->shipping_contact}}"  class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping Address 1</label>
                                            <input type="text" value="{{$packages[0]->shipping_address1}}" class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping Address 2</label>
                                            <input type="text" value="{{$packages[0]->shipping_address2}}" class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping City</label>
                                            <input type="text" value="{{$packages[0]->shipping_city}}"  class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping State</label>
                                            <input type="text" value="{{$packages[0]->shipping_state}}"  class="form-control" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Shipping Zipcode</label>
                                            <input type="text" value="{{$packages[0]->shipping_zipcode}}"  class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>




    @section('custom')
    <script>
        $('a.od-order-history').addClass('active');
        $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection
