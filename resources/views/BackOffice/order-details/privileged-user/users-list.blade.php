@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Privileged User</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
            
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">User Details</h6>
                    <div class="header-elements">
                        <!-- Edit Mode -->
                        <!-- <button class="btn btn-info">Add New Category</button> -->
                    </div>
                </div>
                <div class="row">
                    <div class="card-body">
                        <form action="">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Username:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">First Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Last Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Email:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Mobile Number:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Confirm Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="" id="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-info">Add</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card">
                <div class="card-header header-elements-inline bg-slate">
                    <h6 class="card-title">Manage {{$title}}</h6>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-danger">Delete</button>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                         <tr>
                                            <th style="width:1px">#</th>
                                            <th style="width:1px"><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Mobile No.</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                         @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{$i}}.</td>
                                                <td><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></td>
                                                <td>Name {{$i}}</td>
                                                <td>Username</td>
                                                <td>Mobile Number</td>
                                                <td>Email</td>
                                                <td>
                                                    <div class="btn-group">
                                                    <button class="btn btn-success" data-toggle="modal" data-target="#modal_permission"><i class="icon icon-pencil7"></i></button>
                                                    <button class="btn btn-warning" data-toggle="modal" data-target="#modal_cpassword"><i class="icon icon-lock"></i></button>
                                                    <button class="btn btn-info" data-toggle="modal" data-target="#modal_edit"><i class="icon icon-pencil7"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                         @endfor
                                    </tbody>
                                 </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="">
<div id="modal_edit" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">First Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Last Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Email:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Mobile Number:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="" id="">
                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b>Save</button>
            </div>
        </div>
    </div>
</div>
</form>
<form action="">
<div id="modal_permission" class="modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Set Permission</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Dashboard:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Register:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">E-Commerce Store:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Store Administration:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Business:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">E-Wallet:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Payout:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">E-Pin:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Subscription Renewal:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Mailbox:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label">Support Center:</label>
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="gender" data-fouc="">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">CRM:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Dashboard
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            View Lead
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Graph
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Order Details:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Order History
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Order Approval
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Network:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Genealogy Tree
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Sponsor Tree
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Tree View
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Downline Members
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Referral Members
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Profile Management:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Profile View
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Members List
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            KYC Details
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Reports:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Profile
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Activate / Deactivate
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Joining
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Commission
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Total Bonus
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Top Earners
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Payout
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Rank Performance
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Rank Achievers
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            E-Pin Transfer
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Subscription Report
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Tools:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Auto Responder
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Upload Materials
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            News
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            FAQs
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="d-block">Settings:</label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Commission Settings
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Company Profile
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Content Management
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            Mail Content
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="personal-pv form-check-input-styled" data-fouc>
                                            SMS Content
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b>Set Permission</button>
            </div>
        </div>
    </div>
</div>
</form>
<form action="">
<div id="modal_cpassword" class="modal" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Username:</label>
                    <div class="col-md-8">
                    <input type="text" class="form-control" value="Test" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Password:</label>
                    <div class="col-md-8">
                    <input type="password" class="form-control" name="" id="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label">Confirm Password:</label>
                    <div class="col-md-8">
                    <input type="password" class="form-control" name="" id="">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-teal-400"><b>Save</button>
            </div>
        </div>
    </div>
</div>
</form>
@section('custom')
<script>
    $('a.pu-users-list').addClass('active');
    $('li.privileged-user-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection