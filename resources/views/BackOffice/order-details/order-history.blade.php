@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Order Details</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->

    <div class="content">
        <div class="d-flex align-items-start flex-column flex-md-row">
            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified mt-2">
                    <li class="nav-item">
                        <a href="#statement" class=" nav-link active" data-toggle="tab">Purchase Package Pins</a>
                    </li>
                    <li class="nav-item">
                        <a href="#purchase-wallet" class=" nav-link" data-toggle="tab">Purchase Product Pins</a>
                    </li>
                </ul>
                <div class="col-md-12">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="tab-content w-100 order-2 order-md-1">
                            <!-- Statement -->
                            <div class="tab-pane  active show" id="statement">

                            <div class="row">
                                <div class="w-100 overflow-auto order-2 order-md-1"
                                    style="overflow-x: hidden !important;">
                                    
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-columned datatable-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:1px">#</th>
                                                            <th>Customer</th>
                                                            <th style="width: 1px;">Reference</th>
                                                            <th>Method of Payment</th>
                                                            <th>Distributor ID</th>
                                                            <th style="width:1px">Items</th>
                                                            <th style="width:1px">Total</th>
                                                            <th class="text-center" style="width: 1px;"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1;?>
                                                        @foreach($packages_orders as $package_order)
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>
                                                                <label>{{$package_order->full_name}}</label>
                                                                <div class="text-muted text-uppercase font-size-sm">
                                                                    @if($package_order->order_status == 'To Pay')
                                                                    <span class="badge badge-warning">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'To Ship')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'To Pickup')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'To Receive')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'Completed')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'Cancelled')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @elseif($package_order->order_status == 'Return Refund')
                                                                        <span class="badge badge-primary">{{$package_order->order_status}}</span>
                                                                    @endif
                                                                </div>
                                                            </td>
                                                            <td>{{$package_order->reference}}</td>
                                                            <td>{{$package_order->method_of_payment}}</td>
                                                            <td>{{$package_order->distributor_id}}</td>
                                                            @foreach($total_package_items[$package_order->packages_id] as $price)
                                                            <td> {{$price->items}}</td>
                                                            @endforeach
                                                            @foreach($total_package_price[$package_order->packages_id] as $price)
                                                            <td> {{$package_order->currency_symbol}}{{number_format($price->total,2)}}</td>
                                                            @endforeach
                                                            <td style="width:1px" class="text-center">
                                                                <a class="list-icons-item" href="javascript:void(0)"
                                                                    data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                                <div class="dropdown-menu">
                                                                    <a href="{{url('backoffice/order-details/package-track-order')}}/{{Crypt::encryptString($package_order->reference)}}" class="dropdown-item"><i class="icon-truck"></i>
                                                                        Track Order</a>
            
                                                                    <a href="{{url('backoffice/order-details/package-edit-order-details')}}/{{Crypt::encryptString($package_order->reference)}}" class="dropdown-item"><i class="icon-pencil"></i>
                                                                            Edit Order Details</a>
            
                                                                    <a href="{{url('backoffice/order-details/package-view-orders')}}/{{Crypt::encryptString($package_order->reference)}}" class="dropdown-item"><i class="icon-cart"></i>
                                                                            View Order</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>

                            <!-- Purchase Wallet -->
                            <div class="tab-pane" id="purchase-wallet">
                                <div class="row">
                                    <div class="w-100 overflow-auto order-2 order-md-1"
                                        style="overflow-x: hidden !important;">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-columned datatable-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:1px">#</th>
                                                                <th>Customer</th>
                                                                <th style="width: 1px;">Reference</th>
                                                                <th>Method of Payment</th>
                                                                <th>Distributor ID</th>
                                                                <th style="width:1px">Items</th>
                                                                <th style="width:1px">Total</th>
                                                                <th class="text-center" style="width: 1px;"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 1;?>
                                                            @foreach($orders as $order)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>
                                                                    <label>{{$order->full_name}}</label>
                                                                    <div class="text-muted text-uppercase font-size-sm">
                                                                        @if($order->order_status == 'To Pay')
                                                                            <span class="badge badge-warning">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'To Ship')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'To Pickup')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'To Receive')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'Completed')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'Cancelled')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @elseif($order->order_status == 'Return Refund')
                                                                            <span class="badge badge-primary">{{$order->order_status}}</span>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                                <td>{{$order->reference}}</td>
                                                                <td>{{$order->method_of_payment}}</td>
                                                                <td><a href="">{{$order->distributor_id}}</a></td>
                                                                @foreach($total_items[$order->pin_id] as $price)
                                                                <td> {{$price->items}}</td>
                                                                @endforeach
                                                                @foreach($total_price[$order->pin_id] as $price)
                                                                <td> {{$order->currency_symbol}}{{number_format($price->total,2)}}</td>
                                                                @endforeach
                                                                <td style="width:1px" class="text-center">
                                                                    <a class="list-icons-item" href="javascript:void(0)"
                                                                        data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                                    <div class="dropdown-menu">
                                                                        <a href="{{url('backoffice/order-details/track-order')}}/{{Crypt::encryptString($order->reference)}}" class="dropdown-item"><i class="icon-truck"></i>
                                                                            Track Order</a>
                
                                                                        <a href="{{url('backoffice/order-details/edit-order-details')}}/{{Crypt::encryptString($order->reference)}}" class="dropdown-item"><i class="icon-pencil"></i>
                                                                                Edit Order Details</a>
                
                                                                        <a href="{{url('backoffice/order-details/view-orders')}}/{{Crypt::encryptString($order->reference)}}" class="dropdown-item"><i class="icon-cart"></i>
                                                                                View Order</a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
        $('a.od-order-history').addClass('active');
        $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection
