@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Order Details</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">

                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mb-4">
                                    <img src="{{asset('assets/images/logo.png')}}" class="mb-3 mt-2" alt="" style="width: 150px;">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="mb-4">
                                    <div class="text-sm-right">
                                        <h4 class="text-primary mb-2 mt-lg-2">Invoice #{{$products[0]->reference}}</h4>
                                        <ul class="list list-unstyled mb-0">
                                            <li>Order Date: <span class="font-weight-semibold">{{date('F j, Y',strtotime($products[0]->order_date))}}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="row">
                                <div class="col-md-4 text-center col-12">
                                    <h5 class="title">Billing Details</h5>
                                     <ul class="list list-unstyled mb-0">
                                        <li><h6>Full Name: {{$products[0]->billing_firstname}} {{$products[0]->billing_surname}}</h6> </li>
                                        <li><h6>Address: {{$products[0]->billing_address1}} {{$products[0]->billing_address2}} {{$products[0]->billing_city}} {{$products[0]->billing_state}} {{$products[0]->billing_country}}, {{$products[0]->billing_zipcode}}</h6> </li>
                                    </ul>
                                </div>

                                <div class="col-md-4 text-center col-12">
                                    <h5 class="title">Shipping Details</h5>
                                     <ul class="list list-unstyled mb-0">
                                        <li><h6>Full Name: {{$products[0]->shipping_firstname}} {{$products[0]->shipping_surname}}</h6> </li>
                                        <li><h6>Address: {{$products[0]->shipping_address1}} {{$products[0]->shipping_address2}} {{$products[0]->shipping_city}} {{$products[0]->shipping_state}} {{$products[0]->shipping_country}}, {{$products[0]->shipping_zipcode}}</h6> </li>
                                    </ul>
                                </div>

                                <div class="col-md-4 text-center col-12">
                                    <h5 class="title">Payment Details</h5>
                                     <ul class="list list-unstyled mb-0">
                                        <li><h6>Method of Payment: {{$products[0]->method_of_payment}}</h6> </li>
                                        <li><h6>Order Status: {{$products[0]->order_status}}</h6> </li>
                                        @if($products[0]->local_pickup == null)
                                        @else
                                            <li><h6>Pickup Location: {{$products[0]->local_pickup}}</h6> </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th style="width:1px"></th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ;?>
                                @foreach($products as $product)
                                <tr>
                                    <td><?=$i++?></td>
                                    <td><img style="width:60px;height:60px" src="{{url('assets/products/'.$product->product_image)}}" alt=""></td>
                                    <td>{{$product->product_title}}</td>
                                    <td>{{$product->currency_symbol.number_format(2,2)}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td>{{$product->currency_symbol.number_format(2 * $product->quantity,2)}}</td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /traffic sources -->

            </div>
        </div>
        <!-- /main charts -->
    </div>




    @section('custom')
    <script>
        $('a.od-order-history').addClass('active');
        $('li.order-details-must-open').addClass('nav-item-expanded nav-item-open');

    </script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection
