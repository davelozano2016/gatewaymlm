@extends('layouts.BackOffice.app')
@section('container')


<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Inner container -->
    <div class="d-md-flex align-items-md-start">

        <!-- Left sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left border-0 shadow-0 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                @include('BackOffice.mailbox.sub-menu')
            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /left sidebar component -->


        <!-- Right content -->
        <div class="flex-fill overflow-auto">

            <!-- Single line -->
            <div class="card">
                <div class="card-body">
                    <input type="text" class="form-control" placeholder="Search mail...">
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <h6 class="card-title"><input type="checkbox" class="form-input-styled" data-fouc> </h6>
                    <div class="col-md-10">
                        <div class="row">

                            <div class="col-md-2">
                                <button class="btn btn-light btn-icon">Delete All</button>
                            </div>
                        </div>

                    </div>
                    <div class="header-elements">
                        <div class="navbar-text ml-lg-auto"><span class="font-weight-semibold">1-50</span> of <span class="font-weight-semibold">528</span></div>

                        <div class="ml-lg-3 mb-3 mb-lg-0">

                            <div class="btn-group">
                                <button type="button" class="btn btn-light btn-icon disabled"><i class="icon-arrow-left12"></i></button>
                                <button type="button" class="btn btn-light btn-icon"><i class="icon-arrow-right13"></i></button>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-inbox">
                        <tbody data-link="row" class="rowlink">
                            <tr class="unread">
                                <td class="table-inbox-checkbox rowlink-skip">
                                    <input type="checkbox" class="form-input-styled" data-fouc>
                                </td>
                                <td class="table-inbox-star rowlink-skip">
                                    <a href="#">
                                        <i class="icon-star-empty3 text-muted"></i>
                                    </a>
                                </td>
                                <td class="table-inbox-image rowlink-skip">
                                    <span class="btn bg-warning-400 rounded-circle btn-icon btn-sm">
                                        <span class="letter-icon"></span>
                                    </span>
                                </td>
                                <td class="table-inbox-name">
                                    <a href="{{ url('backoffice/mailbox/mail-read') }}">
                                        <div class="letter-icon-title text-default">James Alexander</div>
                                    </a>
                                </td>
                                <td class="table-inbox-message">
                                    <span class="table-inbox-subject"><span class="badge bg-success mr-2">Promo</span> There are three whales and three boats &nbsp;-&nbsp;</span>
                                    <span class="text-muted font-weight-normal">And one of the boats (presumed to contain the missing leg in all its original integrity) is being crunched by the jaws of the foremost whale</span>
                                </td>
                                <td class="table-inbox-attachment">
                                    <i class="icon-attachment text-muted"></i>
                                </td>
                                <td class="table-inbox-time">
                                    10:21 pm
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /table -->
            </div>
            <!-- /single line -->
        </div>
        <!-- /right content -->
    </div>
    <!-- /inner container -->
</div>

@section('custom')
<script>
    $('a.inbox').addClass('active');
    $('a.m-sent-items').addClass('active');
    $('li.mailbox-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{ asset('assets/js/plugins/extensions/rowlink.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/mail_list.js') }}"  ></script>
@endsection
@endsection