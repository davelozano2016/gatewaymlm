@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Inner container -->
    <div class="d-md-flex align-items-md-start">

        <!-- Left sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left border-0 shadow-0 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                @include('BackOffice.mailbox.sub-menu')
            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /left sidebar component -->


        <!-- Right content -->
        <div class="flex-fill overflow-auto">
            <!-- Single mail -->
							<div class="card">

								<!-- Action toolbar -->
								<div class="navbar navbar-light bg-light navbar-expand-lg border-bottom-0 py-lg-2 rounded-top">
									<div class="text-center d-lg-none w-100">
										<button type="button" class="navbar-toggler w-100 h-100" data-toggle="collapse" data-target="#inbox-toolbar-toggle-write">
											<i class="icon-circle-down2"></i>
										</button>
									</div>

									<div class="navbar-collapse text-center text-lg-left flex-wrap collapse" id="inbox-toolbar-toggle-write">

										<div class="mt-3 mt-lg-0 mr-lg-3">
											<button type="button" class="btn bg-blue"><i class="icon-paperplane mr-2"></i> Send mail</button>
										</div>
									</div>
								</div>
								<!-- /action toolbar -->


								<!-- Mail details -->
								<div class="table-responsive">
									<table class="table">
										<tbody>
											<tr>
												<td class="" style="width: 150px">
													<div class="py-2 mr-sm-3">To:</div>
												</td>
												<td class="">
													<div class="d-sm-flex flex-sm-wrap">
                                                        <select name="" class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                            <option value="" selected> -- Select User -- </option>
                                                            <option value="Single User">Single User</option>
                                                            <option value="All Users">All Users</option>
                                                        </select>
													</div>
												</td>
                                            </tr>
                                            <tr class="single-user" hidden>
                                                <td class="align-top py-0" style="width: 150px">
                                                    <div class="py-2 mr-sm-3">Single User:</div>
                                                </td>
                                                <td class="align-top py-0">
                                                    <div class="d-sm-flex flex-sm-wrap">
													    <input type="text" class="form-control py-2 px-0 border-0 rounded-0" placeholder="Single User">
                                                    </div>
                                                </td>
                                            </tr>
											<tr>
												<td class="align-top py-0">
													<div class="py-2 mr-sm-3">Subject:</div>
												</td>
												<td class="align-top py-0">
													<input type="text" class="form-control py-2 px-0 border-0 rounded-0" placeholder="Add subject">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- /mail details -->


								<!-- Mail container -->
								<div class="card-body p-0">
									<div class="overflow-auto mw-100">
										<div class="summernote summernote-borderless">

										</div>
									</div>
								</div>
								<!-- /mail container -->


								<!-- Attachments -->
								{{-- <div class="card-body border-top">
									<h6 class="mb-0">2 Attachments</h6>

									<ul class="list-inline mb-0">
										<li class="list-inline-item">
											<div class="card bg-light py-2 px-3 mt-3 mb-0">
												<div class="media my-1">
													<div class="mr-3 align-self-center"><i class="icon-file-pdf icon-2x text-danger-400 top-0"></i></div>
													<div class="media-body">
														<div class="font-weight-semibold">new_december_offers.pdf</div>

														<ul class="list-inline list-inline-condensed mb-0">
															<li class="list-inline-item text-muted">174 KB</li>
															<li class="list-inline-item"><a href="#">View</a></li>
															<li class="list-inline-item"><a href="#">Download</a></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
										<li class="list-inline-item">
											<div class="card bg-light py-2 px-3 mt-3 mb-0">
												<div class="media my-1">
													<div class="mr-3 align-self-center"><i class="icon-file-pdf icon-2x text-danger-400 top-0"></i></div>
													<div class="media-body">
														<div class="font-weight-semibold">assignment_letter.pdf</div>

														<ul class="list-inline list-inline-condensed mb-0">
															<li class="list-inline-item text-muted">736 KB</li>
															<li class="list-inline-item"><a href="#">View</a></li>
															<li class="list-inline-item"><a href="#">Download</a></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div> --}}
								<!-- /attachments -->

							</div>
							<!-- /single mail -->
        </div>
        <!-- /right content -->

    </div>
    <!-- /inner container -->

</div>


@section('custom')
<script>
    $('a.inbox').addClass('active');
    $('a.m-composer-mail').addClass('active');
    $('li.mailbox-must-open').addClass('nav-item-expanded nav-item-open');
    $('.select-user').change(function() {
        var select = $('.select-user').val(); 
        if(select == 'Single User') {
            $('tr.single-user').removeAttr('hidden');
        } else {
            $('tr.single-user').attr('hidden',true);
        }
    }) 
</script>
<!-- Theme JS files -->
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/mail_list_write.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<!-- /theme JS files -->
@endsection
@endsection