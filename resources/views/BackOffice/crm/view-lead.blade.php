@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a class="breadcrumb-item">CRM</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">

        <div class="col-xl-4 col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold" id="header-c-title">Lead Details</span>
                    <span style="display:none"><button class="btn btn-info btn-sm"><i class="icon-plus2"></i></button></span>
                </div>
                <div class="card-body">
                    <form>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">First Name:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-form-c">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Last Name:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-form-c">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Email:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-form-c">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Skype ID:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-form-c">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Mobile No.:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-form-c">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Country:</label>
                                <div class="col-md-9">
                                    <select class="form-control form-control-select2 input-form-c">
                                        <option value="">Select Country</option>
                                        <option>Philippines</option>
                                        <option>United Kingdom</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Level of Interest:</label>
                                <div class="col-md-9">
                                    <select class="form-control form-control-select2 input-form-c">
                                        <option value="">Select Level</option>
                                        <option>Very Interested</option>
                                        <option>Interested</option>
                                        <option>Not That Interested</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Lead Status:</label>
                                <div class="col-md-9">
                                    <select class="form-control form-control-select2 input-form-c">
                                        <option value="">Select Status</option>
                                        <option>Ongoing</option>
                                        <option>Accepted</option>
                                        <option>Rejected</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Description:</label>
                                <div class="col-md-9">
                                    <textarea class="form-control input-form-c">Testing</textarea>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-primary btn-form-c">Add Lead</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>

        <div class="col-xl-8 col-md-12">

            <!-- Traffic sources -->
            <div class="card">
                <div class="form-group">
                <button class="btn btn-info float-left mt-3 ml-3" type="button">Block</button>
                    <button type="button" class="btn btn-primary  float-right mt-3 mr-3" onclick="xtoogleSearchx()">Advance Search</button>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search-toogle" style="display:none;padding-top:20px;">
                                <div class="form-group">
                                    <form id="search-form-advance">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="">Search Tag</label>
                                                    <input type="text" class="form-control" placeholder="Search by first name, last name, skype ID, email ID,mobile no.">
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Lead Added From Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Lead Added To Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-12"><br></div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Next Follow-up From Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Next Follow-up To Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Lead Status Change From Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="form-group">
                                                    <label for="">Lead Status Change To Date</label>
                                                    <input type="date" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-12"><br></div>
                                            <div class="col-xl-3 col-md-12">
                                                <div class="form-group">
                                                    <label for="">Country</label>
                                                    <select class="form-control form-control-select2 input-form-c">
                                                        <option value="">Select Country</option>
                                                        <option>Philippines</option>
                                                        <option>United Kingdom</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xl-3 col-md-4">
                                                <div class="form-group">
                                                    <label for="">Level of Interest</label>
                                                    <select class="form-control form-control-select2 input-form-c">
                                                        <option value="">Select Level</option>
                                                        <option>Very Interested</option>
                                                        <option>Interested</option>
                                                        <option>Not That Interested</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xl-3 col-md-4">
                                                <div class="form-group">
                                                    <label for="">Lead Status</label>
                                                    <select class="form-control form-control-select2 input-form-c">
                                                        <option value="">Select Status</option>
                                                        <option>Ongoing</option>
                                                        <option>Accepted</option>
                                                        <option>Rejected</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xl-3 col-md-4">
                                                <div class="form-group">
                                                    <label for="">Assigned To</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group"><br>
                                                    <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                                    <button type="button" class="btn btn-warning" onclick="resetFormSearch()"><i class="icon-reset"></i></button>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-columned datatable-responsive">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">#</th>
                                            <th>Name</th>
                                            <th>Lead Completeness</th>
                                            <th>Email</th>
                                            <th>Edit Lead</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($i=1;$i<=10;$i++)
                                        <tr>
                                            <td>{{ $i }}.</td>
                                            <td>John Doe (username{{$i}}) </td>
                                            <td>
                                                <div class="progress rounded-round">
                                                    <div class="progress-bar bg-slate" style="width: 85%">
                                                        <span>85% Complete</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>test@gmail.com</td>
                                            <td><button class="btn btn-info" data-toggle="modal" data-target="#edit_lead"><i class="icon-pencil7"></i></button></td>
                                            <td><a href="{{url('backoffice/crm/view-lead-details/')}}"class="btn bg-violet"><i class="icon-eye"></i></a></td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                </div>
               </div>
            <!-- /traffic sources -->

        </div>
    </div>
    <!-- /main charts -->
</div>
<div class="modal" id="edit_lead" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Lead</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group row">
                <label class="col-md-3 col-form-label">First Name:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-form-c">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Last Name:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-form-c">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Email:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-form-c">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Skype ID:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-form-c">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Mobile No.:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-form-c">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Country:</label>
                <div class="col-md-9">
                    <select class="form-control form-control-select2 input-form-c">
                        <option value="">Select Country</option>
                        <option>Philippines</option>
                        <option>United Kingdom</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Level of Interest:</label>
                <div class="col-md-9">
                    <select class="form-control form-control-select2 input-form-c">
                        <option value="">Select Level</option>
                        <option>Very Interested</option>
                        <option>Interested</option>
                        <option>Not That Interested</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Lead Status:</label>
                <div class="col-md-9">
                    <select class="form-control form-control-select2 input-form-c">
                        <option value="">Select Status</option>
                        <option>Ongoing</option>
                        <option>Accepted</option>
                        <option>Rejected</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">Description:</label>
                <div class="col-md-9">
                    <textarea class="form-control input-form-c">Testing</textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update Lead</button>
        </div>
            </form>
      </div>
    </div>
</div>
@section('custom')
<script>
    $('a.crm-view-lead').addClass('active');
    $('li.crm-must-open').addClass('nav-item-expanded nav-item-open');
    
    function formInputs() {
        if($('.input-form-update').is('[disabled=disabled]')) {
            $('.input-form-update').prop("disabled", false)
            $('.btn-form-update').show()
        } else {
            $('.input-form-update').prop("disabled", true)
            $('.btn-form-update').hide()

            
        }
    }

    function xtoogleSearchx() {
        if($('.search-toogle:visible').length == 0) {
            $('.search-toogle').show()
        } else {
            $('.search-toogle').hide()
        }
    }

    function resetFormSearch() {
        $('#search-form-advance').find('input,  textarea, select').val('');
        $('#search-form-advance select').trigger("change");
    }
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

@endsection
@endsection