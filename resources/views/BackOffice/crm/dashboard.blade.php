@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">CRM</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="row">

        <div class="col-xl-12">
                <div class="row justify-content-center">
                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-hour-glass icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Total Ongoing Leads</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-checkmark-circle2 icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Total Accepted Leads</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-danger icon-cross icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Total Rejected Leads</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="nav-item">
                                    <a href="#followup-today" class="nav-link active" data-toggle="tab">Your Follow ups for Today</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#missed-followup" class="nav-link" data-toggle="tab">Your Missed Follow up</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#recent-leads" class="nav-link" data-toggle="tab">Recent Leads</a>
                                </li>
                            </ul>

                            <div class="tab-content">

                                <!-- Follow up Today-->
                                <div class="tab-pane fade active show" id="followup-today">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">#</th>
                                                <th>Name</th>
                                                <th>Assigned To</th>
                                                <th>Skype ID</th>
                                                <th>Edit Lead</th>
                                                <th>Add Followup</th>
                                                <th>View Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{ $i }}.</td>
                                                <td>John Doe (username{{$i}}) </td>
                                                <td>John Doe</td>
                                                <td>999</td>
                                                <td><button class="btn btn-info" data-toggle="modal" data-target="#edit_lead"><i class="icon-pencil7"></i></button></td>
                                                <td><button class="btn bg-slate" data-toggle="modal" data-target="#add_follow_up"><i class="icon-plus2"></i></button></td>
                                                <td><a href="{{url('backoffice/crm/view-lead-details/')}}"class="btn bg-violet"><i class="icon-eye"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Your Missed Follow up -->
                                <div class="tab-pane fade" id="missed-followup">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">#</th>
                                                <th>Name</th>
                                                <th>Assigned To</th>
                                                <th>Skype ID</th>
                                                <th>Edit Lead</th>
                                                <th>Add Followup</th>
                                                <th>View Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{ $i }}.</td>
                                                <td>John Doe (username{{$i}}) </td>
                                                <td>John Doe</td>
                                                <td>999</td>
                                                <td><button class="btn btn-info" data-toggle="modal" data-target="#edit_lead"><i class="icon-pencil7"></i></button></td>
                                                <td><button class="btn bg-slate" data-toggle="modal" data-target="#add_follow_up"><i class="icon-plus2"></i></button></td>
                                                <td><a href="{{url('backoffice/crm/view-lead-details/')}}"class="btn bg-violet"><i class="icon-eye"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Recent Leads -->
                                <div class="tab-pane fade" id="recent-leads">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">#</th>
                                                <th>Name</th>
                                                <th>Assigned To</th>
                                                <th>Skype ID</th>
                                                <th>Edit Lead</th>
                                                <th>Add Followup</th>
                                                <th>Next Followup Date</th>
                                                <th>View Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($i=1;$i<=10;$i++)
                                            <tr>
                                                <td>{{ $i }}.</td>
                                                <td>John Doe (username{{$i}}) </td>
                                                <td>John Doe</td>
                                                <td>999</td>
                                                <td><button class="btn btn-info" data-toggle="modal" data-target="#edit_lead"><i class="icon-pencil7"></i></button></td>
                                                <td><button class="btn bg-slate" data-toggle="modal" data-target="#add_follow_up"><i class="icon-plus2"></i></button></td>
                                                <td><button class="btn btn-warning" data-toggle="modal" data-target="#next_follow_up"><i class="icon-plus2"></i></button></td>
                                                <td><a href="{{url('backoffice/crm/view-lead-details/')}}"class="btn bg-violet"><i class="icon-eye"></i></a></td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="modal" id="edit_lead" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-pencil7 mr-2"></i> Edit Lead</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group row">
                <label class="col-md-2 col-form-label">First Name:</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-form-update" value="John Doe">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Last Name:</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-form-update" value="John Doe">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Email:</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-form-update" value="John Doe">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Skype ID:</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-form-update" value="John Doe">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Mobile No.:</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-form-update" value="John Doe">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Country:</label>
                <div class="col-md-10">
                    <select class="form-control form-control-select2 input-form-update">
                        <option selected>Philippines</option>
                        <option>United Kingdom</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Level of Interest.:</label>
                <div class="col-md-10">
                    <select class="form-control form-control-select2 input-form-update" >
                        <option selected>Very Interested</option>
                        <option>Interested</option>
                        <option>Not That Interested</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Lead Status:</label>
                <div class="col-md-10">
                    <select class="form-control form-control-select2 input-form-update">
                        <option>Ongoing</option>
                        <option>Accepted</option>
                        <option selected>Rejected</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 col-form-label">Description:</label>
                <div class="col-md-10">
                    <textarea class="form-control input-form-update">Testing</textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update Lead</button>
        </div>
            </form>
      </div>
    </div>
</div>

<div class="modal" id="add_follow_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-plus2 mr-2"></i> Add Followup</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Lead:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Description:</label>
                    <div class="col-md-10">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>

               <div class="form-group row">
                    <label class="col-md-2 col-form-label">Next Followup Date:</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control">
                    </div>
                </div>

               <div class="form-group row">
                    <label class="col-md-2 col-form-label">Select File:</label>
                    <div class="col-md-10">
                        <input type="file" class="form-control">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Add Followup</button>
        </div>
            </form>
      </div>
    </div>
</div>

<div class="modal" id="view_follow_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-plus2 mr-2"></i> Add Followup</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Lead:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Description:</label>
                    <div class="col-md-10">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>

               <div class="form-group row">
                    <label class="col-md-2 col-form-label">Next Followup Date:</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control">
                    </div>
                </div>

               <div class="form-group row">
                    <label class="col-md-2 col-form-label">Select File:</label>
                    <div class="col-md-10">
                        <input type="file" class="form-control">
                    </div>
                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Add Followup</button>
        </div>
            </form>
      </div>
    </div>
</div>

<div class="modal" id="next_follow_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-plus2 mr-2"></i> Update Followup Date</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
               <div class="form-group row">
                    <label class="col-md-4 col-form-label">Current Followup Date:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" disabled value="000-00-00 00:00:00">
                    </div>
                </div>

               <div class="form-group row">
                    <label class="col-md-4 col-form-label">Next Followup Date:</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Update Followup Date</button>
        </div>
            </form>
      </div>
    </div>
</div>

@section('custom')
<script>
    $('a.crm-dashboard').addClass('active');
    $('li.crm-must-open').addClass('nav-item-expanded nav-item-open');
    $(document).ready(function () {
    $('#example').DataTable({
        responsive: true
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust()
        .responsive.recalc();
    });    
});
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>

<script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
@endsection
@endsection