@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item ">CRM</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold">Lead Details</span>
								<span class="font-size-sm text-uppercase font-weight-semibold"><i class="icon-pencil7" onclick="formInputs()" style="cursor: pointer;"></i></span>
                </div>
                <div class="card-body">
                    <form>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">First Name:</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-form-update" disabled value="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Last Name:</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-form-update" disabled value="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Email:</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-form-update" disabled value="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Skype ID:</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-form-update" disabled value="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Mobile No.:</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control input-form-update" disabled value="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Country:</label>
                                <div class="col-md-10">
                                    <select class="form-control form-control-select2 input-form-update" disabled>
                                        <option selected>Philippines</option>
                                        <option>United Kingdom</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Level of Interest.:</label>
                                <div class="col-md-10">
                                    <select class="form-control form-control-select2 input-form-update" disabled>
                                        <option selected>Very Interested</option>
                                        <option>Interested</option>
                                        <option>Not That Interested</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Lead Status:</label>
                                <div class="col-md-10">
                                    <select class="form-control form-control-select2 input-form-update" disabled>
                                        <option>Ongoing</option>
                                        <option>Accepted</option>
                                        <option selected>Rejected</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Description:</label>
                                <div class="col-md-10">
                                    <textarea class="form-control input-form-update" disabled>Testing</textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-primary btn-form-update" style="display:none">Update Lead</button>
                            </div>
                    </form>
                </div>
            </div>    
        </div>

        <div class="col-xl-8">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold">Lead History</span>
                </div>
                <div class="card-body border-top-teal">
                    <div class="list-feed">
                        <div class="list-feed-item">
                            <div class="text-muted">Jan 12, 12:47</div>
                            <a href="#">David Linner</a> requested refund for a double bank card charge
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 11, 10:25</div>
                            User <a href="#">Christopher Wallace</a> from Google is awaiting for staff reply
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 10, 09:37</div>
                            Ticket <strong>#43683</strong> has been resolved by <a href="#">Victoria Wilson</a>
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 9, 08:28</div>
                            <a href="#">Eugene Kopyov</a> merged <strong>Master</strong>, <strong>Demo</strong> and <strong>Dev</strong> branches
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 8, 07:58</div>
                            All sellers have received payouts for December, 2016!
                        </div>

                        <div class="list-feed-item">
                            <div class="text-muted">Jan 7, 06:32</div>
                            <a href="#">Chris Arney</a> created a new ticket <strong>#43136</strong> and assigned to <a href="#">John Nod</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('custom')
<script>
$('a.crm-dashboard').addClass('active');
$('li.crm-must-open').addClass('nav-item-expanded nav-item-open');

function formInputs() {
    if($('.input-form-update').is('[disabled=disabled]')) {
        $('.input-form-update').prop("disabled", false)
        $('.btn-form-update').show()
    } else {
        $('.input-form-update').prop("disabled", true)
        $('.btn-form-update').hide()

        
    }
}
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
@endsection
@endsection