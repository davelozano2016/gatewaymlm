@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item">Reports</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                    <li class="nav-item">
                        <a href="#via-username" class="nav-link active" data-toggle="tab">Username</a>
                    </li>
                    <li class="nav-item">
                        <a href="#via-count" class="nav-link" data-toggle="tab">Count</a>
                    </li>
                </ul>

                <div class="d-flex align-items-start flex-column flex-md-row">
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Username -->
                        <div class="tab-pane fade active show" id="via-username">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">Username</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3 col-md-4">
                                                            <label for="">&nbsp;</label>
                                                                <div class="form-group">
                                                                    <button class="btn bg-slate"><i
                                                                            class="icon-search4"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-2 text-right">
                                                                <button class="btn bg-slate"><i
                                                                        class="icon-file-excel"></i></button>
                                                                <button class="btn bg-slate"><i
                                                                        class="icon-file-text2"></i></button>
                                                                <button class="btn bg-slate" onclick="printUsername()"><i
                                                                        class="icon-printer"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive" id="MemberTable">
                                                        <table class="table table-columned  datatable-responsive">
                                                            <p>
                                                            <h3>Company Name</h3>
                                                            </p>
                                                            <p><b>Company Address: </b> Address Here</p>
                                                            <p><b>Phone: </b> +63904 3444 444</p>
                                                            <p><b>Email: </b> email@gmail.com</p>
                                                            <hr>
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="7" class="text-center">
                                                                        <h4>Member Name : Your First Name Your Last
                                                                            Name(ad1234)</h4>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="width:1px">#</th>
                                                                    <th>Sponsor</th>
                                                                    <th>Email</th>
                                                                    <th>Mobile No.</th>
                                                                    <th>Country</th>
                                                                    <th>Zip Code</th>
                                                                    <th>Enrollment Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @for($i=1;$i<=10;$i++) <tr>
                                                                    <td>{{ $i }}.</td>
                                                                    <td>John Doe (username{{$i}}) </td>
                                                                    <td>Email {{ $i }}</td>
                                                                    <td>Mobile {{ $i }}</td>
                                                                    <td>Country {{ $i }}</td>
                                                                    <td>Zip {{ $i }}</td>
                                                                    <td>{{ date('M j, Y g:i:s A')}}</td>
                                                                    </tr>
                                                                    @endfor
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Count -->
                        <div class="tab-pane fade" id="via-count">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Enter Count Start From</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Enter Count</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                            <label for="">&nbsp;</label>
                                                                <div class="form-group">
                                                                    <button class="btn bg-slate"><i
                                                                            class="icon-search4"></i></button>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 text-right">
                                                                <button class="btn bg-slate"><i
                                                                        class="icon-file-excel"></i></button>
                                                                <button class="btn bg-slate"><i
                                                                        class="icon-file-text2"></i></button>
                                                                <button class="btn bg-slate" onclick="printCount()"><i
                                                                        class="icon-printer"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive" id="CountTable">
                                                        <table class="table table-columned datatable-responsive">
                                                            <p>
                                                            <h3>Company Name</h3>
                                                            </p>
                                                            <p><b>Company Address: </b> Address Here</p>
                                                            <p><b>Phone: </b> +63904 3444 444</p>
                                                            <p><b>Email: </b> email@gmail.com</p>
                                                            <hr>
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:1px">#</th>
                                                                    <th>Member</th>
                                                                    <th>Sponsor</th>
                                                                    <th>Email</th>
                                                                    <th>Mobile No.</th>
                                                                    <th>Country</th>
                                                                    <th>Zip Code</th>
                                                                    <th>Enrollment Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @for($i=1;$i<=10;$i++) <tr>
                                                                    <td>{{$i}}. </td>
                                                                    <td>John Doe (username{{$i}}) </td>
                                                                    <td>Sponsor {{$i}} </td>
                                                                    <td>Email {{ $i }}</td>
                                                                    <td>Mobile {{ $i }}</td>
                                                                    <td>Country {{ $i }}</td>
                                                                    <td>Zip {{ $i }}</td>
                                                                    <td>{{ date('M j, Y g:i:s A')}}</td>
                                                                    </tr>
                                                                    @endfor
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
    $('a.r-profile').addClass('active');
    $('li.reports-must-open').addClass('nav-item-expanded nav-item-open');

    function printUsername() {
        var divToPrint = document.getElementById('MemberTable');
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    function printCount() {
        var divToPrint = document.getElementById('CountTable');
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
        });    
    });
    </script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    @endsection
    @endsection