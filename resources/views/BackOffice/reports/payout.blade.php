@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Reports</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12" id="exportMenu">
                                <button class="btn bg-slate"><i class="icon-file-excel"></i></button>
                                <button class="btn bg-slate"><i class="icon-file-text2"></i></button>
                                <button class="btn bg-slate" onclick="printList()"><i class="icon-printer"></i></button>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="">Status</label>
                                    <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                        <option value="">Select Status</option>
                                        <option value="1">Paid</option>
                                        <option value="2">Released</option>
                                        <option value="3">Pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="">Date Range</label>
                                    <button type="button" class="btn btn-light daterange-predefined">
                                        <span></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive" id="ListTable">
                        <table class="table table-columned datatable-responsive">
                        <p><h3>Company Name</h3></p>
                        <p><b>Company Address: </b> Address Here</p>
                        <p><b>Phone: </b> +63904 3444 444</p>
                        <p><b>Email: </b> email@gmail.com</p><hr>
                        <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Invoice No.</th>
                                    <th>Member Name</th>
                                    <th>Total Amount</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                        </thead>
                        <tbody>
                                @for($i=1;$i<=10;$i++)
                                <tr>
                                    <td>{{ $i }}.</td>
                                    <td>0000{{$i}}</td>
                                    <td>John Doe (username{{$i}}) </td>
                                    <td>₱ {{ number_format('100', 2) }}</td>
                                    <td>{{ date('M j, Y g:i:s A')}}</td>
                                    <td>Paid</td>
                                </tr>
                                @endfor
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('custom')
<script>
    $('a.r-payout').addClass('active');
    $('li.reports-must-open').addClass('nav-item-expanded nav-item-open');
    function afterPrint() {
        $('.datatable-header').show();
        $('.datatable-footer').show();
    }
    function printList() {
        $('.datatable-header').hide();
        $('.datatable-footer').hide();
        var divToPrint = document.getElementById('ListTable');
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
        afterPrint()
   }
</script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

<script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
@endsection
@endsection