@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item">Reports</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <label class="">Username</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-12">
                                <label for="">&nbsp;</label>
                                <div class="form-group">
                                    <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                </div>
                            </div>
                            <div class="col-lg-4  col-md-12 text-right">
                                <button class="btn bg-slate"><i class="icon-file-excel"></i></button>
                                <button class="btn bg-slate"><i class="icon-file-text2"></i></button>
                                <button class="btn bg-slate" onclick="printList()"><i class="icon-printer"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive" id="ListTable">
                        <table class="table table-columned">
                        <p><h3>Company Name</h3></p>
                        <p><b>Company Address: </b> Address Here</p>
                        <p><b>Phone: </b> +63904 3444 444</p>
                        <p><b>Email: </b> email@gmail.com</p><hr>
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center"><h3>RANK PERFORMANCE REPORT</h3></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>Member Name</b></td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td><b>Current Rank</b></td>
                                <td>NA</td>
                            </tr>
                            <tr>
                                <td><b>Next Rank</b></td>
                                <td>Bronze</td>
                            </tr>
                            <tr>
                                <td><b>Current Referral Count</b></td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td><b>Referral Count for Bronze</b></td>
                                <td>2</td>
                            </tr>
                            <tr>
                                <td><b>Needed Referral Count</b></td>
                                <td>2</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('custom')
<script>
    $('a.r-rank-performance').addClass('active');
    $('li.reports-must-open').addClass('nav-item-expanded nav-item-open');
    function printList() {
        var divToPrint = document.getElementById('ListTable');
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
        afterPrint()
   }
</script>
@endsection
@endsection