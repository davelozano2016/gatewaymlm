@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Main charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="row justify-content-center">
                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Credited</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-danger icon-cash2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Debited</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-brown icon-wallet icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">E-Wallet Balance</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Purchase Wallet</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-credit-card icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">₱ {{ number_format('100', 2) }}</h3>
                                    <span class="text-uppercase font-size-sm">Commission Earned</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                    <li class="nav-item  mr-2">
                        <a href="#e-wallet-summary" class="nav-link active" data-toggle="tab">E-Wallet Summary</a>
                    </li>
                    <li class="nav-item">
                        <a href="#e-wallet-transaction" class="nav-link" data-toggle="tab">E-Wallet Transactions</a>
                    </li>
                    <li class="nav-item">
                        <a href="#e-wallet-balance" class="nav-link" data-toggle="tab">E-Wallet Balance</a>
                    </li>
                    <li class="nav-item">
                        <a href="#e-wallet-statement" class="nav-link" data-toggle="tab">E-Wallet Statement</a>
                    </li>
                    <li class="nav-item">
                        <a href="#purchase-wallet" class="nav-link" data-toggle="tab">Purchase Wallet</a>
                    </li>
                    <li class="nav-item">
                        <a href="#user-earnings" class="nav-link" data-toggle="tab">User Earnings</a>
                    </li>
                </ul>
                <div class="card card-body">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <!-- Left content -->
                        <div class="tab-content w-100 order-2 order-md-1">
                            <!-- E-Wallet Summary -->
                            <div class="tab-pane active show" id="e-wallet-summary">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-light daterange-predefined">
                                                    <span></span>
                                                </button>
                                                <button class="btn bg-primary"><i class="icon-search4"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h2 class="text-uppercase">CREDIT</h2>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">E-Pin
                                                        Refund</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Fund Credit</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Fund
                                                        Transfer</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Payout
                                                        Cancel</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Referral
                                                        Commission</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Rank
                                                        Commission</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Level
                                                        Commission</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Binary
                                                        Commission</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Matching
                                                        Bonus</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Pool Bonus</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Fast Start
                                                        Bonus</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Vacation
                                                        Fund</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Education
                                                        Fund</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Car Fund</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-success rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">House Fund</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-md-12 col-sm-12">
                                        <h2 class="text-uppercase">
                                            <hr>DEBIT
                                        </h2>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">E-Pin
                                                        Purchase</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Fund Debit</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Fund
                                                        Transfer</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Payout
                                                        Request</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Payout Release
                                                        (manual)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">E-wallet Payment
                                                        (registration)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Purchase
                                                        Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="card card-body border-left-danger rounded-left-0">
                                            <div class="media">

                                                <div class="media-body">
                                                    <h3 class="font-weight-semibold mb-0">₱
                                                        {{ number_format('100', 2) }}</h3>
                                                    <span class="text-uppercase font-size-sm">Payout Fee</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- E-Wallet Transactions -->
                            <div class="tab-pane" id="e-wallet-transaction">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-12">
                                                    <div class="form-group">
                                                        <label class="">Username</label>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group">
                                                        <label class="">Type</label>
                                                        <select multiple="multiple"
                                                            class="form-control form-control-select2">
                                                            <option value="1">Credited</option>
                                                            <option value="2">Debited</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group">
                                                        <label class="">Category</label>
                                                        <select multiple="multiple"
                                                            class="form-control form-control-select2">
                                                            <option value="1">E-Pin Purchase</option>
                                                            <option value="2">E-Pin Refund</option>
                                                            <option value="3">Fund Credit</option>
                                                            <option value="4">Fund Debit</option>
                                                            <option value="5">Fund Transfer</option>
                                                            <option value="6">Fund Transfer Fee</option>
                                                            <option value="7">Payout Request</option>
                                                            <option value="8">Payout Release (manual)</option>
                                                            <option value="9">Payout Fee</option>
                                                            <option value="10">Payout Cancel</option>
                                                            <option value="11">E-wallet Payment (registration)</option>
                                                            <option value="12">Purchase Amount</option>
                                                            <option value="13">Referral Commission</option>
                                                            <option value="14">Rank Commission</option>
                                                            <option value="15">Level Commission</option>
                                                            <option value="16">Binary Commission</option>
                                                            <option value="17">Matching Bonus</option>
                                                            <option value="18">Pool Bonus</option>
                                                            <option value="19">Fast Start Bonus</option>
                                                            <option value="20">Vacation Fund</option>
                                                            <option value="21">Education Fund</option>
                                                            <option value="22">Car Fund</option>
                                                            <option value="23">House Fund</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6 row">
                                                    <div class="col-md-12">
                                                        <label for="">Date Range</label>
                                                        <br>
                                                        <button type="button"
                                                            class="btn btn-light btn-block daterange-predefined">
                                                            <span></span>
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="col-lg-1 col-md-6">
                                                    <label for="">&nbsp;</label>
                                                    <div class="form-group">
                                                        <button class="btn bg-slate"><i
                                                                class="icon-search4"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="ListTable">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Member Name</th>
                                                        <th>Category</th>
                                                        <th>Type</th>
                                                        <th>Amount</th>
                                                        <th>Transaction Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>John Doe (username{{$i}}) </td>
                                                        <td>Level Commission</td>
                                                        <td>Credited</td>
                                                        <td>₱ {{ number_format('100', 2) }}</td>
                                                        <td>{{ date('M j, Y g:i:s A')}}</td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- E-Wallet Balance -->
                            <div class="tab-pane" id="e-wallet-balance">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="">Username</label>
                                                        <select multiple="multiple"
                                                            class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user select form-control-select2">
                                                            <option value="1">User 1</option>
                                                            <option value="2">User 2</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <label for="">&nbsp;</label>

                                                    <div class="form-group">
                                                        <button class="btn bg-slate"><i
                                                                class="icon-search4"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="ListTable">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Member Name</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>E-Wallet Balance</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>John Doe</td>
                                                        <td>username</td>
                                                        <td>email</td>
                                                        <td>₱ {{ number_format('100', 2) }}</td>
                                                        <td><a href="{{ url('backoffice/profile-management/profile-view/') }}"
                                                                class="btn btn-info"><i class="icon-eye"></i></a></td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- E-Wallet Statement -->
                            <div class="tab-pane" id="e-wallet-statement">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="">Username</label>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <label for="">&nbsp;</label>
                                                    <div class="form-group">
                                                        <button class="btn bg-slate"><i
                                                                class="icon-search4"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="ListTable">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Category</th>
                                                        <th>Username</th>
                                                        <th>Amount</th>
                                                        <th>Balance</th>
                                                        <th>Transaction Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>Level Commission</td>
                                                        <td>INF170549</td>
                                                        <td>₱ {{ number_format('100', 2) }}</td>
                                                        <td>₱ {{ number_format('100', 2) }}</td>
                                                        <td>{{ date('M j, Y g:i:s A')}}</td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Purchase Wallet -->
                            <div class="tab-pane" id="purchase-wallet">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="ListTable">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Username</th>
                                                        <th>Name</th>
                                                        <th style="width:1px">Amount</th>
                                                        <th>Deposit</th>
                                                        <th>Status</th>
                                                        <th>Date</th>
                                                        <th style="width:1px"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1?>
                                                    @foreach($purhase_wallet_query as $purchase_wallet)
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{$purchase_wallet->username}} <a
                                                                href="{{url('backoffice/profile-management/members')}}/{{Crypt::encryptString($purchase_wallet->id_number)}}">{{$purchase_wallet->id_number}}</a>
                                                        </td>
                                                        <td>{{$purchase_wallet->firstname}}
                                                            {{$purchase_wallet->middlename}}
                                                            {{$purchase_wallet->surname}}</td>
                                                        <td>{{$purchase_wallet->currency_symbol}}{{number_format($purchase_wallet->amount,2)}}
                                                        </td>
                                                        <td>{{$purchase_wallet->deposit_via}}</td>
                                                        <td class="text-uppercase">
                                                            
                                                            @if($purchase_wallet->ewallet_status == 'Processing') 
                                                                <label class="badge badge-warning">{{$purchase_wallet->ewallet_status}}</label>
                                                            @elseif($purchase_wallet->ewallet_status == 'Completed')
                                                                <label class="badge badge-success">{{$purchase_wallet->ewallet_status}}</label>
                                                            @else
                                                                <label class="badge badge-danger">{{$purchase_wallet->ewallet_status}}</label>
                                                            @endif
                                                        </td>
                                                        <td>{{$purchase_wallet->transaction_date}}</td>
                                                        <td style="width:1px" class="text-center">
                                                            <a class="list-icons-item" href="javascript:void(0)"
                                                                data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                            <div class="dropdown-menu">
                                                                <a href="javascript:void(0)" data-toggle="modal"
                                                                    data-target="#ewallet_{{$purchase_wallet->wallet_id}}"
                                                                    class="dropdown-item"><i class="icon-eye"></i> View
                                                                    Receipt</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <div id="ewallet_{{$purchase_wallet->wallet_id}}" class="modal"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-lg modal-dialog-scrollable">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h6 class="modal-title">Purchase E-Wallet Details</h6>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal">&times;</button>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <form action="{{url('backoffice/e-wallet/update/'.Crypt::encryptString($purchase_wallet->wallet_id))}}"  method="POST">
                                                                        @csrf
                                                                        <div class="form-group row">
                                                                            <label class="col-lg-3 col-form-label">Name:</label>
                                                                            <div class="col-lg-9">
                                                                                <input type="text" readonly value="{{$purchase_wallet->firstname}} {{$purchase_wallet->middlename}} {{$purchase_wallet->surname}}" class="form-control">
                                                                            </div>

                                                                            <input type="hidden" name="amount" value="{{$purchase_wallet->amount}}">
                                                                            <input type="hidden" name="current_fund" value="{{$purchase_wallet->fund}}">
                                                                            <input type="hidden" name="users_id" value="{{$purchase_wallet->users_id}}">
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label class="col-lg-3 col-form-label">Current E-Wallet Balance:</label>
                                                                            <div class="col-lg-9">
                                                                                <input type="text" readonly value="{{$purchase_wallet->currency_symbol}}{{number_format($purchase_wallet->fund,2)}}" class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label class="col-lg-3 col-form-label">Amount:</label>
                                                                            <div class="col-lg-9">
                                                                                <input type="text" readonly value="{{$purchase_wallet->currency_symbol}}{{number_format($purchase_wallet->amount,2)}}" class="form-control">
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group row">
                                                                            <label
                                                                                class="col-lg-3 col-form-label">Receipt:</label>
                                                                            <div class="col-lg-9">
                                                                                <img class="img-responsive"
                                                                                    style="width:100%"
                                                                                    src="{{asset('assets/'.$purchase_wallet->upload_receipt)}}"
                                                                                    alt="">
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label class="col-lg-3 col-form-label">Status:</label>
                                                                            <div class="col-lg-9">
                                                                                <select name="status" class="form-control form-control-select2">
                                                                                    <option value="Processing">Processing</option>
                                                                                    <option value="Completed">Completed</option>
                                                                                    <option value="Cancelled">Cancelled</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label class="col-lg-3 col-form-label">Note:</label>
                                                                            <div class="col-lg-9">
                                                                                <textarea name="notes" class="form-control" style="resize:none" cols="30" rows="10"></textarea>
                                                                            </div>
                                                                        </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit"
                                                                        class="btn btn-sm btn-primary btn-sm">Submit</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- User Earnings -->
                            <div class="tab-pane" id="user-earnings">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-5 col-md-12">
                                                    <div class="form-group">
                                                        <label class="">Username</label>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-5">
                                                    <div class="form-group">
                                                        <label class="">Category</label>
                                                        <select multiple="multiple"
                                                            class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user select form-control-select2">
                                                            <option value="1">E-Pin Purchase</option>
                                                            <option value="2">E-Pin Refund</option>
                                                            <option value="3">Fund Credit</option>
                                                            <option value="4">Fund Debit</option>
                                                            <option value="5">Fund Transfer</option>
                                                            <option value="6">Fund Transfer Fee</option>
                                                            <option value="7">Payout Request</option>
                                                            <option value="8">Payout Release (manual)</option>
                                                            <option value="9">Payout Fee</option>
                                                            <option value="10">Payout Cancel</option>
                                                            <option value="11">E-wallet Payment (registration)</option>
                                                            <option value="12">Purchase Amount</option>
                                                            <option value="13">Referral Commission</option>
                                                            <option value="14">Rank Commission</option>
                                                            <option value="15">Level Commission</option>
                                                            <option value="16">Binary Commission</option>
                                                            <option value="17">Matching Bonus</option>
                                                            <option value="18">Pool Bonus</option>
                                                            <option value="19">Fast Start Bonus</option>
                                                            <option value="20">Vacation Fund</option>
                                                            <option value="21">Education Fund</option>
                                                            <option value="22">Car Fund</option>
                                                            <option value="23">House Fund</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-5">
                                                    <div class="form-group">
                                                        <label class="">Date Range</label>
                                                        <select name="date_range_earnings" id="date_range_earnings"
                                                            onchange="dateRangeEarnings()"
                                                            class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                            <option value="1">Overall</option>
                                                            <option value="2">Today</option>
                                                            <option value="3">This Month</option>
                                                            <option value="4">This Year</option>
                                                            <option value="5">Custom</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-2">
                                                    <label for="">&nbsp;</label>
                                                    <div class="form-group">
                                                        <button class="btn bg-slate"><i
                                                                class="icon-search4"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="ListTable">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Member Name</th>
                                                        <th>Username</th>
                                                        <th>Category</th>
                                                        <th>Amount</th>
                                                        <th>Transaction Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>John Doe</td>
                                                        <td>username</td>
                                                        <td>Rank commission</td>
                                                        <td>₱ {{ number_format('100', 2) }}</td>
                                                        <td>{{ date('M j, Y g:i:s A')}}</td>
                                                        </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('custom')
    <script>
        $('a.e-wallet').addClass('active');
        $(document).ready(function () {
            $('#example').DataTable({
                responsive: true
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust()
                    .responsive.recalc();
            });
        });

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{asset('assets/js/demo_pages/form_multiselect.js') }}"></script>
    <script src="{{asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{asset('assets/js/demo_pages/picker_date.js') }}"></script>
    @endsection
    @endsection
