@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title ?? ''}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">E-Commerce Store</span>
                    <span class="breadcrumb-item">Product Setup</span>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <!-- Traffic sources -->
                <div class="form-group">
                    <a href="{{url('backoffice/e-commerce-store/product-setup/products/new-product')}}"
                        class="btn btn-primary"><span>New Product</span></a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">


                                <div class="table-responsive">
                                    <table class="table table-columned datatable-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">#</th>
                                                <th>Product Title</th>
                                                <th>Product Code</th>
                                                <th>Category</th>
                                                <th>Supplier</th>
                                                <th style="width:1px">BPV</th>
                                                <th style="width:1px">UPV</th>
                                                <th style="width:1px">Stocks</th>
                                                <th style="width:1px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1?>
                                            @foreach($products as $product)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$product->product_title}}</td>
                                                <td>{{$product->product_code}}</td>
                                                <td>{{$product->category}}</td>
                                                <td>{{$product->supplier_name}}</td>
                                                <td>{{$product->binary_points}}</td>
                                                <td>{{$product->unilevel_points}}</td>
                                                <td>{{$product->product_stocks}}</td>
                                                <td style="width:1px" class="text-center">
                                                    <a class="list-icons-item" href="javascript:void(0)"
                                                        data-toggle="dropdown"><i class="icon-gear"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a href="{{url('backoffice/e-commerce-store/product-setup/products/visit/'.Crypt::encryptString($product->product_id))}}"
                                                            class="dropdown-item"><i class="icon-pencil"></i> Edit
                                                            Product</a>

                                                        <a href="{{url('backoffice/e-commerce-store/product-setup/products/visit/'.Crypt::encryptString($product->product_id))}}"
                                                            class="dropdown-item"><i class="icon-trash"></i> Delete
                                                            Product</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /traffic sources -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->
    @section('custom')
    <script src="{{asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script>
    $('a.ps-products').addClass('active');
    $('li.e-commerce-store-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    @endsection
    @endsection