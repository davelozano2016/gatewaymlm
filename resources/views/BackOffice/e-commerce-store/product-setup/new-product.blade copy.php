@extends('layouts.BackOffice.app')
@section('container')
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title ?? ''}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Dashboard</span>
                    <span class="breadcrumb-item">E-Commerce Store</span>
                    <span class="breadcrumb-item">Product Setup</span>
                    <span class="breadcrumb-item">{{$title}}</span>
                    <span class="breadcrumb-item active">Create</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Main charts -->
        <div class="row">
            <div class="col-xl-12">
                <!-- Traffic sources -->
                <div class="card">
                    <div class="card-body">
                    <form  action="{{url('backoffice/e-commerce-store/product-setup/products/create')}}" data-parsley-validate enctype="multipart/form-data" method="POST">
                        @csrf
                        <h5 class="title text-uppercase">Product Description</h5>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Product Title:</label>
                            <div class="col-md-10">
                                <input type="text" value="Miracle Cream" name="product_title" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Product Code:</label>
                            <div class="col-md-10">
                                <input type="text" value="P0001" name="product_code" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Category:</label>
                            <div class="col-md-10">
                                <select name="product_category_id" class="form-control payout-fee form-control-select2">
                                    @foreach($categories as $category)
                                        <option value="{{Crypt::encryptString($category->id)}}">{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Binary Points:</label>
                            <div class="col-md-10">
                                <input type="text" value="100" name="binary_points" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Unilevel Points:</label>
                            <div class="col-md-10">
                                <input type="text" value="200" name="unilevel_points" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Net Weight per Grams:</label>
                            <div class="col-md-10">
                                <input type="text" value="10" name="net_weight_per_gram" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Facebook Thumbnail:</label>
                            <div class="col-md-10">
                                <input type="file" name="facebook_thumbnail" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Image:</label>
                            <div class="col-md-10">
                                <input type="file" name="product_image" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label class="col-md-2 col-form-label">Description:</label>
                            <div class="col-md-10">
                                <textarea name="product_description" class="form-control" style="resize:none" id="" cols="30" rows="10">This is sample description</textarea>
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label class="col-md-2 col-form-label">Miscellaneous:</label>
                            <div class="col-md-10">
                                <textarea name="product_miscellaneous" class="form-control" style="resize:none" id="" cols="30" rows="10">This is sample miscellaneous</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Discount Type</label>
                            <div class="col-md-10">
                                <select name="discount_type" class="form-control payout-fee form-control-select2">
                                    <option value="Percentage">%</option>
                                    <option value="Fixed" selected>Fixed</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Product Status</label>
                            <div class="col-md-10">
                                <select name="product_status" class="form-control payout-fee form-control-select2">
                                    <option value="0">Active</option>
                                    <option value="1" >Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Product Supplier</label>
                            <div class="col-md-10">
                                <select name="product_supplier_id" class="form-control payout-fee form-control-select2">
                                    @foreach($suppliers as $supplier)
                                        <option value="{{Crypt::encryptString($supplier->id)}}">{{$supplier->supplier_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Product Stocks</label>
                            <div class="col-md-10">
                                <input type="text" value="10000" class="form-control" name="product_stocks" required>
                            </div>
                        </div>

                        <h5 class="title text-uppercase">Product Price & Discount</h5>
                        
                        @foreach($countries as $country)
                        <input type="hidden" name="countries_id[]" class="form-control" value="{{Crypt::encryptString($country->id)}}">

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Distributor Price in {{$country->currency_code}}</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">{{$country->currency_symbol}}</span>
                                        </span>
                                        <input type="text" value="1000" name="distributor_price[]" class="form-control" placeholder="{{$country->currency_name}}" required>
                                    </div>
                                    @error('price') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Retailer Price in {{$country->currency_code}}</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">{{$country->currency_symbol}}</span>
                                        </span>
                                        <input type="text" value="2000" name="retailer[]" class="form-control" placeholder="{{$country->currency_name}}" required>
                                    </div>
                                    @error('price') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">SRP in {{$country->currency_code}}</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">{{$country->currency_symbol}}</span>
                                        </span>
                                        <input type="text" value="2000" name="srp[]" class="form-control" placeholder="{{$country->currency_name}}" required>
                                    </div>
                                    @error('price') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>


                            <h5 class="title text-uppercase">Target Sales Rewards In {{$country->name}}</h5>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Stockist Discount</label>
                                <div class="col-md-10">
                                    <input type="text" value="750" class="form-control" name="stockist_discount[]" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Depot Discount</label>
                                <div class="col-md-10">
                                    <input type="text" value="750" class="form-control" name="depot_discount[]" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Country Manager Discount</label>
                                <div class="col-md-10">
                                    <input type="text" value="750" class="form-control" name="country_manager_discount[]" required>
                                </div>
                            </div>

                           
                            <div style="border-bottom:solid 1px #e1e1e1" class="mb-3"></div>
                        @endforeach
                        <!-- <h5 class="text-uppercase title">Local Shipment Cost</h5>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Metro Manila:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Luzon:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Visayas:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Mindanao:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <h5 class="text-uppercase title">International Shipment Cost</h5>


                        @foreach($countries as $country)
                            @if($country->name == 'Philippines')
                            @else
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">{{$country->name}}</label>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <span class="input-group-text">{{$country->currency_symbol}}</span>
                                            </span>
                                            <input type="hidden" name="countries_id[]" class="form-control"
                                                value="{{$country->id}}">
                                            <input type="text" name="price[]" class="form-control"
                                                placeholder="{{$country->currency_name}}" required>
                                        </div>
                                        @error('price') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                            @endif
                        @endforeach -->

                        <div class="form-group">
                            <button type="submit" class="btn float-right bg-primary">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- /traffic sources -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- /content area -->

    <!-- Button trigger modal -->
    @section('custom')
    <script src="{{asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>


    <!-- /theme JS files -->
    <script>
        $('a.ps-products').addClass('active');
        $('li.e-commerce-store-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    @endsection
    @endsection