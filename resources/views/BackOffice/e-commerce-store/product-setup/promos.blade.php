@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item">Dashboard</span>
                <span class="breadcrumb-item">E-Commerce Store</span>
                <span class="breadcrumb-item">Product Setup</span>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header bg-dark text-white d-flex justify-content-between">
                    <span class="font-size-sm text-uppercase font-weight-semibold">Promo Details</span>
                </div>
                <div class="card-body">
                    <form method="POST" wire:submit.prevent="store">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Promo Name:</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row" id="testing" style="border: 0px;">
                            <label class="col-md-3 col-form-label">Promo Type:</label>
                            <div class="col-md-9">
                            <select class="form-control payout-fee form-control-select2">
                                <option>Type 1</option>
                                <option>Type 2</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row" id="testing" style="border: 0px;">
                            <label class="col-md-3 col-form-label">Valid From:</label>
                            <div class="col-md-9">
                            <input type="date" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Valid To:</label>
                            <div class="col-md-9">
                            <input type="date" class="form-control">
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button type="submit" class="btn bg-primary">Add New</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-9">
            <!-- Traffic sources -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table datatable-responsive table-columned ">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Promo Name</th>
                                    <th>Promo Type</th>
                                    <th>Valid From</th>
                                    <th>Valid To</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody >
                                <?php $i=1?> 
                                <tr>
                                    <td>#</td>
                                    <td>Promo 1</td>
                                    <td>Promo Type</td>
                                    <td>01-01-21</td>
                                    <td>01-31-21</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_promo_modal"><i class="icon-eye"></i></button>
                                            <button class="btn btn-danger btn-sm"><i class="icon-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /traffic sources -->
        </div>
    </div>
    <!-- /main charts -->
</div>
<!-- /content area -->

<!-- Button trigger modal -->
  
  <!-- Modal -->
  <div class="modal" id="add_promo_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-slate">
          <h5 class="modal-title" id="exampleModalLabel">Add New {{$title}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" wire:submit.prevent="store">
                <input type="hidden" wire:model.lazy="package_entry_id" class="form-control">
                
                    <div class="form-group row" id="testing" style="border: 0px;">
                        <label class="col-md-4 col-form-label">Entry Type:</label>
                        <div class="col-md-8">
                            <div >
                                <select class="form-control form-control-select2" wire:model.lazy="entry_type">
                                    <option value="">Select Entry Type</option>
                                    <option></option>
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="form-group row" id="testing" style="border: 0px;">
                        <label class="col-md-4 col-form-label">Package Type:</label>
                        <div class="col-md-8">
                            <input type="text" wire:model.lazy="package_type" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row" id="testing" style="border: 0px;">
                        <label class="col-md-4 col-form-label">Status:</label>
                        <div class="col-md-8">
                            <select wire:model.lazy="status" class="form-control form-control-select2">
                                <option value="" selected>Select Status</option>
                                <option value="Active" selected>Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
            </div>
        <div class="modal-footer">
            <button type="submit" class="btn bg-primary">SUBMIT</button>
        </div>
            </form>
      </div>
    </div>
  </div>

@section('custom')
<script>
    $('a.ps-promos').addClass('active');
    $('li.e-commerce-store-must-open').addClass('nav-item-expanded nav-item-open');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
@endsection
@endsection