@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>{{$title}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements">
                <div class="col-md-12">
                    <div class="row justify-content-center">
                        <div class="btn-group">
                            <button data-toggle="modal" data-target="#encashment_filter"
                                class="btn btn-info ">Search</button>
                        </div>    
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Dashboard</a>
                    <span class="breadcrumb-item active">{{$title}}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="row justify-content-center">
                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-warning icon-hour-glass icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{$pending}}</h3>
                                    <span class="text-uppercase font-size-sm">Pending</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-info icon-checkmark-circle2 icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{$completed}}</h3>
                                    <span class="text-uppercase font-size-sm">Completed</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-success icon-coins icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">0</h3>
                                    <span class="text-uppercase font-size-sm">Paid</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card card-body">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="text-danger icon-cash icon-3x"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">{{$cancelled}}</h3>
                                    <span class="text-uppercase font-size-sm">Cancelled</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xl-12">
                <ul class="nav nav-tabs nav-tabs-solid  mt-2">
                    {{-- <li class="nav-item">
                        <a href="#payout-summary" class="nav-link active" data-toggle="tab">Payout Summary</a>
                    </li> --}}

                    <li class="nav-item">
                        <a href="#process-payment" class="nav-link" data-toggle="tab">Process Payment</a>
                    </li>

                    <li class="nav-item">
                        <a href="#payout-released" class="nav-link" data-toggle="tab">Payout Released</a>
                    </li>

                    <li class="nav-item">
                        <a href="#payout-cancelled" class="nav-link" data-toggle="tab">Cancelled Payout</a>
                    </li>
                   
                </ul>
                <div class="d-flex align-items-start flex-column flex-md-row">
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Payout Summary -->
                        {{-- <div class="tab-pane  active show" id="payout-summary">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px">#</th>
                                                        <th>Member</th>
                                                        <th>Invoice Number</th>
                                                        <th>Amount</th>
                                                        <th>Payout Method</th>
                                                        <th>Paid Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1;$i<=10;$i++) <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>John Doe (username{{$i}}) </td>
                                                        <td>123 </td>
                                                        <td><span class="badge badge-success">₱
                                                                {{ number_format('100', 2) }}</span></td>
                                                        <td>Bank</td>
                                                        <td>Wednesday, January 01, 2021</td>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}



                         
                      

                        <!-- Process Payment -->
                        <div class="tab-pane active" id="process-payment">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{url('backoffice/payout/multi-process')}}" onclick="return confirm('Are you sure you want to execute batch process?')" class="btn btn-info ">Batch Process</a>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px;">#</th>
                                                        <th>Reference No.</th>
                                                        <th>Type</th>
                                                        <th>Amount</th>
                                                        <th>Country</th>
                                                        <th>IP</th>
                                                        <th>Payout</th>
                                                        <th>Status</th>
                                                        <th>Date Requested</th>
                                                        <th></th>          
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=1;@endphp
                                                    @foreach($encashments as $encashment)
                                                        <tr>
                                                            <td style="width:1px">{{$i++}}.</td>
                                                            <td>{{$encashment->reference}}</td>
                                                            <td>{{$encashment->request_type}}</td>
                                                            <td>{{$encashment->currency_symbol}}{{number_format($encashment->request_bonus,2)}}</td>
                                                            <td>{{$encashment->country_code}}</td>
                                                            <td>{{$encashment->ip_address}}</td>
                                                            <td>{{$encashment->currency_symbol}}{{number_format($encashment->payout,2)}}</td>
                                                            <td>
                                                                @if($encashment->status === 0)
                                                                    <label class="badge badge-warning">Pending</label>
                                                                @elseif($encashment->status === 1)
                                                                    <label class="badge badge-info">Processed</label>
                                                                @elseif($encashment->status === 2)
                                                                <label class="badge badge-success">Completed</label>
                                                                @endif
                                                            </td>
                                                            <td>{{$encashment->created_at}}</td>
                                                            <td>
                                                                <div><a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i class="icon-gear"></i></a><div class="dropdown-menu">
                                                                    <a href="{{url('backoffice/payout/process')}}/{{Crypt::encryptString($encashment->encash_id)}}" " class="dropdown-item"><i class="icon-gear"></i> Process</a>
                                                                    <a href="{{url('backoffice/payout/cancel')}}/{{Crypt::encryptString($encashment->encash_id)}}" class="dropdown-item"><i class="icon-trash"></i> Cancel</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="payout-released">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px;">#</th>
                                                        <th>Reference No.</th>
                                                        <th>Name</th>
                                                        <th>Type</th>
                                                        <th>Amount</th>
                                                        <th>Country</th>
                                                        <th>IP</th>
                                                        <th>Payout</th>
                                                        <th>Date Requested</th>
                                                        <th>Date Processed</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=1;@endphp
                                                    @foreach($released_encashments as $released)
                                                        <tr>
                                                            <td style="width:1px">{{$i++}}.</td>
                                                            <td>{{$released->reference_code}}</td>
                                                            <td>
                                                                @if($released->items > 1)
                                                                <a href="">Batch Encashment</a>
                                                                @else 
                                                                {{$released->firstname}} {{$released->surname}} <a href="javascript:void(0)">{{$released->id_number}}</a>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                @if($released->items > 1)
                                                                <a href="">Batch Encashment</a>
                                                                @else 
                                                                {{$released->request_type}}</a>
                                                                @endif
                                                            </td>
                                                            <td>{{$released->currency_symbol}}{{number_format($released->request_amount,2)}}</td>
                                                            <td>{{$released->country_code}}</td>
                                                            <td>{{$released->ip_address}}</td>
                                                            <td>{{$released->currency_symbol}}{{number_format($released->total_payout,2)}}</td>
                                                            <td>{{$released->date_requested}}</td>
                                                            <td>{{$released->date_processed}}</td>
                                                            <td>
                                                                <div><a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i class="icon-gear"></i></a><div class="dropdown-menu">
                                                                    <a href="{{url('backoffice/payout/voucher')}}/{{Crypt::encryptString($released->reference_code)}}" class="dropdown-item"><i class="icon-printer"></i> Print Voucher</a>
                                                                    <a href="{{url('backoffice/payout/details')}}/{{Crypt::encryptString($released->reference_code)}}" class="dropdown-item"><i class="icon-eye"></i> View Details</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="payout-cancelled">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <table class="table table-columned datatable-responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px;">#</th>
                                                        <th>Reference No.</th>
                                                        <th>Type</th>
                                                        <th>Amount</th>
                                                        <th>Country</th>
                                                        <th>IP</th>
                                                        <th>Payout</th>
                                                        <th>Date Requested</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i=1;@endphp
                                                    @foreach($cancelled_encashments as $cancel)
                                                        <tr>
                                                            <td style="width:1px">{{$i++}}.</td>
                                                            <td>{{$cancel->reference}}</td>
                                                            <td>{{$cancel->request_type}}</td>
                                                            <td>{{$cancel->currency_symbol}}{{number_format($cancel->request_bonus,2)}}</td>
                                                            <td>{{$cancel->country_code}}</td>
                                                            <td>{{$cancel->ip_address}}</td>
                                                            <td>{{$cancel->currency_symbol}}{{number_format($cancel->payout,2)}}</td>
                                                            <td>{{$cancel->created_at}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                       
                    </div>
                </div>

            </div>
        </div>
    </div>




    <!-- Basic modal -->
<form method="POST" data-parsley-validate action="{{url('backoffice/payout/search')}}">
    @csrf
    <div id="encashment_filter" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Search Encashment to Process</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">From:</label>
                        <div class="col-lg-9">
                            <input type="date" name="from" value="{{isset($data) ? $data['from'] : ''}}" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">To:</label>
                        <div class="col-lg-9">
                            <input type="date" name="to" value="{{isset($data) ? $data['to'] : ''}}" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Country:</label>
                        <div class="col-lg-9">
                            <select name="country_code" class="form-control" required>
                                @foreach($countries as $country) 
                                    @if(@$data['country'] == $country->code)
                                    <option value="{{$country->code}}" selected>{{$country->name}}</option>
                                    @else
                                    <option value="{{$country->code}}" >{{$country->name}}</option>
                                    @endif
                                    
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- /basic modal -->


    @section('custom')
    <script>
        $('a.payout').addClass('active');
        $(document).ready(function () {
            $('#example').DataTable({
                responsive: true
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust()
                    .responsive.recalc();
            });
        });

    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_multiselect.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>

    @endsection
    @endsection
