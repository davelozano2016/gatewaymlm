@extends('layouts.guests')
@section('container')
<!-- Page content -->
<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <div class="content d-flex justify-content-center align-items-center">
        
            <div class="row">
                <div class="col-md-12">
                    <div class="content d-flex justify-content-center align-items-center">
                        <img src="{{ asset('assets/images/logo.png') }}" style="width:250px" alt="">
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-user-plus icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Create account</h5>
                                <span class="d-block text-muted">Your credentials</span>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="First Name">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Middle Name">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Last Name">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Email">
                                            <div class="form-control-feedback">
                                                <i class="icon-envelope text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Mobile Phone">
                                            <div class="form-control-feedback">
                                                <i class="icon-phone2 text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Username">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="password" class="form-control" placeholder="Password">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="password" class="form-control" placeholder="Cofirm Password">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Sponsor ID">
                                            <div class="form-control-feedback">
                                                <i class="icon-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Upline ID">
                                            <div class="form-control-feedback">
                                                <i class="icon-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-left">
                                            <input type="text" class="form-control" placeholder="Activation Code">
                                            <div class="form-control-feedback">
                                                <i class="icon-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                    <br>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="remember" class="form-input-styled" data-fouc="">
                                                    I agree all statements in <a href="#">Terms of Service</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Sign up <i class="icon-circle-right2 ml-2"></i></button>
                                        </div>

                                        <div class="form-group text-center text-muted content-divider">
                                            <span class="px-2">Already have an account?</span>
                                        </div>

                                        <div class="form-group">
                                            <a href="{{ url('/login') }}" class="btn btn-light btn-block">Login</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>

        </div>
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
@endsection