@extends('layouts.BackOffice.app')
@section('container')

<div class="content-wrapper">

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>{{$title}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ url('backoffice/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{$title}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        {{-- <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <a href="#" class="breadcrumb-elements-item">
                    <i class="icon-comment-discussion mr-2"></i>
                    Support
                </a>

                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear mr-2"></i>
                        Settings
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Main charts -->
    <div class="row">
        <div class="col-xl-3">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card card-body bg-warning">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="icon-hour-glass icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Pending</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="card card-body bg-info">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="icon-checkmark-circle2 icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Approved</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="card card-body bg-success">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="icon-coins icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Paid</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="card card-body bg-danger">
                            <div class="media">
                                <div class="mr-3 align-self-center">
                                    <i class="icon-cancel-circle2 icon-3x"></i>
                                </div>
                
                                <div class="media-body text-right">
                                    <h3 class="font-weight-semibold mb-0">123</h3>
                                    <span class="text-uppercase font-size-sm">Rejected</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="col-xl-9">
            <!-- Profile navigation -->
            <div class="card">
                <div class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="navbar-collapse collapse" style="margin-left:-40px" id="navbar-second">
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a href="#payout-summary" class="navbar-nav-link active" data-toggle="tab">
                                    <i class="icon-list mr-2"></i> Payout Summary
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#payout-release" class="navbar-nav-link" data-toggle="tab">
                                    <i class="icon-wallet mr-2"></i>Payout Release
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#process-payment" class="navbar-nav-link" data-toggle="tab">
                                    <i class="icon-cash3 mr-2"></i>Process Payment
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /profile navigation -->
            <div class="content">
                <!-- Inner container -->
                <div class="d-flex align-items-start flex-column flex-md-row">
                    <!-- Left content -->
                    <div class="tab-content w-100 order-2 order-md-1">
                        <!-- Payout Summary -->
                        <div class="tab-pane fade active show" id="payout-summary">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Username</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Status</label>
                                                                    <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                                        <option value="Pending">Pending</option>
                                                                        <option value="Approved">Approved</option>
                                                                        <option value="Paid">Paid</option>
                                                                        <option value="Rejected">Rejected</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-8">
                                                                <div class="form-group"><br>
                                                                    <button class="btn bg-slate" data-popup="tooltip" title="Search"><i class="icon-search4"></i></button>
                                                                    <button class="btn btn-warning" data-popup="tooltip" title="Reset"><i class="icon-reset"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table datatable-basic">
                                                        <thead>
                                                                <tr>
                                                                    <th style="width:1px"><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></th>
                                                                    <th>Member</th>
                                                                    <th>Invoice Number</th>
                                                                    <th>Amount</th>
                                                                    <th>Payout Method</th>
                                                                    <th>Paid Date</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                @for($i=1;$i<=10;$i++)
                                                                <tr>
                                                                    <td><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></td>
                                                                    <td>John Doe (username{{$i}}) </td>
                                                                    <td>123 </td>
                                                                    <td><span class="badge badge-success">₹54,542.00</span></td>
                                                                    <td>Bank</td>
                                                                    <td>Wednesday, January 01, 2021</td>
                                                                </tr>
                                                                @endfor
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Payout Release -->
                        <div class="tab-pane fade" id="payout-release">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Username</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Payout Method</label>
                                                                    <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                                        <option value="Paypal">Paypal</option>
                                                                        <option value="Bank Transfer">Bank Transfer</option>
                                                                        <option value="Blockchain">Blockchain</option>
                                                                        <option value="Bitgo">Bitgo</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Payout Type</label>
                                                                    <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                                        <option value="Manual">Manual</option>
                                                                        <option value="User Request">User Request</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">KYC Status</label>
                                                                    <select class="form-control flex-fill w-auto py-2 px-0 border-0 rounded-0 select-user form-control-select2">
                                                                        <option value="KYC Verified">KYC Verified</option>
                                                                        <option value="KYC Not Verified">KYC Not Verified</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group"><br>
                                                                    <button class="btn bg-slate" data-popup="tooltip" title="Search"><i class="icon-search4"></i></button>
                                                                    <button class="btn btn-warning" data-popup="tooltip" title="Reset"><i class="icon-reset"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table datatable-basic">
                                                        <thead>
                                                                <tr>
                                                                    <th style="width:1px"><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></th>
                                                                    <th>Member</th>
                                                                    <th>Payout Amount</th>
                                                                    <th>Payout Method</th>
                                                                    <th>Payout Type</th>
                                                                    <th>E-Wallet Balance</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                @for($i=1;$i<=10;$i++)
                                                                <tr>
                                                                    <td><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></td>
                                                                    <td>John Doe (username{{$i}}) </td>
                                                                    <td><span class="badge badge-info">₹54,542.00</span></td>
                                                                    <td>Bank Transfer</td>
                                                                    <td>Manual</td>
                                                                    <td><span class="badge badge-success">₹54,542.00</span></td>
                                                                </tr>
                                                                @endfor
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Process Payment -->
                        <div class="tab-pane fade" id="process-payment">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="">Username</label>
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-10">
                                                                <div class="form-group"><br>
                                                                    <button class="btn bg-slate" data-popup="tooltip" title="Search"><i class="icon-search4"></i></button>
                                                                    <button class="btn btn-warning" data-popup="tooltip" title="Reset"><i class="icon-reset"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table datatable-basic">
                                                        <thead>
                                                                <tr>
                                                                    <th style="width:1px">#</th>
                                                                    <th style="width:1px"><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></th>
                                                                    <th>Member</th>
                                                                    <th>Amount</th>
                                                                    <th>Payment Method</th>
                                                                    <th>Aprroved Date</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                @for($i=1;$i<=10;$i++)
                                                                <tr>
                                                                    <td>{{$i}}. </td>
                                                                    <td><input type="checkbox" class="group-pv form-check-input-styled" data-fouc></td>
                                                                    <td>John Doe (username{{$i}}) </td>
                                                                    <td><span class="badge badge-info">₹54,542.00</span></td>
                                                                    <th>Bank Transfer</th>
                                                                    <td>Wednesday, January 01, 2021</td>
                                                                </tr>
                                                                @endfor
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('custom')
<script>
    $('a.payout').addClass('active');
</script>
<script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
<script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"  ></script>
<script src="{{ asset('assets/js/plugins/notifications/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"  ></script>
<script src="{{ asset('assets/js/demo_pages/components_modals.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>

@endsection
@endsection