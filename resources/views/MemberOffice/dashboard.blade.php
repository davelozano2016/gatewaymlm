@extends('layouts.MemberOffice.app')
@section('container')

<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">


    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Marketing campaigns -->
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-columned datatable-responsive">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Code</th>
                                <th>Category</th>
                                <th>UPV</th>
                                <th>BPV</th>
                                <th>Distributor</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=1;$i<=10;$i++)
                            <tr>
                                <td>Product {{$i}}</td>
                                <td>Code {{$i}}</td>
                                <td>Category {{$i}}</td>
                                <td>{{number_format('2000',2)}}</td>
                                <td>{{number_format('2000',2)}}</td>
                                <td>Distributor Username (ID No. {{$i}})</td>
                                <td><button type="button" class="btn btn-primary"><i class="icon-eye"></i></button></td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /marketing campaigns -->
        </div>
    </div>
    <!-- /dashboard content -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->



@section('custom')
<script>
    $('a.dashboard').addClass('active');
</script>

<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
<script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>
@endsection
@endsection