@extends('layouts.MemberOffice.app')
@section('container')
<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">


    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <!-- Marketing campaigns -->
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-columned datatable-responsive">
                        <thead>
                            <tr>
                                <th style="width:1px;">#</th>
                                <th>Date Created</th>
                                <th>Package</th>
                                <th>Type</th>
                                <th>UPV</th>
                                <th>BPV</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=1;$i<=10;$i++)
                            <tr>
                                <td>{{$i}}.</td>
                                <td>{{date('Y-m-d h:i:s')}}</td>
                                <td>Platinum</td>
                                <td>PAID</td>
                                <td>{{number_format('100',2)}}</td>
                                <td>{{number_format('100',2)}}</td>
                                </tr>
                                @endfor
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /marketing campaigns -->
        </div>
    </div>
    <!-- /dashboard content -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

<form method="post">
    <div class="modal fade" id="transfer" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-slate">
                    <h5 class="modal-title" id="exampleModalLabel">Transfer Single Pin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Select Package</label>
                                <select class="form-control form-control-select2" required>
                                    <option>Starter</option>
                                    <option>Business</option>
                                    <option>Platinum</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">PIN 1</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">PIN 2</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Transfer Pin to:</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Enter Account Password</label>
                                <input type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Transfer Code</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</form>
    @section('custom')
    <script>
    $('a.mp-paid-pins').addClass('active');
    $('li.my-pins-must-open').addClass('nav-item-expanded nav-item-open');
    </script>
    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_basic.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_responsive.js')}}"></script>

    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/picker_date.js') }}"></script>
    @endsection
    @endsection