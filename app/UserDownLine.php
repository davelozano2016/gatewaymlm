<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDownLine extends Model
{
    function userDetails() {
        return $this->hasOne(User::class, "id", "downline_id");
    }
}

