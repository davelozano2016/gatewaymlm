<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankAccounts extends Model
{
    //

    protected $table = 'users_bank_accounts';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','banks_id','branch','account_name','account_number'];
}
