<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncashmentControls extends Model
{
    //
    protected $table = 'encashment_controls';
    protected $primaryKey = 'id';
    protected $fillable = ['countries_id','tax','maintenance','internal_processing_fee','minimum_encash_request','minimum_gc_request'];
}
