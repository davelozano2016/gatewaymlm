<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageEntry extends Model
{
    //
    protected $table = 'package_entry';
    protected $primaryKey = 'id';
    protected $fillable = ['entries_id','package_type','status'];
}
