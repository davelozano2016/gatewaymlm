<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryTest extends Model
{
    //
    protected $table = 'binary_test';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','waiting_id_number','position'];

}
