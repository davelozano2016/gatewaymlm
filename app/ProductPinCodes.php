<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPinCodes extends Model
{
    //
    protected $table = 'product_pin_codes';
    protected $primaryKey = 'id';
    protected $fillable = ['distributor_id','product_description_id','customer_id','reference','full_name','activation_code','order_number','price','order_status','method_of_payment','local_pickup','status','unilevel_point_value','binary_point_value','lock_status','country_code','is_order','created_by','registered_ip','date_processed','date_used','date_created','date_expiration'];
}
