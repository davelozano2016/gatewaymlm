<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelPackages extends Model
{
    //
    protected $table = 'control_panel_packages';
    protected $primaryKey = 'id';
    protected $fillable = ['entries_id','package_unique_code','package_name','description','package_status'];
}
