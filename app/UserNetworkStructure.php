<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNetworkStructure extends Model
{
    //
    protected $table = 'user_network_structure';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','country','sponsor_id','upline_id','position','activation_code','status_type','group_code','account_type'];
}
