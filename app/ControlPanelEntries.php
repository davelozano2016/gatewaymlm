<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelEntries extends Model
{
    //

    protected $table = 'control_panel_entries';
    protected $primaryKey = 'id';
    protected $fillable = ['countries_id','entry','price','discount_type','stockist_discount','depot_discount','country_manager_discount','direct_referal','binary_point_value'];


}
