<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    //
    protected $table = 'banks';
    protected $primaryKey = 'id';
    protected $fillable = ['banks','banks_logo','banks_status'];

}
