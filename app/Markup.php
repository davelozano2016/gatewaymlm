<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Markup extends Model
{
    //
    protected $table = 'markups';
    protected $primaryKey = 'id';
    protected $fillable = ['booking_type','category','type','amount'];
}
