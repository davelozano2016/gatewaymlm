<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryTotalPoints extends Model
{
    //


    protected $table = 'binary_total_points';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','position','points'];

}
