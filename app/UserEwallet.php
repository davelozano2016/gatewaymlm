<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEwallet extends Model
{
    //
    protected $table = 'user_ewallets';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','fund','verified','status'];
}
