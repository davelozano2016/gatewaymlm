<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTransaction extends Model
{
    //

    protected $table = 'product_transactions';
    protected $primaryKey = 'id';
    protected $fillable = ['reference','billing_firstname','billing_surname','billing_country','billing_email','billing_contact','billing_address1','billing_address2','billing_city','billing_billing_state','billing_zipcode','billing_notes'];
}
