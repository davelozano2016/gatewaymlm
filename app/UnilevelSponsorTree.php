<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnilevelSponsorTree extends Model
{
    //
    protected $table = 'unilevel_sponsor_trees';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','waiting_id_number','upline_id','level','points'];
}
