<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelBinaryParingSettings extends Model
{
    //
    protected $table = 'control_panel_binary_paring_settings';
    protected $primaryKey = 'id';
    protected $fillable = ['countries_id','pair_condition','pair_amount_condition','pair_gc'];

}
