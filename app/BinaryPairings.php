<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryPairings extends Model
{
    //

    protected $table = 'binary_pairings';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','position','points','cycle','date_added'];


}
