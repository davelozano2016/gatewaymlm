<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTracking extends Model
{
    protected $table = 'product_trackings';
    protected $primaryKey = 'id';
    protected $fillable = ['reference','notes','status','order_status'];
}
