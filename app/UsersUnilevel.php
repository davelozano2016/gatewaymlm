<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersUnilevel extends Model
{
    //
    protected $table = 'users_unilevel';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','unilevel_point_value','unilevel_income'];
    
}
