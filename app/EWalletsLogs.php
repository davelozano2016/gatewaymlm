<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EWalletsLogs extends Model
{
    //
    protected $table = 'e_wallets_logs';
    protected $primaryKey = 'id';
    protected $fillable = ['sender_id','receiver_id','category','description','status','value','reference','current_balance'];
}
