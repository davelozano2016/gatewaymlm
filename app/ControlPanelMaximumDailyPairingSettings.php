<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelMaximumDailyPairingSettings extends Model
{
    //
    protected $table = 'control_panel_maximum_daily_pairing_settings';
    protected $primaryKey = 'id';
    protected $fillable = ['entries_id','maximum_pairing'];
}
