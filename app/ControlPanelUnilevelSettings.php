<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelUnilevelSettings extends Model
{
    //
    protected $table = 'control_panel_unilevel_settings';
    protected $primaryKey = 'id';
    protected $fillable = ['maintenance','unilevel_count'];
}
