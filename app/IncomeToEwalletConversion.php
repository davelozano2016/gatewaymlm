<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeToEwalletConversion extends Model
{
    //
    protected $table = 'income_to_ewallet_conversions';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','request_bonus','tax','maintenance','processing_fee','payout'];
}
