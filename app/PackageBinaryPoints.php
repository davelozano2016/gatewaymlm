<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageBinaryPoints extends Model
{
    protected $table = 'package_binary_points';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','upline_id','position','position_on_top','points'];
}
