<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationCodes extends Model
{
    //
    protected $table = 'registration_codes';
    protected $primaryKey = 'id';
    protected $fillable = ['distributor_id','packages_id','customer_id','full_name','activation_code','bpv','order_number','reference','order_status','local_pickup','method_of_payment','price','status','type','country_code','created_by','registered_ip','date_processed','date_used','date_created','date_expiration'];

}
