<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPanelUnilevel extends Model
{
    //

    protected $table = 'control_panel_unilevels';
    protected $primaryKey = 'id';
    protected $fillable = ['level','percentage'];


}
