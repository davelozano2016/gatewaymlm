<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBeneficiary extends Model
{
    //

    protected $table = 'users_beneficiaries';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','name','relationship'];
}
