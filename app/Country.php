<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $fillable = ['name','code','currency_code','currency_name','currency_symbol','timezones','is_active'];

}
