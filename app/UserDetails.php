<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    //
    protected $table = 'users_details';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','nickname','birthdate','civil_status','gender','nationality','province','city','address','zipcode','tin'];
}
