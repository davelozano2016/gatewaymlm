<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    //
    protected $table = 'product_description';
    protected $primaryKey = 'id';

    protected $fillable = ['product_packages_id','country','distributor_price','srp','retailer','discount_type','stockist_discount','depot_discount','country_manager_discount'];

}
