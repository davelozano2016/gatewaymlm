<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductShippingDetails extends Model
{
    protected $table = 'product_shipping_details';
    protected $primaryKey = 'id';
    protected $fillable = ['reference','shipping_firstname','shipping_surname','shipping_country','shipping_email','shipping_contact','shipping_address1','shipping_address2','shipping_city','shipping_shipping_state','shipping_zipcode'];
}
