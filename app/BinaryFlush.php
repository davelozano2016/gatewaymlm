<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryFlush extends Model
{
    //

    protected $table = 'binary_flushes';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','flush_left','flush_right'];

}
