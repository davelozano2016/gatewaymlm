<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'bookings';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','pnr','flight_number','amount','markup'];
}
