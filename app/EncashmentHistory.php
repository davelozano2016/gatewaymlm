<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncashmentHistory extends Model
{
    
    protected $table = 'encashment_histories';
    protected $primaryKey = 'id';
    protected $fillable = ['encashments_id','reference_code'];
    

}
