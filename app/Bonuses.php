<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonuses extends Model
{
    //
    protected $table = 'bonuses';
    protected $primaryKey = 'id';
    protected $fillable = ['recipient','source','bonus_type','amount','cycle','starting_date','ending_date'];
}
