<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryPoints extends Model
{
    //

    protected $table = 'binary_points';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','waiting_id_number','upline_id','level','position','position_on_top','panel','points'];
}
