<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DownLines extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $aRetVal = [];

        foreach($this->downlines AS $aDownLine) {
            $aRetVal[$aDownLine->position] = [
                "user_id" => $aDownLine->downline_id,
                "name" => $aDownLine->userDetails->name,
                "number" => 123450 + $aDownLine->userDetails->id, // TODO: retrieve user number
                "join_date" => "Jan 1, 2021", // TODO: retrieve user join date
                "left_pv" => rand(0, 1000), // TODO: retrieve left points
                "right_pv" => rand(0, 1000), // TODO: retrieve right points
                "left_carry" => rand(0, 1000),  // TODO: retrieve left carry
                "right_carry" => rand(0, 1000), // TODO: retrieve right points
                "personal_pv" => rand(0, 1000), // TODO: retrieve personal points
                "group_pv" => rand(0, 1000), // TODO: retrieve group points
            ];
        }

        return $aRetVal;
    }
}