<?php

namespace App\Http\Controllers\VirtualOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\EWallet;
use App\EWalletsLogs;
use App\IncomeToEwalletConversion;

use Session;
class EWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'E-Wallet';
        $date = Carbon::now();
        $query = DB::table('users')->select('*','users.id as users_id')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        $wallets     = EWallet::where(['id_number' => $query[0]->id_number])->orderBy('created_at','DESC')->get();
        $wallet_logs = EWalletsLogs::where(['sender_id' => $query[0]->id_number])->orderBy('created_at','DESC')->get();
        $credit      = EWalletsLogs::where(['sender_id' => $query[0]->id_number,'status' => 0])->sum('value');
        $debit       = EWalletsLogs::where(['sender_id' => $query[0]->id_number,'status' => 1])->sum('value');
        $total       = $credit - $debit;
        $conversions = IncomeToEwalletConversion::where(['users_id' => Crypt::decryptString(session()->get('id'))])->get();

        $isCD = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->first();


        $encashment = DB::table('control_panel_encashments')->first();


        if($isCD->status_type == 1) {
            $commission_deduction[$isCD->id_number] = $encashment->commission_deduction;
        } else {
            $commission_deduction[$isCD->id_number] = 0;
        }
       

        return view('VirtualOffice.e-wallet',compact('query','title','date','wallet_logs','credit','debit','total','wallets','conversions','commission_deduction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  
        $id_number      = $request->id_number;
        $amount         = $request->amount;
        $notes          = $request->notes;
        $deposit_via    = $request->deposit_via;
        $upload_receipt = $request->file('upload_receipt')->store('/receipts');
        $status         = 'Processing';
        $query          = EWallet::create([
            'id_number'      => $id_number,
            'amount'         => $amount,
            'notes'          => $notes,
            'deposit_via'    => $deposit_via,
            'upload_receipt' => $upload_receipt,
            'status'         => $status,
        ]);
        Session::flash('message', 'Top up message'); 
        return redirect('virtualoffice/e-wallet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
