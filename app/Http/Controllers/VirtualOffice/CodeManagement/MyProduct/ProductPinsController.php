<?php

namespace App\Http\Controllers\VirtualOffice\CodeManagement\MyProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\ProductPinCodes;
use App\Country;
session_start();

class ProductPinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Product Pins';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $products = DB::table('product_pin_codes')->select('product_pin_codes.*','users.id as uid', 'users.id_number','user_network_structure.users_id')
        ->join('users', 'users.id_number', '=', 'product_pin_codes.distributor_id')
        ->join('user_network_structure', 'user_network_structure.users_id', '=', 'users.id')
        ->where(['product_pin_codes.order_status' => 'Completed'])
        ->orWhere(['product_pin_codes.order_status' => 'To Ship'])
        ->orWhere(['product_pin_codes.order_status' => 'To Pickup'])
        ->where(['product_pin_codes.country_code' => $query[0]->country,'users.id' => Crypt::decryptString(session()->get('id'))])->get();


        foreach($products as $product) {
            $product_name[$product->product_description_id] = DB::table('product_packages')->select('*')
            ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
            ->where(['product_description.product_packages_id' => $product->product_description_id])
            ->first();
        }


        
        $unused = ProductPinCodes::where(['status' => 0,'distributor_id' => $query[0]->id_number])->where('order_status','!=','Cancelled')->count();
        $used = ProductPinCodes::where(['status' => 1,'distributor_id' => $query[0]->id_number])->where('order_status','!=','Cancelled')->count();

        $available = ProductPinCodes::where(['status' => 0,'distributor_id' => $query[0]->id_number])
        ->where(['product_pin_codes.order_status' => 'Completed'])
        ->orWhere(['product_pin_codes.order_status' => 'To Ship'])
        ->orWhere(['product_pin_codes.order_status' => 'To Pickup'])
        ->count();


        $pending = ProductPinCodes::where(['status' => 0,'distributor_id' => $query[0]->id_number])
        ->where(['product_pin_codes.order_status' => 'To Pay'])
        ->count();
        
        return view('VirtualOffice.code-management.my-product.product-pins',compact('available','pending','title','query','products','used','unused','product_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function messages()
    {
        return [
            'product_pin_id.required' => 'Choose at least 1 pin code to add to cart.',
        ];
    }


    public function internal_add_to_cart(Request $request)
    {
        $validate = $request->validate(['product_pin_id' => 'required'],[
            'product_pin_id.required' => 'Check at least one product pin code to add.' // custom message
        ]);
        foreach($request->product_pin_id as $data) {
            $id = Crypt::decryptString($data);
            $_SESSION['internal_cart'][$id] = $id;
        }

        
        return redirect('virtualoffice/code-management/my-product/product-cart');
        //
    }

    public function report_pins() {
        $title = 'Report Product Pins';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $products = DB::table('product_pin_codes')->select('product_pin_codes.*','users.id as uid', 'users.id_number','user_network_structure.users_id','product_description.id as pdid','product_description.country','product_description.product_packages_id','product_packages.id as ppid', 'product_packages.product_title','product_packages.product_code')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('users', 'users.id_number', '=', 'product_pin_codes.distributor_id')
        ->join('user_network_structure', 'user_network_structure.users_id', '=', 'users.id')
        ->whereNotIn('product_pin_codes.status',[2])
        ->where(['product_pin_codes.order_status' => 'Completed','product_pin_codes.country_code' => $query[0]->country,'product_pin_codes.distributor_id' => $query[0]->id_number])->get();
        $unused     = ProductPinCodes::where(['status' => 0,'distributor_id' => $query[0]->id_number])->count();
        $used       = ProductPinCodes::where(['status' => 1,'distributor_id' => $query[0]->id_number])->count();
        $countries  = Country::where(['is_active' => 0])->get();
        return view('VirtualOffice.code-management.my-product.report-pins',compact('title','query','products','used','unused','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function result(Request $request) {
        $title = 'Report Product Pins';

        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $report_type  = $request->report_type;
        if($report_type == 'Date Generated') {
            $column   = 'date_created';
        } else {
            $column   = 'date_used';
        }
        $country_code = $query[0]->country;

        $date_start   = $request->date_start.' 00:00:00';
        $date_end     = $request->date_end.' 23:59:59';
        $countries    = Country::where(['is_active' => 0])->get();
        $products = DB::table('product_pin_codes')->select('product_pin_codes.*','users.id as uid', 'users.id_number','user_network_structure.users_id','product_description.id as pdid','product_description.country','product_description.product_packages_id','product_packages.id as ppid', 'product_packages.product_title','product_packages.product_code')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('users', 'users.id_number', '=', 'product_pin_codes.distributor_id')
        ->join('user_network_structure', 'user_network_structure.users_id', '=', 'users.id')
        ->whereBetween($column, [$date_start, $date_end])
        ->where('country_code',$country_code)
        ->whereNotIn('product_pin_codes.status',[2])
        ->where(['product_pin_codes.order_status' => 'Completed','product_pin_codes.country_code' => $query[0]->country,'product_pin_codes.distributor_id' => $query[0]->id_number])->get();
        $used       = ProductPinCodes::where(['status' => 1,'country_code' => $country_code,'distributor_id' => $query[0]->id_number])->whereBetween($column, [$date_start, $date_end])->count();
        $unused     = ProductPinCodes::where(['status' => 0,'country_code' => $country_code,'distributor_id' => $query[0]->id_number])->whereBetween($column, [$date_start, $date_end])->count();
        return view('VirtualOffice.code-management.my-product.report-pins',compact('title','query','products','used','unused','countries','date_start','date_end','country_code'));

    }

    public function unilevel_maintenance() {
        $title = 'Unilevel Maintenance';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $entries = DB::table('product_packages')->select('*')
        ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->where(['product_description.country' => $query[0]->country])
        ->get();

        return view('VirtualOffice.code-management.my-product.unilevel-maintenance',compact('title','query','entries'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
