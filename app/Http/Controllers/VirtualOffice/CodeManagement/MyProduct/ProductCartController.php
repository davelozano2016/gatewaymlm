<?php

namespace App\Http\Controllers\VirtualOffice\CodeManagement\MyProduct;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\ProductPinCodes;
session_start();

class ProductCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $title = 'Product Cart';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        if(isset($_SESSION['internal_cart'])) {
            foreach($_SESSION['internal_cart'] as $id) {
                $cart[] = DB::table('product_pin_codes')->select('product_pin_codes.id','product_pin_codes.unilevel_point_value as upv','product_pin_codes.activation_code as code', 'product_pin_codes.binary_point_value as bpv','product_packages.product_title')
                ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
                ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
                ->where(['product_pin_codes.id' => $id])->get();
            }
        } else {
           
        }

        if(isset($cart)) {
            
        } else {
            return redirect('virtualoffice/code-management/my-product/product-pins');

        }
        return view('VirtualOffice.code-management.my-product.product-cart',compact('title','query','cart'));
    }

    public function remove_item_from_cart($id) {
        unset($_SESSION['internal_cart'][Crypt::decryptString($id)]);
        return redirect('virtualoffice/code-management/my-product/product-cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
