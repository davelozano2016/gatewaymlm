<?php

namespace App\Http\Controllers\VirtualOffice\CodeManagement\MyProduct;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductPinsHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $title = 'Purchase Product History';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        $product_history = DB::table('product_pin_codes')->select('*',DB::raw("SUM(product_pin_codes.unilevel_point_value) as total_upv"),DB::raw("SUM(product_pin_codes.price) as total"),DB::raw("SUM(product_pin_codes.binary_point_value) as total_bpv"),DB::raw("COUNT(product_pin_codes.reference) as items"),'product_pin_codes.date_created as created','product_pin_codes.date_expiration as expiration')
        ->join('countries', 'countries.code', '=', 'product_pin_codes.country_code')
        ->orderBy('product_pin_codes.date_created','DESC')
        ->groupBy('reference')->get();
        return view('VirtualOffice.code-management.my-product.purchase-product-history',compact('title','query','product_history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($reference)
    {
        //
        $title = 'Purchase Product History';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        
        $details = DB::table('product_pin_codes')->select('*',DB::raw("SUM(product_pin_codes.unilevel_point_value) as total_upv"),DB::raw("SUM(product_pin_codes.price) as total"),DB::raw("SUM(product_pin_codes.binary_point_value) as total_bpv"),DB::raw("COUNT(product_pin_codes.reference) as items"),'product_pin_codes.date_created as created')
        ->join('countries', 'countries.code', '=', 'product_pin_codes.country_code')
        ->where(['product_pin_codes.reference' => Crypt::decryptString($reference)])
        ->groupBy('reference')->get();

        $product_history = DB::table('product_pin_codes')->select('*')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('countries', 'countries.code', '=', 'product_pin_codes.country_code')
        ->where(['reference' => Crypt::decryptString($reference)])
        ->orderBy('product_pin_codes.date_created','DESC')
        ->get();


        return view('VirtualOffice.code-management.my-product.view-purchase-product-history',compact('title','query','product_history','details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
