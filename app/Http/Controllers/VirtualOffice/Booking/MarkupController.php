<?php

namespace App\Http\Controllers\VirtualOffice\Booking;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Markup;

class MarkupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Manage Sales Markup';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $markups = Markup::where(['users_id' => Crypt::decryptString(session()->get('id'))])->get();
        return view('VirtualOffice.booking.markup',compact('title','query','markups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) 
    {
        //
        $count = count($request->id);
        for($i=0;$i < $count;$i++) {
            $query = Markup::where(['id' => Crypt::decryptString($request->id[$i])])->first();
            if($query->booking_type == 'Flight') {
                if($query->category == 'Domestic Markup') {
                    $limit = 20;
                } else {
                    $limit = 60;
                }

                if($request->amount[$i] > $limit) {
                    $message = 'Domestic / International something';
                } else {
                    Markup::where(['id' => Crypt::decryptString($request->id[$i])])->update(['type' => $request->type[$i],'amount' => $request->amount[$i]]);
                    $message =  'Markup has been updated';
                }
            } else {
            }
            
        }

        return redirect()->back()->with('message',$message);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
