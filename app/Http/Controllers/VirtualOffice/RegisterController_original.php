<?php

namespace App\Http\Controllers\VirtualOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\UserEwallet;
use App\User;
use App\SocialMedia;
use App\UserNetworkStructure;
use App\RegistrationCodes;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserSocialMediaAccounts;
use App\Bonuses;
use App\UserAccount;
use App\BinaryPoints;
use App\UsersUnilevel;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Register';
        $id = session()->get('id');
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id' => Crypt::decryptString($id)])->get();
        return view('VirtualOffice.register',compact('title','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $surname         = $request->surname;
        $firstname       = $request->firstname;
        $middlename      = $request->middlename;
        $email           = $request->email;

        $sponsor_id      = $request->sponsor_id;
        $upline_id       = $request->upline_id;
        $position        = $request->position;
        $activation_code = $request->activation_code;

        $username        = $request->username;
        $password        = Crypt::encryptString($request->password);
        $query           = RegistrationCodes::where(['activation_code' => $activation_code, 'status' => 0, 'lock_status' => 0])->count();
        if($query == 1) {
            $data       = RegistrationCodes::where(['activation_code' => $activation_code])->get();
            $rc_id      = $data[0]->id;
            $type       = $data[0]->type;
            $country    = $data[0]->country_code;
            $id_number  = rand(111111,999999);
            User::create([
                'id_number'      => $id_number,
                'surname'        => $surname,
                'firstname'      => $firstname,
                'middlename'     => $middlename,
                'email'          => $email,
                'username'       => $username,
                'password'       => $password,
                'status'         => 0,
                'remember_token' => rand(111111,999999),
            ]);
            
            $user                 = User::latest()->first();
            $id                   = $user->id;
            $id_number            = $user->id_number;
            $network = UserNetworkStructure::create([
                'users_id'        => $id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => $activation_code,
                'status_type'     => $type,
                'group_code'      => Crypt::encryptString($id_number),
            ]);

            if($network) {
                RegistrationCodes::where(['id' => $rc_id])->update(['used_by' => $id_number,'status' => 1,'date_used' => NOW()]);
                UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
                return redirect('virtualoffice/register/')->with('message', 'New distributor has been added.');
            }

        } else {
            return redirect('virtualoffice/register/')->with('message', 'Activation code already used or has been locked.');
        }
       

    }

    public function create_sub_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $tophead = User::count();
        $row = User::where(['id'=>  Crypt::decryptString(session()->get('id'))])->get();
        $registration_codes = DB::table('registration_codes')->select('registration_codes.*','control_panel_packages.entries_id','control_panel_packages.id','control_panel_entries.id','control_panel_entries.entry','control_panel_entries.direct_referal','control_panel_entries.binary_point_value','countries.id','control_panel_entries.countries_id','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->where(['registration_codes.status' => 0,'registration_codes.distributor_id' => $row[0]->id_number])->get();

        $increment_username = UserNetworkStructure::where(['sponsor_id' => Crypt::decryptString($sponsor_id)])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id_number' => Crypt::decryptString($sponsor_id)])
        ->get();
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        return view('VirtualOffice.register-sub-account',compact('query','title','tophead','sponsor_id','id_number','position','registration_codes','increment','users'));
    }

    public function create_main_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $countries = Country::where(['is_active' => 0])->get();
        $tophead = User::count();
        $id = session()->get('id');
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id' => Crypt::decryptString($id)])->get();
        return view('VirtualOffice.register-main-account',compact('query','title','countries','tophead','sponsor_id','id_number','position'));
    }

    public function store_sub_account(Request $request) {
        $activation_code = $request->sub_account_activation_code;
        $sponsor_id      = $request->sub_account_sponsor_id;
        $upline_id       = $request->sub_account_placement_id;
        $position        = $request->sub_account_position;
        $group_code      = $request->sub_account_group_code;
        $username        = $request->sub_account_username;
        $password        = Crypt::encryptString($request->sub_account_password);
        $query           = RegistrationCodes::where(['activation_code' => $activation_code, 'status' => 0, 'lock_status' => 0])->count();
        $checkUser       = BinaryPoints::where(['id_number' => $upline_id,'position' => $position])->first();

        if($checkUser->position_on_top == 2) {
            $position_on_top = $position;
        } else {
            $position_on_top = $checkUser->position_on_top;
        }


        if($query == 1) {
            $data       = RegistrationCodes::where(['activation_code' => $activation_code])->get();
            $rc_id      = $data[0]->id;
            $type       = $data[0]->type;
            $country    = $data[0]->country_code;
            $bpv        = $data[0]->bpv;
            $id_number  = rand(111111,999999);
            $row        = User::where(['id_number' => $sponsor_id])->get();
            $data       = DB::table('registration_codes')->select('*')
                        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                        ->where(['registration_codes.activation_code' => $activation_code])
                        ->get();
            
            User::create([
                'id_number'      => $id_number,
                'surname'        => $row[0]->surname,
                'firstname'      => $row[0]->firstname,
                'middlename'     => $row[0]->middlename,
                'email'          => $row[0]->email,
                'username'       => $username,
                'password'       => $password,
                'status'         => 0,
                'remember_token' => rand(1111111,9999999),
            ]);
            
            $user                 = User::latest()->first();
            $users_id             = $user->id;
            $source               = $user->id_number;
            $network = UserNetworkStructure::create([
                'users_id'        => $users_id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => $activation_code,
                'status_type'     => $type,
                'group_code'      => $group_code,
                'account_type'    => 1,
            ]);

            if($network) {
                $direct_referal = $data[0]->direct_referal;
                $sponsor_level_id = $request->sub_account_sponsor_id;
                RegistrationCodes::where(['id' => $rc_id])->update(['used_by' => $id_number,'status' => 1,'date_used' => NOW()]);
                UserEwallet::create(['users_id' => $users_id]);
                UserDetails::create(['users_id' => $users_id]);
                UserContacts::create(['users_id' => $users_id]);
                UserBeneficiary::create(['users_id' => $users_id]);
                UserBankAccounts::create(['users_id' => $users_id]);
                UserAccount::create(['users_id' => $users_id]);
                UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
                BinaryPoints::create(['id_number' => $source,'waiting_id_number' => $source, 'level' => 0,'position' => 0,'position_on_top' => $position_on_top,'points' => 0]);
                BinaryPoints::create(['id_number' => $source,'waiting_id_number' => $source, 'level' => 0,'position' => 1,'position_on_top' => $position_on_top,'points' => 0]);
                $updateBPV = BinaryPoints::where(['id_number' => $sponsor_id,'position' => $position])->first();
                // update point value depends on position on top and position
                if($updateBPV) {
                    $points = $updateBPV->points + $bpv;
                    BinaryPoints::where(['id_number' => $sponsor_id,'position' => $position_on_top,'level' => 0])->update(['points' => $points]);
                }
                $level = BinaryPoints::where(['id_number' => $sponsor_level_id,'waiting_id_number' => $source])->count();
                if($level == 0) {
                    BinaryPoints::create(['id_number' => $sponsor_level_id,'waiting_id_number' => $source, 'level' => 1,'position' => $position,'position_on_top' => $position_on_top,'points' => 0]);
                } 
                $binary_level = BinaryPoints::where(['waiting_id_number' => $sponsor_level_id])->whereNotIn('waiting_id_number',[0])->get();
                foreach($unilevel as $row) {
                    if($row->level >= 1) {
                        $lvl = $row->level + 1;
                        BinaryPoints::create(['id_number' => $row->id_number,'waiting_id_number' => $source, 'level' => $lvl,'position' => $position,'position_on_top' => $position_on_top,'points' => 0]);
                    } 
                }

                // do {
                    // $doBinary = BinaryPoints::where(['id_number' => $upline_id,'position' => $position_on_top])->get();
                    // $points = $doBinary[0]->points + $bpv;
                    // foreach($doBinary as $binary) {
                    //     BinaryPoints::where(['id_number' => $binary->id_number,'position' => $position])->update(['points' => $points]);
                    // }
                // } while($doBinary[0]->level > 0 );
                
                // PackageBinaryPoints::create(['id_number' => $source,'upline_id' => $upline_id,'position' => 1,'position_on_top' => $position_on_top,'points' => 0]);
                
                // if($updateBPV) {
                //     $points = $updateBPV->points + $bpv;
                //     BinaryPoints::where(['id_number' => $sponsor_id,'position' => $position_on_top,'level' => 0])->update(['points' => $points]);
                // }

                // $level = BinaryPoints::where(['id_number' => $sponsor_level_id,'waiting_id_number' => $source])->count();
                // if($level == 0) {
                //     BinaryPoints::create(['id_number' => $sponsor_level_id,'waiting_id_number' => $source, 'level' => 1,'position' => $position,'position_on_top' => $position_on_top,'points' => 0]);
                // } 
                // $unilevel = BinaryPoints::where(['waiting_id_number' => $sponsor_level_id])->whereNotIn('waiting_id_number',[0])->get();
                // foreach($unilevel as $row) {
                //     if($row->level >= 1) {
                //         $lvl = $row->level + 1;
                //         BinaryPoints::create(['id_number' => $row->id_number,'waiting_id_number' => $source, 'level' => $lvl,'position' => $position,'position_on_top' => $position_on_top,'points' => 0]);
                //     } 
                // }

                
                Bonuses::create(['recipient' => $sponsor_id,'source' => $source,'bonus_type' => 'Direct Referral','amount' => $direct_referal, 'cycle' => 2]);
                $social= SocialMedia::all();
                foreach($social as $s) {
                    UserSocialMediaAccounts::create(['users_id' => $users_id,'social_media_id' => $s->id]);
                }
                return redirect('virtualoffice/network/genealogy-tree')->with('message', 'New sub account has been added.');
            }

        } else {
            return redirect('virtualoffice/network/genealogy-tree')->with('message', 'Activation code already used or has been locked.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
