<?php

namespace App\Http\Controllers\VirtualOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\ProductDescription;
use App\ControlPanelPackages;
use App\ControlPanelEntries;
use App\Country;
use App\ProductInclusions;
session_start();
class ECommerceStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'E-Commerce Store';
        $query = DB::table('users')->select('*','countries.id as countries_id')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        
        $entries = DB::table('product_packages')->select('*')
        ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->where(['product_description.country' => $query[0]->country])
        ->get();

        $packages = DB::table('control_panel_entries')
        ->select('countries.name','countries.code','countries.id','countries.currency_code','countries.currency_symbol','countries.currency_name','control_panel_entries.*','control_panel_entries.id as entry_id','control_panel_packages.id as packages_id','control_panel_packages.package_name','control_panel_packages.package_unique_code','control_panel_packages.description')
        ->join('countries', 'control_panel_entries.countries_id', '=', 'countries.id')
        ->join('control_panel_packages', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['countries.id' => $query[0]->countries_id])
        ->get();
        $package_inclusions = [];
        foreach($packages as $package) {
            $package_inclusions[$package->package_unique_code] = DB::table('product_packages')->select('*')
            ->join('product_inclusions', 'product_inclusions.product_package_id', '=', 'product_packages.id')
            ->where(['unique_code' => $package->package_unique_code])
            ->get();  
        }

        return view('VirtualOffice.e-commerce-store',compact('query','title','entries','packages','package_inclusions'));
    }

    public function package_add_to_cart($id) {
        $package_id = Crypt::decryptString($id);
        $package    = ControlPanelPackages::where(['id' => $package_id])->first();
        $entries    = ControlPanelEntries::where(['id' => $package->entries_id])->first();
        $countries  = Country::where(['id' => $entries->countries_id])->first();
        $_SESSION['package_cart'][$package_id] = array(
            'package_id'         => $package_id,
            'entry'              => $entries->entry,
            'package_name'       => $package->package_name,
            'price'              => $entries->price,
            'direct_referal'     => $entries->direct_referal,
            'binary_point_value' => $entries->binary_point_value,
            'country'            => $countries->code,
            'currency_symbol'    => $countries->currency_symbol,
            'quantity'           => isset($_SESSION['package_cart'][$package_id]['quantity']) ? $_SESSION['package_cart'][$package_id]['quantity'] + 1 : 1
        );
        return redirect('virtualoffice/e-commerce-store/package/cart')->with('message','Package has been added to your cart.');
    }



    public function package_cart() {
        $title = 'My Cart';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
       
        if(count($_SESSION['package_cart']) == 0) {
            return redirect('virtualoffice/e-commerce-store')->with('message','Your cart is empty');
        }

        
        return view('VirtualOffice.ecommerce.cart-package',compact('title','query'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function add_to_cart($product_packages_id,$country) {
        $id = Crypt::decryptString($product_packages_id);
        $countries = Crypt::decryptString($country);
        if(isset($product_packages_id)) {
            $query = DB::table('product_packages')->select('product_packages.*','product_description.id as pdid','product_description.product_packages_id','product_description.country','product_description.distributor_price','product_description.srp','product_description.stockist_discount','product_description.depot_discount','country_manager_discount','countries.code','countries.currency_symbol')
            ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
            ->join('countries', 'countries.code', '=', 'product_description.country')
            ->where(['product_description.country' => $countries,'product_description.product_packages_id' => $id])
            ->get();

            if($query->count() != 0) {
                if($id == $query[0]->product_packages_id && $countries == $query[0]->country) {
                    if($query[0]->product_packages_id == @$_SESSION['cart'][$query[0]->product_packages_id]['product_packages_id']) {
                        $_SESSION['cart'][$query[0]->product_packages_id]['quantity'] += 1;
                    } else {
                        $_SESSION['cart'][$query[0]->product_packages_id] = array(
                            'product_description_id' => $query[0]->pdid,
                            'product_packages_id'    => $query[0]->product_packages_id,
                            'country'                => $query[0]->country,
                            'product_title'          => $query[0]->product_title,
                            'product_code'           => $query[0]->product_code,
                            'binary_points'          => $query[0]->binary_points,
                            'unilevel_points'        => $query[0]->unilevel_points,
                            'product_stocks'         => $query[0]->product_stocks,
                            'product_image'          => $query[0]->product_image,
                            'distributor_price'      => $query[0]->distributor_price,
                            'currency_symbol'        => $query[0]->currency_symbol,
                            'quantity'               => 1
                        );
                    }
                }
            } 
            // session_destroy();
            return redirect('virtualoffice/store/cart')->with('message',$query[0]->product_title.' has been added to your cart.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
