<?php

namespace App\Http\Controllers\VirtualOffice\Encashment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Encashment;
use App\UserEwallet;
use App\Country;
use App\EWalletsLogs;
use App\EncashmentControls;
use Carbon\Carbon;


class EncashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Encashment';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $group_code = Crypt::decryptString($query[0]->group_code);

        $group = DB::table('user_network_structure')->get();
        $fund = 0;
        foreach($group as $data) {
            if($group_code == Crypt::decryptString($data->group_code)) {
                $wallet = DB::table('user_ewallets')->where(['users_id' => $data->users_id])->first();
                $fund += $wallet->fund;
                $users[] = DB::table('users')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
                ->where(['users.id' => $data->users_id])->get();
            }
        }
        return view('VirtualOffice.encashment.encash',compact('title','query','fund','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function encash_bonus(Request $request) {
        $users_id      = Crypt::decryptString(session()->get('id'));
        $request_type  = $request->request_type; 
        $country_code  = $request->country_code;
        $request_bonus = $request->request_bonus;
        $prefix        = substr($request_type, 0, 1);
        $reference     = $prefix.rand(111,999).date('mdY').rand(111,999).$country_code;
        $userWallet    = UserEwallet::where(['users_id' => $users_id])->first();
        $countries     = Country::where(['code' => $country_code])->first();
        $userQuery     = DB::table('users')->where(['id' => $users_id])->first();

        if($request_bonus >= 2500) {
            if($request_bonus <= $userWallet->fund) {
                $controls                     = EncashmentControls::where(['countries_id' => $countries->id])->first();
                $fund                         = $userWallet->fund; 
                $payout                       = $request_bonus;
                $wallet                       = $userWallet->fund - $payout;
                $query                        = Encashment::create([
                    'users_id'                => $users_id,
                    'request_type'            => $request_type,
                    'request_bonus'           => $request_bonus,
                    'reference'               => $reference,
                    'country_code'            => $country_code,
                    'ip_address'              => \Request::ip(),
                    'payout'                  => $payout,
                    'status'                  => 0, // Pending
                ]);
                if($query) {
                    $total = $fund - $payout;
                    EWalletsLogs::create(['sender_id' => $userQuery->id_number,'receiver_id' => $userQuery->id_number,'category' => 'Encashment','description' => 'Pending','status' => 1, 'value' => $payout,'reference' => $reference,'current_balance' => $wallet]);
                    UserEwallet::where(['users_id' => $users_id])->update(['fund' => $total]);
                }
                $message = 'Encashment has been sent.'; 
            } else {
                $message = 'Insufficient balance.'; 
            }
        } else {
            $message = '₱2,500.00  minimum encashment';
        }


      
        return redirect('virtualoffice/encashment/encash')->with('message',$message);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
