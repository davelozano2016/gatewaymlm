<?php

namespace App\Http\Controllers\VirtualOffice\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session_start();
        $title = 'My Cart';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $entries = DB::table('product_packages')->select('*')
        ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->where(['product_description.country' => $query[0]->country])
        ->get();
        if(count($_SESSION['cart']) == 0) {
            return redirect('virtualoffice/e-commerce-store')->with('message','Your cart is empty');
        }

        
        return view('VirtualOffice.ecommerce.cart',compact('title','query','entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        session_start();
        $i=0;
        foreach($_SESSION['cart'] as $key => $value) {
            $quantity                           = $request->quantity[$i];
            $_SESSION['cart'][$key]['quantity'] = $quantity;
            if($_SESSION['cart'][$key]['quantity'] == 0) {
                unset($_SESSION['cart'][$key]);
            }
            $i++;
        }

        // session_destroy();
      
        // dd($_SESSION['cart']);

        return redirect('virtualoffice/store/cart')->with('message','Your cart has been updated.');

    }


    public function package_update(Request $request)
    {
        //
        session_start();
        $i=0;
        foreach($_SESSION['package_cart'] as $key => $value) {
            $quantity = $request->quantity[$i];
            $_SESSION['package_cart'][$key]['quantity'] = $quantity;
            if($_SESSION['package_cart'][$key]['quantity'] == 0) {
                unset($_SESSION['package_cart'][$key]);
            }
            $i++;
        }

        // session_destroy();
      
        // dd($_SESSION['cart']);

        return redirect('virtualoffice/e-commerce-store/package/cart')->with('message','Your cart has been updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
