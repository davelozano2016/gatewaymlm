<?php
namespace App\Http\Controllers\VirtualOffice\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserEwallet;
use App\ProductPinCodes;
use App\ProductShippingDetails;
use App\ProductTracking;
use App\ProductTransaction;
use App\EWalletsLogs;
use App\RegistrationCodes;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'My Cart';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $wallet = UserEwallet::where(['users_id' =>Crypt::decryptString(session()->get('id'))])->get();
        
        return view('VirtualOffice.ecommerce.checkout',compact('title','query','wallet'));
    }

    public function package() {
        $title = 'My Cart';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $wallet = UserEwallet::where(['users_id' =>Crypt::decryptString(session()->get('id'))])->get();
        
        return view('VirtualOffice.ecommerce.package-checkout',compact('title','query','wallet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function package_place_order() {

        session_start();
        $id    = Crypt::decryptString(session()->get('id'));
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('users_contacts', 'users_contacts.users_id', '=', 'users.id')
        ->join('users_details', 'users_details.users_id', '=', 'users.id')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => $id])->get();
        $id_number           = $query[0]->id_number;
        $full_name           = $query[0]->firstname.' '.$query[0]->surname;
        $order_number        = 'OR'.rand(111111,999999).date('Ymd');
        $customer_id         = 'C'.rand(11,99).date('Ymd');
        $local_pickup        = session()->get('local_pickup');
        $method_of_payment   = session()->get('method_of_payment');
        $permitted_chars     = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $reference           = substr(str_shuffle($permitted_chars), 0, 7);
        $registered_ip       = \Request::ip();

        if($method_of_payment == 'E-Wallet' || $method_of_payment == 'Credit Card') {

            if($method_of_payment == 'Cash On Delivery') {
                $payment = 'Processing';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'Bank Deposit') {
                $payment = 'Pending Payment';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'GCash') {
                $payment = 'Pending Payment';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'E-Wallet' || $method_of_payment == 'Credit Card') {
                
                ProductTracking::create(['reference' => $reference,'notes' => 'Your order has been placed. Thank you','status' => 'To Pay', 'order_status' => 'Payment Pending']);
                ProductTracking::create(['reference' => $reference,'notes' => 'Your payment has been verifying.','status' => 'To Pay', 'order_status' => 'Verifying']);
                ProductTracking::create(['reference' => $reference,'notes' => 'Your payment has been verified.','status' => 'To Pay', 'order_status' => 'Verified']);
                $payment = 'To Claim';
                $status = 'To Pickup';
                $notes = 'Your order has been processed.';

                // $payment = 'Processing';
                // $status = 'To Ship';
                // $notes = 'Your order has been processed.';
            } 
            
            $total = 0;
            foreach($_SESSION['package_cart'] as $key => $value) {
                $total += $value['price'] * $value['quantity'];

            }

            if($method_of_payment == 'E-Wallet') {
                $wallet  = UserEwallet::where(['users_id' => $id])->first();
                $balance = $wallet->fund;
                if($balance >= $total) {
                $current = $balance - $total;
                
                UserEwallet::where(['users_id' => $id])->update(['fund' => $balance - $total]);
                EWalletsLogs::create(['sender_id' => $id_number,'receiver_id' => $id_number,'category' => 'E-Wallet Purchase','description' => 'Purchase package using E-Wallet','status' => 1,'value'=>$total,'reference' => 'PP'.$reference,'current_balance' => $current]);
                } else {
                    return redirect('virtualoffice/store/package/checkout')->with('message','You do not have enough wallet balance to complete the transaction.');
                }
            }

            
    
            ProductTracking::create(['reference' => $reference,'notes' => $notes,'status' => $status, 'order_status' => $payment]);

            $query_transactions  = ProductTransaction::create([
                'reference'         => $reference,
                'billing_firstname' => $query[0]->firstname,
                'billing_surname'   => $query[0]->surname,
                'billing_country'   => $query[0]->country,
                'billing_email'     => $query[0]->email,
                'billing_contact'   => $query[0]->contact_number,
                'billing_address1'  => $query[0]->address,
                'billing_address2'  => '',
                'billing_city'      => $query[0]->city,
                'billing_state'     => '',
                'billing_zipcode'   => $query[0]->zipcode,
                'billing_notes'     => '',
            ]);
            if($query_transactions) {
                $query_transactions  = ProductShippingDetails::create([
                    'reference'         => $reference,
                    'shipping_firstname' => $query[0]->firstname,
                    'shipping_surname'   => $query[0]->surname,
                    'shipping_country'   => $query[0]->country,
                    'shipping_email'     => $query[0]->email,
                    'shipping_contact'   => $query[0]->contact_number,
                    'shipping_address1'  => $query[0]->address,
                    'shipping_address2'  => '',
                    'shipping_city'      => $query[0]->city,
                    'shipping_state'     => '',
                    'shipping_zipcode'   => $query[0]->zipcode,
                    'shipping_notes'     => '',
                ]);
            }
            

            foreach($_SESSION['package_cart'] as $key => $value) {
                $prefix = substr($value['entry'], 0, 1);
                $distributor_id = $id_number;
                $packages_id = $value['package_id'];
                $bpv = $value['binary_point_value'];
                $price = $value['price'];
                $country_code = $value['country'];
                
                for($x=1;$x<=$value['quantity'];$x++) {
                    $activation_code = $prefix.rand(11111,99999).'P'.rand(11111,99999).$value['country'];
                    RegistrationCodes::create([
                        'distributor_id'    => $distributor_id,
                        'packages_id'       => $packages_id,
                        'customer_id'       => $customer_id,
                        'full_name'         => $full_name,
                        'activation_code'   => $activation_code,
                        'bpv'               => $bpv,
                        'order_number'      => $order_number,
                        'reference'         => $reference,
                        'order_status'      => $status,
                        'local_pickup'      => $local_pickup,
                        'method_of_payment' => $method_of_payment,
                        'price'             => $price,
                        'status'            => 0,
                        'type'              => 0, // Paid
                        'country_code'      => $country_code, 
                        'created_by'        => $full_name,
                        'registered_ip'     => $registered_ip,
                        'date_processed'    => date('Y-m-d h:i:s'),
                        'date_expiration'   => date('Y-m-d h:i:s',strtotime('+1 month')),
                        'date_used'         => '0000-00-00 00:00:00',
                        'date_created'      => date('Y-m-d h:i:s'),
                    ]);
                }
            }
        }
        unset($_SESSION['package_cart']);
        
        return redirect('virtualoffice/dashboard')->with('message','Transaction has been completed.');
        
    }

    public function place_order() {
        session_start();
        $id    = Crypt::decryptString(session()->get('id'));
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('users_contacts', 'users_contacts.users_id', '=', 'users.id')
        ->join('users_details', 'users_details.users_id', '=', 'users.id')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => $id])->get();
        $id_number           = $query[0]->id_number;
        $full_name           = $query[0]->firstname.' '.$query[0]->surname;
        $order_number        = 'OR'.rand(111111,999999).date('Ymd');
        $customer_id         = 'CUST-'.rand(1111,9999).date('Ymd');
        $local_pickup        = session()->get('local_pickup');
        $method_of_payment   = session()->get('method_of_payment');
        $permitted_chars     = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $reference           = substr(str_shuffle($permitted_chars), 0, 7);
        $registered_ip       = \Request::ip();

        if($method_of_payment == 'E-Wallet' || $method_of_payment == 'Credit Card') {

            if($method_of_payment == 'Cash On Delivery') {
                $payment = 'Processing';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'Bank Deposit') {
                $payment = 'Pending Payment';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'GCash') {
                $payment = 'Pending Payment';
                $status - 'To Pay';
                $notes = 'Your order has been placed. Thank you';
            } elseif($method_of_payment == 'E-Wallet' || $method_of_payment == 'Credit Card') {
                
                ProductTracking::create(['reference' => $reference,'notes' => 'Your order has been placed. Thank you','status' => 'To Pay', 'order_status' => 'Payment Pending']);
                ProductTracking::create(['reference' => $reference,'notes' => 'Your payment has been verifying.','status' => 'To Pay', 'order_status' => 'Verifying']);
                ProductTracking::create(['reference' => $reference,'notes' => 'Your payment has been verified.','status' => 'To Pay', 'order_status' => 'Verified']);
                $payment = 'To Claim';
                $status = 'To Pickup';
                $notes = 'Your order has been processed.';

                // $payment = 'Processing';
                // $status = 'To Ship';
                // $notes = 'Your order has been processed.';
            } 
            
            $total = 0;
            foreach($_SESSION['cart'] as $key => $value) {
                $total += $value['distributor_price'] * $value['quantity'];

            }

            if($method_of_payment == 'E-Wallet') {
                $wallet  = UserEwallet::where(['users_id' => $id])->first();
                $balance = $wallet->fund;
                if($balance >= $total) {
                $current = $balance - $total;
                
                UserEwallet::where(['users_id' => $id])->update(['fund' => $balance - $total]);
                EWalletsLogs::create(['sender_id' => $id_number,'receiver_id' => $id_number,'category' => 'E-Wallet Purchase','description' => 'Purchase product using E-Wallet','status' => 1,'value'=>$total,'reference' => 'PP'.$reference,'current_balance' => $current]);
                } else {
                    return redirect('virtualoffice/store/checkout')->with('message','You do not have enough wallet balance to complete the transaction.');
                }
            }

            
    
            ProductTracking::create(['reference' => $reference,'notes' => $notes,'status' => $status, 'order_status' => $payment]);

            $query_transactions  = ProductTransaction::create([
                'reference'         => $reference,
                'billing_firstname' => $query[0]->firstname,
                'billing_surname'   => $query[0]->surname,
                'billing_country'   => $query[0]->country,
                'billing_email'     => $query[0]->email,
                'billing_contact'   => $query[0]->contact_number,
                'billing_address1'  => $query[0]->address,
                'billing_address2'  => '',
                'billing_city'      => $query[0]->city,
                'billing_state'     => '',
                'billing_zipcode'   => $query[0]->zipcode,
                'billing_notes'     => '',
            ]);
            if($query_transactions) {
                $query_transactions  = ProductShippingDetails::create([
                    'reference'         => $reference,
                    'shipping_firstname' => $query[0]->firstname,
                    'shipping_surname'   => $query[0]->surname,
                    'shipping_country'   => $query[0]->country,
                    'shipping_email'     => $query[0]->email,
                    'shipping_contact'   => $query[0]->contact_number,
                    'shipping_address1'  => $query[0]->address,
                    'shipping_address2'  => '',
                    'shipping_city'      => $query[0]->city,
                    'shipping_state'     => '',
                    'shipping_zipcode'   => $query[0]->zipcode,
                    'shipping_notes'     => '',
                ]);
            }
            

            foreach($_SESSION['cart'] as $key => $value) {
                $product_description_id = $value['product_description_id'];
                $product_title          = $value['product_title'];
                $product_code           = $value['product_code'];
                $quantity               = $value['quantity'];


                $unilevel_point_value   = $value['unilevel_points'];
                $binary_point_value     = $value['binary_points'];
                $distributor_price      = $value['distributor_price'];
                $country_code           = $value['country'];
                $created_by             = $full_name;
                $date_processed         = date('Y-m-d h:i:s');
                $date_used              = '0000-00-00 00:00:00';
                $date_created           = date('Y-m-d h:i:s');
                $date_expiration        = date('Y-m-d h:i:s',strtotime('+1 month'));
                $words                  = explode(" ",$product_title);
                $acronym                = "";
                foreach ($words as $w) { $acronym .= $w[0]; }
            
                for($i=1;$i<=$quantity;$i++) {

                    $activation_code = $acronym.rand(111111,999999).$product_code.date('Ymd');
                    ProductPinCodes::create([
                        'distributor_id'         => $id_number,
                        'product_description_id' => $product_description_id,
                        'customer_id'            => $customer_id,
                        'reference'              => $reference,
                        'full_name'              => $full_name,
                        'activation_code'        => $activation_code,
                        'order_number'           => $order_number,
                        'price'                  => $distributor_price,
                        'order_status'           => $status,
                        'method_of_payment'      => $method_of_payment,
                        'local_pickup'           => $local_pickup,
                        'unilevel_point_value'   => $unilevel_point_value,
                        'binary_point_value'     => $binary_point_value,
                        'country_code'           => $country_code,
                        'created_by'             => $created_by,
                        'registered_ip'          => $registered_ip,
                        'date_processed'         => $date_processed,
                        'date_expiration'        => $date_expiration,
                        'date_used'              => $date_used,
                        'date_created'           => $date_created
                    ]);
                }
            }
        }
        unset($_SESSION['cart']);
        
        return redirect('virtualoffice/dashboard')->with('message','Transaction has been completed.');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function processing(Request $request)
    {
        //

        session(['local_pickup' =>  $request->local_pickup, 'method_of_payment' => $request->method_of_payment]);
        return redirect('virtualoffice/store/place-order');

    }

    public function package_processing(Request $request)
    {
        //

        session(['local_pickup' =>  $request->local_pickup, 'method_of_payment' => $request->method_of_payment]);
        return redirect('virtualoffice/store/package/place-order');

    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}