<?php

namespace App\Http\Controllers\VirtualOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserEwallet;
use App\BinaryPoints;
use App\UsersUnilevel;



class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        
    }
    public function index()
    {
        //
        $title = 'Dashboard';
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        $wallet = UserEwallet::where(['users_id' =>Crypt::decryptString(session()->get('id'))])->get();
        $sub_accounts = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
        ->groupBy(['users.id'])
        ->where(['user_network_structure.group_code' => $query[0]->group_code])->get();
        $site = 'https://gatewayexpresstnt.computology.net/'.$query[0]->id_number;

        // dashboard summary 
        $total_waiting_points_left  = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 0])->first();
        $total_waiting_points_right = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 1])->first();
        $queryA                     = DB::table('users')->where(['id_number' => $query[0]->id_number])->first();
        $income                     = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $payout_released            = DB::table('encashments')->where(['users_id' => $queryA->id])->get()->sum('payout');
        $queryUserAccount           = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $total_overall_income       = $queryUserAccount->total_overall_income + $income->total_current_income;
        $querySponsor               = DB::table('user_network_structure')->where(['users_id' => $query[0]->id])->first();
        $total_left                 = DB::table("binary_total_points")->where(['id_number' => $query[0]->id_number,'position' => 0])->get()->sum("points");
        $total_right                = DB::table("binary_total_points")->where(['id_number' => $query[0]->id_number,'position' => 1])->get()->sum("points");
        $total_flush_left           = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_left');
        $total_flush_right          = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_right');
        if($querySponsor->sponsor_id != 0000000) {
            $sponsor_row = DB::table('users')->where(['id_number' => $querySponsor->sponsor_id])->first();
        } else {
            $sponsor_row = [];
        }


        $site = 'https://booking.gatewaybackoffice.com/session/'.$query[0]->id_number;
        $unilevel = UsersUnilevel::where(['id_number' => $query[0]->id_number])->first();
        return view('VirtualOffice.dashboard',compact('title','query','wallet','sub_accounts','site','unilevel','total_waiting_points_left','total_waiting_points_right','income','payout_released','sponsor_row','total_overall_income','total_left','total_right','total_flush_left','total_flush_right'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function SecureLogin(Request $request) {
        session_start();
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required'
        ]);

        $data = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $query = User::where(['username' => $data['username']])->exists();
        if($query) {
            $row = User::where(['username' => $data['username']])->get();
            if(Crypt::decryptString($row[0]->password) == $data['password']) {
                session(['id' => Crypt::encryptString($row[0]->id)]);
                unset($_SESSION['cart']);
                return redirect('virtualoffice/dashboard');
            } else {
                return back()->with('error','Invalid username or password');
            }
        } else {
            return back()->with('error','Invalid username or password');
        }

        // $username = $request->username;
        // $password = $request->password;
        // if($username == 'wooting' && $password == 'emperor1') {
        //     session(['name' => 'wooting']);
        //     return redirect('virtualoffice/dashboard');
        // } else {
        //     return redirect('/');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
