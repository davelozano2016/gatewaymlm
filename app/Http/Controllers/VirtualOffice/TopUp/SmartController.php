<?php

namespace App\Http\Controllers\VirtualOffice\TopUp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\EcpayTopup;
use App\EWalletsLogs;
use App\UserEwallet;

use SoapClient;

class SmartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'SMART / TNT / SUN';
        $topup = EcpayTopup::where(['TelcoName' => 'SMART'])->get();
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        return view('VirtualOffice.top-up.smart',compact('title','topup','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Manila');
        $TelcoName      = $_POST['TelcoName'];
        $ExtTag         = $_POST['ExtTag'];
        $Denomination   = $_POST['Denomination'];
        $mobile_number  = $_POST['mobile_number'];
        try {
            $soapClient = new SoapClient('https://s2s.oneecpay.com/wstopupv2/WSTopUp.asmx?wsdl');

            $encrypt = $soapClient->MD5Encrypt(array(
                'str'  => '38989'.$mobile_number.$Denomination.date('mdy'),
            ));

            $Token = $encrypt->MD5EncryptResult;

            $users_id = Crypt::decryptString(session()->get('id'));

            $query = DB::table('user_ewallets')->where(['users_id' => $users_id])->first();
            $wallet = $query->fund;
            $user = DB::table('users')->where(['id' => $users_id])->first();
            $id_number = $user->id_number;
            if($wallet >= $Denomination) {

                $response = $soapClient->Transact(array(
                    'LoginInfo' => [
                        'AccountID'  => '2411',
                        'Username'   => 'mike.miranda',
                        'Password'   => 'Emperor1@#',
                        'BranchID'   => '38989'
                    ],
                    'Telco'       => $TelcoName, //‘GLOBE’ , ‘SMART’, ‘SUN’
                    'CellphoneNo' => $mobile_number, // Mobile Number to be loaded (Format – 091712345678)
                    'ExtTag'      => $ExtTag, // Based on TELCO PLAN CODE (List available when you call GetTelcoList)
                    'Amount'      => $Denomination, // Amount to be transacted
                    'Token'       => $Token, // MD5Encrypted (BranchID + CellphoneNo+ Amount + Date (Mmddyy))
                ));
    
                foreach($response->TransactResult as $result) {
                    $string[] = $result;
                }

                if($string[0] == 0) {

                    $balance = $wallet - $Denomination;

                    UserEwallet::where([
                        'users_id'  => $users_id
                    ])->update([
                        'fund'      => $balance
                    ]);
                    EWalletsLogs::create([
                        'sender_id'         => $id_number,
                        'receiver_id'       => $id_number,
                        'category'          => 'Loading & Bills Payment',
                        'description'       => $Denomination.' load has been sent to '.$mobile_number,
                        'status'            => 1, 
                        'value'             => $Denomination,
                        'reference'         => 'LBP'.$string[2],
                        'current_balance'   => $balance
                    ]);

                    echo json_encode(array('success' => true,'message' => 'payment has been successfully processed'));
                } else {
                    echo json_encode(array('success' => false,'message' => 'There\'s an error. Please contact administrator.'));
                }
            } else {
                echo json_encode(array('success' => false,'message' => 'You don\'t have enough wallet balance to continue this transaction.'));
            }



        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}