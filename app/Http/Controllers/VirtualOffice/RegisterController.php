<?php

namespace App\Http\Controllers\VirtualOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\UserEwallet;
use App\User;
use App\SocialMedia;
use App\UserNetworkStructure;
use App\RegistrationCodes;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserSocialMediaAccounts;
use App\Bonuses;
use App\UserAccount;
use App\BinaryPoints;
use App\UsersUnilevel;
use App\PackageBinaryPoints;
use App\BinaryTest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Arr;
use Throwable;
use App\UnilevelSponsorTree;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Register';
        $id = session()->get('id');
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id' => Crypt::decryptString($id)])->get();
        return view('VirtualOffice.register',compact('title','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $firstname       = $request->main_account_firstname;
        $middlename      = $request->main_account_middlename;
        $surname         = $request->main_account_surname;
        $email           = $request->main_account_email;
        $contact         = $request->main_account_contact_number;
        $activation_code = $request->main_account_activation_code;
        $sponsor_id      = $request->main_account_sponsor_id;
        $upline_id       = $request->main_account_placement_id;

      
        $position        = $request->main_account_position;
        $username        = $request->main_account_username;
        $password        = Crypt::encryptString($request->main_account_password);
        $query           = RegistrationCodes::where(['activation_code' => $activation_code, 'status' => 0, 'lock_status' => 0])->count();
        $checkUser       = BinaryPoints::where(['id_number' => $upline_id,'position' => $position])->first();

        $position_on_top = $checkUser->position_on_top;


        if($query == 1) {
            $data       = RegistrationCodes::where(['activation_code' => $activation_code])->get();
            $rc_id      = $data[0]->id;
            $type       = $data[0]->type;
            $country    = $data[0]->country_code;
            $bpv        = $data[0]->bpv;
            $id_number  = rand(1111111,9999999);
            $row        = User::where(['id_number' => $sponsor_id])->get();
            $data       = DB::table('registration_codes')->select('*')
                        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                        ->where(['registration_codes.activation_code' => $activation_code])->get();
            $user = User::create([
                'id_number'      => $id_number,
                'surname'        => $surname,
                'firstname'      => $firstname,
                'middlename'     => $middlename,
                'email'          => $email,
                'username'       => $username,
                'password'       => $password,
                'status'         => 0,
                'date_registered'=> date('Y-m-d h:i:s'),
                'month'          => date('m'),
                'remember_token' => rand(111111,999999),
            ]);

            $group_code           = Crypt::encryptString($id_number);
            $users_id             = $user->id;
            $source               = $user->id_number;
            $this->genealogy($source,$upline_id, $position, $bpv);

          
            $network              = UserNetworkStructure::create([
                'users_id'        => $users_id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => $activation_code,
                'status_type'     => $type,
                'group_code'      => $group_code,
                'account_type'    => 0,
            ]);

            if($network) {
                if($data[0]->type == 0) { // paid only
                    $direct_referal = $data[0]->direct_referal;
                } else {
                    // cd and fs 
                    $direct_referal = 0; 
                }
                $sponsor_level_id = $request->main_account_sponsor_id;
                RegistrationCodes::where(['id' => $rc_id])->update(['used_by' => $id_number,'status' => 1,'date_used' => NOW()]);
                UserEwallet::create(['users_id' => $users_id]);
                UserDetails::create(['users_id' => $users_id]);
                UserContacts::create(['users_id' => $users_id,'contact_number' => $contact]);
                UserBeneficiary::create(['users_id' => $users_id]);
                UserBankAccounts::create(['users_id' => $users_id]);
                UserAccount::create(['users_id' => $users_id]);
                UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
                DB::table('user_accounts')->where('users_id', $row[0]->id)->update(['total_current_income'=>DB::raw("total_current_income+$direct_referal")]);
                $level = UnilevelSponsorTree::where(['id_number' => $sponsor_level_id,'waiting_id_number' => $source])->count();
                if($level == 0) {
                    UnilevelSponsorTree::create(['id_number' => $sponsor_level_id,'waiting_id_number' => $source,'upline_id' => $upline_id, 'level' => 1,'points' => 0]);
                } 

                $unilevel = UnilevelSponsorTree::where(['waiting_id_number' => $sponsor_level_id])->whereNotIn('waiting_id_number',[0])->get();
                foreach($unilevel as $row) {
                    if($row->level >= 1) {
                        $lvl = $row->level + 1;
                        UnilevelSponsorTree::create(['id_number' => $row->id_number,'waiting_id_number' => $source,'upline_id' => $upline_id, 'level' => $lvl,'points' => 0]);
                    } 
                }

                Bonuses::create(['recipient' => $sponsor_id,'source' => $source,'bonus_type' => 'Direct Referral','amount' => $direct_referal, 'cycle' => 2]);
                $social= SocialMedia::all();
                foreach($social as $s) {
                    UserSocialMediaAccounts::create(['users_id' => $users_id,'social_media_id' => $s->id]);
                }
                Session::flash('message', 'New account has been added'); 
                return redirect('virtualoffice/network/genealogy-tree');
            }

        } else {
            Session::flash('message', 'Activation code already used or has been locked'); 
            return redirect('virtualoffice/network/genealogy-tree');
        }
    }

    public function create_sub_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $tophead = User::count();
        $row = User::where(['id'=>  Crypt::decryptString(session()->get('id'))])->get();
        $registration_codes = DB::table('registration_codes')->select('registration_codes.*','control_panel_packages.entries_id','control_panel_packages.id','control_panel_entries.id','control_panel_entries.entry','control_panel_entries.direct_referal','control_panel_entries.binary_point_value','countries.id','control_panel_entries.countries_id','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->where(['registration_codes.status' => 0,'registration_codes.distributor_id' => $row[0]->id_number])->get();

        $increment_username = UserNetworkStructure::where(['sponsor_id' => Crypt::decryptString($sponsor_id)])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id_number' => Crypt::decryptString($sponsor_id)])
        ->get();
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        return view('VirtualOffice.register-sub-account',compact('query','title','tophead','sponsor_id','id_number','position','registration_codes','increment','users'));
    }

    public function create_main_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $tophead = User::count();
        $row = User::where(['id'=>  Crypt::decryptString(session()->get('id'))])->get();
        $registration_codes = DB::table('registration_codes')->select('registration_codes.*','control_panel_packages.entries_id','control_panel_packages.id','control_panel_entries.id','control_panel_entries.entry','control_panel_entries.direct_referal','control_panel_entries.binary_point_value','countries.id','control_panel_entries.countries_id','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->where(['registration_codes.status' => 0,'registration_codes.distributor_id' => $row[0]->id_number])->get();

        $increment_username = UserNetworkStructure::where(['sponsor_id' => Crypt::decryptString($sponsor_id)])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id_number' => Crypt::decryptString($sponsor_id)])
        ->get();
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        return view('VirtualOffice.register-main-account',compact('query','title','tophead','sponsor_id','id_number','position','registration_codes','increment','users'));
    }

   
    private function genealogy($source,$uplineId, $position, $points) {
        $table           = 'binary_points';
        $getTreeIds      = function($uplineId, $position) use($table) {
        $level           = 0;
        $idNumber        = $uplineId;
        $currentPosition = $position;
        $ids             = [];

        $get = function($idNumber, $level, $position) use($table) {
            return DB::table($table)->where([
                ['id_number', '=', $idNumber],
                ['level', '=', $level],
                ['position', '=', $position]
            ])->first();
        };
        
        while ($row = $get($idNumber, $level, $currentPosition)) {
            $nextUplineId = $row->upline_id;
            $ids[] = $row->id;
            if ($idNumber === $nextUplineId) {
                break;
            }
            $idNumber = $nextUplineId;
            $currentPosition = $row->position_on_top;
        }
            return $ids;
        };

        
        
        DB::beginTransaction();
        try {
            $partialValues = [
            'id_number'         => $source,
            'waiting_id_number' => $source,
            'upline_id'         => $uplineId,
            'level'             => 0,
            'position_on_top'   => $position,
            'points'            => 0
        ];
        DB::table($table)->insert([
            Arr::add($partialValues, 'position', 0),
            Arr::add($partialValues, 'position', 1)
        ]);
        DB::table($table)->whereIn('id', $getTreeIds($uplineId, $position))->update(['points'=>DB::raw("points+$points")]);
        DB::commit();
        } catch(Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function passUp($tree, $level0, $points) {
        $insertRows = [];
        $insertNewRow = function($recipient, $source, $amount) {
            return [
                'recipient'  => $recipient,
                'source'     => $source,
                'bonus_type' => 'Royalty Bonus',
                'amount'     => $amount,
                'cycle'      => 2
            ];
        };



        $isDirect = function($topBranch, $subBranch) {
            return $topBranch['id_number'] === $subBranch['upline_id'];
        };

        $recur = function($level, $topHead, $parent) use(&$insertRows, $insertNewRow, $isDirect, $tree, $points, &$recur) {
            $topHeadIdNumber = $topHead['id_number'];
            $parentIdNumber = $parent['id_number'];
            $nextLevel = $level+1;
        $branch = Arr::get($tree, $level);
        if (is_null($branch)) {
            return;
        }



        $firstSecondDirects = array_values(Arr::where($branch, function($value) use($isDirect, $parent) {
            return $isDirect($parent, $value);
        }));

        $direct1 = Arr::get($firstSecondDirects, 0);
            $direct2 = Arr::get($firstSecondDirects, 1);
            $direct3 = Arr::first($branch, function($value) use($isDirect, $direct1, $direct2) {
            $isDirect1 = is_null($direct1) ? false : $isDirect($direct1, $value);
            $isDirect2 = is_null($direct2) ? false : $isDirect($direct2, $value);
            return $isDirect1 || $isDirect2;
        });
        
        if (!is_null($direct1)) {
            $insertRows[] = $insertNewRow($topHeadIdNumber, $direct1['id_number'], $points);
        }
        if (!is_null($direct2)) {
            $insertRows[] = $insertNewRow($topHeadIdNumber, $direct2['id_number'], $points);
        }
            if (is_null($direct3)) {
                if (!is_null($direct1)) {
                    $recur($nextLevel, $topHead, $direct1);
                }
                if (!is_null($direct2)) {
                    $recur($nextLevel, $topHead, $direct2);
                }
            } else {
                $recur($nextLevel, $direct3, $direct3);
            }
        };
        $recur(0, $level0, $level0);

        DB::table("bonuses")->insert($insertRows);
    }
        
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
