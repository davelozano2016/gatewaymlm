<?php

namespace App\Http\Controllers\VirtualOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use App\BinaryPoints;
use Illuminate\Support\Facades\Crypt;
use Auth;

class SponsorTreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Sponsor Tree';
        $date = Carbon::now();
        $id = session()->get('id');
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString($id)])
        ->get();

        $users = DB::table('users')->select('*')
        ->join('binary_points', 'binary_points.id_number', '=', 'users.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $query[0]->id_number])
        ->limit(1)->get();

        $level0['id_number'] = $query[0]->id_number;

        if(count($users) > 0) {
            $unilevel = DB::table('unilevel_sponsor_trees')->select('*')
            ->join('users', 'unilevel_sponsor_trees.id_number', '=', 'users.id_number')
            ->where(['unilevel_sponsor_trees.id_number' => $users[0]->id_number])
            ->get();
            $data = [];
            foreach($unilevel as $row) {
                $tmp = array();
                $tmp['id_number']         = $row->id_number;
                $tmp['waiting_id_number'] = $row->waiting_id_number;
                $tmp['upline_id']         = $row->upline_id;
                $tmp['level']             = $row->level;
                array_push($data, $tmp); 
            }
        }

        if(count($data) > 0) {
            foreach($data as $key => $value) {
                $unilevel_dynamic_levels[$value['level']][] = ['id_number' => $value['id_number'],'waiting_id_number' => $value['waiting_id_number'],'upline_id' => $value['upline_id'],'level' => $value['level']];
            }
        } else {
            $array = [];
            $unilevel_dynamic_levels = [];
        }

        $test = $this->generateHierarchyList($level0,$unilevel_dynamic_levels);

        return view('VirtualOffice.network.sponsor-tree',compact('title','date','query','users','unilevel','unilevel_dynamic_levels','test'));
    }

    public function generateHierarchyList($level0, $hierarchy, $level = "0", $filterer = null) {
        $nextLevel = strval($level+1);
        $hasNextLevel = isset($hierarchy[$nextLevel]);
            if ($level === "0"){
                return '<ul id="org" style="display:none">
                            <li><img style="cursor:pointer;" class="img-responsive" src="'.asset("assets/images/mini_logo.png").'" data-content="
                                <div class=\'popover-content-div\'>
                                <span class=\'text-center\'><h4>Gateway Express TNT</h4></span><span class=\'top-pop-span\'>
                                <span class=\'sponsor-tree-id-pop\'>'.$level0['id_number'] . '</span>
                                </span>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Joined Date: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>2021-11-20</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Personal PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Group PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '</div>" />'.
                                '<br/>
                                <span class="sponsor-tree-id">'.$level0['id_number'] . '</span>'.

                                ($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel) : "").'</li>
                        </ul>';

                        /*return '<ul id="org" style="display:none">
                            <li>$level0['id_number'] . ($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel) : "") . '</li>
                        </ul>';*/
            }
        
        $list = $hierarchy[$level];
        $result = "";
        
        $processedIndexes = [];
            
            foreach ($list as $index=>$element) {
                if ((!$filterer || $filterer($element)) && !in_array($index, $processedIndexes)) {
                //     $result .= "<li>" . $element['waiting_id_number'] . ($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel, function($sub) use($element) {
                //     return $sub["upline_id"] === $element["waiting_id_number"];
                // }) : "") . "</li>";
                    $result .= '<li><img style="cursor:pointer;" class="img-responsive" src="'.asset("assets/images/mini_logo.png").'" data-content="
                                <div class=\'popover-content-div\'>
                                <span class=\'text-center\'><h4>Gateway Express TNT</h4></span><span class=\'top-pop-span\'>
                                <span class=\'sponsor-tree-id-pop\'>'. $element['waiting_id_number'] . '</span>
                                </span>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Joined Date: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>2021-11-20</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Personal PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Group PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '</div>" />'.
                                '<br/>
                                <span class="sponsor-tree-id">'. $element['waiting_id_number'] . '</span>'.($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel, function($sub) use($element) {
                                    return $sub["upline_id"] === $element["waiting_id_number"];
                                }) : "").'</li>';
            
                if ($level !== "1") {
                    for ($i = $index+1; $i < count($list); $i++) {
                        $nextElement = $list[$i];
                            if ($element['waiting_id_number'] === $nextElement['upline_id']) {
                            $processedIndexes[] = $i;
                            // $result .= "<li>" . $nextElement['waiting_id_number'] . ($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel, function($sub) use($nextElement) {
                            //     return $sub["upline_id"] === $nextElement["waiting_id_number"];
                            // }) : "") . "</li>";

                            $result .= '<li><img style="cursor:pointer;" class="img-responsive" src="'.asset("assets/images/mini_logo.png").'" data-content="
                                <div class=\'popover-content-div\'>
                                <span class=\'text-center\'><h4>Gateway Express TNT</h4></span><span class=\'top-pop-span\'>
                                <span class=\'sponsor-tree-id-pop\'>'. $nextElement['waiting_id_number'] . '</span>
                                </span>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Joined Date: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>2021-11-20</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Team Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Left: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Waiting Right: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Personal PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '<div class=\'row\'>
                                    <div class=\'col-lg-6 col-sm-6\'>Group PV: </div>
                                    <div class=\'col-lg-6 col-sm-6\'>0</div>
                                </div>'.
                                '</div>" />'.
                                '<br/>
                                <span class="sponsor-tree-id">'. $nextElement['waiting_id_number'] .'</span>'.($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel, function($sub) use($nextElement) {
                                        return $sub["upline_id"] === $nextElement["waiting_id_number"];
                                    }) : "").'</li>';



                            /*$result .= "<li>" . $nextElement['waiting_id_number'] . ($hasNextLevel ? self::generateHierarchyList($level0, $hierarchy, $nextLevel, function($sub) use($nextElement) {
                                return $sub["upline_id"] === $nextElement["waiting_id_number"];
                            }) : "") . "</li>";*/
                        }
                    }
                }
            }
        }

        //echo $result; exit;
        return empty($result) ? $result : "<ul>$result</ul>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
