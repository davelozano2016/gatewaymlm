<?php

namespace App\Http\Controllers\VirtualOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ControlPanelUnilevel;
use App\BinaryPoints;
use App\ControlPanelUnilevelSettings;
use App\UsersUnilevel;
use App\ProductPinCodes;
session_start();
class UnilevelProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Unilevel';
        $id = session()->get('id');
        $query = User::where(['id' => Crypt::decryptString($id)])->get();

        $users = DB::table('users')->select('*')
        ->join('binary_points', 'binary_points.id_number', '=', 'users.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $query[0]->id_number])
        ->limit(1)->get();

        $settings = ControlPanelUnilevelSettings::first();
        if(count($users) > 0) {
            $unilevel = DB::table('binary_points')->select('*')
            ->join('users', 'binary_points.id_number', '=', 'users.id_number')
            ->whereNotIn('binary_points.level',[0])
            ->where(['binary_points.id_number' => $users[0]->id_number])
            ->get();

            if(count($unilevel) > 0) {
                foreach($unilevel as $level) {
                    $row = User::where(['id_number' => $level->waiting_id_number])->first();
                    $UsersUnilevel[$level->waiting_id_number] = UsersUnilevel::where(['id_number' => $level->waiting_id_number])->first();
                    $unilevel_dynamic_levels[$level->waiting_id_number][$level->level] = $UsersUnilevel[$level->waiting_id_number]->unilevel_income;   
                    
                }

                    // foreach($unilevel_dynamic_levels as $key => $id) {
                    //     foreach($id as $value) {
                    //         echo $value.'<br>';
                    //     }
                    // }

            } else {
                $unilevel_dynamic_levels = [];
                $unilevel = [];
                $UsersUnilevel = [];
            }

            // dd($unilevel_dynamic_levels);

            
        } else {
            $unilevel = [];
            $unilevel_dynamic_levels = [];
            $UsersUnilevel = [];
        }

        return view('VirtualOffice.network.unilevel-product',compact('title','query','unilevel','UsersUnilevel','users','unilevel_dynamic_levels','settings'));
    }

    public function unilevel(Request $request) {
        
        $id         = session()->get('id');
        $query      = User::where(['id' => Crypt::decryptString($id)])->first();
        $id_number  = $query->id_number;
        $position   = $request->position;
        $upv        = 0;
        $bpv        = 0;
        foreach($_SESSION['internal_cart'] as $id) {
            $products = ProductPinCodes::where(['id' => $id])->get();
            foreach($products as $product) {
                $upv += $product->unilevel_point_value;
                $bpv += $product->binary_point_value;
            }

            $array = DB::table('binary_points')->select('id_number')->where('waiting_id_number','=',$id_number)->where('level','!=',0)->get();
            $this->unilevel_income($id_number,$array,$upv);
            $this->unilevel_products($id_number,$array,$upv);
            $this->genealogy_products($id_number, $position, $bpv);
            $pins = ProductPinCodes::where(['id' => $id])->update(['used_by' => $id_number,'status' => 1, 'date_used' => NOW()]);
        }

        unset($_SESSION['internal_cart']);
        return redirect('virtualoffice/code-management/my-product/product-pins')->withInput(['message'=> 'Product has been encoded.']);
        

    }

    private function unilevel_products($uplineId,$array,$points) {
        $table = 'users_unilevel';
        DB::table($table)->where(['id_number' => $uplineId])->update(['unilevel_point_value'=>DB::raw("unilevel_point_value+$points")]);
        DB::commit();
        // foreach($array as $key => $data) {
        //     foreach($data as $k) {
        //         $query = DB::table($table)->select('id')->where(['id_number' => $k])->first();
        //         $id = $query->id;
        //         DB::beginTransaction();
        //         try {
        //             DB::table($table)->where(['id' => $id])->update(['unilevel_point_value'=>DB::raw("unilevel_point_value+$points")]);
        //             DB::commit();
        //         } catch(Throwable $e) {
        //             DB::rollback();
        //             throw $e;
        //         }
        //     }
        // }
    }

    private function unilevel_income($uplineId,$array,$points) {
        $table = 'users_unilevel';

        foreach($array as $row) {
            $query         = DB::table('binary_points')->select('level')->where(['id_number' => $row->id_number,'waiting_id_number' => $uplineId])->first();
            $data          = DB::table($table)->select('id')->where(['id_number' => $row->id_number])->first();
            $unilevelQuery = DB::table('control_panel_unilevels')->select('percentage')->where(['level' => $query->level])->first();
            $percentage    = $unilevelQuery->percentage / 100;
            $converted     = $points * $percentage;
            DB::beginTransaction();
            try {
                DB::table($table)->where(['id' => $data->id])->update(['unilevel_income'=>DB::raw("unilevel_income+$converted")]);
                DB::commit();
            } catch(Throwable $e) {
                DB::rollback();
                throw $e;
            }

        }
    }

    private function genealogy_products($uplineId, $position, $points) {
        $table           = 'binary_points';
        $getTreeIds      = function($uplineId, $position) use($table) {
        $level           = 0;
        $idNumber        = $uplineId;
        $currentPosition = $position;
        $ids             = [];
        
        $get = function($idNumber, $level, $position) use($table) {
            return DB::table($table)->where([
                ['id_number', '=', $idNumber],
                ['level', '=', $level],
                ['position', '=', $position]
            ])->first();
        };
        
        while ($row = $get($idNumber, $level, $currentPosition)) {
            $nextUplineId = $row->upline_id;
            $ids[] = $row->id;
            if ($idNumber === $nextUplineId) {
                break;
            }
            $idNumber = $nextUplineId;
            $currentPosition = $row->position_on_top;
        }
            return $ids;
        };
        
        DB::beginTransaction();
        try {
            DB::table($table)->whereIn('id', $getTreeIds($uplineId, $position))->update(['points'=>DB::raw("points+$points")]);
            DB::commit();
        } catch(Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
