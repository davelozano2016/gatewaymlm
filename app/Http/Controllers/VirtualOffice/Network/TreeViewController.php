<?php

namespace App\Http\Controllers\VirtualOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
class TreeViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Tree View';
        $id = session()->get('id');
        $query = User::where(['id' => Crypt::decryptString($id)])->get();

        $users = DB::table('users')->select('*')
        ->join('binary_points', 'binary_points.id_number', '=', 'users.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $query[0]->id_number])
        ->limit(1)->get();

        

        if(count($users) > 0) {
            $unilevel = DB::table('binary_points')->select('*')
            ->join('users', 'binary_points.id_number', '=', 'users.id_number')
            ->whereNotIn('binary_points.level',[0])
            ->where(['binary_points.id_number' => $users[0]->id_number])
            ->get();

            foreach($unilevel as $row) {
                $tmp = array();
                $tmp['id_number']         = $row['id_number'];
                $tmp['waiting_id_number'] = $row['waiting_id_number'];
                $tmp['upline_id']         = $row['upline_id'];
                $tmp['level']             = $row['level'];
                array_push($data, $tmp); 
            }

            if(count($data) > 0) {
                foreach($data as $key => $value) {
                    $unilevel_dynamic_levels[$value['level']][] = ['id_number' => $value['id_number'],'waiting_id_number' => $value['waiting_id_number'],'upline_id' => $value['upline_id'],'level' => $value['level']];
                    
                }
            } else {
                $array = [];
            }

            
        } 
        
        return view('VirtualOffice.network.tree-view',compact('title','query','unilevel','unilevel_dynamic_levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
