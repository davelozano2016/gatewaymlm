<?php

namespace App\Http\Controllers\VirtualOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ControlPanelUnilevel;
use App\BinaryPoints;
use App\ControlPanelUnilevelSettings;
use App\UsersUnilevel;
use App\ProductPinCodes;
session_start();
class UnilevelProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Unilevel';
        $id = session()->get('id');
        $query = User::where(['id' => Crypt::decryptString($id)])->get();

        $users = DB::table('users')->select('*')
        ->join('binary_points', 'binary_points.id_number', '=', 'users.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $query[0]->id_number])
        ->limit(1)->get();

        $settings = ControlPanelUnilevelSettings::first();
        if(count($users) > 0) {
            $unilevel = DB::table('binary_points')->select('*')
            ->join('users', 'binary_points.id_number', '=', 'users.id_number')
            ->whereNotIn('binary_points.level',[0])
            ->where(['binary_points.id_number' => $users[0]->id_number])
            ->get();

            if(count($unilevel) > 0) {
                foreach($unilevel as $level) {
                    $row = User::where(['id_number' => $level->waiting_id_number])->first();
                    $username = $row->username;
                    $UsersUnilevel[$level->waiting_id_number] = UsersUnilevel::where(['id_number' => $level->waiting_id_number])->first();
                    $unilevel_dynamic_levels[$level->waiting_id_number] = $username;
                }
            } else {
                $unilevel_dynamic_levels = [];
                $unilevel = [];
            }

            
        } else {
            $unilevel = [];
            $unilevel_dynamic_levels = [];
        }

        return view('VirtualOffice.network.unilevel-product',compact('title','query','unilevel','UsersUnilevel','users','unilevel_dynamic_levels','settings'));
    }

    public function unilevel(Request $request) {
        
        $id         = session()->get('id');
        $query      = User::where(['id' => Crypt::decryptString($id)])->first();
        $id_number  = $query->id_number;
        $position   = $request->position;
        $upv        = 0;
        $bpv        = 0;
        foreach($_SESSION['internal_cart'] as $id) {
            $products = ProductPinCodes::where(['id' => $id])->get();
            foreach($products as $product) {
                $upv += $product->unilevel_point_value;
                $bpv += $product->binary_point_value;
            }
            $pins = ProductPinCodes::where(['id' => $id])->update(['used_by' => $id_number,'status' => 1, 'date_used' => NOW()]);
        }

        if($pins) {
            $binary = BinaryPoints::where(['waiting_id_number' => $id_number,'position' => $position])->get();
            foreach($binary as $row) {
                $get_level = BinaryPoints::where(['id_number' => $row->id_number,'waiting_id_number' => $id_number,'position' => $position])->get();
                foreach($get_level as $lvl) {
                    $unilevel       = ControlPanelUnilevel::where(['level' => $lvl->level])->first();
                    $percentage     = $unilevel->percentage;
                    $income         = ($percentage / 100) * $upv; 
                }

                $unilevelQuery  = UsersUnilevel::where(['id_number' => $row->id_number])->first();
                
                $totalUPV       = $unilevelQuery->unilevel_point_value + $upv;
                $totalIncome    = $unilevelQuery->unilevel_income + $income;
                $totalBPV       = $row->points + $bpv;

                
                BinaryPoints::where(['id_number' => $row->id_number,'waiting_id_number' => $row->waiting_id_number,'position' => $position])->update(['points' => $totalBPV]);
                UsersUnilevel::where(['id_number' => $row->id_number])->update(['unilevel_point_value' => $totalUPV, 'unilevel_income' => $totalIncome]);


            }
        }
        unset($_SESSION['internal_cart']);
        return redirect('virtualoffice/code-management/my-product/product-pins')->withInput(['message'=> 'Product has been encoded.']);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
