<?php

namespace App\Http\Controllers\VirtualOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Crypt;

class GenealogyTreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $title      = 'Genealogy Tree';
        $date       = Carbon::now();
        $id         = session()->get('id');
        $query      = User::where(['id' => Crypt::decryptString($id)])->get();
        $id_number  = $query[0]->id_number;
        $name       = $query[0]->firstname.' '.$query[0]->surname;
        // dd($query);
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $query[0]->id_number])
        ->limit(1)->get();

        // Left Panel
        if(count($users) > 0) {
            $upline_left_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')  
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 0,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
            
            
            $registered_downlines = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.upline_id' => $users[0]->id_number])
            ->get();

            
    
            if(count($upline_left_level_1) > 0) {
                $upline_left_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_left_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_left_level_2_left = [];
                $upline_left_level_2_right = [];
            }

            if(count($upline_left_level_2_left) > 0) {
                $upline_left_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_A_Query) > 0) {
                    $upline_left_level_3_left_A = $upline_left_level_3_left_A_Query;
                } else {
                    $upline_left_level_3_left_A = [];
                }

                $upline_left_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_B_Query) > 0) {
                    $upline_left_level_3_left_B = $upline_left_level_3_left_B_Query;
                } else {
                    $upline_left_level_3_left_B = [];
                }

            } else {
                $upline_left_level_3_left_A = [];
                $upline_left_level_3_left_B = [];
            }

            if(count($upline_left_level_2_right) > 0) {
                $upline_left_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_A_Query) > 0) {
                    $upline_left_level_3_right_A = $upline_left_level_3_right_A_Query;
                } else {
                    $upline_left_level_3_right_A = [];
                }

                $upline_left_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_B_Query) > 0) {
                    $upline_left_level_3_right_B = $upline_left_level_3_right_B_Query;
                } else {
                    $upline_left_level_3_right_B = [];
                }

            } else {
                $upline_left_level_3_right_A = [];
                $upline_left_level_3_right_B = [];
            }
        } else{
            $upline_left_level_1 = [];
            $upline_left_level_2_left = [];
            $upline_left_level_2_right = [];
            $upline_left_level_3_left_A = [];
            $upline_left_level_3_left_B = [];
            $upline_left_level_3_right_A = [];
            $upline_left_level_3_right_B = [];
            $registered_downlines = [];
        }

        // Right Panel
        if(count($users) > 0) {
            $upline_right_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 1,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
    
            if(count($upline_right_level_1) > 0) {
                $upline_right_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_right_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_right_level_2_left = [];
                $upline_right_level_2_right = [];
            }

            if(count($upline_right_level_2_left) > 0) {
                $upline_right_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_A_Query) > 0) {
                    $upline_right_level_3_left_A = $upline_right_level_3_left_A_Query;
                } else {
                    $upline_right_level_3_left_A = [];
                }

                $upline_right_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_B_Query) > 0) {
                    $upline_right_level_3_left_B = $upline_right_level_3_left_B_Query;
                } else {
                    $upline_right_level_3_left_B = [];
                }

            } else {
                $upline_right_level_3_left_A = [];
                $upline_right_level_3_left_B = [];
            }

            if(count($upline_right_level_2_right) > 0) {
                $upline_right_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_A_Query) > 0) {
                    $upline_right_level_3_right_A = $upline_right_level_3_right_A_Query;
                } else {
                    $upline_right_level_3_right_A = [];
                }

                $upline_right_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_B_Query) > 0) {
                    $upline_right_level_3_right_B = $upline_right_level_3_right_B_Query;
                } else {
                    $upline_right_level_3_right_B = [];
                }

            } else {
                $upline_right_level_3_right_A = [];
                $upline_right_level_3_right_B = [];
            }
        } else{
            $upline_right_level_1 = [];
            $upline_right_level_2_left = [];
            $upline_right_level_2_right = [];
            $upline_right_level_3_left_A = [];
            $upline_right_level_3_left_B = [];
            $upline_right_level_3_right_A = [];
            $upline_right_level_3_right_B = [];
            $registered_downlines = [];
        }

        $id = session()->get('id');
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString($id)])->get();

        $total_waiting_points_left  = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 0])->first();
        $total_waiting_points_right = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 1])->first();
        $queryA                     = DB::table('users')->where(['id_number' => $query[0]->id_number])->first();
        $income                     = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $payout_released            = DB::table('encashments')->where(['users_id' => $queryA->id])->get()->sum('payout');
        $queryUserAccount           = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $total_overall_income       = $queryUserAccount->total_overall_income + $income->total_current_income;
        $querySponsor               = DB::table('user_network_structure')->where(['users_id' => $query[0]->id])->first();
        $total_left                 = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 0])->get()->sum("points");
        $total_right                = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 1])->get()->sum("points");
        $total_flush_left           = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_left');
        $total_flush_right          = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_right');

        return view('VirtualOffice.network.genealogy-tree',compact('title','id_number','name','date','users','upline_left_level_1','upline_left_level_2_left','upline_left_level_2_right','upline_left_level_3_left_A','upline_left_level_3_left_B','upline_left_level_3_right_A','upline_left_level_3_right_B','upline_right_level_1','upline_right_level_2_left','upline_right_level_2_right','upline_right_level_3_left_A','upline_right_level_3_left_B','upline_right_level_3_right_A','upline_right_level_3_right_B','registered_downlines','query','total_waiting_points_left','total_waiting_points_right','total_left','total_right','total_flush_left','total_flush_right'));
    }

    




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function result(Request $request)
    {
        //
        
        $distributor_id = $request->distributor_id;
        $title          = 'Genealogy Tree';
        $date           = Carbon::now();
       
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $distributor_id])
        ->limit(1)->get();
        
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $id_number      = $query[0]->id_number;
        $name           = $query[0]->firstname.' '.$query[0]->surname;
        // Left Panel
        if(count($users) > 0) {
            $upline_left_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 0,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
    
            if(count($upline_left_level_1) > 0) {
                $upline_left_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_left_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_left_level_2_left = [];
                $upline_left_level_2_right = [];
            }

            if(count($upline_left_level_2_left) > 0) {
                $upline_left_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_A_Query) > 0) {
                    $upline_left_level_3_left_A = $upline_left_level_3_left_A_Query;
                } else {
                    $upline_left_level_3_left_A = [];
                }

                $upline_left_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_B_Query) > 0) {
                    $upline_left_level_3_left_B = $upline_left_level_3_left_B_Query;
                } else {
                    $upline_left_level_3_left_B = [];
                }

            } else {
                $upline_left_level_3_left_A = [];
                $upline_left_level_3_left_B = [];
            }

            if(count($upline_left_level_2_right) > 0) {
                $upline_left_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_A_Query) > 0) {
                    $upline_left_level_3_right_A = $upline_left_level_3_right_A_Query;
                } else {
                    $upline_left_level_3_right_A = [];
                }

                $upline_left_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_B_Query) > 0) {
                    $upline_left_level_3_right_B = $upline_left_level_3_right_B_Query;
                } else {
                    $upline_left_level_3_right_B = [];
                }

            } else {
                $upline_left_level_3_right_A = [];
                $upline_left_level_3_right_B = [];
            }
        } else{
            $upline_left_level_1 = [];
            $upline_left_level_2_left = [];
            $upline_left_level_2_right = [];
            $upline_left_level_3_left_A = [];
            $upline_left_level_3_left_B = [];
            $upline_left_level_3_right_A = [];
            $upline_left_level_3_right_B = [];
        }

        // Right Panel
        if(count($users) > 0) {
            $upline_right_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 1,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
    
            if(count($upline_right_level_1) > 0) {
                $upline_right_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_right_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_right_level_2_left = [];
                $upline_right_level_2_right = [];
            }

            if(count($upline_right_level_2_left) > 0) {
                $upline_right_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_A_Query) > 0) {
                    $upline_right_level_3_left_A = $upline_right_level_3_left_A_Query;
                } else {
                    $upline_right_level_3_left_A = [];
                }

                $upline_right_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_B_Query) > 0) {
                    $upline_right_level_3_left_B = $upline_right_level_3_left_B_Query;
                } else {
                    $upline_right_level_3_left_B = [];
                }

            } else {
                $upline_right_level_3_left_A = [];
                $upline_right_level_3_left_B = [];
            }

            if(count($upline_right_level_2_right) > 0) {
                $upline_right_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_A_Query) > 0) {
                    $upline_right_level_3_right_A = $upline_right_level_3_right_A_Query;
                } else {
                    $upline_right_level_3_right_A = [];
                }

                $upline_right_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_B_Query) > 0) {
                    $upline_right_level_3_right_B = $upline_right_level_3_right_B_Query;
                } else {
                    $upline_right_level_3_right_B = [];
                }

            } else {
                $upline_right_level_3_right_A = [];
                $upline_right_level_3_right_B = [];
            }
        } else{
            $upline_right_level_1 = [];
            $upline_right_level_2_left = [];
            $upline_right_level_2_right = [];
            $upline_right_level_3_left_A = [];
            $upline_right_level_3_left_B = [];
            $upline_right_level_3_right_A = [];
            $upline_right_level_3_right_B = [];
        }

        $total_waiting_points_left  = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 0])->first();
        $total_waiting_points_right = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 1])->first();
        $queryA                     = DB::table('users')->where(['id_number' => $query[0]->id_number])->first();
        $income                     = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $payout_released            = DB::table('encashments')->where(['users_id' => $queryA->id])->get()->sum('payout');
        $queryUserAccount           = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $total_overall_income       = $queryUserAccount->total_overall_income + $income->total_current_income;
        $querySponsor               = DB::table('user_network_structure')->where(['users_id' => $query[0]->id])->first();
        $total_left                 = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 0])->get()->sum("points");
        $total_right                = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 1])->get()->sum("points");
        $total_flush_left           = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_left');
        $total_flush_right          = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_right');

        return view('VirtualOffice.network.genealogy-tree',compact('query','id_number','name','title','date','users','upline_left_level_1','upline_left_level_2_left','upline_left_level_2_right','upline_left_level_3_left_A','upline_left_level_3_left_B','upline_left_level_3_right_A','upline_left_level_3_right_B','upline_right_level_1','upline_right_level_2_left','upline_right_level_2_right','upline_right_level_3_left_A','upline_right_level_3_left_B','upline_right_level_3_right_A','upline_right_level_3_right_B','distributor_id','total_waiting_points_left','total_waiting_points_right','total_left','total_right','total_flush_left','total_flush_right'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visit($id)
    {
        //
        $distributor_id = Crypt::decryptString($id);
        $title          = 'Genealogy Tree';
        $date           = Carbon::now();
       

        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();

        $id_number      = $query[0]->id_number;
        $name           = $query[0]->firstname.' '.$query[0]->surname;

        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['users.id_number' => $distributor_id])
        ->limit(1)->get();

        // Left Panel
        if(count($users) > 0) {
            $upline_left_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 0,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
    
            if(count($upline_left_level_1) > 0) {
                $upline_left_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_left_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_left_level_2_left = [];
                $upline_left_level_2_right = [];
            }

            if(count($upline_left_level_2_left) > 0) {
                $upline_left_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_A_Query) > 0) {
                    $upline_left_level_3_left_A = $upline_left_level_3_left_A_Query;
                } else {
                    $upline_left_level_3_left_A = [];
                }

                $upline_left_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_left_B_Query) > 0) {
                    $upline_left_level_3_left_B = $upline_left_level_3_left_B_Query;
                } else {
                    $upline_left_level_3_left_B = [];
                }

            } else {
                $upline_left_level_3_left_A = [];
                $upline_left_level_3_left_B = [];
            }

            if(count($upline_left_level_2_right) > 0) {
                $upline_left_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_A_Query) > 0) {
                    $upline_left_level_3_right_A = $upline_left_level_3_right_A_Query;
                } else {
                    $upline_left_level_3_right_A = [];
                }

                $upline_left_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_left_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_left_level_3_right_B_Query) > 0) {
                    $upline_left_level_3_right_B = $upline_left_level_3_right_B_Query;
                } else {
                    $upline_left_level_3_right_B = [];
                }

            } else {
                $upline_left_level_3_right_A = [];
                $upline_left_level_3_right_B = [];
            }
        } else{
            $upline_left_level_1 = [];
            $upline_left_level_2_left = [];
            $upline_left_level_2_right = [];
            $upline_left_level_3_left_A = [];
            $upline_left_level_3_left_B = [];
            $upline_left_level_3_right_A = [];
            $upline_left_level_3_right_B = [];
        }

        // Right Panel
        if(count($users) > 0) {
            $upline_right_level_1 = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
            ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
            ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
            ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
            ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
            ->where(['user_network_structure.position' => 1,'upline_id' => $users[0]->id_number])->limit(1)
            ->get();
    
            if(count($upline_right_level_1) > 0) {
                $upline_right_level_2_left = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();

                $upline_right_level_2_right = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_1[0]->id_number])->limit(1)
                ->get();
            } else{
                $upline_right_level_2_left = [];
                $upline_right_level_2_right = [];
            }

            if(count($upline_right_level_2_left) > 0) {
                $upline_right_level_3_left_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_A_Query) > 0) {
                    $upline_right_level_3_left_A = $upline_right_level_3_left_A_Query;
                } else {
                    $upline_right_level_3_left_A = [];
                }

                $upline_right_level_3_left_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_left[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_left_B_Query) > 0) {
                    $upline_right_level_3_left_B = $upline_right_level_3_left_B_Query;
                } else {
                    $upline_right_level_3_left_B = [];
                }

            } else {
                $upline_right_level_3_left_A = [];
                $upline_right_level_3_left_B = [];
            }

            if(count($upline_right_level_2_right) > 0) {
                $upline_right_level_3_right_A_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 0,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_A_Query) > 0) {
                    $upline_right_level_3_right_A = $upline_right_level_3_right_A_Query;
                } else {
                    $upline_right_level_3_right_A = [];
                }

                $upline_right_level_3_right_B_Query = DB::table('users')->select('user_network_structure.*','users.id_number','users.firstname','users.surname','registration_codes.packages_id','control_panel_packages.entries_id','control_panel_entries.id','control_panel_entries.entry','users.username','users.date_registered')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->where(['user_network_structure.position' => 1,'upline_id' => $upline_right_level_2_right[0]->id_number])->limit(1)
                ->get();
                if(count($upline_right_level_3_right_B_Query) > 0) {
                    $upline_right_level_3_right_B = $upline_right_level_3_right_B_Query;
                } else {
                    $upline_right_level_3_right_B = [];
                }

            } else {
                $upline_right_level_3_right_A = [];
                $upline_right_level_3_right_B = [];
            }
        } else{
            $upline_right_level_1 = [];
            $upline_right_level_2_left = [];
            $upline_right_level_2_right = [];
            $upline_right_level_3_left_A = [];
            $upline_right_level_3_left_B = [];
            $upline_right_level_3_right_A = [];
            $upline_right_level_3_right_B = [];
        }

        $total_waiting_points_left  = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 0])->first();
        $total_waiting_points_right = DB::table('binary_points')->where(['id_number' => $query[0]->id_number,'position' => 1])->first();
        $queryA                     = DB::table('users')->where(['id_number' => $query[0]->id_number])->first();
        $income                     = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $payout_released            = DB::table('encashments')->where(['users_id' => $queryA->id])->get()->sum('payout');
        $queryUserAccount           = DB::table('user_accounts')->where(['users_id' => $queryA->id])->first();
        $total_overall_income       = $queryUserAccount->total_overall_income + $income->total_current_income;
        $querySponsor               = DB::table('user_network_structure')->where(['users_id' => $query[0]->id])->first();
        $total_left                 = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 0])->get()->sum("points");
        $total_right                = DB::table("binary_pairings")->where(['id_number' => $query[0]->id_number,'position' => 1])->get()->sum("points");
        $total_flush_left           = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_left');
        $total_flush_right          = DB::table('binary_flushes')->where(['id_number' => $query[0]->id_number])->get()->sum('flush_right');

        return view('VirtualOffice.network.genealogy-tree',compact('title','id_number','name','date','users','upline_left_level_1','upline_left_level_2_left','upline_left_level_2_right','upline_left_level_3_left_A','upline_left_level_3_left_B','upline_left_level_3_right_A','upline_left_level_3_right_B','upline_right_level_1','upline_right_level_2_left','upline_right_level_2_right','upline_right_level_3_left_A','upline_right_level_3_left_B','upline_right_level_3_right_A','upline_right_level_3_right_B','distributor_id','query','total_waiting_points_left','total_waiting_points_right','total_left','total_right','total_flush_left','total_flush_right'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function processing(Request $request)
    {
        //

        $id_number   = Crypt::encryptString($request->distributor_id_number);
        $position    = Crypt::encryptString($request->distributor_position);
        $sponsor_id  = Crypt::encryptString($request->distributor_sponsor_id);

        if(isset($_POST['btn_new_account'])) {
            return redirect('virtualoffice/register/main-account/'.$sponsor_id.'/'.$id_number.'/'.$position);
        } else {
            return redirect('virtualoffice/register/sub-account/'.$sponsor_id.'/'.$id_number.'/'.$position);
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

