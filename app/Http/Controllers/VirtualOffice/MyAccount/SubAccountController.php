<?php

namespace App\Http\Controllers\VirtualOffice\MyAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserEwallet;
use App\UserNetworkStructure;
use App\EWalletsLogs;
use App\UserAccount;
use App\IncomeToEwalletConversion;
use Session;
class SubAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Transfer to  E-Wallet';
        $query = DB::table('users')->select('*','countries.id as c_id')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();


        $sub_accounts = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
        ->join('user_accounts', 'user_accounts.users_id', '=', 'users.id')
        ->groupBy(['users.id'])
        ->where(['user_network_structure.group_code' => $query[0]->group_code])->get();

        $transfer_sub = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['user_network_structure.account_type' => 1,'user_network_structure.group_code' => $query[0]->group_code])->get();

        // $validate = UserNetworkStructure::where(['group_code' => $query[0]->group_code,'account_type' => 0])->get();
        $wallet_balance = UserEwallet::where(['users_id' => Crypt::decryptString(session()->get('id'))])->get();

        $controls = DB::table('encashment_controls')->select('*')->where(['countries_id' => $query[0]->c_id])->first();

        $encashment = DB::table('control_panel_encashments')->first();

        $isCD = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['user_network_structure.group_code' => $query[0]->group_code])->get();

        foreach($isCD as $k) {
            $cd = $k->status_type;
            if($cd == 1) {
                $commission_deduction[$k->id_number] = $encashment->commission_deduction;
            }  else {
                $commission_deduction[$k->id_number] = 0;
            }
        }
        return view('VirtualOffice.my-account.sub-account',compact('title','query','sub_accounts','transfer_sub','wallet_balance','controls','commission_deduction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function transfer_wallet(Request $request) {
        $sender_id          = Crypt::decryptString(session()->get('id'));
        $amount             = $request->amount;
        $wallet_balance     = $request->wallet_balance;
        $users_id           = Crypt::decryptString($request->users_id);
        $user_wallet_fund   = $request->user_wallet_fund;
        $senderQuery        = User::where(['id' => $sender_id])->first();
        $permitted_chars    = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $reference          = substr(str_shuffle($permitted_chars), 0, 7);
        if($wallet_balance < $amount) {
            Session::flash('message', 'Insufficient balance.'); 
        } else {
            $updateQuery    = UserEwallet::where(['users_id' => $users_id])->first();
            $query          = UserEwallet::where(['users_id' => $users_id])->update(['fund' => $user_wallet_fund + $amount]);
            $user           = User::where(['id' => $users_id])->first();
            if($query) {
                $balance    = $wallet_balance - $amount;
                $balance1   = $updateQuery->fund + $amount;
                UserEwallet::where(['users_id' => $sender_id])->update(['fund' => $balance]);
                EWalletsLogs::create(['sender_id' => $senderQuery->id_number,'receiver_id' => $user->id_number,'category' => 'E-Wallet Transfer','description' => 'E-Wallet fund transfer to '.$user->id_number,'status' => 1, 'value' => $amount,'reference' => 'WT'.$reference,'current_balance' => $balance]);
                EWalletsLogs::create(['sender_id' => $user->id_number,'receiver_id' => $senderQuery->id_number,'category' => 'E-Wallet Transfer','description' => 'Received E-Wallet fund from '.$senderQuery->id_number,'status' => 0, 'value' => $amount,'reference' => 'WT'.$reference,'current_balance' => $balance1]);
                Session::flash('message', 'You have transferred '.number_format($amount,2).' e-wallet fund to '.$user->username); 
            }
        }
        return redirect('virtualoffice/my-account/sub-account');
    }

    public function income_to_wallet(Request $request) {
        $sender_id          = Crypt::decryptString(session()->get('id'));
        $amount             = $request->income_to_convert;
        $maintenance        = $request->maintenance;
        $tax                = $request->tax;
        $processing_fee     = $request->processing_fee;
        $income_original    = $request->income_original;
        $users_id           = Crypt::decryptString($request->users_id);
        $user_wallet_fund   = $request->user_wallet_fund;
        $senderQuery        = User::where(['id' => $sender_id])->first();
        $permitted_chars    = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $reference          = substr(str_shuffle($permitted_chars), 0, 7);
        $updateQuery        = UserEwallet::where(['users_id' => $users_id])->first();
        $query              = UserEwallet::where(['users_id' => $users_id])->update(['fund' => $user_wallet_fund + $amount]);
        $user               = User::where(['id' => $users_id])->first();
        if($query) {    
            $balance        = $updateQuery->fund + $amount;
            UserEwallet::where(['users_id' => $sender_id])->update(['fund' => $balance]);
            EWalletsLogs::create(['sender_id' => $senderQuery->id_number,'receiver_id' => $user->id_number,'category' => 'Income To E-Wallet Conversion','description' => 'Current income convert to E-Wallet fund.','status' => 0, 'value' => $amount,'reference' => 'IWC'.$reference,'current_balance' => $balance]);
            IncomeToEwalletConversion::create(['users_id' => $sender_id,'request_bonus' => $income_original,'tax' => $tax,'maintenance' => $maintenance,'processing_fee' => $processing_fee,'payout' => $amount]);
            DB::table('user_accounts')->where('users_id', $users_id )->update(['total_current_income' => 0,'total_overall_income'=>DB::raw("total_overall_income+$income_original")]);
            Session::flash('message', 'You have converted your current income '.number_format($amount,2).' to E-Wallet fund'); 
        }
        return redirect('virtualoffice/my-account/convert-current-income');
    }

    public function login_sub_account($id) {
        session_start();
        $id_number = Crypt::decryptString($id);
        $query = User::where(['id_number' => $id_number])->exists();
        if($query) {
            $row = User::where(['id_number' => $id_number])->get();
            session(['id' => Crypt::encryptString($row[0]->id)]);
            Session::flash('message', 'Switched account to '.$id_number); 
            unset($_SESSION['cart']);
            return redirect('virtualoffice/dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
