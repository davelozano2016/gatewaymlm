<?php

namespace App\Http\Controllers\VirtualOffice\MyAccount;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserNetworkStructure;
use App\Country;
use App\SocialMedia;
use App\Banks;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserSocialMediaAccounts;
use App\EWallet;
use App\EWalletsLogs;
use App\RegistrationCodes;
use App\Ranks;
use Response;
use Session;



class AccountsController extends Controller
{
    //

    public function index()
    {
        //
        $title = 'My Profile';
        $query = DB::table('users')->select('*','countries.id as c_id, users.securit_code as pincode')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('users_bank_accounts', 'users_bank_accounts.users_id', '=', 'users.id')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();


        $ranks = Ranks::where(['id' => $query[0]->ranks_id])->first();

        $social_media_accounts = UserSocialMediaAccounts::where(['users_id' => $query[0]->id])->get();


        // $details = DB::table('registration_codes')->select('*')
        // ->join('control_panel_packages', 'registration_codes.packages_id', '=', 'control_panel_packages.id')
        // ->join('control_panel_entries', 'control_panel_packages.entries_id', '=', 'control_panel_entries.id')
        // ->where(['registration_codes.used_by' => Crypt::decryptString($id)])
        // ->get();

        $sponsor_id = $query[0]->sponsor_id;

        $data = DB::table('user_network_structure')->select('*')
        ->join('users', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['user_network_structure.sponsor_id' => $sponsor_id])
        ->get();

        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("SUM(registration_codes.price) as total"),DB::raw("count(registration_codes.order_number) as items"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where('registration_codes.distributor_id',$query[0]->id_number)
        ->groupBy("registration_codes.customer_id")
        ->get();

        $tophead = User::count();

        $countries = Country::where(['is_active' => 0])->get();

        $increment_username = UserNetworkStructure::where(['sponsor_id' =>$query[0]->id_number])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $sub_accounts =  DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['group_code' => $query[0]->group_code])
        ->whereNotIn('upline_id',['0000000'])
        ->get();

        $social_media = SocialMedia::where(['social_media_status' => 0])->get();
        $banks = Banks::where(['banks_status' => 0])->get();

        $product_pins = DB::table('product_pin_codes')->select('product_pin_codes.*','product_pin_codes.id as pin_id','product_description.*','product_packages.id as package_id','product_packages.product_title')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->where(['product_pin_codes.distributor_id' => $query[0]->id_number])
        ->groupBy('order_number')
        ->whereNotIn('product_pin_codes.status',[2])
        ->get();

        if(count($product_pins) > 0) {
            foreach($product_pins as $prods) {
                $total[$prods->pin_id] = DB::table('product_pin_codes')->select(DB::raw("SUM(price) as total"))->where(['customer_id' => $prods->customer_id])->first();
                $items[$prods->pin_id] = DB::table('product_pin_codes')->select(DB::raw("COUNT(order_number) as items"))->where(['customer_id' => $prods->customer_id])->first();
            }
        } else {
            $product_pins = [];
            $total = 0;
            $items = 0;
        }


        $wallets     = EWallet::where(['id_number' => $query[0]->id_number])->orderBy('created_at','DESC')->get();
        $wallet_logs = EWalletsLogs::where(['sender_id' => $query[0]->id_number])->orderBy('created_at','DESC')->get();


        return view('VirtualOffice.my-account.my-profile',compact('title','query','data','pins_history','tophead','countries','increment','sub_accounts','social_media','banks','social_media_accounts','product_pins','wallets','wallet_logs','ranks','total','items'));
    }


    public function update(Request $request, $id)
    {
        //USer
        $surname         = $request->surname;
        $firstname       = $request->firstname;
        $middlename      = $request->middlename;
        $email           = $request->email;
        $username        = $request->username;
        $password        = Crypt::encryptString($request->password);
        $status          = $request->status;
        //User Details
        $nickname        = $request->nickname;
        $birthdate       = $request->birthdate;
        $civil_status    = $request->civil_status;
        $gender          = $request->gender;
        $nationality     = $request->nationality;
        $province        = $request->province;
        $city            = $request->city;
        $address         = $request->address;
        $zipcode         = $request->zipcode;
        $tin             = $request->tin;
        //User Contacts
        $contact_number  = $request->contact_number;
        $landline        = $request->landline;
        $fax             = $request->fax;
        //User Beneficiaries
        $name            = $request->name;
        $relationship    = $request->relationship;
        //User Bank Details
        $banks_id        = Crypt::decryptString($request->banks_id);
        $branch          = $request->branch;
        $account_name    = $request->account_name;
        $account_number  = $request->account_number;
        //User Social Media Accounts
        $social_media_id = $request->social_media_id;
        $url             = $request->url;

        $users_id         = Crypt::decryptString($id);
        $user            = User::where(['id' => $users_id])->get();
        $userQuery       = User::where(['id' => $users_id])->update([
            'surname'       => $surname,
            'firstname'     => $firstname, 
            'middlename'    => $middlename,
            'email'         => $email,
            'username'      => $username,
            'password'      => $password,
            'status'        => $status,
        ]);
        if($userQuery) {
            $userDetailsQuery   = UserDetails::where(['users_id' => $users_id])->update([
                'nickname'      => $nickname,
                'birthdate'     => $birthdate,
                'civil_status'  => $civil_status,
                'gender'        => $gender,
                'nationality'   => $nationality,
                'province'      => $province,
                'city'          => $city,
                'address'       => $address,
                'zipcode'       => $zipcode,
                'tin'           => $tin,
            ]);

            if($userDetailsQuery) {
                $UserContactsQuery      = UserContacts::where(['users_id' => $users_id])->update([
                    'contact_number'    => $contact_number,
                    'landline'          => $landline,
                    'fax'               => $fax,
                ]);
            }

            if($UserContactsQuery) {
                $UserBeneficiaryQuery   = UserBeneficiary::where(['users_id' => $users_id])->update([
                    'name'              => $name,
                    'relationship'      => $relationship,
                ]);
            }

            if($UserBeneficiaryQuery) {
                $BankAccountsQuery   = UserBankAccounts::where(['users_id' => $users_id])->update([
                    'banks_id'          => $banks_id,
                    'branch'            => $branch,
                    'account_name'      => $account_name,
                    'account_number'    => $account_number,
                ]);
            }

            if($BankAccountsQuery) {

                for($x=0; $x<count($social_media_id); $x++) {
                    $media_id   = Crypt::decryptString($social_media_id[$x]);
                    $media_url  = $url[$x];
                    UserSocialMediaAccounts::where(['users_id' => $users_id,'social_media_id' => $media_id])->update([
                        'social_media_id'   => $media_id,
                        'url'               => $media_url,
                    ]);
                }
            }
        }
        Session::flash('message', 'Distributor information has been updated');
        return redirect('virtualoffice/my-account/profile')->with('message','Affiliate details has been updated.');
        
    }

    public function update_pincode(Request $request) {
        $query = DB::table('users')->select('*')->where(['id' => Crypt::decryptString(session()->get('id'))])->first();
        $pincode = $query->security_code;
        if(strlen($pincode) == 4 && strlen($request->old_pincode)) {
            if($pincode == $request->old_pincode) {
                DB::table('users')->where(['id' => $query->id])->update(['security_code' => $request->new_pincode]);
                // echo 'valid';
                return redirect()->back()->with('message', 'Pincode has been changed.');
            } else {
                // echo 'invalid';
                return redirect()->back()->with('message', 'Invalid pincode.');
            }
        } else {
            return redirect()->back()->with('message', 'pincode length must be 4.');
        }
        

    }

    
}
