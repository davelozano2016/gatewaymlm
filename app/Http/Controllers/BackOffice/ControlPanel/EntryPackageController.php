<?php

namespace App\Http\Controllers\BackOffice\ControlPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelPackages;
use App\ControlPanelEntries;
use App\ProductInclusions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;
class EntryPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($entries_id)
    {
        //
        $id = Crypt::decryptString($entries_id);
        $entries = DB::table('control_panel_entries')
        ->select('countries.name','countries.code','countries.currency_code','countries.currency_symbol','countries.currency_name','control_panel_entries.*','control_panel_entries.id as entry_id')
        ->join('countries', 'control_panel_entries.countries_id', '=', 'countries.id')
        ->where(['control_panel_entries.id' => $id])
        ->get();
        $title = $entries[0]->entry.' ('.number_format($entries[0]->price).' '.$entries[0]->currency_code.')';
        $packages = ControlPanelPackages::where(['entries_id' => $entries[0]->id])->get();

        if(count($packages) == 0) {
            $inclusions = [];
        } else {
            foreach($packages as $package) {
                $inclusions[$package->package_unique_code] = DB::table('product_inclusions')
                ->where(['unique_code' => $package->package_unique_code])
                ->get();
            }
        }

        


        return view('BackOffice.control-panel.entry-package',compact('title','entries','id','inclusions','packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(empty($request->packages_id)) {
            ControlPanelPackages::create([
                'package_unique_code'   => rand(111111111,999999999),
                'entries_id'            => Crypt::decryptString($request->entries_id),
                'package_name'          => $request->package_name,
                'package_status'        => $request->package_status,
                'description'           => $request->description,
            ]);
        } else {
            $ProductPackages = ControlPanelPackages::where(['package_unique_code' =>Crypt::decryptString($request->packages_id)]);
            $ProductPackages->update([
                'package_name'    => $request->package_name,
                'package_status'  => $request->package_status,
                'description'     => $request->description,
            ]);
        }

        Session::flash('message', 'New entry has been added.'); 

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($entry_id)
    {
        //
        $id    = Crypt::decryptString($entry_id);
        $query = ControlPanelPackages::findOrFail($id);
        $entry = $query->entry;
        $price = $query->price;
        echo json_encode(['entry' => $entry, 'price' => $price, 'entry_id' => $entry_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($entry_id)
    {
        //

        $id = Crypt::decryptString($entry_id);
        $record = ControlPanelPackages::where('id', $id)->delete();
        Session::flash('message', 'Entry has been deleted.'); 
        return back();
    }
}
