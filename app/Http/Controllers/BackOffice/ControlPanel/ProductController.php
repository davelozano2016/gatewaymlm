<?php

namespace App\Http\Controllers\BackOffice\ControlPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $title = 'Manage Products';
        $entries = ControlPanelEntries::all();
        $date = Carbon::now();
        return view('BackOffice.control-panel.product',compact('title','entries','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(empty($request->entry_id)) {
            ControlPanelEntries::create([
                'entry' => $request->entry,
                'price' => $request->price,
            ]);
        } else {
            $ProductPackages = ControlPanelEntries::find(Crypt::decryptString($request->entry_id));
            $ProductPackages->update([
                'id'    => $request->entry_id,
                'entry' => $request->entry,
                'price' => $request->price,
            ]);
        }
        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($entry_id)
    {
        //
        $id    = Crypt::decryptString($entry_id);
        $query = ControlPanelEntries::findOrFail($id);
        $entry = $query->entry;
        $price = $query->price;
        echo json_encode(['entry' => $entry, 'price' => $price, 'entry_id' => $entry_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($entry_id)
    {
        //

        $id = Crypt::decryptString($entry_id);
        if ($id) {
            $record = ControlPanelEntries::where('id', $id)->delete();
        }
        return back()->withInput();
    }
}
