<?php

namespace App\Http\Controllers\BackOffice\ControlPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use App\ControlPanelPackages;
use App\Country;
use App\ProductInclusions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;

class CreateEntryPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Create Entry';
        $entries = DB::table('control_panel_entries')
            ->select('countries.name','countries.code','countries.currency_code','countries.currency_symbol','countries.currency_name','control_panel_entries.*','control_panel_entries.id as entry_id')
            ->join('countries', 'control_panel_entries.countries_id', '=', 'countries.id')
            ->get();
        $countries = Country::where(['is_active' => 0])->get();
        $date = Carbon::now();
        return view('BackOffice.control-panel.create-entry-package',compact('title','entries','date','countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function create_inclusions(Request $request) {
        foreach($request->quantity as $key => $value) {
            if($value != null) {
                $product_package_id = Crypt::decryptString($request->product_package_id[$key]);
                $unique_code        = Crypt::decryptString($request->unique_code);
                $quantity           = $request->quantity[$key];
                $query = ProductInclusions::where(['unique_code' => $unique_code,'product_package_id' => $product_package_id])->count();
                if($query > 0) {
                    
                } else {
                    ProductInclusions::create([
                        'unique_code' => $unique_code,
                        'product_package_id' => $product_package_id,
                        'quantity' => $quantity,
                    ]);
                }
            } 
        }
        Session::flash('message', 'New inclusion has been added.'); 
        return back()->withInput();
    }

    
    public function delete_inclusion($id) {
        $inclusions_id = Crypt::decryptString($id);
        if ($inclusions_id) {
            $record = ProductInclusions::where('id', $inclusions_id)->delete();
        }
        Session::flash('message', 'Inclusion has been deleted.'); 
        return back()->withInput();
    }

    public function package_inclusion($id) {
        $title = 'Entry Package Inclusion';
        $package_unique_code = Crypt::decryptString($id);
        $entries = DB::table('control_panel_entries')
            ->select('countries.name','countries.code','countries.currency_code','countries.currency_symbol','countries.currency_name','control_panel_entries.*')
            ->join('countries', 'control_panel_entries.countries_id', '=', 'countries.id')
            ->get();
        $countries = Country::where(['is_active' => 0])->get();
        $date = Carbon::now();
        $products = DB::table('product_packages')->select('*','product_packages.id as package_id')
        ->join('product_description', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->groupBy(['product_packages.product_code'])
        ->get();
        $inclusions =  DB::table('product_inclusions')->select('*','product_inclusions.id as inclusions_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_inclusions.product_package_id')
        ->join('control_panel_packages', 'control_panel_packages.package_unique_code', '=', 'product_inclusions.unique_code')
        ->where(['product_inclusions.unique_code' => $package_unique_code])
        ->get();
        return view('BackOffice.control-panel.entry-package-inclusion',compact('title','entries','date','countries','products','package_unique_code','inclusions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for($i=0;$i < count($request->countries_id); $i++) {
            $countries_id               = $request->countries_id[$i];
            $price                      = $request->price[$i];
            $discount_type              = $request->discount_type;
            $stockist_discount          = $request->stockist_discount[$i];
            $depot_discount             = $request->depot_discount[$i];
            $country_manager_discount   = $request->country_manager_discount[$i];
            $direct_referal             = $request->direct_referal[$i];
            $binary_point_value         = $request->binary_point_value;
            ControlPanelEntries::create([
                'countries_id'              => $countries_id,
                'entry'                     => $request->entry,
                'price'                     => $price,
                'discount_type'             => $discount_type,
                'stockist_discount'         => $stockist_discount,
                'depot_discount'            => $depot_discount,
                'country_manager_discount'  => $country_manager_discount,
                'direct_referal'            => $direct_referal,
                'binary_point_value'        => $binary_point_value,
            ]);
        }
        Session::flash('message', 'New package entry has been added.'); 
        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($packages_id)
    {
        //
       
        $package_unique_code    = Crypt::decryptString($packages_id);
        $query = ControlPanelPackages::where(['package_unique_code' =>$package_unique_code])->first();
        $package_name = $query->package_name;
        $package_status = $query->package_status;
        echo json_encode(['package_name' => $package_name,'package_status' => $package_status,'packages_id' => $packages_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($entry_id)
    {
        //
        $id = Crypt::decryptString($entry_id);
        if ($id) {
            $record = ControlPanelPackages::where('id', $id)->delete();
        }
        Session::flash('message', 'Package entry has been deleted.'); 
        return back()->withInput();
    }
}
