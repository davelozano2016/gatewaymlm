<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Throwable;
use App\BinaryPairings;
use App\BinaryFlush;
use App\Bonuses;

class CronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $title = 'Business';
        // return view('BackOffice.business',compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function doPairing() {
        $query = DB::table('binary_points')->where('level','=',0)->get();
        $pairing = DB::table('control_panel_binary_paring_settings')->first();
        foreach($query as $data) {
            $array[$data->id_number][] = [$data->points,$data->id_number];
        }
        foreach($array as $id_number) {
            $arrayLeft[] = $id_number[0];
            $arrayRight[] = $id_number[1];
        }

        $details = DB::table('users')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'user_network_structure.country', '=', 'countries.code')
        ->where(['users.id_number' => $data->id_number])
        ->get();

        
        

        $country = DB::table('countries')->where(['code' => $details[0]->country])->first();
        $country_id = $country->id;
        $pairing_controls = DB::table('control_panel_binary_paring_settings')->where(['countries_id' => $country_id])->first();
        $pair_condition = $pairing_controls->pair_condition;
        $pair_amount_condition = $pairing_controls->pair_amount_condition;

        
        $counterLegs = count($arrayLeft);
        for($i=0;$i<$counterLegs;$i++) {
            if($arrayLeft[$i][0] < $pair_condition || $arrayRight[$i][0] < $pair_condition) {
                // break;
            } else {
                $id_number  = $arrayLeft[$i][1];
                $left       = $arrayLeft[$i][0];
                $right      = $arrayRight[$i][0];
                $l          = $left / $pair_condition;
                $r          = $right / $pair_condition;
                $counter    = min(array($l, $r)); // 5
                for($x=1;$x<=$counter;$x++) {
                    $userQuery = DB::table('users')
                    ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                    ->join('countries', 'user_network_structure.country', '=', 'countries.code')
                    ->where(['users.id_number' => $id_number])
                    ->first();

                    $registration  = DB::table('registration_codes')->where(['activation_code' => $userQuery->activation_code])->first();
                    $packages      = DB::table('control_panel_packages')->where(['id' => $registration->packages_id])->first();
                    $entries_id    = $packages->entries_id;
                    $maximum_query = DB::table('control_panel_maximum_daily_pairing_settings')->where(['entries_id' => $entries_id])->first();
                    // $maximum_pairs = 2; 
                    $maximum_pairs = $maximum_query->maximum_pairing; 

                    $hr = intval(date("H"));
                    
                    if ($hr <= 12)
                    {
                        $cycle = 0;
                    }
                    elseif ($hr >= 13)
                    { //13 up
                        $cycle = 1;
                    }


                    
                    $date = now();
            
                    $check = DB::table('binary_pairings')
                    ->where(['id_number' => $id_number,'cycle' => $cycle,'date_added' => date('Y-m-d')])
                    ->count();

                    
                    if($check <= $maximum_pairs) {
                        $user       = DB::table('users')->where(['id_number' =>$id_number])->first();
                        $users_id   = $user->id;
                        $test       =  $x % 5 == 0;
    
                       
    
                        DB::beginTransaction();
                        try {
                            Bonuses::create([
                                'recipient'     => $id_number,
                                'source'        => $id_number,
                                'bonus_type'    => 'Pairing Bonus',
                                'amount'        => $pair_amount_condition
                            ]);

                            BinaryPairings::create([
                                'id_number' => $id_number,
                                'points'    => $pair_condition,
                                'cycle'     => $cycle,
                                'date_added' => date('Y-m-d')
                            ]);
                            DB::table('user_accounts')->where(['users_id' => $users_id])->update(['total_current_income'=>DB::raw("total_current_income+$pair_amount_condition")]);
                            DB::table('binary_total_points')->insert(['id_number' => $id_number,'position' => 0, 'points' => $pair_condition]);
                            DB::table('binary_total_points')->insert(['id_number' => $id_number,'position' => 1, 'points' => $pair_condition]);
                                
                            DB::table('binary_points')->where(['id_number' => $id_number,'level' => 0])->update(['points'=>DB::raw("points-$pair_condition")]);
                            DB::commit();
                        } catch(Throwable $e) {
                            DB::rollback();
                            throw $e;
                        }
                    } else {
                        DB::beginTransaction();
                        try {
                            BinaryFlush::create([
                                'id_number'  => $id_number,
                                'flush_left' => $pair_condition,
                                'flush_right' => $pair_condition,
                            ]);
                            DB::table('binary_points')->where(['id_number' => $id_number,'level' => 0])->update(['points'=>DB::raw("points-$pair_condition")]);
                            DB::commit();
                        } catch(Throwable $e) {
                            DB::rollback();
                            throw $e;
                        }
                    }
                }
            }
        }  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
