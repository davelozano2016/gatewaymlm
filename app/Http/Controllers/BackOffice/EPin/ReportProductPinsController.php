<?php

namespace App\Http\Controllers\BackOffice\Epin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\ProductPinCodes;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportProductPinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        //
        $title = 'Report Product Pins';
        $details = DB::table('product_pin_codes')->select('product_pin_codes.*','product_description.*','product_packages.id as package_id','product_packages.product_title','product_pin_codes.id as product_pins_id')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->whereNotIn('product_pin_codes.status',[2])
        ->get();
        $used = ProductPinCodes::where(['status' => 1])->count();
        $unused = ProductPinCodes::where(['status' => 0])->count();
        $countries = Country::where(['is_active' => 0])->get();
        return view('BackOffice/e-pin/product-pins-reports',compact('title','details','countries','used','unused'));
    }

    public function result(Request $request) {
        $title        = 'Report Product Pins';
        $report_type  = $request->report_type;
        $status       = $request->status;
        if($report_type == 'Date Generated') {
            $column   = 'date_created';
        } else {
            $column   = 'date_used';
        }
        $date_start   = $request->date_start.' 00:00:00';
        $date_end     = $request->date_end.' 23:59:59';
        $country_code = $request->country_code;
        $start        = $request->date_start;
        $end          = $request->date_end;

        if($status == 'All') {
            $cond = ['country_code' => $country_code];
        } else {
            $cond = ['country_code' => $country_code,'status' => $status];
        }

        $country = Country::where('code',$country_code)->get();

        $details = DB::table('product_pin_codes')->select('product_pin_codes.*','product_description.*','product_packages.id as package_id','product_packages.product_title','product_pin_codes.id as product_pins_id')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->whereNotIn('product_pin_codes.status',[2])
        ->whereBetween($column, [$date_start, $date_end])
        ->where($cond)
        ->whereNotIn('product_pin_codes.status',[2])
        ->get();

        $used = ProductPinCodes::where(['status' => 1,'country_code' => $country_code])->whereBetween($column, [$date_start, $date_end])->count();
        $unused = ProductPinCodes::where(['status' => 0,'country_code' => $country_code])->whereBetween($column, [$date_start, $date_end])->count();
        $countries = Country::where(['is_active' => 0])->get();
        return view('BackOffice/e-pin/product-pins-reports',compact('title','details','status','report_type','countries','start','end','used','unused','country'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
