<?php

namespace App\Http\Controllers\BackOffice\EPin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use App\ControlPanelPackages;
use App\RegistrationCodes;
use App\Country;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;

class PaidCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Paid Codes';
        $packages = ControlPanelPackages::all();
        $countries = Country::where(['is_active' => 0])->get();
        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("SUM(registration_codes.price) as total"),DB::raw("count(registration_codes.order_number) as items"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where('registration_codes.type',0)
        ->groupBy("registration_codes.customer_id")
        ->get();

        $used_paid_pins = DB::table('registration_codes')
        ->select('registration_codes.*','control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2,0])
        ->where(['registration_codes.type' => 0])
        ->get();

        $random = rand(111111111,999999999);
        $date = Carbon::now();
        $used = RegistrationCodes::where(['status' => 1,'type' => 0])->count();
        $unused = RegistrationCodes::where(['status' => 0,'type' => 0])->count();
        return view('BackOffice.e-pin.paid-codes',compact('title','packages','pins_history','date','countries','random','used','unused','used_paid_pins'));
    }

    // custom functions 
    public function generate($id) {
        $title = 'Paid Codes';
        $packages = ControlPanelPackages::all();
        $countries = Country::where(['is_active' => 0])->get();
        $entries = DB::table('control_panel_entries')
        ->select('countries.name','countries.code','countries.currency_code','countries.currency_symbol','countries.currency_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','control_panel_entries.price','control_panel_entries.stockist_discount','control_panel_entries.depot_discount','control_panel_entries.country_manager_discount','control_panel_entries.created_at','control_panel_entries.binary_point_value')
        ->join('countries', 'control_panel_entries.countries_id', '=', 'countries.id')
        ->where(['control_panel_entries.countries_id' => Crypt::decryptString($id)])
        ->get();

        $random = rand(111111111,999999999);
        $date = Carbon::now();
        return view('BackOffice.e-pin.generate-paid-codes',compact('title','packages','entries','date','countries','random'));
    }

    public function visit($customer_id) {
        $title = Crypt::decryptString($customer_id);
        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*','registration_codes.id as registration_codes_id','control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->whereNotIn('registration_codes.status',[2])
        ->get();


        $packages = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("count(registration_codes.packages_id) as items"),DB::raw("sum(registration_codes.price) as total"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->groupBy('registration_codes.packages_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->get();

        $details = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("count(registration_codes.packages_id) as items"),DB::raw("sum(registration_codes.price) as total"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->get();
        return view('BackOffice.e-pin.visit-paid-codes',compact('title','pins_history','packages','details'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function print($customer_id) {
        $title = Crypt::decryptString($customer_id);
        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*','registration_codes.id as registration_codes_id','control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->whereNotIn('registration_codes.status',[2])
        ->get();

        $packages = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("count(registration_codes.packages_id) as items"),DB::raw("sum(registration_codes.price) as total"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->groupBy('registration_codes.packages_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->get();

        $details = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("count(registration_codes.packages_id) as items"),DB::raw("sum(registration_codes.price) as total"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where("registration_codes.customer_id",'=',Crypt::decryptString($customer_id))
        ->get();
        return view('BackOffice.e-pin.print-paid-codes',compact('title','pins_history','packages','details'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $counter = count($request->quantity);
        for($i=0;$i<$counter;$i++) {
            $quantity       = $request->quantity[$i];
            $packages_id    = $request->packages_id[$i];
            $package_type   = $request->package_type[$i];
            $bpv            = $request->bpv[$i];
            $price          = $request->price[$i];
            $country_code   = $request->country_code;
            $customer_id    = $request->customer_id;
            $distributor_id = $request->distributor_id;
            $order_number   = $request->order_number;
            $full_name      = $request->full_name;
            $created_by     = 'Admin';
            $registered_ip  = \Request::ip();
            
            for($x=1;$x<=$quantity;$x++) {

                $prefix = substr($package_type, 0, 1);
                $activation_code = $prefix.rand(11111,99999).'P'.rand(11111,99999).$country_code;

                RegistrationCodes::create([
                    'distributor_id'    => $distributor_id,
                    'packages_id'       => $packages_id,
                    'customer_id'       => $customer_id,
                    'full_name'         => $full_name,
                    'activation_code'   => $activation_code,
                    'bpv'               => $bpv,
                    'order_number'      => $order_number,
                    'price'             => $price,
                    'status'            => 0,
                    'type'              => 0, // Paid
                    'country_code'      => $country_code, 
                    'created_by'        => $created_by,
                    'registered_ip'     => $registered_ip,
                    'date_processed'    => date('Y-m-d h:i:s'),
                    'date_expiration'   => date('Y-m-d h:i:s',strtotime('+1 month')),
                    'date_used'         => '0000-00-00 00:00:00',
                    'date_created'      => date('Y-m-d h:i:s'),
                ]);
            }
        }
        Session::flash('message', 'New paid package pins has been generated with order number '.$order_number);
        return redirect('backoffice/e-pin/paid-codes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function mass_status_update(Request $request) {
        if(isset($_POST['btn_locked'])) {
            $status = 1;
            $message = 'locked';
        } else {
            $status = 0;
            $message = 'unlocked';
        }
        foreach($request->selected as $id) {
            RegistrationCodes::where('id', $id)->update(['lock_status' => $status]);
        }
        return redirect('backoffice/e-pin/paid-codes/visit/'.$request->customer_id)->with('message', 'Pin code has been '.$message);
    }

    public function status($cust_id,$reg_id,$stats) {
        $id     = Crypt::decryptString($reg_id);
        $status = Crypt::decryptString($stats) == 0 ? 1 : 0;
        $message = Crypt::decryptString($stats) == 0 ? 'locked' : 'unlocked';
        RegistrationCodes::where('id', $id)->update(['lock_status' => $status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id)
    {
        //
        // $query = RegistrationCodes::where('customer_id',Crypt::decryptString($customer_id),'status','!=',1));
        $query = RegistrationCodes::where(['customer_id' => Crypt::decryptString($customer_id),'status' => 0]);
        if ($query != null) {
            $query->update(['status' => 2]);
            return back()->withInput(['message'=> 'Successfully deleted!!']);
        } 
    }
}