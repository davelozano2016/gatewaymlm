<?php

namespace App\Http\Controllers\BackOffice\EPin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use App\ControlPanelPackages;
use App\RegistrationCodes;
use App\Country;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DownloadEPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Download E-Pins';
        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("SUM(registration_codes.price) as total"),DB::raw("count(registration_codes.order_number) as items"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->groupBy("registration_codes.customer_id")
        ->get();
        return view('BackOffice/e-pin/download-pins',compact('title','pins_history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
