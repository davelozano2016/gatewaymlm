<?php

namespace App\Http\Controllers\BackOffice\EPin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelEntries;
use App\ControlPanelPackages;
use App\RegistrationCodes;
use App\Country;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportEPinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Report Package Pins';
        $details = DB::table('registration_codes')
        ->select('registration_codes.*','control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->get();
        $used = RegistrationCodes::where(['status' => 1])->count();
        $unused = RegistrationCodes::where(['status' => 0])->count();
        $countries = Country::where(['is_active' => 0])->get();
        return view('BackOffice/e-pin/reports',compact('title','details','countries','used','unused'));
    }

    public function result(Request $request) {
        $title = 'Report Package Pins';
        $report_type  = $request->report_type;
        if($report_type == 'Date Generated') {
            $column   = 'date_created';
        } else {
            $column   = 'date_used';
        }
        $date_start   = $request->date_start.' 00:00:00';
        $date_end     = $request->date_end.' 23:59:59';
        $country_code = $request->country_code;

        $country = Country::where('code',$country_code)->get();

        $details = DB::table('registration_codes')
        ->select('registration_codes.*','control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereBetween($column, [$date_start, $date_end])
        ->where('country_code',$country_code)
        ->whereNotIn('registration_codes.status',[2])
        ->get();

        $used = RegistrationCodes::where(['status' => 1,'country_code' => $country_code])->whereBetween($column, [$date_start, $date_end])->count();
        $unused = RegistrationCodes::where(['status' => 0,'country_code' => $country_code])->whereBetween($column, [$date_start, $date_end])->count();
        $countries = Country::where(['is_active' => 0])->get();
        return view('BackOffice/e-pin/reports',compact('title','details','countries','date_start','date_end','used','unused','country'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
