<?php

namespace App\Http\Controllers\BackOffice\EPin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\ProductPinCodes;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class ProductPinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    public function generate_pins() {
        $title = 'Generate Product Pins';
        $countries  = Country::where(['is_active' => 0])->get();
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->get();
        foreach($users as $user) {
            $used[$user->id_number] = ProductPinCodes::where(['distributor_id' => $user->id_number,'status' => 1])->count();
            $unused[$user->id_number] = ProductPinCodes::where(['distributor_id' => $user->id_number,'status' => 0])->count();
        }
        return view('BackOffice.e-pin.generate-product-pins',compact('title','users','countries','used','unused'));
    }

    public function generate($id_number,$country) {
        $title = 'Generate Product Pins';
        $countries  = Country::where(['is_active' => 0])->get();
        $random = rand(1111111111,9999999999);

        $entries = DB::table('product_packages')->select('product_packages.*','product_description.id as pdid','product_description.product_packages_id','product_description.country','product_description.distributor_price','product_description.srp','product_description.retailer','product_description.stockist_discount','product_description.depot_discount','country_manager_discount','countries.code','countries.currency_symbol')
        ->join('product_description', 'product_description.product_packages_id', '=', 'product_packages.id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->where(['product_description.country' => Crypt::decryptString($country)])
        ->distinct()
        ->get();
        if(count($entries) == 0) {
            return redirect()->back()->with('message', 'No product were found.');
        }

        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id_number' => Crypt::decryptString($id_number)])
        ->get();
        return view('BackOffice.e-pin.generate',compact('title','countries','random','entries','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function unused_pins()
    {
        //
        $title = 'Available Pins';
        $unused = DB::table('product_pin_codes')->select('product_pin_codes.*','product_pin_codes.id as pin_id','product_description.*','product_packages.id as package_id','product_packages.product_title')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->whereNotIn('product_pin_codes.status',[2,1])
        ->get();
        return view('BackOffice.e-pin.unused-product-pins',compact('title','unused'));
    }

    public function used_pins()
    {
        //
        $title = 'Used Product Pins';
        $used = DB::table('product_pin_codes')->select('product_pin_codes.*','product_description.*','product_packages.id as package_id','product_packages.product_title')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->whereNotIn('product_pin_codes.status',[2,0])
        ->get();
        return view('BackOffice.e-pin.used-product-pins',compact('title','used'));
    }

    public function visit($customer_id) {
        $title = Crypt::decryptString($customer_id);

        $product_pins = DB::table('product_pin_codes')->select('product_pin_codes.*','product_description.*','product_packages.id as package_id','product_packages.product_title','product_pin_codes.id as product_pins_id')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->whereNotIn('product_pin_codes.status',[2])
        ->where(['product_pin_codes.customer_id' => $title])
        ->get();
        $details = DB::table('product_pin_codes')->select('*',DB::raw("count(product_pin_codes.customer_id) as items"),DB::raw("sum(product_pin_codes.price) as total"))
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('countries', 'countries.code', '=', 'product_pin_codes.country_code')
        ->where(['product_pin_codes.customer_id' => $title])
        ->first();
        return view('BackOffice.e-pin.visit-product-pins',compact('title','product_pins','details'));
    }

    public function report() {

    }
    
    public function mass_status_update(Request $request) {
        if(isset($_POST['btn_locked'])) {
            $status = 1;
        } else {
            $status = 0;
        }
        foreach($request->selected as $id) {
            ProductPinCodes::where('id', $id)->update(['lock_status' => $status]);
        }
        return redirect('backoffice/e-pin/product-pins/visit/'.$request->customer_id)->with('message', '');
    }

    public function status($customer_id,$pin_id,$status) {
        $id     = Crypt::decryptString($pin_id);
        $status = Crypt::decryptString($status) == 0 ? 1 : 0;

        ProductPinCodes::where('id', $id)->update(['lock_status' => $status]);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function print() {

    }

    public function destroy($customer_id)
    {
        //
        // $query = RegistrationCodes::where('customer_id',Crypt::decryptString($customer_id),'status','!=',1));
        // dd(Crypt::decryptString($customer_id));
        $query = ProductPinCodes::where(['customer_id' => Crypt::decryptString($customer_id),'status' => 0]);
        if ($query != null) {
            $query->update(['status' => 2]);
            return back()->withInput(['message'=> 'Successfully deleted!!']);
        } 
    }
}
