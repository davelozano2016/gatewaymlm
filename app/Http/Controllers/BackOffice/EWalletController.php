<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\EWallet;
use App\UserEwallet;
use App\EWalletsLogs;
use App\User;
use Session;
class EWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'E-Wallet';
        $purhase_wallet_query = DB::table('e_wallets')->select('*','e_wallets.created_at as transaction_date','e_wallets.status as ewallet_status','e_wallets.id as wallet_id','users.id as users_id')
        ->join('users', 'users.id_number', '=', 'e_wallets.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->join('user_ewallets', 'user_ewallets.id', '=', 'users.id')
        ->get();
        return view('BackOffice.e-wallet',compact('title','purhase_wallet_query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $permitted_chars  = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $reference        = substr(str_shuffle($permitted_chars), 0, 7);
        $wallet_id        = Crypt::decryptString($id);
        $count            = EWallet::where(['id' => $wallet_id])->count();
        $users            = User::where(['id' => $request->users_id])->first();

        $validate = EWallet::where(['id' => $wallet_id,'status' => 'Completed'])->get();
        if(count($validate) > 0) {
            $message = 'already completed';
        } else {
            if($count == 0) {

            } else {
                $query = EWallet::where(['id' => $wallet_id])->update(['status' => $request->status,'notes' => $request->notes]);
                if($query) {
                    if($request->status == 'Completed') {
                        $wallet = $request->current_fund + $request->amount;
                        UserEwallet::where(['users_id' => $request->users_id])->update(['fund' => $wallet]);
                        EWalletsLogs::create(['sender_id' => $users->id_number,'receiver_id' => $users->id_number,'category' => 'E-Wallet topup','description' => 'E-Wallet topup','status' => 0, 'value' => $request->amount,'reference' => 'TU'.$reference,'current_balance' => $wallet]);
                        Session::flash('message', 'E-Wallet transaction has been completed.'); 
                    } elseif($request->status == 'Cancelled') {
                        UserEwallet::where(['users_id' => $request->users_id])->update(['status' => $request->status]);
                        Session::flash('message', 'E-Wallet transaction has been cancelled.'); 
                    }
                }
            }
            $message  = 'test';
        }
       
        return redirect('backoffice/e-wallet')->with('message',$message);

    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
