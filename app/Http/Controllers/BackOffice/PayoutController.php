<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\EncashmentHistory;
use App\EWalletsLogs;
use App\UserEwallet;

class PayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Payout';
        $encashments            = $this->encashments(0);
        $released_encashments   = $this->released_encashments();
        $cancelled_encashments  = $this->encashments(2);
        $pending                = $this->status(0);
        $completed              = $this->status(1);
        $cancelled              = $this->status(2);
        $countries              = DB::table('countries')->get();
        return view('BackOffice.payout',compact('title','encashments','countries','pending','completed','cancelled','cancelled_encashments','released_encashments'));
    }


    public function encashments($status) {
        $encashments = DB::table('encashments')->select('*','encashments.id as encash_id')
        ->join('users', 'encashments.users_id', '=', 'users.id')
        ->join('countries', 'countries.code', '=', 'encashments.country_code')
        ->where(['encashments.status' => $status])
        ->get();
        return $encashments;
    }

    public function released_encashments() {
        $encashments = DB::table('encashments')->select('*','encashments.id as encash_id','encashments.created_at as date_requested','encashment_histories.created_at as date_processed',DB::raw('SUM(request_bonus) as request_amount'),DB::raw('SUM(payout) as total_payout'),DB::raw('COUNT(encashment_histories.reference_code) as items'))
        ->join('users', 'encashments.users_id', '=', 'users.id')
        ->join('encashment_histories', 'encashment_histories.encashments_id', '=', 'encashments.id')
        ->join('countries', 'countries.code', '=', 'encashments.country_code')
        ->groupBy(['encashment_histories.reference_code'])
        ->get();
        return $encashments;
    }

    public function search(Request $request) {
        $title  = 'Payout';
        $from   = $request->from.' 00:00:00';
        $to     = $request->to.' 23:59:59';
        $encashments = DB::table('encashments','encashments.id as encash_id')
        ->join('users', 'encashments.users_id', '=', 'users.id')
        ->join('countries', 'countries.code', '=', 'encashments.country_code')
        ->whereBetween('encashments.created_at', array($from, $to))->where(['country_code' => $request->country_code])->get();
        $countries = DB::table('countries')->get();
        $data = array(
            'from'      => $request->from,
            'to'        => $request->to,
            'country'   => $request->country_code,
        );
        $pending    = $this->status(0,$data);
        $completed  = $this->status(1,$data);
        $cancelled  = $this->status(2,$data);
        return view('BackOffice.payout',compact('title','encashments','countries','data','pending','completed','cancelled'));
    }

    
    public function status($status,$data = '') {
        if(empty($data)) {
            $query = DB::table('encashments')->where(['status' => $status])->count();
        } else {
            $from  = $data['from'].' 00:00:00';
            $to    = $data['to'].' 23:59:59';
            $query = DB::table('encashments')->whereBetween('created_at', array($from, $to))->where(['status' => $status,'country_code' => $data['country']])->count();
        }
        return $query;
    }

    public function process($id) {
        $encash_id = Crypt::decryptString($id);
        $permitted_chars    = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        $shuffleData        = substr(str_shuffle($permitted_chars), 0, 7);
        $threeLetters       = substr(str_shuffle($permitted_chars), 0, 3);
        $reference          = 'E'.rand(111,999).$threeLetters.date('mdYhi');
        $query              = DB::table('encashments')->where(['status' => 0,'id' => $encash_id])->get();
        foreach($query as $data) {
            $encash_id   = $data->id;
            $users_id    = $data->users_id;
            $user        = DB::table('users')->where(['id' => $users_id])->first();
            $sender      = $user->id_number;
            $sender_id   = DB::table('encashment_histories')->insert(['encashments_id' => $encash_id,'reference_code' => $reference]);
            $walletQuery = DB::table('user_ewallets')->where(['users_id' => $users_id])->first();
            $wallet      = $walletQuery->fund;

            $queryCheck = EWalletsLogs::where(['reference' => $data->reference])->exists();
            if($queryCheck) {
                $row = EWalletsLogs::where(['reference' => $data->reference])->first();
                EWalletsLogs::where(['reference' => $data->reference])->update(['category' => 'Processed Encashment','description' => 'E-Wallet Withdraw']);
            } else {
                EWalletsLogs::create([
                    'sender_id'       => $sender,
                    'receiver_id'     => $sender,
                    'category'        => 'Processed Encashment',
                    'description'     => 'E-Wallet Withdraw',
                    'status'          => 1, 
                    'value'           => $data->payout,
                    'reference'       => 'EWW'.$reference,
                    'current_balance' => $wallet
                ]);
            }

            
            if($query) {
                DB::table('encashments')->where(['id' => $encash_id])->update(['status' => 1]);
            }
        }

        return redirect()->back()->with('message','Single process has been executed.');

    }


    public function multi_process() {
        // $query = DB::table('encashments')->where(['status' => 0])->get();
        // $permitted_chars    = 'ABCDE012345FGHIJK6789LMNOPQRSTUVWXYZ';
        // $shuffleData        = substr(str_shuffle($permitted_chars), 0, 7);
        // $threeLetters       = substr(str_shuffle($permitted_chars), 0, 3);
        // $reference          = 'E'.rand(111,999).$threeLetters.date('mdYhi');

    
        // // DB::beginTransaction();
        // // try {
           
        // // } catch(Throwable $e) {
        // //     DB::rollback();
        // //     throw $e;
        // // }


        
        // foreach($query as $data) {
        //     $encash_id   = $data->id;
        //     $users_id    = $data->users_id;
        //     $user        = DB::table('users')->where(['id' => $users_id])->first();
        //     $sender      = $user->id_number;
        //     $sender_id   = DB::table('encashment_histories')->insert(['encashments_id' => $encash_id,'reference_code' => $reference]);
        //     $walletQuery = DB::table('user_ewallets')->where(['users_id' => $users_id])->first();
        //     $wallet      = $walletQuery->fund - $data->payout;
        //     EWalletsLogs::create([
        //         'sender_id'       => $sender,
        //         'receiver_id'     => $sender,
        //         'category'        => 'Processed Encashment',
        //         'description'     => 'E-Wallet Withdraw',
        //         'status'          => 1, 
        //         'value'           => $data->payout,
        //         'reference'       => 'EWW'.$reference,
        //         'current_balance' => $wallet
        //     ]);
        //     UserEwallet::where(['users_id' => $users_id])->update(['fund' => $wallet]);
        //     if($query) {
        //         DB::table('encashments')->where(['id' => $encash_id])->update(['status' => 1]);
        //     }
        // }

        // return redirect()->back()->with('message','Multi Encashment has been executed.');
        return redirect()->back()->with('message','Still on development.');


    }

    public function voucher($id) {
        $reference = Crypt::decryptString($id);
        $title = 'Voucher '.$reference;
        $encashments = DB::table('encashments')->select('*','encashments.id as encash_id')
        ->join('users', 'encashments.users_id', '=', 'users.id')
        ->join('countries', 'countries.code', '=', 'encashments.country_code')
        ->join('encashment_histories', 'encashment_histories.encashments_id', '=', 'encashments.id')
        ->where(['encashment_histories.reference_code' => $reference])
        ->get();

        return view('BackOffice.voucher',compact('title','encashments'));
    }


    public function details($id) {
        $reference = Crypt::decryptString($id);

        $query = DB::table('encashments')->select('*','encashments.id as encash_id')
        ->join('users', 'encashments.users_id', '=', 'users.id')
        ->join('countries', 'countries.code', '=', 'encashments.country_code')
        ->join('encashment_histories', 'encashment_histories.encashments_id', '=', 'encashments.id')
        ->where(['encashment_histories.reference_code' => $reference])
        ->get();

        dd($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id_number = Crypt::decryptString($id);
        $check = DB::table('encashments')->where(['id' => $id_number])->get();
        if(count($check) > 0) {
            $users_id   = $check[0]->users_id;
            $fund       = $check[0]->payout;
            $query      = DB::table('user_ewallets')->where(['users_id' => $users_id])->update(['fund'=>DB::raw("fund+$fund")]);
            if($query) {
                $reference = $check[0]->reference;

                DB::table('e_wallets_logs')->where(['reference' => $reference])->delete();
                DB::table('encashments')->where(['id' => $id_number])->delete();
            }
        } else {
            
        }

        return redirect()->back()->with('message','Encashment has been cancelled.');
    }
}