<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserEwallet;
use App\UserNetworkStructure;
use App\RegistrationCodes;
use App\User;
use App\Country;
use App\SocialMedia;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserSocialMediaAccounts;
use App\UserAccount;
use App\BinaryPoints;
use App\UsersUnilevel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Register';
        $countries = Country::where(['is_active' => 0])->get();
        $tophead = User::count();
        return view('BackOffice.register',compact('title','countries','tophead'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function search(Request $request) {
        $query = User::where(['id_number' => $request->id_number])->first();
        echo json_encode(['name' => $query->firstname.' '.$query->surname]);
    }

    public function store(Request $request)
    {
        //
        $surname         = $request->surname;
        $firstname       = $request->firstname;
        $middlename      = $request->middlename;
        $email           = $request->email;

        $sponsor_id      = $request->sponsor_id;
        $upline_id       = $request->upline_id;
        $position        = $request->position;
        $activation_code = $request->activation_code;

        $username        = $request->username;
        $password        = Crypt::encryptString($request->password);
        $query           = RegistrationCodes::where(['activation_code' => $activation_code, 'status' => 0, 'lock_status' => 0])->count();
        if($query == 1) {
            $data       = RegistrationCodes::where(['activation_code' => $activation_code])->get();
            $rc_id      = $data[0]->id;
            $type       = $data[0]->type;
            $country    = $data[0]->country_code;
            $id_number  = rand(1111111,9999999);
            User::create([
                'id_number'      => $id_number,
                'surname'        => $surname,
                'firstname'      => $firstname,
                'middlename'     => $middlename,
                'email'          => $email,
                'username'       => $username,
                'password'       => $password,
                'status'         => 0,
                'date_registered'=> NOW(),
                'month'          => date('m'),
                'ranks_id'       => 1,
                'remember_token' => rand(111111,999999),
            ]);
            
            $user                 = User::latest()->first();
            $id                   = $user->id;
            $id_number            = $user->id_number;
            $network = UserNetworkStructure::create([
                'users_id'        => $id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => $activation_code,
                'status_type'     => $type,
                'group_code'      => Crypt::encryptString($id_number),
            ]);

            if($network) {
                RegistrationCodes::where(['id' => $rc_id])->update(['used_by' => $id_number,'status' => 1,'date_used' => NOW()]);
                UserEwallet::create(['users_id' => $id]);
                UserDetails::create(['users_id' => $id]);
                UserContacts::create(['users_id' => $id]);
                UserBeneficiary::create(['users_id' => $id]);
                UserBankAccounts::create(['users_id' => $id]);
                UserAccount::create(['users_id' => $id]);
                BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 0,'points' => 0]);
                BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 1,'points' => 0]);
                UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
                $social= SocialMedia::all();
                foreach($social as $s) {
                    UserSocialMediaAccounts::create(['users_id' => $id,'social_media_id' => $s->id]);
                }
                return redirect('backoffice/register/')->with('message', 'New distributor has been added.');
            }

        } else {
            return redirect('backoffice/register/')->with('message', 'Activation code already used or has been locked.');
        }
       

    }

    public function create_sub_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $tophead = User::count();
        $registration_codes = RegistrationCodes::where(['status' => 0])->get();
        $increment_username = UserNetworkStructure::where(['sponsor_id' => Crypt::decryptString($sponsor_id)])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $users = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['users.id_number' => Crypt::decryptString($sponsor_id)])
        ->get();

        return view('BackOffice.register-sub-account',compact('title','tophead','sponsor_id','id_number','position','registration_codes','increment','users'));
    }

    public function create_main_account($sponsor_id,$id_number,$position) {
        $title = 'Register';
        $countries = Country::where(['is_active' => 0])->get();
        $tophead = User::count();
        return view('BackOffice.register-main-account',compact('title','countries','tophead','sponsor_id','id_number','position'));
    }

    public function store_sub_account(Request $request) {
        $activation_code = $request->sub_account_activation_code;
        $sponsor_id      = $request->sub_account_sponsor_id;
        $upline_id       = $request->sub_account_placement_id;
        $position        = $request->sub_account_position;
        $group_code      = $request->sub_account_group_code;
        $username        = $request->sub_account_username;
        $password        = Crypt::encryptString($request->sub_account_password);
        $query           = RegistrationCodes::where(['activation_code' => $activation_code, 'status' => 0, 'lock_status' => 0])->count();
        if($query == 1) {
            $data       = RegistrationCodes::where(['activation_code' => $activation_code])->get();
            $rc_id      = $data[0]->id;
            $type       = $data[0]->type;
            $country    = $data[0]->country_code;
            $id_number  = rand(1111111,9999999);
            $row       = User::where(['id_number' => $sponsor_id])->get();
            User::create([
                'id_number'      => $id_number,
                'surname'        => $row[0]->surname,
                'firstname'      => $row[0]->firstname,
                'middlename'     => $row[0]->middlename,
                'email'          => $row[0]->email,
                'username'       => $username,
                'password'       => $password,
                'status'         => 0,
                'date_registered'=> NOW(),
                'month'          => date('m'),
                'ranks_id'       => 1,
                'remember_token' => rand(111111,999999),
            ]);
            
            $user                 = User::latest()->first();
            $users_id             = $user->id;
            $network = UserNetworkStructure::create([
                'users_id'        => $users_id,
                'country'         => $country,
                'sponsor_id'      => $sponsor_id,
                'upline_id'       => $upline_id,
                'position'        => $position,
                'activation_code' => $activation_code,
                'status_type'     => $type,
                'group_code'      => $group_code,
                'account_type'    => 1,
            ]);

            if($network) {
                RegistrationCodes::where(['id' => $rc_id])->update(['used_by' => $id_number,'status' => 1,'date_used' => NOW()]);
                UserEwallet::create(['users_id' => $users_id]);
                UserDetails::create(['users_id' => $users_id]);
                UserContacts::create(['users_id' => $users_id]);
                UserBeneficiary::create(['users_id' => $users_id]);
                UserBankAccounts::create(['users_id' => $users_id]);
                UserAccount::create(['users_id' => $id]);
                BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 0,'points' => 0]);
                BinaryPoints::create(['id_number' => $id_number,'waiting_id_number' => $id_number, 'level' => 0,'position' => 1,'points' => 0]);
                UsersUnilevel::create(['id_number' => $id_number,'unilevel_point_value' => '0.00','unilevel_income' => '0.00']);
                $social= SocialMedia::all();
                foreach($social as $s) {
                    UserSocialMediaAccounts::create(['users_id' => $users_id,'social_media_id' => $s->id]);
                }
                return redirect('backoffice/profile-management/members/'.$group_code)->with('message', 'New sub account has been added.');
            }

        } else {
            return redirect('backoffice/register/')->with('message', 'Activation code already used or has been locked.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}