<?php

namespace App\Http\Controllers\BackOffice\OrderDetails;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\ProductTracking;
use App\ProductPinCodes;
use App\RegistrationCodes;

class OrderHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $date  = Carbon::now();
        $title = 'Order History';
        $orders = DB::table('product_pin_codes')->select('product_pin_codes.*','countries.currency_symbol','product_pin_codes.id as pin_id')
        ->join('countries', 'countries.code', '=', 'product_pin_codes.country_code')
        ->groupBy('reference')->get();
        $total_price = [];
        $total_items = [];
        foreach($orders as $order) {
            $total_price[$order->pin_id] = DB::table('product_pin_codes')->select('*',DB::raw("SUM(product_pin_codes.price) as total"))->where(['reference' => $order->reference])->get();
        }

        foreach($orders as $order) {
            $total_items[$order->pin_id] = DB::table('product_pin_codes')->select('*',DB::raw("count(product_pin_codes.reference) as items"))->where(['reference' => $order->reference])->get();
        }

        
        $packages_orders = DB::table('registration_codes')->select('registration_codes.*','countries.currency_symbol','registration_codes.id as packages_id')
        ->join('countries', 'countries.code', '=', 'registration_codes.country_code')
        ->where('reference','!=',NULL)
        ->groupBy('reference')->get();

        $total_package_price = [];
        $total_package_items = [];
        foreach($packages_orders as $package_order) {
            $total_package_price[$package_order->packages_id] = DB::table('registration_codes')->select('*',DB::raw("SUM(registration_codes.price) as total"))->where(['reference' => $package_order->reference])->get();
        }

        foreach($packages_orders as $package_order) {
            $total_package_items[$package_order->packages_id] = DB::table('registration_codes')->select('*',DB::raw("count(registration_codes.reference) as items"))->where(['reference' => $package_order->reference])->get();
        }

        return view('BackOffice.order-details.order-history',compact('title','orders','total_price','total_items','packages_orders','total_package_price','total_package_items'));
        
    }


    public function package_track($ref) {
        $title = 'Track My Order';
        $reference = Crypt::decryptString($ref);
        $query = DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->where(['users.id' => Crypt::decryptString(session()->get('id'))])->get();
        
        $categories = DB::table('product_categories')->get();

        $timelines = DB::table('product_trackings')->select('product_trackings.*','product_trackings.status as timeline_status','product_trackings.order_status as timeline_order_status','registration_codes.reference','registration_codes.method_of_payment as method','registration_codes.local_pickup as pickup')
        ->join('registration_codes', 'product_trackings.reference', '=', 'registration_codes.reference')
        ->distinct()
        ->orderBy('product_trackings.id','DESC')
        ->where(['product_trackings.reference' => $reference])->get();
        return view('BackOffice.order-details.package-track-orders',compact('title','query','categories','reference','timelines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function track($ref) {
        $title = 'Track My Order';
        $reference = Crypt::decryptString($ref);

        
        $categories = DB::table('product_categories')->get();

        $timelines = DB::table('product_trackings')->select('product_trackings.*','product_trackings.status as timeline_status','product_trackings.order_status as timeline_order_status','product_pin_codes.reference','product_pin_codes.method_of_payment as method','product_pin_codes.local_pickup as pickup','product_pin_codes.distributor_id')
        ->join('product_pin_codes', 'product_trackings.reference', '=', 'product_pin_codes.reference')
        ->distinct()
        ->orderBy('product_trackings.id','DESC')
        ->where(['product_trackings.reference' => $reference])->get();
        return view('BackOffice.order-details.track-orders',compact('title','categories','reference','timelines'));
    }

    public function edit_orders($ref) {
        $reference = Crypt::decryptString($ref);
        $title = $reference;
        $categories = DB::table('product_categories')->get();
        $products = DB::table('product_packages')->select('*',db::raw('COUNT(product_pin_codes.product_description_id) as quantity'),db::raw('SUM(product_pin_codes.price) as total'),'product_pin_codes.created_at as order_date')
        ->join('product_description', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_pin_codes', 'product_pin_codes.product_description_id', '=', 'product_description.id')
        ->join('product_shipping_details', 'product_shipping_details.reference', '=', 'product_pin_codes.reference')
        ->join('product_transactions', 'product_transactions.reference', '=', 'product_pin_codes.reference')        
        ->groupBy('product_pin_codes.product_description_id')
        ->where(['product_pin_codes.reference' => $reference])->get();
        $users = User::where(['id_number' => $products[0]->distributor_id])->first();
        return view('BackOffice.order-details.edit-order-details',compact('title','categories','reference','products','users'));
    }

    public function package_edit_orders($ref) {
        $reference  = Crypt::decryptString($ref);
        $title      = $reference;
        $packages   = DB::table('registration_codes')->select('*',db::raw('COUNT(registration_codes.packages_id) as quantity'),db::raw('SUM(registration_codes.price) as total'),'registration_codes.created_at as order_date')
        ->join('product_shipping_details', 'product_shipping_details.reference', '=', 'registration_codes.reference')
        ->join('product_transactions', 'product_transactions.reference', '=', 'registration_codes.reference')        
        ->groupBy('registration_codes.packages_id')
        ->where(['registration_codes.reference' => $reference])->get();
        $users = User::where(['id_number' => $packages[0]->distributor_id])->first();
        return view('BackOffice.order-details.package-edit-order-details',compact('title','reference','packages','users'));
    }


    
    public function view_orders($ref) {
        $reference = Crypt::decryptString($ref);
        $title = 'Orders';
        
        $categories = DB::table('product_categories')->get();

        $products = DB::table('product_packages')->select('*',db::raw('COUNT(product_pin_codes.product_description_id) as quantity'),db::raw('SUM(product_pin_codes.price) as total'),'product_pin_codes.created_at as order_date')
        ->join('product_description', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_pin_codes', 'product_pin_codes.product_description_id', '=', 'product_description.id')
        ->join('product_shipping_details', 'product_shipping_details.reference', '=', 'product_pin_codes.reference')
        ->join('product_transactions', 'product_transactions.reference', '=', 'product_pin_codes.reference')        
        ->groupBy('product_pin_codes.product_description_id')
        ->where(['product_pin_codes.reference' => $reference])
        
        ->get();
        $users = User::where(['id_number' => $products[0]->distributor_id])->first();
        return view('BackOffice.order-details.view-orders',compact('title','categories','reference','products','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        ProductTracking::create(['reference' => $request->reference,'notes' => $request->notes,'status' => $request->status,'order_status' => $request->order_status]);
        ProductPinCodes::where(['reference' => $request->reference])->update(['order_status' => $request->status]);
        return redirect('backoffice/order-details/order-history')->with('message','Transaction has been updated');
    }

    public function package_update(Request $request)
    {
        //
        ProductTracking::create(['reference' => $request->reference,'notes' => $request->notes,'status' => $request->status,'order_status' => $request->order_status]);
        RegistrationCodes::where(['reference' => $request->reference])->update(['order_status' => $request->status]);
        return redirect('backoffice/order-details/order-history')->with('message','Transaction has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
