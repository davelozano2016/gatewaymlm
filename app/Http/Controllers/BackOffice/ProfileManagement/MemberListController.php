<?php

namespace App\Http\Controllers\BackOffice\ProfileManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserNetworkStructure;
use App\Country;
use App\SocialMedia;
use App\Banks;
use App\UserDetails;
use App\UserContacts;
use App\UserBeneficiary;
use App\UserBankAccounts;
use App\UserSocialMediaAccounts;
use App\EWallet;
use App\EWalletsLogs;
use App\RegistrationCodes;
use App\Ranks;
use Response;
use Session;

class MemberListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Member List';
        $paid     = UserNetworkStructure::where('status_type',0)->count();
        $cd       = UserNetworkStructure::where('status_type',1)->count();
        $free     = UserNetworkStructure::where('status_type',2)->count();
        $floating = UserNetworkStructure::where('status_type',3)->count();
        $today    = User::whereDate('date_registered','>=', date('Y-m-d').' 00:00:00')->whereDate('created_at','<=', date('Y-m-d').' 23:59:59')->count();
        $total    = $paid + $cd + $free;
        return view('BackOffice.profile-management.member-list',compact('title','total','paid','cd','free','floating','today'));
    }


    public function show_data(Request $request) {
        
                $columns = array( 
                    0  => 'created_at',
                    1  => 'created_at',
                    2  => 'username',
                    3  => 'name',
                    4  => 'sponsor',
                    5  => 'upline',
                    6  => 'position',
                    7  => 'type',
                    8  => 'package',
                    9  => 'country',
                    10 => 'action',
                );

                $totalData = User::count();

                $totalFiltered = $totalData; 

                $limit = $request->input('length');
                $start = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $dir   = $request->input('order.0.dir');

                if(empty($request->input('search.value')))
                {            
                    $posts  = DB::table('users')->select('users.firstname','users.surname','users.middlename','users.id as users_id','users.id_number','users.username','users.created_at','user_network_structure.sponsor_id','user_network_structure.upline_id','user_network_structure.position','user_network_structure.status_type','user_network_structure.country','control_panel_entries.entry','users.date_registered as created_at')
                    ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                    ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                    ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                    ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('users.date_registered','DESC')
                    ->get();

               
                } else {
                $search = $request->input('search.value'); 
                $posts = DB::table('users')->select('users.firstname','users.surname','users.middlename','users.id as users_id','users.id_number','users.username','users.created_at','user_network_structure.sponsor_id','user_network_structure.upline_id','user_network_structure.position','user_network_structure.status_type','user_network_structure.country','control_panel_entries.entry','users.created_at')
                ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                ->orWhere('users.username', 'LIKE',"%{$search}%")
                ->orWhere('users.firstname', 'LIKE',"%{$search}%")
                ->orWhere('users.middlename', 'LIKE',"%{$search}%")
                ->orWhere('users.surname', 'LIKE',"%{$search}%")
                ->orWhere('users.id_number', 'LIKE',"%{$search}%")
                ->orWhere('user_network_structure.sponsor_id', 'LIKE',"%{$search}%")
                ->orWhere('user_network_structure.upline_id', 'LIKE',"%{$search}%")
                ->orWhere('status_type', 'LIKE',"%{$search}%")
                ->orWhere('control_panel_entries.entry', 'LIKE',"%{$search}%")
                ->orWhere('user_network_structure.country', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
                
                
                $totalFiltered = DB::table('users')->select('users.firstname','users.surname','users.middlename','users.id as users_id','users.id_number','users.username','users.created_at','user_network_structure.sponsor_id','user_network_structure.upline_id','user_network_structure.position','user_network_structure.status_type','user_network_structure.country','control_panel_entries.entry','users.created_at')
                    ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
                    ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
                    ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
                    ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
                    ->orWhere('users.username', 'LIKE',"%{$search}%")
                    ->orWhere('users.firstname', 'LIKE',"%{$search}%")
                    ->orWhere('users.middlename', 'LIKE',"%{$search}%")
                    ->orWhere('users.surname', 'LIKE',"%{$search}%")
                    ->orWhere('users.id_number', 'LIKE',"%{$search}%")
                    ->orWhere('user_network_structure.sponsor_id', 'LIKE',"%{$search}%")
                    ->orWhere('user_network_structure.upline_id', 'LIKE',"%{$search}%")
                    ->orWhere('status_type', 'LIKE',"%{$search}%")
                    ->orWhere('control_panel_entries.entry', 'LIKE',"%{$search}%")
                    ->orWhere('user_network_structure.country', 'LIKE',"%{$search}%")
                    ->count();
                }

                $data = array();
                if(!empty($posts)) {
                    $i=1;
                    foreach ($posts as $post) {
                        

                        if($post->status_type == 0) {
                            $status_type = 'PAID';
                        } elseif($post->status_type == 1) {
                            $status_type = 'CD';
                        } elseif($post->status_type == 2) {
                            $status_type = 'FREE';
                        } elseif($post->status_type == 3) {
                            $status_type = 'FLOATING';
                        }

                        if($post->position == 2) {
                            $position = 'TOP';
                        } else {
                            $position = $post->position == 0  ? 'LEFT' : 'RIGHT';
                        }
                        $nestedData['id']           = $i++;
                        $nestedData['joined_date']  = date('Y-m-d h:i:s',strtotime($post->created_at));
                        $nestedData['username']     = $post->username.'  <a href="#">'.$post->id_number.'</a>';
                        $nestedData['name']         = $post->firstname.' '.$post->middlename.' '.$post->surname;
                        $nestedData['sponsor']      = $post->sponsor_id;
                        $nestedData['upline']       = $post->upline_id;
                        $nestedData['position']     = $position;
                        $nestedData['type']         = $status_type;
                        $nestedData['package']      = $post->entry;
                        $nestedData['country']      = $post->country;
                        $nestedData['action']       = '<div><a class="list-icons-item" href="javascript:void(0)" data-toggle="dropdown"><i class="icon-gear"></i></a><div class="dropdown-menu">
                            <a href="'.url('backoffice/profile-management/members/'.Crypt::encryptString($post->id_number)).'"
                            class="dropdown-item"><i class="icon-eye"></i> Distributor
                            Details</a>
                        </div>';
                        
                        $data[] = $nestedData;

                    }
                }

                $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                );


        return  json_encode($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $title = 'Member List';

        
        $users = DB::table('users')->select('*','users.id as users_id','countries.name as country_name')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('countries', 'countries.code', '=', 'user_network_structure.country')
        ->leftjoin('users_details', 'users_details.users_id', '=', 'users.id')
        ->leftjoin('users_contacts', 'users_contacts.users_id', '=', 'users.id')
        ->leftjoin('users_bank_accounts', 'users_bank_accounts.users_id', '=', 'users.id')
        ->leftjoin('users_beneficiaries', 'users_beneficiaries.users_id', '=', 'users.id')
        ->leftjoin('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
        ->where(['users.id_number' => Crypt::decryptString($id)])
        ->get();

        $ranks = Ranks::where(['id' => $users[0]->ranks_id])->first();

        $social_media_accounts = UserSocialMediaAccounts::where(['users_id' => $users[0]->id])->get();


        $details = DB::table('registration_codes')->select('*')
        ->join('control_panel_packages', 'registration_codes.packages_id', '=', 'control_panel_packages.id')
        ->join('control_panel_entries', 'control_panel_packages.entries_id', '=', 'control_panel_entries.id')
        ->where(['registration_codes.used_by' => Crypt::decryptString($id)])
        ->get();

        $sponsor_id = $users[0]->sponsor_id;

        $data = DB::table('user_network_structure')->select('*')
        ->join('users', 'users.id', '=', 'user_network_structure.users_id')
        ->where(['user_network_structure.sponsor_id' => $sponsor_id])
        ->get();

        $pins_history = DB::table('registration_codes')
        ->select('registration_codes.*',DB::raw("SUM(registration_codes.price) as total"),DB::raw("count(registration_codes.order_number) as items"),'control_panel_packages.id','control_panel_packages.entries_id','control_panel_packages.package_name','control_panel_entries.id','control_panel_entries.countries_id','control_panel_entries.entry','countries.id','countries.name','countries.code','countries.currency_code','countries.currency_name','countries.currency_symbol')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->join('countries', 'countries.id', '=', 'control_panel_entries.countries_id')
        ->whereNotIn('registration_codes.status',[2])
        ->where('registration_codes.distributor_id',Crypt::decryptString($id))
        ->groupBy("registration_codes.customer_id")
        ->get();

        $tophead = User::count();

        $countries = Country::where(['is_active' => 0])->get();

        $increment_username = UserNetworkStructure::where(['sponsor_id' => Crypt::decryptString($id)])->count() + 1;
        $increment = '-'.str_pad($increment_username, 3, "0", STR_PAD_LEFT);
        $sub_accounts =  DB::table('users')->select('*')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->where(['group_code' => $users[0]->group_code])
        ->whereNotIn('upline_id',['0000000'])
        ->get();

        $social_media = SocialMedia::where(['social_media_status' => 0])->get();
        $banks = Banks::where(['banks_status' => 0])->get();

        $product_pins = DB::table('product_pin_codes')->select('product_pin_codes.*','product_pin_codes.id as pin_id','product_description.*','product_packages.id as package_id','product_packages.product_title')
        ->join('product_description', 'product_description.id', '=', 'product_pin_codes.product_description_id')
        ->join('product_packages', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->where(['product_pin_codes.distributor_id' => Crypt::decryptString($id)])
        ->groupBy('order_number')
        ->whereNotIn('product_pin_codes.status',[2])
        ->get();

        if(count($product_pins) > 0) {
            foreach($product_pins as $prods) {
                $total[$prods->pin_id] = DB::table('product_pin_codes')->select(DB::raw("SUM(price) as total"))->where(['customer_id' => $prods->customer_id])->first();
                $items[$prods->pin_id] = DB::table('product_pin_codes')->select(DB::raw("COUNT(order_number) as items"))->where(['customer_id' => $prods->customer_id])->first();
            }
        } else {
            $product_pins = [];
            $total = 0;
            $items = 0;
        }


        $wallets     = EWallet::where(['id_number' => $users[0]->id_number])->orderBy('created_at','DESC')->get();
        $wallet_logs = EWalletsLogs::where(['sender_id' => $users[0]->id_number])->orderBy('created_at','DESC')->get();


        return view('BackOffice.profile-management.member-overview',compact('title','users','details','data','pins_history','tophead','countries','increment','sub_accounts','social_media','banks','social_media_accounts','product_pins','wallets','wallet_logs','ranks','total','items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //USer
        $surname         = $request->surname;
        $firstname       = $request->firstname;
        $middlename      = $request->middlename;
        $email           = $request->email;
        $username        = $request->username;
        $password        = Crypt::encryptString($request->password);
        $status          = $request->status;
        //User Details
        $nickname        = $request->nickname;
        $birthdate       = $request->birthdate;
        $civil_status    = $request->civil_status;
        $gender          = $request->gender;
        $nationality     = $request->nationality;
        $province        = $request->province;
        $city            = $request->city;
        $address         = $request->address;
        $zipcode         = $request->zipcode;
        $tin             = $request->tin;
        //User Contacts
        $contact_number  = $request->contact_number;
        $landline        = $request->landline;
        $fax             = $request->fax;
        //User Beneficiaries
        $name            = $request->name;
        $relationship    = $request->relationship;
        //User Bank Details
        $banks_id        = Crypt::decryptString($request->banks_id);
        $branch          = $request->branch;
        $account_name    = $request->account_name;
        $account_number  = $request->account_number;
        //User Social Media Accounts
        $social_media_id = $request->social_media_id;
        $url             = $request->url;

        $users_id         = Crypt::decryptString($id);
        $user            = User::where(['id' => $users_id])->get();
        $userQuery       = User::where(['id' => $users_id])->update([
            'surname'       => $surname,
            'firstname'     => $firstname, 
            'middlename'    => $middlename,
            'email'         => $email,
            'username'      => $username,
            'password'      => $password,
            'status'        => $status,
        ]);
        if($userQuery) {
            $userDetailsQuery   = UserDetails::where(['users_id' => $users_id])->update([
                'nickname'      => $nickname,
                'birthdate'     => $birthdate,
                'civil_status'  => $civil_status,
                'gender'        => $gender,
                'nationality'   => $nationality,
                'province'      => $province,
                'city'          => $city,
                'address'       => $address,
                'zipcode'       => $zipcode,
                'tin'           => $tin,
            ]);

            if($userDetailsQuery) {
                $UserContactsQuery      = UserContacts::where(['users_id' => $users_id])->update([
                    'contact_number'    => $contact_number,
                    'landline'          => $landline,
                    'fax'               => $fax,
                ]);
            }

            if($UserContactsQuery) {
                $UserBeneficiaryQuery   = UserBeneficiary::where(['users_id' => $users_id])->update([
                    'name'              => $name,
                    'relationship'      => $relationship,
                ]);
            }

            if($UserBeneficiaryQuery) {
                $BankAccountsQuery   = UserBankAccounts::where(['users_id' => $users_id])->update([
                    'banks_id'          => $banks_id,
                    'branch'            => $branch,
                    'account_name'      => $account_name,
                    'account_number'    => $account_number,
                ]);
            }

            if($BankAccountsQuery) {

                for($x=0; $x<count($social_media_id); $x++) {
                    $media_id   = Crypt::decryptString($social_media_id[$x]);
                    $media_url  = $url[$x];
                    UserSocialMediaAccounts::where(['users_id' => $users_id,'social_media_id' => $media_id])->update([
                        'social_media_id'   => $media_id,
                        'url'               => $media_url,
                    ]);
                }
            }
        }
        Session::flash('message', 'Distributor information has been updated');
        return redirect('backoffice/profile-management/members/'.Crypt::encryptString($user[0]->id_number));
        
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
