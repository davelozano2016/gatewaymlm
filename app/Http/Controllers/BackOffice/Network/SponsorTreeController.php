<?php

namespace App\Http\Controllers\BackOffice\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Crypt;
class SponsorTreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Sponsor Tree';
        $date = Carbon::now();

        $users = DB::table('users')->select('*')
        ->join('binary_points', 'binary_points.id_number', '=', 'users.id_number')
        ->join('user_network_structure', 'users.id', '=', 'user_network_structure.users_id')
        ->join('registration_codes', 'user_network_structure.activation_code', '=', 'registration_codes.activation_code')
        ->join('control_panel_packages', 'control_panel_packages.id', '=', 'registration_codes.packages_id')
        ->join('control_panel_entries', 'control_panel_entries.id', '=', 'control_panel_packages.entries_id')
        ->limit(1)->get();

        

        if(count($users) > 0) {
            $unilevel_level_1 = DB::table('binary_points')->select('*')
            ->join('users', 'binary_points.id_number', '=', 'users.id_number')
            ->where(['binary_points.level' => 1,'binary_points.id_number' => $users[0]->id_number])
            ->get();

            foreach($unilevel_level_1 as $level_1) {
                $unilevel_level_2[$level_1->waiting_id_number] = DB::table('binary_points')->select('*')
                ->join('users', 'binary_points.id_number', '=', 'users.id_number')
                ->where(['binary_points.level' => 1,'binary_points.id_number' => $level_1->waiting_id_number])
                ->get();
            }
        } else {
            $unilevel_level_1 = [];
            $unilevel_level_2 = [];
        }


        return view('BackOffice.network.sponsor-tree',compact('title','date','users','unilevel_level_1','unilevel_level_2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
