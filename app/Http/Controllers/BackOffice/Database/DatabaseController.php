<?php

namespace App\Http\Controllers\BackOffice\Database;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserNetworkStructure;

class DatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $url = 'https://keivirtual.com/keiwwcpanel888/migration.php';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $array = curl_exec($curl);
        curl_close($curl);
        $query = json_decode($array);

        foreach($query as $data) {
            $id_number            = $data->id_number;
            $surname              = $data->lastname;
            $firstname            = $data->firstname;
            $middlename           = $data->middlename;
            $email                = $data->email;
            $username             = $data->username;
            $password             = Crypt::encryptString(12345678);
            $country              = $data->country;
            $sponsor_id           = $data->sponsor_code;
            $upline_id            = $data->upline_code;
            $position             = $data->upline_pos == 'R' ? 0 : 1;
            $group_code           = Crypt::encryptString($id_number);
            $query = User::create([
                'id_number'       => $id_number,
                'surname'         => $surname,
                'firstname'       => $firstname,
                'middlename'      => $middlename,
                'email'           => $email,
                'username'        => $username,
                'password'        => $password,
            ]);
            if($query) {
                $users = User::all();
                foreach($users as $r) {
                    $network              = UserNetworkStructure::create([
                        'users_id'        => $r->id,
                        'country'         => $country,
                        'sponsor_id'      => $sponsor_id,
                        'upline_id'       => $upline_id,
                        'position'        => $position,
                        'activation_code' => 'P85209P87052PH',
                        'status_type'     => 0,
                        'group_code'      => $group_code,
                        'account_type'    => 0,
                    ]);
                }
                
            }
            
        }
        exit;
        $title = 'Database';
        return view('BackOffice.database.database',compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
