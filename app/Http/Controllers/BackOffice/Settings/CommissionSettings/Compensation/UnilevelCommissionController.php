<?php

namespace App\Http\Controllers\BackOffice\Settings\CommissionSettings\Compensation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ControlPanelUnilevel;
use App\ControlPanelUnilevelSettings;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UnilevelCommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Unilevel Commission';
        $unilevel = ControlPanelUnilevel::all();
        $settings = ControlPanelUnilevelSettings::first();
        return view('BackOffice.settings.commission-settings.compensation.unilevel-commission',compact('title','unilevel','settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        ControlPanelUnilevelSettings::where('id',1)->update(['maintenance' => $request->maintenance,'unilevel_count' => $request->unilevel_count]);
        for($i=0;$i<count($request->unilevel_id);$i++) {
            $id = $request->unilevel_id[$i];
            $level = $request->level[$i];
            $query = ControlPanelUnilevel::where(['id' => $id])->count();
            if($query > 0) {
                ControlPanelUnilevel::where(['id' => $id])->update(['percentage' => $request->percentage[$i]]);
            } else {
                ControlPanelUnilevel::create(['level' => $level, 'percentage' => $request->percentage[$i]]);
            }
        }

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
