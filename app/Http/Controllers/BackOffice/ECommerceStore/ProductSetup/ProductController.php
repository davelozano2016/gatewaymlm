<?php

namespace App\Http\Controllers\BackOffice\EcommerceStore\ProductSetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\ProductCategory;
use App\ProductPackages;
use App\ProductDescription;
use App\ProductSupplier;
use App\User;
use App\ProductPinCodes;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Product';
        $products = DB::table('product_packages')->select('*','product_packages.id as product_id')
        ->join('product_description', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->groupBy(['product_packages.product_code'])
        ->get();
        return view('BackOffice.e-commerce-store.product-setup.products',compact('title','products'));
    }

    public function new_product() {
        $title = 'Product';
        $countries  = Country::where(['is_active' => 0])->get();
        $categories = ProductCategory::all();
        $suppliers  = ProductSupplier::all();
        return view('BackOffice.e-commerce-store.product-setup.new-product',compact('title','countries','categories','suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //

        $id                         = Crypt::decryptString($request->distributor_id);
        $query                      = User::where(['id_number' => $id])->get();
        $id_number                  = $query[0]->id_number;
        $full_name                  = $query[0]->firstname.' '.$query[0]->surname;
        $order_number               = 'OR'.rand(111111,999999).date('Ymd');
        $customer_id                = 'CUST-'.rand(1111,9999).date('Ymd');
        $ppid                       = $request->product_description_id;
        for($x=0;$x<count($ppid);$x++) {
            $product_description_id = $request->product_description_id[$x];
            $product_title          = $request->product_title[$x];
            $product_code           = $request->product_code[$x];
            $quantity               = $request->quantity[$x];
            $unilevel_point_value   = $request->upv[$x];
            $binary_point_value     = $request->bpv[$x];
            $distributor_price      = $request->price[$x];
            $country_code           = $request->country_code[$x];
            $created_by             = $full_name;
            $registered_ip          = \Request::ip();
            $date_processed         = date('Y-m-d h:i:s');
            $date_expiration        = date('Y-m-d h:i:s',strtotime('+1 month'));
            $date_used              = '0000-00-00 00:00:00';
            $date_created           = date('Y-m-d h:i:s');
            $words                  = explode(" ",$product_title);
            $acronym                = "";
            foreach ($words as $w) { $acronym .= $w[0]; }
            for($i=1;$i<=$quantity;$i++) {
                $activation_code = $acronym.rand(111111,999999).$product_code.date('Ymd');
                ProductPinCodes::create([
                    'distributor_id'         => $id_number,
                    'product_description_id' => $product_description_id,
                    'customer_id'            => $customer_id,
                    'full_name'              => $full_name,
                    'activation_code'        => $activation_code,
                    'order_number'           => $order_number,
                    'price'                  => $distributor_price,
                    'order_status'           => 'Completed',
                    'unilevel_point_value'   => $unilevel_point_value,
                    'binary_point_value'     => $binary_point_value,
                    'country_code'           => $country_code,
                    'created_by'             => $created_by,
                    'registered_ip'          => $registered_ip,
                    'date_processed'         => $date_processed,
                    'date_expiration'        => $date_expiration,
                    'date_used'              => $date_used,
                    'date_created'           => $date_created
                ]);
            }
        }
        
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $facebook_thumbnail         = $request->file('facebook_thumbnail')->store('/facebook');
        $product_image              = $request->file('product_image')->store('/products');
        $query = ProductPackages::create([
            'product_title'         => $request->product_title,
            'product_code'          => $request->product_code,
            'product_category_id'   => Crypt::decryptString($request->product_category_id),
            'product_supplier_id'   => Crypt::decryptString($request->product_supplier_id),
            'binary_points'         => $request->binary_points,
            'unilevel_points'       => $request->unilevel_points,
            'net_weight_per_gram'   => $request->net_weight_per_gram,
            'facebook_thumbnail'    => $facebook_thumbnail,
            'product_image'         => $product_image,
            'product_description'   => $request->product_description,
            'product_miscellaneous' => $request->product_miscellaneous,
            'product_status'        => $request->product_status,
            'product_stocks'        => $request->product_stocks
        ]);

        for($i=0;$i<count($request->countries_id);$i++) {
            $product_packages_id        = $query->id;
            $discount_type              = $request->discount_type;
            $countries_id               = Crypt::decryptString($request->countries_id[$i]);
            $country                    = Country::where(['id' => $countries_id])->get();
            $distributor_price          = $request->distributor_price[$i];
            $srp                        = $request->srp[$i];
            $retailer                   = $request->retailer[$i];
            $stockist_discount          = $request->stockist_discount[$i];
            $depot_discount             = $request->depot_discount[$i];
            $country_manager_discount   = $request->country_manager_discount[$i];
            ProductDescription::create([
                'product_packages_id'       => $product_packages_id,
                'country'                   => $country[0]->code,
                'distributor_price'         => $distributor_price,
                'srp'                       => $srp,
                'retailer'                  => $retailer,        
                'discount_type'             => $discount_type,
                'stockist_discount'         => $stockist_discount,
                'depot_discount'            => $depot_discount,
                'country_manager_discount'  => $country_manager_discount,
            ]);
        }
        return back()->with('message','New product has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $title = 'Product';
        $countries  = Country::where(['is_active' => 0])->get();
        $products = DB::table('product_packages')->select('*','product_packages.id as product_id')
        ->join('product_description', 'product_packages.id', '=', 'product_description.product_packages_id')
        ->join('product_categories', 'product_categories.id', '=', 'product_packages.product_category_id')
        ->join('countries', 'countries.code', '=', 'product_description.country')
        ->join('product_supplier', 'product_supplier.id', '=', 'product_packages.product_supplier_id')
        ->where(['product_packages.id' => Crypt::decryptString($id)])
        ->first();
        $categories = DB::table('product_categories')->get();
        $suppliers = DB::table('product_supplier')->get();
        return view('BackOffice.e-commerce-store.product-setup.visit-product',compact('title','products','countries','categories','suppliers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
