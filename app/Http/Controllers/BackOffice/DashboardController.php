<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Booking;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Dashboard';
        return view('BackOffice.dashboard',compact('title'));
    }

    public function distributor($id_number) {

        $users = DB::table('users')->select('*')
        ->join('user_ewallets', 'user_ewallets.users_id', '=', 'users.id')
        ->join('markups', 'markups.users_id', '=', 'users.id')
        ->where(['users.id_number' => $id_number])
        ->get();
        return $users->toJson();
    }

    public function validate_wallet($id_number) {

        $users = DB::table('users')->select('*')->where(['id_number' => $id_number])->first();
        $users_id = $users->id;
        $wallet = DB::table('user_ewallets')->where(['users_id' => $users_id])->first();
        $query = ['fund' => $wallet->fund];
        return json_encode($query);
    }


    public function booking(Request $request) {
        $id_number      = $request->id_number;
        $pnr            = $request->pnr;
        $flight_number  = $request->flight_number;
        $amount         = $request->amount;
        $markup         = $request->markup;
        $encryptPNR     = '<a href="https://gatewaybackoffice.com/virtualoffice/booking/booking-history/'.Crypt::encryptString($pnr).'">'.$flight_number.'</a>';
        $users = DB::table('users')->where(['id_number' => $id_number])->first();
        

        DB::table('user_ewallets')->where(['users_id' => $users->id])->update(['fund'=>DB::raw("fund-$amount")]);
        $wallet = DB::table('user_ewallets')->where(['users_id' => $users->id])->first();
        DB::table('e_wallets_logs')->insert([
            'sender_id'         => $id_number,
            'receiver_id'       => $id_number,
            'category'          => 'Flight Booking',
            'description'       => "Booking has been completed with Flight Booking Number ".$encryptPNR,
            'status'            => 1, 
            'value'             => $amount,
            'reference'         => 'EB'.$flight_number.$pnr,
            'current_balance'   => $wallet->fund,
            'created_at'        => date('Y-m-d h:i:s'),
            'updated_at'        => date('Y-m-d h:i:s')
        ]); 
        DB::table('bonuses')->insert([
            'recipient'  => $id_number,
            'source'     => $id_number,
            'bonus_type' => 'Markup'.$pnr,
            'amount'     => $markup,
            'cycle'      => 2,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('user_accounts')->where(['users_id' => $users->id])->update(['total_current_income'=>DB::raw("total_current_income+$markup")]);

        Booking::create(['id_number' => $id_number, 'pnr' => $pnr, 'flight_number' => $flight_number,'amount' => $amount,'markup' => $markup]);
        $query = ['success' => true,'message' => 'Success booking with flight number '.$flight_number];
        return json_encode($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
