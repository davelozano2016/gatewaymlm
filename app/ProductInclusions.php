<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInclusions extends Model
{
    //
    protected $table = 'product_inclusions';
    protected $primaryKey = 'id';
    protected $fillable = ['unique_code','product_package_id','quantity'];

}
