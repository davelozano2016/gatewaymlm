<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    //

    protected $table = 'social_media';
    protected $primaryKey = 'id';
    protected $fillable = ['social_media_name','social_media_logo','social_media_status'];

}
