<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encashment extends Model
{
    //
    protected $table = 'encashments';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','request_type','request_bonus','reference','country_code','ip_address','payout','status'];
}
