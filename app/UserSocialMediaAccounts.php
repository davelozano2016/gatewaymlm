<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialMediaAccounts extends Model
{
    //
    protected $table = 'users_social_media_accounts';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','social_media_id','url'];
}
