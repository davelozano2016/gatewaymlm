<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EWallet extends Model
{
    //

    protected $table = 'e_wallets';
    protected $primaryKey = 'id';
    protected $fillable = ['id_number','amount','notes','deposit_via','upload_receipt','status'];
    
}
