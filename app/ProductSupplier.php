<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSupplier extends Model
{
    //
    protected $table = 'product_supplier';
    protected $primaryKey = 'id';
    protected $fillable = ['supplier_code','supplier_name'];

}
