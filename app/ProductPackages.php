<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPackages extends Model
{
    //
    protected $table = 'product_packages';
    protected $primaryKey = 'id';

    protected $fillable = ['product_title','product_code','product_category_id','product_supplier_id','binary_points','unilevel_points','net_weight_per_gram','facebook_thumbnail','product_image','product_description','product_miscellaneous','product_status','product_stocks'];

}
