<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcpayTopup extends Model
{
    //
    protected $table = 'ecpay_topup';
    protected $primaryKey = 'id';
    protected $fillable = ['TelcoTag','TelcoName','Denomination','ExtTag','Description'];

}
