<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContacts extends Model
{
    //

    protected $table = 'users_contacts';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','contact_number','landline','fax'];

}
