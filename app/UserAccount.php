<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    //
    protected $table = 'user_accounts';
    protected $primaryKey = 'id';
    protected $fillable = ['users_id','total_current_income','total_overall_income','total_point_value','total_gc'];
}
