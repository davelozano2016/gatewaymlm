<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    //

    protected $table = 'user_levels';
    protected $primaryKey = 'id';
    protected $fillable = ['user_level','user_level_status'];


}
