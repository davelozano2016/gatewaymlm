/* ------------------------------------------------------------------------------
 *
 *  # Google Visualization - lines
 *
 *  Google Visualization line chart demonstration
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var GoogleLineBasic = function() {


    //
    // Setup module components
    //

    // Line chart
    var _googleLineBasic = function() {
        if (typeof google == 'undefined') {
            console.warn('Warning - Google Charts library is not loaded.');
            return;
        }

        // Initialize chart
        google.charts.load('current', {
            callback: function () {

                // Draw chart
                drawLineChart();

                // Resize on sidebar width change
                var sidebarToggle = document.querySelector('.sidebar-control');
                sidebarToggle && sidebarToggle.addEventListener('click', drawLineChart);

                // Resize on window resize
                var resizeLineBasic;
                window.addEventListener('resize', function() {
                    clearTimeout(resizeLineBasic);
                    resizeLineBasic = setTimeout(function () {
                        drawLineChart();
                    }, 200);
                });
            },
            packages: ['corechart']
        });

        // Chart settings
        function drawLineChart() {

            // Define charts element
            var line_chart_element = document.getElementById('app_sales');

            // Data
            var data = google.visualization.arrayToDataTable([
                ['Year', 'MONTHLY', 'WEEKLY','DAILY'],
                ['Feb 2020',  50, 57,51],
                ['Mar 2020',  50, 28,21],
                ['Apr 2020',  25, 20,21],
                ['May 2020',  30, 22,21],
                ['Jun 2020',  10, 23,21],
                ['Jul 2020',  60, 69,61],
                ['Aug 2020',  50, 26,21],
                ['Sep 2020',  50, 58,51],
                ['Oct 2020',  50, 57,51],
                ['Nov 2020',  50, 58,51],
                ['Dec 2020',  50, 59,51],
                ['Jan 2021',  50, 60,51],
            ]);

            // Options
            var options = {
                fontName: 'Roboto',
                height: 325,
                fontSize: 12,
                chartArea: {
                    left: '5%',
                    width: '94%',
                    height: 270
                },
                pointSize: 7,
                curveType: 'function',
                backgroundColor: 'transparent',
                tooltip: {
                    textStyle: {
                        fontName: 'Roboto',
                        fontSize: 13
                    }
                },
                vAxis: {
                    title: '',
                    titleTextStyle: {
                        fontSize: 13,
                        italic: false,
                        color: '#333'
                    },
                    textStyle: {
                        color: '#333'
                    },
                    baselineColor: '#ccc',
                    gridlines:{
                        color: '#eee',
                        count: 10
                    },
                    minValue: 0
                },
                hAxis: {
                    textStyle: {
                        color: '#333'
                    }
                },
                legend: {
                    position: 'top',
                    alignment: 'center',
                    textStyle: {
                        color: '#333'
                    }
                },
                series: {
                    0: { color: '#EF5350' },
                    1: { color: '#66BB6A' }
                }
            };

            // Draw chart
            var line_chart = new google.visualization.LineChart(line_chart_element);
            line_chart.draw(data, options);
        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _googleLineBasic();
        }
    }
}();


// Initialize module
// ------------------------------

GoogleLineBasic.init();

