<?php

use Illuminate\Support\Facades\Route;
use App\Http\Resources\DownLines AS DownLinesResource;
use App\Session;
/*


|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Guests
Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index');
Route::get('/register', 'RegisterController@index');

Route::get('/logout',function() {
    session()->flush();
    return redirect('/');
});
// Administrator 
Route::get('backoffice/login', 'BackOffice\LoginController@index');
Route::get('backoffice/dashboard', 'BackOffice\DashboardController@index');
Route::get('backoffice/total-distributors', 'BackOffice\DashboardController@total_distributors');
Route::get('backoffice/e-commerce-store', 'BackOffice\ECommerceStoreController@index');
Route::get('backoffice/store-administration', 'BackOffice\StoreAdministrationController@index');
Route::get('backoffice/business', 'BackOffice\BusinessController@index');
Route::get('backoffice/e-wallet', 'BackOffice\EWalletController@index');

Route::get('backoffice/distributor/{id_number}', 'BackOffice\DashboardController@distributor');
Route::get('backoffice/booking/{id_number}/{pnr}/{flight_number}/{amount}/{markup}', 'BackOffice\DashboardController@booking');
Route::get('backoffice/booking/validate-wallet/{id_number}', 'BackOffice\DashboardController@validate_wallet');


Route::get('backoffice/database', 'BackOffice\Database\DatabaseController@index');
Route::get('backoffice/pairing', 'BackOffice\CronController@doPairing');


Route::get('backoffice/payout', 'BackOffice\PayoutController@index');
Route::prefix('backoffice/payout')->group(function () {
    Route::post('/search', 'BackOffice\PayoutController@search');
    Route::get('/cancel/{id}', 'BackOffice\PayoutController@destroy');
    Route::get('/process/{id}', 'BackOffice\PayoutController@process');
    Route::get('/voucher/{reference_code}', 'BackOffice\PayoutController@voucher');
    Route::get('/details/{reference_code}', 'BackOffice\PayoutController@details');
    Route::get('/multi-process', 'BackOffice\PayoutController@multi_process');
});



Route::post('backoffice/search', 'BackOffice\RegisterController@search');

Route::post('backoffice/e-wallet/update/{wallet_id}', 'BackOffice\EWalletController@update');


Route::post('backoffice/login/secureLogin', 'BackOffice\LoginController@secureLogin');


Route::prefix('backoffice')->group(function () {
    Route::get('/register', 'BackOffice\RegisterController@index');
    Route::get('/register/sub-account/{sponsor_id}/{id_number}/{position}', 'BackOffice\RegisterController@create_sub_account');
    Route::get('/register/main-account/{sponsor_id}/{id_number}/{position}', 'BackOffice\RegisterController@create_main_account');

    Route::post('/register/store', 'BackOffice\RegisterController@store');
    Route::post('/register/store_sub_account', 'BackOffice\RegisterController@store_sub_account');
});



Route::prefix('backoffice/mailbox')->group(function () {
    Route::get('/inbox', 'BackOffice\Mailbox\InboxController@index');
    Route::get('/compose', 'BackOffice\Mailbox\ComposeMailController@index');
    Route::get('/sent-mail', 'BackOffice\Mailbox\SentItemsController@index');
    Route::get('/trash', 'BackOffice\Mailbox\TrashMailController@index');
    Route::get('/mail-read', 'BackOffice\Mailbox\MailReadController@index');
    Route::get('/reply-mail', 'BackOffice\Mailbox\ReplyMailController@index');
});

Route::prefix('backoffice/network')->group(function () {
    Route::get('/binary', 'BackOffice\Network\BinaryController@index');
    Route::get('/genealogy-tree', 'BackOffice\Network\GenealogyTreeController@index');
    Route::get('/sponsor-tree', 'BackOffice\Network\SponsorTreeController@index');
    Route::get('/tree-view', 'BackOffice\Network\TreeViewController@index');
    Route::get('/downline-members', 'BackOffice\Network\DownlineMembersController@index');
    Route::get('/referral-members', 'BackOffice\Network\ReferralMembersController@index');
    Route::get('/genealogy/new-account', 'BackOffice\Network\NewAccountController@index');
    Route::get('/genealogy/demo-account', 'BackOffice\Network\DemoAccountController@index');

    Route::get('/genealogy-tree/visit/{id_number}', 'BackOffice\Network\GenealogyTreeController@visit');
    Route::post('/genealogy-tree/result', 'BackOffice\Network\GenealogyTreeController@result');
    Route::post('/genealogy-tree/processing', 'BackOffice\Network\GenealogyTreeController@processing');
    
});

Route::prefix('backoffice/order-details')->group(function () {
    Route::get('/order-approval', 'BackOffice\OrderDetails\OrderApprovalController@index');
    Route::get('/order-history', 'BackOffice\OrderDetails\OrderHistoryController@index');
    Route::get('/track-order/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@track');
    Route::get('/view-orders/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@view_orders');
    Route::get('/package-track-order/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@package_track');
    Route::get('/package-view-orders/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@package_view_orders');
    Route::get('/edit-order-details/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@edit_orders');
    Route::get('/package-edit-order-details/{reference}', 'BackOffice\OrderDetails\OrderHistoryController@package_edit_orders');
    Route::post('/edit-order-details/update', 'BackOffice\OrderDetails\OrderHistoryController@update');
    Route::post('/package-edit-order-details/update', 'BackOffice\OrderDetails\OrderHistoryController@package_update');
});

Route::prefix('backoffice/settings')->group(function () {
    Route::get('/commission-settings', 'BackOffice\Settings\CommissionSettingsController@index');
    Route::get('/advanced-settings', 'BackOffice\Settings\AdvancedSettingsController@index');
    Route::get('/company-profile', 'BackOffice\Settings\CompanyProfileController@index');
    Route::get('/content-management', 'BackOffice\Settings\ContentManagementController@index');
    Route::get('/mail-content', 'BackOffice\Settings\MailContentController@index');
    Route::get('/sms-content', 'BackOffice\Settings\SMSContentController@index');
    Route::get('/products/auto-order', 'BackOffice\Settings\SMSContentController@index');
});


Route::prefix('backoffice/settings')->group(function () {
    Route::get('/commission-settings', 'BackOffice\Settings\CommissionSettingsController@index');
    Route::get('/advanced-settings', 'BackOffice\Settings\AdvancedSettingsController@index');
    Route::get('/company-profile', 'BackOffice\Settings\CompanyProfileController@index');
    Route::get('/content-management', 'BackOffice\Settings\ContentManagementController@index');
    Route::get('/mail-content', 'BackOffice\Settings\MailContentController@index');
    Route::get('/sms-content', 'BackOffice\Settings\SMSContentController@index');

    Route::get('/commission-settings/compensation/binary-commission', 'BackOffice\Settings\CommissionSettings\Compensation\BinaryCommissionController@index');
    Route::get('/commission-settings/compensation/unilevel-commission', 'BackOffice\Settings\CommissionSettings\Compensation\UnilevelCommissionController@index');
    Route::get('/commission-settings/compensation/referral-commission', 'BackOffice\Settings\CommissionSettings\Compensation\ReferralCommissionController@index');
    Route::get('/commission-settings/compensation/repurchase-sales-commission', 'BackOffice\Settings\CommissionSettings\Compensation\RepurchaseSalesCommissionController@index');
    Route::get('/commission-settings/compensation/rank-commission', 'BackOffice\Settings\CommissionSettings\Compensation\RankCommissionController@index');
    Route::get('/commission-settings/compensation/pool-bonus', 'BackOffice\Settings\CommissionSettings\Compensation\PoolBonusController@index');
    Route::get('/commission-settings/compensation/performance-bonus', 'BackOffice\Settings\CommissionSettings\Compensation\PerformanceBonusController@index');
    Route::get('/commission-settings/compensation/matching-bonus', 'BackOffice\Settings\CommissionSettings\Compensation\MatchingBonusController@index');
    Route::get('/commission-settings/compensation/fast-start-bonus', 'BackOffice\Settings\CommissionSettings\Compensation\FastStartBonusController@index');

    Route::get('/commission-settings/commission', 'BackOffice\Settings\CommissionSettings\CommissionController@index');
    Route::get('/commission-settings/compensation', 'BackOffice\Settings\CommissionSettings\CompensationController@index');
    Route::get('/commission-settings/rank', 'BackOffice\Settings\CommissionSettings\RankController@index');
    Route::get('/commission-settings/payout', 'BackOffice\Settings\CommissionSettings\PayoutController@index');
    Route::get('/commission-settings/payment', 'BackOffice\Settings\CommissionSettings\PaymentController@index');
    Route::get('/commission-settings/signup', 'BackOffice\Settings\CommissionSettings\SignupController@index');
    Route::get('/commission-settings/mail', 'BackOffice\Settings\CommissionSettings\MailController@index');
    Route::get('/commission-settings/api-key', 'BackOffice\Settings\CommissionSettings\APIKeyController@index');
    Route::get('/commission-settings/currency', 'BackOffice\Settings\CommissionSettings\CurrencyController@index');

    Route::get('/advanced-settings/profile', 'BackOffice\Settings\AdvancedSettings\ProfileController@index');
    Route::get('/advanced-settings/conversion', 'BackOffice\Settings\AdvancedSettings\ConversionController@index');
    Route::get('/advanced-settings/language', 'BackOffice\Settings\AdvancedSettings\LanguageController@index');
    Route::get('/advanced-settings/e-pin', 'BackOffice\Settings\AdvancedSettings\EPinController@index');
    Route::get('/advanced-settings/custom-field', 'BackOffice\Settings\AdvancedSettings\CustomFieldController@index');
    Route::get('/advanced-settings/user-dashboard', 'BackOffice\Settings\AdvancedSettings\UserDashboardController@index');
    Route::get('/advanced-settings/tree', 'BackOffice\Settings\AdvancedSettings\TreeController@index');

    Route::post('/commission-settings/compensation/unilevel-commission/store', 'BackOffice\Settings\CommissionSettings\Compensation\UnilevelCommissionController@store');

});


Route::prefix('backoffice/profile-management')->group(function () {
    Route::get('/member-overview', 'BackOffice\ProfileManagement\MemberOverviewController@index');
    Route::get('/member-list', 'BackOffice\ProfileManagement\MemberListController@index');
    Route::post('/get-users', 'BackOffice\ProfileManagement\MemberListController@show_data');
    Route::get('/members/{id_number}', 'BackOffice\ProfileManagement\MemberListController@show');
    Route::get('/change-login-password', 'BackOffice\ProfileManagement\ChangeLoginPasswordController@index');
    Route::get('/change-transaction-password', 'BackOffice\ProfileManagement\ChangeTransactionPasswordController@index');
    Route::get('/update-pv', 'BackOffice\ProfileManagement\UpdatePVController@index');
    Route::get('/kyc-details', 'BackOffice\ProfileManagement\KYCDetailsController@index');
    Route::post('/members/update_distributor/{user_id}', 'BackOffice\ProfileManagement\MemberListController@update');
});


Route::prefix('backoffice/control-panel')->group(function () {
    Route::get('/create-entry-package', 'BackOffice\ControlPanel\CreateEntryPackageController@index');
    Route::get('/create-entry-package/delete-entry-package/{id}', 'BackOffice\ControlPanel\CreateEntryPackageController@destroy');
    Route::get('/create-entry-package/show-entry-package/{id}', 'BackOffice\ControlPanel\CreateEntryPackageController@show');
    Route::get('/create-entry-package/entry-package-inclusion/{package_unique_code}', 'BackOffice\ControlPanel\CreateEntryPackageController@package_inclusion');
    Route::get('/create-entry-package/delete-inclusion/{inclusion_id}', 'BackOffice\ControlPanel\CreateEntryPackageController@delete_inclusion');

    Route::get('/create-entry-package/add-entry-package/{id}', 'BackOffice\ControlPanel\EntryPackageController@index');

    Route::get('/create-entry-package/add-product/{id}', 'BackOffice\ControlPanel\ProductController@index');
    
    Route::post('/create-entry-package/add-inclusion/store', 'BackOffice\ControlPanel\CreateEntryPackageController@create_inclusions');
    Route::post('/create-entry-package/add-entry', 'BackOffice\ControlPanel\CreateEntryPackageController@store');
    Route::post('/create-entry-package/add-entry-package/store', 'BackOffice\ControlPanel\EntryPackageController@store');

});

Route::prefix('backoffice/reports')->group(function () {
    Route::get('/profile', 'BackOffice\Reports\ProfileController@index');
    Route::get('/activate-deactivate', 'BackOffice\Reports\ActivateDeactivateController@index');
    Route::get('/joining', 'BackOffice\Reports\JoiningController@index');
    Route::get('/commission', 'BackOffice\Reports\CommissionController@index');
    Route::get('/total-bonus', 'BackOffice\Reports\TotalBonusController@index');
    Route::get('/top-earners', 'BackOffice\Reports\TopEarnersController@index');
    Route::get('/payout', 'BackOffice\Reports\PayoutController@index');
    Route::get('/rank-performance', 'BackOffice\Reports\RankPerformanceController@index');
    Route::get('/e-pin-transfer', 'BackOffice\Reports\EPinTransferController@index');
});

Route::prefix('backoffice/privileged-users')->group(function () {
    Route::get('/register', 'BackOffice\PrivilegedUser\RegisterController@index');
    Route::get('/users-list', 'BackOffice\PrivilegedUser\UsersListController@index');
    Route::get('/change-password', 'BackOffice\PrivilegedUser\ChangePasswordController@index');
    Route::get('/user-activity', 'BackOffice\PrivilegedUser\UserActivityController@index');
});

Route::get('/downlines/{nUserId?}', function($nUserId = null) {
    if($nUserId === null) {
        // TODO: Get User ID of current logged in user
        $nUserId = 1;
    }

    $aRetVal = [];
    $oUser = App\User::find($nUserId);
    $aRetVal = new DownLinesResource($oUser);

    return $aRetVal;
});



Route::prefix('backoffice/tools')->group(function () {
    Route::get('/auto-responder', 'BackOffice\Tools\AutoResponderController@index');
    Route::get('/upload-materials', 'BackOffice\Tools\UploadMaterialsController@index');
    Route::get('/news', 'BackOffice\Tools\NewsController@index');
    Route::get('/faqs', 'BackOffice\Tools\FAQsController@index');
});

Route::prefix('backoffice/crm')->group(function () {
    Route::get('/dashboard', 'BackOffice\CRM\DashboardController@index');
    Route::get('/add-lead', 'BackOffice\CRM\AddLeadController@index');
    Route::get('/view-lead', 'BackOffice\CRM\ViewLeadController@index');
    Route::get('/graph', 'BackOffice\CRM\GraphController@index');
    Route::get('/view-lead-details', 'BackOffice\CRM\ViewLeadDetailsController@index');
});


Route::prefix('backoffice/e-commerce-store')->group(function () {


    Route::get('/product-setup/products', 'BackOffice\ECommerceStore\ProductSetup\ProductController@index');
    Route::get('/product-setup/promos', 'BackOffice\ECommerceStore\ProductSetup\PromosController@index');
    Route::get('/product-setup/products/new-product', 'BackOffice\ECommerceStore\ProductSetup\ProductController@new_product');
    Route::get('/product-setup/products/visit/{id}', 'BackOffice\ECommerceStore\ProductSetup\ProductController@show');
    Route::post('/product-setup/products/create', 'BackOffice\ECommerceStore\ProductSetup\ProductController@store');

    Route::post('/product-setup/products/distributor-pins', 'BackOffice\ECommerceStore\ProductSetup\ProductController@create');
});
Route::prefix('backoffice/e-pin')->group(function () {

    Route::get('/product-pins/generate', 'BackOffice\EPin\ProductPinsController@generate_pins');
    Route::get('/product-pins/products/generate/{id_number}/{country}', 'BackOffice\EPin\ProductPinsController@generate');

    Route::get('/product-pins/available-pins', 'BackOffice\EPin\ProductPinsController@unused_pins');
    Route::get('/product-pins/used-pins', 'BackOffice\EPin\ProductPinsController@used_pins');
    Route::get('/product-pins/status/{customer_id}/{pin_id}/{status}', 'BackOffice\EPin\ProductPinsController@status');
    Route::get('/product-pins/visit/{customer_id}', 'BackOffice\EPin\ProductPinsController@visit');
    Route::get('/product-pins/destroy/{customer_id}', 'BackOffice\EPin\ProductPinsController@destroy');
    Route::get('/product-pins/print/{customer_id}', 'BackOffice\EPin\ProductPinsController@print');
    Route::get('/product-pins/reports', 'BackOffice\EPin\ReportProductPinsController@report');
    Route::post('/product-pins/result', 'BackOffice\EPin\ReportProductPinsController@result');
    Route::post('/product-pins/mass-status-update', 'BackOffice\EPin\ProductPinsController@mass_status_update');
    
    
    Route::get('/paid-codes', 'BackOffice\EPin\PaidCodesController@index');
    Route::get('/generate/paid-codes/{id}', 'BackOffice\EPin\PaidCodesController@generate');
    Route::get('/paid-codes/visit/{customer_id}', 'BackOffice\EPin\PaidCodesController@visit');
    Route::get('/paid-codes/destroy/{customer_id}', 'BackOffice\EPin\PaidCodesController@destroy');
    Route::get('/paid-codes/print/{customer_id}', 'BackOffice\EPin\PaidCodesController@print');
    Route::get('/paid-codes/status/{customer_id}/{registration_id}/{status}', 'BackOffice\EPin\PaidCodesController@status');
    Route::post('/paid-codes/create', 'BackOffice\EPin\PaidCodesController@store');
    Route::post('/paid-codes/mass-status-update', 'BackOffice\EPin\PaidCodesController@mass_status_update');
    
    
    Route::get('/cd-codes', 'BackOffice\EPin\CDCodesController@index');
    Route::get('/generate/cd-codes/{id}', 'BackOffice\EPin\CDCodesController@generate');
    Route::get('/cd-codes/visit/{customer_id}', 'BackOffice\EPin\CDCodesController@visit');
    Route::get('/cd-codes/print/{customer_id}', 'BackOffice\EPin\CDCodesController@print');
    Route::get('/cd-codes/destroy/{customer_id}', 'BackOffice\EPin\CDCodesController@destroy');
    Route::get('/cd-codes/status/{customer_id}/{registration_id}/{status}', 'BackOffice\EPin\CDCodesController@status');
    Route::post('/c-d-codes/create', 'BackOffice\EPin\CDCodesController@store');
    Route::post('/cd-codes/mass-status-update', 'BackOffice\EPin\CDCodesController@mass_status_update');

    Route::get('/free-slot-codes', 'BackOffice\EPin\FreeSlotCodesController@index');
    Route::get('/generate/free-slot-codes/{id}', 'BackOffice\EPin\FreeSlotCodesController@generate');
    Route::get('/free-slot-codes/visit/{customer_id}', 'BackOffice\EPin\FreeSlotCodesController@visit');
    Route::get('/free-slot-codes/print/{customer_id}', 'BackOffice\EPin\FreeSlotCodesController@print');
    Route::get('/free-slot-codes/destroy/{customer_id}', 'BackOffice\EPin\FreeSlotCodesController@destroy');
    Route::get('/free-slot-codes/status/{customer_id}/{registration_id}/{status}', 'BackOffice\EPin\FreeSlotCodesController@status');
    Route::post('/free-slot-codes/create', 'BackOffice\EPin\FreeSlotCodesController@store');
    Route::post('/free-slot-codes/mass-status-update', 'BackOffice\EPin\FreeSlotCodesController@mass_status_update');

    Route::get('/download-pins', 'BackOffice\EPin\DownloadEPinController@index');

    Route::get('/reports', 'BackOffice\EPin\ReportEPinsController@index');
    Route::post('/reports/result', 'BackOffice\EPin\ReportEPinsController@result');
});

Route::prefix('backoffice/support-center')->group(function () {
    Route::get('/ticket-dashboard', 'BackOffice\SupportCenter\TicketDashboardController@index');
    Route::get('/open-tickets', 'BackOffice\SupportCenter\OpenTicketsController@index');
    Route::get('/view-tickets', 'BackOffice\SupportCenter\ViewTicketsController@index');
    Route::get('/resolved-tickets', 'BackOffice\SupportCenter\ResolvedTicketsController@index');
    Route::get('/configuration', 'BackOffice\SupportCenter\ConfigurationController@index');
    Route::get('/category', 'BackOffice\SupportCenter\CategoryController@index');
    Route::get('/timeline', 'BackOffice\SupportCenter\TimelineController@index');
});

Route::prefix('backoffice/database')->group(function () {
    Route::get('/import', 'BackOffice\Database\ImportController@index');
    Route::get('/export', 'BackOffice\Database\ExportController@index');
});

Route::prefix('backoffice/ecpay')->group(function () {
    Route::get('/dashboard', 'BackOffice\ECPay\DashboardController@index');
});

Route::prefix('backoffice/booking-travel')->group(function () {
    Route::get('/dashboard', 'BackOffice\BookingTravel\DashboardController@index');
});



// Members 

Route::middleware(['SessionChecker'])->group(function () {
    //
    Route::get('virtualoffice/dashboard', 'VirtualOffice\DashboardController@index');
     Route::get('virtualoffice/support-center', 'VirtualOffice\SupportCenterController@index');
    Route::get('virtualoffice/e-commerce-store', 'VirtualOffice\ECommerceStoreController@index');
    Route::get('virtualoffice/e-commerce-store/package/add-to-cart/{package_id}', 'VirtualOffice\ECommerceStoreController@package_add_to_cart');
    Route::get('virtualoffice/e-wallet', 'VirtualOffice\EWalletController@index');
    Route::get('virtualoffice/payout', 'VirtualOffice\PayoutController@index');
    Route::get('virtualoffice/e-pin', 'VirtualOffice\EPinController@index');
    Route::get('virtualoffice/my-profile', 'VirtualOffice\MyProfileController@index');
    Route::get('virtualoffice/bills-payments', 'VirtualOffice\BillsPaymentsController@index');
    Route::get('virtualoffice/unilevel', 'VirtualOffice\Network\UnilevelProductController@index');
    Route::get('/virtualoffice/e-commerce-store/add_to_cart/{id}/{country}', 'VirtualOffice\ECommerceStoreController@add_to_cart');
    Route::get('/virtualoffice/e-commerce-store/package/cart', 'VirtualOffice\ECommerceStoreController@package_cart');

    Route::prefix('virtualoffice/booking')->group(function () {
        Route::get('/markup', 'VirtualOffice\Booking\MarkupController@index');
        Route::get('/booking-history/{pnr}', 'VirtualOffice\Booking\BookingHistoryController@ticket');
        Route::post('/markup/create', 'VirtualOffice\Booking\MarkupController@create');
    });

    Route::prefix('virtualoffice')->group(function () {
        Route::get('/register', 'VirtualOffice\RegisterController@index');
        Route::get('/register/sub-account/{sponsor_id}/{id_number}/{position}', 'VirtualOffice\RegisterController@create_sub_account');
        Route::get('/register/main-account/{sponsor_id}/{id_number}/{position}', 'VirtualOffice\RegisterController@create_main_account');
    });

    Route::prefix('virtualoffice/encashment')->group(function () {
        Route::get('/encash', 'VirtualOffice\Encashment\EncashController@index');
        Route::get('/encashment-history', 'VirtualOffice\Encashment\EncashmentHistoryController@index');
        Route::get('/redeem', 'VirtualOffice\Encashment\RedeemGCController@index');
        Route::get('/rewards-history', 'VirtualOffice\Encashment\RewardsHistoryController@index');
    });

    Route::prefix('virtualoffice/top-up')->group(function () {
        Route::get('/smart', 'VirtualOffice\TopUp\SmartController@index');
        Route::get('/globe', 'VirtualOffice\TopUp\GlobeController@index');
        Route::get('/dito', 'VirtualOffice\TopUp\DitoController@index');
        Route::post('/store', 'VirtualOffice\TopUp\SmartController@store');
    });

    Route::prefix('virtualoffice/my-account')->group(function () {
        Route::get('/sub-account', 'VirtualOffice\MyAccount\SubAccountController@index');
        Route::get('/sub-account/secure/{id_number}', 'VirtualOffice\MyAccount\SubAccountController@login_sub_account');
        Route::get('/profile', 'VirtualOffice\MyAccount\AccountsController@index');
        Route::get('/convert-current-income', 'VirtualOffice\MyAccount\SubAccountController@index');
        Route::post('/update-pincode', 'VirtualOffice\MyAccount\AccountsController@update_pincode');
    });


    Route::prefix('virtualoffice/network')->group(function () {
        Route::get('/genealogy-tree', 'VirtualOffice\Network\GenealogyTreeController@index');
        Route::get('/sponsor-tree', 'VirtualOffice\Network\SponsorTreeController@index');
        Route::get('/tree-view', 'VirtualOffice\Network\TreeViewController@index');
        Route::get('/member-list', 'VirtualOffice\Network\MemberListController@index');
        Route::get('/genealogy/new-account', 'VirtualOffice\Network\NewAccountController@index');
        Route::get('/genealogy/demo-account', 'VirtualOffice\Network\DemoAccountController@index');
    
        Route::get('/genealogy-tree/visit/{id_number}', 'VirtualOffice\Network\GenealogyTreeController@visit');
    });

    
    Route::prefix('virtualoffice/order-details')->group(function () {
        Route::get('/orders', 'VirtualOffice\OrderDetails\OrdersController@index');
        Route::get('/orders/view-details', 'VirtualOffice\OrderDetails\ViewDetailsController@index');
        Route::get('/order-history', 'VirtualOffice\OrderDetails\OrderHistoryController@index');
        Route::get('/track-order/{reference}', 'VirtualOffice\OrderDetails\OrderHistoryController@track');
        Route::get('/view-orders/{reference}', 'VirtualOffice\OrderDetails\OrderHistoryController@view_orders');
        Route::get('/package-track-order/{reference}', 'VirtualOffice\OrderDetails\OrderHistoryController@package_track');
        Route::get('/package-view-orders/{reference}', 'VirtualOffice\OrderDetails\OrderHistoryController@package_view_orders');
        Route::get('/order-reports', 'VirtualOffice\OrderDetails\OrderReportsController@index');
    });

    Route::prefix('virtualoffice/store')->group(function () {
        Route::get('/cart', 'VirtualOffice\ECommerce\CartController@index');
        Route::get('/package/checkout', 'VirtualOffice\ECommerce\CheckoutController@package');
        Route::get('/checkout', 'VirtualOffice\ECommerce\CheckoutController@index');
        Route::get('/place-order', 'VirtualOffice\ECommerce\CheckoutController@place_order');
        Route::get('/package/place-order', 'VirtualOffice\ECommerce\CheckoutController@package_place_order');
        Route::post('/cart/package/update', 'VirtualOffice\ECommerce\CartController@package_update');
        Route::post('/cart/update', 'VirtualOffice\ECommerce\CartController@update');
        Route::post('/processing', 'VirtualOffice\ECommerce\CheckoutController@processing');
    Route::post('/package/processing', 'VirtualOffice\ECommerce\CheckoutController@package_processing');
   
   
    });
    
   

    
    Route::prefix('virtualoffice/mailbox')->group(function () {
        Route::get('/inbox', 'VirtualOffice\Mailbox\InboxController@index');
        Route::get('/compose', 'VirtualOffice\Mailbox\ComposeMailController@index');
        Route::get('/sent-mail', 'VirtualOffice\Mailbox\SentItemsController@index');
        Route::get('/trash', 'VirtualOffice\Mailbox\TrashMailController@index');
        Route::get('/mail-read', 'VirtualOffice\Mailbox\MailReadController@index');
    });
    
    Route::prefix('virtualoffice/support-center')->group(function () {
        Route::get('/tickets', 'VirtualOffice\SupportCenter\TicketsController@index');
        Route::get('/compose', 'VirtualOffice\SupportCenter\ComposeTicketsController@index');
        Route::get('/sent-tickets', 'VirtualOffice\SupportCenter\SentTicketsController@index');
    });
    
    Route::prefix('virtualoffice/crm')->group(function () {
        Route::get('/dashboard', 'VirtualOffice\CRM\DashboardController@index');
        Route::get('/add-lead', 'VirtualOffice\CRM\AddLeadController@index');
        Route::get('/view-lead', 'VirtualOffice\CRM\ViewLeadController@index');
        Route::get('/graph', 'VirtualOffice\CRM\GraphController@index');
    });
    
    Route::prefix('virtualoffice/tools')->group(function () {
        Route::get('/replication-site', 'VirtualOffice\Tools\ReplicationSiteController@index');
        Route::get('/download-materials', 'VirtualOffice\Tools\DownloadMaterialsController@index');
        Route::get('/news', 'VirtualOffice\Tools\NewsController@index');
        Route::get('/faqs', 'VirtualOffice\Tools\FAQsController@index');
    });
        

    Route::prefix('virtualoffice/reports')->group(function () {
        Route::get('/commission', 'VirtualOffice\Reports\CommissionController@index');
        Route::get('/payout', 'VirtualOffice\Reports\PayoutController@index');
        Route::get('/rank-performance', 'VirtualOffice\Reports\RankPerformanceController@index');
        Route::get('/e-pin-transfer', 'VirtualOffice\Reports\EPinTransferController@index');
        Route::get('/top-earners', 'VirtualOffice\Reports\TopEarnersController@index');
        Route::get('/top-recruiters', 'VirtualOffice\Reports\TopRecruitersController@index');
    });



    Route::prefix('virtualoffice/payout')->group(function () {
        Route::get('/encash', 'VirtualOffice\Payout\EncashController@index');
        Route::get('/payout-history', 'VirtualOffice\Payout\PayoutHistoryController@index');
    });

    
Route::prefix('virtualoffice/code-management')->group(function () {
    Route::get('/my-pins/paid-pins', 'VirtualOffice\CodeManagement\MyPins\PaidPinsController@index');
    Route::get('/my-pins/download-pins', 'VirtualOffice\CodeManagement\MyPins\DownloadPinsController@index');
    Route::get('/my-pins/transfer-pins', 'VirtualOffice\CodeManagement\MyPins\TransferPinsController@index');
    Route::get('/my-pins/transfer-history', 'VirtualOffice\CodeManagement\MyPins\TransferHistoryController@index');
    Route::get('/my-pins/report-pins', 'VirtualOffice\CodeManagement\MyPins\TransferHistoryController@report_pins');
    // Route::get('/binary-codes/free-slot-codes', 'VirtualOffice\CodeManagement\BinaryCodes\FreeSlotCodesController@index');
    // Route::get('/binary-codes/cd-codes', 'VirtualOffice\CodeManagement\BinaryCodes\CDCodesController@index');
    Route::get('/my-product/product-pins', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsController@index');
    Route::get('/my-product/encode-product', 'VirtualOffice\CodeManagement\MyProduct\EncodeProductController@index');
    Route::get('/my-product/product-cart', 'VirtualOffice\CodeManagement\MyProduct\ProductCartController@index');
    Route::get('/my-product/purchase-product-history', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsHistoryController@index');
    Route::get('/my-product/purchase-product-history/view/{reference}', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsHistoryController@show');
    Route::get('/my-product/transfer-pins', 'VirtualOffice\CodeManagement\MyProduct\TransferPinsController@index');
    Route::get('/my-product/transfer-pins-history', 'VirtualOffice\CodeManagement\MyProduct\TransferPinsHistoryController@index');
    Route::get('/my-product/report-pins', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsController@report_pins');
    Route::get('/my-product/unilevel-maintenance', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsController@unilevel_maintenance');

    Route::get('/my-bonus/direct-referral', 'VirtualOffice\CodeManagement\MyBonus\DirectReferralController@index');
    Route::get('/my-bonus/indirect-referral', 'VirtualOffice\CodeManagement\MyBonus\IndirectReferralController@index');
    Route::get('/my-bonus/pairing-bonus', 'VirtualOffice\CodeManagement\MyBonus\PairingBonusController@index');
    Route::get('/my-bonus/unilevel-bonus', 'VirtualOffice\CodeManagement\MyBonus\UnilevelBonusController@index');
    Route::get('/my-bonus/markup-bonus', 'VirtualOffice\CodeManagement\MyBonus\MarkupBonusController@index');


    Route::get('/my-product/product-cart/remove-item-from-cart/{id}', 'VirtualOffice\CodeManagement\MyProduct\ProductCartController@remove_item_from_cart');
    

    Route::get('/product-codes', 'VirtualOffice\CodeManagement\ProductCodesController@index');
    Route::get('/promos', 'VirtualOffice\CodeManagement\PromosController@index');
    Route::get('/activation-code', 'VirtualOffice\CodeManagement\EPinController@index');
});

Route::prefix('virtualoffice/transaction-history')->group(function () {
    Route::get('/direct-referral-bonus', 'VirtualOffice\TransactionHistory\DirectReferralBonusController@index');
    Route::get('/sales-match-bonus', 'VirtualOffice\TransactionHistory\SalesMatchBonusController@index');
    Route::get('/royalty-sales-match-bonus', 'VirtualOffice\TransactionHistory\RoyaltySalesMatchBonusController@index');
    Route::get('/product-sales-bonus', 'VirtualOffice\TransactionHistory\ProductSalesBonusController@index');
    Route::get('/unilevel-product-bundle', 'VirtualOffice\TransactionHistory\UnilevelProductBundleController@index');
    Route::get('/unilevel-product-retail', 'VirtualOffice\TransactionHistory\UnilevelProductRetailController@index');
    Route::get('/incentive-points', 'VirtualOffice\TransactionHistory\IncentivePointsController@index');
    Route::get('/e-cash-wallet', 'VirtualOffice\TransactionHistory\ECashWalletController@index');
    Route::get('/shopping-wallet', 'VirtualOffice\TransactionHistory\ShoppingWalletController@index');
});


Route::prefix('virtualoffice/account')->group(function () {

    Route::get('/my-profile', 'VirtualOffice\Account\AccountController@index');
});


    

// end 

});

Route::post('virtualoffice/dashboard/secure-login', 'VirtualOffice\DashboardController@SecureLogin');
Route::post('virtualoffice/e-wallet/store', 'VirtualOffice\EWalletController@store');
Route::prefix('virtualoffice')->group(function () {
    Route::post('/register/store_main_account', 'VirtualOffice\RegisterController@store');
    Route::post('/register/store_sub_account', 'VirtualOffice\RegisterController@store_sub_account');
    Route::get('/register/sample', 'VirtualOffice\RegisterController@sample');
});

Route::prefix('virtualoffice/encashment')->group(function () {
    Route::post('/encash-bonus', 'VirtualOffice\Encashment\EncashController@encash_bonus');
});


Route::prefix('virtualoffice/my-account')->group(function () {
    Route::post('/sub-account/transfer-wallet', 'VirtualOffice\MyAccount\SubAccountController@transfer_wallet');
    Route::post('/sub-account/convert-income-to-wallet', 'VirtualOffice\MyAccount\SubAccountController@income_to_wallet');
    Route::post('/update_distributor/{distributor_id}', 'VirtualOffice\MyAccount\AccountsController@update');
});

Route::prefix('virtualoffice/network')->group(function () {
    Route::post('/genealogy-tree/result', 'VirtualOffice\Network\GenealogyTreeController@result');
    Route::post('/genealogy-tree/processing', 'VirtualOffice\Network\GenealogyTreeController@processing');
});

Route::prefix('virtualoffice/store')->group(function () {
    Route::post('/processing', 'VirtualOffice\ECommerce\CheckoutController@processing');
    Route::post('/package/processing', 'VirtualOffice\ECommerce\CheckoutController@package_processing');
    Route::post('/cart/update', 'VirtualOffice\ECommerce\CartController@update');
    Route::post('/cart/package/update', 'VirtualOffice\ECommerce\CartController@package_update');
});

Route::prefix('virtualoffice/code-management')->group(function () {
    Route::post('/my-product/report-pins/result', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsController@result');
    Route::post('/my-product/product-pins/add-to-cart', 'VirtualOffice\CodeManagement\MyProduct\ProductPinsController@internal_add_to_cart');
    Route::post('/my-product/product-cart/unilevel', 'VirtualOffice\Network\UnilevelProductController@unilevel');

});
// Member 
Route::get('memberoffice/login', 'MemberOffice\LoginController@index');
Route::get('memberoffice/dashboard', 'MemberOffice\DashboardController@index');
Route::get('memberoffice/my-pins/paid-pins', 'MemberOffice\PaidPinsController@index');